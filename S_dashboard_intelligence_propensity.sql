DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_intelligence_propensity`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
    declare IN_AGGREGATION_4 text;
    declare IN_AGGREGATION_5 text;
    declare IN_AGGREGATION_6 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
    SET IN_AGGREGATION_4 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_4"));
    SET IN_AGGREGATION_5 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_5"));
    SET IN_AGGREGATION_6 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_6"));
    
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    
	SET @filter_cond=concat(" AGGREGATION = '",IN_AGGREGATION_2,"' ");
    SET @measure_cond=concat(" Measure = '",IN_AGGREGATION_3,"' ");
    
    
		 set sql_safe_updates = 0; -- to update cumulative base
		 UPDATE  Dashboard_Insights_Propensity
		 set CumlBase = Base * Decile; 
			
    IF IN_MEASURE = 'MODEL_NAME' -- to display all the model names
    THEN
		set @view_stmt = concat('CREATE OR REPLACE VIEW V_Dashboard_Propensity_Names AS
								SELECT DISTINCT ModelName FROM Dashboard_Insights_Propensity ');
                                
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Model_Name",`ModelName`) )),"]") into @temp_result from V_Dashboard_Propensity_Names;') ;
SELECT @Query_String;
    END IF;
    
    IF IN_AGGREGATION_1 = 'SHOW_TRAINING' -- to switch between traning and predicted models
	THEN
		set @chart_select = 'Training';
    ELSEIF IN_AGGREGATION_1 = 'SHOW_PREDICTED'
    THEN
		set @chart_select = 'Predicted';
    END IF;
    
    IF IN_AGGREGATION_6 IS NULL OR IN_AGGREGATION_6 = ''
    THEN
    SELECT DISTINCT ModelName from Dashboard_Insights_Propensity order by TrainingEndDate limit 1 into @model_select; -- selects the latest model by default
    ELSE
    SET @model_select = IN_AGGREGATION_6;
    END IF;
    
    select @model_select,@chart_select;
     
	IF IN_MEASURE = 'TABLE'
	THEN
		set @view_stmt = concat('create or replace  View V_Dashboard_Propensity_Table as
		SELECT 
			Decile,
			`Base`, 
			`Conversions`, 
			concat(round(ifnull(ConversionsPercent*100,0),2),"%") AS `Conversion %`, 
			CONCAT(round((Gain *100),2),"%") as `Gain`, 
			CumlConversions, 
			concat(round(ifnull(CumlConversionsPercent*100,0),2),"%") AS `Cuml Conv. %`  
		FROM Dashboard_Insights_Propensity
		WHERE ModelName = "',@model_select,'"
		and ChartType = "',@chart_select,'"
		GROUP BY Decile
		');
        
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object(
		"Decile",`Decile`,
		"Base",CAST(format(ifnull(`Base`,0),",") AS CHAR),
		"Conversions",CAST(format(ifnull(`Conversions`,0),",") AS CHAR),
		"Conversion %",`Conversion %`,
		"Gain",`Gain`,
		"CumlConversions",`CumlConversions`,
		"Cuml Conv. %",`Cuml Conv. %`
		) )),"]")into @temp_result from V_Dashboard_Propensity_Table;') ;
	END IF;
	
    IF IN_MEASURE = 'GRAPH' AND IN_AGGREGATION_2 = 'CONVERSION' -- CONV % X GAIN
		THEN
			set @view_stmt= CONCAT(' create or replace view V_Dashboard_Propensity_Graph as
							SELECT 
                            Decile, 
                            cast(round(ifnull(ConversionsPercent*100,0),2) as DECIMAL(10,2)) AS value2,
                            cast(round((Gain *100),2) as DECIMAL(10,2)) AS  value1
							from Dashboard_Insights_Propensity
							WHERE ModelName = "',@model_select,'"
							and ChartType = "',@chart_select,'"
							GROUP BY Decile
							order by 1 ASC
							');
     
			select @view_stmt;
			PREPARE statement from @view_stmt;
			Execute statement;
			Deallocate PREPARE statement; 
			
			Select Case when count(1)< 5 Then "1"
				else "0"
				end into @flag
				From V_Dashboard_Propensity_Graph;
     
			SET @Query_String =  CONCAT('SELECT CONCAT("[",(GROUP_CONCAT(json_object("displayName",`Decile`,"value1",`value1`,"value2",`value2`,"Flag",@flag) )),"]")into @temp_result 
            from V_Dashboard_Propensity_Graph;') ;
	END IF;
	
    IF IN_MEASURE = 'GRAPH' AND IN_AGGREGATION_2 = 'CUSTOMER' -- BASE X CONVERSIONS
		THEN

			set @view_stmt= CONCAT(' create or replace view V_Dashboard_Propensity_Graph as
							SELECT Decile, 
                            CAST(ifnull(CumlBase,0) AS INT) AS value2,
                            CAST(ifnull(CumlConversions,0) AS INT) AS  value1
							from Dashboard_Insights_Propensity
							WHERE ModelName = "',@model_select,'"
							and ChartType = "',@chart_select,'"
							GROUP BY Decile
							order by 1 ASC
							');
			select @view_stmt;
			PREPARE statement from @view_stmt;
			Execute statement;
			Deallocate PREPARE statement; 
			
			Select Case when count(1)< 5 Then "1"
				else "0"
				end into @flag
				From V_Dashboard_Propensity_Graph;
			SET @Query_String =  CONCAT('SELECT CONCAT("[",(GROUP_CONCAT(json_object("displayName",`Decile`,"value1",`value1`,"value2",`value2`,"Flag",@flag) )),"]")into @temp_result from V_Dashboard_Propensity_Graph;') ;
            
	END IF;
    
    
    IF IN_MEASURE = 'MODEL_NOTES'
    THEN
		SELECT 
			`Cuml Conv. %`
		FROM
			V_Dashboard_Propensity_Table
		WHERE
			Decile = 10 into @entire_base_conv;

		SELECT 
			`Cuml Conv. %`
		FROM
			V_Dashboard_Propensity_Table
		WHERE
			Decile = 3 into @top_three_conv;

		SELECT 
			`Gain`
		FROM
			V_Dashboard_Propensity_Table
		WHERE
			Decile = 3 into @top_three_cap;
            
		set @out1 = concat('Targeting the entire base is likely to give a ',@entire_base_conv,' conversion rate');
        set @out2 = concat('Targeting the Top 3 Deciles is likely to give ',@top_three_conv,' conversion rate');
        set @out3 = concat('Targeting the Top 3 Deciles captures ',@top_three_cap,' of the conversions');
        
        select @out1, @out2, @out3;
        

		
		SET @Query_String =  CONCAT('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Card_1","',@out1,'","Card_2","',@out2,'","Card_3","',@out3,'") )),"]")into @temp_result;') ;
        
	
    END IF;
    
    IF IN_MEASURE = 'MODEL_COUNTS'
    THEN
		SELECT 
			round(ifnull(SUM(Conversions),0),2)
		FROM V_Dashboard_Propensity_Table
		WHERE
			Decile = 1 into @num1;
            select @num1;

		SELECT 
			round(ifnull(SUM(Conversions),0),2)
		FROM V_Dashboard_Propensity_Table
		WHERE
			Decile < 3 into @top_three_conv;
		select @top_three_conv;
        
		SELECT 
			round(ifnull(SUM(Conversions),0),2)
		FROM V_Dashboard_Propensity_Table
		WHERE
			Decile < 6 into @top_three_cap;
        select @top_three_cap;  
            
            
		SELECT 
			round(ifnull(max(CumlConversions),0),2)
		FROM V_Dashboard_Propensity_Table
		into @max_cuml_conversion;
        select @max_cuml_conversion;
        
        set @out1_percent = round((@num1*100/@max_cuml_conversion),1);
        set @out2_percent = round((@top_three_conv *100/ @max_cuml_conversion) ,1);
        set @out3_percent = round((@top_three_cap *100/ @max_cuml_conversion) ,1);
            
		set @out1 = concat('',cast(format(@num1,",") as char),' Customers out of ',cast(format(@max_cuml_conversion,",") as char),' (',@out1_percent,'%)');
        set @out2 = concat('',cast(format(@top_three_conv,",") as char),' Customers out of ',cast(format(@max_cuml_conversion,",") as char),' (',@out2_percent,'%)');
        set @out3 = concat('',cast(format(@top_three_cap,",") as char),' Customers out of ',cast(format(@max_cuml_conversion,",") as char),' (',@out3_percent,'%)');
        
        select @out1, @out2, @out3;
		
		SET @Query_String =  CONCAT('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Card_1","',@out1,'","Card_2","',@out2,'","Card_3","',@out3,'") )),"]")into @temp_result;') ;
        
	
    END IF;
        
    IF IN_SCREEN <> "DOWNLOAD"
    THEN
    select @view_stmt;
	PREPARE statement from @view_stmt;
	Execute statement;
	Deallocate PREPARE statement;
	SELECT @Query_String;
	PREPARE statement from @Query_String;
	Execute statement;
	Deallocate PREPARE statement; 
    SET out_result_json = @temp_result;
    ELSE
		call S_dashboard_performance_download('V_Dashboard_Propensity_Table',@out_result_json1);
		SELECT @out_result_json1 INTO out_result_json;
    END IF;
    
END$$
DELIMITER ;
