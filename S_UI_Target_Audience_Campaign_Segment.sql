
CREATE OR REPLACE PROCEDURE `S_UI_Target_Audience_Campaign_Segment`(IN in_request_json json, OUT out_result_json json)
BEGIN

    -- User Information
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 

    -- Target Audience	
    declare Requested_Details text;
    declare Input_Data  NVARCHAR(65536);
	declare Input_Data1  NVARCHAR(65536);
	 
   -- User Information
	Set in_request_json = Replace(in_request_json,'\n','\\n');
	Set in_request_json = Replace(in_request_json,'\\n','');
    Set in_request_json = Replace(in_request_json,'\\','');
   
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));

    -- Target Audience

	SET Requested_Details = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET Input_Data= json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET Input_Data1= json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));

      
    -- Checking The Condition Given
    
    
    if Requested_Details='Valid_SVOC_Condition' and Input_Data !='' 
     Then           
     
                     select Input_Data as '';
                     select Requested_Details as '';
          
                    SET @Campaign_Segment_Name = '';
     				Set Input_Data =TRIM(Input_Data);
                    
                    
					
                    
                    Select Input_Data as '';
                    
                    
                    
					SET @Query_String = concat('
					SELECT COUNT(1) INTO @Valid_SVOC FROM V_CLM_Customer_One_View WHERE ',Input_Data,'
                    LIMIT 1') ;
						      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 

			 			
		                    SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","This Is A Valid SVOC Condition") )),"]")
													    into @temp_result 
													   ;') ;
						      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
							

					if @temp_result is null
						then set @temp_result="[]";
					End if;

	End If;


    
    
    
    
-- Checking If The Condtion Already Exists    
    
    
    if Requested_Details='Existing_Campaign_Segment' and Input_Data !='' 
     Then           
     
                     select Input_Data as '';
                     select Requested_Details as '';
          
                    SET @Campaign_Segment_Name = '';
     				Set Input_Data =TRIM(Input_Data);
                    
                    
					
                    
                    Select Input_Data as '';
                    
                    
                    
					SET @Query_String = concat('
					SELECT Segment_Name INTO @Campaign_Segment_Name FROM Campaign_Segment WHERE TRIM(Segment_Condition_Solus) ="',Input_Data,'"
                    AND Segment_Scheme = "CLM Segment" LIMIT 1;') ;
						      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 

     				If @Campaign_Segment_Name   = '' OR @Campaign_Segment_Name is NULL
     				Then
			 			
		                    SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","No Campaign Segment Found") )),"]")
													    into @temp_result 
													   ;') ;
						      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 

					Else 
							SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","A Campaign Segment With This SVOC Condition Already Exists","Campaign_Segment","',@Campaign_Segment_Name,'") )),"]")
													    into @temp_result 
													   ;') ;
						      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
							
					End If;

					if @temp_result is null
						then set @temp_result="[]";
					End if;

	End If;




   -- Checking The Name Given
    
    if Requested_Details='Existing_Campaign_Segment_Name' and Input_Data !='' 
     Then           
     
                     select Input_Data as '';
                     select Requested_Details as '';
          
                    SET @Campaign_Segment_Name = '';
                    
                    
					
                    
                    Select Input_Data as '';
                    
					SET @Query_String = concat('
					SELECT Segment_Name INTO @Campaign_Segment_Name FROM Solus_Segment WHERE Segment_Name ="',Input_Data,'"
                    ;') ;
						      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 

     				If @Campaign_Segment_Name   = '' OR @Campaign_Segment_Name is NULL
     				Then
			 			
		                    SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Segment Name Is Not Taken") )),"]")
													    into @temp_result 
													   ;') ;
						      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 

					Else 
							SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","This Name Is Already Taken") )),"]")
													    into @temp_result 
													   ;') ;
						      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
							
					End If;

					if @temp_result is null
						then set @temp_result="[]";
					End if;

	End If;
    
    

    -- Edit Existing Campaign Segment Condtion
    
    if Requested_Details='Edit_Condition' and Input_Data !='' AND Input_Data1 !=''
       Then         
				
                
                Set Input_Data= replace (Input_Data,"[",'');
				Set Input_Data= replace (Input_Data,"]",'');
				Set Input_Data= replace (Input_Data,'"','');
       
                 Select Input_Data as '';
                 Select Input_Data1 as '';
       
                   /*Update the Campaign Segment Condition */
     				Update Solus_Segment 
					SET Segment_Query_Condition = Input_Data1,
						Segment_Human_Readable_Condition = Input_Data1,
						Segment_Condition_Solus = Input_Data1
					WHERE Segment_Id = Input_Data
                    AND Segment_Type  = 'Campaign';


     				
		                    SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Condition is updated") )),"]")
													    into @temp_result 
													   ;') ;
						      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;
                            
                            
					if @temp_result is null
						then set @temp_result="[]";
					End if;

	End If;

    -- Create New Campaign Segment
    
    if Requested_Details='New_Campaign_Segment' AND Input_Data != '' AND Input_Data1 != '' 
       Then
	        
            /*Insert New Record Into Campaign Segment*/
            INSERT IGNORE INTO Solus_Segment(Segment_Name,
								  Segment_Type,
                                  Segment_Scheme,
                                  Segment_Query_Condition,
                                  Segment_Human_Readable_Condition,
                                  Segment_Condition_Solus)
                                  
								VALUES(Input_Data,
									   'Campaign',
									   'CLM Segment',
										Input_Data1,
										Input_Data1,
										Input_Data1);
            
            
	        SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","New Campaign Segment Is Created") )),"]")
									    into @temp_result
									   ;') ;
                                       SELECT @temp_result;
                                       
					      
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement; 
						
			If @temp_result is null
			   then set @temp_result="[]";
			End if;

	End If;
	
				
	if @temp_result is null
		Then
								
							Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Invalid Parameters") )),"]") into @temp_result; ') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
						
	End if;
							
    SET out_result_json = @temp_result;
    
    SET @temp_result = '';

 END

