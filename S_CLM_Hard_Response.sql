DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_CLM_Hard_Response`()
BEGIN
SET SQL_SAFE_UPDATES=0;
DELETE FROM CLM_Hard_Response
WHERE MonthYear=concat(monthname(Current_Date()),' ',year(CURRENT_DATE()));
INSERT INTO CLM_Hard_Response
select cs.CLMSegmentName as Segment,ceiling((day(eah.Event_Execution_Date_ID) - 
(6 - weekday(date_format(eah.Event_Execution_Date_ID,'%Y-%m-01'))))/7) 
+ case when 6 - weekday(date_format(eah.Event_Execution_Date_ID,'%Y-%m-01'))> 0 
then 1 else 0 end week_of_month,concat(monthname(eah.Event_Execution_Date_ID),' ',year(eah.Event_Execution_Date_ID)) as MonthYear,
eah.Event_ID as Event_ID,
em.Name as TriggerName,
SUM(Matched_ProductId) AS 'RecoMatched',
round(AVG(Matched_ProductGenome_Percent)*100,2) AS 'RecoGenomeMatched',
SUM(CASE WHEN IFNULL(SUBSTRING(Matched_ProductGenome,1,1),0) = '' THEN 0 ELSE IFNULL(SUBSTRING(Matched_ProductGenome,1,1),0) END) AS 'RecoAttr1Matched',
SUM(CASE WHEN IFNULL(SUBSTRING(Matched_ProductGenome,2,1),0) = '' THEN 0 ELSE IFNULL(SUBSTRING(Matched_ProductGenome,2,1),0) END) AS 'RecoAttr2Matched',
SUM(CASE WHEN IFNULL(SUBSTRING(Matched_ProductGenome,3,1),0) = '' THEN 0 ELSE IFNULL(SUBSTRING(Matched_ProductGenome,3,1),0) END) AS 'RecoAttr3Matched',
SUM(CASE WHEN IFNULL(SUBSTRING(Matched_ProductGenome,4,1),0) = '' THEN 0 ELSE IFNULL(SUBSTRING(Matched_ProductGenome,4,1),0) END) AS 'RecoAttr4Matched'
from Event_Attribution_History eah
join Event_Execution_History eeh ON eah.Event_Execution_ID=eeh.Event_Execution_ID
join CLM_Segment cs ON eeh.Segment_Id=cs.CLMSegmentId
join Event_Master em on em.ID=eah.Event_Id
where eah.Event_Execution_Date_Id>=DATE_FORMAT(CURRENT_DATE(),'%Y%m01')
and eeh.Event_Execution_Date_Id>=DATE_FORMAT(CURRENT_DATE(),'%Y%m01')
group by Segment,week_of_month,MonthYear,TriggerName
order by eah.Event_Execution_Date_ID DESC;
end$$
DELIMITER ;