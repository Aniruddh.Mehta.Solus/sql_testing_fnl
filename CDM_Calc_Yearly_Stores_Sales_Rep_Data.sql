DELIMITER $$
CREATE or REPLACE PROCEDURE `CDM_Calc_Yearly_Stores_Sales_Rep_Data`(IN vRep_Year SMALLINT)
BEGIN
	
	CALL CDM_Update_Process_Log('CDM_Calc_Yearly_Stores_Sales_Rep_Data', 1, 'CDM_Calc_Yearly_Stores_Sales_Rep_Data', 1);

	CALL CDM_Prepare_Store_Sales_Rep_Data(vRep_Year, '01');		
	CALL CDM_Prepare_Store_Sales_Rep_Data(vRep_Year, '02');		
	CALL CDM_Prepare_Store_Sales_Rep_Data(vRep_Year, '03');		
	CALL CDM_Prepare_Store_Sales_Rep_Data(vRep_Year, '04');		
	CALL CDM_Prepare_Store_Sales_Rep_Data(vRep_Year, '05');		
	CALL CDM_Prepare_Store_Sales_Rep_Data(vRep_Year, '06');		
	CALL CDM_Prepare_Store_Sales_Rep_Data(vRep_Year, '07');		
	CALL CDM_Prepare_Store_Sales_Rep_Data(vRep_Year, '08');		
	CALL CDM_Prepare_Store_Sales_Rep_Data(vRep_Year, '09');		
	CALL CDM_Prepare_Store_Sales_Rep_Data(vRep_Year, '10');		
	CALL CDM_Prepare_Store_Sales_Rep_Data(vRep_Year, '11');		
	CALL CDM_Prepare_Store_Sales_Rep_Data(vRep_Year, '12');		
	
	CALL CDM_Update_Process_Log('CDM_Calc_Yearly_Stores_Sales_Rep_Data', 2, 'CDM_Calc_Yearly_Stores_Sales_Rep_Data', 1);

END$$
DELIMITER ;
