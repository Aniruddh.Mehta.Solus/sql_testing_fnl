DELIMITER $$
CREATE or replace PROCEDURE `S_dashboard_load_CDM_Customer_Segment`(IN vBatchSize bigint)
BEGIN
DECLARE vStart_Cnt BIGINT DEFAULT 0;
DECLARE vEnd_Cnt BIGINT;
SELECT Customer_Id INTO vStart_Cnt FROM svoc_segments ORDER BY Customer_Id ASC LIMIT 1;
SELECT Customer_Id INTO @Rec_Cnt FROM svoc_segments ORDER BY Customer_Id DESC LIMIT 1;

PROCESS_LOOP: LOOP
SET vEnd_Cnt = vStart_Cnt + vBatchSize;
INSERT IGNORE INTO `CDM_Customer_Segment`
(`Customer_Id`,
`CLMSegmentName`,
`sequence`)
Values
(-1, 'NA', 0);

INSERT IGNORE INTO `CDM_Customer_Segment`
(`Customer_Id`,
`CLMSegmentName`,
`sequence`)
SELECT 
    `b`.`Customer_Id` AS `Customer_Id`,
    IFNULL(`a`.`Description`, 'NA') AS `CLMSegmentName`,
    `a`.`Sequence` AS `sequence`
FROM
    (`svoc_segments_master` `a`
    JOIN `svoc_segments` `b`)
WHERE customer_id between vStart_Cnt and vEnd_Cnt-1 
AND `b`.`Seg_For_model_3` = `a`.`Name`
AND `a`.`SVOC_Segment_For_Model_Id` = 3
AND `b`.`Customer_Id` > 0;

SET vStart_Cnt = vEnd_Cnt;
IF vStart_Cnt  >= @Rec_Cnt THEN
	LEAVE PROCESS_LOOP;
END IF;        
END LOOP PROCESS_LOOP;  
END$$
DELIMITER ;
