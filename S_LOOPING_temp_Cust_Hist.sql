DELIMITER $$
CREATE OR REPLACE PROCEDURE S_LOOPING_temp_Cust_Hist(IN vStart_DateId  varchar(512),IN vEnd_DateId  varchar(512),IN vBatchSize bigint)
BEGIN
DECLARE vStart_Cnt BIGINT DEFAULT 0;
DECLARE vEnd_Cnt BIGINT;
SELECT Customer_Id INTO vStart_Cnt FROM CDM_Customer_Master_Hist where Created_Date between str_to_date(vStart_DateId,'%Y-%m-%d') and str_to_date(vEnd_DateId,'%Y-%m-%d') ORDER BY Customer_Id ASC LIMIT 1;
SELECT Customer_Id INTO @Rec_Cnt FROM CDM_Customer_Master_Hist where Created_Date between str_to_date(vStart_DateId,'%Y-%m-%d') and str_to_date(vEnd_DateId,'%Y-%m-%d') ORDER BY Customer_Id DESC LIMIT 1;

PROCESS_LOOP: LOOP
SET vEnd_Cnt = vStart_Cnt + vBatchSize;
insert ignore into  temp_CDM_Customer_Master_Hist 
select * from CDM_Customer_Master_Hist where Customer_Id between vStart_Cnt and vEnd_Cnt 
and Created_Date between str_to_date(vStart_DateId,'%Y-%m-%d') and str_to_date(vEnd_DateId,'%Y-%m-%d');

SET vStart_Cnt = vEnd_Cnt;

IF vStart_Cnt  >= @Rec_Cnt THEN
	LEAVE PROCESS_LOOP;
END IF;        
END LOOP PROCESS_LOOP;  
END $$
DELIMITER ;
