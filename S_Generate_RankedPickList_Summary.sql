DELIMITER $$
CREATE or REPLACE PROCEDURE `S_Generate_RankedPickList_Summary`()
begin

/********************************************
* This Procedure will create a summary for rcore output  
* based on table RankedPickList and will populate in
* table RankedPickList_Summary
***********************************************/

    DECLARE done1,done2 INT DEFAULT FALSE;
    DECLARE Product_Genome_Lov_Id_cur1 BIGINT(20);
    
    /* Declaring cursor for Looping on distinct Product_Genome_LOV_Id  */
    Declare cur1 cursor for
    select distinct Product_Genome_LOV_Id from CDM_Product_Master;
    
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done1 = TRUE;
    SET done1 = FALSE;
    SET SQL_SAFE_UPDATES=0;
    set foreign_key_checks=0;
    
    /*******************************
    * START LOOP ALL GENOME LOV ID *
    *******************************/
    open cur1;
	new_loop_1: LOOP
		FETCH cur1 into Product_Genome_Lov_Id_cur1;
		IF done1 THEN
			LEAVE new_loop_1;
		END IF;
        
        -- Inserting in Summary table by grouping RankedPickList table on Product_Genome_LOV_Id and Hybridizer_Rank
		Insert Into RankedPickList_Summary(Product_Genome,Product_Genome_LOV_Id,Hybridizer_Rank,Num_of_Customers,Product_Id,Product_Name)
        (select
			CPM.Product_Genome,
            CPM.Product_Genome_LOV_Id,
            RPL.Hybridizer_Rank,
			count(Customer_Id) as Num_of_Customers,
			RPL.Product_Id,
            CPM.Product_Name
		from RankedPickList RPL JOIN CDM_Product_Master CPM ON RPL.Product_Id = CPM.Product_Id
        where Product_Genome_LOV_Id = Product_Genome_LOV_Id_cur1
		group by Product_Genome_LOV_Id,Hybridizer_Rank
        );
        
        -- Updating BU
        UPDATE RankedPickList_Summary a, CDM_Product_Master b, CDM_LOV_Master c
        set a.BU=c.LOV_Value
        where a.Product_Id = b.Product_Id
        and b.BU_LOV_Id = c.LOV_Id
        and a.BU = -1;
        
        -- Updating Section
        UPDATE RankedPickList_Summary a, CDM_Product_Master b, CDM_LOV_Master c
        set a.Section=c.LOV_Value
        where a.Product_Id = b.Product_Id
        and b.Section_LOV_Id = c.LOV_Id
        and a.Section = -1;
        
        -- Updating Brand
        UPDATE RankedPickList_Summary a, CDM_Product_Master b, CDM_LOV_Master c
        set a.Brand=c.LOV_Value
        where a.Product_Id = b.Product_Id
        and b.Brand_LOV_Id = c.LOV_Id
        and a.Brand = -1;
        
        -- Updating Division
        UPDATE RankedPickList_Summary a, CDM_Product_Master b, CDM_LOV_Master c
        set a.Div=c.LOV_Value
        where a.Product_Id = b.Product_Id
        and b.Div_LOV_Id = c.LOV_Id
        and a.Div = -1;
        
        -- Updating Dep
        UPDATE RankedPickList_Summary a, CDM_Product_Master b, CDM_LOV_Master c
        set a.Dep=c.LOV_Value
        where a.Product_Id = b.Product_Id
        and b.Dep_LOV_Id = c.LOV_Id
        and a.Dep = -1;
        
        -- Updating Cat
        UPDATE RankedPickList_Summary a, CDM_Product_Master b, CDM_LOV_Master c
        set a.Cat=c.LOV_Value
        where a.Product_Id = b.Product_Id
        and b.Cat_LOV_Id = c.LOV_Id
        and a.Cat = -1;
        
        -- Updating Season
        UPDATE RankedPickList_Summary a, CDM_Product_Master b, CDM_LOV_Master c
        set a.Season=c.LOV_Value
        where a.Product_Id = b.Product_Id
        and b.Season_LOV_Id = c.LOV_Id
        and a.Season = -1;

	end LOOP;
	close cur1;
    
end$$
DELIMITER ;
