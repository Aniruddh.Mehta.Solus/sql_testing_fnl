
CREATE or replace PROCEDURE `PROC_CLM_CU_CLM_Creative`(
	IN vUserId BIGINT, 
	IN vCreativeName VARCHAR(128), 
	IN vCreativeDesc VARCHAR(1024), 
	IN vExtCreativeKey VARCHAR(128), 
    IN vSenderKey VARCHAR(128),
    IN vTimeslot_Id BIGINT,
	IN vChannelId BIGINT, 
	IN vCreative TEXT, 
	IN vHeader TEXT, 
    IN vMicrosite_Creative TEXT,
    IN vMicrosite_Header TEXT,
	IN vTemplatePersonalizationScore TINYINT, 
	IN vOfferId BIGINT,
    IN vCommunication_Cooloff BIGINT,
    IN vCustom_Attr1 VARCHAR(255),
    IN vCustom_Attr2 VARCHAR(255),
    IN vCustom_Attr3 VARCHAR(255),
    IN vCustom_Attr4 VARCHAR(255),
    IN vCustom_Attr5 VARCHAR(255),
    IN vRecommendation_Type VARCHAR(2048),
	INOUT vCreativeId BIGINT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN

	DECLARE vRowCnt BIGINT DEFAULT 0;
	DECLARE vSqlstmt TEXT;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN 
		SET vSuccess = 0; 
		GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
		SET vErrMsg = CONCAT(IFNULL(vErrMsg,''), "ERROR ", @errno, " (", @sqlstate, "): ", @text);
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 0, vErrMsg, 0);
	END;

	SET vErrMsg = 'Start';
	SET vSuccess = 0;
	SET @CURR_PROC_NAME = 'PROC_CLM_CU_CLM_Creative';
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 1, vErrMsg, vSuccess);

	IF vCreativeName IS NULL OR vCreativeName = '' THEN
		SET vErrMsg = 'Creative Name is a Mandatory Field and cannot be empty or NULL';
		SET vSuccess = 0;
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 2, vErrMsg, vSuccess);
	ELSE
		IF vCreativeId IS NULL OR vCreativeId <= 0 THEN
			SET vSqlstmt = CONCAT(
				'INSERT IGNORE INTO CLM_Creative (
					CreativeName, 
					CreativeDesc, 
					ChannelId, 
					ExtCreativeKey,
                    SenderKey,
					Creative,
					Header,
                    Microsite_Creative,
                    Microsite_Header,
					TemplatePersonalizationScore,
					OfferId,
                    Communication_Cooloff,
                    Custom_Attr1,
                    Custom_Attr2,
                    Custom_Attr3,
                    Custom_Attr4,
                    Custom_Attr5,
                    RankedPickListStory_Id,
                    Timeslot_Id,
					CreatedBy, 
					ModifiedBy
				) VALUES ('
				, '"', vCreativeName, '"'
				, ',"', vCreativeDesc, '"' 
				, ',', vChannelId
				, ',"', vExtCreativeKey, '"' 
                , ',"', vSenderKey, '"' 
				, ',"', vCreative, '"' 
				, ',"', vHeader, '"' 
                , ',"', vMicrosite_Creative, '"' 
				, ',"', vMicrosite_Header, '"' 
				, ',', vTemplatePersonalizationScore
				, ',', vOfferId
                , ',', vCommunication_Cooloff
                , ',', vCustom_Attr1
                , ',', vCustom_Attr2
                , ',', vCustom_Attr3
                , ',', vCustom_Attr4
                , ',', vCustom_Attr5
                , ',', vRecommendation_Type
                , ',', vTimeslot_Id
				, ',', vUserId
				, ',', vUserId
				, ')'
			);

			SET vErrMsg = vSqlstmt;
			            
			SET @sql_stmt = vSqlstmt;
			PREPARE EXEC_STMT FROM @sql_stmt;
			EXECUTE EXEC_STMT;
			DEALLOCATE PREPARE EXEC_STMT;
			CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 3, vErrMsg, vSuccess);
		ELSE
			
				
			SELECT EXISTS(
				SELECT 1 
				FROM CLM_Campaign_Events
				WHERE 
					CreativeId = vCreativeId
					AND Has_Records_In_Execution_Hist = 1
			) INTO vRowCnt;
			
			IF vRowCnt = 0 THEN
				
				SET vSqlstmt = CONCAT(
					'INSERT IGNORE INTO CLM_Creative (
						CreativeId,
						CreativeName, 
						CreativeDesc, 
						ChannelId, 
						ExtCreativeKey,
						SenderKey,
						Creative,
						Header,
                        Microsite_Creative,
						Microsite_Header,
						TemplatePersonalizationScore,
						OfferId,
                        Communication_Cooloff,
                        Custom_Attr1,
                        Custom_Attr2,
                        Custom_Attr3,
                        Custom_Attr4,
                        Custom_Attr5,
                        RankedPickListStory_Id,
                        Timeslot_Id,
						CreatedBy, 
						ModifiedBy
					) VALUES ('
						, vCreativeId
						, ',"', vCreativeName, '"'
						, ',"', vCreativeDesc, '"' 
						, ',', vChannelId
						, ',"', vExtCreativeKey, '"' 
                        , ',"', vSenderKey, '"' 
						, ',"', vCreative, '"' 
						, ',"', vHeader, '"' 
						, ',"', vMicrosite_Creative, '"' 
						, ',"', vMicrosite_Header, '"' 
						, ',', vTemplatePersonalizationScore
						, ',', vOfferId
						, ',', vCommunication_Cooloff
                        , ',', vCustom_Attr1
                        , ',', vCustom_Attr2
                        , ',', vCustom_Attr3
                        , ',', vCustom_Attr4
                        , ',', vCustom_Attr5
						, ',', vRecommendation_Type
						, ',', vTimeslot_Id
						, ',', vUserId
						, ',', vUserId
						,') ON DUPLICATE KEY UPDATE '
						,' CreativeDesc = "', vCreativeDesc, '"'
						,', ChannelId = ', vChannelId
						,', ExtCreativeKey = "', vExtCreativeKey, '"'
                        ,', SenderKey = "', vSenderKey, '"'
						,', Creative = "', vCreative, '"'
						,', Header = "', vHeader, '"'
                        ,', Microsite_Creative = "', vMicrosite_Creative, '"'
						,', Microsite_Header = "', vMicrosite_Header, '"'
						,', TemplatePersonalizationScore = ', vTemplatePersonalizationScore
						,', OfferId = ', vOfferId
						,', Communication_Cooloff = ', vCommunication_Cooloff				
                        ,', Custom_Attr1 = "', vCustom_Attr1, '"'
                        ,', Custom_Attr2 = "', vCustom_Attr2, '"'
                        ,', Custom_Attr3 = "', vCustom_Attr3, '"'
                        ,', Custom_Attr4 = "', vCustom_Attr4, '"'
                        ,', Custom_Attr5 = "', vCustom_Attr5, '"'
                        ,', RankedPickListStory_Id = "', vRecommendation_Type, '"'
                        ,', Timeslot_Id = ',vTimeslot_Id
						,', CreatedBy = ',vUserId
						,', ModifiedBy = ',vUserId
					);

				SET vErrMsg = vSqlstmt;
                CALL CDM_Update_Process_Log('PROC_CLM_CU_CLM_Creative', 7, vErrMsg , vSuccess );
				SET @sql_stmt = vSqlstmt;
				PREPARE EXEC_STMT FROM @sql_stmt;
				EXECUTE EXEC_STMT;
				DEALLOCATE PREPARE EXEC_STMT;
				CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 4, vErrMsg, vSuccess);
			ELSE
				SET vErrMsg = 'The Creative already exists and has been used in Campaign execution and hence cannot be modified.';
				SET vSuccess = 0;
                CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 5, vErrMsg, vSuccess);
			END IF;
		END IF;
	END IF;
	
	SELECT CreativeId INTO vCreativeId 
	FROM CLM_Creative
	WHERE CreativeName = vCreativeName
	LIMIT 1;
 
	SET vSuccess = 1;
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 6, vErrMsg, vSuccess);

END


