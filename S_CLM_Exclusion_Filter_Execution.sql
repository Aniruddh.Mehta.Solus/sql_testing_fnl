
CREATE OR REPLACE PROCEDURE `S_CLM_Exclusion_Filter_Execution`(
IN Event_ID_cur1 BIGINT(20),
IN Channel_Name varchar(128), 
IN vStart_CntCust BIGINT(20),
IN vEnd_CntCust BIGINT(20)
)
begin
  DECLARE Exe_ID, Filter_ID_cur1, Filter_Channel_cur1 int;
  DECLARE Filter_Condition_cur1 text;
  DECLARE done1 INT DEFAULT 0;
  DECLARE cur1 CURSOR for 
		  SELECT `ID`,`Condition`,`Channel` from Exclusion_Filter 
		  WHERE `State`='Enabled' 
          AND `Channel` = Channel_Name
		  ORDER BY Execution_Sequence;
  
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done1 = TRUE;
  SET SQL_SAFE_UPDATES=0;
  SET foreign_key_checks=0;
  SET @Event_ID_cur1=Event_ID_cur1;
  INSERT into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
    values ('S_Exclusion_Filter_Execution', '',now(),null,'Started', 090921);
 
  SET Exe_ID = F_Get_Execution_Id();
  SET @UI_State=(select `Value` from M_Config where `Name`='UI_State');
  

  OPEN cur1;
    new_loop_1: LOOP
        FETCH cur1 into Filter_ID_cur1, Filter_Condition_cur1,Filter_ID_cur1 ;
        IF done1 THEN
           LEAVE new_loop_1;
        END IF;
		IF (UPPER(@UI_State)='ENABLED') THEN  
			SET @Expression_String_Value = Filter_Condition_cur1;  
		ELSE
			SET @Expression_String_Value = REPLACE(ifnull(json_extract(Filter_Condition_cur1, '$.expression'),0),'\"','');
		END IF;
		SET @sql1 = concat('
            UPDATE Event_Execution 
			SET Is_Target="C", 
            Exclusion_Filter_ID = ',(Filter_ID_cur1),' 
            WHERE Event_Execution.Customer_Id between ',vStart_CntCust,' and ',vEnd_CntCust,' 
				AND EXISTS 
                (
					SELECT 1 from  V_CLM_Customer_One_View 
					where V_CLM_Customer_One_View.Customer_Id=Event_Execution.Customer_Id
					AND V_CLM_Customer_One_View.Customer_Id between ',vStart_CntCust,' and ',vEnd_CntCust,' AND ',@Expression_String_Value,' 
                )
				AND  Event_Id = ',@Event_ID_cur1,';
			');	
		PREPARE stmt1 from @sql1;
		EXECUTE stmt1;
    end LOOP;
    close cur1;
	
    
	INSERT ignore into Event_Execution_Rejected SELECT * from Event_Execution where Is_Target = "C";
	DELETE from Event_Execution where Is_Target = "C";
	UPDATE Event_Execution_Rejected a,   Exclusion_Filter
	SET a.Message=CONCAT('Rejected by Exclusion Filter')
	WHERE a.Is_Target="C" ;

	update ETL_Execution_Details
	set Status ='Succeeded',
	End_Time = now( )
	where Procedure_Name='S_Exclusion_Filter_Execution'
	and Execution_ID = Exe_ID;

END

