
CREATE OR REPLACE PROCEDURE `S_UI_Test_Campaign`(IN in_request_json json, OUT out_result_json json)
BEGIN
    
    -- User Information

    declare IN_USER_NAME varchar(240); 
    declare IN_SCREEN varchar(400); 
    declare vSuccess  varchar(400); 
    declare vErrMsg  varchar(400);
    declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text; 
	declare IN_AGGREGATION_3 text;
    
    	

  -- --------------------- Decoding the JSON -----------------------------------------------------------------------------------------------------------------------------------------------
  
  
    Set @Err =0;
	
   
   -- User Information
   
    SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1")); 
    
    
    SELECT IN_AGGREGATION_1 as 'Event_Id';
    
    
    /*Setting the State Of Event to Test*/
    
    SET SQL_SAFE_UPDATES=0;
	UPDATE CLM_Campaign_Trigger A, CLM_Campaign_Events B
	SET  A.CTState =2 
	WHERE A.CampTriggerId= B.CampTriggerId
	 AND  B.EventId=IN_AGGREGATION_1;
		
    
  
    call S_CLM_Run ( IN_AGGREGATION_1,"TEST",@no_of_customers,@no_of_customers_final,@Filename,@FileHeader,@FileLocation,@RecoFilename,@RecoFileHeader,@RecoFileRecoHeader,@RecoFileLocation,@vSuccess,@vErrMsg ); 
    
    
    
    /*Marking The Event AS Tested*/
    Set SQL_SAFE_UPDATES=0;
	Update Event_Master_UI
	Set Status_Tested ="TESTED"
	Where Event_Id =IN_AGGREGATION_1;
    
    
    /*Changing the State from Test to Tested*/
	Set SQL_SAFE_UPDATES=0;
	Update CLM_Campaign_Trigger A, CLM_Campaign_Events B
	 Set  A.CTState =3 
	Where A.CampTriggerId= B.CampTriggerId
	 and  B.EventId=IN_AGGREGATION_1;
     
     
	 /*Capturring The Message In Event Master UI*/
	 SET SQL_SAFE_UPDATES = 0;
	Update Event_Master_UI
	set Message = (Select Message from Event_Execution_Current_Run where  message is not null and Event_Id=IN_AGGREGATION_1 limit 1),
	 Event_Execution_Date_ID = (Select Event_Execution_Date_ID from Event_Execution_Current_Run where   message is not null and Event_Id=IN_AGGREGATION_1 limit 1)
	where Event_Id=IN_AGGREGATION_1;

	SET SQL_SAFE_UPDATES = 0;
	Update Event_Master_UI
	set Message = (Select Creative1 from Event_Master where Id=IN_AGGREGATION_1)
	where Event_Id=IN_AGGREGATION_1 and Message='';


    
    
    IF @vErrMsg = ''
    THEN 
		SET @Msg = 'Sucess';
	ELSE
		SET @Msg = 'Failure';
        
	END IF;
    
    
    	Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","',@Msg,'") )),"]") into @temp_result                       
												       ;') ;
      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;
                            
    SET out_result_json = @temp_result;


END

