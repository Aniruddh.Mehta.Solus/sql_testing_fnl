
CREATE OR REPLACE PROCEDURE `PROC_CLM_Extract_All_Col_Name_From_Comm_Template`(
	IN vInStr TEXT,
	IN vInStartDelimiter VARCHAR(16),
	IN vInEndDelimiter VARCHAR(16),
    IN vInFieldDelimiter VARCHAR(16),
	OUT vOutput TEXT
)
BEGIN
	DECLARE vStartDelimiterLen INT;
	DECLARE vEndDelimiterLen INT;
	DECLARE vColStartPos INT;
	DECLARE vColEndPos INT;
	DECLARE vStartDelimiter VARCHAR(16);
	DECLARE vEndDelimiter VARCHAR(16);
    DECLARE vFieldDelimiter VARCHAR(16);
	DECLARE vTemp TEXT;
    DECLARE vDone INT DEFAULT FALSE; 

	SET vOutput = '';
	SET vColStartPos = 0;
	SET vColEndPos = 0;
	SET vStartDelimiter = TRIM(vInStartDelimiter);
	SET vEndDelimiter = TRIM(vInEndDelimiter);
    SET vFieldDelimiter = TRIM(vInFieldDelimiter);

	SET vColStartPos = 1;
	PROCESS_LOOP: LOOP
		IF vDone THEN
			 LEAVE PROCESS_LOOP;
		END IF;
        SET vTemp = '';
        CALL PROC_CLM_Col_Name_From_Comm_Template(vInStr,vInStartDelimiter,vInEndDelimiter,vColStartPos, vColEndPos, vTemp);
        
        
		IF LENGTH(vTemp) > 0 THEN
			IF LENGTH(vOutput) > 0 THEN
				SET vOutput = CONCAT(vOutput, vFieldDelimiter, vTemp);
			ELSE
				SET vOutput = vTemp;
			END IF;
		ELSE 
			SET vDone = TRUE;
		END IF;
		SET vColStartPos = vColEndPos;
	END LOOP PROCESS_LOOP;
END

