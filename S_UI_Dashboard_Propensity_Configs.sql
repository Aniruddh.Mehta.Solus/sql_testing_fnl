DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_UI_Dashboard_Propensity_Configs`(IN in_request_json json, OUT out_result_json json)
BEGIN
    

    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
    declare EDIT Bigint; 

	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare IN_AGGREGATION_4 text;
	declare IN_AGGREGATION_5 text;
    declare IN_NAME text;
    declare IN_NEXT_TRAIN_DATE TEXT;
    declare IN_NEXT_SCORE_DATE TEXT;
	
	SET SQL_SAFE_UPDATES=0;
	SET foreign_key_checks=0;

	Set in_request_json = Replace(in_request_json,'\n','\\n');
  
   
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1")); 
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_AGGREGATION_4 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_4"));
    SET IN_NAME = json_unquote(json_extract(in_request_json,"$.UPDATED_BAND_NAME"));
    SET IN_NEXT_TRAIN_DATE = json_unquote(json_extract(in_request_json,"$.TRAIN_DATE"));
    SET IN_NEXT_SCORE_DATE = json_unquote(json_extract(in_request_json,"$.SCORE_DATE"));
    
    
	Set @temp_result =Null;
    Set @vSuccess ='';
	Set @vErrMsg='';
	Set @vSuccess='';
	Set @Err =0;
	set @a1 = '[{"Val":"Values"}]';
    
    
    IF IN_AGGREGATION_1 ="MODEL_NAME"
    THEN
		Set @View_STM= Concat("CREATE or REPLACE VIEW `V_Dashboard_Propensity_Config` AS 
			select distinct(ModelName), ID from CLM_MCore_Model_Master
			;");
				
			SELECT @View_STM;
			PREPARE statement from @View_STM;
			Execute statement;
			Deallocate PREPARE statement; 
				
			Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object(
			"Name",`ModelName`,
			"ID",`ID`) )),"]")into @temp_result from V_Dashboard_Propensity_Config ;') ;
												  
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement; 
    END IF;
    
	IF IN_AGGREGATION_1 ="DISPLAY"
		THEN 
			Set @View_STM= Concat("CREATE or REPLACE VIEW `V_Dashboard_Propensity_Config` AS 
			select * from CLM_MCore_Model_Master where ID = ",IN_AGGREGATION_3,"
			;");
				
			SELECT @View_STM;
			PREPARE statement from @View_STM;
			Execute statement;
			Deallocate PREPARE statement; 
				
			Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object(
			"Name",`ModelName`,
			"Enabled",`Enabled`,
			"ResponseWindowDays",`ResponseWindowDays`,
			"ScoringFrequencyDays",`ScoringFrequencyDays`,
			"LastTrainingDate",`LastTrainingDate`,
			"LastScoringDate",`LastScoringDate`,
			"NextTrainingDate",`NextTrainingDate`,
			"NextScoringDate",`NextScoringDate`,
			"ID",`ID`) )),"]")into @temp_result from V_Dashboard_Propensity_Config ;') ;
												  
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement; 
								  
	END IF;
		
	IF IN_AGGREGATION_1 ="EDIT"
    THEN
		UPDATE CLM_MCore_Model_Master
        SET NextTrainingDate = IN_NEXT_TRAIN_DATE, 
        NextScoringDate = IN_NEXT_SCORE_DATE
        WHERE Id = IN_AGGREGATION_2;
        Set @View_STM= Concat("CREATE or REPLACE VIEW `V_Dashboard_Propensity_Config` AS 
			select * from CLM_MCore_Model_Master
			;");
				
			SELECT @View_STM;
			PREPARE statement from @View_STM;
			Execute statement;
			Deallocate PREPARE statement; 
				
			Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object(
			"Name",`ModelName`,
			"Enabled",`Enabled`,
			"ResponseWindowDays",`ResponseWindowDays`,
			"ScoringFrequencyDays",`ScoringFrequencyDays`,
			"LastTrainingDate",`LastTrainingDate`,
			"LastScoringDate",`LastScoringDate`,
			"NextTrainingDate",`NextTrainingDate`,
			"NextScoringDate",`NextScoringDate`,
			"ID",`ID`) )),"]")into @temp_result from V_Dashboard_Propensity_Config ;') ;
												  
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement; 
			
        
    END IF;
    
	If @temp_result is null
		Then
								
							Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Metrics execution never done for Customer Distribution) )),"]") into @temp_result                       
												  ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
						
	End if;
							

    SET out_result_json = @temp_result;                                          
  


END$$
DELIMITER ;
