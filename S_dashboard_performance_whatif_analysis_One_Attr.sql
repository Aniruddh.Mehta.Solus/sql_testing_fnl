DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_performance_whatif_analysis_One_Attr`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text;
	declare IN_FILTER_CHANNEL text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
    SET IN_FILTER_CHANNEL = json_unquote(json_extract(in_request_json,"$.FILTER_CHANNEL"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    
    SET @Interval_month = CASE WHEN IN_AGGREGATION_2='M' THEN 0
							WHEN IN_AGGREGATION_2='L3M' THEN 2
                            WHEN IN_AGGREGATION_2='L12M' THEN 11 END;
                            
    
    
	SET @FY_SATRT_DATE = date_format(date_sub(concat(IN_FILTER_CURRENTMONTH,'01'), INTERVAL @Interval_month MONTH),"%Y%m");
	SET @FY_END_DATE = IN_FILTER_CURRENTMONTH;
    
    SELECT @FY_SATRT_DATE,@FY_END_DATE;
    SET @filter_cond=concat(" YM between '",@FY_SATRT_DATE,"' and '",@FY_END_DATE,"'");
    
    IF IN_FILTER_CHANNEL = "ALL"
    THEN 
		SET IN_FILTER_CHANNEL = "";
	END IF;
    
    IF IN_FILTER_CHANNEL IS  NULL OR IN_FILTER_CHANNEL = ""
    THEN
		SET @filter_cond = @filter_cond;
    ELSE
		SET @filter_cond=concat(@filter_cond," AND chan = '",IN_FILTER_CHANNEL,"'");
    END IF;
    SELECT @filter_cond;
    
    SET @incr_Rev =' round(SUM(Incremental_Revenue),0) ';
    
    
      select Value1 into @Incremental_Revenue_Method from UI_Configuration_Global where Config_Name = 'Incremental_Revenue_Method';
    SET @Incremental_Revenue_Method=ifnull(@Incremental_Revenue_Method,'Method2');
    select @Incremental_Revenue_Method;
    IF @Incremental_Revenue_Method = 'Method1'
    THEN
		SET @incr_rev_concat='_By_Day';
        SET @group_cond1='Event_Id,EE_Date';
        SET @mtd_concat='';
	ELSEIF @Incremental_Revenue_Method = 'Method2'
    THEN
		SET @group_cond1='Event_Id,YM';
		SET @incr_rev_concat='';
        SET @mtd_concat='MTD_';
	ELSE
		SET @incr_rev_concat='';
        SET @mtd_concat='MTD_';
		SET @incr_Rev = ' round((CASE WHEN SUM(Control_Base) < 0 or SUM(Target_Delivered) = 0 then 0 WHEN (((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*100 < 0 then 0 WHEN SUM(Control_Base)>0 and SUM(Control_Responder)=0 then SUM(Target_Revenue)
					ELSE ifnull((((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*(SUM(Target_Revenue)/SUM(Target_Responder))*(SUM(Target_Delivered)),0) END),0) ';
		SET @group_cond1='YM';
	END IF;
    SELECT @mtd_concat;
    SELECT @incr_rev_concat;
    SELECT @incr_Rev;
    SET @sql1=CONCAT('
		CREATE OR REPLACE VIEW Dashboard_Performance_Campaign_Insights_STLT
        as 
		SELECT 
			`Trigger`,
			`EE_Date`,
			`Chan`,
			`Type`,
			`Size`,
			`Recommendation`,
			`Pers`,
			`Offer`,
			`DOW_num`,
			`DOW`,
			`WEEKDAY_WEEKEND`,
			`Segment`,
			`Event_ID`,
			`Event_Execution_Date_ID`,
			`YM`,
			`Target_Base`,
			`Target_Delivered`,
			`Target_Bills`,
			`Conversion_Per`,
			`Incremental_Revenue',@incr_rev_concat,'` AS `Incremental_Revenue`,
			`',@mtd_concat,'Incremental_Revenue',@incr_rev_concat,'` AS `MTD_Incremental_Revenue`,
            `',@mtd_concat,'Incremental_Revenue_By_Segment',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Segment`
		FROM `Dashboard_Performance_Campaign_Insights`;
        ');
        select @sql1;
		PREPARE statement from @sql1;
		Execute statement;
		Deallocate PREPARE statement;
        IF  IN_AGGREGATION_1 = 'SEGMENT'
		THEN 
        SET @create_view=CONCAT('CREATE OR REPLACE VIEW V_Dashboard_What_If_Incr_Rev
			as
			select `Trigger`,
            `Event_Execution_Date_ID`,
			`Type`,
			`Size`,
			`Recommendation`,
			`Pers`,
			`Offer`,
			`DOW_num`,
			`DOW`,
			`WEEKDAY_WEEKEND`,
			`Segment`,
			`Conversion_Per`,
            MAX(EE_Date) as EE_Date,
            MAX(MTD_Incremental_Revenue_By_Segment) AS Incremental_Revenue,
            SUM(Target_Delivered) as Target_Delivered,
            SUM(Target_Bills) as Target_Bills,
            SUM(Target_Base) as Target_Base,
            Event_ID,Chan,YM 
            from Dashboard_Performance_Campaign_Insights_STLT
            group by `Segment`,',@group_cond1,';');
    
     select @create_view;
	PREPARE statement from @create_view;
	Execute statement;
	Deallocate PREPARE statement;
        ELSE
		
         SET @create_view=CONCAT('CREATE OR REPLACE VIEW V_Dashboard_What_If_Incr_Rev
			as
			select `Trigger`,
            `Event_Execution_Date_ID`,
			`Type`,
			`Size`,
			`Recommendation`,
			`Pers`,
			`Offer`,
			`DOW_num`,
			`DOW`,
			`WEEKDAY_WEEKEND`,
			`Segment`,
			`Conversion_Per`,
            MAX(EE_Date) as EE_Date,
            MAX(MTD_Incremental_Revenue) as Incremental_Revenue,
            SUM(Target_Delivered) as Target_Delivered,
            SUM(Target_Bills) as Target_Bills,
            SUM(Target_Base) as Target_Base,
            Event_ID,Chan,YM 
            from Dashboard_Performance_Campaign_Insights_STLT
            group by ',@group_cond1,';');
    
     select @create_view;
	PREPARE statement from @create_view;
	Execute statement;
	Deallocate PREPARE statement;
   END IF;
    IF IN_AGGREGATION_1 = 'TYPE'
    THEN 
		SET @query_str = '`Type`';
        SET @order_by_str= '1';
	END IF;
    
    IF IN_AGGREGATION_1 = 'SIZE'
    THEN 
		SET @query_str = '`Size`';
        SET @order_by_str= '1';
	END IF;
    
    IF IN_AGGREGATION_1 = 'RECO'
    THEN 
		SET @query_str = '`Recommendation`';
        SET @order_by_str= '1';
	END IF;
    
    IF IN_AGGREGATION_1 = 'PERS'
    THEN 
		SET @query_str = '`Pers`';
        SET @order_by_str= '1';
	END IF;
    
    IF IN_AGGREGATION_1 = 'OFFER'
    THEN 
		SET @query_str = '`Offer`';
        SET @order_by_str= '1';
	END IF;
    
    IF IN_AGGREGATION_1 = 'DOW'
    THEN 
		SET @query_str = '`DOW`';
        SET @order_by_str= '`DOW_num`';
        
	END IF;
    
    IF IN_AGGREGATION_1 = 'SEGMENT'
    THEN 
		SET @query_str = '`Segment`';
        SET @order_by_str= '1';
	END IF;
    
	IF IN_MEASURE = 'TABLE' 
    THEN
		
       
        SET @view_stmt=CONCAT('CREATE OR REPLACE VIEW V_Dashboard_Campaign_Insights
			as
			WITH GROUP1 AS
							(
							select ',@query_str,',
            COUNT(DISTINCT Event_Execution_Date_ID) AS Instances,
            SUM(Target_Base) AS Outreaches,
            SUM(Target_Base)/COUNT(DISTINCT Event_Execution_Date_ID) AS `Outreaches/Instance`,
            MAX(Incremental_Revenue)/COUNT(DISTINCT Event_Execution_Date_ID) as `Lift/Instance`,
            SUM(Target_Bills)/SUM(Target_Base) AS `% Conv`,
            round((',@incr_Rev,'/SUM(Target_Delivered)),1) AS Yield 
            from V_Dashboard_What_If_Incr_Rev
			where ',@filter_cond,'
            group by 1
            ORDER BY ',@order_by_str,'
							)
							,
							GROUP2 AS
							(
							select 
							A.',@query_str,',
							(A.c1*100)/B.c2 as OutreachesPercent 
							from 
							(
							(
							SELECT ',@query_str,', SUM(Target_Base) as c1
							FROM Dashboard_Performance_Campaign_Insights
                            WHERE ',@filter_cond,'
							group by 1
							)A
							,
							(
							SELECT SUM(Target_Base) as c2
							from Dashboard_Performance_Campaign_Insights
                            WHERE ',@filter_cond,'
							)B
							)
							group by A.',@query_str,'
							)
							,
							GROUP3 AS
							(
							select 
							A.',@query_str,',
							(A.c1*100)/B.c2 as ConversionPercent 
							from 
							(
							(
							SELECT ',@query_str,', SUM(Target_Bills) as c1
							FROM Dashboard_Performance_Campaign_Insights
                            WHERE ',@filter_cond,'
							group by 1
							)A
							,
							(
							SELECT SUM(Target_Bills) as c2
							from Dashboard_Performance_Campaign_Insights
                            WHERE ',@filter_cond,'
							)B
							)
							group by A.',@query_str,'
							)
							SELECT A.',@query_str,', A.Instances, A.Outreaches, A.`Outreaches/Instance`, A.`Lift/Instance`,A.`% Conv`, A.Yield, OutreachesPercent, ConversionPercent
							FROM GROUP1 A
							JOIN GROUP2 B
							ON A.',@query_str,'=B.',@query_str,'
							JOIN GROUP3 C
							ON B.',@query_str,'=C.',@query_str,'
                            ;');
            
		SET @Query_String =  concat( 'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Type",',@query_str,',
        "Instances",`Instances`,"Outreaches",CAST(format(ifnull(`Outreaches`,0),",") AS CHAR),"Outreaches/Instance",CAST(format(ifnull(`Outreaches/Instance`,0),",") AS CHAR),"Lift/Instance",CAST(format(ifnull(`Lift/Instance`,0),",") AS CHAR),"Conv. %",concat(round(ifnull(`% Conv` * 100,0),2),"%"),"Yield",CAST(round(ifnull(`Yield`,0),2)as CHAR),"% Outreaches",concat(round(ifnull(`OutreachesPercent`,0),2),"%"),"Conv. Contr. %",concat(round(ifnull(`ConversionPercent`,0),2),"%")) )),"]")into @temp_result from V_Dashboard_Campaign_Insights',';') ;
            
		END IF;

	IF IN_MEASURE = 'GRAPH' AND IN_AGGREGATION_3 = 'TYPE1'
    THEN
	
		select @filter_cond;
		SET @view_stmt=CONCAT('CREATE OR REPLACE VIEW V_Dashboard_Campaign_Insights_Graph
			as
			WITH GROUP1 AS
							(
							select 
							A.',@query_str,',
                            A.',@order_by_str,',
							(A.c1*100)/B.c2 as OutreachesPercent 
							from 
							(
							(
							SELECT ',@query_str,',
                            ',@order_by_str,', 
                            SUM(Target_Base) as c1
							FROM Dashboard_Performance_Campaign_Insights
                            WHERE ',@filter_cond,'
							group by 1
							)A
							,
							(
							SELECT SUM(Target_Base) as c2
							from Dashboard_Performance_Campaign_Insights
                            WHERE ',@filter_cond,'
							)B
							)
							group by A.',@query_str,'
                            ORDER BY ',@order_by_str,'
							)
							,
							GROUP2 AS
							(
							select 
							A.',@query_str,',
							(A.c1*100)/B.c2 as ConversionPercent 
							from 
							(
							(
							SELECT ',@query_str,', SUM(Target_Bills) as c1
							FROM Dashboard_Performance_Campaign_Insights
                            WHERE ',@filter_cond,'
							group by 1
							)A
							,
							(
							SELECT SUM(Target_Bills) as c2
							from Dashboard_Performance_Campaign_Insights
                            WHERE ',@filter_cond,'
							)B
							)
							group by A.',@query_str,'
							)
							SELECT A.',@query_str,', 
                            round(ifnull(`OutreachesPercent`,0),2) AS OutreachesPercent,
                            round(ifnull(`ConversionPercent`,0),2) AS ConversionPercent
							FROM GROUP1 A
							JOIN GROUP2 B
							ON A.',@query_str,'=B.',@query_str,';');
		SET @Query_String =  CONCAT('SELECT CONCAT("[",(GROUP_CONCAT(json_object("displayName",',@query_str,',
        "value1",`OutreachesPercent`,"value2",`ConversionPercent`) )),"]")into @temp_result from V_Dashboard_Campaign_Insights_Graph',';') ;
        
	END IF;
    
    IF IN_MEASURE = 'GRAPH' AND IN_AGGREGATION_3 = 'TYPE2'
    THEN
	
		SET @view_stmt=CONCAT('CREATE OR REPLACE VIEW V_Dashboard_Campaign_Insights_Graph
			as
			select ',@query_str,',
            SUM(Target_Bills)/SUM(Target_Base) AS `% Conv`,
            round((',@incr_Rev,'/SUM(Target_Delivered)),1) AS Yield 
            from V_Dashboard_What_If_Incr_Rev
			where ',@filter_cond,'
            group by 1
            ORDER BY ',@order_by_str,'');
            
		SET @Query_String =  CONCAT('SELECT CONCAT("[",(GROUP_CONCAT(json_object("displayName",',@query_str,',"value1",round(ifnull(`% Conv` * 100,0),2),"value2",`Yield`) )),"]")into @temp_result from V_Dashboard_Campaign_Insights_Graph',';') ;
        
	END IF;
	
	
	IF IN_MEASURE <> "DOWNLOAD"
    THEN
	select @view_stmt;
	PREPARE statement from @view_stmt;
	Execute statement;
	Deallocate PREPARE statement;
    
	select @Query_String;
	PREPARE statement from @Query_String;
	Execute statement;
	Deallocate PREPARE statement; 
    
    
    
    SET out_result_json = @temp_result;
    END IF;
    
END$$
DELIMITER ;
