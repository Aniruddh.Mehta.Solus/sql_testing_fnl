DELIMITER $$
CREATE OR REPLACE PROCEDURE  S_MERGE_LOOPING(IN vStart bigint(20) ,IN vEnd bigint(20) ,  IN vInsertStmt varchar(512), IN vBatchSize bigint)
BEGIN
DECLARE vStart_Cnt BIGINT;
DECLARE vEnd_Cnt BIGINT;
set vStart_Cnt=vStart;
set vEnd_Cnt=vEnd;
set SQL_SAFE_UPDATES=0;
PROCESS_LOOP: LOOP
SET vEnd_Cnt = vStart_Cnt + vBatchSize;
select concat( vInsertStmt , vStart_Cnt ,' and ', vEnd_Cnt ) into @vstmt;
prepare stmt1 from @vstmt;
execute stmt1;
deallocate prepare stmt1; 
SET vStart_Cnt = vEnd_Cnt;

IF vStart_Cnt  >= vEnd THEN
	LEAVE PROCESS_LOOP;
END IF;        
END LOOP PROCESS_LOOP; 
END$$
DELIMITER ;
