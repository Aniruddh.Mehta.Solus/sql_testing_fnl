DELIMITER $$
CREATE or REPLACE PROCEDURE `CDM_Calc_Hist_Monthly_CRM_KPIs`(IN vCalendarType TINYINT, IN vActive_Store_LOV_Id BIGINT)
BEGIN
	
    DECLARE vKPI_Year SMALLINT;
	DECLARE vKPI_Curr_Year SMALLINT;
	DECLARE vKPI_Min_Date DATE;
	
	SELECT Bill_Date INTO vKPI_Min_Date FROM CDM_Bill_Header ORDER BY Bill_Date ASC LIMIT 1;
  
    IF vKPI_Min_Date IS NOT NULL THEN 
		SET vKPI_Year = EXTRACT(YEAR FROM vKPI_Min_Date); 		
		SET vKPI_Curr_Year = EXTRACT(YEAR FROM CURDATE()); 		
		SET vKPI_Year = vKPI_Year - 1; 

		CALL CDM_Update_Process_Log('CDM_Calc_Hist_Monthly_CRM_KPIs', 1, 'CDM_Calc_Hist_Monthly_CRM_KPIs', 1);
		
		PROCESS_HIST_REC: LOOP
			CALL CDM_Calculate_CRM_KPIs(vKPI_Year, 1, vCalendarType,vActive_Store_LOV_Id);		
			CALL CDM_Calculate_CRM_KPIs(vKPI_Year, 2, vCalendarType,vActive_Store_LOV_Id);		
			CALL CDM_Calculate_CRM_KPIs(vKPI_Year, 3, vCalendarType,vActive_Store_LOV_Id);		
			CALL CDM_Calculate_CRM_KPIs(vKPI_Year, 4, vCalendarType,vActive_Store_LOV_Id);		
			CALL CDM_Calculate_CRM_KPIs(vKPI_Year, 5, vCalendarType,vActive_Store_LOV_Id);		
			CALL CDM_Calculate_CRM_KPIs(vKPI_Year, 6, vCalendarType,vActive_Store_LOV_Id);		
			CALL CDM_Calculate_CRM_KPIs(vKPI_Year, 7, vCalendarType,vActive_Store_LOV_Id);		
			CALL CDM_Calculate_CRM_KPIs(vKPI_Year, 8, vCalendarType,vActive_Store_LOV_Id);		
			CALL CDM_Calculate_CRM_KPIs(vKPI_Year, 9, vCalendarType,vActive_Store_LOV_Id);		
			CALL CDM_Calculate_CRM_KPIs(vKPI_Year, 10, vCalendarType,vActive_Store_LOV_Id);		
			CALL CDM_Calculate_CRM_KPIs(vKPI_Year, 11, vCalendarType,vActive_Store_LOV_Id);		
			CALL CDM_Calculate_CRM_KPIs(vKPI_Year, 12, vCalendarType,vActive_Store_LOV_Id);		
		
			SET vKPI_Year = vKPI_Year + 1;
			IF vKPI_Year  > vKPI_Curr_Year THEN
				LEAVE PROCESS_HIST_REC;
			END IF;
		
		END LOOP PROCESS_HIST_REC;
		CALL CDM_Update_Process_Log('CDM_Calc_Hist_Monthly_CRM_KPIs', 2, 'CDM_Calc_Hist_Monthly_CRM_KPIs', 1);

	END IF;
	CALL CDM_Update_Process_Log('CDM_Calc_Hist_Monthly_CRM_KPIs', 3, 'CDM_Calc_Hist_Monthly_CRM_KPIs', 1);
	
END$$
DELIMITER ;
