DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_performance_lift_detail`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    select Value1 into @Incremental_Revenue_Method from UI_Configuration_Global where Config_Name = 'Incremental_Revenue_Method';
    SET @Incremental_Revenue_Method=ifnull(@Incremental_Revenue_Method,'Method2');
    select @Incremental_Revenue_Method;
    IF @Incremental_Revenue_Method = 'Method1'
    THEN
		SET @incr_rev_concat='_By_Day';
        SET @group_cond='EE_Date';
        SET @mtd_concat='';
	ELSEIF @Incremental_Revenue_Method = 'Method2'
    THEN
		SET @group_cond='YM';
		SET @incr_rev_concat='';
        SET @mtd_concat='MTD_';
	ELSE
		SET @incr_rev_concat='';
        SET @mtd_concat='';
		SET @incr_Rev = ' round((CASE WHEN SUM(Control_Base) < 0 or SUM(Target_Delivered) = 0 then 0 WHEN (((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*100 < 0 then 0 WHEN SUM(Control_Base)>0 and SUM(Control_Responder)=0 then SUM(Target_Revenue)
					ELSE ifnull((((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*(SUM(Target_Revenue)/SUM(Target_Responder))*(SUM(Target_Delivered)),0) END),0) ';
		SET @group_cond='YM';
	END IF;
    SELECT @mtd_concat;
    IF IN_AGGREGATION_2 = 'ST'
    THEN
    SET @sql1=CONCAT('
		CREATE OR REPLACE VIEW CLM_Event_Dashboard_STLT
        as 
		SELECT `CLM_Event_Dashboard`.`Revenue Center`,
			`CLM_Event_Dashboard`.`Segment`,
			`CLM_Event_Dashboard`.`Campaign`,
			`CLM_Event_Dashboard`.`EE_Date`,
			`CLM_Event_Dashboard`.`Event_Execution_Date_ID`,
			`CLM_Event_Dashboard`.`Theme`,
			`CLM_Event_Dashboard`.`Offer`,
			`CLM_Event_Dashboard`.`Channel`,
			`CLM_Event_Dashboard`.`Has_Recommendations`,
			`CLM_Event_Dashboard`.`Event_ID`,
			`CLM_Event_Dashboard`.`Control_Base_Event` as `Control_Base`,
			`CLM_Event_Dashboard`.`Target_Base`,
			`CLM_Event_Dashboard`.`Target_Delivered`,
			`CLM_Event_Dashboard`.`Target_Delivered_NA`,
			`CLM_Event_Dashboard`.`Control_Responder_Event` as `Control_Responder`,
			`CLM_Event_Dashboard`.`Target_Responder`,
			`CLM_Event_Dashboard`.`Target_Revenue`,
			`CLM_Event_Dashboard`.`Target_Discount`,
			`CLM_Event_Dashboard`.`Target_Bills`,
			`CLM_Event_Dashboard`.`Target_Open`,
			`CLM_Event_Dashboard`.`Target_CTA`,
			`CLM_Event_Dashboard`.`Control_Revenue`,
			`CLM_Event_Dashboard`.`Control_Discount`,
			`CLM_Event_Dashboard`.`Control_Bills`,
			`CLM_Event_Dashboard`.`Created_Date`,
			`CLM_Event_Dashboard`.`Total_Customer_Base`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Day`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Month`,
			`CLM_Event_Dashboard`.`YM`,
			`CLM_Event_Dashboard`.`Incremental_Revenue_ST',@incr_rev_concat,'` AS `Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_ST',@incr_rev_concat,'` AS `MTD_Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Segment_ST',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Segment`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Revenue_Center_ST',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Revenue_Center`
		FROM `CLM_Event_Dashboard`;
        ');
        select @sql1;
		PREPARE statement from @sql1;
		Execute statement;
		Deallocate PREPARE statement;
    ELSE
		SET @sql1=CONCAT('
		CREATE OR REPLACE VIEW CLM_Event_Dashboard_STLT
        as 
		SELECT `CLM_Event_Dashboard`.`Revenue Center`,
			`CLM_Event_Dashboard`.`Segment`,
			`CLM_Event_Dashboard`.`Campaign`,
			`CLM_Event_Dashboard`.`EE_Date`,
			`CLM_Event_Dashboard`.`Event_Execution_Date_ID`,
			`CLM_Event_Dashboard`.`Theme`,
			`CLM_Event_Dashboard`.`Offer`,
			`CLM_Event_Dashboard`.`Channel`,
			`CLM_Event_Dashboard`.`Has_Recommendations`,
			`CLM_Event_Dashboard`.`Event_ID`,
			`CLM_Event_Dashboard`.`Control_Base_Event` as `Control_Base`,
			`CLM_Event_Dashboard`.`Target_Base`,
			`CLM_Event_Dashboard`.`Target_Delivered`,
			`CLM_Event_Dashboard`.`Target_Delivered_NA`,
			`CLM_Event_Dashboard`.`Control_Responder_Event` as `Control_Responder`,
			`CLM_Event_Dashboard`.`Target_Responder`,
			`CLM_Event_Dashboard`.`Target_Revenue`,
			`CLM_Event_Dashboard`.`Target_Discount`,
			`CLM_Event_Dashboard`.`Target_Bills`,
			`CLM_Event_Dashboard`.`Target_Open`,
			`CLM_Event_Dashboard`.`Target_CTA`,
			`CLM_Event_Dashboard`.`Control_Revenue`,
			`CLM_Event_Dashboard`.`Control_Discount`,
			`CLM_Event_Dashboard`.`Control_Bills`,
			`CLM_Event_Dashboard`.`Created_Date`,
			`CLM_Event_Dashboard`.`Total_Customer_Base`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Day`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Month`,
			`CLM_Event_Dashboard`.`YM`,
			`CLM_Event_Dashboard`.`Incremental_Revenue',@incr_rev_concat,'` AS `Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue',@incr_rev_concat,'` AS `MTD_Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Segment',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Segment`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Revenue_Center',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Revenue_Center`
		FROM `CLM_Event_Dashboard`;
        ');
        select @sql1;
		PREPARE statement from @sql1;
		Execute statement;
		Deallocate PREPARE statement;
    END IF;
    CREATE OR REPLACE VIEW V_Dashboard_CLM_Event_Month_Report_temp AS
	   select CASE WHEN datediff(End_Date_ID,Start_Date_ID) < 32 THEN 'GTM' else 'CLM' END AS `Category`,b.Name as `Trigger`, b.CampaignName as `CampaignName`, a.* from CLM_Event_Dashboard_STLT a left join Event_Master b on a.Event_ID = b.ID;
    IF IN_AGGREGATION_1 not in ('Segment','REVCENTER')
    THEN       
       SET @create_view=CONCAT('CREATE OR REPLACE VIEW V_Dashboard_CLM_Event_Month_Report
		AS
		select MAX(MTD_Incremental_Revenue) as Incremental_Revenue,SUM(Target_Revenue) as Target_Revenue,SUM(Target_Base) as Target_Base,SUM(Control_Base) as Control_Base,CASE WHEN (SUM(Target_Delivered) = SUM(Target_Delivered_NA)) then 0 ELSE SUM(Target_Delivered) END AS Target_Delivered,SUM(Target_Responder) as Target_Responder,SUM(Control_Responder) AS Control_Responder,YM,EE_Date,Event_Id,Category,Channel,Theme from V_Dashboard_CLM_Event_Month_Report_temp
        group by ',@group_cond,',Event_Id');
    ELSEIF IN_AGGREGATION_1 = 'Segment'
    THEN
		SET @create_view=CONCAT('CREATE OR REPLACE VIEW V_Dashboard_CLM_Event_Month_Report
		AS
		select MAX(MTD_Incremental_Revenue_By_Segment) as Incremental_Revenue,SUM(Target_Revenue) as Target_Revenue,SUM(Target_Base) as Target_Base,SUM(Control_Base) as Control_Base,CASE WHEN (SUM(Target_Delivered) = SUM(Target_Delivered_NA)) then 0 ELSE SUM(Target_Delivered) END AS Target_Delivered,SUM(Target_Responder) as Target_Responder,SUM(Control_Responder) AS Control_Responder,YM,EE_Date,Event_Id,Category,Channel,Theme,Segment from V_Dashboard_CLM_Event_Month_Report_temp
        group by ',@group_cond,',Event_Id,Segment');
	ELSEIF IN_AGGREGATION_1 = 'REVCENTER'
    THEN
		SET @create_view=CONCAT('CREATE OR REPLACE VIEW V_Dashboard_CLM_Event_Month_Report
		AS
		select MAX(MTD_Incremental_Revenue_By_Revenue_Center) as Incremental_Revenue,SUM(Target_Revenue) as Target_Revenue,SUM(Target_Base) as Target_Base,SUM(Control_Base) as Control_Base,CASE WHEN (SUM(Target_Delivered) = SUM(Target_Delivered_NA)) then 0 ELSE SUM(Target_Delivered) END AS Target_Delivered,SUM(Target_Responder) as Target_Responder,SUM(Control_Responder) AS Control_Responder,YM,EE_Date,Event_Id,Category,Channel,Theme,Segment,`Revenue Center` as "Revcenter" from V_Dashboard_CLM_Event_Month_Report_temp
        group by ',@group_cond,',Event_Id,`Revenue Center`');
	ELSE
		SET @create_view=CONCAT('CREATE OR REPLACE VIEW V_Dashboard_CLM_Event_Month_Report AS
		select CASE WHEN datediff(End_Date_ID,Start_Date_ID) < 32 THEN "GTM" else "CLM" END AS `Category`,b.Name as `Trigger`, b.CampaignName as `CampaignName`, a.* from CLM_Event_Dashboard_STLT a left join Event_Master b on a.Event_ID = b.ID');
    END IF;
    select @create_view;
	PREPARE statement from @create_view;
	Execute statement;
	Deallocate PREPARE statement;
	SET @date_select = ' date_format(concat(YM,"01"),"%b-%y") ';
    IF IN_NUMBER_FORMAT = 'NUMBER'
    THEN
		SET @pct_concat='';
        SET @formating='format(Value,",")';
		SET @Query_Measure='round(SUM(Incremental_Revenue),0)';
	ELSE
		SET @pct_concat='%';
        SET @formating='Value';
		SET @Query_Measure='round((SUM(Incremental_Revenue)/(select SUM(Incremental_Revenue) from V_Dashboard_CLM_Event_Month_Report b
				where b.YM=a.YM))*100,1)';
    END IF;
	IF IN_AGGREGATION_1 = 'Overall'
    THEN
		SET @select_query='"Incremental Revenue"';
        SET @intrtval= '6';
	END IF;
	IF IN_AGGREGATION_1 = 'Category'
	THEN 
		SET @select_query='Category';
        SET @intrtval= '6';
        SET @attr = 'Category';
   END IF;
   IF IN_AGGREGATION_1 = 'Theme'
   THEN 
		SET @select_query='Theme';
        SET @intrtval= '0';
        SET @attr = 'Theme';
   END IF;
    IF IN_AGGREGATION_1 = 'Segment'
   THEN 
		SET @select_query='Segment';
        SET @intrtval= '0';
        SET @attr = 'Theme';
   END IF;
    IF IN_AGGREGATION_1 = 'Channel'
   THEN 
		SET @select_query='Channel';
        SET @intrtval= '6';
        SET @attr = 'Theme';
   END IF;
   IF IN_AGGREGATION_1 = 'REVCENTER'
   THEN 
		SET @select_query='`RevCenter`';
        SET @intrtval= '0';
        SET @attr = 'Theme';
   END IF;
	IF IN_MEASURE = 'GRAPH'
	THEN
		IF IN_AGGREGATION_1 = 'Overall'
        THEN
			SET @view_stmt=concat('create or replace view V_Dashboard_liftdetail_drill as
				select date_format(concat(YM,"01"),"%b-%y") as X_Data,',@Query_Measure,' as `Value`
				from V_Dashboard_CLM_Event_Month_Report a
				where YM between date_format(date_sub(',IN_FILTER_CURRENTMONTH,'01,interval ',@intrtval,' month),"%Y%m") and ',IN_FILTER_CURRENTMONTH,'
				group by YM
				order by YM');
			SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`X_Data`,"Value",`Value`) )),"]")into @temp_result from V_Dashboard_liftdetail_drill;') ;
        ELSEIF IN_AGGREGATION_1 in ('Theme','Segment','Revcenter')
        THEN
			SET @view_stmt=concat('create or replace view V_Dashboard_liftdetail_drill as
				select ',IN_AGGREGATION_1,' as "displayName",ifnull(round((SUM(Incremental_Revenue)/(select SUM(Incremental_Revenue) from V_Dashboard_CLM_Event_Month_Report b where b.YM=a.YM ))*100,1),0) as value1,round(SUM(Incremental_Revenue),0) as value2
                    from V_Dashboard_CLM_Event_Month_Report a
				where YM between date_format(date_sub(',IN_FILTER_CURRENTMONTH,'01,interval ',@intrtval,' month),"%Y%m") and ',IN_FILTER_CURRENTMONTH,'
				group by 1
				order by value2 DESC
                ');
			SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("displayName",`displayName`,"value1",`value1`,"value2",`value2`) )),"]")into @temp_result from V_Dashboard_liftdetail_drill where value2>0;') ;
        ELSEIF IN_AGGREGATION_1 = 'Channel'
        THEN
			SET @stmt = concat('CREATE OR REPLACE VIEW V_Dashboard_Lift_detail
						AS
						select ',@select_query,' as G1,',@date_select,',YM,',@Query_Measure,' as "Value"
						from V_Dashboard_CLM_Event_Month_Report a
						where YM between date_format(date_sub(',IN_FILTER_CURRENTMONTH,'01,interval ',@intrtval,' month),"%Y%m") and ',IN_FILTER_CURRENTMONTH,'
						group by 1,YM
						'); 
			select @stmt;
			PREPARE statement from @stmt;
			Execute statement;
			Deallocate PREPARE statement;
			
			SET @ordering='YM';
			SET @top_query = '';
			select group_concat(DISTINCT G1 order by `Value` DESC) into @distinct_rc from V_Dashboard_Lift_detail where `Value`>0 or G1 = 'NA';
            IF @distinct_rc like '%SMS%'
            THEN
				SET @distinct_rc=@distinct_rc;
            ELSE
				SET @distinct_rc=concat(@distinct_rc,',SMS');
            END IF;
            -- select group_concat(distinct CLMSegmentName) into @distinct_rc from CLM_Segment;
			select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
			set @loop_var=1;
			set @json_select='';
			select @loop_var,@num_of_rc,@distinct_rc;
			WHILE @loop_var<=@num_of_rc
			do
			SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
			set @top_query=concat(@top_query,",MAX(CASE WHEN `G1` = '",@selected_rc,"' Then pct else 0 END) AS `",@selected_rc,"`");
				set @json_select=concat(@json_select,',"',@selected_rc,'",`',@selected_rc,'`');
				set @loop_var=@loop_var+1;
			end while;
			
			SET @view_stmt=concat('create or replace view V_Dashboard_liftdetail_drill as
			select `Year`',@top_query,' from 
			(select max(',@ordering,') as YM,',@date_select,' as `Year`,`G1`, (Value) pct from V_Dashboard_Lift_detail a
			group by 2,`G1`
			order by `Value` DESC
			)C
			group by `Year`
			order by YM');
			
			SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`Year`',@json_select,') )),"]")into @temp_result from V_Dashboard_liftdetail_drill;') ;
		ELSE
			SET @stmt = concat('CREATE OR REPLACE VIEW V_Dashboard_Lift_detail
						AS
						select ',@select_query,' as G1,',@date_select,',YM,',@Query_Measure,' as "Value"
						from V_Dashboard_CLM_Event_Month_Report a
						where YM between date_format(date_sub(',IN_FILTER_CURRENTMONTH,'01,interval ',@intrtval,' month),"%Y%m") and ',IN_FILTER_CURRENTMONTH,'
						group by 1,YM
						'); 
			select @stmt;
			PREPARE statement from @stmt;
			Execute statement;
			Deallocate PREPARE statement;
			
			SET @ordering='YM';
			SET @top_query = '';
			select group_concat(DISTINCT G1 order by `Value` DESC) into @distinct_rc from V_Dashboard_Lift_detail where `Value`>0 or G1 = 'NA';
            SET @distinct_rc='CLM,GTM';
			select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
			set @loop_var=1;
			set @json_select='';
			select @loop_var,@num_of_rc,@distinct_rc;
			WHILE @loop_var<=@num_of_rc
			do
			SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
			set @top_query=concat(@top_query,",MAX(CASE WHEN `G1` = '",@selected_rc,"' Then pct else 0 END) AS `",@selected_rc,"`");
				set @json_select=concat(@json_select,',"',@selected_rc,'",`',@selected_rc,'`');
				set @loop_var=@loop_var+1;
			end while;
			
			SET @view_stmt=concat('create or replace view V_Dashboard_liftdetail_drill as
			select `Year`',@top_query,' from 
			(select max(',@ordering,') as YM,',@date_select,' as `Year`,`G1`, (Value) pct from V_Dashboard_Lift_detail a
			group by 2,`G1`
			order by `Value` DESC
			)C
			group by `Year`
			order by YM');
			
			SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`Year`',@json_select,') )),"]")into @temp_result from V_Dashboard_liftdetail_drill;') ;
		END IF;
	END IF;
   
	IF IN_MEASURE = 'TABLE'
	THEN
		SET @stmt = concat('CREATE OR REPLACE VIEW V_Dashboard_Lift_detail_table
					AS
                    select ',@select_query,' as G1,',@date_select,' as `Year`,YM,',@Query_Measure,' as "Value"
                    from V_Dashboard_CLM_Event_Month_Report
                    where YM between date_format(date_sub(',IN_FILTER_CURRENTMONTH,'01,interval 12 month),"%Y%m") and ',IN_FILTER_CURRENTMONTH,'
                    group by 1,YM
                    order by YM
                    '); 
		select @stmt;
		PREPARE statement from @stmt;
		Execute statement;
		Deallocate PREPARE statement;
        SET @ordering='YM';
        SET @top_query = '';
        select group_concat(DISTINCT `Year` order by YM) into @distinct_rc from V_Dashboard_Lift_detail_table;
        select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
		set @loop_var=1;
		set @json_select='';
		select @loop_var,@num_of_rc,@distinct_rc;
		WHILE @loop_var<=@num_of_rc
		do
        SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
		set @top_query=concat(@top_query,",MAX(CASE WHEN `Year` = '",@selected_rc,"' Then pct else 0 END) AS `",@selected_rc,"`");
			set @json_select=concat(@json_select,',"',@selected_rc,'",`',@selected_rc,'`');
			set @loop_var=@loop_var+1;
		end while;
        
		SET @view_stmt=concat('create or replace view V_Dashboard_liftdetail_drill_table as
		select `G1`',@top_query,' from 
		(select max(',@ordering,') as YM,`Year`,`G1`, concat(',@formating,',"',@pct_concat,'") pct from V_Dashboard_Lift_detail_table a
		group by 2,`G1`
        having pct >= 0)C
		group by `G1`
        order by YM');
        
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("",CAST(`G1` AS CHAR)',@json_select,') )),"]")into @temp_result from V_Dashboard_liftdetail_drill_table;') ;
	
   END IF;
  
	select @view_stmt;
	PREPARE statement from @view_stmt;
	Execute statement;
	Deallocate PREPARE statement;
    
    IF IN_SCREEN <> "DOWNLOAD"
    THEN
		select @Query_String;
		PREPARE statement from @Query_String;
		Execute statement;
		Deallocate PREPARE statement; 
		SET out_result_json = @temp_result;
	ELSE
		call S_dashboard_performance_download('V_Dashboard_liftdetail_drill_table',@out_result_json1);
		select @out_result_json1 into out_result_json;
    END IF;
    
END$$
DELIMITER ;
