
CREATE OR REPLACE FUNCTION `F_Valid_Customer_Name`(vName varchar(128), vNameSize tinyint, vName_Default varchar(128), is_special_char_check char(64)) RETURNS varchar(128) CHARSET latin1
BEGIN     
DECLARE vName_Ret VARCHAR(128);   
DECLARE vName_Str VARCHAR(128);  
DECLARE isregexsucc int;   
DECLARE CONTINUE HANDLER FOR SQLEXCEPTION RETURN 'Customer';  
SET vName_Str = TRIM(ifnull(vName,'Customer'));  
select replace(vName_Str,' ','a') regexp ifnull(is_special_char_check,'^[a-z]*$') into isregexsucc;
IF LENGTH(vName_Str) >= ifnull(vNameSize,3) AND isregexsucc='1'
THEN
	 SET vName_Ret = vName_Str;
ELSE 
	 SET vName_Ret = ifnull(vName_Default,'Customer');
END IF;

RETURN vName_Ret;
END

