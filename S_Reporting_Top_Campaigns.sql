DELIMITER $$
CREATE or REPLACE PROCEDURE `S_Reporting_Top_Campaigns`(IN v_Start_Date DATE, IN v_End_Date DATE)
begin

    DECLARE Load_Exe_ID, Exe_ID int;
    
    set Load_Exe_ID = F_Get_Load_Execution_Id();
    
    
set sql_safe_updates =0;
set foreign_key_checks = 0; 
    
	insert into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
    values ('S_Reporting_Top_Campaigns', '',now(),null,'Started', Load_Exe_ID);
    
    set Exe_ID = F_Get_Execution_Id();


SET @Startdate =  v_Start_Date ;
SET @Enddate   =  v_End_Date   ;



INSERT IGNORE INTO `SOLUS_Campaign_Scores`
(Year_Month_Num, Month_Name , `Mission`, `Event` )

SELECT  
EXTRACT(YEAR_MONTH FROM @Startdate), 
CONCAT(SUBSTRING(MONTHNAME(@Startdate),1,3),'-',SUBSTRING(EXTRACT(YEAR_MONTH FROM @Startdate),3,2)),
B.`Name` as Mission ,A.`Name` as Evnt
FROM Event_Master A
INNER JOIN Mission_Master B ON A.Mission_ID = B.ID
GROUP BY Mission, Evnt
ORDER BY Mission, Evnt;







UPDATE SOLUS_Campaign_Scores A, 
(
SELECT   `Event Name` as `Event`, 
		  COUNT(DISTINCT `Event Execution Date`) as Run_Instances, 
          SUM(`Base (Target)`) as Base_Targeted,
          ROUND(SUM(`Base (Target)`)/COUNT(DISTINCT `Event Execution Date`)) as Avg_Tgbase_Per_Instance,
          DENSE_RANK() OVER(ORDER BY Avg_Tgbase_Per_Instance DESC) as Rank_Reach
FROM     CLM_BI_Suite_Raw_Data_EventPer 
WHERE    `Event Execution Date` BETWEEN @Startdate AND @Enddate
GROUP BY `Event Name` 
ORDER BY Rank_Reach ASC
) B

SET A.Number_Of_Instances 	=   B.Run_Instances
   ,A.Base_Target 			=   B.Base_Targeted
   ,A.Target_Per_Instance 	=   B.Avg_Tgbase_Per_Instance
   ,A.Rank_Reach 			=   B.Rank_Reach
WHERE A.`Event` = B.`Event`
AND  Year_Month_Num = (EXTRACT(YEAR_MONTH FROM @Startdate));









UPDATE SOLUS_Campaign_Scores A, 
(
SELECT   `Event Name` as `Event`, 
		  SUM(Delivered) as Delivered, 
          SUM(`Responder (Target)`) as Responded,
          ROUND((SUM(`Responder (Target)`)/SUM(Delivered))*100,1) as Avg_Response_Per_Instance,
          DENSE_RANK() OVER(ORDER BY Avg_Response_Per_Instance DESC) as Rank_Response
FROM     CLM_BI_Suite_Raw_Data_EventPer 
WHERE    `Event Execution Date` BETWEEN @Startdate AND @Enddate
GROUP BY `Event Name` 
ORDER BY Rank_Response ASC
) B
SET  A.Delivered		= B.Delivered
	,A.Responder		= B.Responded
    ,A.Perc_Response	= B.Avg_Response_Per_Instance
    ,A.Rank_Response	= B.Rank_Response
WHERE A.`Event` = B.`Event`
AND  Year_Month_Num = (EXTRACT(YEAR_MONTH FROM @Startdate));








UPDATE SOLUS_Campaign_Scores Y, 
(
SELECT `Event Name` as `Event`, 
		IFNULL(B.Inc_Resp,0) as Occassion_Inc_Resp, 
		COUNT(DISTINCT `Event Execution Date`) as Run_Instances, 
        ROUND(((IFNULL(B.Inc_Resp,0))/COUNT(DISTINCT `Event Execution Date`))*100,1) as Avg_IncResp,
		DENSE_RANK() OVER(ORDER BY Avg_IncResp DESC) as Rank_Inc_Resp
FROM CLM_BI_Suite_Raw_Data_EventPer A
LEFT JOIN	(
				SELECT   `Event Name` as `Event`, 
						  COUNT(1) as Inc_Resp 
				FROM     CLM_BI_Suite_Raw_Data_EventPer 
                WHERE `Incremental Response (G) %` > 0
                AND (`Event Execution Date` BETWEEN @Startdate AND @Enddate)
                GROUP BY `Event Name`
			) B
ON A.`Event Name` = B.`Event`
WHERE    A.`Event Execution Date` BETWEEN @Startdate AND @Enddate
GROUP BY `Event Name`
) Z
SET  Y.Inc_Response_Instances		= Z.Occassion_Inc_Resp
	,Y.Perc_Inc_Response_Instances	= Z.Avg_IncResp
    ,Y.Rank_Inc_Response_Instances	= Z.Rank_Inc_Resp
WHERE Y.`Event` = Z.`Event`
AND  Year_Month_Num = (EXTRACT(YEAR_MONTH FROM @Startdate));





UPDATE SOLUS_Campaign_Scores A, 
(
SELECT   `Event Name` as `Event`, 
		  SUM(`Incremental Revenue (G)`) as Inc_Rev,
          SUM(Delivered) as Delivered, 
          ROUND((SUM(`Incremental Revenue (G)`)  / SUM(Delivered)),1) as Inc_Rev_Per_Delivery,
          DENSE_RANK() OVER(ORDER BY Inc_Rev_Per_Delivery DESC) as Rank_Inc_Rev
FROM     CLM_BI_Suite_Raw_Data_EventPer 
WHERE    `Event Execution Date` BETWEEN @Startdate AND @Enddate
GROUP BY `Event Name` 
ORDER BY Rank_Inc_Rev ASC
) B
SET   A.Inc_Revenue				= B.Inc_Rev
	 ,A.Inc_Rev_Per_Delivery	= B.Inc_Rev_Per_Delivery
     ,A.Rank_ROI				= B.Rank_Inc_Rev
WHERE A.`Event` = B.`Event`
AND  Year_Month_Num = (EXTRACT(YEAR_MONTH FROM @Startdate));




UPDATE SOLUS_Campaign_Scores
SET Overall_Score = (IFNULL(Rank_Reach,0) + IFNULL(Rank_Response,0) + IFNULL(Rank_Inc_Response_Instances,0) + IFNULL(Rank_ROI,0))
WHERE  Year_Month_Num = (EXTRACT(YEAR_MONTH FROM @Startdate));



UPDATE SOLUS_Campaign_Scores A,
(
SELECT `Event`, Overall_Score, DENSE_RANK() OVER(ORDER BY Overall_Score ASC) as Final_Rank
FROM SOLUS_Campaign_Scores
WHERE  (Year_Month_Num = (EXTRACT(YEAR_MONTH FROM @Startdate)) ) AND (Overall_Score > 0)
ORDER BY Final_Rank
) B
SET   A.Final_Rank	= B.Final_Rank
WHERE A.`Event` = B.`Event`
AND  Year_Month_Num = (EXTRACT(YEAR_MONTH FROM @Startdate));


DELETE FROM SOLUS_Campaign_Scores WHERE Overall_Score < 1;



update ETL_Execution_Details
	set Status ='Succeeded',
	End_Time = now()
	where Procedure_Name='S_Reporting_Top_Campaigns' and 
	Load_Execution_ID = Load_Exe_ID
	and Execution_ID = Exe_ID;
    
    

end$$
DELIMITER ;
