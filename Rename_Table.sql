DELIMITER $$
CREATE OR REPLACE PROCEDURE `Rename_Table`(IN oldName VARCHAR(255), IN newName VARCHAR(255), IN schemaname VARCHAR(255))
dqc_bill:BEGIN

SET @oldTableName = oldName;
SET @newTableName = newName;
SET @schemaname = newName;
IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_name = @oldTableName and table_schema = @schemaname ) THEN
  SET @renameStatement = CONCAT('RENAME TABLE ', @oldTableName, ' TO ', @newTableName);
  PREPARE stmt FROM @renameStatement;
  EXECUTE stmt;
  DEALLOCATE PREPARE stmt;
  SELECT 'Table renamed successfully!';
ELSE
  SELECT 'Table does not exist.';
END IF;

END$$
DELIMITER ;
