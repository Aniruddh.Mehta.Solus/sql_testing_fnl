DELIMITER $$
CREATE or replace PROCEDURE `S_UI_Campaign_Task_List`()
BEGIN
	DECLARE vStart_Cnt BIGINT DEFAULT 0;
    DECLARE vEnd_Cnt BIGINT DEFAULT 10000;
	DECLARE vBatchSize BIGINT;
	
    DECLARE done1,done2,done3,done4 INT DEFAULT FALSE;
	
	-- LifeCycle Segment Master Details
    DECLARE LifeCycleId_cur1,Exe_ID int;
    DECLARE LifeCycleReplacedQuery_cur1 TEXT;
	
	-- Campaign Segment Master Details
	DECLARE CampaignId_cur1,Exe_ID1 int;
    DECLARE CampaignReplacedQuery_cur1 TEXT;
	DECLARE CampaignReplacedBillQuery_cur1 TEXT;
	
	-- Region Master Details
	DECLARE RegionId_cur1,Exe_ID2 int;
    DECLARE RegionReplacedQuery_cur1 TEXT;
	DECLARE RegionReplacedBillQuery_cur1 TEXT;
	
    DECLARE vSuccess int (4);                      
	DECLARE vErrMsg Text ; 
	/*
	Declare cur1 cursor for
    select distinct CLMSegmentId,CLMSegmentReplacedQuery from CLM_Segment
    where CLMSegmentReplacedQuery <> '' and In_Use="Yes" and CLMSegmentId not in (Select Distinct LifeCycleSegmentId from UI_Coverage)
	and CLMSegmentId not in (Select Event_Id  from UI_Campaign_Task_List 
								where (UI_Campaign_Task="LifeCycleSegment_Delete"  and Status="START") 
								and exists(Select 1 from UI_Campaign_Task_List where UI_Campaign_Task="UICoverage_RUN" and Status="ENDED")
							)
	ORDER BY CLMSegmentId ASC;
	
	
	
	Declare cur2 cursor for
    select distinct MicroSegmentId,MSReplacedQuery,MSReplacedQueryBill from CLM_MicroSegment
    where (MSReplacedQuery <> '' or MSReplacedQueryBill<>'') and In_Use="Yes" and MicroSegmentId not in (Select Distinct CampaignSegmentId from UI_Coverage)
    and MicroSegmentId not in (  Select Event_Id  from UI_Campaign_Task_List 
									where (UI_Campaign_Task="CampaignSegment_Delete"  and Status="START") 
									and exists(Select 1 from UI_Campaign_Task_List where UI_Campaign_Task="UICoverage_RUN" and Status="ENDED")
							  )
	ORDER BY MicroSegmentId ASC;
	
	
	Declare cur3 cursor for
     select distinct RegionSegmentId,RegionReplacedQuery,RegionReplacedQueryBill from CLM_RegionSegment
    where (RegionSegmentId <> '' or RegionReplacedQueryBill<>'')and In_Use="Yes"  and RegionSegmentId not in (Select Distinct RegionSegmentId from UI_Coverage)
	and RegionSegmentId not in (Select Event_Id  from UI_Campaign_Task_List 
									where (UI_Campaign_Task="RegionSegment_Delete"  and Status="START") 
									and exists(Select 1 from UI_Campaign_Task_List where UI_Campaign_Task="UICoverage_RUN" and Status="ENDED")
								)
	
    ORDER BY RegionSegmentId ASC;
	
	Declare cur4 cursor for
    select distinct CLMSegmentId,CLMSegmentReplacedQuery from CLM_Segment
    where CLMSegmentReplacedQuery <> ''
    ORDER BY CLMSegmentId ASC;
	
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done1 = TRUE;
*/
	SET vBatchSize =10000;
	Set @S_CLM_RUN_Status='';
	Set @S_CLM_RUN_Event_Id='';
	Set @S_CLM_RUN_Run_Mode='';
	
	Select `Status` into @S_CLM_RUN_Status from UI_Campaign_Task_List 
	where UI_Campaign_Task="S_CLM_RUN" and Status="START" order by UI_Campaign_Task_Id desc limit 1;
	
    Select Event_Id into @S_CLM_RUN_Event_Id from UI_Campaign_Task_List
	where UI_Campaign_Task="S_CLM_RUN" and Status=@S_CLM_RUN_Status  order by UI_Campaign_Task_Id desc limit 1;
	
    Select Run_Mode into @S_CLM_RUN_Run_Mode from UI_Campaign_Task_List 
	where UI_Campaign_Task="S_CLM_RUN" and Status= @S_CLM_RUN_Status and Event_Id=@S_CLM_RUN_Event_Id  order by UI_Campaign_Task_Id desc limit 1;
	
	Select `UI_Campaign_Task_Id` into @UI_Campaign_Task_Id from UI_Campaign_Task_List 
	where UI_Campaign_Task="S_CLM_RUN" and Status="START" order by UI_Campaign_Task_Id desc limit 1;
	Select @S_CLM_RUN_Status ,@S_CLM_RUN_Event_Id, @S_CLM_RUN_Run_Mode;
	
	If @S_CLM_RUN_Status="START"
		Then 
			If @S_CLM_RUN_Run_Mode="RUN"
			THEN 
					SET SQL_SAFE_UPDATES=0;
					Insert IGNORE Into CLM_so2_Product_Master (Product_id,Product_LOV_Id,Product_URL)
					Select Product_id,Product_LOV_Id,Product_URL from CDM_Product_Master;
					
					Update CLM_so2_Product_Master
					Set so2_code= substr(md5(rand()),1,3)
					where so2_code is null;
			End If;
			
			/*Set @SQL1 =Concat('Update 	UI_Campaign_Task_List
			Set  Status="ENDED"
			Where UI_Campaign_Task="S_CLM_RUN" and Event_Id=',@S_CLM_RUN_Event_Id,' 
			and Run_Mode="',@S_CLM_RUN_Run_Mode,'" and UI_Campaign_Task_Id ="',@UI_Campaign_Task_Id,'";');
			  SELECT @SQL1;
						PREPARE statement from @SQL1;
						Execute statement;
						Deallocate PREPARE statement;
			
			*/
			
				Select @S_CLM_RUN_Status ,@S_CLM_RUN_Event_Id, @S_CLM_RUN_Run_Mode;
				
			call S_CLM_Run ( @S_CLM_RUN_Event_Id,@S_CLM_RUN_Run_Mode,@no_of_customers,@no_of_customers_final,@Filename,@FileHeader,@FileLocation,@RecoFilename,@RecoFileHeader,@RecoFileRecoHeader,@RecoFileLocation,@vSuccess,@vErrMsg );
			
			If @S_CLM_RUN_Run_Mode="RUN"
				Then call S_UI_Campaign_Ready_To_Send_Events();
INSERT into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
             values ('S_CLM_Run',concat("Completed the CLM From UI ") ,now(),now(),'Started',@S_CLM_RUN_Event_Id);				
			End if;	
			
			
	End If;

	
	
	
	
	
	
 END$$
DELIMITER ;
