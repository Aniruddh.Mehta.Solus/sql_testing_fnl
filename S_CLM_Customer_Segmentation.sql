
CREATE or replace PROCEDURE `S_CLM_Customer_Segmentation`()
BEGIN
DECLARE N BIGINT DEFAULT 0;
DECLARE I BIGINT;
DECLARE vStart_Cnt BIGINT DEFAULT 0;
DECLARE vEnd_Cnt BIGINT;
DECLARE vBatchSize BIGINT DEFAULT 100000;

truncate CLM_Customer_Segmentation;

insert into CLM_Customer_Segmentation(Customer_id1) select Customer_Id from CDM_Customer_Master;

SELECT max(CLMSegmentId) FROM CLM_Segment INTO N;
SELECT min(CLMSegmentId) FROM CLM_Segment INTO I;


WHILE I<=N DO
SELECT CLMSegmentName INTO @COL_NAME FROM CLM_Segment where CLMSegmentId = I limit 1;

set @col=concat('LC_',replace(@COL_NAME,' ',''));

set @query1=concat("alter table CLM_Customer_Segmentation add column if not exists ","`",@col,"`"," varchar(128);");


select CLMSegmentReplacedQuery into @Repcondition from CLM_Segment where CLMSegmentId = I limit 1;
SELECT Customer_Id1 INTO vStart_Cnt FROM CLM_Customer_Segmentation ORDER BY Customer_Id1 ASC LIMIT 1;
SELECT Customer_Id1 INTO @Rec_Cnt FROM CLM_Customer_Segmentation ORDER BY Customer_Id1 DESC LIMIT 1;

PREPARE statement1 from @query1;
Execute statement1;
Deallocate PREPARE statement1;


set @index1=concat("create index if not exists ","`",@col,"`"," on CLM_Customer_Segmentation(","`",@col,"`",")",";");

PREPARE statement4 from @index1;
Execute statement4;
Deallocate PREPARE statement4;

set sql_safe_updates=0;
PROCESS_LOOP: LOOP
SET vEnd_Cnt = vStart_Cnt + vBatchSize;

set @update1=concat("update CLM_Customer_Segmentation a, V_CLM_Customer_One_View b
set ","`",concat('LC_',replace(@COL_NAME,' ','')),"`"," = 1
where a.Customer_Id1 = b.Customer_Id
and Customer_Id1 between ",vStart_Cnt," and ",vEnd_Cnt," 
and ",@Repcondition,";");

PREPARE statement3 from @update1;
Execute statement3;
Deallocate PREPARE statement3;

SET vStart_Cnt = vEnd_Cnt;

IF vStart_Cnt  >= @Rec_Cnt THEN
	LEAVE PROCESS_LOOP;
END IF;        
END LOOP PROCESS_LOOP; 


SET I = I +1;


END WHILE;


SELECT max(MicroSegmentId) FROM CLM_MicroSegment INTO N;
SELECT min(MicroSegmentId) FROM CLM_MicroSegment INTO I;

WHILE I<=N DO
SELECT MicroSegmentName INTO @COL_NAME FROM CLM_MicroSegment where MicroSegmentId = I limit 1;
SELECT Customer_Id1 INTO vStart_Cnt FROM CLM_Customer_Segmentation ORDER BY Customer_Id1 ASC LIMIT 1;
SELECT Customer_Id1 INTO @Rec_Cnt FROM CLM_Customer_Segmentation ORDER BY Customer_Id1 DESC LIMIT 1;
select MSReplacedQuery into @Repcondition from CLM_MicroSegment where MicroSegmentId = I limit 1;

set @col2=concat('CM_',replace(@COL_NAME,' ',''));

set @query2=concat("alter table CLM_Customer_Segmentation add column if not exists","`",@col2,"`"," varchar(128);");


PREPARE statement1 from @query2;
Execute statement1;
Deallocate PREPARE statement1;

set @index2=concat("create index if not exists ","`",@col2,"`"," on CLM_Customer_Segmentation(","`",@col2,"`",")",";");

PREPARE statement4 from @index2;
Execute statement4;
Deallocate PREPARE statement4;


set sql_safe_updates=0;
PROCESS_LOOP: LOOP
SET vEnd_Cnt = vStart_Cnt + vBatchSize;


set @update2=concat("update CLM_Customer_Segmentation a, V_CLM_Customer_One_View b
set ","`",concat('CM_',replace(@COL_NAME,' ','')),"`"," = 1
where a.Customer_Id1 = b.Customer_Id
and Customer_Id1 between ",vStart_Cnt," and ",vEnd_Cnt," 
and ",@Repcondition,";");

PREPARE statement3 from @update2;
Execute statement3;
Deallocate PREPARE statement3;

SET vStart_Cnt = vEnd_Cnt;

IF vStart_Cnt  >= @Rec_Cnt THEN
	LEAVE PROCESS_LOOP;
END IF;        
END LOOP PROCESS_LOOP; 


SET I = I +1;  

END WHILE;
 
 
 
 SELECT max(EFId) FROM CLM_Exclusion_Filter INTO N;
SELECT min(EFId) FROM CLM_Exclusion_Filter INTO I;

WHILE I<=N DO
SELECT Customer_Id1 INTO vStart_Cnt FROM CLM_Customer_Segmentation ORDER BY Customer_Id1 ASC LIMIT 1;
SELECT Customer_Id1 INTO @Rec_Cnt FROM CLM_Customer_Segmentation ORDER BY Customer_Id1 DESC LIMIT 1;
 select EFReplacedQuery into @Repcondition from CLM_Exclusion_Filter where EFId = I limit 1;



set sql_safe_updates=0;
PROCESS_LOOP: LOOP
SET vEnd_Cnt = vStart_Cnt + vBatchSize;


set @update3=concat("update CLM_Customer_Segmentation a, V_CLM_Customer_One_View b
set Exclusion_Filter = 1
where a.Customer_Id1 = b.Customer_Id
and Customer_Id1 between ",vStart_Cnt," and ",vEnd_Cnt," 
and ",@Repcondition,";");



PREPARE statement3 from @update3;
Execute statement3;
Deallocate PREPARE statement3;

SET vStart_Cnt = vEnd_Cnt;

IF vStart_Cnt  >= @Rec_Cnt THEN
	LEAVE PROCESS_LOOP;
END IF;        
END LOOP PROCESS_LOOP; 

SET I = I +1;  

END WHILE;

 
END

