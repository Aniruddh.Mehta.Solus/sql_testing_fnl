DELIMITER $$
CREATE DEFINER=`DOMINOSM_SOLUS_DEP_USER`@`%` PROCEDURE `S_dashboard_QSR_loc_metrics`()
BEGIN

DELETE from log_solus where module = 'S_dashboard_QSR_loc_metrics';
SET SQL_SAFE_UPDATES=0;

SELECT `Value1` into @TYSM from UI_Configuration_Global where `Config_Name` = 'FISCAL';
SELECT @TYSM;

SELECT `Value2` into @TYEM from UI_Configuration_Global where `Config_Name` = 'FISCAL';
SELECT @TYEM;

SET @LYSM = F_Month_Diff(@TYSM,12);
SET @LYEM = F_Month_Diff(@TYEM,12);

SELECT @LYSM,@LYEM;

SET @LLYSM = F_Month_Diff(@LYSM,12);
SET @LLYEM = F_Month_Diff(@LYEM,12);

SELECT @LLYSM,@LLYEM;

SET @Start_Date = date_format(date_sub(concat(date_format(current_date(),"%Y%m"),'01'), INTERVAL 14 MONTH),"%Y%m");
SET @End_Date = date_format(current_date(),"%Y%m");

SET @TM = (SELECT date_format(current_date(),"%Y%m"));
SET @LM = F_Month_Diff(@TM,1);
SET @SMLY = F_Month_Diff(@TM,12);
SET @TY=substr(@TM,1,4);
SET @LY=substr(@SMLY,1,4);
SET @LLY = @LY - 1;
SET @SMLLY = F_Month_Diff(@TM,24);


	
DROP TABLE IF EXISTS Dashboard_QSR_loc_Temp;
  CREATE TABLE IF NOT EXISTS Dashboard_QSR_loc_Temp ( 
	`Measure` varchar(50),
	`Location` varchar(50),
    `Bill_Year_Month` int(20),
	`TM` int(20),
	`LM` int(20),
	`SMLY` int(20),
	`CH_LM_PER` decimal(10,2),
	`CH_SMLY_PER` decimal(10,2),
	`TY` int(20),
    `LY` int(20),
	`LLY` int(20),
	`CH_LY_PER` decimal(10,2),
	`CH_LLY_PER` decimal(10,2),
	`L12M` int(20),
	`L24M` int(20),
	`CH_L12M_PER` decimal(10,2),
	`OVERALL` int(20)
  ) ENGINE = INNODB DEFAULT CHARACTER SET=LATIN1; 
 
  ALTER TABLE Dashboard_QSR_loc_Temp    
    ADD INDEX (Bill_Year_Month), 
    ADD INDEX (Measure),
    ADD INDEX (Location);
set @temp_var = 1;

LOOP_6_MONTHS : LOOP

		set @temp_var = @temp_var + 1;
		IF @temp_var = 6 THEN
				LEAVE LOOP_6_MONTHS;
			END IF;
            
SELECT @TM, @LM, @SMLY, @TY, @LY, @LLY, @SMLLY, CURRENT_TIMESTAMP();
truncate loc_1;
truncate loc_2;
truncate loc_3;
truncate loc_4;
truncate loc_5;
truncate loc_6;
truncate loc_7;
truncate loc_8;
truncate loc_9;
insert into loc_1 SELECT Location,Bill_Year_Month,count(distinct(Customer_Id)) as TM
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @TM
							 and Region <> ''
							GROUP BY 1;
insert into loc_2 SELECT Location,count(distinct(Customer_Id)) as LM
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @LM
							and Region <> ''
							GROUP BY 1;
insert into loc_3  SELECT Location,count(distinct(Customer_Id)) as SMLY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @SMLY
							and Region <> ''
							GROUP BY 1;

insert into loc_4 SELECT Location,count(distinct(Customer_Id)) as TY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @TYSM AND @TYEM
							and  Region <> ''
							GROUP BY 1;

insert into loc_5  SELECT Location,count(distinct(Customer_Id)) as LY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @LYSM AND @LYEM
							and Region <> ''
							GROUP BY 1;

insert into loc_6 SELECT Location,count(distinct(Customer_Id)) as LLY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @LLYSM AND @LLYEM
							and Region <> ''
							GROUP BY 1;
insert into loc_7 SELECT Location,count(distinct(Customer_Id)) as L12M
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @SMLY AND @TM
							and Region <> ''
							GROUP BY 1   ;    
insert into loc_8                              SELECT Location,count(distinct(Customer_Id)) as L24M
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @SMLLY AND @TM
							and  Region <> ''
							GROUP BY 1;                                                
insert into loc_9 SELECT Location,count(distinct(Customer_Id)) as OVERALL
							from Dashboard_Insights_QSR_Customers_Temp
							WHERE  Region <> ''
							GROUP BY 1;
INSERT INTO Dashboard_QSR_loc_Temp
(
`Measure`,
`Location`,
`Bill_Year_Month`,
`TM`,
`LM`,
`SMLY`,
`TY`,
`LY`,
`LLY`,
`L12M`,
`L24M`,
`OVERALL`
)		
WITH GROUP1 AS
                            (
							SELECT * from loc_1
                            )
                             ,
                             GROUP2 AS
							(
                             select * from loc_2
                            )
                             ,
                             GROUP3 AS
							(
                            select * from loc_3
                            )
                             ,
                             GROUP4 AS
							(
                              select * from loc_4
                            )
                             ,
                             GROUP5 AS
							(
                            select * from loc_5
                            )
                            ,
                             GROUP6 AS
							(
                             select * from loc_6
                            )
                            ,
                             GROUP7 AS
							(
                             select * from loc_7
                            )
                            ,
                             GROUP8 AS
							(
                            select * from loc_8
                            )
                             ,
                             GROUP9 AS
							(
                            select * from loc_9 
                            )
                           
                            SELECT 'Base' AS Measure, A.X, A.Y, A.Z,B.Y, C.Y,D.Y,E.Y,F.Y,G.Y,H.Y, I.Y
                            FROM GROUP1 A
                            JOIN GROUP2 B
                            ON A.X=B.X
                            JOIN GROUP3 C
                            ON C.X=B.X
                            JOIN GROUP4 D
                            ON D.X=C.X
                            JOIN GROUP5 E
                            ON E.X=D.X
                            JOIN GROUP6 F
                            ON F.X=E.X
                            JOIN GROUP7 G
                            ON G.X=F.X
                            JOIN GROUP8 H
                            ON H.X=G.X
                            JOIN GROUP9 I
                            ON H.X=I.X;

UPDATE Dashboard_QSR_loc_Temp
SET CH_LM_PER = (TM-LM)*100/LM
WHERE Bill_Year_Month = @TM
AND Measure = 'Base';

UPDATE Dashboard_QSR_loc_Temp
SET CH_SMLY_PER = (TM-SMLY)*100/SMLY
WHERE Bill_Year_Month = @TM
AND Measure = 'Base';

UPDATE Dashboard_QSR_loc_Temp
SET CH_LY_PER = (TY-LY)*100/LY
WHERE Bill_Year_Month = @TM
AND Measure = 'Base';

UPDATE Dashboard_QSR_loc_Temp
SET CH_LLY_PER = (TY-LLY)*100/LLY
WHERE Bill_Year_Month = @TM
AND Measure = 'Base';

UPDATE Dashboard_QSR_loc_Temp
SET CH_L12M_PER = (L12M-L24M)*100/L24M
WHERE Bill_Year_Month = @TM
AND Measure = 'Base';

truncate loc_1;
truncate loc_2;
truncate loc_3;
truncate loc_4;
truncate loc_5;
truncate loc_6;
truncate loc_7;
truncate loc_8;
truncate loc_9;
insert into loc_1 SELECT Location,Bill_Year_Month,COUNT(DISTINCT Bill_Header_Id) as TM
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @TM
							and Region <> ''
							GROUP BY 1;
insert into loc_2 SELECT Location,COUNT(DISTINCT Bill_Header_Id) as LM
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @LM
							 and Region <> ''
							GROUP BY 1;
insert into loc_3   SELECT Location,COUNT(DISTINCT Bill_Header_Id) as SMLY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @SMLY
							 and Region <> ''
							GROUP BY 1;

insert into loc_4  SELECT Location,COUNT(DISTINCT Bill_Header_Id) as TY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @TYSM AND @TYEM
							 and Region <> ''
							GROUP BY 1;

insert into loc_5  SELECT Location,COUNT(DISTINCT Bill_Header_Id) as LY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @LYSM AND @LYEM
							 and Region <> ''
							GROUP BY 1;

insert into loc_6   SELECT Location,COUNT(DISTINCT Bill_Header_Id) as LLY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @LLYSM AND @LLYEM
							 and Region <> ''
							GROUP BY 1;
insert into loc_7   SELECT Location,COUNT(DISTINCT Bill_Header_Id) as L12M
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @SMLY AND @TM
							 and Region <> ''
							GROUP BY 1   ;    
insert into loc_8        SELECT Location,COUNT(DISTINCT Bill_Header_Id) as L24M
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @SMLLY AND @TM
							 and Region <> ''
							GROUP BY 1;                                                


INSERT INTO Dashboard_QSR_loc_Temp
(
`Measure`,
`Location`,
`Bill_Year_Month`,
`TM`,
`LM`,
`SMLY`,
`TY`,
`LY`,
`LLY`,
`L12M`,
`L24M`
)		
WITH GROUP1 AS
                           (
							SELECT * from loc_1
                            )
                             ,
                             GROUP2 AS
							(
                             select * from loc_2
                            )
                             ,
                             GROUP3 AS
							(
                            select * from loc_3
                            )
                             ,
                             GROUP4 AS
							(
                              select * from loc_4
                            )
                             ,
                             GROUP5 AS
							(
                            select * from loc_5
                            )
                            ,
                             GROUP6 AS
							(
                             select * from loc_6
                            )
                            ,
                             GROUP7 AS
							(
                             select * from loc_7
                            )
                            ,
                             GROUP8 AS
							(
                            select * from loc_8
                            )
                            SELECT 'Bills' AS Measure, A.X, A.Y, A.Z,B.Y, C.Y,D.Y,E.Y,F.Y,G.Y,H.Y
                            FROM GROUP1 A
                            JOIN GROUP2 B
                            ON A.X=B.X
                            JOIN GROUP3 C
                            ON C.X=B.X
                            JOIN GROUP4 D
                            ON D.X=C.X
                            JOIN GROUP5 E
                            ON E.X=D.X
                            JOIN GROUP6 F
                            ON F.X=E.X
                            JOIN GROUP7 G
                            ON G.X=F.X
                            JOIN GROUP8 H
                            ON H.X=G.X;

UPDATE Dashboard_QSR_loc_Temp
SET CH_LM_PER = (TM-LM)*100/LM
WHERE Bill_Year_Month = @TM
AND Measure = 'Bills';

UPDATE Dashboard_QSR_loc_Temp
SET CH_SMLY_PER = (TM-SMLY)*100/SMLY
WHERE Bill_Year_Month = @TM
AND Measure = 'Bills';

UPDATE Dashboard_QSR_loc_Temp
SET CH_LY_PER = (TY-LY)*100/LY
WHERE Bill_Year_Month = @TM
AND Measure = 'Bills';

UPDATE Dashboard_QSR_loc_Temp
SET CH_LLY_PER = (TY-LLY)*100/LLY
WHERE Bill_Year_Month = @TM
AND Measure = 'Bills';

UPDATE Dashboard_QSR_loc_Temp
SET CH_L12M_PER = (L12M-L24M)*100/L24M
WHERE Bill_Year_Month = @TM
AND Measure = 'Bills';


truncate loc_1;
truncate loc_2;
truncate loc_3;
truncate loc_4;
truncate loc_5;
truncate loc_6;
truncate loc_7;
truncate loc_8;
truncate loc_9;
insert into loc_1 SELECT Location,Bill_Year_Month,sum(Sale_Net_Val) as TM
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @TM
							 and Region <> ''
							GROUP BY 1;
insert into loc_2  SELECT Location,sum(Sale_Net_Val) as LM
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @LM
							 and Region <> ''
							GROUP BY 1;
insert into loc_3    SELECT Location,sum(Sale_Net_Val) as SMLY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @SMLY
							 and Region <> ''
							GROUP BY 1;

insert into loc_4   SELECT Location,sum(Sale_Net_Val) as TY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @TYSM AND @TYEM
							 and Region <> ''
							GROUP BY 1;

insert into loc_5    SELECT Location,sum(Sale_Net_Val) as LY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @LYSM AND @LYEM
							 and Region <> ''
							GROUP BY 1;

insert into loc_6     SELECT Location,sum(Sale_Net_Val) as LLY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @LLYSM AND @LLYEM
							 and Region <> ''
							GROUP BY 1;
insert into loc_7    SELECT Location,sum(Sale_Net_Val) as L12M
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @SMLY AND @TM
							 and Region <> ''
							GROUP BY 1   ;    
insert into loc_8          SELECT Location,sum(Sale_Net_Val) as L24M
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @SMLLY AND @TM
							 and Region <> ''
							GROUP BY 1;

INSERT INTO Dashboard_QSR_loc_Temp
(
`Measure`,
`Location`,
`Bill_Year_Month`,
`TM`,
`LM`,
`SMLY`,
`TY`,
`LY`,
`LLY`,
`L12M`,
`L24M`
)		
WITH GROUP1 AS
                            (
							SELECT * from loc_1
                            )
                             ,
                             GROUP2 AS
							(
                             select * from loc_2
                            )
                             ,
                             GROUP3 AS
							(
                            select * from loc_3
                            )
                             ,
                             GROUP4 AS
							(
                              select * from loc_4
                            )
                             ,
                             GROUP5 AS
							(
                            select * from loc_5
                            )
                            ,
                             GROUP6 AS
							(
                             select * from loc_6
                            )
                            ,
                             GROUP7 AS
							(
                             select * from loc_7
                            )
                            ,
                             GROUP8 AS
							(
                            select * from loc_8
                            )
                            SELECT 'Revenue' AS Measure, A.X, A.Y, A.Z,B.Y, C.Y,D.Y,E.Y,F.Y,G.Y,H.Y
                            FROM GROUP1 A
                            JOIN GROUP2 B
                            ON A.X=B.X
                            JOIN GROUP3 C
                            ON C.X=B.X
                            JOIN GROUP4 D
                            ON D.X=C.X
                            JOIN GROUP5 E
                            ON E.X=D.X
                            JOIN GROUP6 F
                            ON F.X=E.X
                            JOIN GROUP7 G
                            ON G.X=F.X
                            JOIN GROUP8 H
                            ON H.X=G.X;

UPDATE Dashboard_QSR_loc_Temp
SET CH_LM_PER = (TM-LM)*100/LM
WHERE Bill_Year_Month = @TM
AND Measure = 'Revenue';

UPDATE Dashboard_QSR_loc_Temp
SET CH_SMLY_PER = (TM-SMLY)*100/SMLY
WHERE Bill_Year_Month = @TM
AND Measure = 'Revenue';

UPDATE Dashboard_QSR_loc_Temp
SET CH_LY_PER = (TY-LY)*100/LY
WHERE Bill_Year_Month = @TM
AND Measure = 'Revenue';

UPDATE Dashboard_QSR_loc_Temp
SET CH_LLY_PER = (TY-LLY)*100/LLY
WHERE Bill_Year_Month = @TM
AND Measure = 'Revenue';

UPDATE Dashboard_QSR_loc_Temp
SET CH_L12M_PER = (L12M-L24M)*100/L24M
WHERE Bill_Year_Month = @TM
AND Measure = 'Revenue';


truncate loc_1;
truncate loc_2;
truncate loc_3;
truncate loc_4;
truncate loc_5;
truncate loc_6;
truncate loc_7;
truncate loc_8;
truncate loc_9;
insert into loc_1 							SELECT Location,Bill_Year_Month,sum(Sale_Net_Val) as TM
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @TM
							and Is_Existing_Customer = 1
							 and Region <> ''
							GROUP BY 1;
insert into loc_2                               SELECT Location,sum(Sale_Net_Val) as LM
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @LM
							and Is_Existing_Customer = 1
							 and Region <> ''
							GROUP BY 1;
insert into loc_3       SELECT Location,sum(Sale_Net_Val) as SMLY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @SMLY
							and Is_Existing_Customer = 1
							 and Region <> ''
							GROUP BY 1;

insert into loc_4    SELECT Location,sum(Sale_Net_Val) as TY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @TYSM AND @TYEM
							and Is_Existing_Customer = 1
							 and Region <> ''
							GROUP BY 1;

insert into loc_5      SELECT Location,sum(Sale_Net_Val) as LY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @LYSM AND @LYEM
							and Is_Existing_Customer = 1
							 and Region <> ''
							GROUP BY 1;

insert into loc_6       SELECT Location,sum(Sale_Net_Val) as LLY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @LLYSM AND @LLYEM
							and Is_Existing_Customer = 1
							 and Region <> ''
							GROUP BY 1;
insert into loc_7      SELECT Location,sum(Sale_Net_Val) as L12M
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @SMLY AND @TM
							and Is_Existing_Customer = 1
							 and Region <> ''
							GROUP BY 1  ;    
insert into loc_8           SELECT Location,sum(Sale_Net_Val) as L24M
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @SMLLY AND @TM
							and Is_Existing_Customer = 1
							 and Region <> ''
							GROUP BY 1;

INSERT INTO Dashboard_QSR_loc_Temp
(
`Measure`,
`Location`,
`Bill_Year_Month`,
`TM`,
`LM`,
`SMLY`,
`TY`,
`LY`,
`LLY`,
`L12M`,
`L24M`
)		
WITH GROUP1 AS
                           (
							SELECT * from loc_1
                            )
                             ,
                             GROUP2 AS
							(
                             select * from loc_2
                            )
                             ,
                             GROUP3 AS
							(
                            select * from loc_3
                            )
                             ,
                             GROUP4 AS
							(
                              select * from loc_4
                            )
                             ,
                             GROUP5 AS
							(
                            select * from loc_5
                            )
                            ,
                             GROUP6 AS
							(
                             select * from loc_6
                            )
                            ,
                             GROUP7 AS
							(
                             select * from loc_7
                            )
                            ,
                             GROUP8 AS
							(
                            select * from loc_8
                            )
                            SELECT 'Repeat_Revenue' AS Measure, A.X, A.Y, A.Z,B.Y, C.Y,D.Y,E.Y,F.Y,G.Y,H.Y
                            FROM GROUP1 A
                            JOIN GROUP2 B
                            ON A.X=B.X
                            JOIN GROUP3 C
                            ON C.X=B.X
                            JOIN GROUP4 D
                            ON D.X=C.X
                            JOIN GROUP5 E
                            ON E.X=D.X
                            JOIN GROUP6 F
                            ON F.X=E.X
                            JOIN GROUP7 G
                            ON G.X=F.X
                            JOIN GROUP8 H
                            ON H.X=G.X;

UPDATE Dashboard_QSR_loc_Temp
SET CH_LM_PER = (TM-LM)*100/LM
WHERE Bill_Year_Month = @TM
AND Measure = 'Repeat_Revenue';

UPDATE Dashboard_QSR_loc_Temp
SET CH_SMLY_PER = (TM-SMLY)*100/SMLY
WHERE Bill_Year_Month = @TM
AND Measure = 'Repeat_Revenue';

UPDATE Dashboard_QSR_loc_Temp
SET CH_LY_PER = (TY-LY)*100/LY
WHERE Bill_Year_Month = @TM
AND Measure = 'Repeat_Revenue';

UPDATE Dashboard_QSR_loc_Temp
SET CH_LLY_PER = (TY-LLY)*100/LLY
WHERE Bill_Year_Month = @TM
AND Measure = 'Repeat_Revenue';

UPDATE Dashboard_QSR_loc_Temp
SET CH_L12M_PER = (L12M-L24M)*100/L24M
WHERE Bill_Year_Month = @TM
AND Measure = 'Repeat_Revenue';


truncate loc_1x;
truncate loc_2x;
truncate loc_3x;
truncate loc_4x;
truncate loc_5x;
truncate loc_6x;
truncate loc_7x;
truncate loc_8x;
truncate loc_9x;
insert into loc_1x 														select 
							A.Location,
                            Bill_Year_Month,
							'OVERALL' AS Measure,
							(A.c1*100)/B.c2 as TM 
							from 
							(
							(select Location,Bill_Year_Month,
							SUM(Sale_Net_Val) as c1
							from Dashboard_Insights_QSR_Customers_Temp 
							where Is_Existing_Customer = "1" 
							AND Bill_Year_Month = @TM
							group by Location)A
							,
							(select Location, 
							SUM(Sale_Net_Val) as c2
							from Dashboard_Insights_QSR_Customers_Temp 
							WHERE Bill_Year_Month = @TM
							group by Location)B
							)
							where A.Location = B.Location
							and A.Location <> ""
							group by A.Location;
insert into loc_2x            select 
							A.Location,
							'OVERALL' AS Measure,
							(A.c1*100)/B.c2 as LM 
							from 
							(
							(select Location,
							SUM(Sale_Net_Val) as c1
							from Dashboard_Insights_QSR_Customers_Temp 
							where Is_Existing_Customer = "1" 
							AND Bill_Year_Month = @LM
							group by Location)A
							,
							(select Location, 
							SUM(Sale_Net_Val) as c2
							from Dashboard_Insights_QSR_Customers_Temp 
							WHERE Bill_Year_Month = @LM
							group by Location)B
							)
							where A.Location = B.Location
							and A.Location <> ""
							group by A.Location;
insert into loc_3x       select 
							A.Location,
							'OVERALL' AS Measure,
							(A.c1*100)/B.c2 as SMLY 
							from 
							(
							(select Location,
							SUM(Sale_Net_Val) as c1
							from Dashboard_Insights_QSR_Customers_Temp 
							where Is_Existing_Customer = "1" 
							AND Bill_Year_Month = @SMLY
							group by Location)A
							,
							(select Location, 
							SUM(Sale_Net_Val) as c2
							from Dashboard_Insights_QSR_Customers_Temp 
							WHERE Bill_Year_Month = @SMLY
							group by Location)B
							)
							where A.Location = B.Location
							and A.Location <> ""
							group by A.Location
                            ;

insert into loc_4x    select 
							A.Location,
							'OVERALL' AS Measure,
							(A.c1*100)/B.c2 as TY 
							from 
							(
							(select Location,
							SUM(Sale_Net_Val) as c1
							from Dashboard_Insights_QSR_Customers_Temp 
							where Is_Existing_Customer = "1" 
							AND Bill_Year_Month BETWEEN @TYSM AND @TYEM
							group by Location)A
							,
							(select Location, 
							SUM(Sale_Net_Val) as c2
							from Dashboard_Insights_QSR_Customers_Temp 
							WHERE Bill_Year_Month BETWEEN @TYSM AND @TYEM
							group by Location)B
							)
							where A.Location = B.Location
							and A.Location <> ""
							group by A.Location
                            ;

insert into loc_5x      select 
							A.Location,
							'OVERALL' AS Measure,
							(A.c1*100)/B.c2 as LY 
							from 
							(
							(select Location,
							SUM(Sale_Net_Val) as c1
							from Dashboard_Insights_QSR_Customers_Temp 
							where Is_Existing_Customer = "1" 
							AND Bill_Year_Month BETWEEN @LYSM AND @LYEM
							group by Location)A
							,
							(select Location, 
							SUM(Sale_Net_Val) as c2
							from Dashboard_Insights_QSR_Customers_Temp 
							WHERE Bill_Year_Month BETWEEN @LYSM AND @LYEM
							group by Location)B
							)
							where A.Location = B.Location
							and A.Location <> ""
							group by A.Location
                            ;

insert into loc_6x       select 
							A.Location,
							'OVERALL' AS Measure,
							(A.c1*100)/B.c2 as LLY 
							from 
							(
							(select Location,
							SUM(Sale_Net_Val) as c1
							from Dashboard_Insights_QSR_Customers_Temp 
							where Is_Existing_Customer = "1" 
							AND Bill_Year_Month BETWEEN @LLYSM AND @LLYEM
							group by Location)A
							,
							(select Location, 
							SUM(Sale_Net_Val) as c2
							from Dashboard_Insights_QSR_Customers_Temp 
							WHERE Bill_Year_Month BETWEEN @LLYSM AND @LLYEM
							group by Location)B
							)
							where A.Location = B.Location
							and A.Location <> ""
							group by A.Location
                            ;
insert into loc_7x       select 
							A.Location,
							'OVERALL' AS Measure,
							(A.c1*100)/B.c2 as L12M
							from 
							(
							(select Location,
							SUM(Sale_Net_Val) as c1
							from Dashboard_Insights_QSR_Customers_Temp 
							where Is_Existing_Customer = "1" 
							AND Bill_Year_Month BETWEEN @SMLY AND @TM
							group by Location)A
							,
							(select Location, 
							SUM(Sale_Net_Val) as c2
							from Dashboard_Insights_QSR_Customers_Temp 
							WHERE Bill_Year_Month BETWEEN @SMLY AND @TM
							group by Location)B
							)
							where A.Location = B.Location
							and A.Location <> ""
							group by A.Location
                              ;    
insert into loc_8x        select 
							A.Location,
							'OVERALL' AS Measure,
							(A.c1*100)/B.c2 as L24M
							from 
							(
							(select Location,
							SUM(Sale_Net_Val) as c1
							from Dashboard_Insights_QSR_Customers_Temp 
							where Is_Existing_Customer = "1" 
							AND Bill_Year_Month BETWEEN @SMLLY AND @TM
							group by Location)A
							,
							(select Location, 
							SUM(Sale_Net_Val) as c2
							from Dashboard_Insights_QSR_Customers_Temp 
							WHERE Bill_Year_Month BETWEEN @SMLLY AND @TM
							group by Location)B
							)
							where A.Location = B.Location
							and A.Location <> ""
							group by A.Location;


INSERT INTO Dashboard_QSR_loc_Temp
(
`Measure`,
`Location`,
`Bill_Year_Month`,
`TM`,
`LM`,
`SMLY`,
`TY`,
`LY`,
`LLY`,
`L12M`,
`L24M`
)		
WITH GROUP1 AS
                            (
							SELECT * from loc_1x
                            )
                             ,
                             GROUP2 AS
							(
                             select * from loc_2x
                            )
                             ,
                             GROUP3 AS
							(
                            select * from loc_3x
                            )
                             ,
                             GROUP4 AS
							(
                              select * from loc_4x
                            )
                             ,
                             GROUP5 AS
							(
                            select * from loc_5x
                            )
                            ,
                             GROUP6 AS
							(
                             select * from loc_6x
                            )
                            ,
                             GROUP7 AS
							(
                             select * from loc_7x
                            )
                            ,
                             GROUP8 AS
							(
                            select * from loc_8x
                            )
                            SELECT 'REPEAT_REV_CONTR' AS Measure, A.X, A.Y, A.Z,B.Z, C.Z,D.Z,E.Z,F.Z,G.Z,H.Z
                            FROM GROUP1 A
                            JOIN GROUP2 B
                            ON A.X=B.X
                            JOIN GROUP3 C
                            ON C.X=B.X
                            JOIN GROUP4 D
                            ON D.X=C.X
                            JOIN GROUP5 E
                            ON E.X=D.X
                            JOIN GROUP6 F
                            ON F.X=E.X
                            JOIN GROUP7 G
                            ON G.X=F.X
                            JOIN GROUP8 H
                            ON H.X=G.X;

UPDATE Dashboard_QSR_loc_Temp
SET CH_LM_PER = (TM-LM)*100/LM
WHERE Bill_Year_Month = @TM
AND Measure = 'REPEAT_REV_CONTR';

UPDATE Dashboard_QSR_loc_Temp
SET CH_SMLY_PER = (TM-SMLY)*100/SMLY
WHERE Bill_Year_Month = @TM
AND Measure = 'REPEAT_REV_CONTR';

UPDATE Dashboard_QSR_loc_Temp
SET CH_LY_PER = (TY-LY)*100/LY
WHERE Bill_Year_Month = @TM
AND Measure = 'REPEAT_REV_CONTR';

UPDATE Dashboard_QSR_loc_Temp
SET CH_LLY_PER = (TY-LLY)*100/LLY
WHERE Bill_Year_Month = @TM
AND Measure = 'REPEAT_REV_CONTR';

UPDATE Dashboard_QSR_loc_Temp
SET CH_L12M_PER = (L12M-L24M)*100/L24M
WHERE Bill_Year_Month = @TM
AND Measure = 'REPEAT_REV_CONTR';

truncate loc_1;
truncate loc_2;
truncate loc_3;
truncate loc_4;
truncate loc_5;
truncate loc_6;
truncate loc_7;
truncate loc_8;
truncate loc_9;
insert into loc_1 						SELECT Location,Bill_Year_Month,sum(Sale_Net_Val)/COUNT(DISTINCT Bill_Header_Id) as TM
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @TM
							 and Region <> ''
							GROUP BY 1;
insert into loc_2        SELECT Location,sum(Sale_Net_Val)/COUNT(DISTINCT Bill_Header_Id) as LM
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @LM
							 and Region <> ''
							GROUP BY 1;
insert into loc_3       SELECT Location,sum(Sale_Net_Val)/COUNT(DISTINCT Bill_Header_Id) as SMLY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @SMLY
							 and Region <> ''
							GROUP BY 1;

insert into loc_4      SELECT Location,sum(Sale_Net_Val)/COUNT(DISTINCT Bill_Header_Id) as TY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @TYSM AND @TYEM
							 and Region <> ''
							GROUP BY 1;

insert into loc_5      SELECT Location,sum(Sale_Net_Val)/COUNT(DISTINCT Bill_Header_Id) as LY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @LYSM AND @LYEM
							 and Region <> ''
							GROUP BY 1;

insert into loc_6           SELECT Location,sum(Sale_Net_Val)/COUNT(DISTINCT Bill_Header_Id) as LLY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @LLYSM AND @LLYEM
							 and Region <> ''
							GROUP BY 1;
insert into loc_7         SELECT Location,sum(Sale_Net_Val)/COUNT(DISTINCT Bill_Header_Id) as L12M
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @SMLY AND @TM
							 and Region <> ''
							GROUP BY 1  ;    
insert into loc_8            SELECT Location,sum(Sale_Net_Val)/COUNT(DISTINCT Bill_Header_Id) as L24M
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @SMLLY AND @TM
							 and Region <> ''
							GROUP BY 1;

INSERT INTO Dashboard_QSR_loc_Temp
(
`Measure`,
`Location`,
`Bill_Year_Month`,
`TM`,
`LM`,
`SMLY`,
`TY`,
`LY`,
`LLY`,
`L12M`,
`L24M`
)		
WITH GROUP1 AS
                         (
							SELECT * from loc_1
                            )
                             ,
                             GROUP2 AS
							(
                             select * from loc_2
                            )
                             ,
                             GROUP3 AS
							(
                            select * from loc_3
                            )
                             ,
                             GROUP4 AS
							(
                              select * from loc_4
                            )
                             ,
                             GROUP5 AS
							(
                            select * from loc_5
                            )
                            ,
                             GROUP6 AS
							(
                             select * from loc_6
                            )
                            ,
                             GROUP7 AS
							(
                             select * from loc_7
                            )
                            ,
                             GROUP8 AS
							(
                            select * from loc_8
                            )
                            SELECT 'APC' AS Measure, A.X, A.Y, A.Z,B.Y, C.Y,D.Y,E.Y,F.Y,G.Y,H.Y
                            FROM GROUP1 A
                            JOIN GROUP2 B
                            ON A.X=B.X
                            JOIN GROUP3 C
                            ON C.X=B.X
                            JOIN GROUP4 D
                            ON D.X=C.X
                            JOIN GROUP5 E
                            ON E.X=D.X
                            JOIN GROUP6 F
                            ON F.X=E.X
                            JOIN GROUP7 G
                            ON G.X=F.X
                            JOIN GROUP8 H
                            ON H.X=G.X;
							
UPDATE Dashboard_QSR_loc_Temp
SET CH_LM_PER = (TM-LM)*100/LM
WHERE Bill_Year_Month = @TM
AND Measure = 'APC';

UPDATE Dashboard_QSR_loc_Temp
SET CH_SMLY_PER = (TM-SMLY)*100/SMLY
WHERE Bill_Year_Month = @TM
AND Measure = 'APC';

UPDATE Dashboard_QSR_loc_Temp
SET CH_LY_PER = (TY-LY)*100/LY
WHERE Bill_Year_Month = @TM
AND Measure = 'APC';

UPDATE Dashboard_QSR_loc_Temp
SET CH_LLY_PER = (TY-LLY)*100/LLY
WHERE Bill_Year_Month = @TM
AND Measure = 'APC';

UPDATE Dashboard_QSR_loc_Temp
SET CH_L12M_PER = (L12M-L24M)*100/L24M
WHERE Bill_Year_Month = @TM
AND Measure = 'APC';

truncate loc_1;
truncate loc_2;
truncate loc_3;
truncate loc_4;
truncate loc_5;
truncate loc_6;
truncate loc_7;
truncate loc_8;
truncate loc_9;
insert into loc_1 					SELECT Location,Bill_Year_Month,sum(Sale_Qty) as TM
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @TM
							 and Region <> ''
							GROUP BY 1;
insert into loc_2       SELECT Location,sum(Sale_Qty) as LM
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @LM
							 and Region <> ''
							GROUP BY 1;
insert into loc_3       SELECT Location,sum(Sale_Qty) as SMLY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @SMLY
							 and Region <> ''
							GROUP BY 1;

insert into loc_4       SELECT Location,sum(Sale_Qty) as TY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @TYSM AND @TYEM
							 and Region <> ''
							GROUP BY 1;

insert into loc_5         SELECT Location,sum(Sale_Qty) as LY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @LYSM AND @LYEM
							 and Region <> ''
							GROUP BY 1;

insert into loc_6            SELECT Location,sum(Sale_Qty) as LLY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @LLYSM AND @LLYEM
							 and Region <> ''
							GROUP BY 1;
insert into loc_7                                    SELECT Location,sum(Sale_Qty) as L12M
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @SMLY AND @TM
							 and Region <> ''
							GROUP BY 1  ;    
insert into loc_8           SELECT Location,sum(Sale_Qty) as L24M
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @SMLLY AND @TM
							 and Region <> ''
							GROUP BY 1;

INSERT INTO Dashboard_QSR_loc_Temp
(
`Measure`,
`Location`,
`Bill_Year_Month`,
`TM`,
`LM`,
`SMLY`,
`TY`,
`LY`,
`LLY`,
`L12M`,
`L24M`
)			
WITH GROUP1 AS
                            (
							SELECT * from loc_1
                            )
                             ,
                             GROUP2 AS
							(
                             select * from loc_2
                            )
                             ,
                             GROUP3 AS
							(
                            select * from loc_3
                            )
                             ,
                             GROUP4 AS
							(
                              select * from loc_4
                            )
                             ,
                             GROUP5 AS
							(
                            select * from loc_5
                            )
                            ,
                             GROUP6 AS
							(
                             select * from loc_6
                            )
                            ,
                             GROUP7 AS
							(
                             select * from loc_7
                            )
                            ,
                             GROUP8 AS
							(
                            select * from loc_8
                            )
                            SELECT 'Units' AS Measure, A.X, A.Y, A.Z,B.Y, C.Y,D.Y,E.Y,F.Y,G.Y,H.Y
                            FROM GROUP1 A
                            JOIN GROUP2 B
                            ON A.X=B.X
                            JOIN GROUP3 C
                            ON C.X=B.X
                            JOIN GROUP4 D
                            ON D.X=C.X
                            JOIN GROUP5 E
                            ON E.X=D.X
                            JOIN GROUP6 F
                            ON F.X=E.X
                            JOIN GROUP7 G
                            ON G.X=F.X
                            JOIN GROUP8 H
                            ON H.X=G.X;

UPDATE Dashboard_QSR_loc_Temp
SET CH_LM_PER = (TM-LM)*100/LM
WHERE Bill_Year_Month = @TM
AND Measure = 'Units';

UPDATE Dashboard_QSR_loc_Temp
SET CH_SMLY_PER = (TM-SMLY)*100/SMLY
WHERE Bill_Year_Month = @TM
AND Measure = 'Units';

UPDATE Dashboard_QSR_loc_Temp
SET CH_LY_PER = (TY-LY)*100/LY
WHERE Bill_Year_Month = @TM
AND Measure = 'Units';

UPDATE Dashboard_QSR_loc_Temp
SET CH_LLY_PER = (TY-LLY)*100/LLY
WHERE Bill_Year_Month = @TM
AND Measure = 'Units';

UPDATE Dashboard_QSR_loc_Temp
SET CH_L12M_PER = (L12M-L24M)*100/L24M
WHERE Bill_Year_Month = @TM
AND Measure = 'Units';


truncate loc_1;
truncate loc_2;
truncate loc_3;
truncate loc_4;
truncate loc_5;
truncate loc_6;
truncate loc_7;
truncate loc_8;
truncate loc_9;
insert into loc_1 						SELECT Location,Bill_Year_Month,sum(Sale_Qty)/COUNT(DISTINCT Bill_Header_Id) as TM
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @TM
							 and Region <> ''
							GROUP BY 1;
insert into loc_2        SELECT Location,sum(Sale_Qty)/COUNT(DISTINCT Bill_Header_Id) as LM
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @LM
							 and Region <> ''
							GROUP BY 1;
insert into loc_3                                   SELECT Location,sum(Sale_Qty)/COUNT(DISTINCT Bill_Header_Id) as SMLY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @SMLY
							 and Region <> ''
							GROUP BY 1;

insert into loc_4            SELECT Location,sum(Sale_Qty)/COUNT(DISTINCT Bill_Header_Id) as TY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @TYSM AND @TYEM
							 and Region <> ''
							GROUP BY 1;

insert into loc_5            SELECT Location,sum(Sale_Qty)/COUNT(DISTINCT Bill_Header_Id) as LY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @LYSM AND @LYEM
							 and Region <> ''
							GROUP BY 1;

insert into loc_6          SELECT Location,sum(Sale_Qty)/COUNT(DISTINCT Bill_Header_Id) as LLY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @LLYSM AND @LLYEM
							 and Region <> ''
							GROUP BY 1;
insert into loc_7            SELECT Location,sum(Sale_Qty)/COUNT(DISTINCT Bill_Header_Id) as L12M
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @SMLY AND @TM
							 and Region <> ''
							GROUP BY 1 ;    
insert into loc_8            SELECT Location,sum(Sale_Qty)/COUNT(DISTINCT Bill_Header_Id) as L24M
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @SMLLY AND @TM
							 and Region <> ''
							GROUP BY 1;


INSERT INTO Dashboard_QSR_loc_Temp
(
`Measure`,
`Location`,
`Bill_Year_Month`,
`TM`,
`LM`,
`SMLY`,
`TY`,
`LY`,
`LLY`,
`L12M`,
`L24M`
)			
WITH GROUP1 AS
                            (
							SELECT * from loc_1
                            )
                             ,
                             GROUP2 AS
							(
                             select * from loc_2
                            )
                             ,
                             GROUP3 AS
							(
                            select * from loc_3
                            )
                             ,
                             GROUP4 AS
							(
                              select * from loc_4
                            )
                             ,
                             GROUP5 AS
							(
                            select * from loc_5
                            )
                            ,
                             GROUP6 AS
							(
                             select * from loc_6
                            )
                            ,
                             GROUP7 AS
							(
                             select * from loc_7
                            )
                            ,
                             GROUP8 AS
							(
                            select * from loc_8
                            )
                            SELECT 'BS' AS Measure, A.X, A.Y, A.Z,B.Y, C.Y,D.Y,E.Y,F.Y,G.Y,H.Y
                            FROM GROUP1 A
                            JOIN GROUP2 B
                            ON A.X=B.X
                            JOIN GROUP3 C
                            ON C.X=B.X
                            JOIN GROUP4 D
                            ON D.X=C.X
                            JOIN GROUP5 E
                            ON E.X=D.X
                            JOIN GROUP6 F
                            ON F.X=E.X
                            JOIN GROUP7 G
                            ON G.X=F.X
                            JOIN GROUP8 H
                            ON H.X=G.X;

UPDATE Dashboard_QSR_loc_Temp
SET CH_LM_PER = (TM-LM)*100/LM
WHERE Bill_Year_Month = @TM
AND Measure = 'BS';

UPDATE Dashboard_QSR_loc_Temp
SET CH_SMLY_PER = (TM-SMLY)*100/SMLY
WHERE Bill_Year_Month = @TM
AND Measure = 'BS';

UPDATE Dashboard_QSR_loc_Temp
SET CH_LY_PER = (TY-LY)*100/LY
WHERE Bill_Year_Month = @TM
AND Measure = 'BS';

UPDATE Dashboard_QSR_loc_Temp
SET CH_LLY_PER = (TY-LLY)*100/LLY
WHERE Bill_Year_Month = @TM
AND Measure = 'BS';

UPDATE Dashboard_QSR_loc_Temp
SET CH_L12M_PER = (L12M-L24M)*100/L24M
WHERE Bill_Year_Month = @TM
AND Measure = 'BS';


		SET @TM = F_Month_Diff(@TM,1);
		SET @LM = F_Month_Diff(@TM,1);
		SET @SMLY = F_Month_Diff(@TM,12);
		SET @TY=substr(@TM,1,4);
		SET @LY=substr(@SMLY,1,4);
		SET @LLY = @LY - 1;
		SET @SMLLY = F_Month_Diff(@TM,24);
END LOOP LOOP_6_MONTHS;

END$$
DELIMITER ;
