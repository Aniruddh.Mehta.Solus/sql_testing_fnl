DELIMITER $$
CREATE or replace PROCEDURE `S_dashboard_performance`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_MEASURE text; 
	
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    
    Select IFNULL(max(IF(Config_Name="CALENDAR",Value1,NULL)),max(IF(Config_Name="FISCAL",Value1,NULL))) into @FY_SATRT_DATE
    from UI_Configuration_Global where isActive="true";
    
    Select IFNULL(max(IF(Config_Name="CALENDAR",Value2,NULL)),max(IF(Config_Name="FISCAL",Value2,NULL))) into @FY_END_DATE
    from UI_Configuration_Global where isActive="true";
    
    set @lift_measure_mtd1=0;
    set @lift_revenue_mtd1=0;
    set @lift_measure_ytd1=0;
    set @live_campaigns1=0;
    set @outreaches1=0;
    set @customer_coverage1=0;
    set @engagement_Channel1=0;
    set @personalized1=0;
    set @ctr1=0;
    
    select Value1 into @Incremental_Revenue_Method from UI_Configuration_Global where Config_Name = 'Incremental_Revenue_Method';
    SET @Incremental_Revenue_Method=ifnull(@Incremental_Revenue_Method,'Method2');
    select @Incremental_Revenue_Method;
    IF @Incremental_Revenue_Method = 'Method1'
    THEN
		SET @incr_rev_concat='_By_Day';
        SET @group_cond1='Event_Id,EE_Date';
        SET @mtd_concat='';
	ELSEIF @Incremental_Revenue_Method = 'Method2'
    THEN
		SET @group_cond1='Event_Id,YM';
		SET @incr_rev_concat='';
        SET @mtd_concat='MTD_';
	ELSE
		SET @incr_rev_concat='';
        SET @mtd_concat='';
		SET @incr_Rev = ' round((CASE WHEN SUM(Control_Base) < 0 or SUM(Target_Delivered) = 0 then 0 WHEN (((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*100 < 0 then 0 WHEN SUM(Control_Base)>0 and SUM(Control_Responder)=0 then SUM(Target_Revenue)
					ELSE ifnull((((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*(SUM(Target_Revenue)/SUM(Target_Responder))*(SUM(Target_Delivered)),0) END),0) ';
		SET @group_cond1='YM';
	END IF;
    SELECT @mtd_concat;
   
   SET @sql1=CONCAT('
		CREATE OR REPLACE VIEW CLM_Event_Dashboard_STLT
        as 
		SELECT `CLM_Event_Dashboard`.`Revenue Center`,
			`CLM_Event_Dashboard`.`Segment`,
			`CLM_Event_Dashboard`.`Campaign`,
			`CLM_Event_Dashboard`.`EE_Date`,
			`CLM_Event_Dashboard`.`Event_Execution_Date_ID`,
			`CLM_Event_Dashboard`.`Theme`,
			`CLM_Event_Dashboard`.`Offer`,
			`CLM_Event_Dashboard`.`Channel`,
			`CLM_Event_Dashboard`.`Has_Recommendations`,
			`CLM_Event_Dashboard`.`Event_ID`,
			`CLM_Event_Dashboard`.`Control_Base` as `Control_Base`,
			`CLM_Event_Dashboard`.`Target_Base`,
			`CLM_Event_Dashboard`.`Target_Delivered`,
			`CLM_Event_Dashboard`.`Control_Responder` as `Control_Responder`,
			`CLM_Event_Dashboard`.`Target_Responder`,
			`CLM_Event_Dashboard`.`Target_Revenue`,
			`CLM_Event_Dashboard`.`Target_Discount`,
			`CLM_Event_Dashboard`.`Target_Bills`,
			`CLM_Event_Dashboard`.`Target_Open`,
			`CLM_Event_Dashboard`.`Target_CTA`,
			`CLM_Event_Dashboard`.`Control_Revenue`,
			`CLM_Event_Dashboard`.`Control_Discount`,
			`CLM_Event_Dashboard`.`Control_Bills`,
			`CLM_Event_Dashboard`.`Created_Date`,
			`CLM_Event_Dashboard`.`Total_Customer_Base`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Day`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Month`,
			`CLM_Event_Dashboard`.`YM`,
			`CLM_Event_Dashboard`.`Incremental_Revenue',@incr_rev_concat,'` AS `Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue',@incr_rev_concat,'` AS `MTD_Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Segment',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Segment`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Revenue_Center',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Revenue_Center`
		FROM `CLM_Event_Dashboard`;
        ');
        select @sql1;
		PREPARE statement from @sql1;
		Execute statement;
		Deallocate PREPARE statement;
 
 
    
   SET @create_view=CONCAT('CREATE OR REPLACE VIEW V_Dashboard_Month_Report_Temp
   as
   select
   Theme,
   YM,
   EE_Date,
   Event_Id,
   SUM(`Target_Base`) AS Target_Base,
   SUM(`Target_Delivered`) AS Target_Delivered,
   SUM(`Control_Base`) AS Control_Base,
   SUM(`Target_Responder`) AS Target_Responder,
   SUM(`Control_Responder`) AS Control_Responder,
   SUM(Target_Revenue) AS Target_Revenue,
   MAX(MTD_Incremental_Revenue) AS Incremental_Revenue
   from CLM_Event_Dashboard_STLT
   group by ',@group_cond1,'');
   
    select @create_view;
	PREPARE statement from @create_view;
	Execute statement;
	Deallocate PREPARE statement;
    
   	select round(ifnull(SUM(Incremental_Revenue),0),1),
    
    round((ifnull(SUM(Incremental_Revenue),0)/SUM(Target_Revenue))*100,1)
    into @lift_measure_mtd1,@lift_revenue_mtd1
    from V_Dashboard_Month_Report_Temp where `YM`=IN_FILTER_CURRENTMONTH GROUP BY `YM`;
    
       select round(ifnull(SUM(Incremental_Revenue),0),1)
	into @lift_measure_ytd1
    from V_Dashboard_Month_Report_Temp where 
    
    YM between @FY_SATRT_DATE and IN_FILTER_CURRENTMONTH and 
    (YM between @FY_SATRT_DATE and @FY_END_DATE);
    
    
	
    if (@lift_measure_ytd1 is null OR @lift_measure_ytd1='') then
      SET @lift_measure_ytd1='0';  
      end if; 
	if (@lift_measure_mtd1 is null OR @lift_measure_mtd1='') then
      SET @lift_measure_mtd1='0';  
      end if; 
   
   select @lift_measure_mtd1;
	SET @lift_measure_ytd=@lift_measure_ytd1;
    SET @lift_measure_mtd=@lift_measure_mtd1;
    
    
    select  @lift_measure_ytd ;
     select  @lift_measure_mtd ;    
    
   
        
        set @view_stmt1=concat('create or replace view V_Dashboard_Performance_Top_Card as 
	    (select "Lift Generated" as `Card_Name`,case when `Number_Format`="Cr" then  
        IF(`Number_Separator`="true",convert(format((ifnull("',@lift_measure_ytd,'",0)/10000000),
        `Decimal_Position`),CHAR),convert(round((ifnull("',@lift_measure_ytd,'",0)/10000000),`Decimal_Position`),CHAR))
        when `Number_Format`="L" then IF(`Number_Separator`="true",convert(format((ifnull("',@lift_measure_ytd,'",0)/100000),
        `Decimal_Position`),CHAR),convert(round((ifnull("',@lift_measure_ytd,'",0)/100000),`Decimal_Position`),CHAR))
        when `Number_Format`="K" then 
        IF(`Number_Separator`="true",convert(format((ifnull("',@lift_measure_ytd,'",0)/1000),
        `Decimal_Position`),CHAR),convert(round((ifnull("',@lift_measure_ytd,'",0)/1000),`Decimal_Position`),CHAR))
         when `Number_Format`="Mn" then 
          IF(`Number_Separator`="true",convert(format((ifnull("',@lift_measure_ytd,'",0)/1000000),
        `Decimal_Position`),CHAR),convert(round((ifnull("',@lift_measure_ytd,'",0)/1000000),`Decimal_Position`),CHAR))
        else
            IF(`Number_Separator`="true",convert(format("',@lift_measure_ytd,'",`Decimal_Position`),CHAR),convert(round("',@lift_measure_ytd,'",`Decimal_Position`),CHAR))
         end  as `Value1`, "Year to Date" as `Value2`, 
        `Currency` as `Value3`,ifnull(`Number_Format`,"") as `Value4`,`Measure_Name` as `Measure`,
        `Information_Text` as `Info` from UI_Configuration  WHERE  `Measure_Name`= "LIFTYTD")        
      	Union
        
         (select "Lift Generated" as `Card_Name`,case when `Number_Format`="Cr" then  
        IF(`Number_Separator`="true",convert(format((ifnull("',@lift_measure_mtd,'",0)/10000000),
        `Decimal_Position`),CHAR),convert(round((ifnull("',@lift_measure_mtd,'",0)/10000000),`Decimal_Position`),CHAR))
        when `Number_Format`="L" then IF(`Number_Separator`="true",convert(format((ifnull("',@lift_measure_mtd,'",0)/100000),
        `Decimal_Position`),CHAR),convert(round((ifnull("',@lift_measure_mtd,'",0)/100000),`Decimal_Position`),CHAR))
        when `Number_Format`="K" then 
        IF(`Number_Separator`="true",convert(format((ifnull("',@lift_measure_mtd,'",0)/1000),
        `Decimal_Position`),CHAR),convert(round((ifnull("',@lift_measure_mtd,'",0)/1000),`Decimal_Position`),CHAR))
         when `Number_Format`="Mn" then 
          IF(`Number_Separator`="true",convert(format((ifnull("',@lift_measure_mtd,'",0)/1000000),
        `Decimal_Position`),CHAR),convert(round((ifnull("',@lift_measure_mtd,'",0)/1000000),`Decimal_Position`),CHAR))
        else
            IF(`Number_Separator`="true",convert(format("',@lift_measure_mtd,'",`Decimal_Position`),CHAR),convert(round("',@lift_measure_ytd,'",`Decimal_Position`),CHAR))
         end  as `Value1`, "Month to Date" as `Value2`, 
        `Currency` as `Value3`,ifnull(`Number_Format`,"") as `Value4`,`Measure_Name` as `Measure`,
        `Information_Text` as `Info` from UI_Configuration  WHERE  `Measure_Name`= "LIFTMTD") 
        
        ;'
        );
        
          
  	
	select @view_stmt1;
	PREPARE statement1 from @view_stmt1;
	Execute statement1;
	Deallocate PREPARE statement1;
    
	SET @Query_String1 =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Card_Name",`Card_Name`,"Value1",`Value1`,"Value2",`Value2`,"Value3",`Value3`,"Value4",`Value4`,"Measure",`Measure`,"Info",`Info`) )),"]")into @temp_result_top_card from V_Dashboard_Performance_Top_Card;' ;
    select @Query_String1;
	PREPARE statement from @Query_String1;
	Execute statement;
	Deallocate PREPARE statement; 
    
    
	
    
    
	select count(distinct a.`Campaign`),SUM(TARGET_BASE),
    ifnull(round((a.`Total_Distinct_Customers_Targeted_This_Month`/a.`Total_Customer_Base`)*100,1),0),
    count( distinct a.`Channel`)
	into @live_campaigns1,@outreaches1,@customer_coverage1,@engagement_Channel1
    from CLM_Event_Dashboard a where a.`YM`= IN_FILTER_CURRENTMONTH and  a.`Channel`<>'NA' GROUP BY a.`YM`;
    
     select
     ifnull(round((SUM(`TARGET_CTA`)/SUM(`Target_Base`))*100,1),0)
       
     into @ctr1
    from `CLM_Event_Dashboard` a where a.`YM`= IN_FILTER_CURRENTMONTH and a.`Channel`= (select `Channel` from UI_Configuration where `Measure_Name`='CTR' )
    GROUP BY a.`YM` ;
    
    
    SELECT 
      round((SUM(CASE WHEN `a`.`Personalization` >0 THEN `a`.`Outreaches` ELSE 0 END)/SUM(`a`.`Outreaches`))*100,1) into @personalized1
	 FROM
      `V_Dashboard_Performance_Campaign` `a` where `a`.`YM`=IN_FILTER_CURRENTMONTH
    GROUP BY `a`.`YM`;
	 
       
    
   select @live_campaigns1;
    if (@live_campaigns1 is null OR @live_campaigns1='') then
      SET @live_campaigns1='0';  
      end if; 
    SET  @live_campaigns= @live_campaigns1;	
   
    select  
     CASE 
     when @live_campaigns >= b.`Threshold2` THEN 3
     when @live_campaigns < b.`Threshold2` AND 
     @live_campaigns > b.`Threshold1` THEN 2
     ELSE 1 END into @live_campaigns_threshold1 from UI_Configuration b WHERE  b.`Measure_Name`= 'CAMPAIGNS';
       
     if (@live_campaigns_threshold1 is null OR @live_campaigns_threshold1='') then
      SET @live_campaigns_threshold1='1';
    
   
      end if;
       SET @live_campaigns_threshold= @live_campaigns_threshold1;
    
   
    
    
   
     select   
     CASE WHEN b.`Number_Format`="L" 
     THEN concat(IF(`Number_Separator`="true",convert(format((ifnull(@outreaches1,0)/100000),`Decimal_Position`),CHAR),
     convert(round((ifnull(@outreaches1,0)/100000),`Decimal_Position`),CHAR)),' L')
     WHEN b.`Number_Format`="Cr."
       THEN concat(IF(`Number_Separator`="true",convert(format((ifnull(@outreaches1,0)/10000000),`Decimal_Position`),CHAR),
     convert(round((ifnull(@outreaches1,0)/10000000),`Decimal_Position`),CHAR)),' Cr.')
       WHEN b.`Number_Format`="Mn"
       THEN concat(IF(`Number_Separator`="true",convert(format((ifnull(@outreaches1,0)/1000000),`Decimal_Position`),CHAR),
     convert(round((ifnull(@outreaches1,0)/1000000),`Decimal_Position`),CHAR)),' Mn')
     WHEN b.`Number_Format`="K"
       THEN concat(IF(`Number_Separator`="true",convert(format((ifnull(@outreaches1,0)/1000),`Decimal_Position`),CHAR),
     convert(round((ifnull(@outreaches1,0)/1000),`Decimal_Position`),CHAR)),' K')
     ELSE IF(`Number_Separator`="true",convert(format(@outreaches1,`Decimal_Position`),CHAR),
     convert(round(@outreaches1,`Decimal_Position`),CHAR))
     END,
     CASE 
     when @outreaches1 >= b.`Threshold2` THEN 3
     when @outreaches1 < b.`Threshold2` AND 
     @outreaches1 > b.`Threshold1` THEN 2
     ELSE 1 END into  @outreaches1,@outreaches_threshold1 from UI_Configuration b WHERE  b.`Measure_Name`= 'OUTREACH';
  
    select @outreaches1;
   
    
    SET @outreaches_threshold=@outreaches_threshold1;
    select @outreaches_threshold;
    
     if (@outreaches1 is null OR @outreaches1='') then
      SET @outreaches1='1';
     end if;
      SET @outreaches=@outreaches1;
	
    
    
    
	select @customer_coverage1;
    if (@customer_coverage1 is null OR @customer_coverage1='') then
      SET @customer_coverage1='0';  
      end if; 
    SET  @customer_coverage= @customer_coverage1;	
    select 
     CASE 
     when @customer_coverage >= b.`Threshold2` THEN 3
     when @customer_coverage < b.`Threshold2` AND 
     @customer_coverage > b.`Threshold1` THEN 2
     ELSE 1 END into @customer_coverage_threshold1 from UI_Configuration b WHERE  b.`Measure_Name`= 'CUSTCOVERAGE';
    
     if (@customer_coverage_threshold1 is null OR @customer_coverage_threshold1='') then
      SET @customer_coverage_threshold1='1';
     end if;
      SET @customer_coverage_threshold=@customer_coverage_threshold1;
    select @customer_coverage_threshold;
    
    
    
    
    
   select @personalized1;
   if (@personalized1 is null OR @personalized1='') then
      SET @personalized1='0';  
      end if; 
    SET  @personalized= @personalized1;	
   
    select  
     CASE 
     when @personalized >= b.`Threshold2` THEN 3
     when @personalized < b.`Threshold2` AND 
     @personalized > b.`Threshold1` THEN 2
     ELSE 1 END into @personalized_threshold1 from UI_Configuration b WHERE  b.`Measure_Name`= 'PERSONALIZED';
       
     if (@personalized_threshold1 is null OR @personalized_threshold1='') then
      SET @personalized_threshold1='1';
    
   
      end if;
       SET @personalized_threshold= @personalized_threshold1;   
    
    
   select @engagement_Channel1;
    if (@engagement_Channel1 is null OR @engagement_Channel1='') then
      SET @engagement_Channel1='0';  
      end if; 
    SET  @engagement_Channel= @engagement_Channel1;	
   
    select  
    CASE 
     when @engagement_Channel >= b.`Threshold2` THEN 3
     when @engagement_Channel < b.`Threshold2` AND 
     @engagement_Channel > b.`Threshold1` THEN 2
     ELSE 1 END into @engagement_Channel_threshold1 from UI_Configuration b WHERE  b.`Measure_Name`= 'ENGAGEMENTCHANNEL';
       
     if (@engagement_Channel_threshold1 is null OR @engagement_Channel_threshold1='') then
      SET @engagement_Channel_threshold1='1';
    
   
      end if;
       SET @engagement_Channel_threshold= @engagement_Channel_threshold1;
       
    
    
    
    
   select @ctr1;
    if (@ctr1 is null OR @ctr1='') then
      SET @ctr1='0';  
      end if; 
    SET  @ctr= @ctr1;
	
  
    select  
     CASE 
     when @ctr >= b.`Threshold2` THEN 3
     when @ctr < b.`Threshold2` AND 
     @ctr > b.`Threshold1` THEN 2
     ELSE 1 END into @ctr_threshold1 from UI_Configuration b WHERE  b.`Measure_Name`= 'CTA';
       
     if (@ctr_threshold1 is null OR @ctr_threshold1='') then
      SET @ctr_threshold1='1';
    
   
      end if;
       SET @ctr_threshold= @ctr_threshold1;
       
	
    
    
    
   select @lift_revenue_mtd1;
    if (@lift_revenue_mtd1 is null OR @lift_revenue_mtd1='' OR @lift_revenue_mtd1<0) then
      SET @lift_revenue_mtd1='0';  
      end if; 
    SET  @lift_revenue_mtd= @lift_revenue_mtd1;	
   
    select 
     CASE 
     when @lift_revenue_mtd >= b.`Threshold2` THEN 3
     when @lift_revenue_mtd < b.`Threshold2` AND 
     @lift_revenue_mtd > b.`Threshold1` THEN 2
     ELSE 1 END into @lift_revenue_mtd_threshold1 from UI_Configuration b WHERE  b.`Measure_Name`= 'LIFT';
       
     if (@lift_revenue_mtd_threshold1 is null OR @lift_revenue_mtd_threshold1='') then
      SET @lift_revenue_mtd_threshold1='1';
    
   
      end if;
       SET @lift_revenue_mtd_threshold= @lift_revenue_mtd_threshold1;
       
      
    
    
	
    
    
    
	set @view_stmt2=concat('create or replace view V_Dashboard_Performance_Bottom_Card as 
		(select "Active Campaigns" as `Card_Name`,"',@live_campaigns,'" as `Value1`, "',@live_campaigns_threshold ,'" as `Value2`, "" as `Value3`,`Measure_Name` as `Measure`,`Information_Text` as `Info` from UI_Configuration  WHERE  `Measure_Name`= "CAMPAIGNS" and `Preference`="true")
		Union
        (select "Outreaches" as `Card_Name`,"',@outreaches,'" as `Value1`, "',@outreaches_threshold ,'" as `Value2`, "" as `Value3`,`Measure_Name` as `Measure` ,`Information_Text` as `Info` from UI_Configuration  WHERE  `Measure_Name`= "OUTREACH" and `Preference`="true")
         Union
        (select "Customer Coverage" as `Card_Name`,concat("',@customer_coverage,'"," %") as `Value1`, "',@customer_coverage_threshold ,'" as `Value2`, "" as `Value3`,`Measure_Name` as `Measure`, `Information_Text` as `Info` from UI_Configuration  WHERE  `Measure_Name`= "CUSTCOVERAGE" and `Preference`="true")
          Union
        (select "Personalized" as `Card_Name`,concat("',@personalized,'"," %") as `Value1`, "',@personalized_threshold ,'" as `Value2`, "" as `Value3`,`Measure_Name` as `Measure` ,`Information_Text` as `Info` from UI_Configuration  WHERE  `Measure_Name`= "PERSONALIZED" and `Preference`="true")
		Union
        (select "Engagement Channels" as `Card_Name`,"',@engagement_Channel,'" as `Value1`, "',@engagement_Channel_threshold ,'" as `Value2`, "" as `Value3`,`Measure_Name` as `Measure` ,`Information_Text` as `Info` from UI_Configuration  WHERE  `Measure_Name`= "ENGAGEMENTCHANNEL" and `Preference`="true")
	     Union
        (select "Recommender Accuracy" as `Card_Name`,"NA" as `Value1`, "1" as `Value2`, "" as `Value3`,`Measure_Name` as `Measure` ,`Information_Text` as `Info` from UI_Configuration  WHERE  `Measure_Name`= "ACCURACY" and `Preference`="true")
          Union
          (select "Revenue Lift" as `Card_Name`,concat("',@lift_revenue_mtd,'"," %") as `Value1`, "',@lift_revenue_mtd_threshold ,'" as `Value2`, "" as `Value3`,`Measure_Name` as `Measure` ,`Information_Text` as `Info` from UI_Configuration  WHERE  `Measure_Name`= "LIFT" and `Preference`="true")
		 Union
     
        (select "CTR" as `Card_Name`,concat("',@ctr,'"," %") as `Value1`, "',@ctr_threshold ,'" as `Value2`, "" as `Value3`,`Measure_Name` as `Measure` ,`Information_Text` as `Info` from UI_Configuration  WHERE  `Measure_Name`= "CTR" and `Preference`="true")
     
           ;');
           
           
       
  	   
	select @view_stmt2;
	PREPARE statement2 from @view_stmt2;
	Execute statement2;
	Deallocate PREPARE statement2;
    
	SET @Query_String2 =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Card_Name",`Card_Name`,"Value1",`Value1`,"Value2",`Value2`,"Value3",`Value3`,"Measure",`Measure`,"Info",`Info`) )),"]")into @temp_result_bottom_card from V_Dashboard_Performance_Bottom_Card;' ;
    select @Query_String2;
	PREPARE stm2 from @Query_String2;
	Execute stm2;
	Deallocate PREPARE stm2; 
    
      
	set @view_stmt3= 'CREATE or REPLACE VIEW V_Dashboard_Performance_Text AS (select `content` as `Value1` from `UI_Configuration_Text` where `Text_Type`="Text" order by `Serial_No`);';
    select @view_stmt3;
	PREPARE statement3 from @view_stmt3;
	Execute statement3;
	Deallocate PREPARE statement3;
    
    SET @Query_String3 =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Value1",`Value1`) )),"]")into @temp_result_perf_text from V_Dashboard_Performance_Text;' ;
    select @Query_String3;
	PREPARE stm3 from @Query_String3;
	Execute stm3;
	Deallocate PREPARE stm3; 
    
    SET @Query_String4 =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Value1",`Content`) )),"]")into @temp_result_perf_marquee from UI_Configuration_Text where `Text_Type`="Marquee";' ;
    select @Query_String4;
	PREPARE stm4 from @Query_String4;
	Execute stm4;
	Deallocate PREPARE stm4; 
    
    
     
                 Set @temp_result_final = concat("[",json_object("topcard",@temp_result_top_card,"bottomcard",@temp_result_bottom_card,"textcard",@temp_result_perf_text,"marquee",@temp_result_perf_marquee),"]");
                SELECT @temp_result_final into out_result_json;
      
END$$
DELIMITER ;
