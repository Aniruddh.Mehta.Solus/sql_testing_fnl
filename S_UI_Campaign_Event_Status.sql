DELIMITER $$
CREATE or replace PROCEDURE `S_UI_Campaign_Event_Status`()
Begin
		DECLARE done1,done2 INT DEFAULT FALSE;
		DECLARE Event_ID_cur1_1 ,Event_ID_cur1_2 int;
		Declare Event_Condition_SVOC_1, Event_Condition_Bill_1  , Creative1,State1 text;


		DECLARE cur1 cursor for 
			SELECT 	ID,
					Replaced_Query, 
		            Replaced_Query_Bill,
		           State
			From  Event_Master
			ORDER BY Id;
		
	

		DECLARE CONTINUE HANDLER FOR NOT FOUND SET done1 = TRUE; 
		SET done1 = FALSE;
		SET SQL_SAFE_UPDATES=0;
		SET foreign_key_checks=0;

		
		SET done1 = FALSE;
		
		Select CONCAT("Inserting the Events into Event_Master_UI");
	
		OPEN cur1;
		BEGIN
		LOOP_ALL_EVENTS: LOOP

			FETCH cur1 into 
			  Event_ID_cur1_1, 
			  Event_Condition_SVOC_1,  
			  Event_Condition_Bill_1,State1;

				SET @Event_ID_cur1=Event_ID_cur1_1;
			    SET @Event_Condition_SVOC=Event_Condition_SVOC_1;
			    SET @Event_Condition_Bill=Event_Condition_Bill_1;
				

				IF done1 THEN
				   LEAVE LOOP_ALL_EVENTS;
				END IF; 
							Select @Event_ID_cur1;
							
							Insert into CDM_PROCESS_LOG (Proc_Name,Step_Id,Step_Desc,Step_Status)
							Value ("Migration_Of_The_Events","1",Concat("Inserting the Event_Id in Event_Master_UI For Id ",@Event_ID_cur1),"1");

							SET @SQL1= concat('
							Insert Ignore Event_Master_UI (Event_Id)
							 Select ',@Event_ID_cur1,'');

							SELECT @SQL1;
				            PREPARE stmt1 from @SQL1;
							EXECUTE stmt1;
							DEALLOCATE PREPARE stmt1;
							
							Insert into CDM_PROCESS_LOG (Proc_Name,Step_Id,Step_Desc,Step_Status)
							Value ("Migration_Of_The_Events","2",concat("Updating the EventSegmentName in Event_Master_UI For Id ",@Event_ID_cur1),"1");
							
							update Event_Master_UI A,Event_Master B
															Set EventSegmentName=CLMSegmentName
															where A.Event_Id=B.Id and Event_Id=@Event_ID_cur1;
							
							Insert into CDM_PROCESS_LOG (Proc_Name,Step_Id,Step_Desc,Step_Status)
							Value ("Migration_Of_The_Events","3",Concat("Updating the Message in Event_Master_UI For Id ",@Event_ID_cur1),"1");
							
							SET @SQL1= concat('Update Event_Master_UI A ,Event_Master B
													  set Message = Creative1 
												where A.Event_Id=B.Id and 	B.Id=',@Event_ID_cur1,' and A.Event_Id=',@Event_ID_cur1,'
													');
							
							SELECT @SQL1;
				            PREPARE stmt1 from @SQL1;
							EXECUTE stmt1;
							DEALLOCATE PREPARE stmt1;
							
							
							
						
							
							
							Insert into CDM_PROCESS_LOG (Proc_Name,Step_Id,Step_Desc,Step_Status)
							Value ("Migration_Of_The_Events","4",Concat("Updating the CTState in CLM_Campaign_Trigger For Event Id ",@Event_ID_cur1),"1");
							
							SET @Query_String =  concat('Update Event_Master_UI A,CLM_Campaign_Trigger B,CLM_Campaign_Events C
																Set  B.`CTState` = 3
														where C.CampTriggerId =B.CampTriggerId
																and A.Event_Id=C.EventId and EventId=',@Event_ID_cur1,'
														and  B.CTState = 2 ;') ; 
																	
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;
							
							Insert into CDM_PROCESS_LOG (Proc_Name,Step_Id,Step_Desc,Step_Status)
							Value ("Migration_Of_The_Events","5",Concat("Updating the Status related to Enable State in Event_Master_UI For Id ",@Event_ID_cur1),"1");
							
							SET @Query_String =  concat('Update Event_Master_UI A,Event_Master B
																Set Status_Tested ="TESTED",
																Status_QC ="QC",
																Status_DLT ="DLT",
																Status_Schedule="Schedule",
																Status_Active="Active",
																Trigger_Type= "GTM_Advance"
														where A.Event_Id=B.Id and Event_Id=',@Event_ID_cur1,'
														and B.State="Enabled" ;') ; 
																	
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;
							
							
							Insert into CDM_PROCESS_LOG (Proc_Name,Step_Id,Step_Desc,Step_Status)
							Value ("Migration_Of_The_Events","6",Concat("Updating the Status related to Disabled State in Event_Master_UI For Id ",@Event_ID_cur1),"1");
							
							SET @Query_String =  concat('Update Event_Master_UI A,Event_Master B
																Set Status_Tested ="TESTED",
																Status_QC ="QC",
																Status_DLT ="DLT",
																Status_Schedule="Schedule",
																Status_Active="InActive"
														where A.Event_Id=B.Id and Event_Id=',@Event_ID_cur1,'
														and B.State="Disabled" ;') ; 
																	
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;
							
							
							Insert into CDM_PROCESS_LOG (Proc_Name,Step_Id,Step_Desc,Step_Status)
							Value ("Migration_Of_The_Events","7",Concat("Updating the Trigger_Type in Event_Master_UI For Id ",@Event_ID_cur1),"1");
							
							SET @Query_String =  concat('Update Event_Master_UI A
																Set Trigger_Type= "GTM_Advance"
														where A.Event_Id=',@Event_ID_cur1,'
														 ;') ; 
																	
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;
							
							
							
						SET SQL_SAFE_UPDATES=0;
						Update Event_Master_UI A,CLM_Campaign_Events B,Event_Master C
						Set Scheduled_at = Concat(" From: ",date_format(Start_Date_ID,'%m-%d')," To: ",date_format(End_Date_ID,'%m-%d')," At: ",case when B.Event_Sent_Time is null then "NA" else B.Event_Sent_Time end )
						where A.Event_Id=B.EventId and C.Id=B.EventId;

		    
	            END LOOP LOOP_ALL_EVENTS;

        END;	
    	close cur1;
		
    	
end$$
DELIMITER ;
