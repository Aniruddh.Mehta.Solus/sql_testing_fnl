DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_metrics_automation`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
    declare IN_COMPONENT varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
	DECLARE Loop_Check_Var Int Default 0;
    DECLARE V_Screen_Name varchar(50);
	DECLARE V_Measure_Name Varchar(50);
    DECLARE V_Component_Type varchar(50);
    
    DECLARE cur_automation
    CURSOR For SELECT IN_SCREEN, IN_MEASURE, IN_COMPONENT;
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET Loop_Check_Var=1;
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    SET IN_COMPONENT = json_unquote(json_extract(in_request_json,"$.COMPONENT"));
    
    CREATE OR REPLACE TABLE `Dashboard_Response_Temp` (
		  `Screen_Name` varchar(100),
		  `Measure_Name` varchar(50),
		  `Graph_Header_Name` varchar(50),
		  `Component_Type` varchar(50),
		  `Xaxis` varchar(50),
		  `Yaxis1` varchar(50),
		  `Yaxis2` varchar(50),
		  `Yaxis3` varchar(50)
	) ENGINE=InnoDB DEFAULT CHARSET=latin1;
    
   
   SET Loop_Check_Var=0;
		OPEN cur_automation;
		LOOP_ALL_METRICS : Loop
			FETCH cur_automation into V_Screen_Name,V_Measure_Name,V_Component_Type;
			IF Loop_Check_Var THEN    
				LEAVE LOOP_ALL_METRICS;
			END IF;
            
            -- To load data to Temp table
            CALL S_dashboard_metrics_automation_loaddata(IN_SCREEN,IN_MEASURE,IN_COMPONENT,IN_FILTER_CURRENTMONTH);
            
            -- To read the data from Temp table and send output json
            CALL S_dashboard_metrics_automation_appendjson(IN_SCREEN,IN_MEASURE,IN_COMPONENT,out_result_json);
    
    END LOOP LOOP_ALL_METRICS;
		CLOSE cur_automation;
    
    IF IN_SCREEN <> "DOWNLOAD"
    THEN
		select @Query_String;
		PREPARE statement from @Query_String;
		Execute statement;
		Deallocate PREPARE statement; 
		SET out_result_json = @temp_result;
	ELSE
		call S_dashboard_performance_download('V_Dashboard_Month_Report',@out_result_json1);
		select @out_result_json1 into out_result_json;
    END IF;
    
    DROP TABLE Dashboard_Response_Temp;
    
END$$
DELIMITER ;
