
CREATE or replace PROCEDURE `S_Threshold_Values`()
BEGIN


drop table if exists Threshold;

CREATE TABLE `Threshold` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) DEFAULT NULL,
  `Value` char(200) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Threshold_uniqe` (`Name`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

insert into Threshold(Name) values
("FTR_RECENCY"),
("ACTIVE_PERIOD"),
("AHF_RECENCY"),
("ALF_RECENCY"),
("LASPER_RECENCY");

set sql_safe_updates = 0;

update Threshold 
set Value = (
select case when (
select (round(day_gap/30)*30) as Active_Period from (
select day_gap, PERCENT_RANK() over (order by day_gap) as Gap from (
Select Customer_id, (DATEDIFF(b.Bill_Date,b.prevorder)) as day_gap from
(
select Customer_Id,Bill_Date,prevorder from 
(
Select Customer_Id,Bill_Date ,
LAG(Bill_Date) over (Partition by Customer_Id order by Bill_Date ) as prevorder
from CDM_Bill_Details 
) a
 where a.prevorder is not NULL
 group by Customer_Id
) b
order by day_gap) d
) e
where Gap > 0.80 limit 1) < 25
then 30
else (
select (round(day_gap/30)*30) as Active_Period from (
select day_gap, PERCENT_RANK() over (order by day_gap) as Gap from (
Select Customer_id, (DATEDIFF(b.Bill_Date,b.prevorder)) as day_gap from
(
select Customer_Id,Bill_Date,prevorder from 
(
Select Customer_Id,Bill_Date ,
LAG(Bill_Date) over (Partition by Customer_Id order by Bill_Date ) as prevorder
from CDM_Bill_Details 
) a
 where a.prevorder is not NULL
 group by Customer_Id
) b
order by day_gap) d
) e
where Gap > 0.80 limit 1)
end

)
where Name = 'ACTIVE_PERIOD';

update Threshold
set Value = (
select (case when 0.25*Value < 30 then (round((0.25*Value)/5)*5) else 30 end)   from Threshold where Name = 'ACTIVE_PERIOD')
where Name = 'FTR_RECENCY';


update Threshold 
set Value = (
select day_gap  from (
select day_gap, PERCENT_RANK() over (order by day_gap) as Gap from (
Select Customer_id, (DATEDIFF(b.Bill_Date,b.prevorder)) as day_gap from
(
select Customer_Id,Bill_Date,prevorder from 
(
Select Customer_Id,Bill_Date ,
LAG(Bill_Date) over (Partition by Customer_Id order by Bill_Date ) as prevorder
from CDM_Bill_Details 
) a
 where a.prevorder is not NULL
 group by Customer_Id
) b
order by day_gap) d
) e
where Gap > 0.10 limit 1)
where Name = 'AHF_RECENCY';


update Threshold
set Value = (select 2*Value from Threshold where Name = 'ACTIVE_PERIOD')
where Name = 'LASPER_RECENCY';

set sql_safe_updates = 0;

update svoc_config
set value = (select Value from Threshold where Name = 'ACTIVE_PERIOD')
where name = 'active_period';

set sql_safe_updates=0;
delete  from CLM_Segment;


set @FTR= (select Value from Threshold where Name = 'FTR_RECENCY');

set @AP= (select Value from Threshold where Name = 'ACTIVE_PERIOD');

alter table CLM_Segment
add column if not exists Sequence int;

insert into CLM_Segment (CLMSegmentId, CLMSegmentName,CLMSegmentReplacedQueryAsInUI, CLMSegmentReplacedQuery, IsValidCondition,CreatedBy,ModifiedBy,Sequence)
values(1,'LEADS','Lt_Num_of_Visits = 0','Lt_Num_of_Visits = 0',1,1,1,1),
(2,'FTR',concat('Lt_Num_of_Visits = 1 and Recency < ',@FTR),concat('Lt_Num_of_Visits = 1 and Recency < ',@FTR),1,1,1,2),
(3,'AHF',concat('Lt_Num_of_Visits >= 4 and Recency < ',round(0.75*@AP)),concat('Lt_Num_of_Visits >= 4 and Recency < ',round(0.75*@AP)) ,1,1,1,3),
(4,'ALF',concat('Lt_Num_of_Visits < 4 and Recency > ',@FTR,' and Recency < ',round(0.75*@AP)),concat('Lt_Num_of_Visits < 4 and Recency > ',@FTR,' and Recency < ',round(0.75*@AP)),1,1,1,4),
(5,'Active',concat('Recency > ',@FTR,' and Recency < ',round(0.75*@AP)),concat('Recency > ',@FTR,' and Recency < ',round(0.75*@AP)),1,1,1,5),
(6,'OTB',concat('Recency >= ',round(0.75*@AP),' and Recency < ',@AP),concat('Recency >= ',round(0.75*@AP),' and Recency < ',@AP),1,1,1,6),
(7,'Inactive',concat('Recency >= ',@AP,' and Recency < ',2*@AP),concat(' Recency >= ',@AP,' and Recency < ',2*@AP),1,1,1,7),
(8,'Lapsed',concat('Recency >= ',2*@AP),concat('Recency >= ',2*@AP),1,1,1,8),
(9,'All','1=1','1=1',1,1,1,9);



END


