DELIMITER $$
CREATE or replace PROCEDURE `PROC_CLM_IS_VALID_SVOC_CONDITION`(
	IN vSQLCondition TEXT,
    IN vBillCondition TEXT,
    In vSegmentid  VARCHAR(128),-- Changed  
    IN vCheckValidityOnly TINYINT,
    OUT vSQLCondition_Segment TEXT,
	OUT vSuccess Varchar(128)
)
BEGIN
	DECLARE vSqlstmt TEXT;
	DECLARE vErrMsg TEXT;
    
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN 
		SET vSuccess = 0; 
		GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
		SET vErrMsg = CONCAT(IFNULL(vErrMsg,''), "ERROR ", @errno, " (", @sqlstate, "): ", @text);
        SET vSuccess=concat('No Customers',' ',@text);
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 0, vErrMsg, 0);
	END;
    IF vSQLCondition like '%select %' OR vBillCondition like '%select % ' 
    THEN 
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR : Subquery is not allowed!'; 
    END IF;
	SET vErrMsg = 'Start';
	SET vSuccess = 0;
	SET @CURR_PROC_NAME = 'PROC_CLM_IS_VALID_SVOC_CONDITION';
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 1, vErrMsg, vSuccess);
    
    
    IF vBillCondition like '%not in%' THEN
    set vBillCondition=replace(vBillCondition,'not in','in');
    set @exists_condn='not exists';
    else 
	set @exists_condn='exists';
    END IF;
    
			Set @Query_String= concat('Select Concat("(",Replace(group_concat("(",CLMSegmentReplacedQuery,")"),","," or "),")") into @CLMSegmentReplacedQuery 
	from CLM_Segment where CLMSegmentId in (',vSegmentid,');
  ;') ;
											   
													SELECT @Query_String;
													PREPARE statement from @Query_String;
													Execute statement;
													Deallocate PREPARE statement; 
				-- Changed  
    select vSegmentid,@CLMSegmentReplacedQuery;
    
    
    IF vSegmentid=0 OR vSegmentid ='' OR vSegmentid IS NULL
    THEN 
    SELECT 1;
    ELSE    
    set vSQLCondition= concat(vSQLCondition,' and ', @CLMSegmentReplacedQuery) ;
    END IF;
    select vSQLCondition;
    
    
    
    SET vSQLCondition_Segment = vSQLCondition;
	IF vCheckValidityOnly = 1 THEN
		IF (vSQLCondition IS NOT NULL) AND (vSQLCondition <> '') THEN
			SET vSqlstmt = CONCAT('SELECT Customer_Id INTO @Temp FROM V_CLM_Customer_One_View WHERE ',vSQLCondition, ' LIMIT 1');
			
		ELSE
			SET vSqlstmt = CONCAT('SELECT Customer_Id INTO @Temp FROM V_CLM_Customer_One_View LIMIT 1');
			
		END IF;

		SET @sql_stmt = vSqlstmt;
        select @sql_stmt;
		PREPARE stmt FROM @sql_stmt;
		EXECUTE stmt; 
		DEALLOCATE PREPARE stmt;
        
        
		
		SET vSuccess = 1;
	ELSE
		IF (vSQLCondition IS NOT NULL) AND (vSQLCondition <> '') THEN
            IF (vBillCondition is not NULL) and (vBillCondition <> '')
               THEN 
			SET @Bill_Cond=CONCAT('',@exists_condn,'(SELECT 1 from V_CLM_Bill_Detail_One_View svot 
			WHERE  svoc.Customer_id=svot.Customer_Id and ',vBillCondition,')'); 
            SET vSqlstmt = CONCAT('Select count(1) INTO @Cust_Cnt from (SELECT Customer_id
			FROM V_CLM_Customer_One_View svoc 
			where ',vSQLCondition,'
            and ',@Bill_Cond,')F'); 
            SET vSQLCondition_Segment=concat(vSQLCondition,' and ',@Bill_Cond);
            ELSE
			SET vSqlstmt = CONCAT('SELECT COUNT(1) INTO @Cust_Cnt FROM V_CLM_Customer_One_View WHERE ',vSQLCondition);
            END IF;
		ELSE
			SET vSqlstmt = 'SELECT COUNT(1) INTO  @Cust_Cnt FROM V_CLM_Customer_One_View';
        END IF;
		SET @sql_stmt = vSqlstmt;
        select @sql_stmt;
		PREPARE stmt FROM @sql_stmt;
		EXECUTE stmt; 
		DEALLOCATE PREPARE stmt;
        
		SET vSuccess = @Cust_Cnt;
	END IF;
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 2, vErrMsg, vSuccess);
   
END$$
DELIMITER ;
