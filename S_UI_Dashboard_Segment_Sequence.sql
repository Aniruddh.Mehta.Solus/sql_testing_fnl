DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_UI_Dashboard_Segment_Sequence`(IN in_request_json json, OUT out_result_json json)
BEGIN
    

    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
    declare EDIT Bigint; 
    DECLARE IN_ACTION TEXT;

	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare IN_AGGREGATION_4 text;
	declare IN_AGGREGATION_5 text;
    declare IN_NAME text;
    declare IN_SEQUENCE INT;
    declare IN_SEG_NAME TEXT;
    declare IN_SEG_QUERY TEXT;
    declare IN_SEG_OLD_CONDITION TEXT;
    declare IN_SEG_ID INT;
    declare IN_SEG_CHANGE_CONDITION TEXT;
    declare IN_SEG_CHANGE_NAME TEXT;
	declare InUse text;
    
	
	SET SQL_SAFE_UPDATES=0;
  
   
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_ACTION = json_unquote(json_extract(in_request_json,"$.ACTION"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1")); 
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_AGGREGATION_4 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_4"));
    SET IN_NAME = json_unquote(json_extract(in_request_json,"$.UPDATED_BAND_NAME"));
    SET IN_SEQUENCE = json_unquote(json_extract(in_request_json,"$.SEQUENCE"));
    SET IN_SEG_NAME = json_unquote(json_extract(in_request_json,"$.SEG_NAME"));
    SET IN_SEG_QUERY = json_unquote(json_extract(in_request_json,"$.SEG_QUERY"));
    SET IN_SEG_ID = json_unquote(json_extract(in_request_json,"$.SEG_ID"));
    SET IN_SEG_CHANGE_CONDITION = json_unquote(json_extract(in_request_json,"$.SEG_CHANGE_CONDITION"));
    SET IN_SEG_CHANGE_NAME = json_unquote(json_extract(in_request_json,"$.SEG_CHANGE_NAME"));
	SET InUse = json_unquote(json_extract(in_request_json,"$.InUse"));
    
    IF EXISTS (select COLUMN_NAME from information_schema.columns
							where TABLE_NAME = "CDM_Customer_Segment_Sequence" AND COLUMN_NAME = "seg_Id" )
	THEN 
		SET @A = 1;
	ELSE
		alter table CDM_Customer_Segment_Sequence
		add column seg_Id int(10) primary key auto_increment; 
	END IF;
    
    IF EXISTS (select COLUMN_NAME from information_schema.columns
							where TABLE_NAME = "Dashboard_Segments_Master" AND COLUMN_NAME = "SegmentOldQuery" )
	THEN 
		SET @A = 1;
	ELSE
		alter table Dashboard_Segments_Master
		add column SegmentOldQuery TEXT; 
	END IF;
    
    
    IF IN_AGGREGATION_1 ="DISPLAY_CONFIG" AND IN_AGGREGATION_4 ="DASHBOARDSEGMENT_DISPLAY"
    THEN
		Set @View_STM= Concat("CREATE or REPLACE VIEW `V_Dashboard_segment` AS 
		SELECT * 
		FROM `Dashboard_Segments_Master` 
        WHERE SegmentType = 'CLM'
        AND IsActive = '",InUse,"'
		ORDER BY SegmentSequence
		;");
			
		SELECT @View_STM;
		PREPARE statement from @View_STM;
		Execute statement;
		Deallocate PREPARE statement; 
			
		Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object(
		"Name",`SegmentName`,
		"current",`SegmentReplacedQuery`,
        "old", `SegmentOldQuery`,
		"ID",`SegmentId`,
        "In_Use",IsActive
		) )),"]")into @temp_result from V_Dashboard_segment ;') ;
											  
		SELECT @Query_String;
		PREPARE statement from @Query_String;
		Execute statement;
		Deallocate PREPARE statement;
        select @temp_result;
    END IF;
    
    IF IN_AGGREGATION_1 ="EDIT_CONFIG" 
    THEN
		IF IN_AGGREGATION_3 ="NOT_CONFIRM"
        THEN
        
        SELECT SegmentReplacedQuery into IN_SEG_OLD_CONDITION from Dashboard_Segments_Master where SegmentId = IN_SEG_ID;
        
        select IN_SEG_OLD_CONDITION;
			
			Set @View_STM= Concat("CREATE or REPLACE VIEW `V_Dashboard_segment` AS 
		SELECT SegmentId,SegmentName,SegmentReplacedQuery,SegmentOldQuery,(Select Count(1) from V_CLM_Customer_One_View where ",IN_SEG_OLD_CONDITION,") AS Old_Coverage,(Select Count(1) from V_CLM_Customer_One_View where
        ",IN_SEG_CHANGE_CONDITION,") AS New_Coverage
		FROM `Dashboard_Segments_Master` 
        WHERE SegmentType = 'CLM' AND SegmentId = ",IN_SEG_ID,"
		ORDER BY SegmentSequence
		;");
			
		SELECT @View_STM;
		PREPARE statement from @View_STM;
		Execute statement;
		Deallocate PREPARE statement; 
			
		Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object(
		"Name",`SegmentName`,
		"Current_condition",`SegmentReplacedQuery`,
        "Old_Coverage",`Old_Coverage`,
        "New_condition", "',IN_SEG_CHANGE_CONDITION,'",
        "New_Coverage",`New_Coverage`,
		"ID",`SegmentId`,
        "New_Name", "',IN_SEG_CHANGE_NAME,'"
		) )),"]")into @temp_result from V_Dashboard_segment ;') ;
											  
		SELECT @Query_String;
		PREPARE statement from @Query_String;
		Execute statement;
		Deallocate PREPARE statement;
        END IF;
        
		IF IN_AGGREGATION_3 = "CONFIRM"
        THEN
		set @current_query = 1;
		SELECT SegmentReplacedQuery from Dashboard_Segments_Master where SegmentId = IN_SEG_ID into @current_query;
		
		update Dashboard_Segments_Master
		set SegmentOldQuery = @current_query,
		SegmentReplacedQuery = IN_SEG_CHANGE_CONDITION,
		SegmentName = IN_SEG_CHANGE_NAME
		WHERE SegmentId = IN_SEG_ID;
            
		Set @View_STM= Concat("CREATE or REPLACE VIEW `V_Dashboard_segment` AS 
		SELECT * 
		FROM `Dashboard_Segments_Master` 
        WHERE SegmentType = 'CLM'
		ORDER BY SegmentSequence
		;");
			
		SELECT @View_STM;
		PREPARE statement from @View_STM;
		Execute statement;
		Deallocate PREPARE statement; 
			
		Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object(
		"Name",`SegmentName`,
		"current",`SegmentReplacedQuery`,
        "old", `SegmentOldQuery`,
		"ID",`SegmentId`
		) )),"]")into @temp_result from V_Dashboard_segment ;') ;
											  
		SELECT @Query_String;
		PREPARE statement from @Query_String;
		Execute statement;
		Deallocate PREPARE statement;
        
        END IF;
    END IF;
    
	IF IN_AGGREGATION_1 ="DISPLAY_SEQUENCE"
		THEN 
		Set @View_STM= Concat("CREATE or REPLACE VIEW `V_Dashboard_segment_sequence` AS 
		SELECT * 
		FROM `CDM_Customer_Segment_Sequence` 
		ORDER BY sequence;
		;");
			
		SELECT @View_STM;
		PREPARE statement from @View_STM;
		Execute statement;
		Deallocate PREPARE statement; 
			
		Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object(
		"Name",`CLMSegmentName`,
		"sequence",`sequence`,
		"ID",`seg_Id`
		) )),"]")into @temp_result from V_Dashboard_segment_sequence ;') ;
											  
		SELECT @Query_String;
		PREPARE statement from @Query_String;
		Execute statement;
		Deallocate PREPARE statement; 
								  
	END IF;
		
	IF IN_AGGREGATION_1 ="EDIT_SEQUENCE"
    THEN
		UPDATE CDM_Customer_Segment_Sequence
        SET sequence = IN_SEQUENCE
        WHERE seg_Id = IN_AGGREGATION_2;
			
		Set @View_STM= Concat("CREATE or REPLACE VIEW `V_Dashboard_segment_sequence` AS 
		SELECT * 
		FROM `CDM_Customer_Segment_Sequence` 
		ORDER BY sequence;
		;");
			
		SELECT @View_STM;
		PREPARE statement from @View_STM;
		Execute statement;
		Deallocate PREPARE statement; 
			
		Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object(
		"Name",`CLMSegmentName`,
		"sequence",`sequence`,
		"ID",`seg_Id`
		) )),"]")into @temp_result from V_Dashboard_segment_sequence ;') ;
											  
		SELECT @Query_String;
		PREPARE statement from @Query_String;
		Execute statement;
		Deallocate PREPARE statement;
		
        
    END IF;
    
    If IN_AGGREGATION_4 ="DASHBOARDSEGMENT"
		THEN
				IF IN_ACTION="Yes"
					THEN

						Set @View_STM= Concat("
						Update Dashboard_Segments_Master
						Set IsActive='1'
						WHERE SegmentId = ",IN_SEG_ID,"
		;");
			
				PREPARE statement from @View_STM;
				Execute statement;
				Deallocate PREPARE statement; 
												   
						Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Dashboard Segment Is Enabled, Please run Insights Historical metrics for Segment numbers updation") )),"]") into @temp_result                       
														  ;') ;
		   
						SELECT @Query_String;
						PREPARE statement from @Query_String;
						Execute statement;
						Deallocate PREPARE statement; 											   
				End If;	

				IF IN_ACTION="No"
					THEN
                    
						Set @View_STM= Concat("
						Update Dashboard_Segments_Master
						Set IsActive='0'
						WHERE SegmentId = ",IN_SEG_ID,"
		;");
			
				PREPARE statement from @View_STM;
				Execute statement;
				Deallocate PREPARE statement; 
						
												   
						Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Dashboard Segment Is Disabled, Please run Insights Historical metrics for Segment numbers updation") )),"]") into @temp_result                       
														  ;') ;
		   
						SELECT @Query_String;
						PREPARE statement from @Query_String;
						Execute statement;
						Deallocate PREPARE statement; 
						
				End If;							   				
	
    End If;	
    
	If @temp_result is null
		Then
								
							Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Metrics execution never done for Customer Distribution") )),"]") into @temp_result                       
												  ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
						
	End if;
							

    SET out_result_json = @temp_result;                                          
  


END$$
DELIMITER ;
