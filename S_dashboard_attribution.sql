DELIMITER $$
CREATE or replace PROCEDURE `S_dashboard_attribution`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    
    
    

	SET @FY_SATRT_DATE = '2021-04-01';
	SET @FY_END_DATE = '2022-03-31';

	
	
	SET @view_stmt=concat('CREATE OR REPLACE VIEW V_Dashboard_Attribution_Graph AS
					(SELECT ',IN_MEASURE,', round((SUM(AttributedResponse)/SUM(Outreach))*100,1) as "value1",
                    SUM(AttributedResponse) as "value2"
					 from CLM_MTA_Response
					 WHERE AttributionMechanism = (case when "',IN_AGGREGATION_1,'" = "FIRST" THEN "First_Touch" when "',IN_AGGREGATION_1,'" = "LAST" THEN "Last_Touch" WHEN "',IN_AGGREGATION_1,'" = "MULTI" THEN "Multi_Touch" END)
                     and YM = "',IN_FILTER_CURRENTMONTH,'"
					 GROUP BY 1
					)');
	SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("displayName",`',IN_MEASURE,'`,"value1",`value1`,"value2",`value2`) )),"]")into @temp_result from V_Dashboard_Attribution_Graph;') ;

	select @view_stmt;
	PREPARE statement from @view_stmt;
	Execute statement;
	Deallocate PREPARE statement;
    
    
	select @Query_String;
	PREPARE statement from @Query_String;
	Execute statement;
	Deallocate PREPARE statement; 
    SET out_result_json = @temp_result;
    
END$$
DELIMITER ;
