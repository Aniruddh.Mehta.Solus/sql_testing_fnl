DELIMITER $$
CREATE or replace PROCEDURE `S_dashboard_blueprint_theme`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
    declare IN_FILTER_CURRENTMONTH text;
    declare IN_MEASURE text;
	
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
   
	CASE IN_SCREEN
    WHEN "BLUEPRINT_THEME_LIST" THEN
    BEGIN
    
    SET IN_MEASURE=json_unquote(json_extract(in_request_json,"$.MEASURE"));
	
    SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
    SET @Query_String1= 'create or replace view V_Dashboard_Blueprint_Campaign_Theme_List as SELECT 
        `CLM_CampaignTheme`.`CampaignThemeName` AS `Campaign Theme Name`,
        date_format(`CLM_CampaignTheme`.`CreatedDate`,"%d-%m-%Y") AS `Date Created`,
        count( `CLM_Campaign`.`CampaignName`) As `# Campaigns`,
       (select count( `CLM_Campaign`.`CampaignName`) from `CLM_Campaign` where `CampaignState`=1 and `CLM_Campaign`.`CampaignThemeId`=`CLM_CampaignTheme`.`CampaignThemeId`) As `# Active Campaigns`
        FROM
        (`CLM_CampaignTheme`
        JOIN `CLM_Campaign`)      
    WHERE
        `CLM_Campaign`.`CampaignThemeId` = `CLM_CampaignTheme`.`CampaignThemeId`
        group by `CLM_Campaign`.`CampaignThemeId`;';
               select @Query_String1;
                PREPARE statement1 from @Query_String1;
                Execute statement1;
                Deallocate PREPARE statement1; 
				
			    SET @Query_String = 'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Campaign Theme Name",`Campaign Theme Name`,"Date Created",`Date Created`,"# Campaigns",`# Campaigns`,"# Active Campaigns",`# Active Campaigns`) )),"]")into @temp_result from V_Dashboard_Blueprint_Campaign_Theme_List;' ;
			
               select @Query_String;
                PREPARE statement from @Query_String;
                Execute statement;
                Deallocate PREPARE statement; 
                select @temp_result into out_result_json;
                                      
    END;
    ELSE
			SET out_result_json=json_object("Status","Not Implemented Yet");
	    END CASE;	
END$$
DELIMITER ;
