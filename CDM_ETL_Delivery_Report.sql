DELIMITER $$
CREATE or replace PROCEDURE `CDM_ETL_Delivery_Report`()
BEGIN 
 
DECLARE vStart_Cnt BIGINT DEFAULT 0;
DECLARE vEnd_Cnt BIGINT;
DECLARE vBatchSize BIGINT DEFAULT 100000; 

  CALL CDM_Update_Process_Log('CDM_ETL_Delivery_Report', 0, 'CDM_ETL_Delivery_Report', 1); 
set  vStart_Cnt = 0;
set @Rec_Cnt = 0;

SELECT Rec_Id INTO vStart_Cnt FROM CDM_Stg_SMS_Del_Rep ORDER BY Rec_Id ASC LIMIT 1;
SELECT Rec_Id INTO @Rec_Cnt FROM CDM_Stg_SMS_Del_Rep ORDER BY Rec_Id DESC LIMIT 1;

 
SMS_LOOP: LOOP

SET vEnd_Cnt = vStart_Cnt + vBatchSize;

 insert ignore into CDM_Delivery_Report(
Customer_Id,
 Event_Execution_ID,
 Event_Execution_Date_ID,
 Delivery_Date, 
 Delivery_Status,
 Open_Date,
 Open_Status
)
select Customer_Id,
Event_Execution_ID,
Sent_Date,
Delivery_Date,
Delivery_Status,
Open_Date,
Open_Status 
from CDM_Stg_SMS_Del_Rep 
where Event_Execution_ID != ''
and Rec_Id between vStart_Cnt and vEnd_Cnt;

SET vStart_Cnt = vEnd_Cnt;

			IF vStart_Cnt  >= @Rec_Cnt THEN
				LEAVE SMS_LOOP;
	
			END IF;
            
END LOOP SMS_LOOP;  


set  vStart_Cnt = 0;
set @Rec_Cnt = 0; 
 
 SELECT Rec_Id INTO vStart_Cnt FROM CDM_Stg_Email_Rep ORDER BY Rec_Id ASC LIMIT 1;
SELECT Rec_Id INTO @Rec_Cnt FROM CDM_Stg_Email_Rep ORDER BY Rec_Id DESC LIMIT 1;

 
EMAIL_LOOP: LOOP

SET vEnd_Cnt = vStart_Cnt + vBatchSize;

 insert ignore into CDM_Delivery_Report(
Customer_Id,
 Event_Execution_ID,
 Event_Execution_Date_ID,
 Delivery_Date, 
 Delivery_Status,
 Open_Date,
 Open_Status,
 CTA_Status
 
)
select Customer_Id,
Event_Execution_ID,
Sent_Date,
Delivery_Date,
Delivery_Status,
Open_Date,
Open_Status,
CTA_Status 
from CDM_Stg_Email_Rep 
where Event_Execution_ID != ''
and Rec_Id between vStart_Cnt and vEnd_Cnt;

SET vStart_Cnt = vEnd_Cnt;

			IF vStart_Cnt  >= @Rec_Cnt THEN
				LEAVE EMAIL_LOOP;
	
			END IF;
            
END LOOP EMAIL_LOOP;  



set  vStart_Cnt = 0;
set @Rec_Cnt = 0;

SELECT Rec_Id INTO vStart_Cnt FROM CDM_Stg_PN_Rep ORDER BY Rec_Id ASC LIMIT 1;
SELECT Rec_Id INTO @Rec_Cnt FROM CDM_Stg_PN_Rep ORDER BY Rec_Id DESC LIMIT 1;

 
PN_LOOP: LOOP

SET vEnd_Cnt = vStart_Cnt + vBatchSize;

 insert ignore into CDM_Delivery_Report(
Customer_Id,
 Event_Execution_ID,
 Event_Execution_Date_ID,
 Delivery_Date, 
 Delivery_Status,
 Open_Date,
 Open_Status
)
select Customer_Id,
Event_Execution_ID,
Sent_Date,
Delivery_Date,
Delivery_Status,
Open_Date,
Open_Status 
from CDM_Stg_PN_Rep 
where Event_Execution_ID != ''
and Rec_Id between vStart_Cnt and vEnd_Cnt;

SET vStart_Cnt = vEnd_Cnt;

			IF vStart_Cnt  >= @Rec_Cnt THEN
				LEAVE PN_LOOP;
	
			END IF;
            
END LOOP PN_LOOP;


set  vStart_Cnt = 0;
set @Rec_Cnt = 0;

SELECT Rec_Id INTO vStart_Cnt FROM CDM_Stg_WhatsApp_Del_Rep ORDER BY Rec_Id ASC LIMIT 1;
SELECT Rec_Id INTO @Rec_Cnt FROM CDM_Stg_WhatsApp_Del_Rep ORDER BY Rec_Id DESC LIMIT 1;

 
WhatsApp_LOOP: LOOP

SET vEnd_Cnt = vStart_Cnt + vBatchSize;

 insert ignore into CDM_Delivery_Report(
Customer_Id,
 Event_Execution_ID,
 Event_Execution_Date_ID,
 Delivery_Date, 
 Delivery_Status,
 Open_Date,
 Open_Status
)
select Customer_Id,
Event_Execution_ID,
Sent_Date,
Delivery_Date,
Delivery_Status,
Open_Date,
Open_Status 
from CDM_Stg_WhatsApp_Del_Rep 
where Event_Execution_ID != ''
and Rec_Id between vStart_Cnt and vEnd_Cnt;

SET vStart_Cnt = vEnd_Cnt;

			IF vStart_Cnt  >= @Rec_Cnt THEN
				LEAVE WhatsApp_LOOP;
	
			END IF;
            
END LOOP WhatsApp_LOOP;



  
END$$
DELIMITER ;
