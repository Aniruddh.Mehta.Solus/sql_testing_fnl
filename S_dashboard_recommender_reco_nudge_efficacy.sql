DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_recommender_reco_nudge_efficacy`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
    declare IN_AGGREGATION_4 text;
    declare IN_AGGREGATION_5 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
    SET IN_AGGREGATION_4 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_4"));
    SET IN_AGGREGATION_5 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_5"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
   
   
    IF IN_MEASURE = 'GRAPH' AND IN_AGGREGATION_1 = 'Story'
		THEN
			SET @select_query = (CASE WHEN IN_NUMBER_FORMAT = 'NUMBER' THEN (case when IN_AGGREGATION_2 ='Product' then ' sum(PIDMatch)' when IN_AGGREGATION_2 = 'Similar_Product' then ' sum(PGMatch) '
			 when IN_AGGREGATION_2 = 'Category' then ' sum(CatMatch) ' end)
								when IN_NUMBER_FORMAT = 'PERCENT' THEN (case when IN_AGGREGATION_2 ='Product' then ' ROUND((sum(PIDMatch)/sum(NumProd)) * 100,2) ' when IN_AGGREGATION_2 = 'Similar_Product' then ' ROUND((sum(PGMatch)/sum(NumProd)) * 100,2) '
			 when IN_AGGREGATION_2 = 'Category' then ' ROUND((sum(CatMatch)/sum(NumProd)) * 100,2) ' end) end);
								
    set @view_stmt= CONCAT(' create or replace view V_Dashboard_Reco_Nudge_Efficacy_Graph as 
							WITH GROUP1 AS
                            (
							SELECT Story_Name as "FactorValue",
                             ',@select_query,' as "Value1",sum(NumProd) AS "Value3"
							from Dashboard_Reco_Nudge_Efficacy
                            where ByFactor = "Story"
                            and Source = "Baseline"
                            and Level = "', IN_AGGREGATION_3, '"
                            and Attribution = "', IN_AGGREGATION_4, '"
							and RespEndDate between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
							group by 1
                            )
                             ,
                             GROUP2 AS
							(
                             SELECT
							 Story_Name as "FactorValue", 
                             ',@select_query,'  as "Value2", sum(NumProd) AS "Value3"
							from Dashboard_Reco_Nudge_Efficacy
                            where ByFactor = "Story"
                            and Source = "Campaign"
							and Level = "', IN_AGGREGATION_3, '"
                            and Attribution = "', IN_AGGREGATION_4, '"
							and RespEndDate between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
                            GROUP By 1
                            )
                            SELECT A.FactorValue, Value1, Value2, A.Value3
                            FROM GROUP1 A
                            JOIN GROUP2 B
                            ON A.FactorValue=B.FactorValue
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("FactorValue",`FactorValue`,"Baseline",`Value1`,"Actual",`Value2`,"Total Purchases",`Value3`) )),"]")into @temp_result from V_Dashboard_Reco_Nudge_Efficacy_Graph;' ;
	END IF;
	
	IF IN_MEASURE = 'GRAPH' AND IN_AGGREGATION_1 = 'Event'
		THEN
			SET @select_query = (CASE WHEN IN_NUMBER_FORMAT = 'NUMBER' THEN (case when IN_AGGREGATION_2 ='Product' then ' sum(PIDMatch)' when IN_AGGREGATION_2 = 'Similar_Product' then ' sum(PGMatch) '
			 when IN_AGGREGATION_2 = 'Category' then ' sum(CatMatch) ' end)
								when IN_NUMBER_FORMAT = 'PERCENT' THEN (case when IN_AGGREGATION_2 ='Product' then ' ROUND((sum(PIDMatch)/sum(NumProd)) * 100,2) ' when IN_AGGREGATION_2 = 'Similar_Product' then ' ROUND((sum(PGMatch)/sum(NumProd)) * 100,2) '
			 when IN_AGGREGATION_2 = 'Category' then ' ROUND((sum(CatMatch)/sum(NumProd)) * 100,2) ' end) end);
								
    set @view_stmt= CONCAT(' create or replace view V_Dashboard_Reco_Nudge_Efficacy_Graph as 
							WITH GROUP1 AS
                            (
							SELECT Event_Name as "FactorValue",
                             ',@select_query,' as "Value1",sum(NumProd) AS "Value3"
							from Dashboard_Reco_Nudge_Efficacy
                            where ByFactor = "Event"
                            and Source = "Baseline"
							and Level = "', IN_AGGREGATION_3, '"
                            and Attribution = "', IN_AGGREGATION_4, '"
							and RespEndDate between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
							group by 1
                            )
                             ,
                             GROUP2 AS
							(
                             SELECT
							 Event_Name as "FactorValue",
                             ',@select_query,'  as "Value2",sum(NumProd) AS "Value3"
							from Dashboard_Reco_Nudge_Efficacy
                            where ByFactor = "Event"
                            and Source = "Campaign"
							and Level = "', IN_AGGREGATION_3, '"
                            and Attribution = "', IN_AGGREGATION_4, '"
							and RespEndDate between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
                            GROUP By 1
                            )
                            SELECT A.FactorValue, Value1, Value2, A.Value3
                            FROM GROUP1 A
                            JOIN GROUP2 B
                            ON A.FactorValue=B.FactorValue
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("FactorValue",`FactorValue`,"Baseline",`Value1`,"Actual",`Value2`,"Total Purchases",`Value3`) )),"]")into @temp_result from V_Dashboard_Reco_Nudge_Efficacy_Graph;' ;
	END IF;
	
	IF IN_MEASURE = 'GRAPH' AND IN_AGGREGATION_1 = 'Segment'
		THEN
			SET @select_query = (CASE WHEN IN_NUMBER_FORMAT = 'NUMBER' THEN (case when IN_AGGREGATION_2 ='Product' then ' sum(PIDMatch)' when IN_AGGREGATION_2 = 'Similar_Product' then ' sum(PGMatch) '
			 when IN_AGGREGATION_2 = 'Category' then ' sum(CatMatch) ' end)
								when IN_NUMBER_FORMAT = 'PERCENT' THEN (case when IN_AGGREGATION_2 ='Product' then ' ROUND((sum(PIDMatch)/sum(NumProd)) * 100,2) ' when IN_AGGREGATION_2 = 'Similar_Product' then ' ROUND((sum(PGMatch)/sum(NumProd)) * 100,2) '
			 when IN_AGGREGATION_2 = 'Category' then ' ROUND((sum(CatMatch)/sum(NumProd)) * 100,2) ' end) end);
								
    set @view_stmt= CONCAT(' create or replace view V_Dashboard_Reco_Nudge_Efficacy_Graph as 
							WITH GROUP1 AS
                            (
							SELECT Segment_Name as "FactorValue",
                             ',@select_query,' as "Value1", sum(NumProd) AS "Value3"
							from Dashboard_Reco_Nudge_Efficacy
                            where ByFactor = "Segment"
                            and Source = "Baseline"
							and Level = "', IN_AGGREGATION_3, '"
                            and Attribution = "', IN_AGGREGATION_4, '"
							and RespEndDate between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
							group by 1
                            )
                             ,
                             GROUP2 AS
							(
                             SELECT
							 Segment_Name as "FactorValue",
                             ',@select_query,'  as "Value2", sum(NumProd) AS "Value3"
							from Dashboard_Reco_Nudge_Efficacy
                            where ByFactor = "Segment"
                            and Source = "Campaign"
							and Level = "', IN_AGGREGATION_3, '"
                            and Attribution = "', IN_AGGREGATION_4, '"
							and RespEndDate between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
                            GROUP By 1
                            )
                            SELECT A.FactorValue, Value1, Value2, A.Value3
                            FROM GROUP1 A
                            JOIN GROUP2 B
                            ON A.FactorValue=B.FactorValue
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("FactorValue",`FactorValue`,"Baseline",`Value1`,"Actual",`Value2`,"Total Purchases",`Value3`) )),"]")into @temp_result from V_Dashboard_Reco_Nudge_Efficacy_Graph;' ;
	END IF;
   
	    
    select @view_stmt;
	PREPARE statement from @view_stmt;
	Execute statement;
	Deallocate PREPARE statement;
    
		select @Query_String;
		PREPARE statement from @Query_String;
		Execute statement;
		Deallocate PREPARE statement; 
		SET out_result_json = @temp_result;
    
END$$
DELIMITER ;
