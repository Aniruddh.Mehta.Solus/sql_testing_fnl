
CREATE OR REPLACE PROCEDURE `S_CLM_Event_Level_Hold_Out`()
BEGIN

DECLARE vHold_Out_Event_ID,vHold_Out_Event_Percentage,Load_Exe_ID,Exe_ID,VCustomer_Count bigint(20);

DECLARE curEventList_done Int Default 0;
DECLARE curEventList Cursor For Select Distinct EE. Event_ID, EM.Event_Control_Group_Percentage  
From Event_Execution EE,Event_Master EM Where  EE.Event_ID = EM.ID 
and EM.Event_Control_Group_Percentage >= 0 
and (( select value from M_Config where name='Event_Level_Control_Group' ) = 'Enabled');
DECLARE CONTINUE HANDLER FOR NOT FOUND SET curEventList_done=1;


SET SQL_SAFE_UPDATES=0;

insert into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) values ('S_Event_Level_Hold_Out','',now(),null,'Started', 090921);
select * from ETL_Execution_Details Order by Execution_Id DESC LIMIT 1;
set Exe_ID = F_Get_Execution_Id();

OPEN curEventList;
LOOP_curEventList : LOOP
FETCH curEventList into vHold_Out_Event_ID,vHold_Out_Event_Percentage;
IF curEventList_done THEN
      LEAVE LOOP_curEventList;
END IF;


SELECT 
    round(count(ee.Customer_Id)*(vHold_Out_Event_Percentage/100))
INTO VCustomer_Count FROM
    Event_Execution ee
        LEFT JOIN
    Event_Level_Hold_Out elho ON ee.Event_ID = elho.Event_ID
        AND ee.Customer_Id = elho.Customer_Id
WHERE
    elho.Customer_Id IS NULL
        AND ee.Event_ID = vHold_Out_Event_ID
        AND ee.In_Control_Group IS NULL;


UPDATE Event_Execution EE,
    (SELECT 
        ee.Customer_Id,ee.Event_ID
    FROM
        Event_Execution ee
    LEFT JOIN Event_Level_Hold_Out elho ON ee.Event_ID = elho.Event_ID
        AND ee.Customer_Id = elho.Customer_Id
    WHERE
        ee.Event_ID = vHold_Out_Event_ID
            AND elho.Customer_Id IS NULL
            AND ee.In_Control_Group IS NULL
    ORDER BY RAND()
    LIMIT VCustomer_Count ) TempSet 
SET 
    EE.In_Event_Control_Group = 'Y'
WHERE
    TempSet.Customer_Id=EE.Customer_Id
AND TempSet.Event_Id=vHold_Out_Event_ID;

INSERT IGNORE INTO  Event_Level_Hold_Out (Event_ID,Customer_Id,Event_Execution_Date_ID) 
(SELECT 
    ee.Event_ID, ee.Customer_Id, ee.Event_Execution_Date_ID
FROM
    Event_Execution ee
WHERE
    ee.Event_ID = vHold_Out_Event_ID
	AND ee.In_Event_Control_Group = 'Y');

END LOOP LOOP_curEventList;
CLOSE curEventList;

UPDATE ETL_Execution_Details SET Status = 'Succeeded', End_Time = NOW() WHERE Procedure_Name = 'S_Event_Level_Hold_Out' AND Execution_ID = Exe_ID;
SELECT concat('S_Event_Level_Hold_Out : COMPLETED ', now(), ' LOAD ID :', Exe_ID) as '';

END

