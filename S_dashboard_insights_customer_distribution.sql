DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_insights_customer_distribution`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    
    IF IN_MEASURE = "Recency"
	THEN
		SET @Query_Measure='Recency';
	END IF;
	IF IN_MEASURE = "ABV" AND IN_AGGREGATION_1 = "FBAP" 
	THEN
		SET @Query_Measure='AP_ABV';
	END IF;
    IF IN_MEASURE = "ABV" AND IN_AGGREGATION_1 = "FBEP" 
	THEN
		SET @Query_Measure='Lt_ABV';
	END IF;
    IF IN_MEASURE = "Frequency" AND IN_AGGREGATION_1 = "FBAP" 
	THEN
		SET @Query_Measure='AP_Num_of_Visits';
	END IF;
    IF IN_MEASURE = "Frequency" AND IN_AGGREGATION_1 = "FBEP" 
	THEN
		SET @Query_Measure='Lt_Num_of_Visits';
	END IF;
     IF IN_MEASURE = "Discount Taken" AND IN_AGGREGATION_1 = "FBAP" 
	THEN
		SET @Query_Measure='AP_Disc_Percent';
	END IF;
    IF IN_MEASURE = "Discount Taken" AND IN_AGGREGATION_1 = "FBEP" 
	THEN
		SET @Query_Measure='Lt_Disc_Percent';
	END IF;
   IF IN_MEASURE = "Range" AND IN_AGGREGATION_1 = "FBAP" 
	THEN
		SET @Query_Measure='AP_Distinct_Prod_Cnt';
	END IF;
    IF IN_MEASURE = "Range" AND IN_AGGREGATION_1 = "FBEP" 
	THEN
		SET @Query_Measure='LT_Distinct_Prod_Cnt';
	END IF;
    
	SET @view_stmt=concat("CREATE OR REPLACE VIEW V_Dashboard_insights_custDist AS
					select attribute,concat('From ',round(lower_limit,0),' To ', case when upper_limit <> -1 then round(upper_limit,0) else ' Max' end ) as `Year` , MAX(Percent_Of_Customers)*100 as `Value`
from Dashboard_Insights_Customer_Distribution where attribute = '",@Query_Measure,"'
group by `Year`  order by lower_limit asc ");
	SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Year",`Year`,"Value",`Value`) )),"]")into @temp_result from V_Dashboard_insights_custDist;') ;

	select @view_stmt;
	PREPARE statement from @view_stmt;
	Execute statement;
	Deallocate PREPARE statement;
    
    
	select @Query_String;
	PREPARE statement from @Query_String;
	Execute statement;
	Deallocate PREPARE statement; 
    SET out_result_json = @temp_result;
    
END$$
DELIMITER ;
