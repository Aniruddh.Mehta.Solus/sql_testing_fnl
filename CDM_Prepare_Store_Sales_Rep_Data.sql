DELIMITER $$
CREATE or REPLACE PROCEDURE `CDM_Prepare_Store_Sales_Rep_Data`(IN vRep_Year CHAR(4), IN vRep_Month CHAR(2))
BEGIN
    DECLARE vRep_Year_Month CHAR(6) DEFAULT '';
    DECLARE vRep_StartDate DATE;
	DECLARE vRep_EndDate DATE;
       
    SET vRep_StartDate = DATE(CONCAT(vRep_Year,'/', vRep_Month,'/01'));
	SET vRep_EndDate = LAST_DAY(vRep_StartDate);
    
    SET vRep_Year_Month = CONCAT(vRep_Year,vRep_Month);
	SELECT vRep_Year_Month, vRep_Year, vRep_Month;
	CALL CDM_Update_Process_Log('CDM_Prepare_Store_Sales_Rep_Data', CONCAT(vRep_Year,'_',vRep_Month,'_1'), 'CDM_Prepare_Store_Sales_Rep_Data', 1);
	 
	INSERT IGNORE INTO CDM_Store_Sales_Rep (
		Store_Id,
		Rep_Year,
		Rep_Month
	)
	SELECT 
		Store_Id,
		vRep_Year,
		vRep_Month
	FROM CDM_Store_Master;

	CALL CDM_Update_Process_Log('CDM_Prepare_Store_Sales_Rep_Data', CONCAT(vRep_Year,'_',vRep_Month,'_2'), 'CDM_Prepare_Store_Sales_Rep_Data', 1);
	-- Calculating Total Values
	UPDATE 
		CDM_Store_Sales_Rep,
		(
			SELECT 
				CDM_BH.Store_Id AS vStore_Id,
				COUNT(CDM_BH.Bill_Header_Id) AS  vBill_Cnt,
				SUM(CDM_BH.Bill_Total_Val) AS vSales_Val
			FROM 
				CDM_Bill_Header AS CDM_BH
			WHERE 
				CDM_BH.Bill_Year_Month = vRep_Year_Month
			GROUP BY
				CDM_BH.Store_Id
		) AS Store_BRR
	SET
			Total_Bill_Cnt = Store_BRR.vBill_Cnt,
			Total_Sales_Val = Store_BRR.vSales_Val
	WHERE
		CDM_Store_Sales_Rep.Store_Id = Store_BRR.vStore_Id AND
		CDM_Store_Sales_Rep.Rep_Year = vRep_Year AND
		CDM_Store_Sales_Rep.Rep_Month = vRep_Month;


	UPDATE 
		CDM_Store_Sales_Rep,
		(
			SELECT 
				CDM_BH.Store_Id AS vStore_Id,
				COUNT(CDM_BH.Bill_Header_Id) AS  vBill_Cnt,
				SUM(CDM_BH.Bill_Total_Val) AS vSales_Val
			FROM 
				CDM_NM_Bill_Header AS CDM_BH
			WHERE 
				CDM_BH.Bill_Year_Month = vRep_Year_Month
			GROUP BY
				CDM_BH.Store_Id
		) AS Store_BRR
	SET
			Total_Bill_Cnt = Total_Bill_Cnt + Store_BRR.vBill_Cnt,
			Total_Sales_Val = Total_Sales_Val + Store_BRR.vSales_Val
	WHERE
		CDM_Store_Sales_Rep.Store_Id = Store_BRR.vStore_Id AND
		CDM_Store_Sales_Rep.Rep_Year = vRep_Year AND
		CDM_Store_Sales_Rep.Rep_Month = vRep_Month;

	UPDATE CDM_Store_Sales_Rep
	SET Total_ABV = (CASE WHEN Total_Bill_Cnt <= 0 THEN 0 ELSE Total_Sales_Val/Total_Bill_Cnt END);
    
	CALL CDM_Update_Process_Log('CDM_Prepare_Store_Sales_Rep_Data', CONCAT(vRep_Year,'_',vRep_Month,'_3'), 'CDM_Prepare_Store_Sales_Rep_Data', 1);
	-- Calculating Enrolment Values
	UPDATE 
		CDM_Store_Sales_Rep,
		(
			SELECT 
				CDM_BH.Store_Id AS vStore_Id,
				COUNT(DISTINCT CDM_BH.Customer_Id) AS vMember_Cnt,
				COUNT(CDM_BH.Bill_Header_Id) AS  vBill_Cnt,
				SUM(CDM_BH.Bill_Total_Val) AS vSales_Val
			FROM 
				CDM_Bill_Header AS CDM_BH, CDM_Customer_TP_Var
			WHERE 
				CDM_BH.Customer_Id =  CDM_Customer_TP_Var.Customer_Id
				AND CDM_BH.Bill_Year_Month = vRep_Year_Month
                AND CDM_BH.Bill_Date = CDM_Customer_TP_Var.FTD 
			GROUP BY
				CDM_BH.Store_Id
		) AS Store_BRR
	SET
		Enrollment_Cnt = Store_BRR.vMember_Cnt,
		Enrollment_Bill_Cnt = Store_BRR.vBill_Cnt,
		Enrollment_Sales_Val = Store_BRR.vSales_Val,
		Enrollment_ABV = (CASE WHEN Store_BRR.vBill_Cnt <= 0 THEN 0 ELSE (Store_BRR.vSales_Val/Store_BRR.vBill_Cnt) END)
	WHERE
		CDM_Store_Sales_Rep.Store_Id = Store_BRR.vStore_Id AND
		CDM_Store_Sales_Rep.Rep_Year = vRep_Year AND
		CDM_Store_Sales_Rep.Rep_Month = vRep_Month;

	CALL CDM_Update_Process_Log('CDM_Prepare_Store_Sales_Rep_Data', CONCAT(vRep_Year,'_',vRep_Month,'_4'), 'CDM_Prepare_Store_Sales_Rep_Data', 1);
	-- Calculating Member Repeat Values
	UPDATE 
		CDM_Store_Sales_Rep,
		(
			SELECT 
				CDM_BH.Store_Id AS vStore_Id,
				COUNT(DISTINCT CDM_BH.Customer_Id) AS vMember_Cnt,
				COUNT(CDM_BH.Bill_Header_Id) AS  vBill_Cnt,
				SUM(CDM_BH.Bill_Total_Val) AS vSales_Val
			FROM 
				CDM_Bill_Header AS CDM_BH, CDM_Customer_TP_Var 
			WHERE 
				CDM_BH.Customer_Id = CDM_Customer_TP_Var.Customer_Id
				AND CDM_BH.Bill_Year_Month = vRep_Year_Month
                AND CDM_BH.Bill_Date <> CDM_Customer_TP_Var.FTD 
			GROUP BY
				CDM_BH.Store_Id
		) AS Store_BRR
	SET
		Member_Repeat_Cnt = Store_BRR.vMember_Cnt,
		Member_Repeat_Bill_Cnt = Store_BRR.vBill_Cnt,
		Member_Repeat_Sales_Val = Store_BRR.vSales_Val,
		Member_Repeat_ABV = (CASE WHEN Store_BRR.vBill_Cnt <= 0 THEN 0 ELSE (Store_BRR.vSales_Val/Store_BRR.vBill_Cnt) END)
	WHERE
		CDM_Store_Sales_Rep.Store_Id = Store_BRR.vStore_Id AND
		CDM_Store_Sales_Rep.Rep_Year = vRep_Year AND
		CDM_Store_Sales_Rep.Rep_Month = vRep_Month;
	
	CALL CDM_Update_Process_Log('CDM_Prepare_Store_Sales_Rep_Data', CONCAT(vRep_Year,'_',vRep_Month,'_5'), 'CDM_Prepare_Store_Sales_Rep_Data', 1);   
	-- Calculating Member Values
	UPDATE 
		CDM_Store_Sales_Rep,
		(
			SELECT 
				CDM_BH.Store_Id AS vStore_Id,
				COUNT(DISTINCT CDM_BH.Customer_Id) AS vMember_Cnt,
				COUNT(DISTINCT CDM_BH.Customer_Id, CDM_BH.Bill_Date) AS vVisits_Cnt,
				COUNT(CDM_BH.Bill_Header_Id) AS  vBill_Cnt,
				SUM(CDM_BH.Bill_Total_Val) AS vSales_Val
			FROM 
				CDM_Bill_Header AS CDM_BH
			WHERE 
				CDM_BH.Bill_Year_Month = vRep_Year_Month
			GROUP BY
				CDM_BH.Store_Id
		) AS Store_BRR
	SET
			Member_Cnt = Store_BRR.vMember_Cnt,
			Member_Visits_Cnt = Store_BRR.vVisits_Cnt,
			Member_Bill_Cnt = Store_BRR.vBill_Cnt,
			Member_Sales_Val = Store_BRR.vSales_Val,
			Member_ABV = CASE WHEN Store_BRR.vBill_Cnt <= 0 THEN 0 ELSE (Store_BRR.vSales_Val/Store_BRR.vBill_Cnt) END
	WHERE
		CDM_Store_Sales_Rep.Store_Id = Store_BRR.vStore_Id AND
		CDM_Store_Sales_Rep.Rep_Year = vRep_Year AND
		CDM_Store_Sales_Rep.Rep_Month = vRep_Month;

	CALL CDM_Update_Process_Log('CDM_Prepare_Store_Sales_Rep_Data', CONCAT(vRep_Year,'_',vRep_Month,'_6'), 'CDM_Prepare_Store_Sales_Rep_Data', 1);

END$$
DELIMITER ;
