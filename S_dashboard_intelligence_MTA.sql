DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_intelligence_MTA`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));


    /*select extract(YEAR_MONTH FROM IN_FILTER_CURRENTMONTH) into @curr_time;*/
    set @curr_time = IN_FILTER_CURRENTMONTH;
    select @curr_time;
    
	IF IN_MEASURE = 'TABLE' 
    THEN
       IF IN_AGGREGATION_1 = 'Channel'
       THEN
          set @view_stmt = concat('create or replace View V_Dashboard_MTA_Table as
								   select ChannelName, 
									sum(LTANum) as `LTA#`,
									round(sum(LTANum)/sum(Outreach)*100,1) as `LTA%`, 
									round(sum(MTANum),1) as `MTA#`,
									round(sum(MTANum)/sum(Outreach)*100,1) as `MTA%`,
									YM from CLM_MTA_Response_Detailed b 
									where YM = ',@curr_time,' group by ChannelName;');
			
		   set @Query_String = concat('SELECT CONCAT("[",GROUP_CONCAT(json_object("Channel",`ChannelName`,"% LTA",`LTA%`,"% MTA",`MTA%`,"# LTA",`LTA#`,"# MTA",`MTA#`)),"]") into @temp_result from V_Dashboard_MTA_Table');
       END IF;
       IF IN_AGGREGATION_1 = 'CLMGTM'
       THEN
          set @view_stmt = concat('create or replace View V_Dashboard_MTA_Table as
                                   select Category,
									sum(LTANum) as `LTA#`,
									round((sum(LTANum)/sum(Outreach))*100,2) as `LTA%`,
									round(sum(MTANum),2) as `MTA#`,
									round((sum(MTANum)/sum(Outreach))*100,2) as `MTA%` from
									(select distinct CASE WHEN datediff(EndDate,StartDate) < 32 THEN "GTM" else "CLM" END AS `Category`,b.ID,
									b.LTANum as `LTANum`,
									b.MTANum as `MTANum`,
									b.Outreach as `Outreach`, b.YM
									from CLM_MTA_Response_Detailed b 
									where b.YM = ',@curr_time,') as temp_table group by Category;');
		  set @Query_String = concat('SELECT CONCAT("[",GROUP_CONCAT(json_object("CLMGTM",`Category`,"% LTA",`LTA%`,"% MTA",`MTA%`,"# LTA",`LTA#`,"# MTA",`MTA#`)),"]") into @temp_result from V_Dashboard_MTA_Table');
       END IF;
       IF IN_AGGREGATION_1 = 'Theme'
       THEN
          set @view_stmt = concat('create or replace View V_Dashboard_MTA_Table as
								   select Theme,
									sum(LTANum) as `LTA#`,
									round((sum(LTANum)/sum(Outreach))*100,2) as `LTA%`,
									round(sum(MTANum),2) as `MTA#`,
									round((sum(MTANum)/sum(Outreach))*100,2) as `MTA%` from
									(select distinct a.CampaignThemeName as `Theme`,a.EventId,
									b.LTANum as `LTANum`,
									b.MTANum as `MTANum`,
									b.Outreach as `Outreach`, b.YM
									from Event_Master a inner join CLM_MTA_Response_Detailed b on a.EventID = b.ID
									where b.YM = ',@curr_time,') as temp_table group by Theme;');
		  set @Query_String = concat('SELECT CONCAT("[",GROUP_CONCAT(json_object("Theme",`Theme`,"% LTA",`LTA%`,"% MTA",`MTA%`,"# LTA",`LTA#`,"# MTA",`MTA#`)),"]") into @temp_result from V_Dashboard_MTA_Table');
       END IF;
       IF IN_AGGREGATION_1 = 'Trigger'
       THEN
          set @view_stmt = concat('create or replace View V_Dashboard_MTA_Table as
                                   select YM as YearMonth,
									Name as `Trigger`,
                                    Outreach as `Outreach`,
                                    LTANum as `LTA#`,
									round(LTAPercent,2) as `LTA%`,
									round(MTANum,2) as `MTA#`,
									round(MTAPercent,2) as `MTA%`
									from CLM_MTA_Response_Detailed 
									where YM = ',@curr_time,';');
		  set @Query_String = concat('SELECT CONCAT("[",GROUP_CONCAT(json_object("Trigger",`Trigger`,"Outreach",`Outreach`,"% LTA",`LTA%`,"% MTA",`MTA%`,"# LTA",`LTA#`,"# MTA",`MTA#`)),"]") into @temp_result from V_Dashboard_MTA_Table');
	   END IF;
       
	END IF;
    
    
    IF IN_MEASURE = 'GRAPH' AND IN_AGGREGATION_2 = 'NUMBER'
    THEN
		IF IN_AGGREGATION_1 = 'Channel'
        THEN
          set @view_stmt = concat('create or replace View V_Dashboard_MTA_Graph as
								   select ChannelName, 
									sum(LTANum) as `LTA#`, 
									round(sum(MTANum),1) as `MTA#`,
									YM from CLM_MTA_Response_Detailed b 
									where YM = ',@curr_time,' group by ChannelName;');
			
		   set @Query_String = concat('SELECT CONCAT("[",GROUP_CONCAT(json_object("X_Data",`ChannelName`,"# LTA",`LTA#`,"# MTA",`MTA#`)),"]") into @temp_result from V_Dashboard_MTA_Graph');
         END IF;
         IF IN_AGGREGATION_1 = 'CLMGTM'
         THEN
          set @view_stmt = concat('create or replace View V_Dashboard_MTA_Graph as
								   select Category,
									sum(LTANum) as `LTA#`,
									round(sum(MTANum),2) as `MTA#` from
									(select distinct CASE WHEN datediff(EndDate,StartDate) < 32 THEN "GTM" else "CLM" END AS `Category`,b.ID,
									b.LTANum as `LTANum`,
									b.MTANum as `MTANum`,
									b.Outreach as `Outreach`, b.YM
									from CLM_MTA_Response_Detailed b 
									where b.YM = ',@curr_time,') as temp_table group by Category;');
			
		   set @Query_String = concat('SELECT CONCAT("[",GROUP_CONCAT(json_object("X_Data",`Category`,"#LTA",`LTA#`,"#MTA",`MTA#`)),"]") into @temp_result from V_Dashboard_MTA_Graph');
         END IF;
         IF IN_AGGREGATION_1 = 'Theme'
		 THEN
          set @view_stmt = concat('create or replace View V_Dashboard_MTA_Graph as
								   select Theme,
									sum(LTANum) as `LTA#`,
									round(sum(MTANum),2) as `MTA#` from
									(select distinct a.CampaignThemeName as `Theme`,a.EventId,
									b.LTANum as `LTANum`,
									b.MTANum as `MTANum`,
									b.Outreach as `Outreach`, b.YM
									from Event_Master a inner join CLM_MTA_Response_Detailed b on a.EventID = b.ID
									where b.YM = ',@curr_time,') as temp_table group by Theme;');
			
		   set @Query_String = concat('SELECT CONCAT("[",GROUP_CONCAT(json_object("X_Data",`Theme`,"# LTA",`LTA#`,"# MTA",`MTA#`)),"]") into @temp_result from V_Dashboard_MTA_Graph');
         END IF;
         IF IN_AGGREGATION_1 = 'Trigger'
		 THEN
             set @view_stmt = concat('create or replace View V_Dashboard_MTA_Graph as
                                   select Name as `Trigger`,
                                   LTANum as `LTA#`,
                                   round(MTANum,1) as `MTA#`
                                   from CLM_MTA_Response_Detailed where YM = ',@curr_time,' order by LTANum desc limit 10;');
		    set @Query_String = concat('SELECT CONCAT("[",GROUP_CONCAT(json_object("X_Data",`Trigger`,"# LTA",`LTA#`,"# MTA",`MTA#`)),"]") into @temp_result from V_Dashboard_MTA_Graph');
	     END IF;
    END IF;
    
    IF IN_MEASURE = 'GRAPH' AND IN_AGGREGATION_2 = 'PERCENT'
    THEN
        IF IN_AGGREGATION_1 = 'Channel'
       THEN
          set @view_stmt = concat('create or replace View V_Dashboard_MTA_Graph as
								   select ChannelName, 
									round(sum(LTANum)/sum(Outreach)*100,1) as `LTA%`, 
									round(sum(MTANum)/sum(Outreach)*100,1) as `MTA%`,
									YM from CLM_MTA_Response_Detailed b 
									where YM = ',@curr_time,' group by ChannelName;');
			
		   set @Query_String = concat('SELECT CONCAT("[",GROUP_CONCAT(json_object("X_Data",`ChannelName`,"% LTA",`LTA%`,"% MTA",`MTA%`)),"]") into @temp_result from V_Dashboard_MTA_Graph');
       END IF;
       IF IN_AGGREGATION_1 = 'CLMGTM'
       THEN
          set @view_stmt = concat('create or replace View V_Dashboard_MTA_Graph as
								   select Category,
									round((sum(LTANum)/sum(Outreach))*100,2) as `LTA%`,
									round((sum(MTANum)/sum(Outreach))*100,2) as `MTA%` from
									(select distinct CASE WHEN datediff(EndDate,StartDate) < 32 THEN "GTM" else "CLM" END AS `Category`,b.ID,
									b.LTANum as `LTANum`,
									b.MTANum as `MTANum`,
									b.Outreach as `Outreach`, b.YM
									from CLM_MTA_Response_Detailed b 
									where b.YM = ',@curr_time,') as temp_table group by Category;');
			
		   set @Query_String = concat('SELECT CONCAT("[",GROUP_CONCAT(json_object("X_Data",`Category`,"% LTA",`LTA%`,"% MTA",`MTA%`)),"]") into @temp_result from V_Dashboard_MTA_Graph');
       END IF;
       IF IN_AGGREGATION_1 = 'Theme'
       THEN
          set @view_stmt = concat('create or replace View V_Dashboard_MTA_Graph as
								   select Theme,
									round((sum(LTANum)/sum(Outreach))*100,2) as `LTA%`,
									round((sum(MTANum)/sum(Outreach))*100,2) as `MTA%` from
									(select distinct a.CampaignThemeName as `Theme`,a.EventId,
									b.LTANum as `LTANum`,
									b.MTANum as `MTANum`,
									b.Outreach as `Outreach`, b.YM
									from Event_Master a inner join CLM_MTA_Response_Detailed b on a.EventID = b.ID
									where b.YM = ',@curr_time,') as temp_table group by Theme;');
												
		   set @Query_String = concat('SELECT CONCAT("[",GROUP_CONCAT(json_object("X_Data",`Theme`,"% LTA",`LTA%`,"% MTA",`MTA%`)),"]") into @temp_result from V_Dashboard_MTA_Graph');
        END IF;
        IF IN_AGGREGATION_1 = 'Trigger'
		THEN
          set @view_stmt = concat('create or replace View V_Dashboard_MTA_Graph as
                                   select Name as `Trigger`,
                                   round(LTAPercent,1) as `LTA%`,
                                   round(MTAPercent,1) as `MTA%`
                                   from CLM_MTA_Response_Detailed where YM = ',@curr_time,' order by LTAPercent desc limit 10;');
		  set @Query_String = concat('SELECT CONCAT("[",GROUP_CONCAT(json_object("X_Data",`Trigger`,"% LTA",`LTA%`,"% MTA",`MTA%`)),"]") into @temp_result from V_Dashboard_MTA_Graph');
	    END IF;
    END IF;
	/*SET @view_stmt=concat('CREATE OR REPLACE VIEW V_Dashboard_Attribution_Graph AS
					(SELECT ',IN_MEASURE,', round((SUM(AttributedResponse)/SUM(Outreach))*100,1) as "value1",
                    SUM(AttributedResponse) as "value2"
					 from CLM_MTA_Response
					 WHERE AttributionMechanism = (case when "',IN_AGGREGATION_1,'" = "FIRST" THEN "First_Touch" when "',IN_AGGREGATION_1,'" = "LAST" THEN "Last_Touch" WHEN "',IN_AGGREGATION_1,'" = "MULTI" THEN "Multi_Touch" END)
                     and YM = "',IN_FILTER_CURRENTMONTH,'"
					 GROUP BY 1
					)');
	SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("displayName",`',IN_MEASURE,'`,"value1",`value1`,"value2",`value2`) )),"]")into @temp_result from V_Dashboard_Attribution_Graph;') ; */

	select @view_stmt;
	PREPARE statement from @view_stmt;
	Execute statement;
	Deallocate PREPARE statement;
    
    
	select @Query_String;
	PREPARE statement from @Query_String;
	Execute statement;
	Deallocate PREPARE statement; 
    SET out_result_json = @temp_result;
    
END$$
DELIMITER ;
