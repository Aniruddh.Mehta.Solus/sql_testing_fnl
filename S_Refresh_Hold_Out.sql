
CREATE OR REPLACE PROCEDURE `S_Refresh_Hold_Out`()
BEGIN
  DECLARE Load_Exe_ID, Exe_ID int;
    
    set Load_Exe_ID = F_Get_Load_Execution_Id();
    set sql_safe_updates=0;
  set foreign_key_checks=0;
    
  insert into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
    values ('S_Refresh_Hold_Out', '',now(),null,'Started', Load_Exe_ID);
    
    set Exe_ID = F_Get_Execution_Id();
    set sql_safe_updates=0;
  set foreign_key_checks=0;
   
    drop table if exists Hold_Out_Bkp_Before_Refresh;
     
    create table if not exists Hold_Out_Bkp_Before_Refresh as
    select * from Hold_Out;
	
    create table if not exists Hold_Out_Back_Up LIKE Hold_Out;
    truncate Hold_Out;
    truncate Hold_Out_Back_Up;
     
    update CDM_Customer_Master set Is_New_Cust_Flag=1;

        update ETL_Execution_Details
        set Status ='Succeeded',
        End_Time = now()
        where Procedure_Name='S_Refresh_Hold_Out' and 
        Load_Execution_ID = Load_Exe_ID
        and Execution_ID = Exe_ID;   
   
END

