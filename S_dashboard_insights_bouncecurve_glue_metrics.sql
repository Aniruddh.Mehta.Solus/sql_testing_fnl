DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_insights_bouncecurve_glue_metrics`()
BEGIN
DECLARE vprevcur_NoOfDaysGLUE int Default 0;
DECLARE vCheckEndOfCursor int Default 0;
DECLARE vcur_NoOfDaysGLUE int Default 0;
DECLARE vStart_Date BIGINT DEFAULT 0;
DECLARE vEnd_Date BIGINT DEFAULT 0;
DECLARE cur_NoOfDaysGLUE CURSOR FOR SELECT DISTINCT NoOfDays FROM Dashboard_Insights_BounceCurve where AGGREGATION='GLUE' ORDER BY NoOfDays DESC;
DECLARE cur_NoOfDaysGLUEL24M CURSOR FOR SELECT DISTINCT NoOfDays FROM Dashboard_Insights_BounceCurve where AGGREGATION='GLUE' AND Measure = 'L24M' ORDER BY NoOfDays DESC;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET vCheckEndOfCursor=1;
DELETE from log_solus where module='S_dashboard_insights_bouncecurve';

SET SQL_SAFE_UPDATES=0;
DELETE FROM Dashboard_Insights_BounceCurve where AGGREGATION = 'GLUE';

INSERT INTO Dashboard_Metrics_Log
(
`Screen_Name`,
`Status`
)
VALUES
('INSIGHTS - GLUE NUMBER',
'Started');

SET vStart_Date = date_format(date_sub(concat(date_format(current_date(),"%Y%m"),'01'), INTERVAL 23 MONTH),"%Y%m");
SET vEnd_Date = date_format(current_date(),"%Y%m");

select vStart_Date, vEnd_Date;

INSERT INTO `Dashboard_Insights_BounceCurve`
(`AGGREGATION`,
`Measure`,
`NoOfDays`,
`NoOfCustomer`,
`NoOfCustomerPercent`,
`Revenue_Center`,
`CLMSegmentName`)
SELECT 
    'GLUE', 'L24M', Lt_Num_of_Visits, COUNT(DISTINCT A.Customer_Id), - 1, 'NA', 'NA'
FROM CDM_Customer_TP_Var A
JOIN CDM_Bill_Details B
ON A.Customer_Id = B.Customer_Id
WHERE Lt_Num_of_Visits > 0
AND Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Lt_Num_of_Visits
ORDER BY Lt_Num_of_Visits ASC;

INSERT INTO `Dashboard_Insights_BounceCurve`
(`AGGREGATION`,
`Measure`,
`NoOfDays`,
`NoOfCustomer`,
`NoOfCustomerPercent`,
`Revenue_Center`,
`CLMSegmentName`)
SELECT 
    'GLUE', 'ATM', Lt_Num_of_Visits, COUNT(Customer_Id), - 1, 'NA', 'NA'
FROM
    CDM_Customer_TP_Var
WHERE Lt_Num_of_Visits > 0
GROUP BY Lt_Num_of_Visits
ORDER BY Lt_Num_of_Visits ASC;

set @totalcustomer=0;
SELECT sum(NoOfCustomer) into @totalcustomer FROM Dashboard_Insights_BounceCurve WHERE AGGREGATION = 'GLUE' AND Measure = 'ATM';
if @totalcustomer=0 then  
set @totalcustomer=1;
end if ;

set @totalcustomerL24M=0;
SELECT sum(NoOfCustomer) into @totalcustomerL24M FROM Dashboard_Insights_BounceCurve WHERE AGGREGATION = 'GLUE' AND Measure = 'L24M';
if @totalcustomerL24M=0 then  
set @totalcustomerL24M=1;
end if ;

DELETE FROM Dashboard_Insights_BounceCurve where AGGREGATION = 'GLUEDISTRIBUTION';

INSERT INTO `Dashboard_Insights_BounceCurve`
(`AGGREGATION`,
`Measure`,
`NoOfDays`,
`NoOfCustomer`,
`NoOfCumulativeCustomer`,
`NoOfCustomerPercent`,
`Revenue_Center`,
`CLMSegmentName`)  
SELECT 
    'GLUEDISTRIBUTION' AS AGGREGATION,
    'L24M',
    NoOfDays,
    NoOfCustomer,
    - 1 AS NoOfCumulativeCustomer,
    100 * (NoOfCustomer / @totalcustomerL24M) AS NoOfCustomerPercent,
    'NA' AS Revenue_Center,
    'NA' AS CLMSegmentName
FROM
    Dashboard_Insights_BounceCurve
WHERE
    AGGREGATION = 'GLUE'
AND Measure = 'L24M'
;

INSERT INTO `Dashboard_Insights_BounceCurve`
(`AGGREGATION`,
`Measure`,
`NoOfDays`,
`NoOfCustomer`,
`NoOfCumulativeCustomer`,
`NoOfCustomerPercent`,
`Revenue_Center`,
`CLMSegmentName`)  
SELECT 
    'GLUEDISTRIBUTION' AS AGGREGATION,
    'ATM',
    NoOfDays,
    NoOfCustomer,
    - 1 AS NoOfCumulativeCustomer,
    100 * (NoOfCustomer / @totalcustomer) AS NoOfCustomerPercent,
    'NA' AS Revenue_Center,
    'NA' AS CLMSegmentName
FROM
    Dashboard_Insights_BounceCurve
WHERE
    AGGREGATION = 'GLUE'
AND Measure = 'ATM'
;

select 'GLUE';
OPEN cur_NoOfDaysGLUE;
BEGIN
	SET vprevcur_NoOfDaysGLUE = 'X';
	SET vCheckEndOfCursor=0;
	LOOP_cur_NoOfDaysGLUE : LOOP
		FETCH cur_NoOfDaysGLUE into vcur_NoOfDaysGLUE;
		IF vCheckEndOfCursor THEN    
			LEAVE LOOP_cur_NoOfDaysGLUE;
		END IF;
        
        UPDATE Dashboard_Insights_BounceCurve
		SET NoOfCumulativeCustomer=NoOfCustomer + ifnull((select ifnull(NoOfCumulativeCustomer,0) from Dashboard_Insights_BounceCurve where AGGREGATION = 'GLUE' AND Measure = 'ATM' and NoOfDays=vprevcur_NoOfDaysGLUE ),0)
		WHERE NoOfDays=vcur_NoOfDaysGLUE 
        AND AGGREGATION = 'GLUE'
        AND Measure = 'ATM';
        
        
        set @numerator=0;
        set @denominator=0;
        select ifnull(sum(NoOfCustomer),0) into @numerator from Dashboard_Insights_BounceCurve 
			where NoOfDays >= vcur_NoOfDaysGLUE + 1 AND AGGREGATION = 'GLUE' AND Measure = 'ATM';
	    select ifnull(sum(NoOfCustomer),0) into @denominator from Dashboard_Insights_BounceCurve 
			where NoOfDays >= vcur_NoOfDaysGLUE AND  AGGREGATION = 'GLUE' AND Measure = 'ATM';
		select vcur_NoOfDaysGLUE,@numerator,@denominator;
		IF @denominator >= 0
        THEN 
			UPDATE Dashboard_Insights_BounceCurve
			SET NoOfCustomerPercent=100*(ifnull(@numerator,0)/@denominator)
			WHERE NoOfDays=vcur_NoOfDaysGLUE
            AND AGGREGATION = 'GLUE'
            AND Measure = 'ATM';
		ELSE
        	UPDATE Dashboard_Insights_BounceCurve
			SET NoOfCustomerPercent=0
			WHERE NoOfDays=vcur_NoOfDaysGLUE
            AND AGGREGATION = 'GLUE'
            AND Measure = 'ATM';
		END IF;
     
		SET vprevcur_NoOfDaysGLUE=vcur_NoOfDaysGLUE;		
	END LOOP LOOP_cur_NoOfDaysGLUE;
    set @totalcustomer = 0;
    
END;


select 'GLUEL24M';
OPEN cur_NoOfDaysGLUEL24M;
BEGIN
	SET vprevcur_NoOfDaysGLUE = 'X';
	SET vCheckEndOfCursor=0;
	LOOP_cur_NoOfDaysGLUEL24M : LOOP
		FETCH cur_NoOfDaysGLUEL24M into vcur_NoOfDaysGLUE;
		IF vCheckEndOfCursor THEN    
			LEAVE LOOP_cur_NoOfDaysGLUEL24M;
		END IF;
        
        UPDATE Dashboard_Insights_BounceCurve
		SET NoOfCumulativeCustomer=NoOfCustomer + ifnull((select ifnull(NoOfCumulativeCustomer,0) from Dashboard_Insights_BounceCurve where AGGREGATION = 'GLUE' AND Measure = 'L24M' and NoOfDays=vprevcur_NoOfDaysGLUE ),0)
		WHERE NoOfDays=vcur_NoOfDaysGLUE 
        AND AGGREGATION = 'GLUE'
        AND Measure = 'L24M';
        
        
        set @numeratorL24M=0;
        set @denominatorL24M=0;
        select ifnull(sum(NoOfCustomer),0) into @numeratorL24M from Dashboard_Insights_BounceCurve 
			where NoOfDays >= vcur_NoOfDaysGLUE + 1 AND AGGREGATION = 'GLUE' AND Measure = 'L24M';
	    select ifnull(sum(NoOfCustomer),0) into @denominatorL24M from Dashboard_Insights_BounceCurve 
			where NoOfDays >= vcur_NoOfDaysGLUE AND  AGGREGATION = 'GLUE' AND Measure = 'L24M';
		select vcur_NoOfDaysGLUE,@numeratorL24M,@denominatorL24M;
		IF @denominator >= 0
        THEN 
			UPDATE Dashboard_Insights_BounceCurve
			SET NoOfCustomerPercent=100*(ifnull(@numeratorL24M,0)/@denominatorL24M)
			WHERE NoOfDays=vcur_NoOfDaysGLUE
            AND AGGREGATION = 'GLUE'
            AND Measure = 'L24M';
		ELSE
        	UPDATE Dashboard_Insights_BounceCurve
			SET NoOfCustomerPercent=0
			WHERE NoOfDays=vcur_NoOfDaysGLUE
            AND AGGREGATION = 'GLUE'
            AND Measure = 'L24M';
		END IF;
     
		SET vprevcur_NoOfDaysGLUE=vcur_NoOfDaysGLUE;		
	END LOOP LOOP_cur_NoOfDaysGLUEL24M;
    set @totalcustomer = 0;
    
END;

UPDATE Dashboard_Metrics_Log
SET `Status` = 'Ended' WHERE Screen_Name = 'INSIGHTS - GLUE NUMBER';

END$$
DELIMITER ;
