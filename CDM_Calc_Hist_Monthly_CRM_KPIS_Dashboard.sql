DELIMITER $$
CREATE or replace PROCEDURE `CDM_Calc_Hist_Monthly_CRM_KPIS_Dashboard`(IN vCalendarType TINYINT)
BEGIN
	
    DECLARE vKPI_Year SMALLINT;
	DECLARE vKPI_Curr_Year SMALLINT;
	DECLARE vKPI_Min_Date DATE;
	
	SELECT Bill_Date INTO vKPI_Min_Date FROM CDM_Bill_Header where Bill_Date>'2016-12-31' ORDER BY Bill_Date ASC LIMIT 1;
  
    IF vKPI_Min_Date IS NOT NULL THEN 
		SET vKPI_Year = EXTRACT(YEAR FROM vKPI_Min_Date); 		
		SET vKPI_Curr_Year = EXTRACT(YEAR FROM CURDATE()); 		
		SET vKPI_Year = vKPI_Year - 1; 

		CALL CDM_Update_Process_Log('CDM_Calc_Hist_Monthly_CRM_KPIs', 1, 'CDM_Calc_Hist_Monthly_CRM_KPIs', 1);
		
		PROCESS_HIST_REC: LOOP
			CALL CDM_Calculate_CRM_KPIS_Dashboard(vKPI_Year, 1, vCalendarType);		
			CALL CDM_Calculate_CRM_KPIS_Dashboard(vKPI_Year, 2, vCalendarType);		
			CALL CDM_Calculate_CRM_KPIS_Dashboard(vKPI_Year, 3, vCalendarType);		
			CALL CDM_Calculate_CRM_KPIS_Dashboard(vKPI_Year, 4, vCalendarType);		
			CALL CDM_Calculate_CRM_KPIS_Dashboard(vKPI_Year, 5, vCalendarType);		
			CALL CDM_Calculate_CRM_KPIS_Dashboard(vKPI_Year, 6, vCalendarType);		
			CALL CDM_Calculate_CRM_KPIS_Dashboard(vKPI_Year, 7, vCalendarType);		
			CALL CDM_Calculate_CRM_KPIS_Dashboard(vKPI_Year, 8, vCalendarType);		
			CALL CDM_Calculate_CRM_KPIS_Dashboard(vKPI_Year, 9, vCalendarType);		
			CALL CDM_Calculate_CRM_KPIS_Dashboard(vKPI_Year, 10, vCalendarType);		
			CALL CDM_Calculate_CRM_KPIS_Dashboard(vKPI_Year, 11, vCalendarType);		
			CALL CDM_Calculate_CRM_KPIS_Dashboard(vKPI_Year, 12, vCalendarType);		
		
			SET vKPI_Year = vKPI_Year + 1;
			IF vKPI_Year  > vKPI_Curr_Year THEN
				LEAVE PROCESS_HIST_REC;
			END IF;
		
		END LOOP PROCESS_HIST_REC;
		CALL CDM_Update_Process_Log('CDM_Calc_Hist_Monthly_CRM_KPIs', 2, 'CDM_Calc_Hist_Monthly_CRM_KPIs', 1);

	END IF;
	CALL CDM_Update_Process_Log('CDM_Calc_Hist_Monthly_CRM_KPIs', 3, 'CDM_Calc_Hist_Monthly_CRM_KPIs', 1);
	
END$$
DELIMITER ;
