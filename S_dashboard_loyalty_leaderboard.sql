DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_loyalty_leaderboard`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
	
	IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_1 = 'TOP_N'
	THEN 
			set @view_stmt = concat("create or replace  View V_Dashboard_Loyalty_leader_Table as 
											WITH GROUP1 AS
                            (
							select Store_name, Region
							from Dashboard_Loyalty_Leaderboard_Temp 
							group by Nation_rank
                            order by Nation_rank limit 20
                            )
                             ,
                             GROUP2 AS
							(
                            select Store_name, City
							from Dashboard_Loyalty_Leaderboard_Temp 
							group by Nation_rank
                            order by Nation_rank limit 20
                            )
							 ,
                             GROUP3 AS
							(
                            select Store_name, EmailcapPercent
							from Dashboard_Loyalty_Leaderboard_Temp 
                            WHERE EmailcapPercent IS NOT NULL
							group by Nation_rank
                            order by Nation_rank limit 20
                            )
                             ,
                             GROUP4 AS
							(
                            select Store_name, MobilecapPercent
							from Dashboard_Loyalty_Leaderboard_Temp 
                            WHERE MobilecapPercent IS NOT NULL
							group by Nation_rank
							order by Nation_rank limit 20
                            )
                             ,
                             GROUP5 AS
							(
                            select Store_name, Tagged_Percent
							from Dashboard_Loyalty_Leaderboard_Temp 
                            WHERE Tagged_Percent IS NOT NULL
							group by Nation_rank
							order by Nation_rank limit 20
                            )
							,
                             GROUP6 AS
							(
                            select Store_name, Enrollment_Percent
							from Dashboard_Loyalty_Leaderboard_Temp 
                            WHERE Enrollment_Percent IS NOT NULL
							group by Nation_rank
							order by Nation_rank limit 20
                            )
                            ,
                             GROUP7 AS
							(
                            select Store_name, 
							Region_rank
							from Dashboard_Loyalty_Leaderboard_Temp 
							group by Nation_rank
							order by Nation_rank limit 20
                            )
                            ,
                             GROUP8 AS
							(
                            select Store_name, Nation_rank
							from Dashboard_Loyalty_Leaderboard_Temp 
							group by Nation_rank
							order by Nation_rank limit 20
                            )
				SELECT DISTINCT A.Store_name, Region, City,EmailcapPercent, MobilecapPercent, Tagged_Percent, Enrollment_Percent,
				Region_rank, Nation_rank
                            FROM GROUP1 A
                            JOIN GROUP2 B
                            ON A.Store_name=B.Store_name
							JOIN GROUP3 C
                            ON B.Store_name=C.Store_name
                            JOIN GROUP4 D
                            ON C.Store_name=D.Store_name
                            JOIN GROUP5 E
                            ON D.Store_name=E.Store_name
                            JOIN GROUP6 F
                            ON E.Store_name=F.Store_name
                            JOIN GROUP7 G
                            ON F.Store_name=G.Store_name
                            JOIN GROUP8 H
                            ON G.Store_name=H.Store_name
			;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Store Name",`Store_name`,"Region",`Region`,"City",`City`,"% Mobile Capture",concat(`MobilecapPercent`,"%"),"% Email Capture",concat(`EmailcapPercent`,"%"),"% Bill Tagging",concat(`Tagged_Percent`,"%"),"% Enrollment",concat(`Enrollment_Percent`,"%"),"Region Rank",`Region_Rank`,"Nation Rank",`Nation_Rank` ) )),"]")into @temp_result from V_Dashboard_Loyalty_leader_Table;') ; 
        
END IF;

    	IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_1 = 'BOTTOM_N'
	THEN 
			set @view_stmt = concat("create or replace  View V_Dashboard_Loyalty_leader_Table as 
											WITH GROUP1 AS
                            (
							select Store_name, Region
							from Dashboard_Loyalty_Leaderboard_Temp 
							group by Nation_rank
                            order by Nation_rank DESC limit 20
                            )
                             ,
                             GROUP2 AS
							(
                            select Store_name, City
							from Dashboard_Loyalty_Leaderboard_Temp 
							group by Nation_rank
                            order by Nation_rank DESC limit 20
                            )
							 ,
                             GROUP3 AS
							(
                            select Store_name, EmailcapPercent
							from Dashboard_Loyalty_Leaderboard_Temp 
                            WHERE EmailcapPercent IS NOT NULL
							group by Nation_rank
                            order by Nation_rank DESC limit 20
                            )
                             ,
                             GROUP4 AS
							(
                            select Store_name, MobilecapPercent
							from Dashboard_Loyalty_Leaderboard_Temp 
                            WHERE MobilecapPercent IS NOT NULL
							group by Nation_rank
							order by Nation_rank DESC limit 20
                            )
                             ,
                             GROUP5 AS
							(
                            select Store_name, Tagged_Percent
							from Dashboard_Loyalty_Leaderboard_Temp 
                            WHERE Tagged_Percent IS NOT NULL
							group by Nation_rank
							order by Nation_rank DESC limit 20
                            )
							,
                             GROUP6 AS
							(
                            select Store_name, Enrollment_Percent
							from Dashboard_Loyalty_Leaderboard_Temp 
                            WHERE Enrollment_Percent IS NOT NULL
							group by Nation_rank
							order by Nation_rank DESC limit 20
                            )
                            ,
                             GROUP7 AS
							(
                            select Store_name, 
							Region_rank
							from Dashboard_Loyalty_Leaderboard_Temp 
							group by Nation_rank
							order by Nation_rank DESC limit 20
                            )
                            ,
                             GROUP8 AS
							(
                            select Store_name, Nation_rank
							from Dashboard_Loyalty_Leaderboard_Temp 
							group by Nation_rank
							order by Nation_rank DESC limit 20
                            )
				SELECT DISTINCT A.Store_name, Region, City,EmailcapPercent, MobilecapPercent, Tagged_Percent, Enrollment_Percent,
				Region_rank, Nation_rank
                            FROM GROUP1 A
                            JOIN GROUP2 B
                            ON A.Store_name=B.Store_name
							JOIN GROUP3 C
                            ON B.Store_name=C.Store_name
                            JOIN GROUP4 D
                            ON C.Store_name=D.Store_name
                            JOIN GROUP5 E
                            ON D.Store_name=E.Store_name
                            JOIN GROUP6 F
                            ON E.Store_name=F.Store_name
                            JOIN GROUP7 G
                            ON F.Store_name=G.Store_name
                            JOIN GROUP8 H
                            ON G.Store_name=H.Store_name
			;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Store Name",`Store_name`,"Region",`Region`,"City",`City`,"% Mobile Capture",concat(`MobilecapPercent`,"%"),"% Email Capture",concat(`EmailcapPercent`,"%"),"% Bill Tagging",concat(`Tagged_Percent`,"%"),"% Enrollment",concat(`Enrollment_Percent`,"%"),"Region Rank",`Region_Rank`,"Nation Rank",`Nation_Rank` ) )),"]")into @temp_result from V_Dashboard_Loyalty_leader_Table;') ; 
        
END IF;


	IF IN_SCREEN <> "DOWNLOAD"
    THEN
		select @view_stmt;
		PREPARE statement from @view_stmt;
		Execute statement;
		Deallocate PREPARE statement;
		select @Query_String;
		PREPARE statement from @Query_String;
		Execute statement;
		Deallocate PREPARE statement;	
		SET out_result_json = @temp_result;
	ELSE
		call S_dashboard_performance_download('V_Dashboard_Loyalty_leader_Table',@out_result_json1);
		select @out_result_json1 into out_result_json;
    END IF;
	
END$$
DELIMITER ;
