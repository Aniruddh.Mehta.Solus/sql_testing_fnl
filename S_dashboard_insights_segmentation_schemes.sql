DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_insights_segmentation_schemes`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
    declare IN_AGGREGATION_4 text;
    declare IN_AGGREGATION_5 text;
    declare IN_AGGREGATION_6 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
    SET IN_AGGREGATION_4 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_4"));
    SET IN_AGGREGATION_5 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_5"));
    SET IN_AGGREGATION_6 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_6"));
    
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    
	SET @filter_cond=concat(" AGGREGATION = '",IN_AGGREGATION_2,"' ");
    SET @measure_cond=concat(" Measure = '",IN_AGGREGATION_3,"' ");
   
      IF IN_AGGREGATION_1 = 'BASE'
    THEN 
		SET @query_str = '`Base`';
	END IF;
    
    IF IN_AGGREGATION_1 = 'RECENCY'
    THEN 
		SET @query_str = '`Recency`';
	END IF;
    
    IF IN_AGGREGATION_1 = 'FREQUENCY'
    THEN 
		SET @query_str = '`Frequency`';
	END IF;
    
    IF IN_AGGREGATION_1 = 'VALUE'
    THEN 
		SET @query_str = '`Value`';
	END IF;
    
    IF IN_AGGREGATION_1 = 'ABV'
    THEN 
		SET @query_str = '`ABV`';
	END IF;
    
    IF IN_AGGREGATION_1 = 'BASE_PERCENT'
    THEN 
		SET @query_str = '`Base_Percent`';
	END IF;
    
    IF IN_AGGREGATION_1 = 'BILLS_PERCENT'
    THEN 
		SET @query_str = '`Bills_Percent`';
	END IF;
    
    IF IN_AGGREGATION_1 = 'AMT_PERCENT'
    THEN 
		SET @query_str = '`Amt_Percent`';
	END IF;
    
	IF IN_MEASURE = 'TABLE' 
    THEN
    
		select @filter_cond;
		
        SET @view_stmt=CONCAT('CREATE OR REPLACE VIEW V_Dashboard_Insights_Segmentation_Schemes
			as
			SELECT Segmentation_Name, Base, Recency, `Value`, ABV, Frequency, Base_Percent, Bills_Percent, Amt_Percent FROM Dashboard_Insights_Segmentation_Schemes WHERE ',@measure_cond,' AND ',@filter_cond,'
                            ;');
            
		SET @Query_String =  concat( 'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Segmentation Name",`Segmentation_Name`,
        "Base",CAST(format(ifnull(`Base`,0),",") AS CHAR),"Recency",CAST(format(ifnull(`Recency`,0),",") AS CHAR),"Value",CAST(format(ifnull(`Value`,0),",") AS CHAR),"ABV",CAST(format(ifnull(`ABV`,0),",") AS CHAR),"Frequency",ifnull(`Frequency`,0),"% Base",concat(round(ifnull(`Base_Percent`,0),1),"%"),"% Bills",concat(round(ifnull(`Bills_Percent`,0),1),"%"),"% Amt",concat(round(ifnull(`Amt_Percent`,0),1),"%")) )),"]")into @temp_result from V_Dashboard_Insights_Segmentation_Schemes',';') ;
            
		END IF;
        
	IF IN_MEASURE = 'GRAPH' 
	THEN 
    
		select @filter_cond;
        
        set @view_stmt= CONCAT(' create or replace view V_Dashboard_Insights_Segmentation_Schemes_Graph as 
							select 
							Segmentation_Name, ',@query_str,' as "Value"
							from Dashboard_Insights_Segmentation_Schemes
							WHERE ',@filter_cond,'
                            AND ',@measure_cond,'
							group by 1;
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`Segmentation_Name`,"Value",`Value`) )),"]")into @temp_result from V_Dashboard_Insights_Segmentation_Schemes_Graph;' ;

	END IF;
		
 
    
    IF IN_MEASURE = 'SEGMENTATION_NAME'
    THEN
		set @view_stmt = concat('CREATE OR REPLACE VIEW V_Dashboard_Segmentation_Schemes_Names AS
								select distinct Segment_Scheme as `Segment_Scheme` from Solus_Segment WHERE Segment_Type = "Dashboard" ');
		SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Segmentation_Scheme",CAST(`Segment_Scheme` AS CHAR)) )),"]") into @temp_result from V_Dashboard_Segmentation_Schemes_Names;' ;
SELECT @Query_String;
    END IF;



    IF IN_SCREEN <> "DOWNLOAD"
    THEN
    select @view_stmt;
	PREPARE statement from @view_stmt;
	Execute statement;
	Deallocate PREPARE statement;
	SELECT @Query_String;
	PREPARE statement from @Query_String;
	Execute statement;
	Deallocate PREPARE statement; 
    SET out_result_json = @temp_result;
    ELSE
		call S_dashboard_performance_download('V_Dashboard_Insights_Conversions',@out_result_json1);
		SELECT @out_result_json1 INTO out_result_json;
    END IF;
    
END$$
DELIMITER ;
