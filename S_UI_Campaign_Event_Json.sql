	DELIMITER $$
	CREATE or replace PROCEDURE `S_UI_Campaign_Event_Json`()
	Begin
			DECLARE done1,done2 INT DEFAULT FALSE;
			DECLARE Event_ID_cur1_1 int;
			Declare Event_Condition_SVOC_1, Event_Condition_Bill_1  , Creative1,State1 text;


			DECLARE cur1 cursor for 
				SELECT 	Event_ID
				From  Event_Master_UI 
				where InputJson is null 
				order by Event_ID asc
				;

			DECLARE CONTINUE HANDLER FOR NOT FOUND SET done1 = TRUE;

			SET done1 = FALSE;
			SET SQL_SAFE_UPDATES=0;
			SET foreign_key_checks=0;

			
			SET done1 = FALSE;
		
		
			Select CONCAT("Updating the InputJson for all the Events");
		
			SET done1 = FALSE;
		
			OPEN cur1;
				BEGIN
					LOOP_ALL_EVENTS: LOOP

						FETCH cur1 into 
						  Event_ID_cur1_1;

							SET @Event_ID_cur1=Event_ID_cur1_1;

								Select @Event_ID_cur1;
								Select done1;
							IF done1 THEN
								   LEAVE LOOP_ALL_EVENTS;
							END IF; 
						
						
							SET @Query_String =  concat('Update Event_Master_UI A,Event_Master B
																Set Status_Tested ="OKTESTED"
														where A.Event_Id=B.Id and Event_Id=',@Event_ID_cur1,'
														and B.State="Testing" ;') ; 
																	
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;
							
							SET @Query_String =  concat('Update Event_Master_UI A,CLM_Campaign_Trigger B,CLM_Campaign_Events C
																Set  B.`CTState` = 3
														where C.CampTriggerId =B.CampTriggerId
																and A.Event_Id=C.EventId and EventId=',@Event_ID_cur1,'
														and  B.CTState = 2 ;') ; 
																	
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;
							
							
							SET @Query_String =  concat('Update Event_Master_UI A,Event_Master B
																Set Status_Tested ="TESTED",
																Status_QC ="QC",
																Status_DLT ="DLT",
																Status_Schedule="Schedule",
																Status_Active="Active"
														where A.Event_Id=B.Id and Event_Id=',@Event_ID_cur1,'
														and B.State="Enabled" ;') ; 
																	
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;
							
							
							
							SET @Query_String =  concat('Update Event_Master_UI A,Event_Master B
																Set Status_Tested ="TESTED",
																Status_QC ="QC",
																Status_DLT ="DLT",
																Status_Schedule="Schedule",
																Status_Active="InActive"
														where A.Event_Id=B.Id and Event_Id=',@Event_ID_cur1,'
														and B.State="Disabled" ;') ; 
																	
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;
							

						 
							SET @IN_USER_NAME = "";
							SET @IN_SCREEN = "TARGET_AUDIENCE";
							
							Select B.CampTriggerName into @Trigger_Name from CLM_Campaign_Events A,CLM_Campaign_Trigger B
							Where A.CampTriggerId=B.CampTriggerId and EventId=@Event_ID_cur1;
								
							Select A.CampaignId into @Existing_Campaign_Id from CLM_Campaign_Events A,CLM_Campaign B
							Where A.CampaignId=B.CampaignId and EventId=@Event_ID_cur1;
							
							Select A.CampaignThemeId into @Existing_Theme_Id from CLM_Campaign_Events A,CLM_CampaignTheme B
							Where A.CampaignThemeId=B.CampaignThemeId and EventId=@Event_ID_cur1;
							
							SET @Campaign_Type = "CLM";
							SET @Region_Id = "";
									
							Select A.MicroSegmentId into @Campaign_Segment_Id from CLM_Campaign_Events A,CLM_MicroSegment B
							Where A.MicroSegmentId=B.MicroSegmentId and EventId=@Event_ID_cur1;

							Select A.CLMSegmentId into @Lifecyle_Segment_Id from CLM_Campaign_Events A,CLM_Segment B
							Where A.CLMSegmentId=B.CLMSegmentId and EventId=@Event_ID_cur1;
							
							 SELECT CASE WHEN    CampaignUsesWaterfall =100  THEN  "PERFORMANCE_BASED" 
										 WHEN   CampaignUsesWaterfall ="-1" and A.Execution_Sequence in (Select min(Execution_Sequence) from CLM_Campaign_Events) THEN     "HIGH_PRIORITY"
										 WHEN   CampaignUsesWaterfall ="-1" and A.Execution_Sequence in (Select max(Execution_Sequence) from CLM_Campaign_Events) THEN     "LOW_PRIORITY"
										 ELSE "LOW_PRIORITY"
									 END INTO @Comm_Priority
							from CLM_Campaign_Events A,CLM_Campaign B
							Where A.CampaignId=B.CampaignId and EventId=@Event_ID_cur1;		 
									 
							Select  Case WHEN    CampaignUsesWaterfall = 99  THEN "IGNORE_COOL_OFF"  
										WHEN    CampaignUsesWaterfall = '-1' THEN "NORMAL_COOL_OFF"
										ELSE ""
									 END INTO @Comm_Cool_Off	
							from CLM_Campaign_Events A,CLM_Campaign B
							Where A.CampaignId=B.CampaignId and EventId=@Event_ID_cur1;								
				 
							
							
							SET @Execution_After = "";
												
							Select Event_Limit into @Throttling_Level from Event_Master where Id=@Event_ID_cur1;

							Select Communication_Cooloff into @Cool_Off from CLM_Campaign_Events A, CLM_Creative B
							Where A.CreativeId=B.CreativeId and EventId=@Event_ID_cur1;
											
							Select  Event_Response_Days into @Attribution from Event_Master where Id=@Event_ID_cur1;
											 
							SELECT CASE WHEN   B.CTReplacedQuery Like "%Lt_Fav_Day_Of_Week%" THEN "YES" 
										else   "NO"
								   END INTO @Favourite_Day
							from CLM_Campaign_Events A,CLM_Campaign_Trigger B
							Where A.CampTriggerId=B.CampTriggerId and EventId=@Event_ID_cur1;
									   
							
							
							SET @Recency_Start = "";
							SET @Recency_End = "";
							SET @Vintage =  "";
							SET @Visit =  "";
							SET @Monetary_Value =  "";
							SET @Feedback_Received = "";
							SET @Last_Bought_Category = "";
							SET @Favourite_Category =  "";
							SET @Items_in_cart_Avl =  "";

											
							
												
							Select Communication_Channel into @Channel from Event_Master A, Communication_Template B 
							Where A.CreativeId1=B.Id and  A.Id=@Event_ID_cur1;

							Select RankedPickListStory_Id into @Personalised_Reccomendation
							from Event_Master where Id=@Event_ID_cur1;
							
							If @Personalised_Reccomendation IS NULL
							  THEN SET @Personalised_Reccomendation ='';
							END IF;
							
							Select Website_URL into @MicroSite_URL from Event_Master where Id=@Event_ID_cur1;
							
							If @MicroSite_URL IS NULL
							  THEN SET @MicroSite_URL ='';
							END IF;
							
							
							SET @Personalised_Reco_via_Solus_Shortener = "";
											
							Select Template into @Personalization from Event_Master A, Communication_Template B 
							Where A.CreativeId1=B.Id and  A.Id=@Event_ID_cur1;
							
							 SELECT CASE WHEN  Valid_Mobile = "1" THEN "YES" 
										else   "NO"
								   END INTO @Valid_Mobile
							from Event_Master_UI Where  Event_Id=@Event_ID_cur1; 
							
							SELECT CASE WHEN  Valid_Email = "1" THEN "YES" 
										else   "NO"
								   END INTO @Valid_Email
							from Event_Master_UI Where  Event_Id=@Event_ID_cur1;
							
							SELECT CASE WHEN  DND = "1" THEN "YES" 
										else   "NO"
								   END INTO @DND
							from Event_Master_UI Where  Event_Id=@Event_ID_cur1; 
							
							SELECT Reco_Based  INTO @Reco_Based
							from Event_Master_UI Where  Event_Id=@Event_ID_cur1;
							
						
							
							Select  D.OfferTypeName into @Offer_Type_Name from CLM_Campaign_Events A,CLM_Creative B,CLM_Offer C,CLM_OfferType D
							where A.CreativeId=B.CreativeId	and B.OfferId=C.OfferId and C.OfferTypeId=D.OfferTypeId	and A.EventId=@Event_ID_cur1;
							
							If  @Offer_Type_Name="NO_OFFER"
							Then
									SET @Offer_Based ="YES";
								Else SET @Offer_Based ="NO";
							
							END IF;
												
							Select @IN_USER_NAME,@IN_SCREEN,@Trigger_Name,@Existing_Campaign_Id,@Existing_Theme_Id,@Campaign_Type,@Region_Id,@Campaign_Segment_Id,
							@Lifecyle_Segment_Id,@Comm_Cool_Off,@Comm_Priority,@Execution_After,@Throttling_Level,@Cool_Off,@Attribution,@Favourite_Day,@Recency_Start,@Recency_End,
							@Vintage,@Visit,@Monetary_Value,@Feedback_Received,@Last_Bought_Category,@Favourite_Category,@Items_in_cart_Avl,@Channel,
							@Personalised_Reccomendation,@Personalised_Reco_via_Solus_Shortener,@Personalization,@Valid_Mobile,@Valid_Email,@DND,@Reco_Based,@Offer_Based,@MicroSite_URL;

											
							SET @Query_String =  concat('SELECT CONCAT((GROUP_CONCAT(json_object("USER_NAME","","SCREEN","',@IN_SCREEN,'","Trigger_Name","',@Trigger_Name,'","Existing_Campaign","',@Existing_Campaign_Id,'","Existing_Theme","',@Existing_Theme_Id,'","Campaign_Type","',@Campaign_Type,'","Region","',@Region_Id,'","Campaign_Segment",',@Campaign_Segment_Id,',"Lifecyle_Segment","',@Lifecyle_Segment_Id,'","Execution_After","',@Execution_After,'","Throttling_Level","',@Throttling_Level,'","Comm_Cool_Off","',@Comm_Cool_Off,'","Cool_Off","',@Cool_Off,'","Comm_Priority","',@Comm_Priority,'","Attribution","',@Attribution,'","Favourite_Day","',@Favourite_Day,'","Recency_Start","',@Recency_Start,'","Recency_End","',@Recency_End,'","Vintage","',@Vintage,'","Visit","',@Visit,'","Monetary_Value","',@Monetary_Value,'","Feedback_Received","',@Feedback_Received,'","Last_Bought_Category","',@Last_Bought_Category,'","Favourite_Category","',@Favourite_Category,'","Items_in_cart_Avl","',@Items_in_cart_Avl,'","Channel","',@Channel,'","Personalised_Reccomendation","',@Personalised_Reccomendation,'","Personalised_Reco_via_Solus_Shortener","',@Personalised_Reco_via_Solus_Shortener,'","Personalization","',@Personalization,'","Valid_Mobile","',@Valid_Mobile,'","Valid_Email","',@Valid_Email,'","DND","',@DND,'","Offer_Based","',@Offer_Based,'","Reco_Based","',@Reco_Based,'","Microsite_URL","',@MicroSite_URL,'") )))
																into @temp_result;') ;
																
																
																
																	  
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;
																			 

							SET @Query_String =  concat('Update Event_Master_UI
																Set InputJson =@temp_result
														where Event_Id=',@Event_ID_cur1,';') ; 
																	
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;
							
							
						if @Event_ID_cur1=(Select max(Event_Id) from Event_Master_UI)
							then SET done1 = TRUE;
						Else SET done1 = FALSE;	
						End If;	

				
				   END LOOP LOOP_ALL_EVENTS;

			   END;	
			close cur1;
				
				
		
		
		
	end$$
	DELIMITER ;
