DELIMITER $$
CREATE  OR REPLACE PROCEDURE S_LOOPING_temp_EEH(IN vStart_DateId bigint,IN vEnd_DateId bigint,IN vBatchSize bigint)
BEGIN
DECLARE vStart_Cnt BIGINT DEFAULT 0;
DECLARE vEnd_Cnt BIGINT;
SELECT Customer_Id INTO vStart_Cnt FROM Event_Execution_History where Event_Execution_Date_ID between vStart_DateId and vEnd_DateId ORDER BY Customer_Id ASC LIMIT 1;
SELECT Customer_Id INTO @Rec_Cnt FROM Event_Execution_History where Event_Execution_Date_ID between vStart_DateId and vEnd_DateId ORDER BY Customer_Id DESC LIMIT 1;

PROCESS_LOOP: LOOP
SET vEnd_Cnt = vStart_Cnt + vBatchSize;
insert ignore into  temp_Event_Execution_History 
select * from Event_Execution_History where Customer_Id between vStart_Cnt and vEnd_Cnt and Event_Execution_Date_ID between vStart_DateId and vEnd_DateId; 
SET vStart_Cnt = vEnd_Cnt;

IF vStart_Cnt  >= @Rec_Cnt THEN
	LEAVE PROCESS_LOOP;
END IF;        
END LOOP PROCESS_LOOP;  
END$$
DELIMITER ;
