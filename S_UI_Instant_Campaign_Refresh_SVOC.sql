
CREATE or replace PROCEDURE `S_UI_Instant_Campaign_Refresh_SVOC`()
BEGIN

DECLARE done1,done2 INT DEFAULT FALSE;
declare v_Column_Name_in_One_View varchar(512);
DECLARE cur1 cursor for 
select Column_Name_in_One_View from Customer_View_Config where Table_Name ="Client_SVOC" AND In_Use_Customer_One_View ="Y";
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done1 = TRUE;
SET done1 = FALSE;
SET SQL_SAFE_UPDATES=0;

insert ignore into Campaign_Segment_Dictionary(UI_Variable_Name,SOLUS_Attribute,Variable_Category)
select Column_Name_in_One_View,Column_Name_in_One_View,'Custom Variable' from Customer_View_Config 
where Table_Name ="Client_SVOC" AND In_Use_Customer_One_View ="Y";
 

SET done1 = FALSE;
OPEN cur1;

LOOP_ALL_EVENTS: LOOP
	FETCH cur1 into v_Column_Name_in_One_View;
	IF done1 THEN
	   LEAVE LOOP_ALL_EVENTS;
	END IF;


SET @sqlcnt=concat('select count(distinct(',v_Column_Name_in_One_View,')) into @cnt from Client_SVOC ;');

PREPARE stmt1 from @sqlcnt; EXECUTE stmt1; DEALLOCATE PREPARE stmt1;
                
if @cnt > 30
then set @UI_Data_Type = 'Continous' ;

else
set @UI_Data_Type =  'list';

set @col_name = v_Column_Name_in_One_View;

SET @sqlcnt=concat('
update Campaign_Segment_Dictionary
set UI_Value_List =  (select group_concat(concat("{Val:", t1.c1,"}")) from (
select distinct(',@col_name,') as c1
from Client_SVOC) t1),
UI_Data_Type = ','"',@UI_Data_Type,'"','
where SOLUS_Attribute = ','"',@col_name,'"',';
');

select @sqlcnt;
PREPARE stmt1 from @sqlcnt; EXECUTE stmt1; DEALLOCATE PREPARE stmt1;
   
end if;


END LOOP LOOP_ALL_EVENTS;	
CLOSE cur1;

set sql_safe_updates=0;
update Campaign_Segment_Dictionary
set UI_Value_List = replace(UI_Value_List,'{Val:','{"Val":"')
where Variable_Category = 'Custom Variable';

update Campaign_Segment_Dictionary
set UI_Value_List = replace(UI_Value_List,'}','"}')
where Variable_Category = 'Custom Variable';

END

