DELIMITER $$
CREATE or replace PROCEDURE `S_dashboard_insights_metrics_T12existing_new`(IN vBatchSize bigint(10), in vStartCntInput bigint(10), in vYYYYMM_StartMonth int(6), IN vAPMonth int(2),IN vLapserMonth int(2), IN vFYMonth int(2))
BEGIN
DECLARE vStart_Cnt 		BIGINT DEFAULT 0;
DECLARE vEnd_Cnt 		BIGINT;
DECLARE vCurYYYYMM 		int(6);
DECLARE vCurYYYY 		int(4);

DECLARE vCheckEndOfCursor int Default 0;

DECLARE curALL_MONTHS CURSOR FOR 
SELECT DISTINCT 
	Bill_Year_Month
FROM
    CDM_Bill_Details
where Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt
and Bill_Year_Month >= vYYYYMM_StartMonth 
ORDER BY Bill_Year_Month ASC;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET vCheckEndOfCursor=1;
SET SQL_SAFE_UPDATES=0;
Delete from log_solus where module='S_dashboard_insights_metrics';

SELECT 
    Customer_Id
INTO @Rec_Cnt FROM
    CDM_Customer_Master
ORDER BY Customer_Id DESC
LIMIT 1;
insert into log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_insights_metrics',-1,-1,'INITIALIZE');
select 'Version No : 07-Mar 8.24 AM' as '';
select 'select * from log_solus where module=\'S_dashboard_insights_metrics\' order by id desc;' as '';

CREATE OR REPLACE TABLE All_Dates as select distinct bill_date from CDM_Bill_Details;
CREATE OR REPLACE TABLE All_Revenue_Center as select distinct Revenue_Center from CDM_Revenue_Center;
CREATE OR REPLACE TABLE All_CLMSegmentName as select distinct CLMSegmentName from CDM_Customer_Segment;
delete from All_Revenue_Center where Revenue_Center='NA';
insert into All_Revenue_Center values ('NA');
delete from All_CLMSegmentName where CLMSegmentName='NA';
insert into All_CLMSegmentName values ('NA');
CREATE OR REPLACE TABLE Dashboard_Insights_Base_CURRENTRUN LIKE Dashboard_Insights_Base;
TRUNCATE Dashboard_Insights_Base_CURRENTRUN;
INSERT IGNORE INTO Dashboard_Insights_Base_CURRENTRUN
SELECT * from Dashboard_Insights_Base;

INSERT IGNORE INTO Dashboard_Insights_Base_CURRENTRUN
		(
			AGGREGATION,
			YYYYMMDD,
			YYYYMM,
			YYYY,
			Revenue_Center,
            CLMSegmentName
		)
SELECT
	'BY_DAY',
	date_format(bill_date,'%Y%m%d') as YYYYMMDD,
	date_format(bill_date,'%Y%m') as YYYYMM,
	date_format(bill_date,'%Y') as YYYY,
	Revenue_Center,
	CLMSegmentName
FROM All_Dates, All_Revenue_Center, All_CLMSegmentName;
INSERT IGNORE INTO Dashboard_Insights_Base_CURRENTRUN
		(
			AGGREGATION,
			YYYYMMDD,
			YYYYMM,
			YYYY,
			Revenue_Center,
            CLMSegmentName
		)
SELECT
	'BY_MONTH',
	-1 as YYYYMMDD,
	date_format(bill_date,'%Y%m') as YYYYMM,
	date_format(bill_date,'%Y') as YYYY,
	Revenue_Center,
	CLMSegmentName
FROM All_Dates, All_Revenue_Center, All_CLMSegmentName;
   
INSERT IGNORE INTO Dashboard_Insights_Base_CURRENTRUN
		(
			AGGREGATION,
			YYYYMMDD,
			YYYYMM,
			YYYY,
			Revenue_Center,
            CLMSegmentName
		)
SELECT
	'BY_YEAR',
	-1 as YYYYMMDD,
	-1 as YYYYMM,
	date_format(bill_date,'%Y') as YYYY,
	Revenue_Center,
	CLMSegmentName
FROM All_Dates, All_Revenue_Center, All_CLMSegmentName;

insert into log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_insights_metrics',-1,-1,'Dashboard Metrics Initialized');

-- SET vStart_Cnt=0;
SET vStart_Cnt=vStartCntInput;
LOOP_ALL_CUSTOMERS: LOOP
	SET vEnd_Cnt = vStart_Cnt + vBatchSize;
	IF vStart_Cnt  >= @Rec_Cnt THEN
		LEAVE LOOP_ALL_CUSTOMERS;
	END IF;  
    
	select vStart_Cnt, vEnd_Cnt, current_timestamp();
    insert into log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_insights_metrics',vStart_Cnt,vEnd_Cnt,'STARTING BATCH');
    
	TRUNCATE Dashboard_CDM_Bill_Details_ForOneBatch;
	INSERT INTO Dashboard_CDM_Bill_Details_ForOneBatch 
    (
    YYYYMMDD,
    YYYYMM,
    YYYY,
    Customer_Id,
    Lineitems,
    Bills, 
    Visits, 
    Revenue, 
    Discount
    )
	SELECT
		date_format(bill_date, '%Y%m%d') 			as YYYYMMDD,
		Bill_Year_Month 		as YYYYMM,
        Bill_Year  				as YYYY,
		bill.Customer_Id		as Customer_Id,
        count(distinct bill.Bill_Details_Id) 		as Lineitems,
		count(distinct bill.Bill_Header_Id) 		as Bills,
		count(distinct bill.Bill_Date) 				as Visits,
		sum(bill.Sale_Net_Val) 						as Revenue,
		sum(bill.Sale_Disc_Val) 					as Discount
    FROM
        CDM_Bill_Details bill
	WHERE
		bill.Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt
        and bill_date is not null
	group by Customer_Id, bill_date;
    
	insert into log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_insights_metrics',vStart_Cnt,vEnd_Cnt,'Load Bills');
    
    UPDATE Dashboard_CDM_Bill_Details_ForOneBatch dashboard
    LEFT JOIN CDM_Revenue_Center revcenter ON dashboard.Customer_Id=revcenter.Customer_Id
    SET dashboard.Revenue_Center=ifnull(revcenter.Revenue_Center,'NA');

	UPDATE Dashboard_CDM_Bill_Details_ForOneBatch dashboard 
    LEFT JOIN CDM_Customer_Segment seg ON dashboard.Customer_Id=seg.Customer_Id
    SET dashboard.CLMSegmentName=ifnull(seg.CLMSegmentName,'NA');
    
    insert into log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_insights_metrics',vStart_Cnt,vEnd_Cnt,'Load Revenue Center and CLM Segment');

	SET vCheckEndOfCursor=0;
	OPEN curALL_MONTHS;
	LOOP_ALL_MONTHS : LOOP
		FETCH curALL_MONTHS into vCurYYYYMM;
		IF vCheckEndOfCursor THEN    
			LEAVE LOOP_ALL_MONTHS;
		END IF;
        
		set vCurYYYY			=	substring(vCurYYYYMM,1,4);
		set @vAPYYYYMM			=	F_Month_Diff(vCurYYYYMM,vAPMonth);
		set @vLapserYYYYMM		=	F_Month_Diff(vCurYYYYMM,vLapserMonth);
		set @vFYYYYYMM			=	F_Month_Diff(vCurYYYYMM,vFYMonth); 
		set @vQtrYYYYMM			=	F_Month_Diff(vCurYYYYMM,3);
		set @v12MthsYYYYMM		=	F_Month_Diff(vCurYYYYMM,12);
	insert into log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_insights_metrics',vStart_Cnt,vCurYYYYMM,'BY_MONTH Base');
	
insert into log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_insights_metrics',vStart_Cnt,vCurYYYYMM,'BY_MONTH T12MthNew');

UPDATE Dashboard_Insights_Base_CURRENTRUN dashboard,
    (SELECT 
        'T12MthNew',
            COUNT(DISTINCT Customer_Id) AS count,
            SUM(Revenue) AS Revenue,
            Revenue_Center,
            CLMSegmentName
    FROM Dashboard_CDM_Bill_Details_ForOneBatch a
    WHERE a.YYYYMM BETWEEN @v12MthsYYYYMM AND vCurYYYYMM
            AND NOT EXISTS( SELECT 
                Customer_Id
            FROM
                Dashboard_CDM_Bill_Details_ForOneBatch b
            WHERE
                a.Customer_Id = b.Customer_Id
                    AND b.YYYYMM < @v12MthsYYYYMM)
    GROUP BY Revenue_Center , CLMSegmentName) temp 
	SET 
		dashboard.T12MthNewBase = dashboard.T12MthNewBase + temp.count,
		dashboard.T12MthNewRevenue = dashboard.T12MthNewRevenue + temp.revenue
	WHERE
    dashboard.YYYYMM = vCurYYYYMM
        AND AGGREGATION = 'BY_MONTH'
        AND dashboard.Revenue_Center = temp.Revenue_Center
        AND dashboard.CLMSegmentName = temp.CLMSegmentName;

	insert into log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_insights_metrics',vStart_Cnt,vCurYYYYMM,'BY_MONTH T12MthExisting');

	UPDATE Dashboard_Insights_Base_CURRENTRUN dashboard,
    (SELECT 
        'T12MthExisting',
            COUNT(DISTINCT Customer_Id) AS count,
            SUM(Revenue) AS Revenue,
            Revenue_Center,
            CLMSegmentName
    FROM
        Dashboard_CDM_Bill_Details_ForOneBatch a
    WHERE
       a.YYYYMM BETWEEN @v12MthsYYYYMM AND vCurYYYYMM
            AND EXISTS( SELECT 
                Customer_Id
            FROM
                Dashboard_CDM_Bill_Details_ForOneBatch b
            WHERE
                a.Customer_Id = b.Customer_Id
                    AND b.YYYYMM < @v12MthsYYYYMM)
    GROUP BY Revenue_Center , CLMSegmentName) temp 
	SET 
		dashboard.T12MthExistingBase 	= dashboard.T12MthExistingBase + temp.count,
		dashboard.T12MthExistingRevenue = dashboard.T12MthExistingRevenue + temp.revenue
	WHERE
    dashboard.YYYYMM = vCurYYYYMM
        AND AGGREGATION = 'BY_MONTH'
        AND dashboard.Revenue_Center = temp.Revenue_Center
        AND dashboard.CLMSegmentName = temp.CLMSegmentName;
        
	insert into log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_insights_metrics',vStart_Cnt,vCurYYYYMM,'BY_MONTH T12MthMulti');
	UPDATE Dashboard_Insights_Base_CURRENTRUN dashboard,
		(SELECT 
        'T12MthMulti',
            COUNT(Customer_Id) AS count,
            SUM(Revenue) AS Revenuex,
            SUM(Discount) AS Discountx,
            SUM(Bills) AS Billsx,
            SUM(Visits) AS visitsx,
            Revenue_Center,
            CLMSegmentName
			FROM (
			SELECT 
			Customer_Id,
			SUM(Revenue) AS Revenue,
			SUM(Discount) AS Discount,
			SUM(Bills) AS Bills,
			SUM(Visits) AS Visits,
			YYYYMM,
			Revenue_Center, 
			CLMSegmentName
		FROM
			Dashboard_CDM_Bill_Details_ForOneBatch
		WHERE
			YYYYMM BETWEEN @v12MthsYYYYMM AND vCurYYYYMM
		GROUP BY Customer_Id
		HAVING SUM(visits) > 1) temp1 
		 WHERE YYYYMM BETWEEN @v12MthsYYYYMM AND vCurYYYYMM GROUP BY Revenue_Center, CLMSegmentName) temp 
			SET 
				dashboard.T12MthMulti 		= dashboard.T12MthMulti + temp.count,
				dashboard.T12MthMultiRevenue 	= dashboard.T12MthMultiRevenue	+ temp.Revenuex,
				dashboard.T12MthMultiDiscount 	= dashboard.T12MthMultiDiscount 	+ temp.Discountx,
				dashboard.T12MthMultiBills 	= dashboard.T12MthMultiBills 		+ temp.Billsx,
				dashboard.T12MthMultiVisits 	= dashboard.T12MthMultiVisits 	+ temp.Visitsx
			WHERE
			dashboard.YYYYMM = vCurYYYYMM
				AND AGGREGATION = 'BY_MONTH'
				AND dashboard.Revenue_Center = temp.Revenue_Center
				AND dashboard.CLMSegmentName = temp.CLMSegmentName;

		update Dashboard_Insights_Base_CURRENTRUN dashboard
			SET 
				dashboard.T12MthSingle 				= dashboard.T12MthActive -  dashboard.T12MthMulti,
				dashboard.T12MthSingleRevenue 		= dashboard.T12MthActiveRevenue -  dashboard.T12MthMultiRevenue,
				dashboard.T12MthSingleDiscount 		= dashboard.T12MthActiveDiscount -  dashboard.T12MthMultiDiscount,
				dashboard.T12MthSingleBills 		= dashboard.T12MthActiveBills -  dashboard.T12MthMultiBills,
                dashboard.T12MthSingleVisits 		= dashboard.T12MthActiveVisits -  dashboard.T12MthMultiVisits
		where dashboard.YYYYMM = vCurYYYYMM
				AND AGGREGATION = 'BY_MONTH';
     

	END LOOP LOOP_ALL_MONTHS;
    COMMIT;
	CLOSE curALL_MONTHS;
    
SET vStart_Cnt = vEnd_Cnt;
END LOOP LOOP_ALL_CUSTOMERS; 

-- CALL S_dashboard_insights_metrics_initialize(vYYYYMM_StartMonth);

select concat('Refreshing Main Table Starts :',current_timestamp());
CREATE OR REPLACE TABLE Dashboard_Insights_Base_LASTRUN LIKE Dashboard_Insights_Base;
TRUNCATE Dashboard_Insights_Base_LASTRUN;
INSERT INTO Dashboard_Insights_Base_LASTRUN SELECT * FROM Dashboard_Insights_Base;
TRUNCATE Dashboard_Insights_Base;
INSERT INTO Dashboard_Insights_Base SELECT * FROM Dashboard_Insights_Base_CURRENTRUN;
select YYYYMM,sum(MonthlyActiveRevenue),sum(MonthlyActive) from Dashboard_Insights_Base_CURRENTRUN group by YYYYMM;
select YYYYMM,sum(MonthlyActiveRevenue),sum(MonthlyActive) from Dashboard_Insights_Base_LASTRUN group by YYYYMM;
select YYYYMM,sum(MonthlyActiveRevenue),sum(MonthlyActive) from Dashboard_Insights_Base group by YYYYMM;

select concat('Refreshing Main Table End Table :',current_timestamp());

END$$
DELIMITER ;
