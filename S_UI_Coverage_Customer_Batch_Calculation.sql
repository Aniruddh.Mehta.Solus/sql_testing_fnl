
CREATE OR REPLACE PROCEDURE `S_UI_Coverage_Customer_Batch_Calculation`(IN SQL_Stmt TEXT,IN vBatchSize int(8), OUT Output BIGINT(20))
BEGIN
DECLARE Loop_Check_Var Int Default 0;
DECLARE vStart_Cnt BIGINT DEFAULT 0;
DECLARE vEnd_Cnt BIGINT DEFAULT 0;
DECLARE vRec_Cnt BIGINT DEFAULT 0;
DECLARE Result BIGINT DEFAULT 0;


DECLARE CONTINUE HANDLER FOR NOT FOUND SET Loop_Check_Var=1;
SELECT Customer_Id INTO vRec_Cnt FROM Customer_Coverage_View ORDER BY Customer_Id DESC LIMIT 1;
SET sql_safe_updates=0;

select concat('MAX Customer Id: ',vRec_Cnt);
SET Loop_Check_Var = FALSE;

    
	SET vStart_Cnt=0;
	SET vEnd_Cnt=0;
    
    Select now() as 'Start';
	PROCESS_LOOP: LOOP
		SET vEnd_Cnt = vStart_Cnt + vBatchSize;
		
        SET @Batch_Result = 0;
		SET @sql_stmt = SQL_Stmt;
        
        select CONCAT(@sql_stmt);

                
				PREPARE statement from @sql_stmt;
				Execute statement;
				Deallocate PREPARE statement; 
                
		SET 
		Result = Result + @Batch_Result;
		
	   
		

		SET vStart_Cnt = vEnd_Cnt +1;
		IF vStart_Cnt  >= vRec_Cnt THEN
			LEAVE PROCESS_LOOP;
		END IF;      
        
	END LOOP PROCESS_LOOP;   
    
SET Output = Result;

select now() as 'End';

END

