DELIMITER $$
CREATE or replace PROCEDURE `S_dashboard_explore_variable_metrics`()
BEGIN
-- DECLARE cur_NoOfDays CURSOR FOR SELECT DISTINCT NoOfDays FROM Dashboard_Insights_BounceCurve where AGGREGATION='NTN1' ORDER BY NoOfDays ASC;

-- DECLARE cur_NoOfDaysNTR CURSOR FOR SELECT DISTINCT NoOfDays FROM Dashboard_Insights_BounceCurve where AGGREGATION='NTR' ORDER BY NoOfDays ASC;

-- DECLARE CONTINUE HANDLER FOR NOT FOUND SET vCheckEndOfCursor=1;


SET SQL_SAFE_UPDATES=0;

create or replace table  Dashboard_Explore_Variable
(
	`Attribute` VARCHAR(256),
    `Segment` VARCHAR(256),
    `Mean` VARCHAR(64) default 0,
    `Median` VARCHAR(64) default 0,
    `Mode` VARCHAR(64) default 0,
    `SD` VARCHAR(64) default 0,
    `Value` VARCHAR(64) default -1,
    primary key(Attribute,Segment),
    key(Value)
);

-- truncate Dashboard_Explore_Variable;

-- RECENCY 		

INSERT INTO Dashboard_Explore_Variable(`Attribute`,`Segment`,`Value`)
select 'Recency',c.Description as "Segment", round(avg(Recency),0) Value from CDM_Customer_Time_Dependent_Var a join svoc_segments b on a.Customer_Id=b.Customer_Id
join (select * from svoc_segments_master where SVOC_Segment_For_Model_Id=3) c on b.Seg_For_model_3 = c.Name
where a.Recency>0
group by 2
having Value > 0
order by c.Sequence;

UPDATE Dashboard_Explore_Variable a ,(select 'Recency' as Attribute, round(avg(Recency)) as 'Mean', round(STD(Recency)) as 'SD' from CDM_Customer_Time_Dependent_Var where Recency>0) b
SET a.Mean=b.Mean,
a.SD=b.SD
where a.Attribute=b.Attribute;

UPDATE Dashboard_Explore_Variable a ,(select 'Recency' as Attribute,group_concat(`value`) as `Mode` from
(SELECT Recency as "value", count(1) as occurs FROM CDM_Customer_Time_Dependent_Var where Recency>0 GROUP BY 1) T1 WHERE occurs = (select max(occurs) from (select Recency as `value`, count(1) as occurs from CDM_Customer_Time_Dependent_Var where Recency>0 group by 1) t)) b
set a.Mode=b.Mode
where a.Attribute=b.Attribute;

UPDATE Dashboard_Explore_Variable a ,(select 'Recency' as `Attribute`,MAX(Recency) as `Median` from 
(select Recency,ntile(2) over(order by Recency) as bin from CDM_Customer_Time_Dependent_Var where Recency>0) a
where bin =1) b
set a.Median=b.Median
where a.Attribute=b.Attribute;

-- Frequency(ever)


INSERT INTO Dashboard_Explore_Variable(`Attribute`,`Segment`,`Value`)
select 'Frequency(ever)',c.Description as "Segment", round(avg(Lt_Num_of_Visits),1) Value from CDM_Customer_TP_Var a join svoc_segments b on a.Customer_Id=b.Customer_Id
join (select * from svoc_segments_master where SVOC_Segment_For_Model_Id=3) c on b.Seg_For_model_3 = c.Name
where a.Lt_Num_of_Visits>0
group by 2
having Value > 0
order by c.Sequence;

UPDATE Dashboard_Explore_Variable a ,(select 'Frequency(ever)' as Attribute, round(avg(Lt_Num_of_Visits),1) as 'Mean', round(STD(Lt_Num_of_Visits),1) as 'SD' from CDM_Customer_TP_Var where Lt_Num_of_Visits>0) b
SET a.Mean=b.Mean,
a.SD=b.SD
where a.Attribute=b.Attribute;

UPDATE Dashboard_Explore_Variable a ,(select 'Frequency(ever)' as Attribute,group_concat(`value`) as `Mode` from
(SELECT Lt_Num_of_Visits as "value", count(1) as occurs FROM CDM_Customer_TP_Var where Lt_Num_of_Visits>0 GROUP BY 1) T1 WHERE occurs = (select max(occurs) from (select Lt_Num_of_Visits as `value`, count(1) as occurs from CDM_Customer_TP_Var where Lt_Num_of_Visits>0 group by 1) t)) b
set a.Mode=b.Mode
where a.Attribute=b.Attribute;

UPDATE Dashboard_Explore_Variable a ,(select 'Frequency(ever)' as `Attribute`,MAX(Lt_Num_of_Visits) as `Median` from 
(select Lt_Num_of_Visits,ntile(2) over(order by Lt_Num_of_Visits) as bin from CDM_Customer_TP_Var where Lt_Num_of_Visits>0) a
where bin =1) b
set a.Median=b.Median
where a.Attribute=b.Attribute;

-- ADGBT

INSERT INTO Dashboard_Explore_Variable(`Attribute`,`Segment`,`Value`)
select 'ADGBT',c.Description as "Segment", round(avg(Lt_ADGBT),0) Value from CDM_Customer_TP_Var a join svoc_segments b on a.Customer_Id=b.Customer_Id
join (select * from svoc_segments_master where SVOC_Segment_For_Model_Id=3) c on b.Seg_For_model_3 = c.Name
where a.Lt_Num_of_Visits>1
group by 2
having Value > 0
order by c.Sequence;

UPDATE Dashboard_Explore_Variable a ,(select 'ADGBT' as Attribute, round(avg(Lt_ADGBT)) as 'Mean', round(STD(Lt_ADGBT)) as 'SD' from CDM_Customer_TP_Var where Lt_Num_of_Visits>1) b
SET a.Mean=b.Mean,
a.SD=b.SD
where a.Attribute=b.Attribute;

UPDATE Dashboard_Explore_Variable a ,(select 'ADGBT' as Attribute,group_concat(`value`) as `Mode` from
(SELECT Lt_ADGBT as "value", count(1) as occurs FROM CDM_Customer_TP_Var where Lt_Num_of_Visits>1 GROUP BY 1) T1 WHERE occurs = (select max(occurs) from (select Lt_ADGBT as `value`, count(1) as occurs from CDM_Customer_TP_Var where Lt_Num_of_Visits>1 group by 1) t)) b
set a.Mode=b.Mode
where a.Attribute=b.Attribute;

UPDATE Dashboard_Explore_Variable a ,(select 'ADGBT' as `Attribute`,MAX(Lt_ADGBT) as `Median` from 
(select Lt_ADGBT,ntile(2) over(order by Lt_ADGBT) as bin from CDM_Customer_TP_Var where Lt_Num_of_Visits>1) a
where bin =1) b
set a.Median=b.Median
where a.Attribute=b.Attribute;

-- TENURE

INSERT INTO Dashboard_Explore_Variable(`Attribute`,`Segment`,`Value`)
select 'Tenure',c.Description as "Segment", round(avg(Tenure),0) Value from CDM_Customer_TP_Var a join svoc_segments b on a.Customer_Id=b.Customer_Id
join (select * from svoc_segments_master where SVOC_Segment_For_Model_Id=3) c on b.Seg_For_model_3 = c.Name
where a.Lt_Num_of_Visits>-1
group by 2
having Value > 0
order by c.Sequence;

UPDATE Dashboard_Explore_Variable a ,(select 'Tenure' as Attribute, round(avg(Tenure)) as 'Mean', round(STD(Tenure)) as 'SD' from CDM_Customer_TP_Var where Lt_Num_of_Visits>-1) b
SET a.Mean=b.Mean,
a.SD=b.SD
where a.Attribute=b.Attribute;

UPDATE Dashboard_Explore_Variable a ,(select 'Tenure' as Attribute,group_concat(`value`) as `Mode` from
(SELECT Tenure as "value", count(1) as occurs FROM CDM_Customer_TP_Var where Lt_Num_of_Visits>-1 GROUP BY 1) T1 WHERE occurs = (select max(occurs) from (select Tenure as `value`, count(1) as occurs from CDM_Customer_TP_Var where Lt_Num_of_Visits>-1 group by 1) t)) b
set a.Mode=b.Mode
where a.Attribute=b.Attribute;

UPDATE Dashboard_Explore_Variable a ,(select 'Tenure' as `Attribute`,MAX(Tenure) as `Median` from 
(select Tenure,ntile(2) over(order by Tenure) as bin from CDM_Customer_TP_Var where Lt_Num_of_Visits>=0) a
where bin =1) b
set a.Median=b.Median
where a.Attribute=b.Attribute;

-- Value(ever)

INSERT INTO Dashboard_Explore_Variable(`Attribute`,`Segment`,`Value`)
select 'Value(ever)',c.Description as "Segment", round(avg(Lt_Rev),0) Value from CDM_Customer_TP_Var a join svoc_segments b on a.Customer_Id=b.Customer_Id
join (select * from svoc_segments_master where SVOC_Segment_For_Model_Id=3) c on b.Seg_For_model_3 = c.Name
where a.Lt_Num_of_Visits>1
group by 2
having Value > 0
order by c.Sequence;

UPDATE Dashboard_Explore_Variable a ,(select 'Value(ever)' as Attribute, round(avg(Lt_Rev)) as 'Mean', round(STD(Lt_Rev)) as 'SD' from CDM_Customer_TP_Var where Lt_Num_of_Visits>1) b
SET a.Mean=b.Mean,
a.SD=b.SD
where a.Attribute=b.Attribute;

UPDATE Dashboard_Explore_Variable a ,(select 'Value(ever)' as Attribute,group_concat(`value`) as `Mode` from
(SELECT Lt_Rev as "value", count(1) as occurs FROM CDM_Customer_TP_Var where Lt_Num_of_Visits>1 GROUP BY 1) T1 WHERE occurs = (select max(occurs) from (select Lt_Rev as `value`, count(1) as occurs from CDM_Customer_TP_Var where Lt_Num_of_Visits>1 group by 1) t)) b
set a.Mode=b.Mode
where a.Attribute=b.Attribute;

UPDATE Dashboard_Explore_Variable a ,(select 'Value(ever)' as `Attribute`,MAX(Lt_Rev) as `Median` from 
(select Lt_Rev,ntile(2) over(order by Lt_Rev) as bin from CDM_Customer_TP_Var where Lt_Num_of_Visits>1) a
where bin =1) b
set a.Median=b.Median
where a.Attribute=b.Attribute;

-- Value(12mth)

/*INSERT INTO Dashboard_Explore_Variable(`Attribute`,`Segment`,`Value`)
select 'Value(12mth)',c.Description as "Segment", round(avg(Sale_Net_Val),0) Value from CDM_Bill_Details a join svoc_segments b on a.Customer_Id=b.Customer_Id
join (select * from svoc_segments_master where SVOC_Segment_For_Model_Id=3) c on b.Seg_For_model_3 = c.Name
where Bill_Date>date_sub(current_Date(),interval 12 month)
group by 2
having Value > 0
order by c.Sequence;

INSERT INTO Dashboard_Explore_Variable(`Attribute`,`Segment`,`Value`)
select 'Value(12mth)',CLMSegmentName as "Segment", round(avg(value1),0) as `Value` from (
select CLMSegmentName,a.Customer_Id,SUM(Sale_Net_Val) as value1 from CDM_Bill_Details a join CDM_Customer_Segment b on a.Customer_Id=b.Customer_Id
where a.Bill_Date>date_sub(current_Date(),interval 12 month)
group by a.Customer_Id)A
group by CLMSegmentName;*/



INSERT INTO Dashboard_Explore_Variable(`Attribute`,`Segment`,`Value`)
(select 'Value(12mth)',CLMSegmentName as "Segment",round(SUM(T12MthActiveRevenue)/SUM(T12MthActive),0) as `Value` from Dashboard_Insights_Base
where YYYYMM = date_format(Current_date(),"%Y%m")
and AGGREGATION = 'BY_MONTH'
group by CLMSegmentName);

UPDATE Dashboard_Explore_Variable a ,(select 'Value(12mth)' as Attribute, round(avg(value1)) as 'Mean', round(STD(value1)) as 'SD' from  (
select CLMSegmentName,a.Customer_Id,SUM(Sale_Net_Val) as value1 from CDM_Bill_Details a join CDM_Customer_Segment b on a.Customer_Id=b.Customer_Id
where a.Bill_Date>date_sub(current_Date(),interval 12 month)
group by a.Customer_Id)A) b
SET a.Mean=b.Mean,
a.SD=b.SD
where a.Attribute=b.Attribute;

UPDATE Dashboard_Explore_Variable a ,(select 'Value(12mth)' as Attribute,group_concat(`value`) as `Mode` from
(SELECT value1 as "value", count(1) as occurs FROM (
select CLMSegmentName,a.Customer_Id,SUM(Sale_Net_Val) as value1 from CDM_Bill_Details a join CDM_Customer_Segment b on a.Customer_Id=b.Customer_Id
where a.Bill_Date>date_sub(current_Date(),interval 12 month)
group by a.Customer_Id)A GROUP BY 1) T1 WHERE occurs = (select max(occurs) from (select value1 as `value`, count(1) as occurs from (
select CLMSegmentName,a.Customer_Id,SUM(Sale_Net_Val) as value1 from CDM_Bill_Details a join CDM_Customer_Segment b on a.Customer_Id=b.Customer_Id
where a.Bill_Date>date_sub(current_Date(),interval 12 month)
group by a.Customer_Id)A group by 1) t)) b
set a.Mode=b.Mode
where a.Attribute=b.Attribute;

UPDATE Dashboard_Explore_Variable a ,(select 'Value(12mth)' as `Attribute`,round(MAX(value1)) as `Median` from 
(select value1,ntile(2) over(order by value1) as bin from (
select CLMSegmentName,a.Customer_Id,SUM(Sale_Net_Val) as value1 from CDM_Bill_Details a join CDM_Customer_Segment b on a.Customer_Id=b.Customer_Id
where a.Bill_Date>date_sub(current_Date(),interval 12 month)
group by a.Customer_Id)A) a
where bin =1) b
set a.Median=b.Median
where a.Attribute=b.Attribute;

-- ABV

INSERT INTO Dashboard_Explore_Variable(`Attribute`,`Segment`,`Value`)
(select 'ABV',CLMSegmentName as "Segment",round(SUM(BaseRevenue)/SUM(BaseBills),0) as `Value` from Dashboard_Insights_Base
where YYYYMM = date_format(Current_date(),"%Y%m")
and AGGREGATION = 'BY_MONTH'
group by CLMSegmentName);

/*INSERT INTO Dashboard_Explore_Variable(`Attribute`,`Segment`,`Value`)
select 'ABV',c.Description as "Segment", round(avg(Lt_ABV),0) Value from CDM_Customer_TP_Var a join svoc_segments b on a.Customer_Id=b.Customer_Id
join (select * from svoc_segments_master where SVOC_Segment_For_Model_Id=3) c on b.Seg_For_model_3 = c.Name
where a.Lt_Num_of_Visits>1
group by 2
having Value > 0
order by c.Sequence;
*/
UPDATE Dashboard_Explore_Variable a ,(select 'ABV' as Attribute, (select round(SUM(BaseRevenue)/SUM(BaseBills),0) from Dashboard_Insights_Base
where YYYYMM = date_format(Current_date(),"%Y%m")
and AGGREGATION = 'BY_MONTH') as 'Mean', round(STD(Lt_ABV)) as 'SD' from CDM_Customer_TP_Var where Lt_Num_of_Visits>1) b
SET a.Mean=b.Mean,
a.SD=b.SD
where a.Attribute=b.Attribute;

UPDATE Dashboard_Explore_Variable a ,(select 'ABV' as Attribute,group_concat(`value`) as `Mode` from
(SELECT Lt_ABV as "value", count(1) as occurs FROM CDM_Customer_TP_Var where Lt_Num_of_Visits>1 GROUP BY 1) T1 WHERE occurs = (select max(occurs) from (select Lt_ABV as `value`, count(1) as occurs from CDM_Customer_TP_Var where Lt_Num_of_Visits>1 group by 1) t)) b
set a.Mode=b.Mode
where a.Attribute=b.Attribute;

UPDATE Dashboard_Explore_Variable a ,(select 'ABV' as `Attribute`,MAX(Lt_ABV) as `Median` from 
(select Lt_ABV,ntile(2) over(order by Lt_ABV) as bin from CDM_Customer_TP_Var where Lt_Num_of_Visits>1) a
where bin =1) b
set a.Median=b.Median
where a.Attribute=b.Attribute;

-- Discount%

-- select * from Dashboard_Insights_Base;


INSERT INTO Dashboard_Explore_Variable(`Attribute`,`Segment`,`Value`)
select 'Discount%',c.Description as "Segment", round(avg(Lt_Disc_Percent),1) Value from CDM_Customer_TP_Var a join svoc_segments b on a.Customer_Id=b.Customer_Id
join (select * from svoc_segments_master where SVOC_Segment_For_Model_Id=3) c on b.Seg_For_model_3 = c.Name
where a.Lt_Num_of_Visits>1
group by 2
having Value > 0
order by c.Sequence;

UPDATE Dashboard_Explore_Variable a ,(select 'Discount%' as Attribute, round(avg(Lt_Disc_Percent),1) as 'Mean', round(STD(Lt_Disc_Percent),1) as 'SD' from CDM_Customer_TP_Var where Lt_Num_of_Visits>1) b
SET a.Mean=b.Mean,
a.SD=b.SD
where a.Attribute=b.Attribute;

UPDATE Dashboard_Explore_Variable a ,(select 'Discount%' as Attribute,group_concat(`value`) as `Mode` from
(SELECT Lt_Disc_Percent as "value", count(1) as occurs FROM CDM_Customer_TP_Var where Lt_Num_of_Visits>1 GROUP BY 1) T1 WHERE occurs = (select max(occurs) from (select Lt_Disc_Percent as `value`, count(1) as occurs from CDM_Customer_TP_Var where Lt_Num_of_Visits>1 group by 1) t)) b
set a.Mode=b.Mode
where a.Attribute=b.Attribute;

UPDATE Dashboard_Explore_Variable a ,(select 'Discount%' as `Attribute`,MAX(Lt_Disc_Percent) as `Median` from 
(select Lt_Disc_Percent,ntile(2) over(order by Lt_Disc_Percent) as bin from CDM_Customer_TP_Var where Lt_Num_of_Visits>1) a
where bin =1) b
set a.Median=b.Median
where a.Attribute=b.Attribute;

-- Points

INSERT INTO Dashboard_Explore_Variable(`Attribute`,`Segment`,`Value`)
select 'Point Balance',c.Description as "Segment", round(avg(Point_Balance),0) Value from CDM_Customer_Master a join svoc_segments b on a.Customer_Id=b.Customer_Id
join (select * from svoc_segments_master where SVOC_Segment_For_Model_Id=3) c on b.Seg_For_model_3 = c.Name
join CDM_Customer_TP_Var tpv on a.Customer_Id=tpv.Customer_Id
where tpv.Lt_Num_of_Visits>1
group by 2
having Value > 0
order by c.Sequence;

UPDATE Dashboard_Explore_Variable a ,(select 'Point Balance' as Attribute, round(avg(Point_Balance)) as 'Mean', round(STD(Point_Balance)) as 'SD' from CDM_Customer_Master a join CDM_Customer_TP_Var tpv on a.Customer_Id=tpv.Customer_Id where Lt_Num_of_Visits>1) b
SET a.Mean=b.Mean,
a.SD=b.SD
where a.Attribute=b.Attribute;

UPDATE Dashboard_Explore_Variable a ,(select 'Point Balance' as Attribute,group_concat(`value`) as `Mode` from
(SELECT Point_Balance as "value", count(1) as occurs FROM CDM_Customer_Master a join CDM_Customer_TP_Var tpv on a.Customer_Id=tpv.Customer_Id where Lt_Num_of_Visits>1 GROUP BY 1) T1 WHERE occurs = (select max(occurs) from (select Point_Balance as `value`, count(1) as occurs from CDM_Customer_Master a join CDM_Customer_TP_Var tpv on a.Customer_Id=tpv.Customer_Id where Lt_Num_of_Visits>1 group by 1) t)) b
set a.Mode=b.Mode
where a.Attribute=b.Attribute;

UPDATE Dashboard_Explore_Variable a ,(select 'Point Balance' as `Attribute`,MAX(Point_Balance) as `Median` from 
(select Point_Balance,ntile(2) over(order by Point_Balance) as bin from CDM_Customer_Master a join CDM_Customer_TP_Var tpv on a.Customer_Id=tpv.Customer_Id where Lt_Num_of_Visits>1) a
where bin =1) b
set a.Median=b.Median
where a.Attribute=b.Attribute;

-- Redemption

INSERT INTO Dashboard_Explore_Variable(`Attribute`,`Segment`,`Value`)
select 'Redemption',c.Description as "Segment", round(avg(Lt_Pts_Redemption_FD),0) Value from CDM_Customer_TP_Var a join svoc_segments b on a.Customer_Id=b.Customer_Id
join (select * from svoc_segments_master where SVOC_Segment_For_Model_Id=3) c on b.Seg_For_model_3 = c.Name
where a.Lt_Num_of_Visits>1
group by 2
having Value > 0
order by c.Sequence;

UPDATE Dashboard_Explore_Variable a ,(select 'Redemption' as Attribute, round(avg(Lt_Pts_Redemption_FD)) as 'Mean', round(STD(Lt_Pts_Redemption_FD)) as 'SD' from CDM_Customer_TP_Var where Lt_Num_of_Visits>1) b
SET a.Mean=b.Mean,
a.SD=b.SD
where a.Attribute=b.Attribute;

UPDATE Dashboard_Explore_Variable a ,(select 'Redemption' as Attribute,group_concat(`value`) as `Mode` from
(SELECT Lt_Pts_Redemption_FD as "value", count(1) as occurs FROM CDM_Customer_TP_Var where Lt_Num_of_Visits>1 GROUP BY 1) T1 WHERE occurs = (select max(occurs) from (select Lt_Pts_Redemption_FD as `value`, count(1) as occurs from CDM_Customer_TP_Var where Lt_Num_of_Visits>1 group by 1) t)) b
set a.Mode=b.Mode
where a.Attribute=b.Attribute;

UPDATE Dashboard_Explore_Variable a ,(select 'Redemption' as `Attribute`,MAX(Lt_Pts_Redemption_FD) as `Median` from 
(select Lt_Pts_Redemption_FD,ntile(2) over(order by Lt_Pts_Redemption_FD) as bin from CDM_Customer_TP_Var where Lt_Num_of_Visits>1) a
where bin =1) b
set a.Median=b.Median
where a.Attribute=b.Attribute;

-- Product Range

INSERT INTO Dashboard_Explore_Variable(`Attribute`,`Segment`,`Value`)
select 'Product Range',c.Description as "Segment", round(avg(LT_Distinct_Prod_Cnt),1) Value from CDM_Customer_TP_Var a join svoc_segments b on a.Customer_Id=b.Customer_Id
join (select * from svoc_segments_master where SVOC_Segment_For_Model_Id=3) c on b.Seg_For_model_3 = c.Name
where a.Lt_Num_of_Visits>1
group by 2
having Value > 0
order by c.Sequence;

UPDATE Dashboard_Explore_Variable a ,(select 'Product Range' as Attribute, round(avg(LT_Distinct_Prod_Cnt),1) as 'Mean', round(STD(LT_Distinct_Prod_Cnt),1) as 'SD' from CDM_Customer_TP_Var where Lt_Num_of_Visits>1) b
SET a.Mean=b.Mean,
a.SD=b.SD
where a.Attribute=b.Attribute;

UPDATE Dashboard_Explore_Variable a ,(select 'Product Range' as Attribute,group_concat(`value`) as `Mode` from
(SELECT LT_Distinct_Prod_Cnt as "value", count(1) as occurs FROM CDM_Customer_TP_Var where Lt_Num_of_Visits>1 GROUP BY 1) T1 WHERE occurs = (select max(occurs) from (select LT_Distinct_Prod_Cnt as `value`, count(1) as occurs from CDM_Customer_TP_Var where Lt_Num_of_Visits>1 group by 1) t)) b
set a.Mode=b.Mode
where a.Attribute=b.Attribute;

UPDATE Dashboard_Explore_Variable a ,(select 'Product Range' as `Attribute`,MAX(LT_Distinct_Prod_Cnt) as `Median` from 
(select LT_Distinct_Prod_Cnt,ntile(2) over(order by LT_Distinct_Prod_Cnt) as bin from CDM_Customer_TP_Var where Lt_Num_of_Visits>1) a
where bin =1) b
set a.Median=b.Median
where a.Attribute=b.Attribute;

-- Cat Range

INSERT INTO Dashboard_Explore_Variable(`Attribute`,`Segment`,`Value`)
select 'Cat Range',c.Description as "Segment", round(avg(LT_Distinct_Cat_Cnt),1) Value from CDM_Customer_TP_Var a join svoc_segments b on a.Customer_Id=b.Customer_Id
join (select * from svoc_segments_master where SVOC_Segment_For_Model_Id=3) c on b.Seg_For_model_3 = c.Name
where a.Lt_Num_of_Visits>1
group by 2
having Value > 0
order by c.Sequence;

UPDATE Dashboard_Explore_Variable a ,(select 'Cat Range' as Attribute, round(avg(LT_Distinct_Cat_Cnt),1) as 'Mean', round(STD(LT_Distinct_Cat_Cnt),1) as 'SD' from CDM_Customer_TP_Var where Lt_Num_of_Visits>1) b
SET a.Mean=b.Mean,
a.SD=b.SD
where a.Attribute=b.Attribute;

UPDATE Dashboard_Explore_Variable a ,(select 'Cat Range' as Attribute,group_concat(`value`) as `Mode` from
(SELECT LT_Distinct_Cat_Cnt as "value", count(1) as occurs FROM CDM_Customer_TP_Var where Lt_Num_of_Visits>1 GROUP BY 1) T1 WHERE occurs = (select max(occurs) from (select LT_Distinct_Cat_Cnt as `value`, count(1) as occurs from CDM_Customer_TP_Var where Lt_Num_of_Visits>1 group by 1) t)) b
set a.Mode=b.Mode
where a.Attribute=b.Attribute;

UPDATE Dashboard_Explore_Variable a ,(select 'Cat Range' as `Attribute`,MAX(LT_Distinct_Cat_Cnt) as `Median` from 
(select LT_Distinct_Cat_Cnt,ntile(2) over(order by LT_Distinct_Cat_Cnt) as bin from CDM_Customer_TP_Var where Lt_Num_of_Visits>1) a
where bin =1) b
set a.Median=b.Median
where a.Attribute=b.Attribute;


/*

		set @view_stmt = concat('create or replace view V_Dashboard_explore_Variable_table as
								select "Recency" as "Variable",round(avg(Recency)) as Value from CDM_Customer_Time_Dependent_Var
                                UNION
                                select "Frequency(12mth)",round(avg(Lt_Num_of_Visits),1) as Value from CDM_Customer_TP_Var
                                UNION
                                select "Frequency(ever)",round(avg(Lt_Num_of_Visits),1) as Value from CDM_Customer_TP_Var
                                UNION
                                select "ADGBT",round(avg(Lt_ADGBT)) as Value from CDM_Customer_TP_Var
                                UNION
                                select "Tenure", round(avg(Tenure)) as Value from CDM_Customer_TP_Var
                                UNION
                                select "Value(12mth)", round(avg(Sale_Net_Val)) as Value from CDM_Bill_Details where Bill_Date>date_sub(current_Date(),interval 12 month)
                                UNION
                                select "Value(ever)", round(avg(Lt_Rev)) as Value from CDM_Customer_TP_Var
                                UNION
                                select "ABV", round(avg(Lt_ABV)) as Value from CDM_Customer_TP_Var
                                UNION
                                select "Discount%", round(avg(Lt_Disc_Percent)) as Value from CDM_Customer_TP_Var
                                UNION
                                select "Points", round(avg(ifnull(Point_Balance,0))) as Value from CDM_Customer_Master
                                UNION
                                select "Redemption", round(avg(ifnull(Lt_Pts_Redemption_FD,0))) as Value from CDM_Customer_TP_Var
                                UNION
                                select "Product Range", round(avg(ifnull(LT_Distinct_Prod_Cnt,1))) as Value from CDM_Customer_TP_Var
                                UNION
                                select "Cat Range", round(avg(ifnull(LT_Distinct_Cat_Cnt,1))) as Value from CDM_Customer_TP_Var
                                ');*/
-- insert into log_solus(module,rec_start, rec_end,msg) values ('S_dashboard_explore_variable',ifnull(@x,0), ifnull(@y,0),'NTR Done');
	
END$$
DELIMITER ;
