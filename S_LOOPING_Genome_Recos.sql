DELIMITER $$
CREATE or Replace PROCEDURE `S_LOOPING_Genome_Recos`()
BEGIN
DECLARE vStart_Cnt BIGINT DEFAULT 0;
DECLARE vEnd_Cnt BIGINT;
SELECT Customer_Id INTO @Rec_Cnt FROM CDM_Customer_Master ORDER BY Customer_Id DESC LIMIT 1;
Create or replace table RankedPickList_Temp
SELECT 
    Customer_id, pm.Product_Genome_LOV_Id, Hybridizer_Rank
FROM
    RankedPickList rpl,     CDM_Product_Master pm
WHERE 1<> 1 ;
alter table RankedPickList_Temp add index (Customer_id);
alter table RankedPickList_Temp add index (Product_Genome_LOV_Id);
alter table RankedPickList_Temp add index (Hybridizer_Rank);

TRUNCATE SVOC_Genome_Recos_Temp;
PROCESS_LOOP: LOOP
SET vEnd_Cnt = vStart_Cnt + 1000;
select vEnd_Cnt,vStart_Cnt,current_timestamp();

INSERT INTO RankedPickList_Temp
SELECT 
    Customer_id, pm.Product_Genome_LOV_Id,Hybridizer_Rank
FROM
    RankedPickList rpl,     CDM_Product_Master pm
WHERE   customer_id  between vStart_Cnt and vEnd_Cnt-1
        AND Hybridizer_Rank <= 8
        and rpl.Product_Id=pm.product_id
GROUP BY Customer_id,pm.Product_Genome_LOV_Id;

INSERT INTO SVOC_Genome_Recos_Temp(Customer_Key,Product_Key,Reco_Rank)
SELECT Customer_Key,Product_Key,rank from 
(
SELECT 
    reco.Customer_Id,
    pm.Product_Id,
    ROW_NUMBER() OVER (PARTITION BY Customer_Id order by Reco_Qty_Total DESC  ) as rank
FROM
    SVOC_Genome_CosSimilar_Recommendation reco,
    CDM_Customer_Master cm,
    CDM_Product_Master pm,
    CDM_LOV_Master lov
WHERE
    reco.customer_id  between vStart_Cnt and vEnd_Cnt-1
        AND reco.customer_id 	= cm.customer_id
        AND reco.reco_id 		= pm.Product_Genome_LOV_ID
        AND pm.Custom_Tag4_Lov_id = lov.lov_id
        and reco.reco_id not in (select Product_Genome_LOV_ID from RankedPickList_Temp where Customer_id = reco.customer_id)
group by reco.customer_id,reco.reco_id
) TEMP,     
	CDM_Product_Key_Lookup pmlkp,
    CDM_Customer_Key_Lookup cmlkp
where rank <= 8
AND TEMP.product_id = pmlkp.product_id
AND TEMP.customer_id = cmlkp.customer_id;


SET vStart_Cnt = vEnd_Cnt;
IF vStart_Cnt  >= @Rec_Cnt THEN
	LEAVE PROCESS_LOOP;
END IF;      

END LOOP PROCESS_LOOP;  
END$$
DELIMITER ;
