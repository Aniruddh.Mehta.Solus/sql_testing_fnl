DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_UI_Campaign_Details`(IN in_request_json json, OUT out_result_json json)
BEGIN

    -- User Information
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 

    -- Target Audience	
    declare Requested_Details text;
    declare Input_Data  NVARCHAR(65536);
	declare Input_Data2  NVARCHAR(65536);
    declare Input_Data3  NVARCHAR(65536);
	declare Input_Data4  NVARCHAR(65536);
	declare Input_Data5  NVARCHAR(65536);
	declare Input_Data6  NVARCHAR(65536);
	 
   -- User Information
	Set in_request_json = Replace(in_request_json,'\n','\\n');
	Set in_request_json = Replace(in_request_json,'\\n','');
    Set in_request_json = Replace(in_request_json,'\\','');
   
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));

    -- Target Audience

	SET Requested_Details = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET Input_Data= json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET Input_Data2= json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET Input_Data3= json_unquote(json_extract(in_request_json,"$.AGGREGATION_4"));
	SET Input_Data4= json_unquote(json_extract(in_request_json,"$.AGGREGATION_5"));
	SET Input_Data5= json_unquote(json_extract(in_request_json,"$.AGGREGATION_6"));
	SET Input_Data6= json_unquote(json_extract(in_request_json,"$.AGGREGATION_7"));
    SET @temp_result = Null;
      
    -- Featching Trigger Details
    
    if Requested_Details='Existing_Trigger' and Input_Data !='' 
     Then
     				Set Input_Data =Replace(Input_Data,'','_');
					Set Input_Data =Replace(Input_Data,' ','_');
					SET @Query_String = concat('
					SELECT CampTriggerName into @CampTriggerName FROM CLM_Campaign_Trigger where CampTriggerName ="',Input_Data,'";') ;
						      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 

     				If @CampTriggerName  = Input_Data
     				Then
			 			
		                    SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Trigger Name Already Present") )),"]")
													    into @temp_result 
													   ;') ;
						      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 

					Else 
							SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Trigger Name Not Present") )),"]")
													    into @temp_result 
													   ;') ;
						      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
							
					End If;

					if @temp_result is null
						then set @temp_result="[]";
					End if;

	End If;


    -- Checking_Existing_Campaign
    
    if Requested_Details='Checking_Existing_Campaign' and Input_Data !='' 
       Then
     				SELECT CampaignName into @CampName FROM CLM_Campaign where CampaignName =Input_Data;

     				If @CampName  = Input_Data
     				Then
			 			
		                    SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Campaign Name Already Present") )),"]")
													    into @temp_result 
													   ;') ;
						      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 

					Else 
							SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Campaign Name Not Present") )),"]")
													    into @temp_result 
													   ;') ;
						      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
							
					End If;

					if @temp_result is null
						then set @temp_result="[]";
					End if;

	End If;

    -- Featching Campaign Details
    
    if Requested_Details='Existing_Campaign' 
       Then
	 
	        SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Campaign_ID",`CampaignId`,"Campaign_Name",`CampaignName`) )),"]")
									    into @temp_result from  CLM_Campaign
									   ;') ;
                                       SELECT @temp_result;
                                       
					      
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement; 
						
			If @temp_result is null
			   then set @temp_result="[]";
			End if;

	End If;
	
	
	
	
    -- Checking_Existing_Theme
    
    if Requested_Details='Checking_Existing_Theme' and Input_Data !='' 
     Then
     				SELECT CampaignThemeName into @CampThemeName FROM CLM_CampaignTheme where CampaignThemeName =Input_Data;

     				If @CampThemeName  = Input_Data
     				   Then
			 			
		                    SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Theme Name Already Present") )),"]")
													    into @temp_result 
													   ;') ;
						      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 

					Else 
							SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Theme Name Not Present") )),"]")
													    into @temp_result 
													   ;') ;
						      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
							
					End If;

					if @temp_result is null
						Then set @temp_result="[]";
					End if;

	End If;

    -- Featching Theme Details
    
    if Requested_Details= 'Existing_Theme' and Input_Data !='' 
       Then
	   
			
				SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Theme_ID",`CampaignThemeId`,"Theme_Name",`CampaignThemeName`) )),"]")
											into @temp_result from  CLM_CampaignTheme
										   ;') ;
						  
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 
				
			
					
			if @temp_result is null
			   Then set @temp_result="[]";
			End if;				
					
    End if; 

	
		
    -- Checking_Existing_Segment
    
    if Requested_Details='Checking_Existing_CampaignSegment' and Input_Data !='' 
     Then
     				SELECT MicroSegmentName into @CampaignSegmentName FROM CLM_MicroSegment where MicroSegmentName =Input_Data;

     				If @CampaignSegmentName  = Input_Data
     				   Then
			 			
		                    SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Campaign Segment Name Already Present") )),"]")
													    into @temp_result 
													   ;') ;
													      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 

					Else 
							SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Campaign Segment Name Not Present") )),"]")
													    into @temp_result 
													  ;') ;
						      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
							
					End If;

					If @temp_result is null
						Then set @temp_result="[]";
					End if;

	End If;

-- Fetching Instant Campaign Details
    
        if Requested_Details='Existing_Instant_Campaign' and Input_Data !='' 
     Then	
				select Trigger_Name into @InstantTriggerName from Event_Master_Instant_Campaign where Trigger_Name=Input_Data limit 1;
     				
     				If @InstantTriggerName  = Input_Data
     				Then
			 			
		                    SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Trigger Name Already Present") )),"]")
													    into @temp_result 
													   ;') ;
						      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 

					Else 
							SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Trigger Name Not Present") )),"]")
													    into @temp_result 
													   ;') ;
						      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
							
					End If;

					if @temp_result is null
						then set @temp_result="[]";
					End if;

	End If;

-- Fetching Instant Campaign Segment Details


If Requested_Details= 'Instant_Campaign_Segment'
       Then
          SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Campaign_Segment_ID",`MicroSegmentId`,"Campaign_Segment_Name",`MicroSegmentName`) )),"]")
									  into @temp_result from  CLM_MicroSegment
									;') ;
				      
		   SELECT @Query_String;
		   PREPARE statement from @Query_String;
		   Execute statement;
		   Deallocate PREPARE statement; 
					
		   If @temp_result is null
			 Then set @temp_result="[]";
		    End if;				
					
    End if; 


      -- Featching Campaign Segment Details
    
    If Requested_Details= 'Campaign_Segment'
       Then
				
          SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Campaign_Segment_ID",`Segment_Id`,"Campaign_Segment_Name",`Segment_Name`,"Campaign_Segment_Condition",`Segment_Condition_Solus`) )),"]")
									  into @temp_result from Campaign_Segment
                                      
                                      WHERE Segment_Scheme = "CLM Segment"
                                      ORDER BY ModifiedDate DESC
									;') ;
				      
		   SELECT @Query_String;
		   PREPARE statement from @Query_String;
		   Execute statement;
		   Deallocate PREPARE statement; 
					
		   If @temp_result is null
			 Then set @temp_result="[]";
		    End if;				
			
					
    End if; 

	If Requested_Details= 'Campaign_Segment_Condition' and Input_Data!='' and Input_Data2 != ''
       Then
        			Set Input_Data= replace (Input_Data,"[",'');
					Set Input_Data= replace (Input_Data,"]",'');
					Set Input_Data= replace (Input_Data,'"','');
					Set Input_Data= replace (Input_Data,', ',',');
                    
           
            IF Input_Data = 'DEFAULT'
            THEN 
                
                SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Campaign_Segment_Condition","") )),"]")
										  into @temp_result 
										;') ;
						  
			   SELECT @Query_String;
			   PREPARE statement from @Query_String;
			   Execute statement;
			   Deallocate PREPARE statement; 
           
           ELSE 
			
				IF Input_Data2 = 'MULTI'
				
				THEN
		
					SET @view_stmt=concat('CREATE OR REPLACE VIEW V_UI_Campaign_Campaign_Segment AS
												(SELECT GROUP_CONCAT("(",
												Segment_Condition_Solus,
												")"
												SEPARATOR " OR ") as Segment_Condition_Solus
												 From Campaign_Segment where Segment_Id in (',Input_Data,')
											  );');
										   
						SELECT @view_stmt;
						PREPARE statement from @view_stmt;
						Execute statement;
						Deallocate PREPARE statement;
				ELSE
				
					SET @view_stmt=concat('CREATE OR REPLACE VIEW V_UI_Campaign_Campaign_Segment AS
												(SELECT 
												 Segment_Condition_Solus
												 From Campaign_Segment where Segment_Id in (',Input_Data,')
											  );');
										   
						SELECT @view_stmt;
						PREPARE statement from @view_stmt;
						Execute statement;
						Deallocate PREPARE statement;
						
			    END IF;
				
				
					
			  SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Campaign_Segment_Condition",`Segment_Condition_Solus`) )),"]")
										  into @temp_result from V_UI_Campaign_Campaign_Segment
										;') ;
						  
			   SELECT @Query_String;
			   PREPARE statement from @Query_String;
			   Execute statement;
			   Deallocate PREPARE statement; 
						
			   If @temp_result is null
				 Then set @temp_result="[]";
				End if;				
			
            END IF;
					
    End if; 

  -- Checking_Existing_LifeCycleSegment
    
    If Requested_Details='Checking_Existing_Lifecycle_Segment' and Input_Data !='' 
     Then
     				SELECT CLMSegmentName into @SegmentName FROM CLM_Segment where CLMSegmentName =Input_Data;

     				If @SegmentName  = Input_Data
     				   Then
			 			
		                    SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Lifecycle Segment Name Already Present") )),"]")
													    into @temp_result 
													  ;') ;
						      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 

					Else 
							SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Lifecycle Segment Name Not Present") )),"]")
													    into @temp_result 
													   ;') ;
						      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
							
					End If;

					if @temp_result is null
						Then set @temp_result="[]";
					End if;

	End If;

	
      -- Fetching Instant Lifecyle Segment Details
      
       if Requested_Details= 'Instant_Lifecyle_Segment'
     Then
				If Input_Data =""
				   THEN	Set Input_Data ='-1';
				End If;
				
				If Input_Data2 =""
				   THEN	Set Input_Data2 ='-1';
				End If;
					
				If Input_Data3 ="NO"
				   THEN	Set Input_Data3 ='-1,1';
				Else 
					Set Input_Data3 ='1';
				End If;

				If Input_Data4 ="NO"
				   THEN	Set Input_Data4 ='-1,1';
				Else 
					Set Input_Data4 ='1';
				End If;
				
				If Input_Data5 ="NO"
				   THEN	Set Input_Data5 ='-1,1';
				Else 
					Set Input_Data5 ='1';
				End If;
				
				If Input_Data6 ="NO"
				   THEN	Set Input_Data6 ='1=1';
				Else 
					Set Input_Data6 ='Favourite_Day = weekDay(now())';
				End If;

	 
	 
		        SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Campaign_LifeCycleSegment_ID",CLMSegmentId,"Campaign_LifeCycleSegment_Name",CLMSegmentName,"Campaign_LifeCycleSegment_Value",Coverage) )),"]")
										    into @temp_result from CLM_Segment
										   ;') ;
						      
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 
					
				If @temp_result is null
				  Then set @temp_result="[]";
				End if;				
					
    End if; 
      
      
      
      
      -- Featching Lifecyle Segment Details
    
    if Requested_Details= 'Lifecyle_Segment'
     Then
		
				SET @view_stmt=concat('CREATE OR REPLACE VIEW V_UI_Campaign_Coverage AS
										(SELECT CLMSegmentId,CLMSegmentName,Convert(format(Sum(Coverage),","),CHAR) as Coverage,CLMSegmentReplacedQuery
				                         From CLM_Segment 
				                         WHERE In_Use="Yes"
				                         Group by CLMSegmentId,CLMSegmentName
				                      );');
					               
	            SELECT @view_stmt;
				PREPARE statement from @view_stmt;
				Execute statement;
				Deallocate PREPARE statement;
	 
	 
		        SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Campaign_LifeCycleSegment_ID",CLMSegmentId,"Campaign_LifeCycleSegment_Name",CLMSegmentName,"Campaign_LifeCycleSegment_Value",Coverage,"Campaign_LifeCycleSegment_Condition",CLMSegmentReplacedQuery) )),"]")
										    into @temp_result from V_UI_Campaign_Coverage
										   ;') ;
						      
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 
					
				If @temp_result is null
				  Then set @temp_result="[]";
				End if;					
					
    End if; 


	  -- Featching Lifecycle_View Details
    
    if Requested_Details= 'Lifecyle_View'
       Then
              SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Campaign_LifeCycleSegment_ID",CLMSegmentId,"Campaign_LifeCycleSegment_Name",CLMSegmentName,"Campaign_LifeCycleSegment_Value","") )),"]")
										    into @temp_result from CLM_Segment
										   ;') ;
				      
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement; 
					
			if @temp_result is null
			   Then set @temp_result="[]";
			End if;				
					
    End if; 

	

  -- Checking_Existing_RegionSegment
    
    If Requested_Details='Checking_Existing_Region' and Input_Data !='' 
       Then
     				SELECT RegionSegmentName into @RegionSegmentName FROM CLM_RegionSegment where RegionSegmentName =Input_Data;

     				If @RegionSegmentName  = Input_Data
     	     			Then
			 			
		                    SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Region Segment Name Already Present") )),"]")
													    into @temp_result 
													   ;') ;
						      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 

					Else 
							SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Region Segment Name Not Present") )),"]")
													    into @temp_result 
													   ;') ;
													      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
							
					End If;

					if @temp_result is null
						Then set @temp_result="[]";
					End if;

	End If;

       -- Featching Region Details
    
    if Requested_Details= 'Region'
     Then
         SET @Query_String = concat('Create or replace view Region_Segment_View as
										Select  RegionSegmentId,RegionSegmentName,concat(RegionReplacedQuery,case when (RegionReplacedQueryBill is null or RegionReplacedQueryBill ="") then "" else concat (" and ",RegionReplacedQueryBill)end ) as `Condition`	
										from  CLM_RegionSegment where In_Use="Yes"
								   ;') ;
								      
		SELECT @Query_String;
		PREPARE statement from @Query_String;
		Execute statement;
		Deallocate PREPARE statement; 

         SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Region_ID",`RegionSegmentId`,"Region",`RegionSegmentName`,"Region_Condition",`Condition`) )),"]")
								    into @temp_result from  Region_Segment_View
								   ;') ;
								      
		SELECT @Query_String;
		PREPARE statement from @Query_String;
		Execute statement;
		Deallocate PREPARE statement; 
					
		If @temp_result is null
			Then set @temp_result="[]";
		End if;					
					
    End if; 

	if Requested_Details= 'Region_Condition' and Input_Data!=''
     Then
         SET @Query_String = concat('Create or replace view Region_Segment_Condition_View as
										Select  RegionSegmentId,RegionSegmentName,concat(RegionReplacedQuery,case when (RegionReplacedQueryBill is null or RegionReplacedQueryBill ="") then "" else concat (" and ",RegionReplacedQueryBill)end ) as `Condition`	
										from  CLM_RegionSegment where In_Use="Yes" and RegionSegmentId="',Input_Data,'"
								   ;') ;
								      
		SELECT @Query_String;
		PREPARE statement from @Query_String;
		Execute statement;
		Deallocate PREPARE statement; 
		
         SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Region_Condition",`Condition`) )),"]")
								    into @temp_result from  Region_Segment_Condition_View
								   ;') ;
								      
		SELECT @Query_String;
		PREPARE statement from @Query_String;
		Execute statement;
		Deallocate PREPARE statement; 
					
		If @temp_result is null
			Then set @temp_result="[]";
		End if;					
					
    End if; 
      -- Featching Event Details
    
    If Requested_Details= 'Execution_After'
     Then
        SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Event_ID",`Id`,"Event_Name",`Name`)Order by id desc )),"]")
								    into @temp_result from  Event_Master
								   ;') ;
				      
		SELECT @Query_String;
		PREPARE statement from @Query_String;
		Execute statement;
		Deallocate PREPARE statement; 
					
		if @temp_result is null
		   Then set @temp_result="[]";
		End if;				
					
    End if; 

	
	 If Requested_Details= 'Reminder_Execution_After'
     Then
        SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Event_ID",A.Id,"Event_Name",A.Name)Order by A.Name )),"]")
								    into @temp_result from  Event_Master A,Communication_Template B
									where A.CreativeId1=B.Id and Offer_Code ="CUSTOMER_LEVEL_OFFER"
								   ;') ;
				      
		SELECT @Query_String;
		PREPARE statement from @Query_String;
		Execute statement;
		Deallocate PREPARE statement; 
					
		if @temp_result is null
		   Then set @temp_result="[]";
		End if;				
					
    End if; 
    -- Featching Category Details
    
    If Requested_Details= 'Category'
       Then
	        SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Category_ID",`LOV_ID`,"Category_Name",`Cat_Comm_Name`) )),"]")
									    into @temp_result from  CDM_Cat_Master
									     ;') ;
									      
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement; 
						
			If @temp_result is null
			   Then set @temp_result="[]";
			End if;				
						
    End if; 



      -- Featching Clone_from_existing_Creativetion Details
    
   If Requested_Details= 'Existing_Creative' 
       Then
	   
			IF Input_Data IS NULL OR Input_Data =''
				THEN 
				SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Creative_ID",`CreativeId`,"Creative_Name",`CreativeName`)order by A.CreativeName  )),"]")
												into @temp_result from CLM_Creative A,CLM_Channel B
												where A.ChannelId=B.ChannelId
											 ;') ;

			ELSE 
				   SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Creative_ID",`CreativeId`,"Creative_Name",`CreativeName`)order by A.CreativeName )),"]")
												into @temp_result from CLM_Creative A,CLM_Channel B
												where A.ChannelId=B.ChannelId and B.ChannelName="',Input_Data,'"
											 ;') ;
			END IF;						      
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement; 
					
			If @temp_result is null
				Then set @temp_result="[]";
			End if;				
					
    End if;
   -- Featching Personalization of the Clone_from_existing_Creativetion Details
    
   If Requested_Details= 'Personalization_Clone' and Input_Data !=''
       Then

	     	Set @Creative_Id=Input_Data;
	        SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Personalization",`Creative`) )),"]")
									    into @temp_result from  CLM_Creative where CreativeId = ',@Creative_Id,'
									   ;') ;
									      
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement; 
						
			If @temp_result is null
			   Then set @temp_result="[]";
			End if;				
						
    End if; 

     -- Featching Personalization of the Clone_from_existing_Creativetion Details
    
    If Requested_Details= 'Personalization_Required'
     Then
     	
     	Set @Creative_Id=Input_Data;
        SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Personalization",`Creative`) )),"]")
								    into @temp_result from  CLM_Creative where CreativeId = ',@Creative_Id,'
								   ;') ;
				      
		SELECT @Query_String;
		PREPARE statement from @Query_String;
		Execute statement;
		Deallocate PREPARE statement; 
					
		If @temp_result is null
		   Then set @temp_result="[]";
		End if;				
					
    End if; 



     -- Featching Personalization for display.
    
    if Requested_Details= 'Preview Creative' and Input_Data != '' 
     Then
     		SELECT Input_Data as 'Creative_Given'; 
            Set @Comm_Template =Input_Data;
     		Set @Header ="";
			
			If Input_Data3 = ''
				THEN
					If Input_Data2 ="SMS"
					Then Select `Value`  into @Header from M_Config where Name ='SMS_Communication_Header';
					End if;
					
					If Input_Data2 ="EMAIL"
					Then Select `Value` into @Header from M_Config where Name ='Email_Communication_Header';
					End if;
					
					If Input_Data2 ="WHATSAPP"
					Then Select `Value` into @Header from M_Config where Name ='Whatapp_Communication_Header';
					End if;
					
					If Input_Data2 ="PN"
					Then Select `Value` into @Header from M_Config where Name ='PN_Communication_Header';
					End if;
					
			ELSE 		SEt @Header =Input_Data3;
			
			End If;
			
     		Set @Template_Check = Null;
			
			Set @Message ='';
			
			Select @Comm_Template;
			
			
     		Select 1 into @Template_Check From DUAL where @Comm_Template Like "%Concat%";

     		If 	@Template_Check is Null
     		    Then
			
		     		-- CALL S_UI_Campaign_Is_Valid_Template(@Comm_Template, @Header, @Message);
					CALL PROC_CLM_IS_VALID_Template(@Comm_Template, @Header, @Message);
                    
                    SELECT @Message as 'Final_Output_Creative';
					
					set @Error_Message='';
					Select "YES" into @Error_Message From DUAL where @Message Like "%error%";
					
					Select @Error_Message ;
					if @Error_Message != "YES" then
					
					Select "YES"  into @Error_Message From DUAL where @Message Like "%'field list'%";
						
					end if;
					
					if @Error_Message is null then
					
					Select "YES"  into @Error_Message From DUAL where @Message Like "%'field list'%";
						
					end if;
						
			        SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message",@Message,"error",@Error_Message) )),"]") into @temp_result ;') ;
							      
					SELECT @Query_String;
					PREPARE statement from @Query_String;
					Execute statement;
					Deallocate PREPARE statement; 
							
					if @temp_result is null
					   Then set @temp_result="[]";
					End if;	

			Else 		
					
					SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Concat functionality is not provided") )),"]") into @temp_result ;') ;
						      
					SELECT @Query_String;
					PREPARE statement from @Query_String;
					Execute statement;
					Deallocate PREPARE statement; 
			End If;						
					
    End if; 


    -- Featching Personalised Reco via Solus Shortener for display.
    
    If Requested_Details= 'Personalization_Reco' 
        Then
     		
			Select `value` into @Personalization_Reco_Link  from M_Config  where Name ='Personalization_Reco_Link';
     		Select CONCAT(Input_Data,@Personalization_Reco_Link ,"<SOLUS_PFIELD> Microsite_URL </SOLUS_PFIELD>") into @Message ;

	        SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message",@Message) )),"]") into @temp_result ;') ;
					      
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement; 
					
			If @temp_result is null
			   Then set @temp_result="[]";
			End if;				
					
    End if; 
    
-- Featching Personalised Reco via Solus Shortener for display.
    
    If Requested_Details= 'Advanced_Personalization' 
       Then
     		Set @DefaultName='';
			
     		If Input_Data ="Customer_Name"
     		Then
     			Select Value into @DefaultName from M_Config where Name ='Customer_Valid_Name_Default';
     		End If;
     		
     		Select @DefaultName;
			
		

			
			If Input_Data = "SO2_PDP_Reco1_PDP"	
			THEN
					Set Input_Data ="Reco1_PDP";
			End If;
			
			If Input_Data ="Reco1_Image_URL"
				Then Set Input_Data ="Reco1_Image_Link";
			End If;

			If Input_Data ="Reco1_Genome"
				Then Set Input_Data ="Reco1_Genome";
			End If;

			If Input_Data ="Reco1_Product"
				Then Set Input_Data ="Reco1_ProductName";
			End If;

			If Input_Data ="Reco1_Category"
				Then Set Input_Data ="Reco1_Cat";
			End If;

			If Input_Data ="UseCase_Pfied2"
			    Then Set Input_Data ="RecoPField2";
			End If;

			If Input_Data ="UseCase_Pfiedl1"
				Then Set Input_Data ="RecoPField1";
			End If;

			If Input_Data ="UseCase_NumCust"
				Then Set Input_Data ="NumCust";
			End If;

			If Input_Data ="customer_level_voucher"
				Then Set Input_Data ="Offer_Code";
			End If;
			
			
			If Input_Data ="Fav_Category"
				Then Set Input_Data ="LT_Fav_Cat";
			End If;
			
			
			If Input_Data ="Fav_Product"
				Then Set Input_Data ="LT_Fav_Prod";
			End If;
			
			If Input_Data ="Fav Product"
				Then Set Input_Data ="LT_Fav_Prod";
			End If;
			
			
			If Input_Data ="Fav_PDP_so2"
				Then Set Input_Data ="FavProduct_PDP_so2";
			End If;
			
			If Input_Data ="Last_Bought_Category"
				Then Set Input_Data ="LTD_Fav_Cat";
			End If;
			
			If Input_Data ="Last_Bought_Product"
				Then Set Input_Data ="LTD_Fav_Prod";
			End If;
			
			If Input_Data ="Last_Bought_PDP_so2"
				Then Set Input_Data ="LastBoughtProduct_PDP_so2";
			End If;
			
			
			
			

			If Input_Data ="Customer_Name"
				Then Set Input_Data ="Customer_Name";
			End If;
				
			Select Input_Data;
			
			IF Input_Data ="SO2_Curated_Products" or Input_Data ="SO2_SmartBasket"  or Input_Data ="so2_Deeplink" or Input_Data ="Reco1_PDP" or Input_Data ="Reco2_PDP" or Input_Data ="Reco3_PDP" or Input_Data ="Reco4_PDP" or Input_Data ="Reco5_PDP" or Input_Data ="Reco6_PDP"
			THEN
			
					Select `value` into @Personalization_Reco_Link  from M_Config  where Name ='Personalization_Reco_Link';
					
					
					Select CONCAT(Input_Data2,@Personalization_Reco_Link ,"<SOLUS_PFIELD>Microsite_URL</SOLUS_PFIELD>") into @Message ;

			ELSE
     		Select CONCAT(Input_Data2,"<SOLUS_PFIELD>",Input_Data,"</SOLUS_PFIELD>") into @Message ;
			End iF;
			
			Select  @Message;
	        
	        SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message",@Message,"Status",@DefaultName) )),"]") into @temp_result ;') ;
					      
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement; 
					
			If @temp_result is null
				Then set @temp_result="[]";
			End if;				
					
    End if; 
    
     -- Featching Ignore CollOff
    
    if Requested_Details='Ignore_CollOff' 
       Then
     				Select `Value` into @Communication_CoolOff_Days from M_Config where Name ="Communication_CoolOff_Days";

     				
		            SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message",@Communication_CoolOff_Days) )),"]")
					                    	    into @temp_result 
						                       ;') ;
						      
					SELECT @Query_String;
					PREPARE statement from @Query_String;
					Execute statement;
					Deallocate PREPARE statement; 
					
					if @temp_result is null
						then set @temp_result="[]";
					End if;

	End If;
    
    
    
      -- Featching Global Cool Off Conditions
    
    if Requested_Details='Cool_Off' 
       Then         
                    Select `Value` into @Global_Communication_CoolOff_Days from M_Config where Name ="Communication_CoolOff_Days";
     				Select `Value` into @SMS_Communication_CoolOff_Days from M_Config where Name ="SMS_Communication_CoolOff_Days";
					Select `Value` into @Email_Communication_CoolOff_Days from M_Config where Name ="Email_Communication_CoolOff_Days";
					Select `Value` into @PN_Communication_CoolOff_Days from M_Config where Name ="PN_Communication_CoolOff_Days";
					Select `Value` into @WhatsApp_Communication_CoolOff_Days from M_Config where Name ="WhatsApp_Communication_CoolOff_Days";

					
					
     				SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Global","',@Global_Communication_CoolOff_Days,'","SMS","',@SMS_Communication_CoolOff_Days,'","Email","',@Email_Communication_CoolOff_Days,'","PN","',@PN_Communication_CoolOff_Days,'","WhatsApp","',@WhatsApp_Communication_CoolOff_Days,'" ) )),"]") into @temp_result
								     	') ;
						      
					SELECT @Query_String;
					PREPARE statement from @Query_String;
					Execute statement;
					Deallocate PREPARE statement; 
					
					if @temp_result is null
						then set @temp_result="[]";
					End if;

	End If;
	
	  -- Featching Ignore CollOff
    
    if Requested_Details='Channel_CollOff' 
       Then
     				Select `Value` into @SMS_Communication_CoolOff_Days from M_Config where Name ="SMS_Communication_CoolOff_Days";
					Select `Value` into @Email_Communication_CoolOff_Days from M_Config where Name ="Email_Communication_CoolOff_Days";
					Select `Value` into @PN_Communication_CoolOff_Days from M_Config where Name ="PN_Communication_CoolOff_Days";
					Select `Value` into @WhatsApp_Communication_CoolOff_Days from M_Config where Name ="WhatsApp_Communication_CoolOff_Days";

					
					
     				Set @Channel_Communication_CoolOff_Days = Concat ("SMS Channel Level Cool Off is:",@SMS_Communication_CoolOff_Days," Days Email Channel Level Cool Off is:",@Email_Communication_CoolOff_Days," Days PN Channel Level Cool Off is:",@PN_Communication_CoolOff_Days," Days WhatsApp Channel Level Cool Off is:",@WhatsApp_Communication_CoolOff_Days," Days");
					
		            SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message",@Channel_Communication_CoolOff_Days) )),"]")
					                    	    into @temp_result 
						                       ;') ;
						      
					SELECT @Query_String;
					PREPARE statement from @Query_String;
					Execute statement;
					Deallocate PREPARE statement; 
					
					if @temp_result is null
						then set @temp_result="[]";
					End if;

	End If;

	-- Featching Ignore CollOff
    
    if Requested_Details='Personalised_Reco_Solus_Shortener' 
     Then
					Set @Personalization_Reco_LandingPage ='';
     				Select `Value` into @Personalization_Reco_LandingPage from M_Config where Name ="Personalization_Reco_LandingPage";

     				SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message",@Personalization_Reco_LandingPage) )),"]")
					    	                    into @temp_result 
						                      ;') ;
						      
					SELECT @Query_String;
					PREPARE statement from @Query_String;
					Execute statement;
					Deallocate PREPARE statement; 
					
					If @temp_result is null
						Then set @temp_result="[]";
					End if;

	End If;


	-- Featching Event Status Count
    
    If Requested_Details= 'Event_Status_Count'
       Then
			Set @Type_of_Event =Input_Data;
			Set @Type_of_Channel =Input_Data2;

         -- ----Type_Of_Event_Radio_Button-------


		IF  @Type_of_Event = "ALL"
		THEN 

			SET @Event_Type_Filter = "1=1";
            
		END IF;
            
		
		IF @Type_of_Event = "CLM"
        THEN

			SET @Event_Type_Filter = "B.Type_Of_Event='CLM'";
		
        END IF;
        
		
		IF @Type_of_Event = "GTM"
        THEN 

			SET @Event_Type_Filter = "B.Type_Of_Event='GTM'";
		
		END IF;

			Select @Type_of_Event,@Type_of_Channel,@Event_Type_Filter;

			SET @Query_String0 = concat('Select Count(1) INTO @Draft_Count  from Event_Master A,Event_Master_UI B where A.Id=B.Event_ID and State ="Draft" and ',@Event_Type_Filter,' and A.ChannelName="',@Type_of_Channel,'";') ;

            
            SELECT @Query_String0 as "DRAFT";
			PREPARE statement from @Query_String0;
			Execute statement;
			Deallocate PREPARE statement; 
			
			SET @Query_String0 = concat('Select Count(1) INTO @Schedule_Count  from Event_Master A,Event_Master_UI B where A.Id=B.Event_ID and State ="Schedule" and ',@Event_Type_Filter,' and A.ChannelName="',@Type_of_Channel,'";') ;

           SELECT @Query_String0 as "Schedule";
			PREPARE statement from @Query_String0;
			Execute statement;
			Deallocate PREPARE statement; 

			SET @Query_String0 = concat('Select Count(1) INTO @Live_Count  from Event_Master A,Event_Master_UI B where A.Id=B.Event_ID and State ="Enabled" and ',@Event_Type_Filter,' and A.ChannelName="',@Type_of_Channel,'";') ;

            
            SELECT @Query_String0 as "Live";
			PREPARE statement from @Query_String0;
			Execute statement;
			Deallocate PREPARE statement; 

			SET @Query_String0 = concat('Select Count(1) INTO @Inactive_Count  from Event_Master A,Event_Master_UI B where A.Id=B.Event_ID and State ="Disabled" and ',@Event_Type_Filter,' and A.ChannelName="',@Type_of_Channel,'";') ;

            SELECT @Query_String0 as "Inactive";
			PREPARE statement from @Query_String0;
			Execute statement;
			Deallocate PREPARE statement;
	 
     
     SELECT @Draft_Count, @Schedule_Count, @Live_Count , @Inactive_Count;
	 
			SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("DRAFT",',@Draft_Count,',"TESTING",0,"VERIFIED",0,"Schedule",',@Schedule_Count,',"ACTIVE",',@Live_Count,',"INACTIVE",',@Inactive_Count,'
					                     ) )),"]") into @temp_result 
					                   ;') ;
					  
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement; 
					
			If @temp_result is null
				Then set @temp_result="[]";
			End if;				
					
    End if; 
	

-- SOLUS Variables Type
 
    if Requested_Details='SOLUS_Variables_Type' 
     Then
					
					Set @Views ='';
					IF Input_Data ="SVOC"
					   THEN  Set @Views ="V_CLM_Customer_One_View"; 
					END IF;
					
					IF Input_Data ="SVOT"
					   THEN  Set @Views ="V_CLM_Bill_Detail_One_View"; 
					END IF;
					
					Insert Ignore into UI_SVOC_SVOT_Variables(View_Name,View_Variable_Type,View_Variable_Name,View_Vairable_Descripition)
					SELECT "V_CLM_Customer_One_View","Custom",COLUMN_NAME,COLUMN_NAME
					FROM INFORMATION_SCHEMA.COLUMNS
					WHERE TABLE_NAME = 'V_CLM_Customer_One_View' and COLUMN_NAME<>"Customer_Id" ;
					
					Insert Ignore into UI_SVOC_SVOT_Variables(View_Name,View_Variable_Type,View_Variable_Name,View_Vairable_Descripition)
					SELECT "V_CLM_Bill_Detail_One_View","Custom",COLUMN_NAME,COLUMN_NAME
					FROM INFORMATION_SCHEMA.COLUMNS
					WHERE TABLE_NAME = 'V_CLM_Bill_Detail_One_View'and COLUMN_NAME<>"Customer_Id" ;

					
					SET @view_stmt=concat("CREATE OR REPLACE VIEW V_UI_SVOC_SVOT_Variables AS
											(SELECT Distinct View_Variable_Type as View_Variable_Type
											 From UI_SVOC_SVOT_Variables
					                         WHERE  View_Name='",@Views,"' and 
											        View_Variable_Name in (SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='",@Views,"')
					                        )");
	               
	                SELECT @view_stmt;
					PREPARE statement from @view_stmt;
					Execute statement;
					Deallocate PREPARE statement;
												
					Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("View_Type",View_Variable_Type) )),"]") into @temp_result 
											   From V_UI_SVOC_SVOT_Variables 
											 ;') ;
					
					SELECT @Query_String;
					PREPARE statement from @Query_String;
					Execute statement;
					Deallocate PREPARE statement; 
					
					If @temp_result is null
					   Then set @temp_result="[]";
					End if;

	End If;
	

-- SOLUS Variables
    
    if Requested_Details='SOLUS_Variables' 
     Then
					Set @Variables_Type ='';
					Set @Views ='';
					
					Set @Variables_Type =Input_Data2;
					Set @Views ='';
					
					IF Input_Data ="SVOC"
					   THEN Set @Views ="V_CLM_Customer_One_View"; 
					END IF;
					
					IF Input_Data ="SVOT"
					   THEN Set @Views ="V_CLM_Bill_Detail_One_View"; 
					END IF;
												
					
					
					Set @Query_String= concat("SELECT CONCAT('[',(GROUP_CONCAT(json_object('View_Variable_Name',View_Variable_Name,'View_Vairable_Descripition',View_Vairable_Descripition) )),']')
						                        into @temp_result 
						                       From UI_SVOC_SVOT_Variables
											   Where  View_Variable_Type='",@Variables_Type,"' and  View_Name='",@Views,"'
												       and  View_Variable_Name in (SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='",@Views,"');
											 ") ;
												
					
					SELECT @Query_String;
					PREPARE statement from @Query_String;
					Execute statement;
					Deallocate PREPARE statement; 
					
					If @temp_result is null
						Then set @temp_result="[]";
					End if;

	End If;

	-- Featching Global_Configuration
    
    If Requested_Details='Global_Configuration' 
     Then
					    DROP table If EXISTS Global_Configuration;
						
                        CREATE TABLE Global_Configuration 
			     		(Control_Group bigint(20) DEFAULT NULL,
						Response_Window bigint(20) DEFAULT NULL,
						Global_Communication_Cool_Off bigint(20) DEFAULT NULL,
						Throttling_Limit bigint(20) DEFAULT NULL,
                        Control_Group_refresh_Frequency bigint(20) DEFAULT NULL,
                        Response_Calculation_Method Varchar(128) DEFAULT NULL,
                        Starting_Day_of_week  bigint(20) DEFAULT NULL,
                        Campaign_Execution_Delay  bigint(20) DEFAULT NULL,
                        Calculate_Hard_Response  Varchar(128) DEFAULT NULL,
						Bill_Check_For_CLM  Varchar(128) DEFAULT NULL,
						Bill_Check_Interval_in_Days  Varchar(128) DEFAULT NULL,
						Reco_Check_For_CLM  Varchar(128) DEFAULT NULL,
						New_Control_Group_Mechanism Varchar(128) DEFAULT NULL, 
						Customer_Valid_Name_Size  Varchar(128) DEFAULT NULL,
						Customer_Valid_Name_Default  Varchar(128) DEFAULT NULL,
						Customer_Valid_Name_OnlyCharCheck  Varchar(128) DEFAULT NULL,
						Default_Character_Set Varchar(128) DEFAULT NULL,
						Keep_Channel_Config_Dynamic Varchar(128) DEFAULT NULL,
						Recommendation_Type Varchar(128) DEFAULT NULL,
						Recommendation_UseCase_Method  Varchar(128) DEFAULT NULL,
						Personalization_Reco_Link Varchar(128) DEFAULT NULL,
						Custom_Variable_Path Varchar(128) DEFAULT NULL,
						SMS_Communication_CoolOff_Days Varchar(128) DEFAULT NULL,
						Email_Communication_CoolOff_Days Varchar(128) DEFAULT NULL,
						PN_Communication_CoolOff_Days Varchar(128) DEFAULT NULL,
						WhatsApp_Communication_CoolOff_Days Varchar(128) DEFAULT NULL,
						SMS_Communication_Header Varchar(128) DEFAULT NULL,
						Email_Communication_Header Varchar(128) DEFAULT NULL,
						Whatapp_Communication_Header Varchar(128) DEFAULT NULL,
						PN_Communication_Header Varchar(128) DEFAULT NULL) ;

						SET @Query_String0 = concat('Insert into Global_Configuration(Control_Group,Response_Window,Global_Communication_Cool_Off,Throttling_Limit,Control_Group_refresh_Frequency,Response_Calculation_Method,Starting_Day_of_week,Campaign_Execution_Delay,Calculate_Hard_Response,Bill_Check_For_CLM,Bill_Check_Interval_in_Days,Reco_Check_For_CLM,New_Control_Group_Mechanism,Customer_Valid_Name_Size,Customer_Valid_Name_Default,Customer_Valid_Name_OnlyCharCheck,Default_Character_Set,Keep_Channel_Config_Dynamic,Recommendation_Type,Recommendation_UseCase_Method,Personalization_Reco_Link,Custom_Variable_Path,SMS_Communication_CoolOff_Days,Email_Communication_CoolOff_Days,PN_Communication_CoolOff_Days,WhatsApp_Communication_CoolOff_Days,SMS_Communication_Header,Email_Communication_Header,Whatapp_Communication_Header,PN_Communication_Header)
			   				             Value
										( 
											(Select Value from M_Config where Name="Hold_Out_Percentage"),
											(Select Value from M_Config where Name="Response_Days"),
											(Select Value from M_Config where Name="Communication_CoolOff_Days"),
											(Select Value from M_Config where Name="CLM_Event_Limit"),
											(Select Value from CLM_Response_Config where Name="Control_Group_Refresh_Interval"),
											(Select Value from CLM_Response_Config where Name="Control_Group_Method"),
											(Select Value from CLM_Response_Config where Name="Starting_Day_of_Week"),
											(Select Value from CLM_Response_Config where Name="Campaign_Execution_Delay"),
											(Select Value from CLM_Response_Config where Name="Calculate_Hard_Response"),
											(Select Value from M_Config where Name="Bill_Check_For_CLM"),
											(Select Value from M_Config where Name="Bill_Check_Interval_in_Days"),
											(Select Value from M_Config where Name="Reco_Check_For_CLM"),
											(Select Value from M_Config where Name="New_Control_Group_Mechanism"),
											(Select Value from M_Config where Name="Customer_Valid_Name_Size"),
											(Select Value from M_Config where Name="Customer_Valid_Name_Default"),
											(Select Value from M_Config where Name="Customer_Valid_Name_OnlyCharCheck"),
											(Select Value from M_Config where Name="Default_Character_Set"),
											(Select Value from M_Config where Name="Keep_Channel_Config_Dynamic"),
											(Select Value from M_Config where Name="Recommendation_Type"),
											(Select Value from M_Config where Name="Recommendation_UseCase_Method"),
											(Select Value from M_Config where Name="Personalization_Reco_Link"),
											(Select Value from M_Config where Name="Custom_Variable_Path"),
											(Select Value from M_Config where Name="SMS_Communication_CoolOff_Days"),
											(Select Value from M_Config where Name="Email_Communication_CoolOff_Days"),
											(Select Value from M_Config where Name="PN_Communication_CoolOff_Days"),
											(Select Value from M_Config where Name="WhatsApp_Communication_CoolOff_Days"),
											(Select Value from M_Config where Name="SMS_Communication_Header"),
											(Select Value from M_Config where Name="Email_Communication_Header"),
											(Select Value from M_Config where Name="Whatapp_Communication_Header"),
											(Select Value from M_Config where Name="PN_Communication_Header")
											
										)  

																;') ;
						SELECT @Query_String0;
						PREPARE statement from @Query_String0;
						Execute statement;
						Deallocate PREPARE statement; 
	 
						SET @Query_String = concat('SELECT 	 	      CONCAT("[",(GROUP_CONCAT(json_object("Control_Group",`Control_Group`,"Response_Window",`Response_Window`,"Global_Communication_Cool_Off",`Global_Communication_Cool_Off`,"Throttling_Limit",`Throttling_Limit`,"Control_Group_refresh_Frequency",`Control_Group_refresh_Frequency`,"Response_Calculation_Method",`Response_Calculation_Method`,"Starting_Day_of_week",`Starting_Day_of_week`,"Campaign_Execution_Delay",`Campaign_Execution_Delay`,"Calculate_Hard_Response",`Calculate_Hard_Response`,"Bill_Check_For_CLM",Bill_Check_For_CLM,"Bill_Check_Interval_in_Days","Bill_Check_Interval_in_Days","Reco_Check_For_CLM",Reco_Check_For_CLM,"New_Control_Group_Mechanism",New_Control_Group_Mechanism,"Customer_Valid_Name_Size",Customer_Valid_Name_Size,"Customer_Valid_Name_Default",Customer_Valid_Name_Default,"Customer_Valid_Name_OnlyCharCheck",Customer_Valid_Name_OnlyCharCheck,"Default_Character_Set",Default_Character_Set,"Keep_Channel_Config_Dynamic",Keep_Channel_Config_Dynamic,"Recommendation_Type",Recommendation_Type,"Recommendation_UseCase_Method",Recommendation_UseCase_Method,"Personalization_Reco_Link",Personalization_Reco_Link,"Custom_Variable_Path",Custom_Variable_Path,"SMS_Communication_CoolOff_Days",SMS_Communication_CoolOff_Days,"Email_Communication_CoolOff_Days",Email_Communication_CoolOff_Days,"PN_Communication_CoolOff_Days",PN_Communication_CoolOff_Days,"WhatsApp_Communication_CoolOff_Days",WhatsApp_Communication_CoolOff_Days,"SMS_Communication_Header",SMS_Communication_Header,"Email_Communication_Header",Email_Communication_Header,"Whatapp_Communication_Header",Whatapp_Communication_Header,"PN_Communication_Header",PN_Communication_Header	)  )),"]") into @temp_result 
						 From  Global_Configuration
															   ;') ;
						
					
										  
						SELECT @Query_String;
						PREPARE statement from @Query_String;
						Execute statement;
						Deallocate PREPARE statement; 
						
						If @temp_result is null
							Then set @temp_result="[]";
						End if;	

	End If;



-- Featching Exeution_Configuration
    
    If Requested_Details='Execution_Configuration' 
     Then
					    DROP table If EXISTS Exeution_Configuration;
						
                        CREATE TABLE Exeution_Configuration 
			     		(File_Path Varchar(128) DEFAULT NULL,
						SMS_Extension Varchar(128) DEFAULT NULL,
						SMS_Delimiter Varchar(128) DEFAULT NULL,
						SMS_Marker_File Varchar(128) DEFAULT NULL,
						SMS_Send_Time Varchar(128) DEFAULT NULL,
						Email_Extension Varchar(128) DEFAULT NULL,
						Email_Delimiter Varchar(128) DEFAULT NULL,
						Email_Marker_File Varchar(128) DEFAULT NULL,
						Email_Send_Time Varchar(128) DEFAULT NULL,
						PN_Extension Varchar(128) DEFAULT NULL,
						PN_Delimiter Varchar(128) DEFAULT NULL,
						PN_Marker_File Varchar(128) DEFAULT NULL,
						PN_Send_Time Varchar(128) DEFAULT NULL,
						WhatsApp_Extension Varchar(128) DEFAULT NULL,
						WhatsApp_Delimiter Varchar(128) DEFAULT NULL,
						WhatsApp_Marker_File Varchar(128) DEFAULT NULL,
						WhatsApp_Send_Time Varchar(128) DEFAULT NULL,
						Execution_Agency Varchar(128) DEFAULT NULL,
						Event_Execution_Id_Position Varchar(128) DEFAULT NULL,
						Enclose_Output_in_Double_Quotes Varchar(128) DEFAULT NULL,
						File_Generation_Method Varchar(128) DEFAULT NULL,
						File_Generation_BatchSize Varchar(128) DEFAULT NULL,
						Message_Update_Batch_Size Varchar(128) DEFAULT NULL,
						Event_Execution_Batch_Size Varchar(128) DEFAULT NULL,
						SMS_Send_GateWay Varchar(128) DEFAULT NULL,
						Email_Send_GateWay Varchar(128) DEFAULT NULL,
						Whatsapp_Send_GateWay Varchar(128) DEFAULT NULL,
						PN_Send_GateWay Varchar(128) DEFAULT NULL						
						);

						SET @Query_String0 = concat('Insert into Exeution_Configuration(File_Path,SMS_Extension,SMS_Delimiter,SMS_Marker_File,SMS_Send_Time,Email_Extension,Email_Delimiter,Email_Marker_File,Email_Send_Time,PN_Extension,PN_Delimiter,PN_Marker_File,PN_Send_Time,WhatsApp_Extension,WhatsApp_Delimiter,WhatsApp_Marker_File,WhatsApp_Send_Time,Execution_Agency,Event_Execution_Id_Position,Enclose_Output_in_Double_Quotes,File_Generation_Method,File_Generation_BatchSize,Message_Update_Batch_Size,Event_Execution_Batch_Size,SMS_Send_GateWay,Email_Send_GateWay,Whatsapp_Send_GateWay,PN_Send_GateWay)
			   				             Value
										( 
											(Select Value from M_Config where Name="File_Path"),
											(Select Value from M_Config where Name="SMS_File_Extension"),
											(Select Value from M_Config where Name="SMS_File_Delimiter"),
											(Select Value from M_Config where Name="SMS_Marker_File_Required"),
											(Select Value from M_Config where Name="SMS_Send_Time"),
											(Select Value from M_Config where Name="Email_File_Extension"),
											(Select Value from M_Config where Name="Email_File_Delimiter"),
											(Select Value from M_Config where Name="Email_Marker_File_Required"),
											(Select Value from M_Config where Name="Email_Send_Time"),
											(Select Value from M_Config where Name="PN_File_Extension"),
											(Select Value from M_Config where Name="PN_File_Delimiter"),
											(Select Value from M_Config where Name="PN_Marker_File_Required"),
											(Select Value from M_Config where Name="PN_Send_Time"),
											(Select Value from M_Config where Name="WhatsApp_File_Extension"),
											(Select Value from M_Config where Name="WhatsApp_File_Delimiter"),
											(Select Value from M_Config where Name="WhatsApp_Marker_File_Required"),
											(Select Value from M_Config where Name="WhatsApp_Send_Time"),
											(Select Value from M_Config where Name="Execution_Agency"),
											(Select Value from M_Config where Name="Event_Execution_Id_Position"),
											(Select Value from M_Config where Name="Enclose_Output_in_Double_Quotes"),
											(Select Value from M_Config where Name="File_Generation_Method"),
											(Select Value from M_Config where Name="File_Generation_BatchSize"),
											(Select Value from M_Config where Name="Message_Update_Batch_Size"),
											(Select Value from M_Config where Name="Event_Execution_Batch_Size"),
											(Select Value from M_Config where Name="SMS_Send_GateWay"),
											(Select Value from M_Config where Name="Email_Send_GateWay"),
											(Select Value from M_Config where Name="Whatsapp_Send_GateWay"),
											(Select Value from M_Config where Name="PN_Send_GateWay")
											
											) ;') ;
						SELECT @Query_String0;
						PREPARE statement from @Query_String0;
						Execute statement;
						Deallocate PREPARE statement; 
	 
						SET @Query_String = concat('SELECT 	 	      CONCAT("[",(GROUP_CONCAT(json_object("File_Path",`File_Path`,"SMS_Extension",`SMS_Extension`,"SMS_Delimiter",`SMS_Delimiter`,"SMS_Marker_File",`SMS_Marker_File`,"SMS_Send_Time",`SMS_Send_Time`,"Email_Extension",`Email_Extension`,"Email_Delimiter",`Email_Delimiter`,"Email_Marker_File",`Email_Marker_File`,"Email_Send_Time",`Email_Send_Time`,"PN_Extension",`PN_Extension`,"PN_Delimiter",`PN_Delimiter`,"PN_Marker_File",`PN_Marker_File`,"PN_Send_Time",`PN_Send_Time`,"WhatsApp_Extension",`WhatsApp_Extension`,"WhatsApp_Delimiter",`WhatsApp_Delimiter`,"WhatsApp_Marker_File",`WhatsApp_Marker_File`,"WhatsApp_Send_Time",`WhatsApp_Send_Time`,"Execution_Agency",Execution_Agency,"Event_Execution_Id_Position",Event_Execution_Id_Position,"Enclose_Output_in_Double_Quotes",Enclose_Output_in_Double_Quotes,"File_Generation_Method",File_Generation_Method,"File_Generation_BatchSize",File_Generation_BatchSize,"Message_Update_Batch_Size",Message_Update_Batch_Size,"Event_Execution_Batch_Size",Event_Execution_Batch_Size,"SMS_Send_GateWay",SMS_Send_GateWay,"Email_Send_GateWay",Email_Send_GateWay,"Whatsapp_Send_GateWay",Whatsapp_Send_GateWay,"PN_Send_GateWay",PN_Send_GateWay)  )),"]") into @temp_result 
						 From  Exeution_Configuration
															   ;') ;
										  
						SELECT @Query_String;
						PREPARE statement from @Query_String;
						Execute statement;
						Deallocate PREPARE statement; 
						
						If @temp_result is null
							Then set @temp_result="[]";
						End if;	

	End If;

	-- Featching Customer_Views
    
    if Requested_Details='Customer_Views' 
       Then
     			
					If Input_Data ='' and Input_Data2 =''
					  THEN
							Set @Filter_Condition =concat("In_Use_Customer_One_View ='Y' or In_Use_Customer_Details_View ='Y' ");
					 End If; 
					 
					 If Input_Data ='' and Input_Data2 !=''
					  THEN
							Set @Filter_Condition =concat("Column_Name Like '%",Input_Data2,"%'");
					 End If; 
					 
					If Input_Data !='' and Input_Data2 =''
					  THEN
							Set @Filter_Condition =concat("Table_Name Like '%",Input_Data,"%'");
					 End If; 
					 
					If Input_Data !='' and Input_Data2!=''
					  THEN
							Set @Filter_Condition =concat("Table_Name Like '%",Input_Data,"%' and Column_Name Like '%",Input_Data2,"%' ");
					 End If;
					 Select @Filter_Condition ;
					 
					SET @view_stmt=concat("CREATE OR REPLACE VIEW V_UI_Customer_View_Config AS
											(Select ID,Table_Name,Table_Alias,Column_Name,Column_Name_in_One_View,Column_Name_in_Details_View,
													Case When In_Use_Customer_One_View ='Y' or In_Use_Customer_One_View ='Mandatory' then 'Y'
													 When In_Use_Customer_One_View ='N' or In_Use_Customer_One_View ='NA' then 'N'
													 End as In_Use_Customer_One_View, 
													 Case When In_Use_Customer_Details_View ='Y' or In_Use_Customer_Details_View ='Mandatory' then 'Y'
													 When In_Use_Customer_Details_View ='N' or In_Use_Customer_Details_View ='NA' then 'N'
													 End as In_Use_Customer_Details_View
												from Customer_View_Config where  ",@Filter_Condition ,"
										  );");
									   
					SELECT @view_stmt;
					PREPARE statement from @view_stmt;
					Execute statement;
					Deallocate PREPARE statement;
     				
		            SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("ID",`ID`,"Table_Name",`Table_Name`,"Table_Alias",`Table_Alias`,"Column_Name",`Column_Name`,"Column_Name_in_One_View",`Column_Name_in_One_View`,"In_Use_Customer_One_View",`In_Use_Customer_One_View`,"Column_Name_in_Details_View",`Column_Name_in_Details_View`,"In_Use_Customer_Details_View",`In_Use_Customer_Details_View`) )),"]")
					                    	    into @temp_result  From V_UI_Customer_View_Config
						                       ;') ;
						      
					SELECT @Query_String;
					PREPARE statement from @Query_String;
					Execute statement;
					Deallocate PREPARE statement; 
					
					if @temp_result is null
						then set @temp_result="[]";
					End if;

	End If;
	
	If Requested_Details="Custom_Variable_Table_Name"
		THEN
		
		
		Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Table",TABLE_NAME) )),"]") 
														into @temp_result  
									FROM  information_schema.tables
                                      where  Table_Name in ("CDM_Bill_Details","CDM_Customer_Master","CDM_Product_Master","Customer_One_View","RankedPickList")					
												      ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;
	End If;	
	
	If Requested_Details="Custom_Variable_Table_Column" and Input_Data!=''
		THEN
		
		
		Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Column",COLUMN_NAME) )),"]") 
														into @temp_result  
									FROM  information_schema.COLUMNS
                                      where Table_Name ="',Input_Data,'"				
												      ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
		
	End If;	
	
	If Requested_Details="Custom_Variable_Table_Joins" and Input_Data!=''
		THEN
				-- "CDM_Bill_Details","CDM_Customer_Master","CDM_Product_Master","CDM_LOV_Master","Customer_One_View","RankedPickList"
				
				If Input_Data ="CDM_Bill_Details"
					Then Set @Joins1 = Concat("NA");
						
				End If;
				
				If Input_Data ="CDM_Product_Master"
					Then Set @Joins1 = Concat("NA");
				End If;
				
				If Input_Data ="CDM_Customer_Master"
					Then Set @Joins1 = Concat("'Custom.Customer_Id_1=CDM_Customer_Master.Customer_Id'");
				End If;
				
				If Input_Data ="Customer_One_View"
					Then Set @Joins1 = Concat("'Custom.Customer_Id_1=Customer_One_View.Customer_Id'");
					
				End If;
				
				If Input_Data ="RankedPickList"
					Then Set @Joins1 = Concat("'RankedPickList.Customer_Id_1=Custom.Customer_Id'");
				
				End If;
				
				
				
		
		Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Joins",',@Joins1,') )),"]") into @temp_result   ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
		
	End If;	

	
	IF Requested_Details ='BrewingStation_Values'
		THEN
		
			IF Input_Data ="SVOC"
			Then
					Select Input_Data2;
					
					SET @view_stmt=concat("CREATE OR REPLACE VIEW BrewingStation_Values AS
								(Select Distinct ",Input_Data2,"  as BrewingStation_Values
														from V_CLM_Customer_One_View limit 50
												  );");
											   
							SELECT @view_stmt;
							PREPARE statement from @view_stmt;
							Execute statement;
							Deallocate PREPARE statement;
			End IF;
			
			IF Input_Data ="SVOT"
			Then
					Select Input_Data2;
					
					SET @view_stmt=concat("CREATE OR REPLACE VIEW BrewingStation_Values AS
								(Select Distinct ",Input_Data2," as BrewingStation_Values
													From 	V_CLM_Bill_Detail_One_View limit 50
												  );");
											   
							SELECT @view_stmt;
							PREPARE statement from @view_stmt;
							Execute statement;
							Deallocate PREPARE statement;
			End IF;
			
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "BrewingStation_Values",BrewingStation_Values) )),"]") 
														into @temp_result   from BrewingStation_Values                     
												      ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
		
	END IF;	
	
	
	IF Requested_Details ='BrewingStation_VariableValues'
		THEN
		
			IF Input_Data ="SVOC"
			Then
					Select Input_Data2;
					
					SET @view_stmt=concat("CREATE OR REPLACE VIEW BrewingStation_Values AS
								(Select Distinct ",Input_Data2,"  as BrewingStation_Values
														from V_CLM_Customer_One_View 
														where ",Input_Data2," like '",Input_Data3,"%'
												  );");
											   
							SELECT @view_stmt;
							PREPARE statement from @view_stmt;
							Execute statement;
							Deallocate PREPARE statement;
			End IF;
			
			IF Input_Data ="SVOT"
			Then
					Select Input_Data2;
					
					SET @view_stmt=concat("CREATE OR REPLACE VIEW BrewingStation_Values AS
								(Select Distinct ",Input_Data2," as BrewingStation_Values
													From 	V_CLM_Bill_Detail_One_View
													where ",Input_Data2," like '",Input_Data3,"%'
												  );");
											   
							SELECT @view_stmt;
							PREPARE statement from @view_stmt;
							Execute statement;
							Deallocate PREPARE statement;
			End IF;
			
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "BrewingStation_Values",BrewingStation_Values) )),"]") 
														into @temp_result   from BrewingStation_Values                     
												      ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
		
	END IF;	
	
	
	If Requested_Details='CLM_Status' and Input_Data!=''
	    THEN
	
			Set Input_Data= replace (Input_Data,"[",'');
			Set Input_Data= replace (Input_Data,"]",'');
			Set Input_Data= replace (Input_Data,'"','');
			Set Input_Data= replace (Input_Data,', ',',');
			SET @Event_CLM_Status='';
			SET @EVENTENDDATE='';
			SET @EVENT_CLM_STATUS='0';
			SET @Event_Run_Status=0;
			Select CASE WHEN `End` IS NULL THEN 'CLM_Is_Running' ELSE 'CLM_Is_Not_Running' END into @Event_CLM_Status
			FROM CLM_Process_Log ORDER BY  Id DESC LIMIT 1;
			
			Select Count(1) into @CLM_Process_Log_Count from CLM_Process_Log;
			
			If @CLM_Process_Log_Count<=0 then  Set @Event_CLM_Status='CLM_Is_Not_Running'; end if;
			
			Select @Event_CLM_Status;
			IF @Event_CLM_Status='CLM_Is_Not_Running'
				THEN 
					 SET @Query_String1 = concat('SELECT max(TriggerEndDate) INTO @EVENTENDDATE from CLM_Campaign_Events A, CLM_Campaign_Trigger B
						where A.CampTriggerId=B.CampTriggerId and EventId in (',Input_Data,');') ;
					SELECT @Query_String1;
					PREPARE statement from @Query_String1;
					Execute statement;
					Deallocate PREPARE statement; 
					
					IF @EVENTENDDATE < CURRENT_DATE
					
					THEN	
						Set @Query_String2 = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Please Reschedule the Campaign as.It is Scheduled for the older date.","Status","NO") )),"]") into @temp_result  ;') ;
						SELECT @Query_String2;
						PREPARE statement from @Query_String2;
						Execute statement;
						Deallocate PREPARE statement;
						
					ELSE 	
					
						Set @Query_String3 = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","","Status","YES") )),"]") into @temp_result  ;') ;
						SELECT @Query_String3;
						PREPARE statement from @Query_String3;
						Execute statement;
						Deallocate PREPARE statement;
						
					END IF;
			END IF;	

			IF @Event_CLM_Status='CLM_Is_Running'
				THEN 
					 SET @Query_String4 = concat('SELECT TriggerEndDate INTO @EVENTENDDATE from CLM_Campaign_Events A, CLM_Campaign_Trigger B
						where A.CampTriggerId=B.CampTriggerId and EventId in (',Input_Data,');') ;
						
					SELECT @Query_String4;
					PREPARE statement from @Query_String4;
					Execute statement;
					Deallocate PREPARE statement; 
					
					IF @EVENTENDDATE < CURRENT_DATE
					
					THEN	
						Set @Query_String5 = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","The SOLUS Engine (CLM) is already running.<br>Please Reschedule the Campaign as.It is Scheduled for the older date.","Status","NO") )),"]") into @temp_result  ;') ;
						SELECT @Query_String5;
						PREPARE statement from @Query_String5;
						Execute statement;
						Deallocate PREPARE statement;
						
					ELSE 	
					
						Set @Query_String5 = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","The SOLUS Engine (CLM) is already running","Status","NO") )),"]") into @temp_result  ;') ;
						SELECT @Query_String5;
						PREPARE statement from @Query_String5;
						Execute statement;
						Deallocate PREPARE statement;
						
					END IF;
			END IF;		
							
			Select date_format(Event_Execution_Date_ID,"%Y-%m-%d") into @Latest_CLM_Run_Date from Event_Execution_History order by Event_Execution_Date_ID desc limit 1;
			Select Current_Date into @Today_Current_Date;
			
			If @Latest_CLM_Run_Date=@Today_Current_Date
			Then 
				SET @Query_String1 = concat('Select 1 into @Event_Run_Status from Event_Execution_History
												where date_format(Event_Execution_Date_ID,"%Y-%m-%d") =@Latest_CLM_Run_Date 
												and Event_ID in (',Input_Data,')
												 order by Event_Execution_Date_ID desc limit 1;') ;
						SELECT @Query_String1;
						PREPARE statement from @Query_String1;
						Execute statement;
						Deallocate PREPARE statement; 
						
				If 	@Event_Run_Status=1
				Then	
						Set @Query_String5 = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","The SOLUS Engine (CLM) already Ran the Event.Please check Campaign History Screen","Status","NO") )),"]") into @temp_result  ;') ;
							SELECT @Query_String5;
							PREPARE statement from @Query_String5;
							Execute statement;
							Deallocate PREPARE statement;
				End if;			
			END IF;
	End If;
	
	If Requested_Details="Rcore_Story"
		THEN
		
		
		Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Id",Story_Id,"Name",Name) )),"]") 
														into @temp_result  
									FROM  RankedPickList_Stories_Master
                                      
												      ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;
	End If;	
	
	
	If Requested_Details="SOLUS_Customer_Sample_Data" and (Input_Data is null or Input_Data='')
		THEN
		
		
		Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Variable_Name",Variable_Name,"Variable_Data",Vairable_Sample_Data) )),"]") 
														into @temp_result  
									FROM  UI_Variables_Sample_Data
                                      
												      ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;
	End If;	
	
	
	If Requested_Details="SOLUS_Customer_Sample_Data" and (Input_Data is not null or Input_Data!='') and (Input_Data2 is not null or Input_Data2!='')
		THEN
		Select Input_Data2,Input_Data;
		Set @Query_String= concat('Update UI_Variables_Sample_Data
		Set Vairable_Sample_Data="',Input_Data2,'"
		where Variable_Name = "',Input_Data,'";') ;
		SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;
		
		Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","SUCCESS") )),"]") 
														into @temp_result  
									
                                      
												      ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;
	End If;	
	
	If Requested_Details="Channels" 
		THEN
		
		
			Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Channel_Id",ChannelId,"Channel_Name",ChannelName) )),"]") from CLM_Channel into @temp_result  ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;
	End If;	
	
	-- Checking_Existing_EXCLUSION_FILTER
    
    if Requested_Details='Checking_Existing_EXCLUSION_FILTER' and Input_Data !='' 
       Then
     				SELECT EFName into @CampName FROM CLM_Exclusion_Filter where EFName =Input_Data;

     				If @CampName  = Input_Data
     				Then
			 			
		                    SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Exclusion Filter Name Already Present") )),"]")
													    into @temp_result 
													   ;') ;
						      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 

					Else 
							SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Exclusion Filter Name Not Present") )),"]")
													    into @temp_result 
													   ;') ;
						      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
							
					End If;

					if @temp_result is null
						then set @temp_result="[]";
					End if;

	End If;
	
	
    -- Featching Offer Type Details

    if Requested_Details='Offer_Type'
       Then
	 
	        SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("CLM_OfferType_ID",`OfferTypeId`,"CLM_OfferType_Name",`OfferTypeName`) )),"]")
									    into @temp_result from  CLM_OfferType
									   ;') ;
					      
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement; 
						
			If @temp_result is null
			   then set @temp_result="[]";
			End if;

	End If;
    
     
    -- Fetching offer code details
    
      if Requested_Details='Offer_Code' 
       Then
	 
	        SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("OfferType_Id",`OfferTypeId`,"Offer_Def",`OfferDef`) )),"]")
									    into @temp_result from  CLM_Offer 
                                        WHERE OfferDef <> ""
									   ;') ;
                                       SELECT @temp_result;
                                       
					      
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement; 
						
			If @temp_result is null
			   then set @temp_result="[]";
			End if;

	End If;
	
	-- Featching Communication Header Details
    
    if Requested_Details='Communication_Header' and  Input_Data != ''
       Then
	 
	 
			If Input_Data ="SMS"
			Then Select `Value`  into @Communication_Header from M_Config where Name ='SMS_Communication_Header';
			End if;
			
			If Input_Data ="EMAIL"
			Then Select `Value` into @Communication_Header from M_Config where Name ='Email_Communication_Header';
			End if;
			
			If Input_Data ="WHATSAPP"
			Then Select `Value` into @Communication_Header from M_Config where Name ='Whatapp_Communication_Header';
			End if;
			
			If Input_Data ="PN"
			Then Select `Value` into @Communication_Header from M_Config where Name ='PN_Communication_Header';
			End if;
			
			
	        SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Communication_Header",@Communication_Header) )),"]")
									    into @temp_result
									   ;') ;
					      
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement; 
						
			If @temp_result is null
			   then set @temp_result="[]";
			End if;

	End If;
	
	
	-- Featching Communication Variable Details
    
If Requested_Details="Communication_Variable" and Input_Data='RECO'
		THEN
		
	    Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Communication_Variable",COLUMN_NAME) order by COLUMN_NAME ) ),"]") into @temp_result from Customer_View_Config 
					where (Column_Name_in_Details_View in ("Reco2_Cat","Reco2_ProductName","Reco2_Genome","Reco2_PDP","Reco3_PDP","Reco1_PDP","Reco3_Cat","Reco3_ProductName","Reco3_Genome") or Column_Name_in_Details_View like "%Reco%" and Column_Name_in_Details_View not in ("Reco1_PDP","Reco1_Image_URL","Reco1_Genome","Reco1_Product","Reco1_Category") ) and In_Use_Customer_Details_View IN ("Y","Mandatory")
								
                          
                        					      ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
                                      				
							
		
	End If;	
	
	
	
	
If Requested_Details="Communication_Variable" and Input_Data='PROFILE'
		THEN
		
		
		
		
		Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Communication_Variable",Column_Name_in_Details_View) order by Column_Name_in_Details_View)),"]") into @temp_result from Customer_View_Config 
														  
									
                                where Column_Name_in_Details_View in ("Point_Balance","Points_Expiry_Date",
								"Mobile_No","Email_Id","LTD_Store","Lt_Num_of_Visits","Recency","FavProduct_PDP_so2","LastBoughtProduct_PDP_so2","DOJ","Vintage","SenderKey") 
								 or (Column_Name_in_Details_View like "%LTD%"   
								 and (Column_Name_in_Details_View not like "LTD_Fav_Prod") 
									and (Column_Name_in_Details_View not like  "LTD_Fav_Cat" )
									
								)
								or (Column_Name_in_Details_View like "%LT%" AND In_Use_Customer_Details_View ="Y")
								or (Column_Name_in_Details_View= "Event_Id" AND In_Use_Customer_Details_View in ("Y","Mandatory"))
								
								OR (Table_Name ="Client_SVOC")
                                      				
												      ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
		
	End If;	
	
	
	
	
	   -- Featching CLM OUTPUT Path
    
    if Requested_Details='OUTPUT_PATH'
       Then
	 
	        SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("OUTPUT_PATH",`Value`) )),"]")
									    into @temp_result from  M_Config where Name="File_Path"
									   ;') ;
					      
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement; 
						
			If @temp_result is null
			   then set @temp_result="[]";
			End if;

	End If;
	
	   -- Featching DEFAULT_SENT_TIME
    
    if Requested_Details='DEFAULT_SENT_TIME'
       Then
	 
	        SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("DEFAULT_SENT_TIME",`Value`) )),"]")
									    into @temp_result from  M_Config where Name="Default_Sent_time"
									   ;') ;
					      
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement; 
						
			If @temp_result is null
			   then set @temp_result="[]";
			End if;

	End If;
	
		   -- Featching DEFAULT_SENT_TIME
    
    if Requested_Details='SFTP_STATUS' and Input_Data!=''
       Then
	 
	        SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("SFTP_STATUS",`Run_Mode`) )),"]")
									    into @temp_result from   UI_Campaign_Task_List 
										where UI_Campaign_Task="Paste_File_On_SFTP"
												and  Event_Id= "',Input_Data,'"
												ORDER BY UI_Campaign_Task_Id DESC
												
									   ;') ;
					      
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement; 
						
			If @temp_result is null
			   then set @temp_result="[]";
			End if;

	End If;
	
	IF Requested_Details='REMAINDER_PARENT_INFO' AND Input_Data !=''
       Then
	   
			

	 
	        SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("REMINDER_PARENT_CHANNEL",upper(A.ChannelName),"REMINDER_PARENT_TEMPLATE",B.Header)limit 1 )),"]")
									    into @temp_result  from Event_Master A,Communication_Template B
											Where A.CreativeId1=B.Id and A.Id=',Input_Data,'
									   ;') ;
					      
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement; 
						
			If @temp_result is null
			   then set @temp_result="[]";
			End if;

	End If;
	
	IF Requested_Details='Customer_List_Pullout_Variables'
       Then
	   
			

	 
	        Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Id",ID,"Variable_Name",Column_Name_in_Details_View) )),"]") into @temp_result from Customer_View_Config 
			where  In_Use_Customer_Details_View in  ("Y","Mandatory") ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
						
			If @temp_result is null
			   then set @temp_result="[]";
			End if;

	End If;
	
	IF Requested_Details='Response_Days'
       Then
	   
			

	 
	        Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Response_Days",`Value`) )),"]") into @temp_result from M_Config 
			where Name="Response_Days" ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
						
			If @temp_result is null
			   then set @temp_result="[]";
			End if;

	End If;
	
	
	IF Requested_Details='Ext_Campaign_Key'
       Then
	   
			

	 
	        Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Ext_Campaign_Key",`Value`) )),"]") into @temp_result from M_Config 
			where Name="Default_Ext_Campaign_Key" ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
						
			If @temp_result is null
			   then set @temp_result="[]";
			End if;

	End If;
	
	
	IF Requested_Details='Sender_Key'
       Then
	   
			

	 
	        Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Sender_Key",`Value`) )),"]") into @temp_result from M_Config 
			where Name="Default_Sender_Key" ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
						
			If @temp_result is null
			   then set @temp_result="[]";
			End if;

	End If;
	
	IF Requested_Details='Critical_Trigger_Email_Ids'
       Then
	   
			

	 
	        Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Critical_Trigger_Email_ids",`Value`) )),"]") into @temp_result from M_Config 
			where Name="Critical_Trigger_Email_ids" ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
						
			If @temp_result is null
			   then set @temp_result="[]";
			End if;

	End If;
	
    IF Requested_Details='Adapter_Details' and Input_Data!=''
       Then
	   
			
			  Set @Query_String= concat('
			  Create View Adapter_Details as
			Select case when `type`="API-Based" then Provider_Name else "SFTP" end as Adapter_Details 
			from  Downstream_Adapter_Details where Channel="',Input_Data,'" and In_Use="Enable";') ;
   
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
						
	 
	        Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Adapter_Details",Adapter_Details ) )),"]") into @temp_result from Adapter_Details ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							
							
				DROP  VIEW IF EXISTS Adapter_Details;

				
			If @temp_result is null
			   then set @temp_result="[]";
			End if;

	End If;
	
	 IF Requested_Details='Adapter_Details_Update' and Input_Data!='' and Input_Data2!='' and Input_Data3!=''
       Then
			If Input_Data3="ENABLED"
				THEN
					Set @Query_String= concat('Update Downstream_Adapter_Details
												Set In_Use="Disable"
												where Channel="',Input_Data,'"') ;
		   
									SELECT @Query_String;
									PREPARE statement from @Query_String;
									Execute statement;
									
					Set @Query_String= concat('Update Downstream_Adapter_Details
												Set In_Use="Enable"
												where Channel="',Input_Data,'"
												and  Provider_Name="',Input_Data2,'"') ;
		   
									SELECT @Query_String;
									PREPARE statement from @Query_String;
									Execute statement;
												
			END IF;	

			If Input_Data3="DISABLED"
				THEN
					Set @Query_String= concat('Update Downstream_Adapter_Details
												Set In_Use="Enable"
												where Channel="',Input_Data,'"') ;
		   
									SELECT @Query_String;
									PREPARE statement from @Query_String;
									Execute statement;
									
					Set @Query_String= concat('Update Downstream_Adapter_Details
												Set In_Use="Disable"
												where Channel="',Input_Data,'"
												and  Provider_Name="',Input_Data2,'"') ;
		   
									SELECT @Query_String;
									PREPARE statement from @Query_String;
									Execute statement;
												
			END IF;		

	 
	        Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","SUCCESS") )),"]") into @temp_result ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
						
			If @temp_result is null
			   then set @temp_result="[]";
			End if;

	End If;
	
	
	IF Requested_Details='File_Transfer_Details'
       Then
	   
			Set @File_Transfer_Details_Status=1;
			Select ifnull(Value,'') into @SFTP_HOST from M_Config where Name='SFTP_HOST';
			Select ifnull(Value,'') into @SFTP_USERNAME from M_Config where Name='SFTP_USERNAME';
			Select ifnull(Value,'') into @SFTP_PASSWORD from M_Config where Name='SFTP_PASSWORD';
			Select ifnull(Value,'') into @SFTP_PORT from M_Config where Name='SFTP_PORT';
			Select ifnull(Value,'') into @SFTP_PATH_TO_PASTE_FILE from M_Config where Name='SFTP_PATH_TO_PASTE_FILE'; 

			If @SFTP_HOST=''
				then 
						Set @File_Transfer_Details_Status=0;
					 Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Please Enter the SFTP Details","Status","NO") )),"]") into @temp_result  ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
					
				else 		Set @File_Transfer_Details_Status=1;
						 Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Success","Status","YES") )),"]") into @temp_result  ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
			
					
			end if;	
			
			If @File_Transfer_Details_Status=1
				THEN
					If @SFTP_USERNAME='' 
						then 
						
						Set @File_Transfer_Details_Status=0;
							 Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Please Enter the SFTP Details","Status","NO") )),"]") into @temp_result  ;') ;
		   
									SELECT @Query_String;
									PREPARE statement from @Query_String;
									Execute statement;
							
						else 		Set @File_Transfer_Details_Status=1;
								 Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Success","Status","YES") )),"]") into @temp_result  ;') ;
		   
									SELECT @Query_String;
									PREPARE statement from @Query_String;
									Execute statement;
					
							
					end if;	
			end if;
			
			If @File_Transfer_Details_Status=1
				THEN
					If @SFTP_PASSWORD='' 
						then 
						
						Set @File_Transfer_Details_Status=0;
							 Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Please Enter the SFTP Details","Status","NO") )),"]") into @temp_result  ;') ;
		   
									SELECT @Query_String;
									PREPARE statement from @Query_String;
									Execute statement;
							
						else 			Set @File_Transfer_Details_Status=1;
								 Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Success","Status","YES") )),"]") into @temp_result  ;') ;
		   
									SELECT @Query_String;
									PREPARE statement from @Query_String;
									Execute statement;
					
							
					end if;	
			END IF;
			
			
			If @File_Transfer_Details_Status=1
				THEN
					If @SFTP_PORT='' 
						then 
						
						Set @File_Transfer_Details_Status=0;
							 Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Please Enter the SFTP Details","Status","NO") )),"]") into @temp_result  ;') ;
		   
									SELECT @Query_String;
									PREPARE statement from @Query_String;
									Execute statement;
							
						else 		Set @File_Transfer_Details_Status=1;
								 Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Success","Status","YES") )),"]") into @temp_result  ;') ;
		   
									SELECT @Query_String;
									PREPARE statement from @Query_String;
									Execute statement;
					
							
					end if;	
			END IF;
			
			If @File_Transfer_Details_Status=1
				THEN
					If @SFTP_PATH_TO_PASTE_FILE=''
						then 
						Set @File_Transfer_Details_Status=0;
							 Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Please Enter the SFTP Details","Status","NO") )),"]") into @temp_result  ;') ;
		   
									SELECT @Query_String;
									PREPARE statement from @Query_String;
									Execute statement;
							
						else 		Set @File_Transfer_Details_Status=1;
								 Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Success","Status","YES") )),"]") into @temp_result  ;') ;
		   
									SELECT @Query_String;
									PREPARE statement from @Query_String;
									Execute statement;
					
							
					end if;	
			END IF;
	 
	       
						
			If @temp_result is null
			   then set @temp_result="[]";
			End if;

	End If;
	
	
		IF Requested_Details='ONBOARDING_CAMPAIGN' 
       Then
	   
			
				CREATE TABLE IF NOT EXISTS  ONBOARDING_CAMPAIGN (
					Id  BIGINT NOT NULL DEFAULT -1,
					ONBOARDING_TIME VARCHAR(128) NOT NULL DEFAULT -1,
					Recency_Start VARCHAR(128) NOT NULL DEFAULT -1,
					Recency_End VARCHAR(128) NOT NULL DEFAULT -1,
					LifeCycleSegmentId VARCHAR(128) NOT NULL DEFAULT -1,
					PRIMARY KEY ONBOARDING_TIME (ONBOARDING_TIME)
				);
				
				Select CLMSegmentId into @ONBOARDING_CAMPAIGNCLMSegmentId from CLM_Segment where CLMSegmentName="FTR";
				INSERT IGNORE INTO ONBOARDING_CAMPAIGN
				SELECT "1","Next day after the trasaction (T+1)","1","1",@ONBOARDING_CAMPAIGNCLMSegmentId
                   UNION ALL
                SELECT  "2", "On the second day after the trasaction (T+2)","2","2",@ONBOARDING_CAMPAIGNCLMSegmentId
                    UNION ALL
                SELECT "3","On the third day after the trasaction (T+3)","3","3",@ONBOARDING_CAMPAIGNCLMSegmentId
                   UNION ALL
                SELECT "4","On the fourth day after the trasaction (T+4)","4","4",@ONBOARDING_CAMPAIGNCLMSegmentId
                    UNION ALL
                SELECT "5","On the fifth day after the trasaction (T+5)","5","5",@ONBOARDING_CAMPAIGNCLMSegmentId
                    UNION ALL
                SELECT "6","On the sixth day after the trasaction (T+6)","6","6",@ONBOARDING_CAMPAIGNCLMSegmentId;
				
				
	        SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Id",Id,"ONBOARDING_TIME",ONBOARDING_TIME,"Recency_Start",Recency_Start,"Recency_End",Recency_End,"LifeCycleSegmentId",@ONBOARDING_CAMPAIGNCLMSegmentId)order by Id)),"]")
									    into @temp_result  from ONBOARDING_CAMPAIGN
									   ;') ;
					      
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement; 
						
			If @temp_result is null
			   then set @temp_result="[]";
			End if;

	End If;
	
	
If Requested_Details="Campaign_UI_Sample_Contact_Details" and Input_Data !=''
	THEN
		
			Set @Campaign_UI_Sample_Contact_Details_Event_id=Input_Data;
			
			Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(Distinct json_object("Group_Id",Group_Id,"Id",@Campaign_UI_Sample_Contact_Details_Event_id) )),"]") into @temp_result from Campaign_UI_Sample_Contact_Details  ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;
	End If;	
	
	
If Requested_Details="Mcore_Module"
	THEN
		
				
			Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT( json_object("Mcore_Module_Id",ID,"Mcore_Module_Name",Description) )),"]") into @temp_result from CLM_MCore_Model_Master  ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;
	End If;		
	
	IF Requested_Details="BluePrint"
		THEN
				
			Select CONCAT("[",(GROUP_CONCAT(json_object("Type",Type_Of_Event,"Segment",SegmentJson) )),"]") into @temp_result
				From Event_Master_UI A,
			(Select B.Id,CONCAT("[",(GROUP_CONCAT(json_object("TypeCLMSegmentName",CLMSegmentName,"Channel",ChannelJson) )),"]") as SegmentJson
				From Event_Master B,
					(Select C.Id,CONCAT("[",(GROUP_CONCAT(json_object("TypeChannelName",ChannelName,"Theme",ThemeJson) )),"]") as ChannelJson
						From Event_Master C,
						(Select D.Id,CONCAT("[",(GROUP_CONCAT(json_object("TypeThemeName",CampaignThemeName,"Campaign",CampaignJson) )),"]") as ThemeJson
							From Event_Master D,
							(Select E.Id,CONCAT("[",(GROUP_CONCAT(json_object("TypeCampaignName",CampaignName,"Trigger",TriggerJson) )),"]") as CampaignJson
							From Event_Master E,
									(Select Id, CONCAT("[",(GROUP_CONCAT(json_object("TypeTriggerName",TypeTriggerName,"Coverage",Coverage) )),"]") as TriggerJson from 
										(Select Id,json_object("TriggerName",Name) as TypeTriggerName from Event_Master)F
										 ,(Select Event_Id,json_object("Coverage",Coverage) as Coverage from Event_Master_UI)G
										 where F.Id=G.Event_Id
										 Group by Id
									) TriggerJsonTable
							WHERE E.Id=TriggerJsonTable.Id
							Group by E.Id) CampaignJsonTable
						WHERE D.Id=CampaignJsonTable.Id
						Group by D.Id) ThemeJsonTable
					WHERE C.Id=ThemeJsonTable.Id
					Group by C.Id) ChannelJsonTable
			WHERE B.Id=ChannelJsonTable.Id
			Group by B.Id) SegmentJsonTable
			WHERE A.Event_Id=SegmentJsonTable.Id;
			
					
						
						
	
	END IF;	
				
	if @temp_result is null
		Then
								
							Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Invalid Parameters") )),"]") into @temp_result; ') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
						
	End if;
							
    SET out_result_json = @temp_result;

 END$$
DELIMITER ;
