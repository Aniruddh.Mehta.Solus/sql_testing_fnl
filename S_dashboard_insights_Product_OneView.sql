DELIMITER $$
CREATE or replace PROCEDURE `S_dashboard_insights_Product_OneView`(IN in_request_json json, OUT out_result_json json)
BEGIN

   declare IN_USER_NAME varchar(240); 
   declare IN_SCREEN varchar(400);
   declare IN_MEASURE text; 
   declare IN_FILTER_CURRENTMONTH text; 
   declare IN_FILTER_CURRENTYEAR text;
   declare IN_FILTER_MONTHDURATION text; 
   declare IN_NUMBER_FORMAT text;
   declare IN_AGGREGATION_1 text; 
   declare IN_AGGREGATION_2 text;
   declare IN_AGGREGATION_3 text;
   declare Query_String text;
   declare Query_Measure_Name varchar(100);
   
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    
    IF IN_MEASURE = 'Product'
    THEN
      set @view_stmt = concat('create or replace view V_Dashboard_Product_OneView_prodsearch as
                               select distinct product_name as `ProductName` from Dashboard_Insights_Base_ByProduct where product_name != "null" ');
      set @Query_String = concat('select CONCAT("[",(GROUP_CONCAT(json_object("Name",`ProductName`) )),"]")into @temp_result from V_Dashboard_Product_OneView_prodsearch');
    END IF;
    
    IF IN_MEASURE = 'Segment'
    THEN
      set @view_stmt = concat('create or replace view V_Dashboard_Product_OneView_segsearch as
                               select CLMSegmentName as `SegmentName` from Dashboard_Insights_Base_ByProduct where CLMSegmentName NOT LIKE "%Leads%" AND CLMSegmentName NOT LIKE "%NA%" group by CLMSegmentName having sum(T2MthActive) > 10 ');
      set @Query_String = concat('select CONCAT("[",(GROUP_CONCAT(json_object("Name",`SegmentName`) )),"]")into @temp_result from V_Dashboard_Product_OneView_segsearch');
    END IF;
    IF IN_NUMBER_FORMAT = 'M'
    THEN
    
     select round(ifnull(avg(T1MthActive),0),1) into @AvgCustomer from Dashboard_Insights_Base_ByProduct where YYYYMM = IN_FILTER_CURRENTMONTH and AGGREGATION="BY_MONTH" and CLMSegmentName = IN_AGGREGATION_1 and product_name = IN_AGGREGATION_2 and Product_Type='Bought';
     select concat(round(ifnull(((avg(T1MthActive)-avg(T2MthActive))/avg(T2MthActive))*100,0),1),"%") into @AvgPercentChangeCust from Dashboard_Insights_Base_ByProduct where YYYYMM = IN_FILTER_CURRENTMONTH and AGGREGATION="BY_MONTH" and CLMSegmentName = IN_AGGREGATION_1 and product_name = IN_AGGREGATION_2 and Product_Type='Bought';
     select round(ifnull(avg(T1MthActiveBills),0),1) into @AvgBills from Dashboard_Insights_Base_ByProduct where YYYYMM = IN_FILTER_CURRENTMONTH and AGGREGATION="BY_MONTH" and CLMSegmentName = IN_AGGREGATION_1 and product_name = IN_AGGREGATION_2 and Product_Type='Bought';
     select concat(round(ifnull(((avg(T1MthActiveBills)-avg(T2MthActiveBills))/avg(T2MthActiveBills))*100,0),1),"%") into @AvgPercentChangeBills from Dashboard_Insights_Base_ByProduct where YYYYMM = IN_FILTER_CURRENTMONTH and AGGREGATION="BY_MONTH" and CLMSegmentName = IN_AGGREGATION_1 and product_name = IN_AGGREGATION_2 and Product_Type='Bought';
     select round(ifnull(avg(T1MthActiveRevenue),0),1) into @AvgAmount from Dashboard_Insights_Base_ByProduct where YYYYMM = IN_FILTER_CURRENTMONTH and AGGREGATION="BY_MONTH" and CLMSegmentName = IN_AGGREGATION_1 and product_name = IN_AGGREGATION_2 and Product_Type='Bought';
     select concat(round(ifnull(((avg(T1MthActiveRevenue)-avg(T2MthActiveRevenue))/avg(T2MthActiveRevenue))*100,0),1),"%") into @AvgPercentChangeAmt from Dashboard_Insights_Base_ByProduct where YYYYMM = IN_FILTER_CURRENTMONTH and AGGREGATION="BY_MONTH" and CLMSegmentName = IN_AGGREGATION_1 and product_name = IN_AGGREGATION_2 and Product_Type='Bought';
     select round(ifnull(avg(T1MthNew),0),1) into @AvgNewCustomer from Dashboard_Insights_Base_ByProduct where YYYYMM = IN_FILTER_CURRENTMONTH and AGGREGATION="BY_MONTH" and CLMSegmentName = IN_AGGREGATION_1 and product_name = IN_AGGREGATION_2 and Product_Type='Bought';
     select round(ifnull(avg(T1MthExisting),0),1) into @AvgExistCustomer from Dashboard_Insights_Base_ByProduct where YYYYMM = IN_FILTER_CURRENTMONTH and AGGREGATION="BY_MONTH" and CLMSegmentName = IN_AGGREGATION_1 and product_name = IN_AGGREGATION_2 and Product_Type='Bought';
     select round(ifnull((avg(T1MthNew)/avg(T1MthActive))*100,0),1) into @AvgNewPercentCustomer from Dashboard_Insights_Base_ByProduct where YYYYMM = IN_FILTER_CURRENTMONTH and AGGREGATION="BY_MONTH" and CLMSegmentName = IN_AGGREGATION_1 and product_name = IN_AGGREGATION_2 and Product_Type='Bought';
     select round(ifnull((avg(T1MthExisting)/avg(T1MthActive))*100,0),1) into @AvgExistPercentCustomer from Dashboard_Insights_Base_ByProduct where YYYYMM = IN_FILTER_CURRENTMONTH and AGGREGATION="BY_MONTH" and CLMSegmentName = IN_AGGREGATION_1 and product_name = IN_AGGREGATION_2 and Product_Type='Bought';
     select round(ifnull((avg(T1MthNewRevenue)/avg(T1MthActiveRevenue))*100,0),1) into @AvgNewPercentRev from Dashboard_Insights_Base_ByProduct where YYYYMM = IN_FILTER_CURRENTMONTH and AGGREGATION="BY_MONTH" and CLMSegmentName = IN_AGGREGATION_1 and product_name = IN_AGGREGATION_2 and Product_Type='Bought';
     select round(ifnull((avg(T1MthExistingRevenue)/avg(T1MthActiveRevenue))*100,0),1) into @AvgExistPercentRev from Dashboard_Insights_Base_ByProduct where YYYYMM = IN_FILTER_CURRENTMONTH and AGGREGATION="BY_MONTH" and CLMSegmentName = IN_AGGREGATION_1 and product_name = IN_AGGREGATION_2 and Product_Type='Bought';
     select @AvgCustomer,@AvgPercentChangeCust,@AvgBills,@AvgPercentChangeBills,@AvgAmount,@AvgPercentChangeAmt,@AvgNewCustomer,@AvgExistCustomer,@AvgNewPercentCustomer,@AvgExistPercentCustomer,@AvgNewPercentRev,@AvgExistPercentRev;

    set @view_stmt = concat('create or replace view V_Dashboard_Product_OneView as
							(select "Customers" as `Card_Name`, format(ifnull(sum(T1MthActive),0),",") as `Value1`, "Average:',@AvgCustomer,'" as `Value2` from Dashboard_Insights_Base_ByProduct where YYYYMM = "',IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and CLMSegmentName = "',IN_AGGREGATION_1,'" and product_name = "',IN_AGGREGATION_2,'" and Product_Type="Bought" )
							UNION
                            (select "% Cust Change LM" as `Card_Name`, concat(round(ifnull(((sum(T1MthActive)-sum(T2MthActive))/sum(T2MthActive))*100,0),1),"%") as `Value1`, "Average:',@AvgPercentChangeCust,'" as `Value2` from Dashboard_Insights_Base_ByProduct where YYYYMM = "',IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and CLMSegmentName = "',IN_AGGREGATION_1,'" and product_name = "',IN_AGGREGATION_2,'" and Product_Type="Bought" )
                            UNION
							(select "Bills" as `Card_Name`, format(ifnull(sum(T1MthActiveBills),0),",") as `Value1`, "Average:',@AvgBills,'" as `Value2` from Dashboard_Insights_Base_ByProduct where YYYYMM = "',IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and CLMSegmentName = "',IN_AGGREGATION_1,'" and product_name = "',IN_AGGREGATION_2,'" and Product_Type="Bought" )
                            UNION
                            (select "% Bill Change LM" as `Card_Name`, concat(round(ifnull(((avg(T1MthActiveBills)-avg(T2MthActiveBills))/avg(T2MthActiveBills))*100,0),1),"%") as `Value1`, "Average:',@AvgPercentChangeBills,'" as `Value2` from Dashboard_Insights_Base_ByProduct where YYYYMM = "',IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and CLMSegmentName = "',IN_AGGREGATION_1,'" and product_name = "',IN_AGGREGATION_2,'" and Product_Type="Bought" )
                            UNION
                            (select "Amount" as `Card_Name`, format(ifnull(sum(T1MthActiveRevenue),","),0) as `Value1`, "Average:',@AvgAmount,'" as `Value2` from Dashboard_Insights_Base_ByProduct where YYYYMM = "',IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and CLMSegmentName = "',IN_AGGREGATION_1,'" and product_name = "',IN_AGGREGATION_2,'" and Product_Type="Bought" )
                            UNION
                            (select "% Amt Change LM" as `Card_Name`, concat(round(ifnull(((avg(T1MthActiveRevenue)-avg(T2MthActiveRevenue))/avg(T2MthActiveRevenue))*100,0),1),"%") as `Value1`, "Average:',@AvgPercentChangeAmt,'" as `Value2` from Dashboard_Insights_Base_ByProduct where YYYYMM = "',IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and CLMSegmentName = "',IN_AGGREGATION_1,'" and product_name = "',IN_AGGREGATION_2,'" and Product_Type="Bought" )
                            UNION
                            (select "New Customers" as `Card_Name`, format(ifnull(sum(T1MthNew),0),",") as `Value1`, "Average:',@AvgNewCustomer,'" as `Value2` from Dashboard_Insights_Base_ByProduct where YYYYMM = "',IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and CLMSegmentName = "',IN_AGGREGATION_1,'" and product_name = "',IN_AGGREGATION_2,'" and Product_Type="Bought")
                            UNION
                            (select "Existing Customers" as `Card_Name`, format(ifnull(sum(T1MthExisting),0),",") as `Value1`, "Average:',@AvgExistCustomer,'" as `Value2` from Dashboard_Insights_Base_ByProduct where YYYYMM = "',IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and CLMSegmentName = "',IN_AGGREGATION_1,'" and product_name = "',IN_AGGREGATION_2,'" and Product_Type="Bought")
                            UNION
                            (select "%New" as `Card_Name`, round(ifnull((sum(T1MthNew)/sum(T1MthActive))*100,0),1) as `Value1`, "Average:',@AvgNewPercentCustomer,'" as `Value2` from Dashboard_Insights_Base_ByProduct where YYYYMM = "',IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and CLMSegmentName = "',IN_AGGREGATION_1,'" and product_name = "',IN_AGGREGATION_2,'" and Product_Type="Bought")
                            UNION
                            (select "%Existing" as `Card_Name`, round(ifnull((sum(T1MthExisting)/sum(T1MthActive))*100,0),1) as `Value1`, "Average:',@AvgExistPercentCustomer,'" as `Value2` from Dashboard_Insights_Base_ByProduct where YYYYMM = "',IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and CLMSegmentName = "',IN_AGGREGATION_1,'" and product_name = "',IN_AGGREGATION_2,'" and Product_Type="Bought")
                            UNION
                            (select "%New Rev" as `Card_Name`, round(ifnull((sum(T1MthNewRevenue)/sum(T1MthActiveRevenue))*100,0),1) as `Value1`, "Average:',@AvgNewPercentRev,'" as `Value2` from Dashboard_Insights_Base_ByProduct where YYYYMM = "',IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and CLMSegmentName = "',IN_AGGREGATION_1,'" and product_name = "',IN_AGGREGATION_2,'" and Product_Type="Bought")
                            UNION
                            (select "%Existing Rev" as `Card_Name`, round(ifnull((sum(T1MthExistingRevenue)/sum(T1MthActiveRevenue))*100,0),1) as `Value1`, "Average:',@AvgExistPercentRev,'" as `Value2` from Dashboard_Insights_Base_ByProduct where YYYYMM = "',IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and CLMSegmentName = "',IN_AGGREGATION_1,'" and product_name = "',IN_AGGREGATION_2,'" and Product_Type="Bought")');
                            
    set @Query_String = 'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Card_Name",`Card_Name`,"Value1",`Value1`,"Value2",`Value2`) )),"]")into @temp_result from V_Dashboard_Product_OneView';                        
    END IF;
    
    IF IN_NUMBER_FORMAT = '3M'
    THEN
    
     select avg(T12MthActive) into @AvgCustomer from Dashboard_Insights_Base_ByProduct where YYYYMM = IN_FILTER_CURRENTMONTH and AGGREGATION="BY_MONTH" and CLMSegmentName = IN_AGGREGATION_1 and product_name = IN_AGGREGATION_2;
     select avg(T12MthActiveBills) into @AvgBills from Dashboard_Insights_Base_ByProduct where YYYYMM = IN_FILTER_CURRENTMONTH and AGGREGATION="BY_MONTH" and CLMSegmentName = IN_AGGREGATION_1 and product_name = IN_AGGREGATION_2;
     select avg(T12MthActiveRevenue) into @AvgAmount from Dashboard_Insights_Base_ByProduct where YYYYMM = IN_FILTER_CURRENTMONTH and AGGREGATION="BY_MONTH" and CLMSegmentName = IN_AGGREGATION_1 and product_name = IN_AGGREGATION_2;
     select avg(T12MthExistingBase) into @AvgExistCust from Dashboard_Insights_Base_ByProduct where YYYYMM = IN_FILTER_CURRENTMONTH and AGGREGATION="BY_MONTH" and CLMSegmentName = IN_AGGREGATION_1 and product_name = IN_AGGREGATION_2;

    set @view_stmt = concat('create or replace view V_Dashboard_Product_OneView as
							(select "Customers" as `Card_Name`, format(sum(T12MthActive),",") as `Value1`, "Average:',@AvgCustomer,'" as `Value2` from Dashboard_Insights_Base_ByProduct where YYYYMM = "',IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and CLMSegmentName = "',IN_AGGREGATION_1,'" and product_name = "',IN_AGGREGATION_2,'" )
							UNION
							(select "Bills" as `Card_Name`, format(sum(T12MthActiveBills),",") as `Value1`, "Average:',@AvgBills,'" as `Value2` from Dashboard_Insights_Base_ByProduct where YYYYMM = "',IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and CLMSegmentName = "',IN_AGGREGATION_1,'" and product_name = "',IN_AGGREGATION_2,'" )
                            UNION
                            (select "Amount" as `Card_Name`, format(sum(T12MthActiveRevenue),",") as `Value1`, "Average:',@AvgAmount,'" as `Value2` from Dashboard_Insights_Base_ByProduct where YYYYMM = "',IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and CLMSegmentName = "',IN_AGGREGATION_1,'" and product_name = "',IN_AGGREGATION_2,'" )
                            UNION
                            (select "Existing Customer" as `Card_Name`, format(sum(T12MthExistingBase),",") as `Value1`, "Average:',@AvgExistCust,'" as `Value2` from Dashboard_Insights_Base_ByProduct where YYYYMM = "',IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and CLMSegmentName = "',IN_AGGREGATION_1,'" and product_name = "',IN_AGGREGATION_2,'")');
                            
    set @Query_String = 'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Card_Name",`Card_Name`,"Value1",`Value1`,"Value2",`Value2`) )),"]")into @temp_result from V_Dashboard_Product_OneView';                        
    END IF;

    select @view_stmt;
	PREPARE statement from @view_stmt;
	Execute statement;
	Deallocate PREPARE statement;
    
	select @Query_String;
	PREPARE statement from @Query_String;
	Execute statement;
	Deallocate PREPARE statement; 
    SET out_result_json = @temp_result;

END$$
DELIMITER ;
