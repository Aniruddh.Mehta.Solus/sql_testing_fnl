
CREATE OR REPLACE PROCEDURE `S_UI_Execute_Campaign`(IN in_request_json json, OUT out_result_json json)
BEGIN
    
    -- User Information

    declare IN_USER_NAME varchar(240); 
    declare IN_SCREEN varchar(400); 
    declare vSuccess  varchar(400); 
    declare vErrMsg  varchar(400);
    declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text; 
	declare IN_AGGREGATION_3 text;
    
    	

  -- --------------------- Decoding the JSON -----------------------------------------------------------------------------------------------------------------------------------------------
  
  
    Set @Err =0;
	
   
   -- User Information
   
    SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1")); 
	
	-- Insert The Record
    
    INSERT INTO Event_Execution_History_Summary(Event_Id,Event_Execution_Date_Id,Start_Time,UI_Status)
    SELECT In_AGGREGATION_1,CAST(REPLACE(CURRENT_DATE(), '-', '') AS UNSIGNED),now(),'Submitted';
                 
    
		SET @Msg = 'Sucess';
	  
    	Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","',@Msg,'") )),"]") into @temp_result                       
												       ;') ;
      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;
                            
    SET out_result_json = @temp_result;


END

