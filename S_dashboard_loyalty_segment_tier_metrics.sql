DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_loyalty_segment_tier_metrics`()
BEGIN
DECLARE Cursor_Check_Var Int Default 0;
DECLARE cur_Execution_Date_ID INT(11);
DECLARE cur_Segment CHAR(20);

DECLARE curALL_SEGMENTS
    CURSOR For SELECT DISTINCT
    Segment
FROM
    Dashboard_Loyalty_Metrics_Temp
	ORDER BY Segment ASC;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET Cursor_Check_Var=1;

DELETE from log_solus where module = 'S_dashboard_segment_tier_metrics';
SET SQL_SAFE_UPDATES=0;

SELECT `Value1` into @TYSM from UI_Configuration_Global where `Config_Name` = 'FISCAL';
SELECT @TYSM;

SELECT `Value2` into @TYEM from UI_Configuration_Global where `Config_Name` = 'FISCAL';
SELECT @TYEM;

SET @LYSM = F_Month_Diff(@TYSM,12);
SET @LYEM = F_Month_Diff(@TYEM,12);

SELECT @LYSM,@LYEM;

SET @LLYSM = F_Month_Diff(@LYSM,12);
SET @LLYEM = F_Month_Diff(@LYEM,12);

SELECT @LLYSM,@LLYEM;

SET @Start_Date = date_format(date_sub(concat(date_format(current_date(),"%Y%m"),'01'), INTERVAL 14 MONTH),"%Y%m");
SET @End_Date = date_format(current_date(),"%Y%m");

SET @TM = (SELECT date_format(current_date(),"%Y%m"));
SET @LM = F_Month_Diff(@TM,1);
SET @SMLY = F_Month_Diff(@TM,12);
SET @TY=substr(@TM,1,4);
SET @LY=substr(@SMLY,1,4);
SET @LLY = @LY - 1;

SELECT @TM,@LM,@SMLY,@TY,@LY,@LLY;
	
DROP TABLE IF EXISTS Dashboard_Loyalty_Segments_Tier_Temp;
  CREATE TABLE IF NOT EXISTS Dashboard_Loyalty_Segments_Tier_Temp ( 
	`Tier` varchar(50),
	`Region` varchar(20),
	`Segment` varchar(128),
	`Measure` varchar(50),
	`SegmentPercent` decimal(10,2)
  ) ENGINE = INNODB DEFAULT CHARACTER SET=LATIN1; 
 
  ALTER TABLE Dashboard_Loyalty_Segments_Tier_Temp    
    ADD INDEX (Segment), 
    ADD INDEX (Tier),
    ADD INDEX (Measure),
	ADD INDEX (Region);

INSERT INTO log_solus(module, msg) values ('S_dashboard_segment_tier_metrics', 'Loading segment wise data');

SET Cursor_Check_Var = FALSE;
OPEN curALL_SEGMENTS;
LOOP_ALL_DATES: LOOP

	FETCH curALL_SEGMENTS into cur_Segment;
	IF Cursor_Check_Var THEN
	   LEAVE LOOP_ALL_DATES;
	END IF;

		Select cur_Segment,now();

INSERT INTO Dashboard_Loyalty_Segments_Tier_Temp
(
Tier,
Segment,
Measure,
SegmentPercent
)
SELECT
Tier,
Segment,
"Overall" AS Measure,
SegmentPercent
FROM
(
SELECT C.Tier, C.Segment, Bills AS SegmentPercent FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills', A.Tier, A.Segment FROM
(
select Tier, Segment, count(distinct Customer_Id) AS c1
from Dashboard_Loyalty_Metrics_Temp
where Tier is not NULL and Tier <> "NOT-CAPTUR"
and Segment = cur_Segment
group by 1,2
)A
,
(
select Tier, count(distinct Customer_Id) AS COUNT2
from Dashboard_Loyalty_Metrics_Temp
group by Tier
)B
WHERE A.Tier = B.Tier
) C
GROUP BY C.Tier
) AS T;

INSERT INTO Dashboard_Loyalty_Segments_Tier_Temp
(
Tier,
Segment,
Measure,
SegmentPercent
)
SELECT
Tier,
Segment,
"TY" AS Measure,
SegmentPercent
FROM
(
SELECT C.Tier, C.Segment, Bills AS SegmentPercent FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills', A.Tier, A.Segment FROM
(
select Tier, Segment, count(distinct Customer_Id) AS c1
from Dashboard_Loyalty_Metrics_Temp
where Tier is not NULL and Tier <> "NOT-CAPTUR"
and Segment = cur_Segment
and Bill_Year_Month BETWEEN @TYSM AND @TYEM
group by 1,2
)A
,
(
select Tier, count(distinct Customer_Id) AS COUNT2
from Dashboard_Loyalty_Metrics_Temp
where Bill_Year_Month BETWEEN @TYSM AND @TYEM
group by Tier
)B
WHERE A.Tier = B.Tier
) C
GROUP BY C.Tier
) AS T;

INSERT INTO Dashboard_Loyalty_Segments_Tier_Temp
(
Tier,
Segment,
Measure,
SegmentPercent
)
SELECT
Tier,
Segment,
"LY" AS Measure,
SegmentPercent
FROM
(
SELECT C.Tier, C.Segment, Bills AS SegmentPercent FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills', A.Tier, A.Segment FROM
(
select Tier, Segment, count(distinct Customer_Id) AS c1
from Dashboard_Loyalty_Metrics_Temp
where Tier is not NULL and Tier <> "NOT-CAPTUR"
and Segment = cur_Segment
and substr(Bill_Year_Month,1,4) = @LY
group by 1,2
)A
,
(
select Tier, count(distinct Customer_Id) AS COUNT2
from Dashboard_Loyalty_Metrics_Temp
where substr(Bill_Year_Month,1,4) = @LY
group by Tier
)B
WHERE A.Tier = B.Tier
) C
GROUP BY C.Tier
) AS T;

INSERT INTO Dashboard_Loyalty_Segments_Tier_Temp
(
Tier,
Segment,
Measure,
SegmentPercent
)
WITH GROUP1 AS
                            (
							SELECT Tier, Segment,
                             SegmentPercent as "Value1"
							from Dashboard_Loyalty_Segments_Tier_Temp
                            where Measure = "TY"
                            and Segment = cur_Segment
							group by 1
                            order by 1 ASC
                            )
                             ,
                             GROUP2 AS
							(
                             SELECT Tier, Segment,
                             SegmentPercent as "Value2"
							from Dashboard_Loyalty_Segments_Tier_Temp
                            where Measure = "LY"
                            and Segment = cur_Segment
							group by 1
                            order by 1 ASC
                            )
                            SELECT A.Tier, A.Segment, "TYLYCHANGE" AS Measure, (Value1-Value2) AS SegmentPercent
                            FROM GROUP1 A
                            JOIN GROUP2 B
                            ON A.Tier=B.Tier
							AND A.Segment=B.Segment;

INSERT INTO Dashboard_Loyalty_Segments_Tier_Temp
(
Tier,
Segment,
Measure,
SegmentPercent
)
SELECT
Tier,
Segment,
"L12M" AS Measure,
SegmentPercent
FROM
(
SELECT C.Tier, C.Segment, Bills AS SegmentPercent FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills', A.Tier, A.Segment FROM
(
select Tier, Segment, count(distinct Customer_Id) AS c1
from Dashboard_Loyalty_Metrics_Temp
where Tier is not NULL and Tier <> "NOT-CAPTUR"
and Segment = cur_Segment
and Bill_Year_Month BETWEEN @SMLY AND @TM
group by 1,2
)A
,
(
select Tier, count(distinct Customer_Id) AS COUNT2
from Dashboard_Loyalty_Metrics_Temp
where Bill_Year_Month BETWEEN @SMLY AND @TM
group by Tier
)B
WHERE A.Tier = B.Tier
) C
GROUP BY C.Tier
) AS T;

INSERT INTO Dashboard_Loyalty_Segments_Tier_Temp
(
Tier,
Segment,
Measure,
SegmentPercent
)
SELECT
Tier,
Segment,
"L1224M" AS Measure,
SegmentPercent
FROM
(
SELECT C.Tier, C.Segment, Bills AS SegmentPercent FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills', A.Tier, A.Segment FROM
(
select Tier, Segment, count(distinct Customer_Id) AS c1
from Dashboard_Loyalty_Metrics_Temp
where Tier is not NULL and Tier <> "NOT-CAPTUR"
and Segment = cur_Segment
and substr(Bill_Year_Month,1,4) BETWEEN @LLY AND @LY
group by 1,2
)A
,
(
select Tier, count(distinct Customer_Id) AS COUNT2
from Dashboard_Loyalty_Metrics_Temp
where substr(Bill_Year_Month,1,4) BETWEEN @LLY AND @LY
group by Tier
)B
WHERE A.Tier = B.Tier
) C
GROUP BY C.Tier
) AS T;

INSERT INTO Dashboard_Loyalty_Segments_Tier_Temp
(
Tier,
Segment,
Measure,
SegmentPercent
)
WITH GROUP1 AS
                            (
							SELECT Tier, Segment,
                             SegmentPercent as "Value1"
							from Dashboard_Loyalty_Segments_Tier_Temp
                            where Measure = "L12M"
                            and Segment = cur_Segment
							group by 1
                            order by 1 ASC
                            )
                             ,
                             GROUP2 AS
							(
                             SELECT Tier, Segment,
                             SegmentPercent as "Value2"
							from Dashboard_Loyalty_Segments_Tier_Temp
                            where Measure = "L1224M"
                            and Segment = cur_Segment
							group by 1
                            order by 1 ASC
                            )
                            SELECT A.Tier, A.Segment, "L1224MCHANGE" AS Measure, (Value1-Value2) AS SegmentPercent
                            FROM GROUP1 A
                            JOIN GROUP2 B
                            ON A.Tier=B.Tier
							AND A.Segment=B.Segment;

END LOOP LOOP_ALL_DATES;

END$$
DELIMITER ;
