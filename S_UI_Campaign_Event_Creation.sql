DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_UI_Campaign_Event_Creation`(IN in_request_json json, OUT out_result_json json)
BEGIN
    
    -- User Information

    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
    declare EDIT Bigint; 
	declare Trigger_Type VARCHAR(128);
    -- Target Audience	

	declare Trigger_Name text; 
	declare Trigger_Id Bigint;
	declare Existing_Campaign Bigint;
	declare Existing_Theme Bigint;
	declare Campaign_Type varchar(400);
	declare Region Bigint;
	declare Campaign_Segment  varchar(400);
	declare Lifecyle_Segment varchar(400);
	declare Comm_Cool_Off text;
	declare Comm_Priority text;
	declare Execution_After Bigint;
	declare Throttling_Level Bigint;
	declare Cool_Off Bigint;
	declare Attribution Bigint;
	declare Favourite_Day VARCHAR(128);
	declare Offer_Based VARCHAR(128);
	declare Reco_Based VARCHAR(128);
    -- Advanced RFMP Criteria
	declare Valid_Mobile VARCHAR(128);
	declare Valid_Email VARCHAR(128);
	declare DND VARCHAR(128);
	declare Recency_Start VARCHAR(128);
	declare Recency_End VARCHAR(128);
	declare Vintage_Start VARCHAR(128);
	declare Vintage_End VARCHAR(128);
	declare Visit_Start VARCHAR(128);
	declare Visit_End VARCHAR(128);
	declare Monetary_Value_Start VARCHAR(128);
	declare Monetary_Value_End VARCHAR(128);
	declare AP_FAV_CAT VARCHAR(128);
	declare Last_Bought_Category VARCHAR(128);
	declare Favourite_Category VARCHAR(128);
	declare Purchase_Anniversary VARCHAR(128);
	declare Multiple_of_5 VARCHAR(128);
	declare Discount VARCHAR(128);
    -- Construct Creatives 	
    declare Channel text;
	declare Personalised_Reccomendation NVARCHAR(65536);
	declare Personalised_Reco_via_Solus_Shortener NVARCHAR(65536);
	declare Personalization NVARCHAR(65536);
	declare HEADER text;
	declare Microsite_URL text;
	
	
	-- Advance Campaign
	declare TimeSlot VARCHAR(128);
	declare Offer_Type BIGINT;
	declare Offer_Code VARCHAR(128);
	declare Offer_Start_Date VARCHAR(128);
	declare Offer_End_Date VARCHAR(128);
	declare Ext_Campaign_Key VARCHAR(128);
	declare DLT VARCHAR(128);
	declare Sender_Key VARCHAR(128);
	declare Communication_Header TEXT;
	declare Advance_Trigger_SVOC text;
	declare Advance_Trigger_SVOT text;
	declare Reminder_Parent_Trigger_Id BIGINT;
	declare Reminder_Parent_Trigger_OffSetDays BIGINT;
	
	
	-- Other
	
	declare Coverage VARCHAR(128);
	
	declare Email_Template_Personalization NVARCHAR(65536);
	declare Email_Subject_Personalization NVARCHAR(65536);
	declare WhatsApp_Banner_Personalization NVARCHAR(65536);
	declare WhatsApp_Template_Id TEXT;
	declare WhatsApp_Variable1_Personalization NVARCHAR(65536);
	declare WhatsApp_Variable2_Personalization NVARCHAR(65536);
	declare WhatsApp_Variable3_Personalization NVARCHAR(65536);
	declare WhatsApp_Variable4_Personalization NVARCHAR(65536);
	declare WhatsApp_Variable5_Personalization NVARCHAR(65536);
	declare WhatsApp_Variable6_Personalization NVARCHAR(65536);
	declare WhatsApp_Variable7_Personalization NVARCHAR(65536);
	declare WhatsApp_Variable8_Personalization NVARCHAR(65536);
	declare WhatsApp_Variable9_Personalization NVARCHAR(65536);
	declare WhatsApp_Variable10_Personalization NVARCHAR(65536);

	
	declare So2_Website_Deep_Link_Page TEXT;
	declare So2_Product_Display_Page TEXT;
	declare So2_Personalised_Smart_Basket_Recommendation_Page TEXT;
	declare So2_Dynamic_Landing_Page TEXT;
	
	declare Event_Start_Date TEXT;
	declare Event_End_Date TEXT;
	declare In_Control_Group TEXT;	
	
	declare Predictive_Score TEXT;
	declare Mcore_Module TEXT;
	
	SET SQL_SAFE_UPDATES=0;
	SET foreign_key_checks=0;

	Set in_request_json = Replace(in_request_json,'\n','\\n');
  -- --------------------- Decoding the JSON -----------------------------------------------------------------------------------------------------------------------------------------------
  Select in_request_json;
   -- User Information
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    SET EDIT = json_unquote(json_extract(in_request_json,"$.EDIT"));
	SET Trigger_Type = json_unquote(json_extract(in_request_json,"$.Trigger_Type"));

    -- Target Audience

    SET Trigger_Name = json_unquote(json_extract(in_request_json,"$.Trigger_Name"));
	SET Trigger_Name = Replace(Trigger_Name,'','_');
	SET Trigger_Name =Replace(Trigger_Name,'','_');
	SET Trigger_Name =Replace(Trigger_Name,' ','_');
	SET Trigger_Id = json_unquote(json_extract(in_request_json,"$.Trigger_Id"));
    SET Existing_Campaign = json_unquote(json_extract(in_request_json,"$.Existing_Campaign"));
    SET Existing_Theme = json_unquote(json_extract(in_request_json,"$.Existing_Theme"));
    SET Campaign_Type = json_unquote(json_extract(in_request_json,"$.Campaign_Type"));
    SET Region = json_unquote(json_extract(in_request_json,"$.Region"));
    SET Campaign_Segment = json_unquote(json_extract(in_request_json,"$.Campaign_Segment"));
	
	Set Campaign_Segment= replace (Campaign_Segment,"[",'');
	Set Campaign_Segment= replace (Campaign_Segment,"]",'');
	Set Campaign_Segment= replace (Campaign_Segment,'"','');
	Set Campaign_Segment= replace (Campaign_Segment,', ',',');	
	
    SET Lifecyle_Segment = json_unquote(json_extract(in_request_json,"$.Lifecyle_Segment"));
	
	Set Lifecyle_Segment= replace (Lifecyle_Segment,"[",'');
	
	Set Lifecyle_Segment= replace (Lifecyle_Segment,"]",'');
	Set Lifecyle_Segment= replace (Lifecyle_Segment,'"','');
	Set Lifecyle_Segment= replace (Lifecyle_Segment,', ',',');
    SET Comm_Cool_Off = json_unquote(json_extract(in_request_json,"$.Comm_Cool_Off"));
	SET Comm_Priority = json_unquote(json_extract(in_request_json,"$.Comm_Priority"));
    SET Execution_After = json_unquote(json_extract(in_request_json,"$.Execution_After"));
    SET Throttling_Level = json_unquote(json_extract(in_request_json,"$.Throttling_Level"));
    SET Cool_Off = json_unquote(json_extract(in_request_json,"$.CoolOff"));
    SET Attribution = json_unquote(json_extract(in_request_json,"$.Attribution"));
    SET Favourite_Day = json_unquote(json_extract(in_request_json,"$.Favourite_Day"));
    SET Offer_Based = json_unquote(json_extract(in_request_json,"$.Offer_based"));
	SET Reco_Based = json_unquote(json_extract(in_request_json,"$.Reco_based"));
    -- Advanced RFMP Criteria
	SET Valid_Mobile = json_unquote(json_extract(in_request_json,"$.Valid_Mobile"));
	SET Valid_Email = json_unquote(json_extract(in_request_json,"$.Valid_Email"));
	SET DND = json_unquote(json_extract(in_request_json,"$.DND"));
    SET Recency_Start = json_unquote(json_extract(in_request_json,"$.Recency_Start"));
    SET Recency_End = json_unquote(json_extract(in_request_json,"$.Recency_End"));
    SET Visit_Start = json_unquote(json_extract(in_request_json,"$.Visit_Start"));
	SET Visit_End = json_unquote(json_extract(in_request_json,"$.Visit_End"));
    SET Monetary_Value_Start = json_unquote(json_extract(in_request_json,"$.Monetary_Value_Start"));
	SET Monetary_Value_End = json_unquote(json_extract(in_request_json,"$.Monetary_Value_End"));
    SET Discount = json_unquote(json_extract(in_request_json,"$.Discount"));
    SET Last_Bought_Category = json_unquote(json_extract(in_request_json,"$.Last_Bought_Category"));
    SET Favourite_Category = json_unquote(json_extract(in_request_json,"$.Favourite_Category"));
    SET AP_FAV_CAT = json_unquote(json_extract(in_request_json,"$.Active_Period_Category"));
	SET Purchase_Anniversary  = json_unquote(json_extract(in_request_json,"$.Purchase_Anniversary"));
	SET Multiple_of_5  = json_unquote(json_extract(in_request_json,"$.Multiple_Of_5_Transaction"));

    -- Construct Creatives 

    SET Channel = json_unquote(json_extract(in_request_json,"$.Channel"));
    SET Personalised_Reccomendation = json_unquote(json_extract(in_request_json,"$.Personalised_Reccomendation"));
    SET Personalised_Reco_via_Solus_Shortener = json_unquote(json_extract(in_request_json,"$.Personalised_Reco_via_Solus_Shortener"));
    SET Personalization = json_unquote(json_extract(in_request_json,"$.Personalization"));
	SET Microsite_URL =json_unquote(json_extract(in_request_json,"$.Microsite_URL"));

	-- Advance Campaign
	SET TimeSlot = json_unquote(json_extract(in_request_json,"$.TimeSlot"));
	SET Offer_Type  = json_unquote(json_extract(in_request_json,"$.Offer_Type"));
	SET Offer_Code  = json_unquote(json_extract(in_request_json,"$.Offer_Code"));

	SET Offer_Start_Date = json_unquote(json_extract(in_request_json,"$.Offer_Start_Date"));
	SET Offer_End_Date  = json_unquote(json_extract(in_request_json,"$.Offer_End_Date"));
	SET Ext_Campaign_Key  = json_unquote(json_extract(in_request_json,"$.Ext_Campaign_Key"));
	SET DLT = json_unquote(json_extract(in_request_json,"$.DLT"));
	SET Sender_Key  = json_unquote(json_extract(in_request_json,"$.Sender_Key"));
	SET Communication_Header  = json_unquote(json_extract(in_request_json,"$.Communication_Header"));
	SET Advance_Trigger_SVOC  = json_unquote(json_extract(in_request_json,"$.Advance_Trigger_SVOC"));
	SET Advance_Trigger_SVOT  = json_unquote(json_extract(in_request_json,"$.Advance_Trigger_SVOT"));
	SET Reminder_Parent_Trigger_Id  = json_unquote(json_extract(in_request_json,"$.Remainder_Parent_Trigger_Id"));
	SET Reminder_Parent_Trigger_OffSetDays  = json_unquote(json_extract(in_request_json,"$.Remainder_Parent_Trigger_OffsetDays"));
	
	
	-- Others
	SET Coverage  = json_unquote(json_extract(in_request_json,"$.Event_Coverage"));
	Set Coverage =replace (Coverage,",",'');
	
	SET Email_Template_Personalization  = json_unquote(json_extract(in_request_json,"$.Email_Template_Personalization"));
	SET Email_Subject_Personalization  = json_unquote(json_extract(in_request_json,"$.Email_Subject_Personalization"));
	SET WhatsApp_Banner_Personalization  = json_unquote(json_extract(in_request_json,"$.WhatsApp_Banner_Personalization"));
	SET WhatsApp_Template_Id  = json_unquote(json_extract(in_request_json,"$.WhatsApp_Template_Id"));
	SET WhatsApp_Variable1_Personalization  = json_unquote(json_extract(in_request_json,"$.WhatsApp_Variable1_Personalization"));
	SET WhatsApp_Variable2_Personalization  = json_unquote(json_extract(in_request_json,"$.WhatsApp_Variable2_Personalization"));
	SET WhatsApp_Variable3_Personalization  = json_unquote(json_extract(in_request_json,"$.WhatsApp_Variable3_Personalization"));
	SET WhatsApp_Variable4_Personalization  = json_unquote(json_extract(in_request_json,"$.WhatsApp_Variable4_Personalization"));
	SET WhatsApp_Variable5_Personalization  = json_unquote(json_extract(in_request_json,"$.WhatsApp_Variable5_Personalization"));
	SET WhatsApp_Variable6_Personalization  = json_unquote(json_extract(in_request_json,"$.WhatsApp_Variable6_Personalization"));
	SET WhatsApp_Variable7_Personalization  = json_unquote(json_extract(in_request_json,"$.WhatsApp_Variable7_Personalization"));
	SET WhatsApp_Variable8_Personalization  = json_unquote(json_extract(in_request_json,"$.WhatsApp_Variable8_Personalization"));
	SET WhatsApp_Variable9_Personalization  = json_unquote(json_extract(in_request_json,"$.WhatsApp_Variable9_Personalization"));
	SET WhatsApp_Variable10_Personalization  = json_unquote(json_extract(in_request_json,"$.WhatsApp_Variable10_Personalization"));
						
	SET So2_Website_Deep_Link_Page  = json_unquote(json_extract(in_request_json,"$.So2_Website_Deep_Link_Page"));
	SET So2_Product_Display_Page  = json_unquote(json_extract(in_request_json,"$.So2_Product_Display_Page"));
	SET So2_Personalised_Smart_Basket_Recommendation_Page  = json_unquote(json_extract(in_request_json,"$.So2_Personalised_Smart_Basket_Recommendation_Page"));
	SET So2_Dynamic_Landing_Page  = json_unquote(json_extract(in_request_json,"$.So2_Dynamic_Landing_Page"));
	
	SET Event_Start_Date   = json_unquote(json_extract(in_request_json,"$.Event_Start_Date"));
	SET Event_End_Date   = json_unquote(json_extract(in_request_json,"$.Event_End_Date"));
	
	SET Predictive_Score   = json_unquote(json_extract(in_request_json,"$.Predictive_Score"));
	SET Mcore_Module   = json_unquote(json_extract(in_request_json,"$.Mcore_Module"));
	SET In_Control_Group   = json_unquote(json_extract(in_request_json,"$.In_Control_Group"));
		
	Set Mcore_Module= replace (Mcore_Module,"[",'');
	Set Mcore_Module= replace (Mcore_Module,"]",'');
	Set Mcore_Module= replace (Mcore_Module,'"','');
	Set Mcore_Module= replace (Mcore_Module,', ',',');	
	
	
	
	If Attribution is null then Set Attribution=""; End If;
	
	IF Trigger_Type IS NULL OR Trigger_Type='' THEN SET Trigger_Type="Normal"; END IF;
	
	Set @temp_result =Null;
    Set @vSuccess ='';

	Set @UserId='';   
	Set @vCampaignThemeId=''; 
	Set @vCLMSegmentId=''; 
	Set @vCampaignId=''; 
	Set @vMicroSegmentId=''; 
	Set @vChannelId=''; 
	Set @vOfferTypeId='';  

	Set @vOfferName='';
	Set @vOfferDesc='';
	Set @vOfferDef='';
	Set @vOfferStartDate='';
	Set @vOfferEndDate='';
	Set @vOfferRedemptionDays='';
	Set @vOfferIsAtCustomerLevel='';

	Set @vCampTriggerName='';
	Set @vCampTriggerDesc='';
	Set @vPrecedingTriggerName=''; 
	Set @vPrecedingTriggerId='';
	Set @vDaysOffsetFromPrecedingTrigger='';
	Set @vCTState='';
	Set @vCTIsMandatory='';
	Set @vIsAReminder='';
	Set @vCTReplacedQueryAsInUI='';
	Set @vCTReplacedQuery='';
	Set @vCTIsValidSVOCCondition='';
	Set @vCTReplacedQueryBillAsInUI='';
	Set @vCTReplacedQueryBill='';
	Set @vCTIsValidBillCondition='';
	Set @vCTUsesGoodTimeModelSet ='';

	Set @ChannelName='';
	Set @vCreativeName='';
	Set @vExtCreativeKey='';
	Set @vSenderKey='';
	Set @vTimeslot_Id='';
	Set @vCreativeDesc='';
	Set @vCreative='';
	Set @vHeader='';
	Set @vMicrosite_Creative='';
	Set @vMicrosite_Header='';

	Set @vExclude_Recommended_SameAP_NotLT_FavCat='';
	Set @vExclude_Recommended_SameLTD_FavCat='';
	Set @vExclude_Recommended_SameLT_FavCat='';
	Set @vRecommendation_Column='';
	Set @vRecommendation_Type='';
	Set @vExclude_Stock_out='';
	Set @vExclude_Transaction_X_Days='';
	Set @vExclude_Recommended_X_Days='';
	Set @vExclude_Never_Brought='';
	Set @vExclude_No_Image='';
	Set @vNumber_Of_Recommendation='';
	Set @vRecommendation_Filter_Logic='';
	Set @vRecommendation_Column_Header='';
	Set @vIs_Recommended_SameLT_FavCat='';
	Set @vIs_Recommended_SameLTD_Cat='';
	Set @vIs_Recommended_SameLTD_Dep='';
	Set @vIs_Recommended_NewLaunch_Product='';
	Set @vEvent_Limit ='';
	Set @vCommunication_Cooloff='';
	Set @vCustom_Attr1='';
	Set @vCustom_Attr2='';
	Set @vCustom_Attr3='';
	Set @vCustom_Attr4='';
	Set @vCustom_Attr5='';
					
												
	Set @vCampTriggerId='';
	Set @vOfferId='';
	Set @vCreativeId='';
	Set @Event_Id='';
	Set @vErrMsg='';
	Set @vSuccess='';
	Set @Err =0;

	
	-- Checking the Recency condition
	
	If Recency_Start !='' and Recency_End =''
	    Then Set @Err = '1';
	End If;
	If Visit_Start !='' and Visit_End =''
	    Then Set @Err = '1';
	End If;
	If Monetary_Value_Start !='' and Monetary_Value_End =''
	    Then Set @Err = '1';
	End If;
	
	If Comm_Cool_Off='CAMPAIGN_COOL_OFF' and (Cool_Off ='' or Cool_Off <=0 or Cool_Off is null)
	  Then Set @Err = '1';
	End If;

	    -- Creating the Replace Queary
	Select Favourite_Day;
   Select  CONCAT_WS(' AND ',
                CASE
                    WHEN Recency_Start = '' THEN NULL
                    ELSE concat (' Recency Between ', Recency_Start ,' and ', Recency_End)
                END
                ,CASE
                    WHEN Visit_Start = '' THEN NULL
                    ELSE concat (' Lt_Num_of_Visits Between ', Visit_Start ,' and ', Visit_End)
                END
                ,CASE
                    WHEN Monetary_Value_Start = '' THEN NULL
                    ELSE concat (' LT_ABV Between ', Monetary_Value_Start ,' and ', Monetary_Value_End)
                END
                ,CASE
                    WHEN Last_Bought_Category = '' THEN NULL
                    ELSE Concat ('LTD_Fav_Cat_LOV_ID in (',Last_Bought_Category,')')
                END
                ,CASE
                    WHEN Favourite_Category = '' THEN NULL
                    ELSE Concat ('LT_Fav_Cat_LOV_ID in (',Favourite_Category,')')
                END
                ,CASE
                    WHEN Favourite_Day = "No" or Favourite_Day = " " or Favourite_Day is null THEN NULL
                    ELSE  Concat ('Lt_Fav_Day_Of_Week =weekDay(now())')
                END
				 ,CASE
                    WHEN Purchase_Anniversary = '' THEN NULL
                    ELSE  Concat ('((Vintage/365)  in (',Purchase_Anniversary,'))')
                END
				 ,CASE
                    WHEN Multiple_of_5 ='' THEN NULL
                    ELSE  Concat ('((Lt_Num_of_Visits/5) in (',Multiple_of_5,'))')
                END
				 ,CASE
                    WHEN AP_FAV_CAT ='' THEN NULL
                    ELSE Concat ('AP_Fav_Cat_LOV_ID in (',AP_FAV_CAT,')')
                END
				,CASE
                    WHEN Discount ='' THEN NULL
                    ELSE Concat ('Lt_Disc_Percent in (',Discount,')')
                END
				
                ) into @Replaced_Query;

	
   	If (@Replaced_Query is null or  @Replaced_Query ='') 
   	Then Set @Replaced_Query = '1=1';
	End if;
	
	Select @Replaced_Query;
	Select Advance_Trigger_SVOC;
	Set Advance_Trigger_SVOC =replace (Advance_Trigger_SVOC,"\n",'');
	
	Select Advance_Trigger_SVOC;
	Set Advance_Trigger_SVOT =replace (Advance_Trigger_SVOT,"\n",'');
	
	IF Advance_Trigger_SVOC IS NULL OR Advance_Trigger_SVOC ='' 
		THEN SET Advance_Trigger_SVOC="1=1";
		Select 1;
	ELSE 
              SET @Replaced_Query = CONCAT('( ',@Replaced_Query,' )');
			  Set @Replaced_Query = CONCAT(@Replaced_Query,' AND ' , Advance_Trigger_SVOC);
             
	END IF;	
	 Select @Replaced_Query,Advance_Trigger_SVOC;
	 
	IF Advance_Trigger_SVOT IS NULL OR Advance_Trigger_SVOT ='' THEN SET Advance_Trigger_SVOT='';END IF;	
	
   
   If Reminder_Parent_Trigger_Id IS NOT NULL OR Reminder_Parent_Trigger_Id !=''
		THEN
		
			Select  RankedPickListStory_Id into Personalised_Reccomendation  from CLM_Campaign_Events A,CLM_Creative B
			where A.CreativeId=B.CreativeId and EventId=Reminder_Parent_Trigger_Id;
		
	END IF;
	Select @Replaced_Query,Advance_Trigger_SVOT;
	If So2_Personalised_Smart_Basket_Recommendation_Page= "YES"
		THEN
			Select Story_Id  into Personalised_Reccomendation from RankedPickList_Stories_Master where Name ='Top SmartBasket recos with reorder not included';
	End If;	
	
	Set @Recommendation_Type=Personalised_Reccomendation;
	
	If @Recommendation_Type ='' or  @Recommendation_Type is null or @Recommendation_Type <=0
    Then Set @vNumber_Of_Recommendation ='0' ;
		Set @Recommendation_Type='0';	
	else Set @vNumber_Of_Recommendation ='5' ;
	
	END If;    
		
	Select ChannelName into Channel from CLM_Campaign_Events A,CLM_Channel B
		where A.ChannelId= B.ChannelId and  EventId=Reminder_Parent_Trigger_Id;	

	Set @Email_Adapter = Null;
 
	If Channel = "EMAIL"
	   Then 
	   
			   SET @sql_stmt000=concat('SELECT type into @Email_Adapter from Downstream_Adapter_Details where Channel = "Email" and In_Use = "Enable";');
			   PREPARE statement from @sql_stmt000;
				Execute statement;
				Deallocate PREPARE statement;
				select @Email_Adapter;

				if @Email_Adapter is NULL
				Then set @Email_Adapter = 'SFTP';
				 End if;
	End if;

	Set @WhatsApp_Adapter = Null;
	If Channel = "WhatsApp"
	   Then
			  SET @sql_stmt000=concat('SELECT type into @WhatsApp_Adapter from Downstream_Adapter_Details where Channel = "WhatsApp" and In_Use = "Enable";');
			   PREPARE statement from @sql_stmt000;
				Execute statement;
				Deallocate PREPARE statement;
			
			select @WhatsAPP_Adapter;
			if @WhatsApp_Adapter is NULL
			  Then set @WhatsAPP_Adapter = 'SFTP';
			End if;
	End if;
                                
	
								If Channel ="PN"
								Then Select `Value` into Communication_Header from M_Config where Name ='PN_Communication_Header';
								End if;
                                
                               
                               
                                
                                  If Channel ="EMAIL" and @Email_Adapter = "SFTP"
								  Then Select `Value` into Communication_Header from M_Config where Name ='EMAIL_Communication_Header';
								  End if;
                                
                                
                                    If Channel ="WhatsApp" and @WhatsApp_Adapter = "SFTP"
								    Then Select `Value` into Communication_Header from M_Config where Name ='WhatsApp_Communication_Header';
								    End if;
                                
                                
                                
	

                                
		IF Channel ="EMAIL"  and @Email_Adapter !="SFTP"
		THEN
				Set Communication_Header='Customer_Key|EmailSection_1|EmailSection_2|EmailSection_3|EmailSection_4|EmailSection_5|EmailSection_6|Reco1_ProductURL|Reco2_ProductURL|Reco3_ProductURL|Reco4_ProductURL|Reco5_ProductURL|Reco6_ProductURL|Reco1_ProductName|Reco2_ProductName|Reco3_ProductName|Reco4_ProductName|Reco5_ProductName|Reco6_ProductName|Reco1_Image|Reco2_Image|Reco3_Image|Reco4_Image|Reco5_Image|Reco6_Image|Reco1_ActualPrice|Reco2_ActualPrice|Reco3_ActualPrice|Reco4_ActualPrice|Reco5_ActualPrice|Reco6_ActualPrice|Reco1_DiscountPrice|Reco2_DiscountPrice|Reco3_DiscountPrice|Reco4_DiscountPrice|Reco5_DiscountPrice|Reco6_DiscountPrice|Reco1_Qty|Reco2_Qty|Reco3_Qty|Reco4_Qty|Reco5_Qty|Reco6_Qty|Name|Communication_Template_ID|Customer_Key|EmailSubject';
		End If;
        
        
		
		IF Channel ="WHATSAPP"  and @WhatsApp_Adapter != "SFTP"
		THEN
			Set Communication_Header=Concat('Mobile|Event_Execution_Id|WhatsApp_Sender_Id|Template_ID|WhatsApp_URL|Section1|Section2|Section3|Section4|Section5|Section6|Section7|Section8|Section9|Section10');
		End If;
		
		Select Value into @WhatsApp_Sender_Id from M_Config where Name="WhatsApp_Sender_Id";

		IF Channel ="WHATSAPP" and @WhatsApp_Adapter != "SFTP"
		THEN	
			
			Set Personalization=Concat('<SOLUS_PFIELD>Mobile_No</SOLUS_PFIELD>|<SOLUS_PFIELD>Event_Execution_Id</SOLUS_PFIELD>|',@WhatsApp_Sender_Id,'|');
			Set Personalization=Concat(Personalization,WhatsApp_Template_Id);
			Set Personalization=Concat(Personalization,'|',WhatsApp_Banner_Personalization);
			Set Personalization=Concat(Personalization,'|',WhatsApp_Variable1_Personalization);
			Set Personalization=Concat(Personalization,'|',WhatsApp_Variable2_Personalization);
			Set Personalization=Concat(Personalization,'|',WhatsApp_Variable3_Personalization);
			Set Personalization=Concat(Personalization,'|',WhatsApp_Variable4_Personalization);
			Set Personalization=Concat(Personalization,'|',WhatsApp_Variable5_Personalization);
			Set Personalization=Concat(Personalization,'|',WhatsApp_Variable6_Personalization);
			Set Personalization=Concat(Personalization,'|',WhatsApp_Variable7_Personalization);
			Set Personalization=Concat(Personalization,'|',WhatsApp_Variable8_Personalization);
			Set Personalization=Concat(Personalization,'|',WhatsApp_Variable9_Personalization);
			Set Personalization=Concat(Personalization,'|',WhatsApp_Variable10_Personalization);
						
			
			
		End If;
		
		IF Channel ="EMAIL" and  @Email_Adapter !="SFTP"
		THEN
		
					Set Personalization=concat('<SOLUS_PFIELD>Email_Id</SOLUS_PFIELD>|null|null|null|null|null|null|null|null|null|null|null|null|null|null|null|null|null|null|null|null|null|null|null|null|null|null|null|null|null|null|null|null|null|null|null|null|null|null|null|null|null|null|<SOLUS_PFIELD>Name</SOLUS_PFIELD>|<SOLUS_PFIELD>Communication_Template_ID</SOLUS_PFIELD>|<SOLUS_PFIELD>Customer_Key</SOLUS_PFIELD>|',Email_Subject_Personalization);
				
		End If;
		
		
		
		If Valid_Mobile ="NO"
		THEN	Set Valid_Mobile ='-1,1';
		Else Set Valid_Mobile ='1';
		End If;

		If Valid_Email ="NO"
			THEN	Set Valid_Email ='-1,1';
		Else Set Valid_Email ='1';
		End If;

		If DND ="NO"
			THEN	Set DND ='-1,1';
		Else Set DND ='1';
		End If;
		
		
	Select  IN_USER_NAME ,IN_SCREEN ,EDIT ,Trigger_Type,Trigger_Name,Trigger_Id,Existing_Campaign, Existing_Theme,Campaign_Type,Region ,Campaign_Segment ,Lifecyle_Segment ,
	 Comm_Cool_Off,	 Comm_Priority,	 Execution_After ,	 Throttling_Level ,	 Cool_Off ,	 Attribution ,	 Favourite_Day ,	 Offer_Based ,	 Reco_Based ,	 Valid_Mobile ,
	 Valid_Email ,	 DND ,	 Recency_Start ,	 Recency_End  ,	 Visit_Start ,Visit_End,Monetary_Value_Start,Monetary_Value_End ,	 AP_FAV_CAT ,	 Last_Bought_Category ,	 Favourite_Category  ,     Channel ,	 Personalised_Reccomendation ,	 Personalised_Reco_via_Solus_Shortener ,	 Personalization ,	 HEADER ,
	 Microsite_URL , TimeSlot ,	 Offer_Type ,	 Offer_Code ,	 Offer_Start_Date ,	 Offer_End_Date ,	 Ext_Campaign_Key ,	 DLT ,	 Sender_Key ,	 Communication_Header  ,
	 Advance_Trigger_SVOC ,	 Advance_Trigger_SVOT,@Replaced_Query,Reminder_Parent_Trigger_Id,Reminder_Parent_Trigger_OffSetDays,Event_Start_Date,Event_End_Date,Personalization ;

 
		If @Err = '0' 

		Then

-- ------------------------------------------------- Call the Event Creation Proc ---------------------------------------------------------------------------------------------------------
						SELECT CampTriggerName into @CampTriggerName FROM CLM_Campaign_Trigger where CampTriggerName =Trigger_Name;		

							Select @CampTriggerName;
		     			            
                                    
                                    
                                    

									If Lifecyle_Segment = "All_segment" OR Lifecyle_Segment = "All"
									Then
										  Select group_concat(CLMSegmentId) into Lifecyle_Segment from CLM_Segment where In_Use="Yes" ;
													
									End If; -- End If of Lifecyle_Segment is All Segment or not.
									
											SET @UserId =1;
											
											IF Trigger_Type ="GTM_Offer_Based" 
												THEN
													Set Execution_After=Reminder_Parent_Trigger_Id;
													SET @vCampTriggerName=Trigger_Name;
													SET @vCampTriggerDesc=Trigger_Name;
													
													Select CampaignThemeId into @vCampaignThemeId from CLM_Campaign_Events where  EventId=Reminder_Parent_Trigger_Id;
													
													Select CampaignId into @vCampaignId from CLM_Campaign_Events where  EventId=Reminder_Parent_Trigger_Id;
													
													Select Type_Of_Event into Campaign_Type from Event_Master_UI where Event_id=Reminder_Parent_Trigger_Id;
													
													Select MicroSegmentId into @vMicroSegmentId from CLM_Campaign_Events where  EventId=Reminder_Parent_Trigger_Id;		
													
													Select CLMSegmentId into @vCLMSegmentId from CLM_Campaign_Events where  EventId=Reminder_Parent_Trigger_Id;
													
													Select  Communication_Cooloff into @vCommunication_Cooloff from CLM_Campaign_Events A,CLM_Creative B
													where A.CreativeId=B.CreativeId		and EventId=Reminder_Parent_Trigger_Id;
													
													Select Event_Limit into  @vEvent_Limit from CLM_Campaign_Events A,CLM_Campaign_Trigger B
													where A.CampTriggerId=B.CampTriggerId and EventId=Reminder_Parent_Trigger_Id;

													Select Reco_Based into Reco_Based from Event_Master_UI where Event_id=Reminder_Parent_Trigger_Id;
													
													Select Offer_Based into Offer_Based from Event_Master_UI where Event_id=Reminder_Parent_Trigger_Id;
													
													Select Valid_Mobile into Valid_Mobile from Event_Master_UI where Event_id=Reminder_Parent_Trigger_Id;
													Select Valid_Email into Valid_Email from Event_Master_UI where Event_id=Reminder_Parent_Trigger_Id;
													Select DND into DND from Event_Master_UI where Event_id=Reminder_Parent_Trigger_Id;
													
													Select ChannelId into @vChannelId from CLM_Campaign_Events where  EventId=Reminder_Parent_Trigger_Id;

													SET @vCreativeName= Trigger_Name;
													SET @vCreativeDesc= Trigger_Name;
													SET @vCreative=Personalization;
													SET @vHeader= Communication_Header;
															
													Select CTUsesGoodTimeModel into  @vCTUsesGoodTimeModel from CLM_Campaign_Events A,CLM_Campaign_Trigger B
													where A.CampTriggerId=B.CampTriggerId and EventId=Reminder_Parent_Trigger_Id;												
													Select RegionSegmentId into Region from CLM_Campaign_Events where  EventId=Reminder_Parent_Trigger_Id;	
													
													SET @vIsAReminder='1';
													
												
															
														
													SET @vExtCreativeKey =Trigger_Name; 
													
													SET @vSenderKey= Trigger_Name ;
															
													Select  Timeslot_Id into @vTimeslot_Id from CLM_Campaign_Events A,CLM_Creative B
															where A.CreativeId=B.CreativeId and EventId=Reminder_Parent_Trigger_Id;

															
													IF @vTimeslot_Id <=0 THEN SET @vTimeslot_Id= 1; END IF;	
													
													Select  Website_URL into Microsite_URL  from CLM_Campaign_Events A,CLM_Creative B
															where A.CreativeId=B.CreativeId and EventId=Reminder_Parent_Trigger_Id;

													Select OfferTypeId into @vOfferTypeId from CLM_OfferType where OfferTypeName ='Reminder_Offer';
													Select OfferName into @vOfferName from CLM_Offer   where OfferName ='Reminder_Offer';
													Select OfferDesc into @vOfferDesc from CLM_Offer where OfferName ='Reminder_Offer';
													Select OfferDef into @vOfferDef from CLM_Offer where OfferName ='Reminder_Offer';
													Select OfferStartDate into @vOfferStartDate from CLM_Offer where OfferName ='Reminder_Offer';
													Select OfferEndDate into @vOfferEndDate from CLM_Offer where OfferName ='Reminder_Offer';
															
													Select CTReplacedQuery into @vCTReplacedQuery from CLM_Campaign_Events A,CLM_Campaign_Trigger B
															where A.CampTriggerId=B.CampTriggerId and EventId=Reminder_Parent_Trigger_Id;
													
															SET @vCTReplacedQueryAsInUI= @vCTReplacedQuery;
															SET @vCTIsValidSVOCCondition= '';
															
															
													Select CTReplacedQueryBill into @vCTReplacedQueryBill from CLM_Campaign_Events A,CLM_Campaign_Trigger B
															where A.CampTriggerId=B.CampTriggerId and EventId=Reminder_Parent_Trigger_Id;
															
													SET @vCTReplacedQueryBillAsInUI=@vCTReplacedQueryBill;
													SET @vCTIsValidBillCondition=''; 
															
															
															
											
												ELSE 
												
													SET @vCampTriggerName=Trigger_Name;
													SET @vCampTriggerDesc=Trigger_Name;
													
													SET @vCampaignId=Existing_Campaign;
													
													SET @vCampaignThemeId=Existing_Theme;
													
													Select Campaign_Type;
													
													 
													Select MicroSegmentId into @vMicroSegmentId From CLM_MicroSegment where MicroSegmentName = "NONE"; 
													
											
													
													SET @vCLMSegmentId=Lifecyle_Segment;
													
													SET @vCommunication_Cooloff= Cool_Off;
													If Throttling_Level =0 then Set Throttling_Level ="-1"; end if;
													If Throttling_Level ="0" then Set Throttling_Level ="-1"; end if;
													If Throttling_Level = '' then Set Throttling_Level ="-1"; end if;
													If Throttling_Level is null then Set Throttling_Level ="-1"; end if;
									
													SET @vEvent_Limit= Throttling_Level;
														
													Select Offer_Based;
													Select Reco_Based;
													Select Valid_Mobile;
													Select Valid_Email;
													Select DND;
														
													Select  ChannelId into @vChannelId from CLM_Channel where ChannelName = Channel;
													
													IF @vChannelId IS NULL or @vChannelId='' or @vChannelId=0
														THEN		
															Set @New_CLM_Channel='';
															Set @New_CLM_Channel=Channel;
															INSERT ignore INTO `CLM_Channel` (`ChannelName`, `ChannelDesc`, `File_Delimiter`, `File_Extension`, `Marker_File_Required`, `Marker_File_Extension`, `CreatedBy`, `ModifiedBy`)
															VALUES (@New_CLM_Channel, @New_CLM_Channel, '|', 'csv', 'Y', 'done', '1', '1');

													END IF;		
														
													Select  ChannelId into @vChannelId from CLM_Channel where ChannelName = Channel;	
													SET @vIsAReminder='';
													SET @vCreativeName= Trigger_Name;
													SET @vCreativeDesc= Trigger_Name;
													SET @vCreative=Personalization;
													SET @vHeader= Communication_Header;
													
													
													IF Ext_Campaign_Key IS NULL OR Ext_Campaign_Key='' 
														THEN	
															Select Value into Ext_Campaign_Key from M_Config where Name='Default_Ext_Campaign_Key';	
													END IF;	
													
													IF Ext_Campaign_Key IS NULL OR Ext_Campaign_Key='' 
															THEN	
																SET @vExtCreativeKey =Trigger_Name; 
													ELSE SET @vExtCreativeKey =Ext_Campaign_Key; 
													END IF;	
													
					
														
														
													IF Sender_Key IS NULL OR Sender_Key='' 
														THEN	
															Select Value into @vSenderKey from M_Config where Name='Default_Sender_Key';	
													ELSE  SET @vSenderKey= Sender_Key ;
													END IF;	
														
														
													IF TimeSlot IS NULL OR TimeSlot ='' 
														THEN	
															SET @vTimeslot_Id= 1;
													ELSE 		Select Timeslot_Id into @vTimeslot_Id from CLM_Timeslot_Master where Timeslot_Name= TimeSlot;
													END IF;	
														
													
													IF Offer_Type IS NULL OR Offer_Type =''
														THEN Select OfferTypeId into @vOfferTypeId from CLM_OfferType where OfferTypeName ='NO_OFFER';
																
													ELSE	SET @vOfferTypeId = Offer_Type;
																
													END IF;	
													Select Offer_Code;
													
													IF Offer_Code IS NULL OR Offer_Code =''
														THEN 
																
															IF Offer_Type IS NULL OR Offer_Type =''
																THEN 
																	Select OfferId into @vOfferId from CLM_Offer where OfferName ='NO_OFFER';
																	Select OfferName into @vOfferName from CLM_Offer   where OfferName ='NO_OFFER';
																	Select OfferDesc into @vOfferDesc from CLM_Offer where OfferName ='NO_OFFER';
																	Select OfferDef into @vOfferDef from CLM_Offer where OfferName ='NO_OFFER';
																
															ELSE	Select OfferId into @vOfferId from CLM_Offer where OfferName =Offer_Type;
																	SET @vOfferName= Offer_Type;
																	SET @vOfferDesc= Offer_Type;
																	SET @vOfferDef= Offer_Type;	
																
															END IF;
															
																
													ELSE	
															SET @vOfferName= Offer_Code;
															SET @vOfferDesc= Offer_Code;
															SET @vOfferDef= Offer_Code;
															Select OfferId into @vOfferId from CLM_Offer where OfferName=Offer_Code;															
																
													END IF;
													Select Offer_Code;
													
													IF Offer_Code="CUSTOMER_LEVEL_OFFER"
														THEN
															Select OfferId into @vOfferId from CLM_Offer where OfferName ='CUSTOMER_LEVEL_OFFER' limit 1;
																	Select OfferName into @vOfferName from CLM_Offer   where OfferName ='CUSTOMER_LEVEL_OFFER' limit 1;
																	Select OfferDesc into @vOfferDesc from CLM_Offer where OfferName ='CUSTOMER_LEVEL_OFFER' limit 1;
																	Select OfferDef into @vOfferDef from CLM_Offer where OfferName ='CUSTOMER_LEVEL_OFFER' limit 1;
													END IF;
												
													IF Offer_Start_Date IS NULL OR Offer_Start_Date =''
														THEN Select OfferStartDate into @vOfferStartDate from CLM_Offer where OfferName ='NO_OFFER';
															
													ELSE	SET @vOfferStartDate = Offer_Start_Date;
															
													END IF;
														
													IF Offer_End_Date IS NULL OR Offer_End_Date =''
														THEN 	Select OfferEndDate into @vOfferEndDate from CLM_Offer where OfferName ='NO_OFFER';
															
													ELSE	SET @vOfferEndDate = Offer_End_Date;
																
													END IF;
		
													SET  @vOfferRedemptionDays=0;
													SET  @vOfferIsAtCustomerLevel=0;	
													
													SET @vCTReplacedQuery= @Replaced_Query;
													SET @vCTReplacedQueryAsInUI= @Replaced_Query;
													SET @vCTIsValidSVOCCondition= 1;
													
													SET @vCTReplacedQueryBillAsInUI=Advance_Trigger_SVOT;
													SET @vCTReplacedQueryBill=Advance_Trigger_SVOT;
													SET @vCTIsValidBillCondition=1;
													
													SET @vCTUsesGoodTimeModel=0 ; 
													
														
														
											
											
											
														
																												
											END IF;	
											
											
											IF Reminder_Parent_Trigger_Id IS NULL OR Reminder_Parent_Trigger_Id =''
												THEN 	SET @vPrecedingTriggerId='';
														SET @vPrecedingTriggerName=''; 
												
											ELSE	
													Select A.CampTriggerId into @vPrecedingTriggerId 
													from CLM_Campaign_Events A,CLM_Campaign_Trigger B
													where A.CampTriggerId=B.CampTriggerId and EventId=Reminder_Parent_Trigger_Id;
													
													Select B.CampTriggerName into @vPrecedingTriggerName 
													from CLM_Campaign_Events A,CLM_Campaign_Trigger B
													where A.CampTriggerId=B.CampTriggerId and EventId=Reminder_Parent_Trigger_Id;
														
													
											END IF;
											
											IF Reminder_Parent_Trigger_OffSetDays IS NULL OR Reminder_Parent_Trigger_OffSetDays =''
												THEN 	SET @vDaysOffsetFromPrecedingTrigger = '';
												
											ELSE	
													SET @vDaysOffsetFromPrecedingTrigger=Reminder_Parent_Trigger_OffSetDays;
													
											END IF;
											
											
											
											SET @vCTState= 3;
											SET @vCTIsMandatory= 1;
											
											SET @vMicrosite_Creative='';
											SET @vMicrosite_Header=''; 
											SET @vExclude_Recommended_SameAP_NotLT_FavCat='';
											SET @vExclude_Recommended_SameLTD_FavCat='';
											SET @vExclude_Recommended_SameLT_FavCat='';
											SET @vRecommendation_Column ='';
											SET @vRecommendation_Type= @Recommendation_Type; 
											SET @vExclude_Stock_out='';
											SET @vExclude_Transaction_X_Days='';
											SET @vExclude_Recommended_X_Days='';
											SET @vExclude_Never_Brought='';
											SET @vExclude_No_Image='';
											SET @vNumber_Of_Recommendation= @vNumber_Of_Recommendation;
											SET @vRecommendation_Filter_Logic='';
											SET @vRecommendation_Column_Header=''; 
											SET @vIs_Recommended_SameLT_FavCat=''; 
											SET @vIs_Recommended_SameLTD_Cat='';
											SET @vIs_Recommended_SameLTD_Dep='';
											SET @vIs_Recommended_NewLaunch_Product=''; 
											Select Trigger_Id;
											
											If Trigger_Id is Not Null
											Then 
												  SET  @vCampTriggerId =Trigger_Id ;
											End If;
											
											If Trigger_Id is Null
											Then 
											   Select CampTriggerId into @vCampTriggerId From CLM_Campaign_Trigger where CampTriggerName = Trigger_Name; 
											End If;
											
											Select EventId into @Event_Id from CLM_Campaign_Events where CampTriggerId=Trigger_Id;
											
											Select CreativeId into @vCreativeId from CLM_Creative where CreativeName = Trigger_Name;

											Select 

												@UserId ,   
												@vCampaignThemeId , 
												@vCLMSegmentId , 
												@vCampaignId , 
												@vMicroSegmentId , 
												@vChannelId , 
												@vOfferTypeId ,  

												@vOfferName, 
												@vOfferDesc,
												@vOfferDef,
												@vOfferStartDate,
												@vOfferEndDate,
												@vOfferRedemptionDays ,
												@vOfferIsAtCustomerLevel ,

												@vCampTriggerName, 
												@vCampTriggerDesc,
												@vPrecedingTriggerName, 
												@vPrecedingTriggerId ,
												@vDaysOffsetFromPrecedingTrigger ,
												@vCTState ,
												@vCTIsMandatory ,
												@vIsAReminder ,
												@vCTReplacedQueryAsInUI ,
												@vCTReplacedQuery ,
												@vCTIsValidSVOCCondition ,
												@vCTReplacedQueryBillAsInUI ,
												@vCTReplacedQueryBill ,
												@vCTIsValidBillCondition ,
												@vCTUsesGoodTimeModel	,

												@vCreativeName, 
												@vExtCreativeKey,
											    @vSenderKey ,
											    @vTimeslot_Id,
												@vCreativeDesc,
												@vCreative ,
												@vHeader ,
											    @vMicrosite_Creative ,
											    @vMicrosite_Header ,

												@vExclude_Recommended_SameAP_NotLT_FavCat ,
												@vExclude_Recommended_SameLTD_FavCat ,
												@vExclude_Recommended_SameLT_FavCat ,
												@vRecommendation_Column,
												@vRecommendation_Type,
												@vExclude_Stock_out ,
												@vExclude_Transaction_X_Days ,
												@vExclude_Recommended_X_Days ,
												@vExclude_Never_Brought ,
												@vExclude_No_Image ,
												@vNumber_Of_Recommendation ,
												@vRecommendation_Filter_Logic,
												@vRecommendation_Column_Header,
												@vIs_Recommended_SameLT_FavCat ,
												@vIs_Recommended_SameLTD_Cat ,
												@vIs_Recommended_SameLTD_Dep ,
												@vIs_Recommended_NewLaunch_Product ,
												@vEvent_Limit  ,
											    @vCommunication_Cooloff ,
												
												@vCustom_Attr1,
												@vCustom_Attr2,
												@vCustom_Attr3,
												@vCustom_Attr4,
												@vCustom_Attr5,
												
												@vCampTriggerId ,
												@vOfferId ,
												@vCreativeId ,
												@Event_Id ,
												@vErrMsg ,
												@vSuccess
											;

											-- Call the Trigger Creation Funcation

											  Call PROC_UI_Create_Event (

												@UserId ,   
												@vCampaignThemeId , 
												@vCLMSegmentId , 
												@vCampaignId , 
												@vMicroSegmentId , 
												@vChannelId , 
												@vOfferTypeId ,  

												@vOfferName, 
												@vOfferDesc,
												@vOfferDef,
												@vOfferStartDate,
												@vOfferEndDate,
												@vOfferRedemptionDays ,
												@vOfferIsAtCustomerLevel ,

												@vCampTriggerName, 
												@vCampTriggerDesc,
												@vPrecedingTriggerName, 
												@vPrecedingTriggerId ,
												@vDaysOffsetFromPrecedingTrigger ,
												@vCTState ,
												@vCTIsMandatory ,
												@vIsAReminder ,
												@vCTReplacedQueryAsInUI ,
												@vCTReplacedQuery ,
												@vCTIsValidSVOCCondition ,
												@vCTReplacedQueryBillAsInUI ,
												@vCTReplacedQueryBill ,
												@vCTIsValidBillCondition ,
												@vCTUsesGoodTimeModel	,

												@vCreativeName, 
												@vExtCreativeKey,
											    @vSenderKey ,
											    @vTimeslot_Id,
												@vCreativeDesc,
												@vCreative ,
												@vHeader ,
											    @vMicrosite_Creative ,
											    @vMicrosite_Header ,

												@vExclude_Recommended_SameAP_NotLT_FavCat ,
												@vExclude_Recommended_SameLTD_FavCat ,
												@vExclude_Recommended_SameLT_FavCat ,
												@vRecommendation_Column,
												@vRecommendation_Type,
												@vExclude_Stock_out ,
												@vExclude_Transaction_X_Days ,
												@vExclude_Recommended_X_Days ,
												@vExclude_Never_Brought ,
												@vExclude_No_Image ,
												@vNumber_Of_Recommendation ,
												@vRecommendation_Filter_Logic,
												@vRecommendation_Column_Header,
												@vIs_Recommended_SameLT_FavCat ,
												@vIs_Recommended_SameLTD_Cat ,
												@vIs_Recommended_SameLTD_Dep ,
												@vIs_Recommended_NewLaunch_Product ,
												@vEvent_Limit  ,
											    @vCommunication_Cooloff ,
										
												
												
												@vCampTriggerId ,
												@vOfferId ,
												@vCreativeId ,
												@Event_Id ,
												@vErrMsg ,
												@vSuccess

											   );
											   

											   -- Inserting the Data into Event_Master_UI

											   if @vSuccess ='1'
											   Then 

														
        
												
														-- Inserting the Data into Event_Master_UI
														Insert ignore into Event_Master_UI (Event_Id )
														Values (@Event_Id);
														
														Update Event_Master_UI
														Set Message='',
														Event_Execution_Date_ID='-1',
														-- No_Of_Communication_Sent ='-1',
														-- No_Of_Communication_Rejected ='-1',
														Test_Moblie_Number_1 ='-1',
														Test_Moblie_Number_2 ='-1',
														Test_Moblie_Number_3 ='-1',
														Test_Moblie_Number_4 ='-1',
														Test_Moblie_Number_5 ='-1',
														Status_Tested ="",
														Status_Sent="",
														Status_QC="",
														Status_DLT="",
														Status_Schedule= "",
														Status_Active="",
														Status_RunMode= "",
														Trigger_Type ="",
														Recency_Start='',
														Recency_End='',
														Visit_Start ='',
														Visit_End ='',
														Monetary_Value_Start='',
														Monetary_Value_End='',
														Last_Bought_Category ='',
														Favourite_Category='',
														AP_Favourite_Category='',
														Discount ='',
														Purchase_Anniversary='',
														Multiple_of_5 ='',
														Favourite_Day ='',
														EventSegmentName ='',
														Email_Template_Personalization='',
														Email_Subject_Personalization='',
														WhatsApp_Banner_Personalization='',
														WhatsApp_Variable1_Personalization='',
														WhatsApp_Variable2_Personalization='',
														WhatsApp_Variable3_Personalization='',
														WhatsApp_Variable4_Personalization='',
														WhatsApp_Variable5_Personalization='',
														So2_Website_Deep_Link_Page='',
														So2_Product_Display_Page='',
														So2_Personalised_Smart_Basket_Recommendation_Page='',
														So2_Dynamic_Landing_Page='',
														Comm_Cool_Off_Type=''
													Where Event_ID= @Event_Id;

														Update Event_Master_UI
														     Set Type_Of_Event=Campaign_Type,
																InputJson=	in_request_json,
																Valid_Mobile =Valid_Mobile,
																Valid_Email =Valid_Email,
																DND =DND,
																Reco_Based=Reco_Based,
																Offer_Based=Offer_Based,
																Trigger_Type =Trigger_Type,
																Recency_Start=Recency_Start,
																Recency_End=Recency_End,
																Visit_Start =Visit_Start,
																Visit_End =Visit_End,
																Monetary_Value_Start=Monetary_Value_Start,
																Monetary_Value_End=Monetary_Value_End,
																Last_Bought_Category =Last_Bought_Category,
																Favourite_Category=Favourite_Category,
																Discount =Discount,
																Favourite_Day =Favourite_Day,
																Purchase_Anniversary=Purchase_Anniversary,
																Multiple_of_5 =Multiple_of_5,
																Favourite_Day =Favourite_Day,
																AP_Favourite_Category=AP_FAV_CAT,
																Email_Template_Personalization=Email_Template_Personalization,
																Email_Subject_Personalization=Email_Subject_Personalization,
																WhatsApp_Banner_Personalization=WhatsApp_Banner_Personalization,
																WhatsApp_Variable1_Personalization=WhatsApp_Variable1_Personalization,
																WhatsApp_Variable2_Personalization=WhatsApp_Variable2_Personalization,
																WhatsApp_Variable3_Personalization=WhatsApp_Variable3_Personalization,
																WhatsApp_Variable4_Personalization=WhatsApp_Variable4_Personalization,
																WhatsApp_Variable5_Personalization=WhatsApp_Variable5_Personalization,
																So2_Website_Deep_Link_Page=So2_Website_Deep_Link_Page,
																So2_Product_Display_Page=So2_Product_Display_Page,
																So2_Personalised_Smart_Basket_Recommendation_Page=So2_Personalised_Smart_Basket_Recommendation_Page,
																So2_Dynamic_Landing_Page=So2_Dynamic_Landing_Page,
																Comm_Cool_Off_Type=Comm_Cool_Off																

														Where 	Event_Id=@Event_Id;
														
														update Event_Master_UI A,Event_Master B
															Set EventSegmentName=CLMSegmentName,
																	Message=Creative1
															where A.Event_Id=B.Id and Event_Id=@Event_Id;
														
														If Comm_Cool_Off ="IGNORE_COOL_OFF"
												           Then 
																SET @sql_stmt1=concat('Update CLM_Creative A,CLM_Campaign_Events B
													            Set Communication_Cooloff = 0
													            where A.CreativeId =B.CreativeId and B.EventId =',@Event_Id,'; ');
																SELECT @sql_stmt1;
																PREPARE statement from @sql_stmt1;
																Execute statement;
																Deallocate PREPARE statement;
																
																SET @sql_stmt2=concat('Update CLM_Campaign_Trigger A,CLM_Campaign_Events B
													            Set TriggerUsesWaterfall = 99
													            where A.CampTriggerId =B.CampTriggerId and B.EventId =',@Event_Id,'; ');
																SELECT @sql_stmt2;
																PREPARE statement from @sql_stmt2;
																Execute statement;
																Deallocate PREPARE statement;
																

													     End If;	-- End If Ignore_Cool_Off is Yes or not 
														 
														 Set @ChannelCoolOff='';
														IF Channel ="SMS"
														THEN Select ifnull(Value,0) into @ChannelCoolOff from M_Config where Name='SMS_Communication_CoolOff_Days';
														END IF;
														
														IF Channel ="Email"
														THEN Select ifnull(Value,0) into @ChannelCoolOff from M_Config where Name='Email_Communication_CoolOff_Days';
														END IF;
														
														IF Channel ="WhatsApp"
														THEN Select ifnull(Value,0) into @ChannelCoolOff from M_Config where Name='WhatsApp_Communication_CoolOff_Days';
														END IF;
														
														IF Channel ="PN"
														THEN Select ifnull(Value,0) into @ChannelCoolOff from M_Config where Name='PN_Communication_CoolOff_Days';
														END IF;	
														Select ifnull(Value,0) into  @GlobalCoolOff  from M_Config where Name='Communication_CoolOff_Days';
														
														If Comm_Cool_Off ="NORMAL_COOL_OFF" 
												           Then  	
																SET @sql_stmt=concat('Update CLM_Creative A,CLM_Campaign_Events B
													            Set Communication_Cooloff = Greatest(',@GlobalCoolOff,',',@ChannelCoolOff,') 
													            where A.CreativeId =B.CreativeId and B.EventId =',@Event_Id,';');
																SELECT @sql_stmt;
																PREPARE statement from @sql_stmt;
																Execute statement;
																Deallocate PREPARE statement;
																
																SET @sql_stmt3=concat('Update CLM_Campaign_Trigger A,CLM_Campaign_Events B
													            Set TriggerUsesWaterfall = -1
													            where A.CampTriggerId =B.CampTriggerId and B.EventId =',@Event_Id,';');
																SELECT @sql_stmt3;
																PREPARE statement from @sql_stmt3;
																Execute statement;
																Deallocate PREPARE statement; 
						
																
														End If;	-- End If Normal_Cool_Off is Yes or not 
														
														If Comm_Cool_Off ="CAMPAIGN_COOL_OFF"
												           Then 
																SET @sql_stmt4=concat('Update CLM_Creative A,CLM_Campaign_Events B
													            Set Communication_Cooloff = ',Cool_Off,'
													            where A.CreativeId =B.CreativeId and B.EventId =',@Event_Id,';');
																SELECT @sql_stmt4;
																PREPARE statement from @sql_stmt4;
																Execute statement;
																Deallocate PREPARE statement;
																
																
																
																SET @sql_stmt5=concat('Update CLM_Campaign_Trigger A,CLM_Campaign_Events B
													            Set TriggerUsesWaterfall = -1
													            where A.CampTriggerId =B.CampTriggerId and B.EventId =',@Event_Id,';');
																SELECT @sql_stmt5;
																PREPARE statement from @sql_stmt5;
																Execute statement;
																Deallocate PREPARE statement;
						
						
														End If;	-- End If Normal_Cool_Off is Yes or not 
														 
														-- Updating the Execution_Sequence

														If Comm_Priority ="HIGH_PRIORITY" or Comm_Priority ="ONDEMAND" or Comm_Priority ="HIGHEST_PRIORITY" 
												           Then Update CLM_Campaign_Events 
													            Set Execution_Sequence =Execution_Sequence+1;
																
																
														
																SET @sql_stmt6=concat('Update CLM_Campaign_Events 
													             Set Execution_Sequence =1 
													            where EventId  =',@Event_Id,';');
																SELECT @sql_stmt6;
																PREPARE statement from @sql_stmt6;
																Execute statement;
																Deallocate PREPARE statement;
																
											            End If;	 	-- End If TOP_PRIORITY is Yes or not
                                                        
                                                        
                                                        
                                                        If Comm_Priority ="PERFORMANCE_BASED" 
												           Then 
																SET @sql_stmt8=concat('Update CLM_Campaign_Trigger A,CLM_Campaign_Events B
													            Set TriggerUsesWaterfall =100
													            where A.CampTriggerId =B.CampTriggerId and B.EventId=',@Event_Id,';');
																SELECT @sql_stmt8;
																PREPARE statement from @sql_stmt8;
																Execute statement;
																Deallocate PREPARE statement;

											            End If;

														If Comm_Priority ="LOW_PRIORITY" 
												           Then 
																SET @sql_stmt7=concat('Update CLM_Campaign_Events 
													             Set Execution_Sequence = (SELECT MAX(Execution_Sequence)+1 FROM CLM_Campaign_Events)
													            where EventId  =',@Event_Id,';');
																SELECT @sql_stmt7;
																PREPARE statement from @sql_stmt7;
																Execute statement;
																Deallocate PREPARE statement;
																

											            End If;	 	-- End If TOP_PRIORITY is Yes or not 

														 
													     If Reminder_Parent_Trigger_Id IS NOT NULL OR Reminder_Parent_Trigger_Id!=''
												           Then 
																SET @sql_stmt10=concat('Update CLM_Campaign_Trigger A,CLM_Campaign_Events B
													            Set TriggerUsesWaterfall = (Select TriggerUsesWaterfall from CLM_Campaign_Events A,CLM_Campaign_Trigger B
																							where A.CampTriggerId=B.CampTriggerID AND A.EventId =',Reminder_Parent_Trigger_Id,')
													            where A.CampTriggerId =B.CampTriggerId and B.EventId=',@Event_Id,';');
																SELECT @sql_stmt10;
																PREPARE statement from @sql_stmt10;
																Execute statement;
																Deallocate PREPARE statement;
																
													     End If;

														 
														 
														  If Attribution ="" or Attribution is null
												           Then 	Set @Event_Attribution='';
																Select Value into @Event_Attribution from  M_Config	where Name="Response_Days";
																
																SET @sql_stmt11=concat('
																Update CLM_Campaign_Trigger A,CLM_Campaign_Events B
													            Set TriggerResponseDays ="',@Event_Attribution,'"
													            where A.CampTriggerId =B.CampTriggerId and B.EventId =',@Event_Id,';');
																SELECT @sql_stmt11;
																PREPARE statement from @sql_stmt11;
																Execute statement;
																Deallocate PREPARE statement;
																
													     End If;	-- End If Attribution is Yes or not 
															
														 
														 If Attribution != ""
												           Then 
																SET @sql_stmt12=concat('Update CLM_Campaign_Trigger A,CLM_Campaign_Events B
													            Set TriggerResponseDays ="',Attribution,'"
													            where A.CampTriggerId =B.CampTriggerId
																		and B.EventId =',@Event_Id,';');
																		
																SELECT @sql_stmt12;
																PREPARE statement from @sql_stmt12;
																Execute statement;
																Deallocate PREPARE statement;
													     End If;	
														 
														 If Region =""
												           Then 
														   
																SET @sql_stmt13=concat('Update CLM_Campaign_Events 
													            Set RegionSegmentId =(Select RegionSegmentId from CLM_RegionSegment where RegionSegmentName="NONE")
													            where EventId =',@Event_Id,';');
																		
																SELECT @sql_stmt13;
																PREPARE statement from @sql_stmt13;
																Execute statement;
																Deallocate PREPARE statement;

													     End If;	-- End If Region

														 If Region !=""
												           Then 	
																SET @sql_stmt14=concat('Update CLM_Campaign_Events Set RegionSegmentId ="',Region,'"
													            where EventId  =',@Event_Id,';');
																		
																SELECT @sql_stmt14;
																PREPARE statement from @sql_stmt14;
																Execute statement;
																Deallocate PREPARE statement;
													     End If;	-- End If Region
														 
														
																SET @sql_stmt14=concat('Update CLM_Campaign_Events Set Campaign_SegmentId ="',Campaign_Segment,'"
													            where EventId  =',@Event_Id,';');
																		
																SELECT @sql_stmt14;
																PREPARE statement from @sql_stmt14;
																Execute statement;
																Deallocate PREPARE statement;
                                                                
													    	-- End If Region

											            If Execution_After !="" and Comm_Priority ="MANUALLY_DEFINED" 
												           Then 
																SET @sql_stmt15a=concat('Select Execution_Sequence into @Execution_After_Sequence From CLM_Campaign_Events where EventId=',Execution_After,';');
																		
																SELECT @sql_stmt15a;
																PREPARE statement from @sql_stmt15a;
																Execute statement;
																Deallocate PREPARE statement;
																
																SET @sql_stmt15b=concat('Update CLM_Campaign_Events
																Set Execution_Sequence =Execution_Sequence+2 
																where Execution_Sequence >= ',@Execution_After_Sequence,';');
																		
																SELECT @sql_stmt15b;
																PREPARE statement from @sql_stmt15b;
																Execute statement;
																Deallocate PREPARE statement;
																
																SET @sql_stmt15c=concat('Update CLM_Campaign_Events
																Set Execution_Sequence =',@Execution_After_Sequence,'+1
																where  EventId=',@Event_Id,';');
																		
																SELECT @sql_stmt15c;
																PREPARE statement from @sql_stmt15c;
																Execute statement;
																Deallocate PREPARE statement;
																
																SET @sql_stmt15d=concat('Update CLM_Campaign_Events
																Set Execution_Sequence =',@Execution_After_Sequence,'
																where  EventId=',Execution_After,';');
																		
																SELECT @sql_stmt15d;
																PREPARE statement from @sql_stmt15d;
																Execute statement;
																Deallocate PREPARE statement;
																
																
																
																
																/*
																SET @sql_stmt15=concat('Update CLM_Campaign_Events 
													            Set Execution_Sequence = (Select Execution_Sequence From CLM_Campaign_Events where EventId=',Execution_After,')
													            where EventId  =',@Event_Id,';');
																		
																SELECT @sql_stmt15;
																PREPARE statement from @sql_stmt15;
																Execute statement;
																Deallocate PREPARE statement;
																
																*/
											            End If;	-- End If Execution_After is Yes or not 
														
														If Microsite_URL is null then Set Microsite_URL=''; end if;
														
														SET @sql_stmt16=concat('Update CLM_Creative A,CLM_Campaign_Events B
														Set A.Website_URL="',Microsite_URL,'"
														where A.CreativeId=B.CreativeId
														and EventId =',@Event_Id,';');
																		
														SELECT @sql_stmt16;
														PREPARE statement from @sql_stmt16;
														Execute statement;
														Deallocate PREPARE statement;
														
														SET @sql_stmt17=concat('Update CLM_Campaign_Trigger A,CLM_Campaign_Events B
														Set CTReplacedQueryAsInUI=CTReplacedQuery,
														CTReplacedQueryBillAsInUI=CTReplacedQueryBill
														where A.CampTriggerId =B.CampTriggerId and B.EventId =',@Event_Id,';');
																		
														SELECT @sql_stmt17;
														PREPARE statement from @sql_stmt17;
														Execute statement;
														Deallocate PREPARE statement;
                                                        
                                                        
                                                        /*
					
														SET Event_Start_Date=CURDATE();
														SET Event_End_Date=DATE_ADD(CURDATE(),INTERVAL 100000 DAY);
                                                        
                                                        */
														
														SET @sql_stmt18=concat('Update CLM_Campaign_Trigger A,CLM_Campaign_Events B
														Set TriggerStartDate="',Event_Start_Date,'",
														TriggerEndDate="',Event_End_Date,'"
														where A.CampTriggerId =B.CampTriggerId and B.EventId=',@Event_Id,';');
																		
														SELECT @sql_stmt18;
														PREPARE statement from @sql_stmt18;
														Execute statement;
														Deallocate PREPARE statement;
														
														
														If Predictive_Score is null then Set Predictive_Score=''; end if;
														
														If Mcore_Module != '-1' AND Predictive_Score != '-1' 
														THEN
														
														If Predictive_Score is null then Set Predictive_Score=''; end if;
														
														SET @sql_stmt19=concat('Update CLM_Campaign_Trigger A,CLM_Campaign_Events B
														Set PropensityTopN_Percentile="',Predictive_Score,'"
														where A.CampTriggerId =B.CampTriggerId and B.EventId=',@Event_Id,';');
																		
														SELECT @sql_stmt19;
														PREPARE statement from @sql_stmt19;
														Execute statement;
														Deallocate PREPARE statement;
														
														If Mcore_Module is null then Set Mcore_Module=''; end if;
														
														SET @sql_stmt20=concat('Update CLM_Campaign_Trigger A,CLM_Campaign_Events B
														Set PropensityModel_Id="',Mcore_Module,'"
														where A.CampTriggerId =B.CampTriggerId and B.EventId=',@Event_Id,';');
																		
														SELECT @sql_stmt20;
														PREPARE statement from @sql_stmt20;
														Execute statement;
														Deallocate PREPARE statement;
														
														SET @sql_stmt21=concat('Update CLM_Campaign_Trigger A,CLM_Campaign_Events B
														Set Event_Limit="-1",
														CTReplacedQuery="1=1",
														CTReplacedQueryAsInUI="1=1",
														CTReplacedQueryBill="",
														CTReplacedQueryBill=""
														
														where A.CampTriggerId =B.CampTriggerId and B.EventId=',@Event_Id,';');
																		
														SELECT @sql_stmt21;
														PREPARE statement from @sql_stmt21;
														Execute statement;
														Deallocate PREPARE statement;
                                                        
                                                        
                                                        SET @sql_stmt8=concat('Update CLM_Campaign_Trigger A,CLM_Campaign_Events B
													            Set TriggerUsesWaterfall =100
													            where A.CampTriggerId =B.CampTriggerId and B.EventId=',@Event_Id,';');
																SELECT @sql_stmt8;
																PREPARE statement from @sql_stmt8;
																Execute statement;
																Deallocate PREPARE statement;
                                                        
                                                        
                                                        
														
														ELSE
															SET @sql_stmt19=concat('Update CLM_Campaign_Trigger A,CLM_Campaign_Events B
															Set PropensityTopN_Percentile="-1"
															where A.CampTriggerId =B.CampTriggerId and B.EventId=',@Event_Id,';');
																			
															SELECT @sql_stmt19;
															PREPARE statement from @sql_stmt19;
															Execute statement;
															Deallocate PREPARE statement;
															
															SET @sql_stmt20=concat('Update CLM_Campaign_Trigger A,CLM_Campaign_Events B
														Set PropensityModel_Id= Null
														where A.CampTriggerId =B.CampTriggerId and B.EventId=',@Event_Id,';');
																		
														SELECT @sql_stmt20;
														PREPARE statement from @sql_stmt20;
														Execute statement;
														Deallocate PREPARE statement;
														
																												
    												    END IF;
														
														If In_Control_Group="YES" then Set In_Control_Group="Y";  else  Set In_Control_Group="N"; end if;
														
														
														SET @sql_stmt22=concat('Update CLM_Campaign_Trigger A,CLM_Campaign_Events B
														Set TriggerUsesControlGroup="',In_Control_Group,'"
														
														where A.CampTriggerId =B.CampTriggerId and B.EventId=',@Event_Id,';');
																		
														SELECT @sql_stmt22;
														PREPARE statement from @sql_stmt22;
														Execute statement;
														Deallocate PREPARE statement;
													
														
														If Coverage <0 or Coverage is NULL or  Coverage=''
															THEN
															
																SET @SQL1= concat('Update Event_Master_UI 
																					   set coverage ="-1"
																					   where Event_Id= ',@Event_Id,' ');
																	
																SELECT @SQL1;
																PREPARE stmt1 from @SQL1;
																EXECUTE stmt1;
																DEALLOCATE PREPARE stmt1;
														
																	
																
															ELSE 
																	SET @SQL1= concat('Update Event_Master_UI 
																					   set coverage ="',Coverage,'"
																					   where Event_Id= ',@Event_Id,' ');
																	
																SELECT @SQL1;
																PREPARE stmt1 from @SQL1;
																EXECUTE stmt1;
																DEALLOCATE PREPARE stmt1;
														
														END IF;

														
														Select @Event_Id
															
														into @Event_Message;
														
														Select @Event_Message	;
														
												        Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message",@Event_Message,"Event_Status","Event_Created") )),"]") into @temp_result                       
																							  ;') ;
											   
														SELECT @Query_String;
														PREPARE statement from @Query_String;
														Execute statement;
														Deallocate PREPARE statement; 		


											   End if; 	-- End If vSuccess =1.


											    if @vSuccess !='1'
												Then
													Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message",@vErrMsg,"Event_Status","Event_Not_Created") )),"]") into @temp_result                       
																							  ;') ;
											   
													SELECT @Query_String;
													PREPARE statement from @Query_String;
													Execute statement;
													Deallocate PREPARE statement; 
																	
												End if; -- End If vSuccess !=1

								

							-- End If;	-- End If Trigger Name checking.		

		 Else     Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Invalid Parameters:-Please Check the RFMP Condition  and the Cool Off Days also and the Message Template","Event_Status","Event_Not_Created") )),"]") into @temp_result                       
		                                                                                  ;') ;
		                                   
		          SELECT @Query_String;
		          PREPARE statement from @Query_String;
		          Execute statement;
		          Deallocate PREPARE statement; 

		End If; -- End if of Rececny Check.




		if @temp_result is null
		Then
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Invalid Parameters") )),"]") into @temp_result ;') ;
			   	SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 
									
		End if;
									
		SET out_result_json = @temp_result;

END$$
DELIMITER ;
