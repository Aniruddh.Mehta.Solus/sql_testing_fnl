
CREATE OR REPLACE PROCEDURE `S_Generate_Comm_Template`(IN v_pfields VARCHAR(512), 
IN sms_flag char(1), 
IN pn_flag char(1), 
IN email_flag char(1), 
OUT Comm_Header VARCHAR(512), 
OUT Comm_Template VARCHAR (512)
)
BEGIN
DECLARE  v_Del varchar(12) default ',';

DECLARE  v_emailStr varchar(64) default '"{"Email":[';
DECLARE  v_smsStr varchar(64) default '],"SMS":[';
DECLARE  v_pnStr varchar(64) default '],"PN":[';
DECLARE  v_closingStr varchar(64) default ']}"';
DECLARE  v_sms_header varchar(512) DEFAULT '';
DECLARE  v_pn_header varchar(512) DEFAULT '';
DECLARE  v_email_header varchar(512) DEFAULT '';
DECLARE  v_HeaderValues varchar(512) DEFAULT '';

DECLARE  v_emailstart varchar(64) default '""<Email>';
DECLARE  v_emailend varchar(64) default '</Email>';
DECLARE  v_smsstart varchar(64) default '<SMS>';
DECLARE  v_smsend varchar(64) default '</SMS>';
DECLARE  v_pnstart varchar(64) default '<PN>';
DECLARE  v_pnend varchar(64) default '</PN>""';
DECLARE  v_sms_template varchar(512) DEFAULT '';
DECLARE  v_pn_template varchar(512) DEFAULT '';
DECLARE  v_email_template varchar(512) DEFAULT '';
DECLARE  v_TemplateValues varchar(512) DEFAULT '';


SELECT 
    CONCAT('\'',
            GROUP_CONCAT(item
                SEPARATOR '","'),
            '\'')
INTO v_HeaderValues FROM
    (SELECT 
        position,
            TRIM(SUBSTRING_INDEX(SUBSTRING_INDEX(v_pfields, v_Del, position + 1), v_Del, - 1)) AS `item`
    FROM
        (SELECT 
        n AS `position`
    FROM
        (SELECT 0 AS `n` UNION ALL SELECT 1 AS `1` UNION ALL SELECT 2 AS `2` UNION ALL SELECT 3 AS `3` UNION ALL SELECT 4 AS `4` UNION ALL SELECT 5 AS `5` UNION ALL SELECT 6 AS `6` UNION ALL SELECT 6 AS `7` UNION ALL SELECT 6 AS `8` UNION ALL SELECT 6 AS `9` UNION ALL SELECT 6 AS `10` UNION ALL SELECT 6 AS `11` UNION ALL SELECT 6 AS `12` UNION ALL SELECT 6 AS `13` UNION ALL SELECT 6 AS `14` UNION ALL SELECT 6 AS `15` UNION ALL SELECT 6 AS `16`) vw) tallyGenerator
    WHERE
        position <= (CHAR_LENGTH(v_pfields) - CHAR_LENGTH(REPLACE(v_pfields, v_Del, '')))) delimitedString;

if sms_flag = 'Y' then
SET  v_sms_header = v_HeaderValues;
end if;
if pn_flag = 'Y' then
SET  v_pn_header = v_HeaderValues;
end if ;
if email_flag = 'Y' then
SET v_email_header  = v_HeaderValues;
END IF;
SELECT CONCAT(v_emailStr, v_email_header ,v_smsStr, v_sms_header,v_pnStr, v_pn_header, v_closingStr) INTO Comm_Header;
select Comm_Header;


SELECT 
    GROUP_CONCAT(CONCAT('"|ifnull(', item, ',"")|"')
        SEPARATOR ',')
INTO v_TemplateValues FROM
    (SELECT 
        position,
            TRIM(SUBSTRING_INDEX(SUBSTRING_INDEX(v_pfields, v_Del, position + 1), v_Del, - 1)) AS `item`
    FROM
        (SELECT 
        n AS `position`
    FROM
        (SELECT 0 AS `n` UNION ALL SELECT 1 AS `1` UNION ALL SELECT 2 AS `2` UNION ALL SELECT 3 AS `3` UNION ALL SELECT 4 AS `4` UNION ALL SELECT 5 AS `5` UNION ALL SELECT 6 AS `6` UNION ALL SELECT 6 AS `7` UNION ALL SELECT 6 AS `8` UNION ALL SELECT 6 AS `9` UNION ALL SELECT 6 AS `10` UNION ALL SELECT 6 AS `11` UNION ALL SELECT 6 AS `12` UNION ALL SELECT 6 AS `13` UNION ALL SELECT 6 AS `14` UNION ALL SELECT 6 AS `15` UNION ALL SELECT 6 AS `16`) vw) tallyGenerator
    WHERE
        position <= (CHAR_LENGTH(v_pfields) - CHAR_LENGTH(REPLACE(v_pfields, v_Del, '')))) delimitedString;

if sms_flag = 'Y' then
SET  v_sms_template = v_TemplateValues;
end if;

if pn_flag = 'Y' then
SET  v_pn_template = v_TemplateValues;
end if ;

if email_flag = 'Y' then
SET v_email_template  = v_TemplateValues;
END IF;

SET Comm_Template=   CONCAT( v_emailstart,
             v_email_template,
             v_emailend,
             v_smsstart,
             v_sms_template,
            v_smsend,
             v_pnstart,
             v_pn_template,
             v_pnend);
END

