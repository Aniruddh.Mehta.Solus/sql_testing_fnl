
CREATE OR REPLACE PROCEDURE `S_CLM_Upload_Blueprint`( OUT vSuccess TINYINT, OUT vErrMsg TEXT)
begin
    DECLARE done1,done2 INT DEFAULT FALSE;
    DECLARE vOfferId BIGINT;
	DECLARE vCreativeId BIGINT;
	
    DECLARE vRecordId BIGINT DEFAULT 1;
    DECLARE LIFE_CYCLE_SEGMENT_curEvents text;
	DECLARE CAMPAIGN_THEME_curEvents text;
	DECLARE CAMPAIGN_NAME_curEvents TEXT;
	DECLARE TRIGGER_NAME_curEvents TEXT;
	DECLARE TRIGGER_STATE_curEvents TINYINT;
	DECLARE CLM_SEGMENT_ID_curEvents TINYINT;
	DECLARE CLM_SEGMENT_CONDITION_curEvents TEXT;
	DECLARE CAMPAIGN_THEME_ID_curEvents BIGINT;
	DECLARE CAMPAIGN_ID_curEvents BIGINT;
	DECLARE TRIGGER_ID_curEvents BIGINT;
	DECLARE CAMPAIGN_THEME_EXE_SEQ_curEvents BIGINT;
	DECLARE CAMPAIGN_EXE_SEQ_curEvents BIGINT;
	DECLARE TRIGGER_EXE_SEQ_curEvents BIGINT;
	DECLARE TRIGGER_DESCRIPTION_curEvents TEXT;
	DECLARE USES_GOOD_TIME_MODEL_curEvents TINYINT;
	DECLARE TRIGGER_REPLACED_QUERY_curEvents TEXT;
	DECLARE TRIGGER_REPLACED_QUERY_BILL_curEvents TEXT;
    DECLARE NUMBER_OF_RECOS_curEvents BIGINT;
    DECLARE PERSONALISED_FIELD_curEvents TEXT;
    DECLARE PESONALISED_FIELD_HEADER_curEvents TEXT;
    DECLARE EVENT_LIMIT_curEvents BIGINT;
    DECLARE START_DATE_curEvents TEXT;
    DECLARE END_DATE_curEvents TEXT;
    DECLARE ALGO_CODE_curEvents BIGINT;
    
    
    
    
DECLARE curEvents cursor for
SELECT 
    LIFE_CYCLE_SEGMENT,
    CLM_SEGMENT_ID,
    CAMPAIGN_THEME,
    CAMPAIGN_THEME_ID,
    CAMPAIGN_NAME,
    CAMPAIGN_ID,
    TRIGGER_NAME,
    TRIGGER_ID,
    CLM_SEGMENT_CONDITION,
    TRIGGER_EXE_SEQ,
    USES_GOOD_TIME_MODEL,
    TRIGGER_REPLACED_QUERY,
    TRIGGER_REPLACED_QUERY_BILL,
    NUMBER_OF_RECOS,
    PERSONALISED_FIELD,
    PESONALISED_FIELD_HEADER,
    EVENT_LIMIT,
    START_DATE,
    END_DATE,
    ALGO_CODE
FROM
    Blueprint_csv;
	
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done1 = TRUE;
SET done1 = FALSE;
select done1;
SET SQL_SAFE_UPDATES=0;
set foreign_key_checks=0;
SET vRecordId = 1;
CALL PROC_CLM_CU_CLM_Channel(1,'SMS','SMS','|','csv','Y','done',vRecordId,vErrMsg,vSuccess);
SET vRecordId = 2;
CALL PROC_CLM_CU_CLM_Channel(1,'EMAIL','EMAIL','|','csv','Y','done',vRecordId,vErrMsg,vSuccess);
SET vRecordId = 3;
CALL PROC_CLM_CU_CLM_Channel(1,'PN','Push Notification','|','csv','Y','done',vRecordId,vErrMsg,vSuccess);
SET vRecordId = 4;
CALL PROC_CLM_CU_CLM_Channel(1,'WSN','Website Notification','|','csv','Y','done',vRecordId,vErrMsg,vSuccess);
SET vRecordId = 5;
CALL PROC_CLM_CU_CLM_Channel(1,'WEBSITE','Website Personalization','|','csv','Y','done',vRecordId,vErrMsg,vSuccess);
SET vRecordId = 6;
CALL PROC_CLM_CU_CLM_Channel(1,'APP','App Personalization','|','csv','Y','done',vRecordId,vErrMsg,vSuccess);
SET vRecordId = 1;
CALL PROC_CLM_CU_CLM_MicroSegment(1,'NONE','Same message template is used for everyone.', '', '', 1, '', '', 1, vRecordId,vErrMsg,vSuccess);
SET vRecordId = 1;
CALL PROC_CLM_CU_CLM_Offer(1,'CUSTOMER_LEVEL_OFFER','Customer level unique offer',1,'2021-01-15','2040-01-15','','',1,vRecordId,vErrMsg,vSuccess);
SET vRecordId = 2;
CALL PROC_CLM_CU_CLM_Offer(1,'GOLBAL_OFFER','GOLBAL_OFFER',2,'2021-01-15','2040-01-15','','0',2,vRecordId,vErrMsg,vSuccess);
SET vRecordId = 3;
CALL PROC_CLM_CU_CLM_Offer(1,'NO_OFFER','NO_OFFER',3,'2021-01-15','2040-01-15','','0',3,vRecordId,vErrMsg,vSuccess);
SET vRecordId = 4;
CALL PROC_CLM_CU_CLM_Offer(1,'Reminder_Offer','Reminder_Offer',4,'2021-01-15','2040-01-15','','0',4,vRecordId,vErrMsg,vSuccess);
INSERT IGNORE INTO `CLM_OfferType` (`OfferTypeName`) VALUES ('CUSTOMER_LEVEL_OFFER');
INSERT IGNORE INTO `CLM_OfferType` (`OfferTypeName`) VALUES ('GOLBAL_OFFER');
INSERT IGNORE INTO `CLM_OfferType` (`OfferTypeName`) VALUES ('NO_OFFER');
INSERT IGNORE INTO `CLM_OfferType` (`OfferTypeName`) VALUES ('Reminder_Offer');
set sql_safe_updates=0;
Update CLM_Offer A,CLM_OfferType B
set A.OfferTypeId=B.OfferTypeId
where A.OfferName=B.OfferTypeName;
SET vRecordId = 1;
	
OPEN curEvents;
LOOP_EVENTS: LOOP
		FETCH curEvents into LIFE_CYCLE_SEGMENT_curEvents,CLM_SEGMENT_ID_curEvents,CAMPAIGN_THEME_curEvents,CAMPAIGN_THEME_ID_curEvents,CAMPAIGN_NAME_curEvents,CAMPAIGN_ID_curEvents,TRIGGER_NAME_curEvents,TRIGGER_ID_curEvents,CLM_SEGMENT_CONDITION_curEvents,TRIGGER_EXE_SEQ_curEvents,USES_GOOD_TIME_MODEL_curEvents,TRIGGER_REPLACED_QUERY_curEvents,TRIGGER_REPLACED_QUERY_BILL_curEvents,NUMBER_OF_RECOS_curEvents,PERSONALISED_FIELD_curEvents,PESONALISED_FIELD_HEADER_curEvents,EVENT_LIMIT_curEvents,START_DATE_curEvents,END_DATE_curEvents,ALGO_CODE_curEvents;
        IF done1 THEN
			 LEAVE LOOP_EVENTS;
		END IF;
        
        select LIFE_CYCLE_SEGMENT_curEvents,CLM_SEGMENT_ID_curEvents,CAMPAIGN_THEME_curEvents,CAMPAIGN_THEME_ID_curEvents,CAMPAIGN_NAME_curEvents,CAMPAIGN_ID_curEvents,TRIGGER_NAME_curEvents,TRIGGER_ID_curEvents,CLM_SEGMENT_CONDITION_curEvents,TRIGGER_EXE_SEQ_curEvents,USES_GOOD_TIME_MODEL_curEvents,TRIGGER_REPLACED_QUERY_curEvents,TRIGGER_REPLACED_QUERY_BILL_curEvents,NUMBER_OF_RECOS_curEvents,PERSONALISED_FIELD_curEvents,PESONALISED_FIELD_HEADER_curEvents,EVENT_LIMIT_curEvents,START_DATE_curEvents,END_DATE_curEvents;
        select -1 into @vEventId;
        CALL PROC_CLM_CU_CLM_Segment(1,LIFE_CYCLE_SEGMENT_curEvents,'',CLM_SEGMENT_CONDITION_curEvents,CLM_SEGMENT_CONDITION_curEvents,1,CLM_SEGMENT_ID_curEvents,vErrMsg,vSuccess);
        CALL PROC_CLM_CU_CLM_CampaignTheme(1,CAMPAIGN_THEME_curEvents,CAMPAIGN_THEME_curEvents,1,CAMPAIGN_THEME_ID_curEvents,vErrMsg,vSuccess);
        CALL PROC_CLM_CU_CLM_Campaign(1,CAMPAIGN_NAME_curEvents,CAMPAIGN_NAME_curEvents,CAMPAIGN_THEME_ID_curEvents,CLM_SEGMENT_ID_curEvents,0.05,7,1,0,0,1,1,START_DATE_curEvents,END_DATE_curEvents,1,CAMPAIGN_ID_curEvents,vErrMsg,vSuccess);
        CALL PROC_UI_CLM_S_Generate_Comm_Template(PERSONALISED_FIELD_curEvents,'Y','','',@Comm_Template,@Comm_Header,vSuccess,vErrMsg);
        CALL PROC_UI_Create_Event(1,CAMPAIGN_THEME_ID_curEvents,CLM_SEGMENT_ID_curEvents,CAMPAIGN_ID_curEvents,1,1,5,'NO_OFFER','','',current_date(),date_add(current_date(),interval 365 day),0,-1,TRIGGER_NAME_curEvents,'','',-1,-1,1,1,-1,TRIGGER_REPLACED_QUERY_curEvents,TRIGGER_REPLACED_QUERY_curEvents,1,TRIGGER_REPLACED_QUERY_BILL_curEvents,TRIGGER_REPLACED_QUERY_BILL_curEvents,1,USES_GOOD_TIME_MODEL_curEvents,TRIGGER_NAME_curEvents,vRecordId,'Sender',-1,'',@Comm_Template,@Comm_Header,'','','','','','',ALGO_CODE_curEvents,-1,-1,-1,-1,-1,NUMBER_OF_RECOS_curEvents,'','',-1,-1,-1,-1,EVENT_LIMIT_curEvents,0,TRIGGER_ID_curEvents,@vOfferId,vRecordId,@vEventId,vErrMsg,vSuccess);
        
		SET vRecordId = vRecordId+1;  
    
end LOOP LOOP_EVENTS;
CLOSE curEvents;


set sql_safe_updates=0;
Update CLM_Offer A,CLM_OfferType B
set A.OfferTypeId=B.OfferTypeId
where A.OfferName=B.OfferTypeName;

END

