
CREATE OR REPLACE PROCEDURE `S_CLM_Customer_Segmentation_Refresh`(IN SegName Varchar(128),IN SegType Varchar(128))
BEGIN

DECLARE vStart_Cnt BIGINT DEFAULT 0;
DECLARE vEnd_Cnt BIGINT;
DECLARE vBatchSize BIGINT DEFAULT 100000;
DECLARE vStart_Cnt_loop BIGINT DEFAULT 0;
DECLARE vEnd_Cnt_loop BIGINT;
set @COL_NAME = SegName;
set @Seg_Type = SegType;
set  @Is_Valid = '';



select Customer_Id into @max_Cust from CDM_Customer_Master order by Customer_id desc limit 1;
SELECT Segment_Id INTO @Is_Valid FROM Solus_Segment where Segment_Name = @COL_NAME and Segment_Type = @Seg_Type limit 1;
set sql_safe_updates = 0;


if @Is_Valid != ''
then
update Solus_Segment
set `status`="Refreshing",
LastRefreshDate=0,
Coverage=0,
progress=0
where Segment_Name = @COL_NAME and Segment_Type = @Seg_Type limit 1;

IF @Seg_Type = 'DASHBOARD' 
THEN 
	set @Col=concat('DB_',replace(@COL_NAME,' ',''));
ELSE
	set @Col=concat('CM_',replace(@COL_NAME,' ',''));
END IF;

set @query1=concat("alter table CLM_Customer_Segmentation add column if not exists ","`",@Col,"`"," int(10);");

PREPARE statement1 from @query1;
Execute statement1;
Deallocate PREPARE statement1;

select Segment_Query_Condition into @Repcondition from Solus_Segment where Segment_Name = @COL_NAME and Segment_Type = @Seg_Type limit 1;


set @Repcondition = replace(@Repcondition,"`Transacated On`","Bill_Date");
set @Repcondition = replace(@Repcondition,"`Point Balance`","Point_Balance");
set @Repcondition = replace(@Repcondition,"`Total Revenue`","Lt_Rev");
set @Repcondition = replace(@Repcondition,"`Visits in Active Period`","AP_Num_of_Visits");
set @Repcondition = replace(@Repcondition,"`Total Visits`","Lt_Num_of_Visits");
set @Repcondition = replace(@Repcondition,"`Number of Items in Cart`","Num_Of_Cart_Items");
set @Repcondition = replace(@Repcondition,"`Fav Day Of Week`","Lt_Fav_Day_Of_Week");
set @Repcondition = replace(@Repcondition,"ABV","Lt_ABV");
set @Repcondition = replace(@Repcondition,"`Category Purchased`","Cat_Name");
set @Repcondition = replace(@Repcondition,"`Favourite Category`","Fav_Cat");
set @Repcondition = replace(@Repcondition,"`Distinct Category Bought`","LT_Distinct_Cat_Cnt");
set @Repcondition = replace(@Repcondition,"`Distinct Products Bought`","LT_Distinct_Prod_Cnt");
set @Repcondition = replace(@Repcondition,"`Units per Transaction`","Lt_UPT");
set @Repcondition = replace(@Repcondition,"`Gross Margin value`","Lt_GMV");
set @Repcondition = replace(@Repcondition,"`Average Selling Price`","Lt_ASP");
          set @Repcondition = replace(@Repcondition,"equal"," IN ");
           set @Repcondition = replace(@Repcondition,"NOT >"," < ");
           set @Repcondition = replace(@Repcondition,"Avg"," ");
           set @Repcondition = replace(@Repcondition,"Max"," ");

SELECT Customer_Id1 INTO vStart_Cnt FROM CLM_Customer_Segmentation ORDER BY Customer_Id1 ASC LIMIT 1;
SELECT Customer_Id1 INTO @Rec_Cnt FROM CLM_Customer_Segmentation ORDER BY Customer_Id1 DESC LIMIT 1;

set @index1=concat("create index if not exists ","`",@Col,"`"," on CLM_Customer_Segmentation(","`",@Col,"`",")",";");

PREPARE statement4 from @index1;
Execute statement4;
Deallocate PREPARE statement4;


set sql_safe_updates=0;
set @update0=concat("update CLM_Customer_Segmentation a
set ","`",@Col,"`"," = 0;");

PREPARE statement3 from @update0;
Execute statement3;
Deallocate PREPARE statement3;


PROCESS_LOOP: LOOP
SET vEnd_Cnt = vStart_Cnt + vBatchSize;


if @Repcondition not RLIKE 'Bill_Date|Product_Name|Cat_Name|LT_Distinct_Cat_Cnt|LT_Distinct_Prod_Cnt'
			then
set @update1=concat("update CLM_Customer_Segmentation a, V_CLM_Customer_One_View b
set ","`",@Col,"`"," = 1
where a.Customer_Id1 = b.Customer_Id
and Customer_Id1 between ",vStart_Cnt," and ",vEnd_Cnt," 
and ",@Repcondition,";");

else

set @update1=concat("update CLM_Customer_Segmentation a, 
(SELECT 
y.*,
    Recency,
    Vintage,
    City,
    Point_Balance,
    Customer_Id,
    AP_Num_of_Visits,
    Lt_Num_of_Visits,
    Lt_Rev,
    Age,
    Num_Of_Cart_Items,
    Tenure,
Lt_UPT,
Lt_GMV,
Lt_ASP,
LT_Distinct_Prod_Cnt,
LT_Distinct_Cat_Cnt,
    (select Lov_Value from CDM_LOV_Master a where
    a.Lov_Id = LT_Fav_Cat_LOV_Id) as Fav_Cat,
    (select Lov_Value from CDM_LOV_Master a where
    a.Lov_Id = LT_Fav_Prod_LOV_Id) as Fav_Prod
FROM
    Customer_One_View x, Client_SVOC y
    where x.Customer_Id = y.Customer_Id_1) as b, 
 (select 
Customer_id, Bill_Date ,
(select Product_Comm_Name from CDM_Product_Master_Original a where a.Product_Id = CDM_Bill_Details.Product_Id) as Product_Name,
(select Lov_Value from CDM_LOV_Master c,CDM_Product_Master_Original d where c.Lov_Id = d.Cat_Lov_Id and d.Product_Id = CDM_Bill_Details.Product_Id) as Cat_Name
from CDM_Bill_Details) as c
set ","`",@Col,"`"," = 1
 where a.Customer_Id1 = b.Customer_Id and
 b.Customer_Id = c.Customer_id
and Customer_Id1 between ",vStart_Cnt," and ",vEnd_Cnt," 
and ",@Repcondition,";");

end if;


PREPARE statement3 from @update1;
Execute statement3;
Deallocate PREPARE statement3;

SET vStart_Cnt = vEnd_Cnt;

update Solus_Segment
set progress=round((vEnd_Cnt/@max_Cust)*100)
where Segment_Name = @COL_NAME and Segment_Type = @Seg_Type limit 1;

IF vStart_Cnt  >= @Rec_Cnt THEN

	LEAVE PROCESS_LOOP;



END IF;        
END LOOP PROCESS_LOOP; 

set @query1=concat('select count(1) into @Cov from CLM_Customer_Segmentation where ',@Col,' = 1;');
PREPARE statement3 from @query1;
Execute statement3;
Deallocate PREPARE statement3;
select @query1;


set @query2=concat('
update Solus_Segment
set `status`="Refresh",
LastRefreshDate = current_timestamp(),
Coverage = ',@Cov,' , 
progress=100
where Segment_Name = ','"',@COL_NAME,'"',' and Segment_Type = ','"',@Seg_Type,'"',' limit 1;');

select @query2;
PREPARE statement3 from @query2;
Execute statement3;
Deallocate PREPARE statement3;
else 
select "Invalid Segment";
end if;
 
 
 
END

