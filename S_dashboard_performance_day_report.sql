DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_performance_day_report`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
    declare IN_AGGREGATION_4 text;
    declare IN_AGGREGATION_5 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
    SET IN_AGGREGATION_4 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_4"));
    SET IN_AGGREGATION_5 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_5"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    
    IF IN_AGGREGATION_4 = ''
	THEN
		SET @wherecond1 = ' 1=1 ';
	ELSE
		SET @wherecond1 = CONCAT(' CLMSegmentName="',IN_AGGREGATION_4,'" ');
	END IF;
	IF IN_AGGREGATION_5 = ''
	THEN
		SET @wherecond2 = ' 1=1 ';
	ELSE
		SET @wherecond2 = CONCAT(' Channel="',IN_AGGREGATION_5,'" ');
	END IF;
    CREATE OR REPLACE VIEW V_Dashboard_CLM_Event_Month_Report AS
   select CASE WHEN datediff(End_Date_ID,Start_Date_ID) < 32 THEN 'GTM' else 'CLM' END AS `Category`,b.Name as `Trigger`, b.CampaignName as `CampaignName`,a.Segment AS `CLMSegmentName`, a.* from CLM_Event_Dashboard a join Event_Master b on a.Event_ID = b.ID;
   
   IF IN_AGGREGATION_2 = 'CG'
   THEN
		SET @cg_clause='100';
	ELSE
		SET @cg_clause='0';
	END IF;
		
		SET @select_query='Category,Theme,CampaignName AS `Campaign`,`Channel`,`Trigger`,`EE_Date` as `Date`';
        set @group_cond='1,2,3,4,5,6';
        set @json_select = '"Category",`Category`,"Theme",`Theme`,"Campaign",`Campaign`,"Channel",`Channel`,"Trigger",`Trigger`,"Date",`Date`';
        SET @attr = '`Trigger`';
        SET @cg_clause='0';

   SET @sql1= concat('CREATE OR REPLACE VIEW V_Dashboard_Month_Report_Temp
   as
   select Category,
   Theme,
   CampaignName,
   CLMSegmentName,
   `Channel`,
   `Trigger`,
   YM,
   Event_Id,
   EE_Date,
   SUM(`Target_Base`) AS Target_Base,
   SUM(`Target_Delivered`) AS Target_Delivered,
   SUM(`Control_Base`) AS Control_Base,
   SUM(`Target_Responder`) AS Target_Responder,
   SUM(`Control_Responder`) AS Control_Responder,
   SUM(Target_Revenue) AS Target_Revenue,
   MAX(Incremental_Revenue) AS Incremental_Revenue
   from V_Dashboard_CLM_Event_Month_Report
   where ',@wherecond1,' and ',@wherecond2,'
   and EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
   group by Event_Id,YM,EE_Date');
   
   select @sql1;
	PREPARE statement from @sql1;
	Execute statement;
	Deallocate PREPARE statement;
   
    SET @create_view=CONCAT('CREATE OR REPLACE VIEW V_Dashboard_Month_Report_Temp2
   as
   select Category,
   Theme,
   CampaignName,
   CLMSegmentName,
   `Channel`,
   `Trigger`,
   YM,
   Event_Id,
   EE_Date,
   SUM(`Target_Base`) AS Target_Base,
   SUM(`Target_Delivered`) AS Target_Delivered,
   SUM(`Control_Base`) AS Control_Base,
   SUM(`Target_Responder`) AS Target_Responder,
   SUM(`Control_Responder`) AS Control_Responder,
   SUM(Target_Revenue) AS Target_Revenue,
   MAX(Incremental_Revenue) AS Incremental_Revenue
   from V_Dashboard_CLM_Event_Month_Report
   where ',@wherecond1,' and ',@wherecond2,'
   and EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
   group by Event_Id,YM,EE_Date');
    select @create_view;
	PREPARE statement from @create_view;
	Execute statement;
	Deallocate PREPARE statement;
   
   IF IN_MEASURE = 'TABLE'
   THEN
   set @view_stmt= concat('CREATE OR REPLACE VIEW V_Dashboard_Month_Report
					AS
					(SELECT Category,Theme,CampaignName AS `Campaign`,`Channel`,
                    IFNULL(`Trigger`,"Total") as `Trigger`,
                    IFNULL(`EE_Date`,"") as `Date`,
					SUM(`Target_Delivered`) AS `TG Delivered`,
                    SUM(`Control_Base`) AS `CG Base`,
					SUM(`Target_Responder`) AS `TG Responders`,
                    SUM(`Control_Responder`) AS `CG Responders`,
					concat(round((CASE WHEN SUM(`Target_Delivered`) <=0 THEN 0 ELSE (SUM(`Target_Responder`)/SUM(`Target_Delivered`))*100 END),2),"%") AS `Target Response`,
					concat(round((CASE WHEN SUM(`Control_Base`) <=0 THEN 0 ELSE (SUM(`Control_Responder`)/SUM(`Control_Base`))*100 END),2),"%") AS `Control Response`,
                    round(CASE WHEN SUM(Target_Responder) <= 0 then 0 else
					SUM(Target_Revenue)/SUM(Target_Responder) END,0) AS `Average Spend`,
                    concat(round(CASE WHEN (CASE WHEN SUM(`Target_Delivered`) <=0 THEN 0 
                    ELSE (SUM(`Target_Responder`)/SUM(`Target_Delivered`))*100 END)-(CASE WHEN SUM(`Control_Base`) <=0 THEN 0 ELSE (SUM(`Control_Responder`)/SUM(`Control_Base`))*100 END)<0 THEN 0 ELSE
(CASE WHEN SUM(`Target_Delivered`) <=0 THEN 0 
                    ELSE (SUM(`Target_Responder`)/SUM(`Target_Delivered`))*100 END)-(CASE WHEN SUM(`Control_Base`) <=0 THEN 0 ELSE (SUM(`Control_Responder`)/SUM(`Control_Base`))*100 END)END,2),"%") AS `TG-CG Response`,
					round(SUM(Incremental_Revenue),0) AS `Rev Lift`
					from V_Dashboard_Month_Report_Temp
					 GROUP BY `Trigger`,`EE_Date` WITH ROLLUP
                     having SUM(Target_Base)>0 and SUM(Control_Base)>',@cg_clause,')');
                     
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object(',@json_select,',"TG Delivered",CAST(format(`TG Delivered`,",") AS CHAR),"CG Base",format(`CG Base`,","),"TG Responders",format(`TG Responders`,","),"CG Responders",format(`CG Responders`,","),"Target Response",`Target Response`,"Control Response",`Control Response`,"TG-CG Response",`TG-CG Response`,"Average Spend",format(`Average Spend`,","),"Rev Lift",format(`Rev Lift`,",")) )),"]")into @temp_result from V_Dashboard_Month_Report ',';') ;
	END IF;
           
	IF IN_MEASURE = 'CARD'
    THEN
		set @view_stmt = concat('CREATE OR REPLACE VIEW V_Dashboard_Month_Report_Card AS 
        select SUM(cnt) as `Count`, MAX(`# With Lift`) as "# With Lift",MAX(`Incremental Revenue`) as `Incremental Revenue` from
        (
        select count(distinct ',@attr,') as `cnt`,(select count(distinct ',@attr,') as "# With Lift" from (select ',@attr,', round(SUM(Incremental_Revenue),2) as Lift from V_Dashboard_Month_Report_Temp2 where EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'" group by ',@attr,' having SUM(`Control_Base`) > ',@cg_clause,')A where Lift>0) as "# With Lift",(select round(SUM(Incremental_Revenue),0) AS `Incremental_Revenue` from(select round(SUM(Incremental_Revenue),1) AS `Incremental_Revenue` from V_Dashboard_Month_Report_Temp2 where EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'" group by ',@attr,')B) AS `Incremental Revenue` from V_Dashboard_Month_Report_Temp2 where EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'" group by ',@attr,' having SUM(`Control_Base`) > ',@cg_clause,')A');
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Count",`Count`,"# With Lift",`# With Lift`,"Incremental Revenue",format(`Incremental Revenue`,",")) )),"]")into @temp_result from V_Dashboard_Month_Report_Card ',';') ;
		
	END IF;
   
	    
    select @view_stmt;
	PREPARE statement from @view_stmt;
	Execute statement;
	Deallocate PREPARE statement;
    
    IF IN_SCREEN <> "DOWNLOAD"
    THEN
		select @Query_String;
		PREPARE statement from @Query_String;
		Execute statement;
		Deallocate PREPARE statement; 
		SET out_result_json = @temp_result;
	ELSE
		call S_dashboard_performance_download('V_Dashboard_Month_Report',@out_result_json1);
		select @out_result_json1 into out_result_json;
    END IF;
    
END$$
DELIMITER ;
