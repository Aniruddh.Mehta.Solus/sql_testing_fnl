DELIMITER $$
CREATE or replace PROCEDURE `S_UI_CLM_Segment_Coverage_Update`()
Begin
	DECLARE vStart_Cnt BIGINT DEFAULT 0;
    DECLARE vEnd_Cnt BIGINT DEFAULT 0;
	
    DECLARE done1,done2,done3,done4 INT DEFAULT FALSE;
	
    DECLARE LifeCycleId_cur1,Exe_ID int;
    DECLARE LifeCycleReplacedQuery_cur1 TEXT;
	
	
    DECLARE vSuccess int (4);                      
	DECLARE vErrMsg Text ; 
    
	
	Declare cur1 cursor for
    select distinct CLMSegmentId,CLMSegmentReplacedQuery from CLM_Segment
    where CLMSegmentReplacedQuery <> '' 
    ORDER BY CLMSegmentId ASC;
	
	
		DECLARE CONTINUE HANDLER FOR NOT FOUND SET done1 = TRUE;
		
		 DECLARE CONTINUE HANDLER FOR SQLWARNING
		BEGIN
			GET DIAGNOSTICS CONDITION 1 @errsqlstate=RETURNED_SQLSTATE, @errno=MYSQL_ERRNO, @errtext = MESSAGE_TEXT;
			SET @logmsg=CONCAT(ifnull(@errsqlstate,'WARNING') , ifnull(@errno,'1'), ifnull(@errtext,'WARNING MESSAGE'));
			
			SELECT 
					'S_UI_CLM_Segment_Coverage_Update : Warning Message :' AS '',
					@logmsg AS '';
		END;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
		BEGIN
			GET DIAGNOSTICS CONDITION 1 @errsqlstate=RETURNED_SQLSTATE, @errno=MYSQL_ERRNO, @errtext = MESSAGE_TEXT;
			SET @logmsg=CONCAT(ifnull(@errsqlstate,'ERROR') , ifnull(@errno,'1'), ifnull(@errtext,'ERROR MESSAGE'));
			SET vErrMsg=@logmsg;
			SET vSuccess=0;
			SELECT 'S_UI_CLM_Segment_Coverage_Update : Error Message :' AS '', @logmsg AS '';
		END;
		SET done1 = FALSE;
	open cur1;
					
							
							new_loop_1: LOOP
									FETCH cur1 into LifeCycleId_cur1,LifeCycleReplacedQuery_cur1;
									IF done1 THEN
											 LEAVE new_loop_1;
									END IF;

									

									SELECT LifeCycleId_cur1,LifeCycleReplacedQuery_cur1;
									
									
								    Set @Sql1=CONCAT('Update CLM_Segment 
													Set Coverage = (Select Count(1) from V_CLM_Customer_One_View where ',LifeCycleReplacedQuery_cur1,')
											where CLMSegmentId=',LifeCycleId_cur1,';');
											
									   SELECT @Sql1;
										prepare stmt1 from @Sql1;
										execute stmt1;
										deallocate prepare stmt1;
								
							end LOOP new_loop_1;
						  
					
				 close cur1;
end$$
DELIMITER ;
