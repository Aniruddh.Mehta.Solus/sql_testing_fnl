DELIMITER $$
CREATE or replace PROCEDURE `S_UI_Campaign_Is_Valid_Template`(IN Comm_Template TEXT,IN Header TEXT,OUT vOutput TEXT)
Begin
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		GET DIAGNOSTICS CONDITION 1 @errsqlstate=RETURNED_SQLSTATE, @errno=MYSQL_ERRNO, @errtext = MESSAGE_TEXT;
		SET vOutput=CONCAT(ifnull(@errsqlstate,'ERROR') , ifnull(@errno,'1'), ifnull(@errtext,'ERROR MESSAGE'));
    END;
	
		
	Select Group_Concat(Distinct 
	Concat('Max(Case when Variable_Name=''',Variable_Name,'''then Vairable_Sample_Data end )',Variable_Name)
	) into @SQL1
	From UI_Variables_Sample_Data;
	
    
	Set @Table1= concat('Select  Variable_View_Name,',@SQL1,' From UI_Variables_Sample_Data Group by Variable_View_Name');
	
    set Comm_Template = replace(Comm_Template,'\\"','"');
    select (char_length(Comm_Template)-char_length(replace(Comm_Template,'|',''))) as a into @Template_Col_No;
    select (char_length(Header)-char_length(replace(Header,'|',''))) as b into @Header_Col_No;
    select replace(replace(Comm_Template,'<SOLUS_PFIELD>','<SOLUS_PFIELD>ifnull('),'</SOLUS_PFIELD>',',"")</SOLUS_PFIELD>') into Comm_Template;
	select replace(concat('concat("',replace(replace(replace(Comm_Template,'|','"),concat("'),'<SOLUS_PFIELD>','",'),'</SOLUS_PFIELD>',',"'),'")'),',concat',',"|",concat') into @Template;
      
      
      set @sql2 = concat("select concat(",@Template,") into @vOutput from (select * from (",@Table1,")A) TEMP");
      select @sql2;
		prepare stmt2 from @sql2;
		execute stmt2;
		deallocate prepare stmt2;
        if @vOutput = ''
        THEN
			SET @vOutput = 'Template is Correct, data not available';
		ELSEIF @Template_Col_No <> @Header_Col_No
        THEN
			SET @vOutput = 'Template and header have different number of columns, Please check configuration';
		END IF;
        set vOutput = ifnull(@vOutput,'Template is Correct, data not available');
END$$
DELIMITER ;
