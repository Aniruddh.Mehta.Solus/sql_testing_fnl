DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_loyalty_region`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare IN_AGGREGATION_4 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_AGGREGATION_4 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_4"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    
    
     SET @FY_SATRT_DATE = date_format(date_sub(concat(IN_FILTER_CURRENTMONTH,'01'), INTERVAL 11 MONTH),"%Y%m");
	 SET @FY_END_DATE = IN_FILTER_CURRENTMONTH;
	 SET @TM = (SELECT date_format(current_date(),"%Y%m"));
	SET @LM = F_Month_Diff(@TM,1);
	SET @SMLY = F_Month_Diff(@TM,12);
	SET @TY=substr(@TM,1,4);
	SET @LY=substr(@SMLY,1,4);
	SET @LLY = @LY - 1;
     SET @SMLLY = F_Month_Diff(IN_FILTER_CURRENTMONTH,24);
     SELECT @TM,@LM,@SMLY,@TY,@LY,@LLY,@SMLLY;
	 
	 IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_1 = 'MEMBERBASE' AND IN_AGGREGATION_2 = 'BYMONTH' AND IN_NUMBER_FORMAT = 'NUMBER'
		THEN
        SELECT group_concat(distinct Tier order by Tier) into @distinct_rc from Dashboard_Loyalty_Region_Temp;
		    select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
			set @loop_var=1;
			set @json_select='';
            set @top_query='';
			select @loop_var,@num_of_rc,@distinct_rc;
			WHILE @loop_var<=@num_of_rc
			do
			SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
            SET @alias_Name = @selected_rc;
            
			set @top_query=concat(@top_query,",MAX(CASE WHEN Tier = '",@selected_rc,"' Then Member_Count ELSE 0 END) AS `",@alias_Name,"`");
				set @json_select=concat(@json_select,',"',@alias_Name,'",`',@alias_Name,'`');
				set @loop_var=@loop_var+1;
			end while;
			
			SELECT @json_select,@top_query;
            
            set @view_stmt1 = concat("create or replace View V_Dashboard_Loyalty_Region_Table_1 as
			SELECT
			Region,
			SUM(Member_count) AS Members
			FROM Dashboard_Loyalty_Region_Temp
			WHERE Measure = 'BYMONTH'
			AND Bill_Year_Month = ",IN_FILTER_CURRENTMONTH,"
			GROUP BY 1
			;");
            
            select @view_stmt1;
			PREPARE statement from @view_stmt1;
			Execute statement;
			Deallocate PREPARE statement;
			
			set @view_stmt = concat("create or replace View V_Dashboard_Loyalty_Region_Table as
			(
			select Region, Members
			",@top_query,"
			from 
			(SELECT A.Region, Tier, Members, ifnull(Member_Count,0) AS Member_Count
			FROM Dashboard_Loyalty_Region_Temp A
            JOIN V_Dashboard_Loyalty_Region_Table_1 B
            ON A.Region=B.Region
			WHERE Measure = 'BYMONTH'
			AND Bill_Year_Month = ",IN_FILTER_CURRENTMONTH,"
            AND A.Region IS NOT NULL
			GROUP BY 1,2
			)A
			GROUP BY 1)
			;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Region",`Region`,"Members",`Members`',@json_select,') )),"]")into @temp_result from V_Dashboard_Loyalty_Region_Table;') ; 
        
END IF;
        
        IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_1 = 'MEMBERBASE' AND IN_AGGREGATION_2 = 'BYMONTH' AND IN_NUMBER_FORMAT = 'PERCENT'
		THEN
        SELECT group_concat(distinct TierPercent order by TierPercent) into @distinct_rc from Dashboard_Loyalty_Region_Temp;
		    select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
			set @loop_var=1;
			set @json_select='';
            set @top_query='';
			select @loop_var,@num_of_rc,@distinct_rc;
			WHILE @loop_var<=@num_of_rc
			do
			SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
            SET @alias_Name = @selected_rc;
            
			set @top_query=concat(@top_query,",MAX(CASE WHEN TierPercent = '",@selected_rc,"' Then MemberPercent ELSE 0.00 END) AS `",@alias_Name,"`");
				set @json_select=concat(@json_select,',"',@alias_Name,'",`',@alias_Name,'`');
				set @loop_var=@loop_var+1;
			end while;
			
			SELECT @json_select,@top_query;
			
			set @view_stmt = concat("create or replace View V_Dashboard_Loyalty_Region_Table as
			(
			select Region
			",@top_query,"
			from 
			(SELECT A.Region, TierPercent, concat(ifnull(MemberPercent,0),'%') AS MemberPercent
			FROM Dashboard_Loyalty_Region_Temp A
			WHERE Measure = 'BYMONTH'
			AND Bill_Year_Month = ",IN_FILTER_CURRENTMONTH,"
            AND A.Region IS NOT NULL
			GROUP BY 1,2
			)A
			GROUP BY 1)
			;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Region",`Region`',@json_select,') )),"]")into @temp_result from V_Dashboard_Loyalty_Region_Table;') ; 
							
END IF;

	 IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_1 = 'MEMBERBASE' AND IN_AGGREGATION_2 <> 'BYMONTH' AND IN_NUMBER_FORMAT = 'NUMBER'
		THEN
        SELECT group_concat(distinct Tier order by Tier) into @distinct_rc from Dashboard_Loyalty_Region_Temp;
		    select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
			set @loop_var=1;
			set @json_select='';
            set @top_query='';
			select @loop_var,@num_of_rc,@distinct_rc;
			WHILE @loop_var<=@num_of_rc
			do
			SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
            SET @alias_Name = @selected_rc;
            
			set @top_query=concat(@top_query,",MAX(CASE WHEN Tier = '",@selected_rc,"' Then Member_Count ELSE 0 END) AS `",@alias_Name,"`");
				set @json_select=concat(@json_select,',"',@alias_Name,'",`',@alias_Name,'`');
				set @loop_var=@loop_var+1;
			end while;
			
			SELECT @json_select,@top_query;
            
            SET @where_cond = CASE WHEN IN_AGGREGATION_2 = 'OVERALL' THEN "Measure = 'OVERALL'" WHEN IN_AGGREGATION_2 = 'TY' THEN "Measure = 'TY'" WHEN IN_AGGREGATION_2 = 'L12M' THEN "Measure = 'L12M'" END;
            
            set @view_stmt1 = concat("create or replace View V_Dashboard_Loyalty_Region_Table_1 as
			SELECT
			Region,
			SUM(Member_count) AS Members
			FROM Dashboard_Loyalty_Region_Temp
			where ",@where_cond,"
			GROUP BY 1
			;");
            
            select @view_stmt1;
			PREPARE statement from @view_stmt1;
			Execute statement;
			Deallocate PREPARE statement;
			
			set @view_stmt = concat("create or replace View V_Dashboard_Loyalty_Region_Table as
			(
			select Region, Members
			",@top_query,"
			from 
			(SELECT A.Region, Tier, Members, ifnull(Member_Count,0) AS Member_Count
			FROM Dashboard_Loyalty_Region_Temp A
            JOIN V_Dashboard_Loyalty_Region_Table_1 B
            ON A.Region=B.Region
			where ",@where_cond,"
            AND A.Region IS NOT NULL
			GROUP BY 1,2
			)A
			GROUP BY 1)
			;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Region",`Region`,"Members",`Members`',@json_select,') )),"]")into @temp_result from V_Dashboard_Loyalty_Region_Table;') ; 
        
END IF;
        
        IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_1 = 'MEMBERBASE' AND IN_AGGREGATION_2 <> 'BYMONTH' AND IN_NUMBER_FORMAT = 'PERCENT'
		THEN
        SELECT group_concat(distinct TierPercent order by TierPercent) into @distinct_rc from Dashboard_Loyalty_Region_Temp;
		    select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
			set @loop_var=1;
			set @json_select='';
            set @top_query='';
			select @loop_var,@num_of_rc,@distinct_rc;
			WHILE @loop_var<=@num_of_rc
			do
			SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
            SET @alias_Name = @selected_rc;
            
			set @top_query=concat(@top_query,",MAX(CASE WHEN TierPercent = '",@selected_rc,"' Then MemberPercent ELSE 0.00 END) AS `",@alias_Name,"`");
				set @json_select=concat(@json_select,',"',@alias_Name,'",`',@alias_Name,'`');
				set @loop_var=@loop_var+1;
			end while;
			
			SELECT @json_select,@top_query;
            
            SET @where_cond = CASE WHEN IN_AGGREGATION_2 = 'OVERALL' THEN "Measure = 'OVERALL'" WHEN IN_AGGREGATION_2 = 'TY' THEN "Measure = 'TY'" WHEN IN_AGGREGATION_2 = 'L12M' THEN "Measure = 'L12M'" END;
			
			set @view_stmt = concat("create or replace View V_Dashboard_Loyalty_Region_Table as
			(
			select Region
			",@top_query,"
			from 
			(SELECT A.Region, TierPercent, concat(ifnull(MemberPercent,0),'%') AS MemberPercent
			FROM Dashboard_Loyalty_Region_Temp A
			where ",@where_cond,"
            AND A.Region IS NOT NULL
			GROUP BY 1,2
			)A
			GROUP BY 1)
			;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Region",`Region`',@json_select,') )),"]")into @temp_result from V_Dashboard_Loyalty_Region_Table;') ; 
							
END IF;

		        IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_1 = 'ENROLLMENT' AND IN_AGGREGATION_2 = 'BYMONTH'
		THEN
        
			set @view_stmt = concat("create or replace View V_Dashboard_Loyalty_Region_Table as
							WITH GROUP1 AS
                            (
							SELECT Region, ifnull(Non_Members,0) AS Non_Members
							FROM Dashboard_Loyalty_Region_Temp A
							WHERE Measure = 'BYMONTH'
                            AND Bill_Year_Month = ",IN_FILTER_CURRENTMONTH,"
							AND Non_Members IS NOT NULL
							GROUP BY 1
                            )
                             ,
                             GROUP2 AS
							(
                            SELECT Region, concat(ifnull(Enrollment_Percent,0),'%') AS Enrollment_Percent
							FROM Dashboard_Loyalty_Region_Temp A
							WHERE Measure = 'BYMONTH'
                            AND Bill_Year_Month = ",IN_FILTER_CURRENTMONTH,"
							AND Enrollment_Percent IS NOT NULL
							GROUP BY 1
                            )
							 ,
                             GROUP3 AS
							(
                            SELECT Region, concat(ifnull(Tagged_Percent,0),'%') AS Tagged_Percent
							FROM Dashboard_Loyalty_Region_Temp A
							WHERE Measure = 'BYMONTH'
                            AND Bill_Year_Month = ",IN_FILTER_CURRENTMONTH,"
							AND Tagged_Percent IS NOT NULL
							GROUP BY 1
                            )
                             ,
                             GROUP4 AS
							(
                            SELECT Region, ifnull(New_Members,0) AS New_Members
							FROM Dashboard_Loyalty_Region_Temp A
							WHERE Measure = 'BYMONTH'
                            AND Bill_Year_Month = ",IN_FILTER_CURRENTMONTH,"
							AND New_Members IS NOT NULL
							GROUP BY 1
                            )
                            SELECT DISTINCT A.Region, Non_Members, Enrollment_Percent, Tagged_Percent, New_Members
                            FROM GROUP1 A
                            JOIN GROUP2 B
                            ON A.Region=B.Region
							JOIN GROUP3 C
                            ON B.Region=C.Region
                            JOIN GROUP4 D
                            ON C.Region=D.Region
			;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Region",`Region`,"Non Members",CAST(format(ifnull(`Non_Members`,0),",") AS CHAR),"New Members",CAST(format(ifnull(`New_Members`,0),",") AS CHAR),"Enrollment %",`Enrollment_Percent`,"Tagged %",`Tagged_Percent`) )),"]")into @temp_result from V_Dashboard_Loyalty_Region_Table;') ; 
        
END IF;
        
        IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_1 = 'ENROLLMENT' AND IN_AGGREGATION_2 <> 'BYMONTH'
		THEN
        
        SET @where_cond = CASE WHEN IN_AGGREGATION_2 = 'OVERALL' THEN "Measure = 'OVERALL'" WHEN IN_AGGREGATION_2 = 'TY' THEN "Measure = 'TY'" WHEN IN_AGGREGATION_2 = 'L12M' THEN "Measure = 'L12M'" END;
        
			set @view_stmt = concat("create or replace View V_Dashboard_Loyalty_Region_Table as
							WITH GROUP1 AS
                            (
							SELECT Region, ifnull(Non_Members,0) AS Non_Members
							FROM Dashboard_Loyalty_Region_Temp A
							WHERE ",@where_cond,"
							AND Non_Members IS NOT NULL
							GROUP BY 1
                            )
                             ,
                             GROUP2 AS
							(
                            SELECT Region, concat(ifnull(Enrollment_Percent,0),'%') AS Enrollment_Percent
							FROM Dashboard_Loyalty_Region_Temp A
							WHERE ",@where_cond,"
							AND Enrollment_Percent IS NOT NULL
							GROUP BY 1
                            )
							 ,
                             GROUP3 AS
							(
                            SELECT Region, concat(ifnull(Tagged_Percent,0),'%') AS Tagged_Percent
							FROM Dashboard_Loyalty_Region_Temp A
							WHERE ",@where_cond,"
							AND Tagged_Percent IS NOT NULL
							GROUP BY 1
                            )
                             ,
                             GROUP4 AS
							(
                            SELECT Region, ifnull(New_Members,0) AS New_Members
							FROM Dashboard_Loyalty_Region_Temp A
							WHERE ",@where_cond,"
							AND New_Members IS NOT NULL
							GROUP BY 1
                            )
                            SELECT DISTINCT A.Region, Non_Members, Enrollment_Percent, Tagged_Percent, New_Members
                            FROM GROUP1 A
                            JOIN GROUP2 B
                            ON A.Region=B.Region
							JOIN GROUP3 C
                            ON B.Region=C.Region
                            JOIN GROUP4 D
                            ON C.Region=D.Region
			;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Region",`Region`,"Non Members",CAST(format(ifnull(`Non_Members`,0),",") AS CHAR),"New Members",CAST(format(ifnull(`New_Members`,0),",") AS CHAR),"Enrollment %",`Enrollment_Percent`,"Tagged %",`Tagged_Percent`) )),"]")into @temp_result from V_Dashboard_Loyalty_Region_Table;') ; 

END IF;

		        IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_1 = 'DATACAPTURE' AND IN_AGGREGATION_2 = 'BYMONTH'
		THEN
        
			set @view_stmt = concat("create or replace View V_Dashboard_Loyalty_Region_Table as
							WITH GROUP1 AS
                            (
							SELECT Region, ifnull(EmailcapPercent,0.00) AS EmailcapPercent
							FROM Dashboard_Loyalty_Region_Temp A
							WHERE Measure = 'BYMONTH'
                            AND Bill_Year_Month = ",IN_FILTER_CURRENTMONTH,"
							AND EmailcapPercent IS NOT NULL
							GROUP BY 1
                            )
                             ,
                             GROUP2 AS
							(
                            SELECT Region, ifnull(AddresscapPercent,0.00) AS AddresscapPercent
							FROM Dashboard_Loyalty_Region_Temp A
							WHERE Measure = 'BYMONTH'
                            AND Bill_Year_Month = ",IN_FILTER_CURRENTMONTH,"
							AND EmailcapPercent IS NOT NULL
							GROUP BY 1
                            )
							 ,
                             GROUP3 AS
							(
                            SELECT Region, ifnull(MobilecapPercent,0.00) AS MobilecapPercent
							FROM Dashboard_Loyalty_Region_Temp A
							WHERE Measure = 'BYMONTH'
                            AND Bill_Year_Month = ",IN_FILTER_CURRENTMONTH,"
							AND MobilecapPercent IS NOT NULL
							GROUP BY 1
                            )
                             ,
                             GROUP4 AS
							(
                            SELECT Region, ifnull(NamecapPercent,0.00) AS NamecapPercent
							FROM Dashboard_Loyalty_Region_Temp A
							WHERE Measure = 'BYMONTH'
                            AND Bill_Year_Month = ",IN_FILTER_CURRENTMONTH,"
							AND NamecapPercent IS NOT NULL
							GROUP BY 1
                            )
                             ,
                             GROUP5 AS
							(
                            SELECT Region, ifnull(DOBcapPercent,0.00) AS DOBcapPercent
							FROM Dashboard_Loyalty_Region_Temp A
							WHERE Measure = 'BYMONTH'
                            AND Bill_Year_Month = ",IN_FILTER_CURRENTMONTH,"
							AND EmailcapPercent IS NOT NULL
							GROUP BY 1
                            )
							,
                             GROUP6 AS
							(
                            SELECT Region, ifnull(GendercapPercent,0.00) AS GendercapPercent
							FROM Dashboard_Loyalty_Region_Temp A
							WHERE Measure = 'BYMONTH'
                            AND Bill_Year_Month = ",IN_FILTER_CURRENTMONTH,"
							AND EmailcapPercent IS NOT NULL
							GROUP BY 1
                            )
                            ,
                             GROUP7 AS
							(
                            SELECT Region, ifnull(DOAcapPercent,0.00) AS DOAcapPercent
							FROM Dashboard_Loyalty_Region_Temp A
							WHERE Measure = 'BYMONTH'
                            AND Bill_Year_Month = ",IN_FILTER_CURRENTMONTH,"
							AND EmailcapPercent IS NOT NULL
							GROUP BY 1
                            )
                            SELECT DISTINCT A.Region, EmailcapPercent, AddresscapPercent, MobilecapPercent, NamecapPercent,DOBcapPercent,GendercapPercent,DOAcapPercent
                            FROM GROUP1 A
                            JOIN GROUP2 B
                            ON A.Region=B.Region
							JOIN GROUP3 C
                            ON B.Region=C.Region
                            JOIN GROUP4 D
                            ON C.Region=D.Region
                            JOIN GROUP5 E
                            ON D.Region=E.Region
                            JOIN GROUP6 F
                            ON E.Region=F.Region
                            JOIN GROUP7 G
                            ON F.Region=G.Region
			;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Region",`Region`,"% Email Capture",concat(`EmailcapPercent`,"%"),"% Address Capture",concat(`AddresscapPercent`,"%"),"% Mobile Capture",concat(`MobilecapPercent`,"%"),"% Name Capture",concat(`NamecapPercent`,"%"),"% DOB Capture",concat(`DOBcapPercent`,"%"),"% Gender Capture",concat(`GendercapPercent`,"%"),"% Anniv. Capture",concat(`DOAcapPercent`,"%") ) )),"]")into @temp_result from V_Dashboard_Loyalty_Region_Table;') ; 
        
END IF;
        
        IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_1 = 'DATACAPTURE' AND IN_AGGREGATION_2 <> 'BYMONTH'
		THEN
        
        SET @where_cond = CASE WHEN IN_AGGREGATION_2 = 'OVERALL' THEN "Measure = 'OVERALL'" WHEN IN_AGGREGATION_2 = 'TY' THEN "Measure = 'TY'" WHEN IN_AGGREGATION_2 = 'L12M' THEN "Measure = 'L12M'" END;
        
			set @view_stmt = concat("create or replace View V_Dashboard_Loyalty_Region_Table as
							WITH GROUP1 AS
                            (
							SELECT Region, ifnull(EmailcapPercent,0.00) AS EmailcapPercent
							FROM Dashboard_Loyalty_Region_Temp A
                            WHERE ",@where_cond,"
							AND EmailcapPercent IS NOT NULL
							GROUP BY 1
                            )
                             ,
                             GROUP2 AS
							(
                            SELECT Region, ifnull(AddresscapPercent,0.00) AS AddresscapPercent
							FROM Dashboard_Loyalty_Region_Temp A
							WHERE ",@where_cond,"
							AND EmailcapPercent IS NOT NULL
							GROUP BY 1
                            )
							 ,
                             GROUP3 AS
							(
                            SELECT Region, ifnull(MobilecapPercent,0.00) AS MobilecapPercent
							FROM Dashboard_Loyalty_Region_Temp A
							WHERE ",@where_cond,"
							AND MobilecapPercent IS NOT NULL
							GROUP BY 1
                            )
                             ,
                             GROUP4 AS
							(
                            SELECT Region, ifnull(NamecapPercent,0.00) AS NamecapPercent
							FROM Dashboard_Loyalty_Region_Temp A
							WHERE ",@where_cond,"
							AND NamecapPercent IS NOT NULL
							GROUP BY 1
                            )
                             ,
                             GROUP5 AS
							(
                            SELECT Region, ifnull(DOBcapPercent,0.00) AS DOBcapPercent
							FROM Dashboard_Loyalty_Region_Temp A
							WHERE ",@where_cond,"
							AND EmailcapPercent IS NOT NULL
							GROUP BY 1
                            )
							,
                             GROUP6 AS
							(
                            SELECT Region, ifnull(GendercapPercent,0.00) AS GendercapPercent
							FROM Dashboard_Loyalty_Region_Temp A
							WHERE ",@where_cond,"
							AND EmailcapPercent IS NOT NULL
							GROUP BY 1
                            )
                            ,
                             GROUP7 AS
							(
                            SELECT Region, ifnull(DOAcapPercent,0.00) AS DOAcapPercent
							FROM Dashboard_Loyalty_Region_Temp A
							WHERE ",@where_cond,"
							AND EmailcapPercent IS NOT NULL
							GROUP BY 1
                            )
                            SELECT DISTINCT A.Region, EmailcapPercent, AddresscapPercent, MobilecapPercent, NamecapPercent,DOBcapPercent,GendercapPercent,DOAcapPercent
                            FROM GROUP1 A
                            JOIN GROUP2 B
                            ON A.Region=B.Region
							JOIN GROUP3 C
                            ON B.Region=C.Region
                            JOIN GROUP4 D
                            ON C.Region=D.Region
                            JOIN GROUP5 E
                            ON D.Region=E.Region
                            JOIN GROUP6 F
                            ON E.Region=F.Region
                            JOIN GROUP7 G
                            ON F.Region=G.Region
			;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Region",`Region`,"% Email Capture",concat(`EmailcapPercent`,"%"),"% Address Capture",concat(`AddresscapPercent`,"%"),"% Mobile Capture",concat(`MobilecapPercent`,"%"),"% Name Capture",concat(`NamecapPercent`,"%"),"% DOB Capture",concat(`DOBcapPercent`,"%"),"% Gender Capture",concat(`GendercapPercent`,"%"),"% Anniv. Capture",concat(`DOAcapPercent`,"%") ) )),"]")into @temp_result from V_Dashboard_Loyalty_Region_Table;') ; 
        
END IF;
        
		        IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_1 = 'KPI' AND IN_AGGREGATION_2 = 'BYMONTH'
		THEN
        
			set @view_stmt = concat("create or replace View V_Dashboard_Loyalty_Region_Table as
							select Region,
							sum(Revenue) as Revenue, 
							sum(Repeat_rev)as Repeat_Revenue,
							round(sum(Repeat_Rev)*100/sum(Revenue),2) as Repeat_Revenue_PER,
							sum(Revenue)/sum(visits) as ABV,
							ifnull(sum(QTY)/sum(visits),0) as BS
							from Dashboard_Loyalty_Metrics_Temp
							where Region <> ''
							AND Bill_Year_Month = ",IN_FILTER_CURRENTMONTH,"
							group by Region
							;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Region",`Region`,"Rev.",CAST(format(ifnull(`Revenue`,0),",") AS CHAR),"Repeat Rev.",CAST(format(ifnull(`Repeat_Revenue`,0),",") AS CHAR),"Repeat Rev.%",concat(`Repeat_Revenue_PER`,"%"),"ABV",CAST(format(ifnull(`ABV`,0),",") AS CHAR ),"BS",CAST(format(ifnull(`BS`,0),",") AS CHAR )) )),"]") into @temp_result from V_Dashboard_Loyalty_Region_Table;') ; 
        
END IF;
        
        IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_1 = 'KPI' AND IN_AGGREGATION_2 = 'OVERALL'
		THEN
        
			set @view_stmt = concat("create or replace View V_Dashboard_Loyalty_Region_Table as
							select Region,
							sum(Revenue) as Revenue, 
							sum(Repeat_rev)as Repeat_Revenue,
							round(sum(Repeat_Rev)*100/sum(Revenue),2) as Repeat_Revenue_PER,
							sum(Revenue)/sum(visits) as ABV,
							ifnull(sum(QTY)/sum(visits),0) as BS
							from Dashboard_Loyalty_Metrics_Temp
							where Region <> ''
							group by Region
							;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Region",`Region`,"Rev.",CAST(format(ifnull(`Revenue`,0),",") AS CHAR),"Repeat Rev.",CAST(format(ifnull(`Repeat_Revenue`,0),",") AS CHAR),"Repeat Rev.%",concat(`Repeat_Revenue_PER`,"%"),"ABV",CAST(format(ifnull(`ABV`,0),",") AS CHAR ),"BS",CAST(format(ifnull(`BS`,0),",") AS CHAR )) )),"]") into @temp_result from V_Dashboard_Loyalty_Region_Table;') ; 
        
END IF;

        IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_1 = 'KPI' AND IN_AGGREGATION_2 = 'TY'
		THEN
        
			set @view_stmt = concat("create or replace View V_Dashboard_Loyalty_Region_Table as
							select Region,
							sum(Revenue) as Revenue, 
							sum(Repeat_rev)as Repeat_Revenue,
							round(sum(Repeat_Rev)*100/sum(Revenue),2) as Repeat_Revenue_PER,
							sum(Revenue)/sum(visits) as ABV,
							ifnull(sum(QTY)/sum(visits),0) as BS
							from Dashboard_Loyalty_Metrics_Temp
							where Region <> ''
                            AND substr(Bill_Year_Month,1,4) = ",@TY,"
							group by Region
							;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Region",`Region`,"Rev.",CAST(format(ifnull(`Revenue`,0),",") AS CHAR),"Repeat Rev.",CAST(format(ifnull(`Repeat_Revenue`,0),",") AS CHAR),"Repeat Rev.%",concat(`Repeat_Revenue_PER`,"%"),"ABV",CAST(format(ifnull(`ABV`,0),",") AS CHAR ),"BS",CAST(format(ifnull(`BS`,0),",") AS CHAR )) )),"]") into @temp_result from V_Dashboard_Loyalty_Region_Table;') ; 
        
END IF;

        IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_1 = 'KPI' AND IN_AGGREGATION_2 = 'L12M'
		THEN
        
			set @view_stmt = concat("create or replace View V_Dashboard_Loyalty_Region_Table as
							select Region,
							sum(Revenue) as Revenue, 
							sum(Repeat_rev)as Repeat_Revenue,
							round(sum(Repeat_Rev)*100/sum(Revenue),2) as Repeat_Revenue_PER,
							sum(Revenue)/sum(visits) as ABV,
							ifnull(sum(QTY)/sum(visits),0) as BS
							from Dashboard_Loyalty_Metrics_Temp
							where Region <> ''
                            AND Bill_Year_Month BETWEEN ",@SMLY," AND ",@TM,"
							group by Region
							;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Region",`Region`,"Rev.",CAST(format(ifnull(`Revenue`,0),",") AS CHAR),"Repeat Rev.",CAST(format(ifnull(`Repeat_Revenue`,0),",") AS CHAR),"Repeat Rev.%",concat(`Repeat_Revenue_PER`,"%"),"ABV",CAST(format(ifnull(`ABV`,0),",") AS CHAR ),"BS",CAST(format(ifnull(`BS`,0),",") AS CHAR )) )),"]") into @temp_result from V_Dashboard_Loyalty_Region_Table;') ; 
        
END IF;

        IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_1 = 'CATEGORY' AND IN_AGGREGATION_2 = 'BYMONTH'
        
		THEN
        SELECT group_concat(distinct Category order by Category) into @distinct_rc from Dashboard_Loyalty_Region_Temp;
		    select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
			set @loop_var=1;
			set @json_select='';
            set @top_query='';
			select @loop_var,@num_of_rc,@distinct_rc;
			WHILE @loop_var<=@num_of_rc
			do
			SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
            SET @alias_Name = @selected_rc;
            
			set @top_query=concat(@top_query,",MAX(CASE WHEN Category = '",@selected_rc,"' Then CategoryPercent ELSE 0.00 END) AS `",@alias_Name,"`");
				set @json_select=concat(@json_select,',"',@alias_Name,'",`',@alias_Name,'`');
				set @loop_var=@loop_var+1;
			end while;
			
			SELECT @json_select,@top_query;
			
			set @view_stmt = concat("create or replace View V_Dashboard_Loyalty_Region_Table as
			(
			select Region
			",@top_query,"
			from 
			(SELECT A.Region, Category, concat(ifnull(CategoryPercent,0),'%') AS CategoryPercent
			FROM Dashboard_Loyalty_Region_Temp A
			WHERE Measure = 'BYMONTH'
			AND Bill_Year_Month = ",IN_FILTER_CURRENTMONTH,"
            AND A.Region IS NOT NULL
			GROUP BY 1,2
			)A
			GROUP BY 1)
			;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Region",`Region`',@json_select,') )),"]")into @temp_result from V_Dashboard_Loyalty_Region_Table;') ; 
							
END IF;

		IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_1 = 'CATEGORY' AND IN_AGGREGATION_2 <> 'BYMONTH'
        
		THEN
        SELECT group_concat(distinct Category order by Category) into @distinct_rc from Dashboard_Loyalty_Region_Temp;
		    select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
			set @loop_var=1;
			set @json_select='';
            set @top_query='';
			select @loop_var,@num_of_rc,@distinct_rc;
			WHILE @loop_var<=@num_of_rc
			do
			SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
            SET @alias_Name = @selected_rc;
            
			set @top_query=concat(@top_query,",MAX(CASE WHEN Category = '",@selected_rc,"' Then CategoryPercent ELSE 0.00 END) AS `",@alias_Name,"`");
				set @json_select=concat(@json_select,',"',@alias_Name,'",`',@alias_Name,'`');
				set @loop_var=@loop_var+1;
			end while;
			
			SELECT @json_select,@top_query;
            
            SET @where_cond = CASE WHEN IN_AGGREGATION_2 = 'OVERALL' THEN "Measure = 'OVERALL'" WHEN IN_AGGREGATION_2 = 'TY' THEN "Measure = 'TY'" WHEN IN_AGGREGATION_2 = 'L12M' THEN "Measure = 'L12M'" END;
			
			set @view_stmt = concat("create or replace View V_Dashboard_Loyalty_Region_Table as
			(
			select Region
			",@top_query,"
			from 
			(SELECT A.Region, Category, concat(ifnull(CategoryPercent,0),'%') AS CategoryPercent
			FROM Dashboard_Loyalty_Region_Temp A
			WHERE ",@where_cond,"
            AND A.Region IS NOT NULL
			GROUP BY 1,2
			)A
			GROUP BY 1)
			;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Region",`Region`',@json_select,') )),"]")into @temp_result from V_Dashboard_Loyalty_Region_Table;') ; 
							
END IF;

	IF IN_SCREEN <> "DOWNLOAD"
    THEN
		select @view_stmt;
		PREPARE statement from @view_stmt;
		Execute statement;
		Deallocate PREPARE statement;
		select @Query_String;
		PREPARE statement from @Query_String;
		Execute statement;
		Deallocate PREPARE statement;	
		SET out_result_json = @temp_result;
	ELSE
		call S_dashboard_performance_download('V_Dashboard_Loyalty_Category_Table',@out_result_json1);
		select @out_result_json1 into out_result_json;
    END IF;
	
END$$
DELIMITER ;
