DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_QSR_platform`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    
    
     SET @FY_SATRT_DATE = date_format(date_sub(concat(IN_FILTER_CURRENTMONTH,'01'), INTERVAL 11 MONTH),"%Y%m");
	 SET @FY_END_DATE = IN_FILTER_CURRENTMONTH;
	 SET @TM = IN_FILTER_CURRENTMONTH;
	 SET @LM = F_Month_Diff(IN_FILTER_CURRENTMONTH,1);
	 SET @SMLY = F_Month_Diff(IN_FILTER_CURRENTMONTH,12);
	 SET @TY=substr(@TM,1,4);
	 SET @LY=substr(@SMLY,1,4);
	 SET @LLY = @LY - 1;
     SET @SMLLY = F_Month_Diff(IN_FILTER_CURRENTMONTH,24);
     SELECT @TM,@LM,@SMLY,@TY,@LY,@LLY,@SMLLY;
	
	IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_1 = 'BASE'
	THEN 
			SELECT "in table base;";
			set @view_stmt = concat("create or replace  View V_Dashboard_QSR_Platform_Table as
							SELECT Platform as 'Platform',TM, LM, SMLY, CH_LM_PER, CH_SMLY_PER, TY, LY, LLY, CH_LY_PER, CH_LLY_PER, L12M, L24M, CH_L12M_PER, OVERALL
							from Dashboard_QSR_Platform_Temp
							where Bill_Year_Month = ",@TM,"
                            AND Measure = 'Base'
							GROUP BY 1
							;");
		
							SET @Query_String =  
                            'SELECT CONCAT("[",(GROUP_CONCAT(json_object(
                            "Platform",`Platform`,
                            "TM",CAST(format(ifnull(`TM`,0),",") AS CHAR),
                            "LM",CAST(format(ifnull(`LM`,0),",") AS CHAR),
                            "SMLY",CAST(format(ifnull(`SMLY`,0),",") AS CHAR),
                            "% CH. LM",concat(ifnull(`CH_LM_PER`,0),"%"),
                            "% CH. SMLY",concat(ifnull(`CH_SMLY_PER`,0),"%"),
                            "TY",CAST(format(ifnull(`TY`,0),",") AS CHAR),
                            "LY",CAST(format(ifnull(`LY`,0),",") AS CHAR),
                            "LLY",CAST(format(ifnull(`LLY`,0),",") AS CHAR),
                            "% CH. LY",concat(ifnull(`CH_LY_PER`,0),"%"),
                            "% CH. LLY",concat(ifnull(`CH_LLY_PER`,0),"%"),
                            "L12M",CAST(format(ifnull(`L12M`,0),",") AS CHAR),
                            "L24M",CAST(format(ifnull(`L24M`,0),",") AS CHAR),
                            "% CH. L12M",concat(ifnull(`CH_L12M_PER`,0),"%"),
                            "OVERALL",CAST(format(ifnull(`OVERALL`,0),",") AS CHAR)
                            ) )),"]")into @temp_result from V_Dashboard_QSR_Platform_Table;' ;
		

END IF;

IF IN_AGGREGATION_1 = 'VISIT'
THEN
	SET @where_cond = 'Measure = "Bills"';
END IF;

IF IN_AGGREGATION_1 = 'REV'
THEN 
	SET @where_cond = 'Measure = "Revenue"';
END IF;

IF IN_AGGREGATION_1 = 'REPEAT_REV'
THEN
	SET @where_cond = 'Measure = "Repeat_Revenue"';
END IF;

IF IN_AGGREGATION_1 = 'REPEAT_REV_CONTR'
THEN
	SET @where_cond = 'Measure = "REPEAT_REV_CONTR"';
END IF;

IF IN_AGGREGATION_1 = 'APC'
THEN
	SET @where_cond = 'Measure = "APC"';
END IF;

IF IN_AGGREGATION_1 = 'Units'
THEN
	SET @where_cond = 'Measure = "Units"';
END IF;

IF IN_AGGREGATION_1 = 'BS'
THEN
	SET @where_cond = 'Measure = "BS"';
END IF;


IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_1 <> 'BASE'
	THEN 
			set @view_stmt = concat("create or replace  View V_Dashboard_QSR_Platform_Table as
							SELECT Platform as 'Platform',TM, LM, SMLY, CH_LM_PER, CH_SMLY_PER, TY, LY, LLY, CH_LY_PER, CH_LLY_PER, L12M, L24M, CH_L12M_PER
							from Dashboard_QSR_Platform_Temp
							where Bill_Year_Month = ",@TM,"
                            AND ",@where_cond,"
							GROUP BY 1
							;");
		
							SET @Query_String =  
                            'SELECT CONCAT("[",(GROUP_CONCAT(json_object(
                            "Platform",`Platform`,
                            "TM",CAST(format(ifnull(`TM`,0),",") AS CHAR),
                            "LM",CAST(format(ifnull(`LM`,0),",") AS CHAR),
                            "SMLY",CAST(format(ifnull(`SMLY`,0),",") AS CHAR),
                            "% CH. LM",concat(ifnull(`CH_LM_PER`,0),"%"),
                            "% CH. SMLY",concat(ifnull(`CH_SMLY_PER`,0),"%"),
                            "TY",CAST(format(ifnull(`TY`,0),",") AS CHAR),
                            "LY",CAST(format(ifnull(`LY`,0),",") AS CHAR),
                            "LLY",CAST(format(ifnull(`LLY`,0),",") AS CHAR),
                            "% CH. LY",concat(ifnull(`CH_LY_PER`,0),"%"),
                            "% CH. LLY",concat(ifnull(`CH_LLY_PER`,0),"%"),
                            "L12M",CAST(format(ifnull(`L12M`,0),",") AS CHAR),
                            "L24M",CAST(format(ifnull(`L24M`,0),",") AS CHAR),
                            "% CH. L12M",concat(ifnull(`CH_L12M_PER`,0),"%")
                            ) )),"]")into @temp_result from V_Dashboard_QSR_Platform_Table;' ;
		

END IF;
	
	IF IN_SCREEN <> "DOWNLOAD"
    THEN
		select @view_stmt;
		PREPARE statement from @view_stmt;
		Execute statement;
		Deallocate PREPARE statement;
		select @Query_String;
		PREPARE statement from @Query_String;
		Execute statement;
		Deallocate PREPARE statement;	
		SET out_result_json = @temp_result;
	ELSE
		call S_dashboard_performance_download('V_Dashboard_QSR_Platform_Table',@out_result_json1);
		select @out_result_json1 into out_result_json;
    END IF;
	
END$$
DELIMITER ;
