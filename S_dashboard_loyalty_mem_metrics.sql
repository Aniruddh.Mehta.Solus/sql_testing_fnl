DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_loyalty_mem_metrics`()
BEGIN

DELETE from log_solus where module = 'S_dashboard_loyalty_mem_metrics';
SET SQL_SAFE_UPDATES=0;

SELECT `Value1` into @TYSM from UI_Configuration_Global where `Config_Name` = 'FISCAL';
SELECT @TYSM;

SELECT `Value2` into @TYEM from UI_Configuration_Global where `Config_Name` = 'FISCAL';
SELECT @TYEM;

SET @LYSM = F_Month_Diff(@TYSM,12);
SET @LYEM = F_Month_Diff(@TYEM,12);

SELECT @LYSM,@LYEM;

SET @LLYSM = F_Month_Diff(@LYSM,12);
SET @LLYEM = F_Month_Diff(@LYEM,12);

SELECT @LLYSM,@LLYEM;

SET @Start_Date = date_format(date_sub(concat(date_format(current_date(),"%Y%m"),'01'), INTERVAL 14 MONTH),"%Y%m");
SET @End_Date = date_format(current_date(),"%Y%m");

SET @TM = (SELECT date_format(current_date(),"%Y%m"));
SET @LM = F_Month_Diff(@TM,1);
SET @SMLY = F_Month_Diff(@TM,12);
SET @TY=substr(@TM,1,4);
SET @LY=substr(@SMLY,1,4);
SET @LLY = @LY - 1;
SET @SMLLY = F_Month_Diff(@TM,24);

SELECT @TM,@LM,@SMLY,@TY,@LY,@LLY,@SMLLY;
	
DROP TABLE IF EXISTS Dashboard_Loyalty_Mem_Temp;
  CREATE TABLE IF NOT EXISTS Dashboard_Loyalty_Mem_Temp ( 
	`Measure` varchar(50),
	`Membership` varchar(50),
    `Bill_Year_Month` int(20),
	`TM` int(20),
	`LM` int(20),
	`SMLY` int(20),
	`CH_LM_PER` decimal(10,2),
	`CH_SMLY_PER` decimal(10,2),
	`TY` int(20),
    `LY` int(20),
	`LLY` int(20),
	`CH_LY_PER` decimal(10,2),
	`CH_LLY_PER` decimal(10,2),
	`L12M` int(20),
	`L24M` int(20),
	`CH_L12M_PER` decimal(10,2),
	`OVERALL` int(20)
  ) ENGINE = INNODB DEFAULT CHARACTER SET=LATIN1; 
 
  ALTER TABLE Dashboard_Loyalty_Mem_Temp    
    ADD INDEX (Measure), 
    ADD INDEX (Membership);
    
set @temp_var = 0;

LOOP_12_MONTHS : LOOP

		set @temp_var = @temp_var + 1;
		IF @temp_var = 6 THEN
				LEAVE LOOP_12_MONTHS;
			END IF;
            
SELECT @TM, @LM, @SMLY, @TY, @LY, @LLY, @SMLLY, CURRENT_TIMESTAMP();
INSERT INTO Dashboard_Loyalty_Mem_Temp
(
`Measure`,
`Membership`,
`Bill_Year_Month`,
`TM`,
`LM`,
`SMLY`,
`TY`,
`LY`,
`LLY`,
`L12M`,
`L24M`,
`OVERALL`
)		
WITH GROUP1 AS
                            (
							SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',Bill_Year_Month,count(distinct(Customer_Id)) as TM
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month = @TM
							GROUP BY 1
                            )
                             ,
                             GROUP2 AS
							(
                             SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',count(distinct(Customer_Id)) as LM
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month = @LM
							GROUP BY 1
                            )
                             ,
                             GROUP3 AS
							(
                             SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',count(distinct(Customer_Id)) as SMLY
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month = @SMLY
							GROUP BY 1
                            )
                             ,
                             GROUP4 AS
							(
                             SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',count(distinct(Customer_Id)) as TY
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @TYSM AND @TYEM
							GROUP BY 1
                            )
                             ,
                             GROUP5 AS
							(
                             SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',count(distinct(Customer_Id)) as LY
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @LYSM AND @LYEM
							GROUP BY 1
                            )
                            ,
                             GROUP6 AS
							(
                             SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',count(distinct(Customer_Id)) as LLY
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @LLYSM AND @LLYEM
							GROUP BY 1
                            )
                            ,
                             GROUP7 AS
							(
                             SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',count(distinct(Customer_Id)) as L12M
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @SMLY AND @TM
							GROUP BY 1
                            )
                            ,
                             GROUP8 AS
							(
                             SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',count(distinct(Customer_Id)) as L24M
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @SMLLY AND @SMLY
							GROUP BY 1
                            )
                             ,
                             GROUP9 AS
							(
                             SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',count(distinct(Customer_Id)) as OVERALL
							from Dashboard_Loyalty_Metrics_Temp
							GROUP BY 1
                            )
                            SELECT 'Base' AS Measure, A.Membership, A.Bill_Year_Month, TM, LM, SMLY, TY, LY, LLY, L12M, L24M, OVERALL
                            FROM GROUP1 A
                            JOIN GROUP2 B
                            ON A.Membership=B.Membership
                            JOIN GROUP3 C
                            ON C.Membership=B.Membership
                            JOIN GROUP4 D
                            ON D.Membership=C.Membership
                            JOIN GROUP5 E
                            ON E.Membership=D.Membership
                            JOIN GROUP6 F
                            ON F.Membership=E.Membership
                            JOIN GROUP7 G
                            ON G.Membership=F.Membership
                            JOIN GROUP8 H
                            ON H.Membership=G.Membership
                            JOIN GROUP9 I
                            ON H.Membership=I.Membership;

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_LM_PER = (TM-LM)*100/LM
WHERE Bill_Year_Month = @TM
AND Measure = 'Base';

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_SMLY_PER = (TM-SMLY)*100/SMLY
WHERE Bill_Year_Month = @TM
AND Measure = 'Base';

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_LY_PER = (TY-LY)*100/LY
WHERE Bill_Year_Month = @TM
AND Measure = 'Base';

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_LLY_PER = (TY-LLY)*100/LLY
WHERE Bill_Year_Month = @TM
AND Measure = 'Base';

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_L12M_PER = (L12M-L24M)*100/L24M
WHERE Bill_Year_Month = @TM
AND Measure = 'Base';



INSERT INTO Dashboard_Loyalty_Mem_Temp
(
`Measure`,
`Membership`,
`Bill_Year_Month`,
`TM`,
`LM`,
`SMLY`,
`TY`,
`LY`,
`LLY`,
`L12M`,
`L24M`
)		
WITH GROUP1 AS
                            (
							SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',Bill_Year_Month,sum(Visits) as TM
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month = @TM
							GROUP BY 1
                            )
                             ,
                             GROUP2 AS
							(
                             SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Visits) as LM
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month = @LM
							GROUP BY 1
                            )
                             ,
                             GROUP3 AS
							(
                             SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Visits) as SMLY
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month = @SMLY
							GROUP BY 1
                            )
                             ,
                             GROUP4 AS
							(
                             SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Visits) as TY
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @TYSM AND @TYEM
							GROUP BY 1
                            )
                             ,
                             GROUP5 AS
							(
                             SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Visits) as LY
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @LYSM AND @LYEM
							GROUP BY 1
                            )
                            ,
                             GROUP6 AS
							(
                             SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Visits) as LLY
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @LLYSM AND @LLYEM
							GROUP BY 1
                            )
                            ,
                             GROUP7 AS
							(
                             SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Visits) as L12M
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @SMLY AND @TM
							 
							GROUP BY 1
                            )
                            ,
                             GROUP8 AS
							(
                             SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Visits) as L24M
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @SMLLY AND @SMLY
							 
							GROUP BY 1
                            )
                            SELECT 'Bills' AS Measure, A.Membership, A.Bill_Year_Month, TM, LM, SMLY, TY, LY, LLY, L12M, L24M
                            FROM GROUP1 A
                            JOIN GROUP2 B
                            ON A.Membership=B.Membership
                            JOIN GROUP3 C
                            ON C.Membership=B.Membership
                            JOIN GROUP4 D
                            ON D.Membership=C.Membership
                            JOIN GROUP5 E
                            ON E.Membership=D.Membership
                            JOIN GROUP6 F
                            ON F.Membership=E.Membership
                            JOIN GROUP7 G
                            ON G.Membership=F.Membership
                            JOIN GROUP8 H
                            ON H.Membership=G.Membership;

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_LM_PER = (TM-LM)*100/LM
WHERE Bill_Year_Month = @TM
AND Measure = 'Bills';

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_SMLY_PER = (TM-SMLY)*100/SMLY
WHERE Bill_Year_Month = @TM
AND Measure = 'Bills';

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_LY_PER = (TY-LY)*100/LY
WHERE Bill_Year_Month = @TM
AND Measure = 'Bills';

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_LLY_PER = (TY-LLY)*100/LLY
WHERE Bill_Year_Month = @TM
AND Measure = 'Bills';

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_L12M_PER = (L12M-L24M)*100/L24M
WHERE Bill_Year_Month = @TM
AND Measure = 'Bills';



INSERT INTO Dashboard_Loyalty_Mem_Temp
(
`Measure`,
`Membership`,
`Bill_Year_Month`,
`TM`,
`LM`,
`SMLY`,
`TY`,
`LY`,
`LLY`,
`L12M`,
`L24M`
)		
WITH GROUP1 AS
                            (
							SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',Bill_Year_Month,sum(Revenue) as TM
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month = @TM
							 
							GROUP BY 1
                            )
                             ,
                             GROUP2 AS
							(
                             SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Revenue) as LM
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month = @LM
							 
							GROUP BY 1
                            )
                             ,
                             GROUP3 AS
							(
                             SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Revenue) as SMLY
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month = @SMLY
							 
							GROUP BY 1
                            )
                             ,
                             GROUP4 AS
							(
                             SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Revenue) as TY
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @TYSM AND @TYEM
							 
							GROUP BY 1
                            )
                             ,
                             GROUP5 AS
							(
                             SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Revenue) as LY
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @LYSM AND @LYEM
							 
							GROUP BY 1
                            )
                            ,
                             GROUP6 AS
							(
                             SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Revenue) as LLY
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @LLYSM AND @LLYEM
							 
							GROUP BY 1
                            )
                            ,
                             GROUP7 AS
							(
                             SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Revenue) as L12M
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @SMLY AND @TM
							 
							GROUP BY 1
                            )
                            ,
                             GROUP8 AS
							(
                             SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Revenue) as L24M
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @SMLLY AND @SMLY
							 
							GROUP BY 1
                            )
                            SELECT 'Revenue' AS Measure, A.Membership, A.Bill_Year_Month, TM, LM, SMLY, TY, LY, LLY, L12M, L24M
                            FROM GROUP1 A
                            JOIN GROUP2 B
                            ON A.Membership=B.Membership
                            JOIN GROUP3 C
                            ON C.Membership=B.Membership
                            JOIN GROUP4 D
                            ON D.Membership=C.Membership
                            JOIN GROUP5 E
                            ON E.Membership=D.Membership
                            JOIN GROUP6 F
                            ON F.Membership=E.Membership
                            JOIN GROUP7 G
                            ON G.Membership=F.Membership
                            JOIN GROUP8 H
                            ON H.Membership=G.Membership;

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_LM_PER = (TM-LM)*100/LM
WHERE Bill_Year_Month = @TM
AND Measure = 'Revenue';

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_SMLY_PER = (TM-SMLY)*100/SMLY
WHERE Bill_Year_Month = @TM
AND Measure = 'Revenue';

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_LY_PER = (TY-LY)*100/LY
WHERE Bill_Year_Month = @TM
AND Measure = 'Revenue';

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_LLY_PER = (TY-LLY)*100/LLY
WHERE Bill_Year_Month = @TM
AND Measure = 'Revenue';

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_L12M_PER = (L12M-L24M)*100/L24M
WHERE Bill_Year_Month = @TM
AND Measure = 'Revenue';



INSERT INTO Dashboard_Loyalty_Mem_Temp
(
`Measure`,
`Membership`,
`Bill_Year_Month`,
`TM`,
`LM`,
`SMLY`,
`TY`,
`LY`,
`LLY`,
`L12M`,
`L24M`
)		
WITH GROUP1 AS
                            (
							SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',Bill_Year_Month,sum(Repeat_Rev) as TM
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month = @TM
							 
							GROUP BY 1
                            )
                             ,
                             GROUP2 AS
							(
                             SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Repeat_Rev) as LM
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month = @LM
							 
							GROUP BY 1
                            )
                             ,
                             GROUP3 AS
							(
                             SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Repeat_Rev) as SMLY
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month = @SMLY
							 
							GROUP BY 1
                            )
                             ,
                             GROUP4 AS
							(
                             SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Repeat_Rev) as TY
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @TYSM AND @TYEM
							 
							GROUP BY 1
                            )
                             ,
                             GROUP5 AS
							(
                             SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Repeat_Rev) as LY
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @LYSM AND @LYEM
							 
							GROUP BY 1
                            )
                            ,
                             GROUP6 AS
							(
                             SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Repeat_Rev) as LLY
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @LLYSM AND @LLYEM
							 
							GROUP BY 1
                            )
                            ,
                             GROUP7 AS
							(
                             SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Repeat_Rev) as L12M
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @SMLY AND @TM
							 
							GROUP BY 1
                            )
                            ,
                             GROUP8 AS
							(
                             SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Repeat_Rev) as L24M
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @SMLLY AND @SMLY
							 
							GROUP BY 1
                            )
                            SELECT 'Repeat_Revenue' AS Measure, A.Membership, A.Bill_Year_Month, TM, LM, SMLY, TY, LY, LLY, L12M, L24M
                            FROM GROUP1 A
                            JOIN GROUP2 B
                            ON A.Membership=B.Membership
                            JOIN GROUP3 C
                            ON C.Membership=B.Membership
                            JOIN GROUP4 D
                            ON D.Membership=C.Membership
                            JOIN GROUP5 E
                            ON E.Membership=D.Membership
                            JOIN GROUP6 F
                            ON F.Membership=E.Membership
                            JOIN GROUP7 G
                            ON G.Membership=F.Membership
                            JOIN GROUP8 H
                            ON H.Membership=G.Membership;

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_LM_PER = (TM-LM)*100/LM
WHERE Bill_Year_Month = @TM
AND Measure = 'Repeat_Revenue';

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_SMLY_PER = (TM-SMLY)*100/SMLY
WHERE Bill_Year_Month = @TM
AND Measure = 'Repeat_Revenue';

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_LY_PER = (TY-LY)*100/LY
WHERE Bill_Year_Month = @TM
AND Measure = 'Repeat_Revenue';

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_LLY_PER = (TY-LLY)*100/LLY
WHERE Bill_Year_Month = @TM
AND Measure = 'Repeat_Revenue';

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_L12M_PER = (L12M-L24M)*100/L24M
WHERE Bill_Year_Month = @TM
AND Measure = 'Repeat_Revenue';



INSERT INTO Dashboard_Loyalty_Mem_Temp
(
`Measure`,
`Membership`,
`Bill_Year_Month`,
`TM`,
`LM`,
`SMLY`,
`TY`,
`LY`,
`LLY`,
`L12M`,
`L24M`
)		
WITH GROUP1 AS
                            (
							SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',Bill_Year_Month,sum(Repeat_Rev)*100/sum(Revenue) as TM
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month = @TM
							 
							GROUP BY 1
                            )
                             ,
                             GROUP2 AS
							(
                            SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Repeat_Rev)*100/sum(Revenue) as LM
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month = @LM
							 
							GROUP BY 1
                            )
                             ,
                             GROUP3 AS
							(
                            SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Repeat_Rev)*100/sum(Revenue) as SMLY
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month = @SMLY
							 
							GROUP BY 1
                            )
                             ,
                             GROUP4 AS
							(
                            SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Repeat_Rev)*100/sum(Revenue) as TY
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @TYSM AND @TYEM
							 
							GROUP BY 1
                            )
                             ,
                             GROUP5 AS
							(
                            SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Repeat_Rev)*100/sum(Revenue) as LY
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @LYSM AND @LYEM
							 
							GROUP BY 1
                            )
                            ,
                             GROUP6 AS
							(
                            SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Repeat_Rev)*100/sum(Revenue) as LLY
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @LLYSM AND @LLYEM
							 
							GROUP BY 1
                            )
                            ,
                             GROUP7 AS
							(
                            SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Repeat_Rev)*100/sum(Revenue) as L12M
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @SMLY AND @TM
							 
							GROUP BY 1
                            )
                            ,
                             GROUP8 AS
							(
                            SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Repeat_Rev)*100/sum(Revenue) as L24M
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @SMLLY AND @SMLY
							 
							GROUP BY 1
                            )
                            SELECT 'REPEAT_REV_CONTR' AS Measure, A.Membership, A.Bill_Year_Month, TM, LM, SMLY, TY, LY, LLY, L12M, L24M
                            FROM GROUP1 A
                            JOIN GROUP2 B
                            ON A.Membership=B.Membership
                            JOIN GROUP3 C
                            ON C.Membership=B.Membership
                            JOIN GROUP4 D
                            ON D.Membership=C.Membership
                            JOIN GROUP5 E
                            ON E.Membership=D.Membership
                            JOIN GROUP6 F
                            ON F.Membership=E.Membership
                            JOIN GROUP7 G
                            ON G.Membership=F.Membership
                            JOIN GROUP8 H
                            ON H.Membership=G.Membership;

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_LM_PER = (TM-LM)*100/LM
WHERE Bill_Year_Month = @TM
AND Measure = 'REPEAT_REV_CONTR';

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_SMLY_PER = (TM-SMLY)*100/SMLY
WHERE Bill_Year_Month = @TM
AND Measure = 'REPEAT_REV_CONTR';

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_LY_PER = (TY-LY)*100/LY
WHERE Bill_Year_Month = @TM
AND Measure = 'REPEAT_REV_CONTR';

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_LLY_PER = (TY-LLY)*100/LLY
WHERE Bill_Year_Month = @TM
AND Measure = 'REPEAT_REV_CONTR';

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_L12M_PER = (L12M-L24M)*100/L24M
WHERE Bill_Year_Month = @TM
AND Measure = 'REPEAT_REV_CONTR';



INSERT INTO Dashboard_Loyalty_Mem_Temp
(
`Measure`,
`Membership`,
`Bill_Year_Month`,
`TM`,
`LM`,
`SMLY`,
`TY`,
`LY`,
`LLY`,
`L12M`,
`L24M`
)		
WITH GROUP1 AS
                            (
							SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',Bill_Year_Month,sum(Revenue)/Sum(Visits) as TM
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month = @TM
							 
							GROUP BY 1
                            )
                             ,
                             GROUP2 AS
							(
                            SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Revenue)/Sum(Visits) as LM
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month = @LM
							 
							GROUP BY 1
                            )
                             ,
                             GROUP3 AS
							(
                            SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Revenue)/Sum(Visits) as SMLY
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month = @SMLY
							 
							GROUP BY 1
                            )
                             ,
                             GROUP4 AS
							(
                            SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Revenue)/Sum(Visits) as TY
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @TYSM AND @TYEM
							 
							GROUP BY 1
                            )
                             ,
                             GROUP5 AS
							(
                            SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Revenue)/Sum(Visits) as LY
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @LYSM AND @LYEM
							 
							GROUP BY 1
                            )
                            ,
                             GROUP6 AS
							(
                            SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Revenue)/Sum(Visits) as LLY
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @LLYSM AND @LLYEM
							 
							GROUP BY 1
                            )
                            ,
                             GROUP7 AS
							(
                            SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Revenue)/Sum(Visits) as L12M
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @SMLY AND @TM
							 
							GROUP BY 1
                            )
                            ,
                             GROUP8 AS
							(
                            SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Revenue)/Sum(Visits) as L24M
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @SMLLY AND @SMLY
							 
							GROUP BY 1
                            )
                            SELECT 'ABV' AS Measure, A.Membership, A.Bill_Year_Month, TM, LM, SMLY, TY, LY, LLY, L12M, L24M
                            FROM GROUP1 A
                            JOIN GROUP2 B
                            ON A.Membership=B.Membership
                            JOIN GROUP3 C
                            ON C.Membership=B.Membership
                            JOIN GROUP4 D
                            ON D.Membership=C.Membership
                            JOIN GROUP5 E
                            ON E.Membership=D.Membership
                            JOIN GROUP6 F
                            ON F.Membership=E.Membership
                            JOIN GROUP7 G
                            ON G.Membership=F.Membership
                            JOIN GROUP8 H
                            ON H.Membership=G.Membership;
							
UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_LM_PER = (TM-LM)*100/LM
WHERE Bill_Year_Month = @TM
AND Measure = 'ABV';

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_SMLY_PER = (TM-SMLY)*100/SMLY
WHERE Bill_Year_Month = @TM
AND Measure = 'ABV';

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_LY_PER = (TY-LY)*100/LY
WHERE Bill_Year_Month = @TM
AND Measure = 'ABV';

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_LLY_PER = (TY-LLY)*100/LLY
WHERE Bill_Year_Month = @TM
AND Measure = 'ABV';

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_L12M_PER = (L12M-L24M)*100/L24M
WHERE Bill_Year_Month = @TM
AND Measure = 'ABV';



INSERT INTO Dashboard_Loyalty_Mem_Temp
(
`Measure`,
`Membership`,
`Bill_Year_Month`,
`TM`,
`LM`,
`SMLY`,
`TY`,
`LY`,
`LLY`,
`L12M`,
`L24M`
)			
WITH GROUP1 AS
                            (
							SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',Bill_Year_Month,sum(Qty) as TM
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month = @TM
							 
							GROUP BY 1
                            )
                             ,
                             GROUP2 AS
							(
                            SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Qty) as LM
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month = @LM
							 
							GROUP BY 1
                            )
                             ,
                             GROUP3 AS
							(
                            SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Qty) as SMLY
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month = @SMLY
							 
							GROUP BY 1
                            )
                             ,
                             GROUP4 AS
							(
                            SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Qty) as TY
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @TYSM AND @TYEM
							 
							GROUP BY 1
                            )
                             ,
                             GROUP5 AS
							(
                            SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Qty) as LY
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @LYSM AND @LYEM
							 
							GROUP BY 1
                            )
                            ,
                             GROUP6 AS
							(
                            SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Qty) as LLY
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @LLYSM AND @LLYEM
							 
							GROUP BY 1
                            )
                            ,
                             GROUP7 AS
							(
                            SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Qty) as L12M
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @SMLY AND @TM
							 
							GROUP BY 1
                            )
                            ,
                             GROUP8 AS
							(
                            SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Qty) as L24M
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @SMLLY AND @SMLY
							 
							GROUP BY 1
                            )
                            SELECT 'QTY' AS Measure, A.Membership, A.Bill_Year_Month, TM, LM, SMLY, TY, LY, LLY, L12M, L24M
                            FROM GROUP1 A
                            JOIN GROUP2 B
                            ON A.Membership=B.Membership
                            JOIN GROUP3 C
                            ON C.Membership=B.Membership
                            JOIN GROUP4 D
                            ON D.Membership=C.Membership
                            JOIN GROUP5 E
                            ON E.Membership=D.Membership
                            JOIN GROUP6 F
                            ON F.Membership=E.Membership
                            JOIN GROUP7 G
                            ON G.Membership=F.Membership
                            JOIN GROUP8 H
                            ON H.Membership=G.Membership;

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_LM_PER = (TM-LM)*100/LM
WHERE Bill_Year_Month = @TM
AND Measure = 'QTY';

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_SMLY_PER = (TM-SMLY)*100/SMLY
WHERE Bill_Year_Month = @TM
AND Measure = 'QTY';

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_LY_PER = (TY-LY)*100/LY
WHERE Bill_Year_Month = @TM
AND Measure = 'QTY';

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_LLY_PER = (TY-LLY)*100/LLY
WHERE Bill_Year_Month = @TM
AND Measure = 'QTY';

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_L12M_PER = (L12M-L24M)*100/L24M
WHERE Bill_Year_Month = @TM
AND Measure = 'QTY';



INSERT INTO Dashboard_Loyalty_Mem_Temp
(
`Measure`,
`Membership`,
`Bill_Year_Month`,
`TM`,
`LM`,
`SMLY`,
`TY`,
`LY`,
`LLY`,
`L12M`,
`L24M`
)			
WITH GROUP1 AS
                            (
							SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',Bill_Year_Month,sum(Qty)/sum(visits) as TM
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month = @TM
							 
							GROUP BY 1
                            )
                             ,
                             GROUP2 AS
							(
                            SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Qty)/sum(visits) as LM
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month = @LM
							 
							GROUP BY 1
                            )
                             ,
                             GROUP3 AS
							(
                            SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Qty)/sum(visits) as SMLY
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month = @SMLY
							 
							GROUP BY 1
                            )
                             ,
                             GROUP4 AS
							(
                            SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Qty)/sum(visits) as TY
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @TYSM AND @TYEM
							 
							GROUP BY 1
                            )
                             ,
                             GROUP5 AS
							(
                            SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Qty)/sum(visits) as LY
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @LYSM AND @LYEM
							 
							GROUP BY 1
                            )
                            ,
                             GROUP6 AS
							(
                            SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Qty)/sum(visits) as LLY
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @LLYSM AND @LLYEM
							 
							GROUP BY 1
                            )
                            ,
                             GROUP7 AS
							(
                            SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Qty)/sum(visits) as L12M
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @SMLY AND @TM
							 
							GROUP BY 1
                            )
                            ,
                             GROUP8 AS
							(
                            SELECT case when Membership_Status = 'Y' then 'Member' else 'Non-Member' end as 'Membership',sum(Qty)/sum(visits) as L24M
							from Dashboard_Loyalty_Metrics_Temp
                            where Bill_Year_Month BETWEEN @SMLLY AND @SMLY
							 
							GROUP BY 1
                            )
                            SELECT 'BS' AS Measure, A.Membership, A.Bill_Year_Month, TM, LM, SMLY, TY, LY, LLY, L12M, L24M
                            FROM GROUP1 A
                            JOIN GROUP2 B
                            ON A.Membership=B.Membership
                            JOIN GROUP3 C
                            ON C.Membership=B.Membership
                            JOIN GROUP4 D
                            ON D.Membership=C.Membership
                            JOIN GROUP5 E
                            ON E.Membership=D.Membership
                            JOIN GROUP6 F
                            ON F.Membership=E.Membership
                            JOIN GROUP7 G
                            ON G.Membership=F.Membership
                            JOIN GROUP8 H
                            ON H.Membership=G.Membership;

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_LM_PER = (TM-LM)*100/LM
WHERE Bill_Year_Month = @TM
AND Measure = 'BS';

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_SMLY_PER = (TM-SMLY)*100/SMLY
WHERE Bill_Year_Month = @TM
AND Measure = 'BS';

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_LY_PER = (TY-LY)*100/LY
WHERE Bill_Year_Month = @TM
AND Measure = 'BS';

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_LLY_PER = (TY-LLY)*100/LLY
WHERE Bill_Year_Month = @TM
AND Measure = 'BS';

UPDATE Dashboard_Loyalty_Mem_Temp
SET CH_L12M_PER = (L12M-L24M)*100/L24M
WHERE Bill_Year_Month = @TM
AND Measure = 'BS';

SET @TM = F_Month_Diff(@TM,1);
		SET @LM = F_Month_Diff(@TM,1);
		SET @SMLY = F_Month_Diff(@TM,12);
		SET @TY=substr(@TM,1,4);
		SET @LY=substr(@SMLY,1,4);
		SET @LLY = @LY - 1;
		SET @SMLLY = F_Month_Diff(@TM,24);
END LOOP LOOP_12_MONTHS;

END$$
DELIMITER ;
