DELIMITER $$
CREATE or replace PROCEDURE `CDM_Calculate_CRM_KPIS_Dashboard`(IN  vKPI_Year SMALLINT, vKPI_Month  TINYINT, vCalendarType TINYINT)
BEGIN
	
    DECLARE vKPI_Year_Month CHAR(6) DEFAULT '';
    DECLARE vKPI_StartDate DATE;
	DECLARE vKPI_EndDate DATE;
	
    DECLARE vKPI_CurrYear SMALLINT;
    DECLARE vKPI_CurrYear_StartDate DATE;
	DECLARE vKPI_CurrYear_EndDate DATE;

    DECLARE vKPI_PrevYear SMALLINT;
    DECLARE vKPI_PrevYear_StartDate DATE;
	DECLARE vKPI_PrevYear_EndDate DATE;

	DECLARE vKPI_YearBeforePrevYear SMALLINT;
    DECLARE vKPI_YearBeforePrevYear_StartDate DATE;
	DECLARE vKPI_YearBeforePrevYear_EndDate DATE;

	DECLARE vKPI_Numerator DECIMAL(15,4);
	DECLARE vKPI_Denominator DECIMAL(15,4);
	DECLARE vAst_Dormant_Winback_Var DECIMAL(15,4);
	DECLARE vAll_Dormant_Winback_Var DECIMAL(15,4);

	SET vKPI_StartDate = DATE(CONCAT(vKPI_Year,'\\', vKPI_Month,'\\01'));
	SET vKPI_EndDate = LAST_DAY(vKPI_StartDate);
	SET vKPI_Year_Month = CONCAT (vKPI_Year,vKPI_Month);

	CASE vCalendarType 
	WHEN 0 THEN  
	
		SET vKPI_CurrYear = vKPI_Year; 
		SET vKPI_PrevYear = vKPI_Year - 1;    
		SET vKPI_YearBeforePrevYear = vKPI_Year - 2; 

		SET vKPI_CurrYear_StartDate = DATE(CONCAT(vKPI_CurrYear,'/01/01')); 
		SET vKPI_CurrYear_EndDate = DATE(CONCAT(vKPI_CurrYear,'/12/31')); 
		
		SET vKPI_PrevYear_StartDate = DATE(CONCAT(vKPI_PrevYear,'/01/01')); 
		SET vKPI_PrevYear_EndDate = DATE(CONCAT(vKPI_Year,'/12/31')); 
		
		SET vKPI_YearBeforePrevYear_StartDate = DATE(CONCAT(vKPI_YearBeforePrevYear,'\\01\\01')); 
		SET vKPI_YearBeforePrevYear_EndDate = DATE(CONCAT(vKPI_YearBeforePrevYear,'\\12\\31')); 

	WHEN 1 THEN 
		
		CASE  
		WHEN vKPI_Month BETWEEN 1 AND 3 THEN
			SET vKPI_CurrYear = vKPI_Year - 1; 
			SET vKPI_PrevYear = vKPI_Year - 2; 
			SET vKPI_YearBeforePrevYear = vKPI_Year - 3; 

			SET vKPI_CurrYear_StartDate = DATE(CONCAT(vKPI_CurrYear,'/04/01')); 
			SET vKPI_CurrYear_EndDate = DATE(CONCAT(vKPI_Year,'/03/31')); 
			
			SET vKPI_PrevYear_StartDate = DATE(CONCAT(vKPI_PrevYear,'/04/01')); 
			SET vKPI_PrevYear_EndDate = DATE(CONCAT(vKPI_CurrYear,'/03/31')); 

			SET vKPI_YearBeforePrevYear_StartDate = DATE(CONCAT(vKPI_YearBeforePrevYear,'/04/01')); 
			SET vKPI_YearBeforePrevYear_EndDate = DATE(CONCAT(vKPI_PrevYear,'/03/31')); 

		WHEN vKPI_Month BETWEEN 4 AND 12 THEN

			SET vKPI_CurrYear = vKPI_Year; 
			SET vKPI_PrevYear = vKPI_Year - 1; 
			SET vKPI_YearBeforePrevYear = vKPI_Year - 2; 

			SET vKPI_CurrYear_StartDate = DATE(CONCAT(vKPI_CurrYear,'/04/01')); 
			SET vKPI_CurrYear_EndDate = DATE(CONCAT(vKPI_Year + 1,'/03/31')); 
			
			SET vKPI_PrevYear_StartDate = DATE(CONCAT(vKPI_PrevYear,'/04/01')); 
			SET vKPI_PrevYear_EndDate = DATE(CONCAT(vKPI_CurrYear,'/03/31')); 

			SET vKPI_YearBeforePrevYear_StartDate = DATE(CONCAT(vKPI_YearBeforePrevYear,'/04/01')); 
			SET vKPI_YearBeforePrevYear_EndDate = DATE(CONCAT(vKPI_PrevYear,'/03/31')); 
		END CASE;
	END CASE;
	INSERT IGNORE INTO CDM_CRM_KPIS_Dashboard(KPI_Year,KPI_Month)	VALUES (vKPI_Year, vKPI_Month);
	IF vKPI_Month BETWEEN 1 AND 3 THEN 
		UPDATE CDM_CRM_KPIS_Dashboard 
		SET 
			KPI_Year_Fiscal=vKPI_Year-1,
			KPI_Month_Fiscal= vKPI_Month+9
        WHERE KPI_Year=vKPI_Year AND KPI_Month=vKPI_Month;
	ELSE
		IF vKPI_Month BETWEEN 4 AND 12 THEN
			UPDATE CDM_CRM_KPIS_Dashboard 
			SET KPI_Year_Fiscal= vKPI_Year,
			KPI_Month_Fiscal= vKPI_Month-3
			WHERE KPI_Year=vKPI_Year AND KPI_Month=vKPI_Month;
		END IF;
	END IF;
    
    
    SET vKPI_Numerator = 0;
	SET vKPI_Denominator = 0;

	SELECT 
    COUNT(Customer_Id)
	INTO vKPI_Numerator FROM
    (SELECT 
        CUST_BASE.Customer_Id
    FROM
        (SELECT 
        Customer_Id, COUNT(DISTINCT Bill_Date) AS Visits
    FROM
        CDM_Bill_Header
    WHERE
        Bill_Date BETWEEN vKPI_CurrYear_StartDate AND vKPI_EndDate
            AND Bill_Total_Val > 0
            AND Customer_Id > 0
            AND Customer_Id IN (SELECT 
                Customer_Id
            FROM
                CDM_Customer_TP_Var
            WHERE
                FTD BETWEEN vKPI_CurrYear_StartDate AND vKPI_EndDate)
    GROUP BY Customer_Id) AS CUST_BASE
    WHERE
        CUST_BASE.Visits > 1) AS NUMERATOR;

	SELECT 
    COUNT(1)
	INTO vKPI_Denominator FROM
    (SELECT 
        Customer_Id
    FROM
        CDM_Bill_Header
    WHERE
        Bill_Date BETWEEN vKPI_CurrYear_StartDate AND vKPI_EndDate
            AND Bill_Total_Val > 0
            AND Customer_Id > 0
            AND Customer_Id IN (SELECT 
                Customer_Id
            FROM
                CDM_Customer_TP_Var
            WHERE
                FTD BETWEEN vKPI_CurrYear_StartDate AND vKPI_EndDate)
    GROUP BY Customer_Id) AS DENOMINATOR;
		
	UPDATE CDM_CRM_KPIS_Dashboard 
	SET 
    All_New_To_Repeat_P = (vKPI_Numerator / vKPI_Denominator) * 100,
    All_New_To_Repeat_N = vKPI_Numerator
	WHERE
    KPI_Year = vKPI_Year
        AND KPI_Month = vKPI_Month;
    
	SET vKPI_Numerator = 0;
	SET vKPI_Denominator = 0;

    
    
	SET vKPI_Numerator = 0;
	SET vKPI_Denominator = 0;

	SELECT 
    COUNT(Customer_Id)
INTO vKPI_Numerator FROM
    (SELECT 
        CUST_BASE.Customer_Id
    FROM
        (SELECT 
        Customer_Id, COUNT(DISTINCT Bill_Date) AS Visits
    FROM
        CDM_Bill_Header
    WHERE
        Bill_Date BETWEEN vKPI_CurrYear_StartDate AND vKPI_EndDate
            AND Bill_Total_Val > 0
            AND Customer_Id > 0
            -- AND Customer_Id IN (SELECT 
               --  Customer_Id
            -- FROM
               -- CDM_Customer_TP_Var
            -- WHERE
               -- FTD BETWEEN vKPI_CurrYear_StartDate AND vKPI_EndDate
               -- )
    GROUP BY Customer_Id) AS CUST_BASE
    WHERE
        CUST_BASE.Visits > 1) AS NUMERATOR;

	SELECT 
    COUNT(1)
INTO vKPI_Denominator FROM
    (SELECT 
        Customer_Id
    FROM
        CDM_Bill_Header
    WHERE
        Bill_Date BETWEEN vKPI_CurrYear_StartDate AND vKPI_EndDate
            AND Bill_Total_Val > 0
            AND Customer_Id > 0
           /* AND Customer_Id IN (SELECT 
                Customer_Id
            FROM
                CDM_Customer_TP_Var
            WHERE
                FTD BETWEEN vKPI_CurrYear_StartDate AND vKPI_EndDate)*/
    GROUP BY Customer_Id) AS DENOMINATOR;
		
	UPDATE CDM_CRM_KPIS_Dashboard 
SET 
    All_In_Year_Repeat_P = (vKPI_Numerator / vKPI_Denominator) * 100,
    All_In_Year_Repeat_N = vKPI_Numerator
WHERE
    KPI_Year = vKPI_Year
        AND KPI_Month = vKPI_Month;
    
	SET vKPI_Numerator = 0;
	SET vKPI_Denominator = 0;

	SELECT 
    COUNT(DISTINCT Customer_Id)
INTO vKPI_Numerator FROM
    CDM_Bill_Header
WHERE
    Bill_Date BETWEEN vKPI_CurrYear_StartDate AND vKPI_EndDate
        AND Bill_Total_Val > 0
        AND Customer_Id > 0
        AND Customer_Id IN (SELECT 
            Customer_Id
        FROM
            CDM_Bill_Header
        WHERE
            Bill_Date BETWEEN vKPI_PrevYear_StartDate AND vKPI_PrevYear_EndDate
                AND Bill_Total_Val > 0
                AND Customer_Id > 0
        GROUP BY Customer_Id);
				

	SELECT 
    COUNT(1)
INTO vKPI_Denominator FROM
    (SELECT 
        Customer_Id
    FROM
        CDM_Bill_Header
    WHERE
        Bill_Date BETWEEN vKPI_PrevYear_StartDate AND vKPI_PrevYear_EndDate
            AND Bill_Total_Val > 0
            AND Customer_Id > 0
    GROUP BY Customer_Id) AS DENOMINATOR;

	UPDATE CDM_CRM_KPIS_Dashboard 
SET 
    All_Active_Retention_P = ((vKPI_Numerator / vKPI_Denominator) * 100),
    All_Active_Retention_N = vKPI_Numerator
WHERE
    KPI_Year = vKPI_Year
        AND KPI_Month = vKPI_Month;


	CREATE TABLE IF NOT EXISTS Dormant_Cust_List (
    Customer_Id BIGINT NOT NULL
)  ENGINE=MEMORY;
	
    TRUNCATE TABLE Dormant_Cust_List;
    
	INSERT IGNORE INTO Dormant_Cust_List (Customer_Id) 
		SELECT Customer_Id
		FROM CDM_Bill_Header 
		WHERE 
			Bill_Date < vKPI_PrevYear_StartDate
			AND Bill_Total_Val >0 AND Customer_Id >0
			AND Customer_Id NOT IN (
				SELECT Customer_Id
				FROM CDM_Bill_Header 
				WHERE 
					Bill_Date BETWEEN vKPI_PrevYear_StartDate AND vKPI_PrevYear_EndDate
					AND Bill_Total_Val >0 AND Customer_Id >0
				GROUP BY Customer_Id
			) 
		GROUP BY Customer_Id
		ORDER BY Customer_Id;
	
SELECT 
    ((SELECT 
            COUNT(DISTINCT Customer_Id)
        FROM
            CDM_Bill_Header
        WHERE
            Bill_Date BETWEEN vKPI_CurrYear_StartDate AND vKPI_EndDate
                AND Bill_Total_Val > 0
                AND Customer_Id > 0
                AND Customer_Id IN (SELECT 
                    Customer_Id
                FROM
                    Dormant_Cust_List)) / (SELECT 
            COUNT(Customer_Id)
        FROM
            Dormant_Cust_List))
FROM DUAL INTO vAll_Dormant_Winback_Var;

	UPDATE CDM_CRM_KPIS_Dashboard 
SET 
    All_Dormant_Winback_P = ROUND(vAll_Dormant_Winback_Var * 100, 1),
    All_Dormant_Winback_N = vAll_Dormant_Winback_Var * (SELECT 
            COUNT(Customer_Id)
        FROM
            Dormant_Cust_List)
WHERE
    KPI_Year = vKPI_Year
        AND KPI_Month = vKPI_Month;

	DROP TABLE Dormant_Cust_List;
	 
	SET vKPI_Numerator = 0;
	SET vKPI_Denominator = 0;

	SELECT 
    COUNT(1)
INTO vKPI_Numerator FROM
    (SELECT 
        Customer_Id, Bill_Date
    FROM
        CDM_Bill_Header
    WHERE
        Bill_Date BETWEEN vKPI_CurrYear_StartDate AND vKPI_EndDate
            AND Bill_Total_Val > 0
            AND Customer_Id > 0
    GROUP BY Customer_Id , Bill_Date) AS NUMERATOR;
	
	SELECT 
    COUNT(DISTINCT Customer_Id)
INTO vKPI_Denominator FROM
    CDM_Bill_Header
WHERE
    Bill_Date BETWEEN vKPI_CurrYear_StartDate AND vKPI_EndDate
        AND Bill_Total_Val > 0
        AND Customer_Id > 0;
				
	UPDATE CDM_CRM_KPIS_Dashboard 
SET 
    All_ATF = vKPI_Numerator / vKPI_Denominator
WHERE
    KPI_Year = vKPI_Year
        AND KPI_Month = vKPI_Month;

	 
	SET vKPI_Numerator = 0;
	SET vKPI_Denominator = 0;

	UPDATE CDM_CRM_KPIS_Dashboard 
SET 
    All_ABV = (SELECT 
            SUM(Bill_Total_Val) / COUNT(Bill_Header_Id)
        FROM
            CDM_Bill_Header
        WHERE
            Bill_Date BETWEEN vKPI_CurrYear_StartDate AND vKPI_EndDate
                AND Bill_Total_Val > 0
                AND Customer_Id > 0)
WHERE
    KPI_Year = vKPI_Year
        AND KPI_Month = vKPI_Month;
		
	SET vKPI_Numerator = 0;
	SET vKPI_Denominator = 0;

	SELECT 
    COUNT(DISTINCT Customer_Id)
INTO vKPI_Denominator FROM
    CDM_Bill_Header
WHERE
    Bill_Date BETWEEN vKPI_CurrYear_StartDate AND vKPI_EndDate
        AND Bill_Total_Val > 0
        AND Customer_Id > 0;
        
	
    
    	SELECT 
    COUNT(1)
INTO vKPI_Numerator FROM
    (SELECT 
        Customer_Id, COUNT(DISTINCT Bill_Date) AS Visits
    FROM
        CDM_Bill_Header
    WHERE
        Bill_Date BETWEEN vKPI_CurrYear_StartDate AND vKPI_EndDate
            AND Bill_Total_Val > 0
            AND Customer_Id > 0
    GROUP BY Customer_Id
    HAVING Visits > 1) AS NUMERATOR;

	UPDATE CDM_CRM_KPIS_Dashboard 
SET 
    All_Multi_N = vKPI_Numerator,
    All_Multi_P = (vKPI_Numerator / vKPI_Denominator)*100
WHERE
    KPI_Year = vKPI_Year
        AND KPI_Month = vKPI_Month;
    
    

	SET vKPI_Numerator = 0;
    
	SELECT 
    COUNT(1)
INTO vKPI_Numerator FROM
    (SELECT 
        Customer_Id, COUNT(DISTINCT Bill_Date) AS Visits
    FROM
        CDM_Bill_Header
    WHERE
        Bill_Date BETWEEN vKPI_CurrYear_StartDate AND vKPI_EndDate
            AND Bill_Total_Val > 0
            AND Customer_Id > 0
    GROUP BY Customer_Id
    HAVING Visits = 1) AS NUMERATOR;

	UPDATE CDM_CRM_KPIS_Dashboard 
SET 
    All_Cust_With_1_Visit = vKPI_Numerator
WHERE
    KPI_Year = vKPI_Year
        AND KPI_Month = vKPI_Month;

	UPDATE CDM_CRM_KPIS_Dashboard 
SET 
    All_Percent_Cust_With_1_Visit = vKPI_Numerator / vKPI_Denominator
WHERE
    KPI_Year = vKPI_Year
        AND KPI_Month = vKPI_Month;

	SET vKPI_Numerator = 0;
	
	SELECT 
    COUNT(1)
INTO vKPI_Numerator FROM
    (SELECT 
        Customer_Id, COUNT(DISTINCT Bill_Date) AS Visits
    FROM
        CDM_Bill_Header
    WHERE
        Bill_Date BETWEEN vKPI_CurrYear_StartDate AND vKPI_EndDate
            AND Bill_Total_Val > 0
            AND Customer_Id > 0
    GROUP BY Customer_Id
    HAVING Visits = 2) AS NUMERATOR;

	UPDATE CDM_CRM_KPIS_Dashboard 
SET 
    All_Cust_With_2_Visits = vKPI_Numerator
WHERE
    KPI_Year = vKPI_Year
        AND KPI_Month = vKPI_Month;

	UPDATE CDM_CRM_KPIS_Dashboard 
SET 
    All_Percent_Cust_With_2_Visits = vKPI_Numerator / vKPI_Denominator
WHERE
    KPI_Year = vKPI_Year
        AND KPI_Month = vKPI_Month;

	SET vKPI_Numerator = 0;
	
	SELECT 
    COUNT(1)
INTO vKPI_Numerator FROM
    (SELECT 
        Customer_Id, COUNT(DISTINCT Bill_Date) AS Visits
    FROM
        CDM_Bill_Header
    WHERE
        Bill_Date BETWEEN vKPI_CurrYear_StartDate AND vKPI_EndDate
            AND Bill_Total_Val > 0
            AND Customer_Id > 0
    GROUP BY Customer_Id
    HAVING Visits = 3) AS NUMERATOR;

	UPDATE CDM_CRM_KPIS_Dashboard 
SET 
    All_Cust_With_3_Visits = vKPI_Numerator
WHERE
    KPI_Year = vKPI_Year
        AND KPI_Month = vKPI_Month;

	UPDATE CDM_CRM_KPIS_Dashboard 
SET 
    All_Percent_Cust_With_3_Visits = vKPI_Numerator / vKPI_Denominator
WHERE
    KPI_Year = vKPI_Year
        AND KPI_Month = vKPI_Month;

	SET vKPI_Numerator = 0;
	
	SELECT 
    COUNT(1)
INTO vKPI_Numerator FROM
    (SELECT 
        Customer_Id, COUNT(DISTINCT Bill_Date) AS Visits
    FROM
        CDM_Bill_Header
    WHERE
        Bill_Date BETWEEN vKPI_CurrYear_StartDate AND vKPI_EndDate
            AND Bill_Total_Val > 0
            AND Customer_Id > 0
    GROUP BY Customer_Id
    HAVING Visits = 4) AS NUMERATOR;

	UPDATE CDM_CRM_KPIS_Dashboard 
SET 
    All_Cust_With_4_Visits = vKPI_Numerator
WHERE
    KPI_Year = vKPI_Year
        AND KPI_Month = vKPI_Month;

	UPDATE CDM_CRM_KPIS_Dashboard 
SET 
    All_Percent_Cust_With_4_Visits = vKPI_Numerator / vKPI_Denominator
WHERE
    KPI_Year = vKPI_Year
        AND KPI_Month = vKPI_Month;

	SET vKPI_Numerator = 0;
	
	SELECT 
    COUNT(1)
INTO vKPI_Numerator FROM
    (SELECT 
        Customer_Id, COUNT(DISTINCT Bill_Date) AS Visits
    FROM
        CDM_Bill_Header
    WHERE
        Bill_Date BETWEEN vKPI_CurrYear_StartDate AND vKPI_EndDate
            AND Bill_Total_Val > 0
            AND Customer_Id > 0
    GROUP BY Customer_Id
    HAVING Visits > 4) AS NUMERATOR;

	UPDATE CDM_CRM_KPIS_Dashboard 
SET 
    All_Cust_With_5_OR_More_Visits = vKPI_Numerator
WHERE
    KPI_Year = vKPI_Year
        AND KPI_Month = vKPI_Month;

	UPDATE CDM_CRM_KPIS_Dashboard 
SET 
    All_Percent_Cust_With_5_OR_More_Visits = vKPI_Numerator / vKPI_Denominator
WHERE
    KPI_Year = vKPI_Year
        AND KPI_Month = vKPI_Month;


	UPDATE CDM_CRM_KPIS_Dashboard 
SET 
    All_Avg_Member_Discount = (SELECT 
            (SUM(Bill_Disc) / SUM((Bill_Total_Val) + (Bill_Disc)))
        FROM
            CDM_Bill_Header
        WHERE
            Bill_Date BETWEEN vKPI_CurrYear_StartDate AND vKPI_EndDate
                AND Bill_Total_Val > 0
                AND Customer_Id > 0)
WHERE
    KPI_Year = vKPI_Year
        AND KPI_Month = vKPI_Month;
    
END$$
DELIMITER ;
