DELIMITER $$
CREATE or replace PROCEDURE `S_UI_Campaign_Funnel_Coverage`(IN in_request_json json, OUT out_result_json json)
BEGIN


    declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text; 
	declare IN_AGGREGATION_3 text;
	declare Mobile varchar(400);
	declare Email varchar(400);
	declare DNDP varchar(400);

    declare Throtling Bigint;
    
    declare Recency_Start VARCHAR(128);
	declare Recency_End VARCHAR(128);
	declare Vintage VARCHAR(128);
	declare Visit VARCHAR(128);
	declare Monetary_Value VARCHAR(128);
	declare Feedback_Received VARCHAR(128);
	declare Last_Bought_Category VARCHAR(128);
	
	declare Items_in_cart_Avl VARCHAR(128);

    declare Visit_Start VARCHAR(128);
	declare Visit_End VARCHAR(128);
	declare Monetary_Value_Start VARCHAR(128);
	declare Monetary_Value_End VARCHAR(128);
	declare Favourite_Category VARCHAR(128);
	declare SVOC_Condition VARCHAR(128);
	declare SVOT_Conditiondo VARCHAR(128);
	declare Replaced_Query1 VARCHAR(128);
	declare Favourite_Day VARCHAR(128);
	declare Purchase_Anniversary VARCHAR(128);
	declare Multiple_of_5 VARCHAR(128);
    declare AP_FAV_CAT VARCHAR(128);
    declare Discount VARCHAR(128);
    -- Advanced RFMP Criteria   
	
	Set in_request_json = Replace(in_request_json,'\n','\\n');
 -- --------------------- Decoding the JSON -----------------------------------------------------------------------------------------------------------------------------------------------
    SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.Campagin_Segment")); 
    SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.Region")); 
    SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.Lifecycle_Segment")); 
	SET Mobile = "Yes";
	SET Email = "Yes";
	SET DNDP ="Yes";
	

	SET Throtling = json_unquote(json_extract(in_request_json,"$.Throtling"));
    SET Recency_Start = json_unquote(json_extract(in_request_json,"$.Recency_Start"));
	SET Recency_End = json_unquote(json_extract(in_request_json,"$.Recency_End"));
    SET Vintage = json_unquote(json_extract(in_request_json,"$.Vintage"));

    SET Monetary_Value = json_unquote(json_extract(in_request_json,"$.Monetary_Value"));
    SET Last_Bought_Category = json_unquote(json_extract(in_request_json,"$.Last_Bought_Category"));
   
	
	
	SET Visit_Start = json_unquote(json_extract(in_request_json,"$.Visit_Start"));
	SET Visit_End = json_unquote(json_extract(in_request_json,"$.Visit_End"));
	SET Monetary_Value_Start = json_unquote(json_extract(in_request_json,"$.Monetary_Value_Start"));
	SET Monetary_Value_End = json_unquote(json_extract(in_request_json,"$.Monetary_Value_End"));
	SET Favourite_Category = json_unquote(json_extract(in_request_json,"$.Favourite_Category"));
	SET Favourite_Day = json_unquote(json_extract(in_request_json,"$.Favourite")); 
    SET SVOC_Condition = json_unquote(json_extract(in_request_json,"$.Advance_Trigger_SVOC"));
	SET SVOT_Conditiondo = json_unquote(json_extract(in_request_json,"$.Advance_Trigger_SVOT"));
    SET Purchase_Anniversary  = json_unquote(json_extract(in_request_json,"$.Purchase_Anniversary"));
	SET Multiple_of_5  = json_unquote(json_extract(in_request_json,"$.Multiple_Of_5_Transaction"));
    SET AP_FAV_CAT = json_unquote(json_extract(in_request_json,"$.Active_Period_Category"));
    SET Discount = json_unquote(json_extract(in_request_json,"$.Discount"));
	Set IN_AGGREGATION_3= replace (IN_AGGREGATION_3,"[",'');
	Set SVOC_Condition= replace (SVOC_Condition,"\n",'');
	Set SVOT_Conditiondo= replace (SVOT_Conditiondo,"\n",'');
	Set IN_AGGREGATION_3= replace (IN_AGGREGATION_3,"]",'');
	Set IN_AGGREGATION_3= replace (IN_AGGREGATION_3,'"','');
	Set IN_AGGREGATION_3= replace (IN_AGGREGATION_3,', ',',');
	

	
	
    -- Advanced RFMP Criteria
		 Select  CONCAT_WS(' AND ',
                CASE
                    WHEN Recency_Start = '' THEN NULL
                    ELSE concat (' Recency Between ', Recency_Start ,' and ', Recency_End)
                END
                ,CASE
                    WHEN Visit_Start = '' THEN NULL
                    ELSE concat (' Lt_Num_of_Visits Between ', Visit_Start ,' and ', Visit_End)
                END
                ,CASE
                    WHEN Monetary_Value_Start = '' THEN NULL
                    ELSE concat (' LT_ABV Between ', Monetary_Value_Start ,' and ', Monetary_Value_End)
                END
                ,CASE
                    WHEN Last_Bought_Category = '' THEN NULL
                    ELSE Concat ('LTD_Fav_Cat_LOV_ID in (',Last_Bought_Category,')')
                END
                ,CASE
                    WHEN Favourite_Category = '' THEN NULL
                    ELSE Concat ('LT_Fav_Cat_LOV_ID in (',Favourite_Category,')')
                END
                ,CASE
                    WHEN Favourite_Day = "No" or Favourite_Day = " " or Favourite_Day is null THEN NULL
                    ELSE  Concat ('Lt_Fav_Day_Of_Week =weekDay(now())')
                END
				 ,CASE
                    WHEN Purchase_Anniversary = '' THEN NULL
                    ELSE  Concat ('((Vintage/365)  in (',Purchase_Anniversary,'))')
                END
				 ,CASE
                    WHEN Multiple_of_5 ='' THEN NULL
                    ELSE  Concat ('((Lt_Num_of_Visits/5) in (',Multiple_of_5,'))')
                END
				 ,CASE
                    WHEN AP_FAV_CAT ='' THEN NULL
                    ELSE Concat ('AP_Fav_Cat_LOV_ID in (',AP_FAV_CAT,')')
                END
				,CASE
                    WHEN Discount ='' THEN NULL
                    ELSE Concat ('Lt_Disc_Percent in (',Discount,')')
                END
				
                ) into @Replaced_Query;
				
	
	
	set @SVOC_Condition1 =trim(coalesce(SVOC_Condition));
	
	/*select @Replaced_Query,@SVOC_Condition1;*/
	
	
	
	IF IN_AGGREGATION_1 = " " OR IN_AGGREGATION_1 IS NULL THEN  SET IN_AGGREGATION_1 = 0; END IF;
	IF IN_AGGREGATION_2 = " " OR IN_AGGREGATION_2 IS NULL THEN  SET IN_AGGREGATION_2 = 0; END IF;
	IF IN_AGGREGATION_3 = " " OR IN_AGGREGATION_3 IS NULL THEN  SET IN_AGGREGATION_3 = 0; END IF;
	IF SVOT_Conditiondo = " " OR SVOT_Conditiondo IS NULL THEN  SET SVOT_Conditiondo = 0; END IF;
	IF @Replaced_Query = " " OR @Replaced_Query IS NULL THEN  SET @Replaced_Query = 0; END IF;
	
	/*select SVOC_Condition,@SVOC_Condition1,@Replaced_Query;*/

	
	If @SVOC_Condition1 != "" 
	Then 
				If @Replaced_Query is Null or @Replaced_Query ='' or @Replaced_Query =0
					THEN 
					SET @Replaced_Query=concat(@SVOC_Condition1);	
						ELSE 
						SET @Replaced_Query=concat(@Replaced_Query," AND ",@SVOC_Condition1);	
				end If;	
			
	End If;
	

	
	
	IF @Replaced_Query = " " OR @Replaced_Query IS NULL THEN  SET @Replaced_Query = 0; END IF;
	
	
	
	/*Select IN_AGGREGATION_1,IN_AGGREGATION_2,IN_AGGREGATION_3,@Replaced_Query,SVOT_Conditiondo,Mobile,Email,DNDP;*/
	
SET @sql_stmt = CONCAT('CREATE OR REPLACE VIEW CLM_Eve
        as 
		(SELECT "1" as s_no ,"',IN_AGGREGATION_3,'" AS Lifecycle_SegmentCondition,',0,'  AS Campaign_Segment,',0,' AS Region,
		                    "',0,'" AS Replaced_Querys,"',0,'" AS SVOT_Conditionall,"',0,'" AS Valid_Mobiles,"',0,'" AS Valid_Emails,
							"',0,'" AS DNDO)
		union all
		(SELECT "2" as s_no ,"',IN_AGGREGATION_3,'" AS Lifecycle_SegmentCondition,',IN_AGGREGATION_1,'  AS Campaign_Segment,',0,' AS Region,
		                    "',0,'" AS Replaced_Querys,"',0,'" AS SVOT_Conditionall,"',0,'" AS Valid_Mobiles,"',0,'" AS Valid_Emails,
							"',0,'" AS DNDO)
		union all
		(SELECT "3" as s_no ,"',IN_AGGREGATION_3,'" AS Lifecycle_SegmentCondition,',IN_AGGREGATION_1,'  AS Campaign_Segment,',IN_AGGREGATION_2,' AS Region,
		                    "',0,'" AS Replaced_Querys,"',0,'" AS SVOT_Conditionall,"',0,'" AS Valid_Mobiles,"',0,'" AS Valid_Emails,
							"',0,'" AS DNDO)
	    union all
		(SELECT "4" as s_no ,"',IN_AGGREGATION_3,'" AS Lifecycle_SegmentCondition,',IN_AGGREGATION_1,'  AS Campaign_Segment,',IN_AGGREGATION_2,' AS Region,
		                    "',@Replaced_Query,'" AS Replaced_Querys,"',0,'" AS SVOT_Conditionall,"',0,'" AS Valid_Mobiles,"',0,'" AS Valid_Emails,
							"',0,'" AS DNDO)
	    union all
		(SELECT "5" as s_no ,"',IN_AGGREGATION_3,'" AS Lifecycle_SegmentCondition,',IN_AGGREGATION_1,'  AS Campaign_Segment,',IN_AGGREGATION_2,' AS Region,
		                    "',@Replaced_Query,'" AS Replaced_Querys,"',SVOT_Conditiondo,'" AS SVOT_Conditionall,"',0,'" AS Valid_Mobiles,"',0,'" AS Valid_Emails,
							"',0,'" AS DNDO)
	    union all
		(SELECT "6" as s_no ,"',IN_AGGREGATION_3,'" AS Lifecycle_SegmentCondition,',IN_AGGREGATION_1,'  AS Campaign_Segment,',IN_AGGREGATION_2,' AS Region,
		                    "',@Replaced_Query,'" AS Replaced_Querys,"',SVOT_Conditiondo,'" AS SVOT_Conditionall,"',Mobile,'" AS Valid_Mobiles,"',0,'" AS Valid_Emails,
							"',0,'" AS DNDO)
	    union all
		(SELECT "7" as s_no ,"',IN_AGGREGATION_3,'" AS Lifecycle_SegmentCondition,',IN_AGGREGATION_1,'  AS Campaign_Segment,',IN_AGGREGATION_2,' AS Region,
		                    "',@Replaced_Query,'" AS Replaced_Querys,"',SVOT_Conditiondo,'" AS SVOT_Conditionall,"',0,'" AS Valid_Mobiles,"',Email,'" AS Valid_Emails,
							"',0,'" AS DNDO)	 
	    union all
		(SELECT "8" as s_no ,"',IN_AGGREGATION_3,'" AS Lifecycle_SegmentCondition,',IN_AGGREGATION_1,'  AS Campaign_Segment,',IN_AGGREGATION_2,' AS Region,
		                    "',@Replaced_Query,'" AS Replaced_Querys,"',SVOT_Conditiondo,'" AS SVOT_Conditionall,"',Mobile,'" AS Valid_Mobiles,"',Email,'" AS Valid_Emails,
							"',DNDP,'" AS DNDO)');
							

        select @sql_stmt;
		PREPARE statement1 from @sql_stmt;
		Execute statement1;
		Deallocate PREPARE statement1; 
	
	
	
	 -- Creating the Replace Queary
	/*select * from CLM_Eve;*/
    drop table if exists Temp_CLM_Eve;

	create table  Temp_CLM_Eve  as select * from CLM_Eve;
	
	drop table if exists TEMP_Result_iter;
    CREATE TABLE TEMP_Result_iter ( SNO INT NOT NULL  AUTO_INCREMENT,
			                                Lifecycle_NAME varchar(100),Campaign_NAME  INT,Regions_NAME INT,
											Replaced_Query_NAME varchar(100),SVOT_Condition_NAME varchar(100),Valid_Mobile_NAME varchar(100),Valid_Email_NAME varchar(100)
											,DNDL_NAME varchar(100),Message_NAME varchar(100), validmobilednd varchar(100)
											,validemaildnd varchar(100), PRIMARY KEY(SNO));
	
	
	select * from Temp_CLM_Eve;
	/*SELECT * FROM TEMP_Result_iter;*/
	SELECT Email,Mobile;
	
	begin

	
    DECLARE done1 INT DEFAULT FALSE;
	declare Campaign Bigint;
    declare Lifecycle VARCHAR(100);
	declare s_nos Bigint;
    declare Regions Bigint;
	declare Valid_Mobile varchar(400);
	declare Valid_Email varchar(400);
	declare DNDL varchar(400);
	declare vSQLCondition varchar(400);
	declare Replaced_Query varchar(400);
	declare SVOT_Condition  varchar(400);
	declare Valid_Mobileso varchar(400);
	declare Valid_Emailso varchar(400);
	declare DNDLso varchar(400);
	declare Replaced_Queryso varchar(400);
	
	

    
	
	Declare cur1 cursor for
    select s_no,Campaign_Segment,Region,Lifecycle_SegmentCondition,Valid_Mobiles,Valid_Emails,DNDO,Replaced_Querys,SVOT_Conditionall from Temp_CLM_Eve ;
	
	
    SET done1 = FALSE;
	
	
	
	open cur1;
	        begin
					
							
							new_loop_1: LOOP
									FETCH cur1 into s_nos,Campaign,Regions, Lifecycle,Valid_Mobile,Valid_Email,DNDL,Replaced_Queryso,SVOT_Condition;
									

									

									SELECT s_nos,Campaign,Regions, Lifecycle,Valid_Mobile,Valid_Email,DNDL,Replaced_Queryso,SVOT_Condition;

									
									
		
   
  /*select 		Replaced_Queryso,@Replaced_Query2;*/
   

	
	If  Replaced_Queryso = "0"
	THEN 	Select 3;SET @Replaced_Query2="1=1";	
		ELSE
		set @Replaced_Query2 = Replaced_Queryso;
    end If;	
	

	select @Replaced_Query2;
	
	If Valid_Mobile ="YES"  
	THEN	Set Valid_Mobileso ='1';
	Else Set Valid_Mobileso ='-1,1';
	End If;

	If Valid_Email = "YES"
		THEN	Set Valid_Emailso ='1';
	Else Set Valid_Emailso ='-1,1';
	End If;

	If DNDL = "YES"
		THEN	Set DNDLso ='1';
	Else Set DNDLso ='-1,1';
	End If;

	
	
	/*SELECT s_nos,Replaced_Query,Valid_Mobileso,Valid_Emailso,DNDLso;*/
	
		
	
	

	  
	             /*-- Select s_nos,Campaign,Regions,Lifecycle;*/
	             
	              
	                   
	                    Select Segment_Condition_Solus into @Campaign_SegmentReplacedQuery  from Campaign_Segment where Segment_Id=Campaign;
						Select RegionReplacedQuery into @Region_SegmentReplacedQuery  from CLM_RegionSegment where RegionSegmentId=Regions;
						-- Select CLMSegmentReplacedQuery into @Lifecycle_SegmentReplacedQuery  from CLM_Segment where CLMSegmentId=Lifecycle;
						
						If Lifecycle != "All_segment"
							Then 
							
							Set @SQL1=Concat('Select Concat("(",Replace(group_concat("(",CLM_Segment.CLMSegmentReplacedQuery,")"),","," or "),")") 
							into @Lifecycle_SegmentReplacedQuery  from CLM_Segment where CLMSegmentId in (',Lifecycle,');
							');
							SELECT @SQL1;
							PREPARE statement from @SQL1;
							Execute statement;
							Deallocate PREPARE statement; 
												
						Else Set @Lifecycle_SegmentReplacedQuery='1=1';
						End If;	
						
						If @Campaign_SegmentReplacedQuery is Null or @Campaign_SegmentReplacedQuery ='' 
							Then Set @Campaign_SegmentReplacedQuery="1=1";
						End If;	
						
						If @Region_SegmentReplacedQuery is Null or @Region_SegmentReplacedQuery=''
							Then Set @Region_SegmentReplacedQuery="1=1";
						End If;	
						
						If @Lifecycle_SegmentReplacedQuery is Null or @Lifecycle_SegmentReplacedQuery =''
							Then Set @Lifecycle_SegmentReplacedQuery="1=1";
						End If;	
						
						
						
	                    /*Select @Replaced_Query2, @Campaign_SegmentReplacedQuery, @Region_SegmentReplacedQuery ,@Lifecycle_SegmentReplacedQuery;*/

						
	                    set vSQLCondition= concat(@Replaced_Query2,'  and ', @Campaign_SegmentReplacedQuery,' and ', @Region_SegmentReplacedQuery,' and ',@Lifecycle_SegmentReplacedQuery) ;
	                    
	                    Select vSQLCondition;
						
						
						Select RegionReplacedQueryBill into @Region_SegmentBillQuery  from CLM_RegionSegment where RegionSegmentId=Regions;
						
						

					

						IF  @Region_SegmentBillQuery IS NULL OR @Region_SegmentBillQuery = ''
							THEN
								SET @SVOT_Condition_Region='SELECT 1 from DUAL';
						ELSE
							SET @SVOT_Condition_Region = CONCAT('SELECT 1 from V_CLM_Bill_Detail_One_View rsvot  WHERE rsvot.Customer_id=V_CLM_Customer_One_View.Customer_id and  ',@Region_SegmentBillQuery,'');
						END IF;		
						
						IF  Valid_Mobileso != '1'
							THEN
								SET @SVOT_Condition_Valid_Mobile='SELECT 1 from DUAL';
						ELSE
								SET @SVOT_Condition_Valid_Mobile = CONCAT('SELECT 1 from CDM_Customer_PII_Master Valid_Mobileso  WHERE Valid_Mobileso.Customer_id=V_CLM_Customer_One_View.Customer_id and length(Mobile) >= 10 and Mobile REGEXP "^[0-9]+$"');
						END IF; 	
														
						IF  Valid_Emailso != '1'
							THEN
								SET @SVOT_Condition_Valid_Email='SELECT 1 from DUAL';
						ELSE
								SET @SVOT_Condition_Valid_Email = CONCAT('SELECT 1 from CDM_Customer_PII_Master Valid_Emailso  WHERE Valid_Emailso.Customer_id=V_CLM_Customer_One_View.Customer_id and Email REGEXP "(.*)@(.*)\\.(.*)"');
						END IF; 

						IF  DNDLso != '1'
							THEN
								SET @SVOT_Condition_DND='SELECT 1 from DUAL';
						ELSE
								SET @SVOT_Condition_DND = CONCAT('SELECT 1 from CDM_Customer_Master DNDLso  WHERE DNDLso.Customer_id=V_CLM_Customer_One_View.Customer_id and DND not in(1,"y","Y")');
						END IF; 
                        
						select SVOT_Condition;
						IF  SVOT_Condition IS  NULL OR SVOT_Condition ='' OR SVOT_Condition = "0"
							THEN
								SET @Advance_SVOT_Condition='SELECT 1 from DUAL';
						ELSE		
								Set @TriggerSVOT_Condition=SVOT_Condition;
								SET @Advance_SVOT_Condition = CONCAT('SELECT 1 from V_CLM_Bill_Detail_One_View DNDL  WHERE DNDL.Customer_id=V_CLM_Customer_One_View.Customer_id and ',@TriggerSVOT_Condition,'');
						END IF; 
						select @TriggerSVOT_Condition;
	    
        IF s_nos = "1"
		  THEN
	                    SET @sql_stmt = CONCAT('SELECT Convert(format(Count(1),","),CHAR)  INTO @temp_result 
													FROM V_CLM_Customer_One_View 
													WHERE   ',vSQLCondition,'');
	                    
	                   Select @sql_stmt;
	                    PREPARE stmt FROM @sql_stmt;
	                    EXECUTE stmt; 
	                    DEALLOCATE PREPARE stmt;


	        
		INSERT INTO TEMP_Result_iter (Lifecycle_NAME,Campaign_NAME,Regions_NAME,Replaced_Query_NAME,SVOT_Condition_NAME,Valid_Mobile_NAME,Valid_Email_NAME,DNDL_NAME,Message_NAME)
					VALUES (Lifecycle,Campaign,Regions,Replaced_Queryso,SVOT_Condition,Valid_Mobile,Valid_Email,DNDL,@temp_result);
        END IF;
		
        IF s_nos = "2"
		    THEN
		        IF @Campaign_SegmentReplacedQuery="1=1"
			        THEN
					
					    SET @temp_result = @temp_result;
						
						ELSE 
	                    SET @sql_stmt = CONCAT('SELECT Convert(format(Count(1),","),CHAR)  INTO @temp_result 
													FROM V_CLM_Customer_One_View 
													WHERE  ',vSQLCondition,'
													');
	                    
	                 /*Select @sql_stmt;*/
	                    PREPARE stmt FROM @sql_stmt;
	                    EXECUTE stmt; 
	                    DEALLOCATE PREPARE stmt;
				 END IF;


	        
		INSERT INTO TEMP_Result_iter (Lifecycle_NAME,Campaign_NAME,Regions_NAME,Replaced_Query_NAME,SVOT_Condition_NAME,Valid_Mobile_NAME,Valid_Email_NAME,DNDL_NAME,Message_NAME)
					VALUES (Lifecycle,Campaign,Regions,Replaced_Queryso,SVOT_Condition,Valid_Mobile,Valid_Email,DNDL,@temp_result);
        END IF;
         IF s_nos = "3"
		    THEN
		        IF @Region_SegmentReplacedQuery="1=1" 
			        THEN
					
					    SET @temp_result = @temp_result;
						
						ELSE 
	                    SET @sql_stmt = CONCAT('SELECT Convert(format(Count(1),","),CHAR)  INTO @temp_result 
													FROM V_CLM_Customer_One_View 
													WHERE  ',vSQLCondition,'
													
													AND EXISTS(',@SVOT_Condition_Region,')');
	                    
	                /*Select @sql_stmt;*/
	                    PREPARE stmt FROM @sql_stmt;
	                    EXECUTE stmt; 
	                    DEALLOCATE PREPARE stmt;
				 END IF;


	        
		INSERT INTO TEMP_Result_iter (Lifecycle_NAME,Campaign_NAME,Regions_NAME,Replaced_Query_NAME,SVOT_Condition_NAME,Valid_Mobile_NAME,Valid_Email_NAME,DNDL_NAME,Message_NAME)
					VALUES (Lifecycle,Campaign,Regions,Replaced_Queryso,SVOT_Condition,Valid_Mobile,Valid_Email,DNDL,@temp_result);
        END IF;
		
		    IF s_nos = "4"
		    THEN
		        IF (Replaced_Queryso is Null or Replaced_Queryso ='' )
			        THEN
					 
					    SET @temp_result = @temp_result;
						
						ELSE 
	                    SET @sql_stmt = CONCAT('SELECT Convert(format(Count(1),","),CHAR)  INTO @temp_result 
													FROM V_CLM_Customer_One_View 
													WHERE  ',vSQLCondition,'
													
													AND EXISTS(',@SVOT_Condition_Region,')');
	                    
	                    /*Select @sql_stmt;*/
	                    PREPARE stmt FROM @sql_stmt;
	                    EXECUTE stmt; 
	                    DEALLOCATE PREPARE stmt;
				 END IF;


	        
		INSERT INTO TEMP_Result_iter (Lifecycle_NAME,Campaign_NAME,Regions_NAME,Replaced_Query_NAME,SVOT_Condition_NAME,Valid_Mobile_NAME,Valid_Email_NAME,DNDL_NAME,Message_NAME)
					VALUES (Lifecycle,Campaign,Regions,Replaced_Queryso,SVOT_Condition,Valid_Mobile,Valid_Email,DNDL,@temp_result);
        END IF;
		

		
		IF s_nos = "5"
		    THEN
		        IF SVOT_Condition IS  NULL OR SVOT_Condition =''
			        THEN
					
					    SET @temp_result = @temp_result;
						
						ELSE 
	                    SET @sql_stmt = CONCAT('SELECT Convert(format(Count(1),","),CHAR) INTO @temp_result 
													FROM V_CLM_Customer_One_View 
													WHERE  ',vSQLCondition,' 
														AND  EXISTS(',@Advance_SVOT_Condition,')
														
														AND EXISTS(',@SVOT_Condition_Region,')
	
												
												');
	                    
	                   Select @sql_stmt;
	                    PREPARE stmt FROM @sql_stmt;
	                    EXECUTE stmt; 
	                    DEALLOCATE PREPARE stmt;
				 END IF;


	        
		INSERT INTO TEMP_Result_iter (Lifecycle_NAME,Campaign_NAME,Regions_NAME,Replaced_Query_NAME,SVOT_Condition_NAME,Valid_Mobile_NAME,Valid_Email_NAME,DNDL_NAME,Message_NAME)
					VALUES (Lifecycle,Campaign,Regions,Replaced_Queryso,SVOT_Condition,Valid_Mobile,Valid_Email,DNDL,@temp_result);
        END IF;	
		
		
		 IF s_nos = "6"
		    THEN
		        IF Valid_Mobile = "NO" 
			        THEN
					
					    SET @temp_result = @temp_result;
						
						ELSE 
	                    SET @sql_stmt = CONCAT('SELECT Convert(format(Count(1),","),CHAR)  INTO @temp_result 
													FROM V_CLM_Customer_One_View 
													WHERE  ',vSQLCondition,' 
													AND EXISTS( ',@SVOT_Condition_Valid_Mobile,')
													AND EXISTS(',@Advance_SVOT_Condition,')
													
													AND EXISTS(',@SVOT_Condition_Region,')
	
												
												');
	                    
	                   Select @sql_stmt;
	                    PREPARE stmt FROM @sql_stmt;
	                    EXECUTE stmt; 
	                    DEALLOCATE PREPARE stmt;
				 END IF;


	        
		INSERT INTO TEMP_Result_iter (Lifecycle_NAME,Campaign_NAME,Regions_NAME,Replaced_Query_NAME,SVOT_Condition_NAME,Valid_Mobile_NAME,Valid_Email_NAME,DNDL_NAME,Message_NAME)
					VALUES (Lifecycle,Campaign,Regions,Replaced_Queryso,SVOT_Condition,Valid_Mobile,Valid_Email,DNDL,@temp_result);
        END IF;
		
		IF s_nos = "7"
		    THEN
		        IF Valid_Email= "NO" 
			        THEN
					
					    SET @temp_result = @temp_result;
						
						ELSE 
	                    SET @sql_stmt = CONCAT('SELECT Convert(format(Count(1),","),CHAR)  INTO @temp_result 
													FROM V_CLM_Customer_One_View 
													WHERE  ',vSQLCondition,' 
													AND EXISTS( ',@SVOT_Condition_Valid_Email,')
													AND EXISTS(',@Advance_SVOT_Condition,')
													
													AND EXISTS(',@SVOT_Condition_Region,')
	
												
												');
	                    
	                   Select @sql_stmt;
	                    PREPARE stmt FROM @sql_stmt;
	                    EXECUTE stmt; 
	                    DEALLOCATE PREPARE stmt;
				 END IF;


	        
		INSERT INTO TEMP_Result_iter (Lifecycle_NAME,Campaign_NAME,Regions_NAME,Replaced_Query_NAME,SVOT_Condition_NAME,Valid_Mobile_NAME,Valid_Email_NAME,DNDL_NAME,Message_NAME)
					VALUES (Lifecycle,Campaign,Regions,Replaced_Queryso,SVOT_Condition,Valid_Mobile,Valid_Email,DNDL,@temp_result);
        END IF;

		

		
		IF s_nos = "8"
		    THEN
		        IF DNDL= "NO" 
			        THEN
					
					    SET @temp_result = @temp_result;
						
						ELSE 
	                    SET @sql_stmt = CONCAT('SELECT Convert(format(Count(1),","),CHAR)  INTO  @validmobilednd 
													FROM V_CLM_Customer_One_View 
													WHERE  ',vSQLCondition,' 
													AND EXISTS(',@SVOT_Condition_Valid_Mobile,')
													AND EXISTS(',@SVOT_Condition_DND,')
													AND EXISTS(',@Advance_SVOT_Condition,')
													
													AND EXISTS(',@SVOT_Condition_Region,')
													
	
												
												');
	                    
	                   Select @sql_stmt;
	                    PREPARE stmt FROM @sql_stmt;
	                    EXECUTE stmt; 
	                    DEALLOCATE PREPARE stmt;
						
						SET @sql_stmt = CONCAT('SELECT Convert(format(Count(1),","),CHAR)  INTO @validemaildnd 
													FROM V_CLM_Customer_One_View 
													WHERE  ',vSQLCondition,' 
													AND EXISTS(',@SVOT_Condition_Valid_Email,')
													AND EXISTS(',@SVOT_Condition_DND,')
													AND EXISTS(',@Advance_SVOT_Condition,')
													
													AND EXISTS(',@SVOT_Condition_Region,')
	
												
												');
	                    
	                --    Select @sql_stmt;
	                    PREPARE stmt FROM @sql_stmt;
	                    EXECUTE stmt; 
	                    DEALLOCATE PREPARE stmt;
				     END IF;
					 
        INSERT INTO TEMP_Result_iter (Lifecycle_NAME,Campaign_NAME,Regions_NAME,Replaced_Query_NAME,SVOT_Condition_NAME,Valid_Mobile_NAME,DNDL_NAME,validmobilednd)
					VALUES (Lifecycle,Campaign,Regions,Replaced_Queryso,SVOT_Condition,Valid_Mobile,DNDL,@validmobilednd);
		 INSERT INTO TEMP_Result_iter (Lifecycle_NAME,Campaign_NAME,Regions_NAME,Replaced_Query_NAME,SVOT_Condition_NAME,Valid_Email_NAME,DNDL_NAME,validemaildnd)
					VALUES (Lifecycle,Campaign,Regions,Replaced_Queryso,SVOT_Condition,Valid_Email,DNDL,@validemaildnd);

	        
		
        END IF;
		 
		 IF s_nos = "8" THEN
											 LEAVE new_loop_1;
									END IF;
			
			
		
				    SELECT  @temp_result;
	
	                SET @Lifecycle_SegmentReplacedQuery = NULL;
	                SET @Region_SegmentReplacedQuery  = NULL;
                    SET @Campaign_SegmentReplacedQuery = NULL;
				    SET @Advance_SVOT_Condition = NULL;
					SET Replaced_Queryso  = NULL;
					SET @Replaced_Query  = NULL;
					
					
			
					
					 
	
			end LOOP ;
						  
			end;		
	close cur1;
	END;
	
	   set sql_safe_updates=0;
       delete from  TEMP_Result_iter where SNO in(
  
     select SNO from(select *, row_number() 
    over(partition by Lifecycle , Campaign,Regions,Replaced_Query, SVOT_Condition,Valid_Mobile,Valid_Email,DNDs,Message_NAME)
    as rn from( select SNO,
	case  when Lifecycle_NAME = 0 then "" else "Lifecycle" end as Lifecycle,
	case  when Campaign_NAME = 0 then "" else "Campaign" end as Campaign,
	case  when Regions_NAME = 0 then "" else "Regions" end as Regions ,
	case  when Replaced_Query_NAME = "0" then "" else "Replaced_Query" end as Replaced_Query ,
	case  when SVOT_Condition_NAME = "0" then "" else "SVOT_Condition" end as SVOT_Condition,
	case  when Valid_Mobile_NAME = "YES" then "Valid_Mobile" else "" end as Valid_Mobile,
	case  when Valid_Email_NAME = "YES" then "Valid_Email" else "" end as Valid_Email,
	case  when DNDL_NAME = "YES" then "DND" else "" end as DNDs ,
    ifnull(Message_NAME,"") as Message_NAME ,
	case  when validmobilednd is null then "" else validmobilednd end as validmobilednd,
	case  when validemaildnd is null then "" else validemaildnd end as validemaildnd
	 from  TEMP_Result_iter ) d1) d3  where rn>1);
	 
	 
	  SET @sql_stmt = CONCAT('create or replace view coverage as (select replace(d2," ","") as stat from(
    SELECT CONCAT(Lifecycle,
	Case when Campaign ="" or Campaign is null then ""  else concat("+",Campaign) end,
	Case when Regions ="" or Regions is null then ""  else concat("+",Regions) end,
	Case when Replaced_Query ="" or Replaced_Query is null then ""  else concat("+",Replaced_Query) end,
	Case when SVOT_Condition ="" or SVOT_Condition is null then ""  else concat("+",SVOT_Condition) end,
	Case when Valid_Mobile ="" or Valid_Mobile is null then ""  else concat("+",Valid_Mobile) end,
	Case when Valid_Email ="" or Valid_Email is null then ""  else concat("+",Valid_Email) end,DNDs,"  = ",
    Message_NAME," ",validmobilednd,"  ",validemaildnd) as d2   FROM
	(
    select 
	case  when Lifecycle_NAME = "0" then "" else "Lifecycle" end as Lifecycle,
	case  when Campaign_NAME = "0" then "" else "Campaign" end as Campaign,
	case  when Regions_NAME = "0" then "" else "Regions" end as Regions ,
	case  when Replaced_Query_NAME = "0" then "" else "Replaced_Query" end as Replaced_Query ,
	case  when SVOT_Condition_NAME = "0" then "" else "SVOT_Condition" end as SVOT_Condition,
	case  when Valid_Mobile_NAME = "YES" then "Valid_Mobile" else "" end as Valid_Mobile,
	case  when Valid_Email_NAME = "YES" then "Valid_Email" else "" end as Valid_Email,
	case  when DNDL_NAME = "YES" then "DND" else "" end as DNDs ,
    ifnull(Message_NAME,"") as Message_NAME ,
	case  when validmobilednd is null then "" else validmobilednd end as validmobilednd,
	case  when validemaildnd is null then "" else validemaildnd end as validemaildnd
	 from  TEMP_Result_iter 
     ) D1
     ) d3)' );
						
					Select @sql_stmt;
	                PREPARE stmt FROM @sql_stmt;
	                EXECUTE stmt; 
	                DEALLOCATE PREPARE stmt;
		
				 drop table if exists Coverage_view;
                 CREATE TABLE Coverage_view ( SNO INT NOT NULL  AUTO_INCREMENT, stat varchar(100),PRIMARY KEY(SNO)); 
			                               
                 insert ignore into Coverage_view(stat) (select * from coverage);
                                  
                        
                        
    /*emaildnd*/
                 select substring_index(stat,"=",-1) into @emaildnd from Coverage_view where SNO =(select max(SNO) from Coverage_view);
 /*mobilednd*/
                 select substring_index(stat,"=",-1) into @mobilednd from Coverage_view where SNO =(select max(SNO)-1 from Coverage_view) ;
											
					
	Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Coverage",total_count,"Mobile_Coverage",@mobilednd,"Email_Coverage",@emaildnd) )),"]") into @temp_result from
	                             (select replace(replace(group_concat(replace(stat,",","|")),",","<br>"),"|",",") as total_count from coverage)d1 ;') ;
   
            SELECT @Query_String;
            PREPARE statement from @Query_String;
            Execute statement;
            Deallocate PREPARE statement; 
		
			
			
			
	
			
   SET out_result_json = @temp_result;
	
	

END$$
DELIMITER ;
