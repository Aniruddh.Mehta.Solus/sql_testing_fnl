DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_insights_conversion_metrics_initialize`(IN vBatchSize BIGINT)
BEGIN
DECLARE vStart_Cnt BIGINT DEFAULT 0;
DECLARE vEnd_Cnt BIGINT DEFAULT 0;
DECLARE vRec_Cnt BIGINT DEFAULT 0;

DELETE from log_solus where module = 'S_dashboard_insights_conversion_metrics';
SET SQL_SAFE_UPDATES=0;

    SELECT `Value` into @Online_Store from M_Config where `Name` = 'Online_Store_Id';
        
        SELECT @Online_Store;

SET @Start_Date = date_format(date_sub(concat(date_format(current_date(),"%Y%m"),'01'), INTERVAL 12 MONTH),"%Y%m");
SET @End_Date = date_format(current_date(),"%Y%m");

SELECT 
    Customer_Id
INTO vRec_Cnt FROM
    Event_Attribution_History
ORDER BY Customer_Id DESC
LIMIT 1;
	SET vStart_Cnt=0;
	SET vEnd_Cnt=0; 
LOOP_ALL_CUSTOMERS: LOOP
	SET vEnd_Cnt = vStart_Cnt + vBatchSize;
	SELECT vStart_Cnt, vEnd_Cnt, CURRENT_TIMESTAMP();
    IF vStart_Cnt  >= vRec_Cnt THEN
		LEAVE LOOP_ALL_CUSTOMERS;
	END IF;  
INSERT INTO log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_insights_conversion_metrics',vStart_Cnt,vEnd_Cnt,'Loading Data for all the Customers');

INSERT INTO Dashboard_Insights_Conversions
(
`Customer_Id`,
`Bill_Details_ID`,
`Bill_Header_ID`,
`Event_Execution_ID`,
`Event_ID`,
`Event_Execution_Date_ID`,
`EE_Date`,
`Event_Execution_Month`,
`Bill_Date`,
`Product_Id`,
`Sale_Net_Val`,
`Sale_Disc_Val`,
`Bill_Total_Val`,
`Bill_Disc`,
`Sale_Qty`
)
SELECT
`Customer_Id`,
`Bill_Details_ID`,
`Bill_Header_ID`,
`Event_Execution_ID`,
`Event_ID`,
`Event_Execution_Date_ID`,
date_format(`Event_Execution_Date_ID`,"%Y-%m-%d") AS EE_Date,
date_format(`Event_Execution_Date_ID`,"%Y%m") AS Event_Execution_Month,
`Bill_Date`,
`Product_Id`,
`Sale_Net_Val`,
`Sale_Disc_Val`,
`Bill_Total_Val`,
`Bill_Disc`,
`Sale_Qty`
FROM Event_Attribution_History
WHERE In_Control_Group IS NULL
AND Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt;


UPDATE Dashboard_Insights_Conversions A, CDM_Bill_Details B
SET A.Store_Id = B.Store_Id
WHERE A.Customer_Id = B.Customer_Id
AND A.Bill_Details_Id = B.Bill_Details_Id
AND A.Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt;

UPDATE Dashboard_Insights_Conversions A, CLM_Segment B
SET A.Segment = B.CLMSegmentName
WHERE A.Segment_Id = B.CLMSegmentId
AND A.Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt;

UPDATE Dashboard_Insights_Conversions A, Event_Master B
SET A.Theme = B.CampaignThemeName
WHERE A.Event_ID = B.ID
AND A.Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt;

UPDATE Dashboard_Insights_Conversions A, Event_Master B
SET A.Campaign = B.CampaignName
WHERE A.Event_ID = B.ID
AND A.Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt;

UPDATE Dashboard_Insights_Conversions A, 
(
select EventId, B.ChannelId, ChannelName
FROM CLM_Campaign_Events A, CLM_Channel B
WHERE A.ChannelId = B.ChannelId
) AS B
SET A.`Channel` = B.ChannelName
WHERE A.Event_ID = B.EventId
AND A.Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt;

UPDATE Dashboard_Insights_Conversions A, Event_Master_UI B
SET A.Category = Type_Of_Event
WHERE A.Event_ID = B.Event_ID
AND A.Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt;

UPDATE Dashboard_Insights_Conversions A, 
(
select EventId, B.CampTriggerId, CampTriggerName
FROM CLM_Campaign_Events A, CLM_Campaign_Trigger B
WHERE A.CampTriggerId = B.CampTriggerId
) AS B
SET A.`Trigger` = B.CampTriggerName
WHERE A.Event_ID = B.EventId
AND A.Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt;

UPDATE Dashboard_Insights_Conversions
SET Transaction_Mode = CASE WHEN Store_Id = @Online_Store THEN 'Online' ELSE 'Offline' END
WHERE Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt;

UPDATE Dashboard_Insights_Conversions A,
(
select Product_Id, LOV_Key
FROM CDM_LOV_Master A, CDM_Product_Master B
WHERE B.Cat_LOV_Id = A.LOV_id
) B
SET A.Product_Category = B.LOV_Key
WHERE A.Product_Id=B.Product_Id
AND A.Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt;

UPDATE Dashboard_Insights_Conversions A, CDM_Product_Master B
SET Product = B.Product_Name
WHERE A.Product_Id=B.Product_Id
AND A.Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt;

UPDATE Dashboard_Insights_Conversions A, CDM_Store_Master B
SET Store = B.Store_Name
WHERE A.Store_Id=B.Store_Id
AND A.Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt;

SET vStart_Cnt = vEnd_Cnt;
END LOOP LOOP_ALL_CUSTOMERS;

END$$
DELIMITER ;
