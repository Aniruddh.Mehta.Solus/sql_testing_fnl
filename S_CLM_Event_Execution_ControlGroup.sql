
CREATE OR REPLACE PROCEDURE `S_CLM_Event_Execution_ControlGroup`(IN Event_ID_cur1 BIGINT(20),IN Event_Execution_Date_ID VARCHAR(64),IN vStart_CntCust BIGINT(20),IN vEnd_CntCust BIGINT(20))
begin
set @Event_ID_cur1=Event_ID_cur1;
set @Event_Execution_Date_ID=Event_Execution_Date_ID;
set sql_safe_updates=0;


select `Value` into @New_Hold_Out_Flag FROM M_Config where `Name`='New_Control_Group_Mechanism';

IF IFNULL(@New_Hold_Out_Flag,'N')='Y' THEN	
	select Event_Response_Days into @reponse_days from Event_Master where ID = @Event_ID_cur1;
    SET @reponse_days = ifnull(@reponse_days,7);
	
	UPDATE Event_Execution ee, Hold_Out ho 
	SET ee.In_Control_Group = "Y",ee.Is_Target = "Y"
	WHERE ee.Customer_Id = ho.Customer_Id 
	AND ho.Long_Term_Insert_Date is not NULL
	AND ee.Event_Id=@Event_ID_cur1
    and ee.Customer_Id between vStart_CntCust and vEnd_CntCust;

	UPDATE Event_Execution ee, Hold_Out ho 
	SET ee.In_Event_Control_Group = "Y",ee.Is_Target = "Y" 
	WHERE ee.Customer_Id = ho.Customer_Id 
	AND ho.Short_Term_Insert_Date is not NULL
	AND ee.Event_Id=@Event_ID_cur1
    and ee.Customer_Id between vStart_CntCust and vEnd_CntCust;
    
    UPDATE Event_Execution ee, Hold_Out ho
    SET ee.LT_Control_Covers_Response_Days = 'Y'
    WHERE ee.Customer_Id = ho.Customer_Id
    AND ee.In_Control_Group='Y'
    and ho.Long_Term_Exit_Date>=date_add(ee.event_Execution_Date_ID,interval @reponse_days day)
    and ee.Customer_Id between vStart_CntCust and vEnd_CntCust;
    
    UPDATE Event_Execution ee, Hold_Out ho
    SET ee.ST_Control_Covers_Response_Days = 'Y'
    WHERE ee.Customer_Id = ho.Customer_Id
    AND ee.In_Event_Control_Group='Y'
    and ho.Short_Term_Exit_Date>=date_add(ee.event_Execution_Date_ID,interval @reponse_days day)
    and ee.Customer_Id between vStart_CntCust and vEnd_CntCust;
ELSE
	UPDATE Event_Execution ee, Hold_Out ho 
	SET ee.In_Control_Group = "Y",
    ee.Is_Target = "Y",
    ee.LT_Control_Covers_Response_Days="Y",
    ee.ST_Control_Covers_Response_Days="Y"
	WHERE ee.Customer_Id = ho.Customer_Id  
	AND ee.Customer_Id = ho.Customer_Id
	AND ee.Event_Id=@Event_ID_cur1
    and ee.Customer_Id between vStart_CntCust and vEnd_CntCust;
    
	SELECT ChannelName INTO @Event_Channel FROM Event_Master
	WHERE EventId = @Event_ID_cur1;
    
   	UPDATE Event_Execution ee, Hold_Out_Channel ho 
	SET ee.In_Control_Group = "Y",
    ee.Is_Target = "Y",
    ee.LT_Control_Covers_Response_Days="Y",
    ee.ST_Control_Covers_Response_Days="Y",
    ee.In_Event_Control_Group = (CASE 
								WHEN ho.Channel_Name = 'SMS' THEN 'S'
                                WHEN ho.Channel_Name = 'EMAIL' THEN 'E'
                                WHEN ho.Channel_Name = 'PN' THEN 'P'
                                WHEN ho.Channel_Name = 'WhatsApp' THEN 'W'
                                ELSE 'Y'
                                END)
	WHERE ee.Customer_Id = ho.Customer_Id  
	AND ee.Customer_Id = ho.Customer_Id
	AND ee.Event_Id=@Event_ID_cur1
    AND ho.Channel_Name = @Event_Channel
    and ee.Customer_Id between vStart_CntCust and vEnd_CntCust; 
    
    

END IF;

END

