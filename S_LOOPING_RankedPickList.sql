DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_LOOPING_RankedPickList`(IN vBatchSize bigint)
BEGIN
DECLARE vStart_Cnt BIGINT DEFAULT 0;
DECLARE vEnd_Cnt BIGINT DEFAULT 0;
SELECT Customer_Id INTO vStart_Cnt FROM RankedPickList_TODAY ORDER BY Customer_Id ASC LIMIT 1;
SELECT Customer_Id INTO @Rec_Cnt FROM RankedPickList_TODAY ORDER BY Customer_Id DESC LIMIT 1;
set sql_safe_updates=0;
PROCESS_LOOP: LOOP
SET vEnd_Cnt = vStart_Cnt + vBatchSize;
delete from RankedPickList where Customer_Id in ( select Customer_Id from RankedPickList_TODAY where Customer_Id between vStart_Cnt and vEnd_Cnt);
insert ignore into RankedPickList select * from RankedPickList_TODAY where Customer_Id between vStart_Cnt and vEnd_Cnt; 
SET vStart_Cnt = vEnd_Cnt;

IF vStart_Cnt  >= @Rec_Cnt THEN
	LEAVE PROCESS_LOOP;
END IF;        
END LOOP PROCESS_LOOP;  
END$$
DELIMITER ;


