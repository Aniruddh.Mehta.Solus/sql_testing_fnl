DELIMITER $$
CREATE or replace PROCEDURE `S_UI_SolusService`(IN page_name varchar(240), IN in_request_json NVARCHAR(65536), OUT out_result_json json)
BEGIN
	declare temp_id bigint(20);
    declare user_name varchar(240); 
    declare screen_name varchar(400); 
    SET user_name = json_unquote(json_extract(in_request_json,"$.USER"));
    SET screen_name = json_unquote(json_extract(in_request_json,"$.SCREEN"));
	
    create table IF NOT EXISTS log_solus_dashboard like log_solus;
    
	insert into log_solus_dashboard(module,input_json) values (page_name,in_request_json);
    select id into @id from log_solus_dashboard order by id desc limit 1;

    CASE page_name
	WHEN "dashboard_testing" THEN
	begin
		SELECT output_json  into @a from Dashboard_Testing  where ScreenName=screen_name and input_json=in_request_json;
		SET out_result_json=@a;
	end;   
	WHEN "dashboard_insights_movements" THEN
  			begin
                 CALL S_dashboard_insights_SegmentMovement(in_request_json,out_result_json);
			end;   
    WHEN "dashboard_performance_outreaches" THEN
			begin
                 CALL S_dashboard_performance_outreaches(in_request_json,out_result_json);
			end;
	WHEN "admin" THEN
		begin
			CALL S_DIY_UserMgmt(in_request_json,out_result_json);
		end;
	WHEN "dashboard_performance_attribution" THEN
			begin
                 CALL S_dashboard_attribution(in_request_json,out_result_json);
			end;
	WHEN "dashboard_performance_accuracy" THEN
			begin
                 CALL S_dashboard_performance_accuracy(in_request_json,out_result_json);
			end;
	WHEN "dashboard_performance_lift" THEN
			begin
                 CALL S_dashboard_performance_lift(in_request_json,out_result_json);
			end;
	WHEN "dashboard_performance_hardresponse" THEN
			begin
                 CALL S_dashboard_performance_hard_response(in_request_json,out_result_json);
			end;
	WHEN "dashboard_performance_yield" THEN
			begin
                 CALL S_dashboard_performance_yield(in_request_json,out_result_json);
			end;
	WHEN "dashboard_performance_trigger" THEN
			begin
                 CALL S_dashboard_performance_trigger(in_request_json,out_result_json);
			end;
	WHEN "dashboard_performance_goals" THEN
			begin
                 CALL S_dashboard_performance_goals(in_request_json,out_result_json);
			end;
	WHEN "dashboard_performance_monthreport" THEN
			begin
                 CALL S_dashboard_performance_month_report(in_request_json,out_result_json);
			end;
	WHEN "dashboard_performance_funnelreport" THEN
			begin
                 CALL S_dashboard_performance_funnel_report(in_request_json,out_result_json);
			end;
	WHEN "dashboard_performance_dayreport" THEN
			begin
                 CALL S_dashboard_performance_day_report(in_request_json,out_result_json);
			end;
	WHEN "dashboard_performance_liftdetail" THEN
			begin
                 CALL S_dashboard_performance_lift_detail(in_request_json,out_result_json);
			end;
	WHEN "dashboard_insights_business" THEN
			begin
                 CALL S_dashboard_insights_Business(in_request_json,out_result_json);
			end;
            
	WHEN "dashboard_insights_base" THEN
			begin
                 CALL S_dashboard_insights_Base(in_request_json,out_result_json);
			end;
	WHEN "dashboard_insights_segment" THEN
			begin
                 CALL S_dashboard_insights_segment(in_request_json,out_result_json);
			end;
	WHEN "dashboard_insights_monthbase" THEN
			begin
                 CALL S_dashboard_insights_monthbase(in_request_json,out_result_json);
			end;
	WHEN "dashboard_insights_bouncecurve" THEN
			begin
                 CALL S_dashboard_insights_bouncecurve(in_request_json,out_result_json);
			end;
	WHEN "dashboard_insights_customerdistribution" THEN
			begin
                 CALL S_dashboard_insights_customer_distribution(in_request_json,out_result_json);
			end;
            WHEN "dashboard_explore_base" THEN
			begin
                 CALL S_dashboard_explore_Customer(in_request_json,out_result_json);
			end;
            WHEN "dashboard_explore_variable" THEN
			begin
                 CALL S_dashboard_explore_variable(in_request_json,out_result_json);
			end;

	 WHEN "dashboard_performance_campaigns" THEN
			begin
               
                CALL S_dashboard_performance_campaigns(in_request_json,out_result_json);
			end;
            
	WHEN "dashboard_performance" THEN
			begin
              
                CALL S_dashboard_performance(in_request_json,out_result_json);
			end;
            WHEN "dashboard_performance_config" THEN
			begin
              
                CALL S_dashboard_performance_config(in_request_json,out_result_json);
			end;
            
            WHEN "dashboard_version_info" THEN
       
	   begin
                declare IN_SCREEN text;
                declare IN_FILTER_CURRENTMONTH text;
                declare IN_FILTER_MONTHDURATION text;
				declare IN_MEASURE text;
                declare IN_NUMBER_FORMAT text;
				declare IN_AGGREGATION_1 text;
			    declare IN_AGGREGATION_2 text;
                declare IN_AGGREGATION_3 text;
                
                
			    SET IN_SCREEN=json_unquote(json_extract(in_request_json,"$.SCREEN"));
                SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
                SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
                SET IN_MEASURE=json_unquote(json_extract(in_request_json,"$.MEASURE"));
                SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
                SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
                SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
                SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
                
                select Patch_Version,date_format(CreatedDate,'%Y-%m-%d')  into @patch_version,@created_date from mysolus_version where Module IN ( 'CLIENTUSERINTERFACE', 'SOLUS' ) order by Id DESC LIMIT 1;
                select substring_index(`Value`,'_',1) into @clientname from M_Config where `Name` = 'Tenant_Name';
				select count(1) into @noofreportqueriesin7Days from log_solus_dashboard where input_json <> '' and Created_Date > date_sub(current_date(), INTERVAL 7 day);
                select count(1) into @signedin7Days  from log_solus_dashboard where module='LOGIN' and Created_Date > date_sub(current_date(), INTERVAL 7 day);
                select count(1) into @nameduser from DIY_User ;


                
                select @patch_version,@created_date,@clientname,@noofreportqueriesin7Days,@signedin7Days,@nameduser;
                SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object(
                "user", concat("Client: ","',@clientname,'"), 
                "NO_OF_REPORTQUERIES_7DAYS", concat("Report Accessed(7 Day): ","',@noofreportqueriesin7Days,'"),
				"SIGNED_IN_7DAYS", concat("Logged In Instances(7 Day) : ","',@signedin7Days,'"),
                "NAMED_USER", concat("Named User: ","',@nameduser,'"),
                "version", concat("Patch Version: ","',@patch_version,'"),
                "date", concat("Patch Applied Date: ","',@created_date,'")              
                ) )),"]")into @temp_result',';') ;
                
                select @Query_String;
                PREPARE statement from @Query_String;
                Execute statement;
                Deallocate PREPARE statement;   

        --        Set  @temp_result='["Message","SUCCESSFUL"]';
                select @temp_result into out_result_json;
		end;
		
        WHEN "dashboard_performance_Lift_Drill" THEN
        begin
             declare IN_SCREEN text;
                declare IN_FILTER_CURRENTMONTH text;
                declare IN_FILTER_MONTHDURATION text;
				declare IN_MEASURE text;
                declare IN_NUMBER_FORMAT text;
				declare IN_AGGREGATION_1 text;
			    declare IN_AGGREGATION_2 text;
                declare IN_AGGREGATION_3 text;
                declare IN_AGGREGATION_4 text;
                declare IN_AGGREGATION_5 text;
                
                
			    SET IN_SCREEN=json_unquote(json_extract(in_request_json,"$.SCREEN"));
                SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
                SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
                SET IN_MEASURE=json_unquote(json_extract(in_request_json,"$.MEASURE"));
                SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
                SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
                SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
                SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
                SET IN_AGGREGATION_4 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_4"));
                SET IN_AGGREGATION_5 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_5"));
				IF IN_MEASURE = 'TOP_TABLE'
                THEN
                select Value1 into @Incremental_Revenue_Method from UI_Configuration_Global where Config_Name = 'Incremental_Revenue_Method';
    SET @Incremental_Revenue_Method=ifnull(@Incremental_Revenue_Method,'Method2');
    select @Incremental_Revenue_Method;
    IF @Incremental_Revenue_Method = 'Method1'
    THEN
		SET @incr_rev_concat='_By_Day';
        SET @group_cond1='Event_Id,EE_Date';
        SET @mtd_concat='';
	ELSEIF @Incremental_Revenue_Method = 'Method2'
    THEN
		SET @group_cond1='Event_Id,YM';
		SET @incr_rev_concat='';
        SET @mtd_concat='MTD_';
	ELSE
		SET @incr_rev_concat='';
        SET @mtd_concat='';
		SET @incr_Rev = ' round((CASE WHEN SUM(Control_Base) < 0 or SUM(Target_Delivered) = 0 then 0 WHEN (((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*100 < 0 then 0 WHEN SUM(Control_Base)>0 and SUM(Control_Responder)=0 then SUM(Target_Revenue)
					ELSE ifnull((((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*(SUM(Target_Revenue)/SUM(Target_Responder))*(SUM(Target_Delivered)),0) END),0) ';
		SET @group_cond1='YM';
	END IF;
    SELECT @mtd_concat;
   
   SET @sql1=CONCAT('
		CREATE OR REPLACE VIEW CLM_Event_Dashboard_STLT
        as 
		SELECT `CLM_Event_Dashboard`.`Revenue Center`,
			`CLM_Event_Dashboard`.`Segment`,
			`CLM_Event_Dashboard`.`Campaign`,
			`CLM_Event_Dashboard`.`EE_Date`,
			`CLM_Event_Dashboard`.`Event_Execution_Date_ID`,
			`CLM_Event_Dashboard`.`Theme`,
			`CLM_Event_Dashboard`.`Offer`,
			`CLM_Event_Dashboard`.`Channel`,
			`CLM_Event_Dashboard`.`Has_Recommendations`,
			`CLM_Event_Dashboard`.`Event_ID`,
			`CLM_Event_Dashboard`.`Control_Base` as `Control_Base`,
			`CLM_Event_Dashboard`.`Target_Base`,
			`CLM_Event_Dashboard`.`Target_Delivered`,
			`CLM_Event_Dashboard`.`Control_Responder` as `Control_Responder`,
			`CLM_Event_Dashboard`.`Target_Responder`,
			`CLM_Event_Dashboard`.`Target_Revenue`,
			`CLM_Event_Dashboard`.`Target_Discount`,
			`CLM_Event_Dashboard`.`Target_Bills`,
			`CLM_Event_Dashboard`.`Target_Open`,
			`CLM_Event_Dashboard`.`Target_CTA`,
			`CLM_Event_Dashboard`.`Control_Revenue`,
			`CLM_Event_Dashboard`.`Control_Discount`,
			`CLM_Event_Dashboard`.`Control_Bills`,
			`CLM_Event_Dashboard`.`Created_Date`,
			`CLM_Event_Dashboard`.`Total_Customer_Base`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Day`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Month`,
			`CLM_Event_Dashboard`.`YM`,
			`CLM_Event_Dashboard`.`Incremental_Revenue',@incr_rev_concat,'` AS `Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue',@incr_rev_concat,'` AS `MTD_Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Segment',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Segment`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Revenue_Center',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Revenue_Center`
		FROM `CLM_Event_Dashboard`;
        ');
        select @sql1;
		PREPARE statement from @sql1;
		Execute statement;
		Deallocate PREPARE statement;
        
         SET @create_view=CONCAT('CREATE OR REPLACE VIEW V_Dashboard_Month_Report_Temp
		   as
		   select
		   Theme,
		   YM,
		   EE_Date,
		   Event_Id,
		   SUM(`Target_Base`) AS Target_Base,
		   SUM(`Target_Delivered`) AS Target_Delivered,
		   SUM(`Control_Base`) AS Control_Base,
		   SUM(`Target_Responder`) AS Target_Responder,
		   SUM(`Control_Responder`) AS Control_Responder,
		   SUM(Target_Revenue) AS Target_Revenue,
		   MAX(MTD_Incremental_Revenue) AS Incremental_Revenue
		   from CLM_Event_Dashboard_STLT
		   group by ',@group_cond1,'');
		   
			select @create_view;
			PREPARE statement from @create_view;
			Execute statement;
			Deallocate PREPARE statement;
            
		IF @Incremental_Revenue_Method <> 'METHOD3'
        THEN
		select round(SUM(Incremental_Revenue)) into @incr_revenue from V_Dashboard_Month_Report_Temp where YM=IN_FILTER_CURRENTMONTH;
        SET @incr_revenue = concat('"',@incr_revenue,'"');
        ELSE
        set @incr_revenue=concat('round((CASE WHEN SUM(Control_Base) < (Select `Value` from CLM_Response_Config where `Name` = "Control_Group_Min_Control_Base") or SUM(Target_Delivered) = 0 then 0 WHEN (((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*100 < 0 then 0 WHEN SUM(Control_Base)>0 and SUM(Control_Responder)=0 then SUM(Target_Revenue)
					ELSE ifnull((((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*(SUM(Target_Revenue)/SUM(Target_Responder))*(SUM(Target_Delivered)),0) END),0)');
		END IF;
        
					set @SQL1= concat('CREATE OR REPLACE VIEW Z_UI_Lift_Drill_Top_View
					AS
					(SELECT SUM(`Target_Base`) as Target,
					SUM(`Target_Delivered`) AS `Delivered`,
					SUM(`Target_Responder`) AS `TG Response`,
					concat(round(CASE WHEN SUM(`Target_Delivered`) <=0 THEN 0 ELSE (SUM(`Target_Responder`)/SUM(`Target_Delivered`))*100 END,2),"%") AS `TG % Response`,
					concat(round(CASE WHEN SUM(`Control_Base`) <=0 THEN 0 ELSE (SUM(`Control_Responder`)/SUM(`Control_Base`))*100 END,2),"%") AS `Control Resp`,
					concat(round((CASE WHEN SUM(`Control_Base`) < (Select `Value` from CLM_Response_Config where `Name` = "Control_Group_Min_Control_Base") or SUM(Target_Delivered) = 0 then 0
					WHEN SUM(Control_Base) > 0 and SUM(Control_Responder) <=0 and SUM(Target_Responder) > 0 THEN 100
					WHEN ((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base)))/(SUM(Control_Responder)/SUM(Control_Base)) < 0 then 0 WHEN (SUM(Control_Responder)/SUM(Control_Base)) = 0 THEN ((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base)))*100 else ifnull((((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base)))/(SUM(Control_Responder)/SUM(Control_Base)))*100,0) END),2),"%") AS `% Lift`,
					concat(round(CASE WHEN (CASE WHEN SUM(`Target_Delivered`) <=0 THEN 0 
                    ELSE (SUM(`Target_Responder`)/SUM(`Target_Delivered`))*100 END)-(CASE WHEN SUM(`Control_Base`) <=0 THEN 0 ELSE (SUM(`Control_Responder`)/SUM(`Control_Base`))*100 END)<0 THEN 0 ELSE
(CASE WHEN SUM(`Target_Delivered`) <=0 THEN 0 
                    ELSE (SUM(`Target_Responder`)/SUM(`Target_Delivered`))*100 END)-(CASE WHEN SUM(`Control_Base`) <=0 THEN 0 ELSE (SUM(`Control_Responder`)/SUM(`Control_Base`))*100 END)END,2),"%") AS `Response Lift`,
					round(CASE WHEN SUM(Target_Responder) <= 0 then 0 else
					SUM(Target_Revenue)/SUM(Target_Responder) END,0) AS `ABV`,
					',@incr_revenue,' AS `Rev Lift`,
					concat(CASE WHEN SUM(Target_Revenue) = 0 THEN 0 ELSE round(((CASE WHEN SUM(Control_Base) < (Select `Value` from CLM_Response_Config where `Name` = "Control_Group_Min_Control_Base") or SUM(Target_Delivered) = 0 then 0 WHEN (((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*100 < 0 then 0 WHEN SUM(Control_Base)>0 and SUM(Control_Responder)=0 then SUM(Target_Revenue)
					ELSE ifnull((((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*(SUM(Target_Revenue)/SUM(Target_Responder))*(SUM(Target_Delivered)),0) END)/(SUM(Target_Revenue))*100),2) END,"%") AS `Lift as % of TG REV`
					from CLM_Event_Dashboard where date_format(EE_Date,"%Y%m")=',IN_FILTER_CURRENTMONTH,'
					)');
                    PREPARE statement from @SQL1;
					Execute statement;
					Deallocate PREPARE statement;
					SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object(" ", "ALL","Target",`Target`,"Delivered",`Delivered`,"TG Response",`TG Response`,"TG % Response",`TG % Response`,"Control Resp",`Control Resp`,"% Lift",`% Lift`,"Response Lift",`Response Lift`,"ABV",`ABV`,"Rev Lift",`Rev Lift`,"Lift as % of TG Rev",`Lift as % of TG REV`) )),"]")into @temp_result from Z_UI_Lift_Drill_Top_View ',';') ;
				ELSEIF IN_MEASURE = 'BOTTOM_TABLE'
                THEN
					SET IN_AGGREGATION_1= case when IN_AGGREGATION_1 = "Revenue Center" THEN "`Revenue Center`" 
												when IN_AGGREGATION_1 = "Daily" THEN "EE_Date" 
                                                WHEN IN_AGGREGATION_1 = "Hard Response" THEN "CASE WHEN Has_Recommendations = 'Y' THEN 'Uses Reco' ELSE 'No Reco' END"
                                                when IN_AGGREGATION_1 = "Weekly" THEN " ceiling((day(Event_Execution_Date_ID) - 
(6 - weekday(date_format(Event_Execution_Date_ID,'%Y-%m-01'))))/7) 
+ case when 6 - weekday(date_format(Event_Execution_Date_ID,'%Y-%m-01'))> 0 
then 1 else 0 end "
                                                else IN_AGGREGATION_1 END;
					SET IN_AGGREGATION_2= case when IN_AGGREGATION_2 = "Revenue Center" THEN "`Revenue Center`" 
												when IN_AGGREGATION_2 = "Daily" THEN "EE_Date" 
                                                 WHEN IN_AGGREGATION_2 = "Hard Response" THEN "CASE WHEN Has_Recommendations = 'Y' THEN 'Uses Reco' ELSE 'No Reco' END"
                                                when IN_AGGREGATION_2 = "Weekly" THEN " ceiling((day(Event_Execution_Date_ID) - 
(6 - weekday(date_format(Event_Execution_Date_ID,'%Y-%m-01'))))/7) 
+ case when 6 - weekday(date_format(Event_Execution_Date_ID,'%Y-%m-01'))> 0 
then 1 else 0 end "
                                                else IN_AGGREGATION_2 END;
					
                    SET IN_AGGREGATION_3= case when IN_AGGREGATION_3 = "Revenue Center" THEN "`Revenue Center`" 
												when IN_AGGREGATION_3 = "Daily" THEN "EE_Date" 
                                                 WHEN IN_AGGREGATION_3 = "Hard Response" THEN "CASE WHEN Has_Recommendations = 'Y' THEN 'Uses Reco' ELSE 'No Reco' END"
                                                when IN_AGGREGATION_3 = "Weekly" THEN " ceiling((day(Event_Execution_Date_ID) - 
(6 - weekday(date_format(Event_Execution_Date_ID,'%Y-%m-01'))))/7) 
+ case when 6 - weekday(date_format(Event_Execution_Date_ID,'%Y-%m-01'))> 0 
then 1 else 0 end "
                                                else IN_AGGREGATION_3 END;
					SET IN_AGGREGATION_4= case when IN_AGGREGATION_4 = "Revenue Center" THEN "`Revenue Center`" 
												when IN_AGGREGATION_4 = "Daily" THEN "EE_Date" 
                                                 WHEN IN_AGGREGATION_4 = "Hard Response" THEN "CASE WHEN Has_Recommendations = 'Y' THEN 'Uses Reco' ELSE 'No Reco' END"
                                                when IN_AGGREGATION_4 = "Weekly" THEN " ceiling((day(Event_Execution_Date_ID) - 
(6 - weekday(date_format(Event_Execution_Date_ID,'%Y-%m-01'))))/7) 
+ case when 6 - weekday(date_format(Event_Execution_Date_ID,'%Y-%m-01'))> 0 
then 1 else 0 end "
                                                else IN_AGGREGATION_4 END;
					SET IN_AGGREGATION_5= case when IN_AGGREGATION_5 = "Revenue Center" THEN "`Revenue Center`" 
												when IN_AGGREGATION_5 = "Daily" THEN "EE_Date" 
                                                 WHEN IN_AGGREGATION_5 = "Hard Response" THEN "CASE WHEN Has_Recommendations = 'Y' THEN 'Uses Reco' ELSE 'No Reco' END"
                                                when IN_AGGREGATION_5 = "Weekly" THEN " ceiling((day(Event_Execution_Date_ID) - 
(6 - weekday(date_format(Event_Execution_Date_ID,'%Y-%m-01'))))/7) 
+ case when 6 - weekday(date_format(Event_Execution_Date_ID,'%Y-%m-01'))> 0 
then 1 else 0 end "
                                                else IN_AGGREGATION_5 END;
					set @SQL1= concat('CREATE OR REPLACE VIEW Z_UI_Lift_Drill_Bottom_View
					AS
					(SELECT ifnull(',IN_AGGREGATION_1,',"") AS G1,
					ifnull(',IN_AGGREGATION_2,',"") AS G2,
					ifnull(',IN_AGGREGATION_3,',"") AS G3,
					ifnull(',IN_AGGREGATION_4,',"") AS G4,
					ifnull(',IN_AGGREGATION_5,',"") AS G5,
                    SUM(`Target_Base`) as Target,
					SUM(`Target_Delivered`) AS `Delivered`,
					SUM(`Target_Responder`) AS `TG Response`,
					concat(round((CASE WHEN SUM(`Target_Delivered`) <=0 THEN 0 ELSE (SUM(`Target_Responder`)/SUM(`Target_Delivered`))*100 END),2),"%") AS `TG % Response`,
					concat(round((CASE WHEN SUM(`Control_Base`) <=0 THEN 0 ELSE (SUM(`Control_Responder`)/SUM(`Control_Base`))*100 END),2),"%") AS `Control Resp`,
					concat(round((CASE WHEN SUM(`Control_Base`) < (Select `Value` from CLM_Response_Config where `Name` = "Control_Group_Min_Control_Base") or SUM(Target_Delivered) = 0 then 0
					WHEN SUM(Control_Base) > 0 and SUM(Control_Responder) <=0 and SUM(Target_Responder) > 0 THEN 100
					WHEN ((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base)))/(SUM(Control_Responder)/SUM(Control_Base)) < 0 then 0 WHEN (SUM(Control_Responder)/SUM(Control_Base)) = 0 THEN ((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base)))*100 else ifnull((((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base)))/(SUM(Control_Responder)/SUM(Control_Base)))*100,0) END),2),"%") AS `% Lift`,
					concat(round(CASE WHEN (CASE WHEN SUM(`Target_Delivered`) <=0 THEN 0 
                    ELSE (SUM(`Target_Responder`)/SUM(`Target_Delivered`))*100 END)-(CASE WHEN SUM(`Control_Base`) <=0 THEN 0 ELSE (SUM(`Control_Responder`)/SUM(`Control_Base`))*100 END)<0 THEN 0 ELSE
(CASE WHEN SUM(`Target_Delivered`) <=0 THEN 0 
                    ELSE (SUM(`Target_Responder`)/SUM(`Target_Delivered`))*100 END)-(CASE WHEN SUM(`Control_Base`) <=0 THEN 0 ELSE (SUM(`Control_Responder`)/SUM(`Control_Base`))*100 END)END,2),"%") AS `Response Lift`,
					round(CASE WHEN SUM(Target_Responder) <= 0 then 0 else
					SUM(Target_Revenue)/SUM(Target_Responder) END,0) AS `ABV`,
					round((CASE WHEN SUM(Control_Base) < (Select `Value` from CLM_Response_Config where `Name` = "Control_Group_Min_Control_Base") or SUM(Target_Delivered) = 0 then 0 WHEN (((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*100 < 0 then 0 WHEN SUM(Control_Base)>0 and SUM(Control_Responder)=0 then SUM(Target_Revenue)
					ELSE ifnull((((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*(SUM(Target_Revenue)/SUM(Target_Responder))*(SUM(Target_Delivered)),0) END),0) AS `Rev Lift`,
					concat(CASE WHEN SUM(Target_Revenue) = 0 THEN 0 ELSE round(((CASE WHEN SUM(Control_Base) < (Select `Value` from CLM_Response_Config where `Name` = "Control_Group_Min_Control_Base") or SUM(Target_Delivered) = 0 then 0 WHEN (((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*100 < 0 then 0 WHEN SUM(Control_Base)>0 and SUM(Control_Responder)=0 then SUM(Target_Revenue)
					ELSE ifnull((((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*(SUM(Target_Revenue)/SUM(Target_Responder))*(SUM(Target_Delivered)),0) END)/(SUM(Target_Revenue))*100),2) END,"%") AS `Lift as % of TG REV`
					from CLM_Event_Dashboard where date_format(EE_Date,"%Y%m")=',IN_FILTER_CURRENTMONTH,'
					 GROUP BY 1,2,3,4,5
                     having SUM(Target_Base)>0)');
                     SELECT @SQL1;
                    PREPARE statement from @SQL1;
					Execute statement;
					Deallocate PREPARE statement;
                    SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
					SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
					SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
					SET IN_AGGREGATION_4 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_4"));
					SET IN_AGGREGATION_5 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_5"));
					SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("',IN_AGGREGATION_1,'",G1,"',IN_AGGREGATION_2,'",G2,"',IN_AGGREGATION_3,'",G3,"',IN_AGGREGATION_4,'",G4,"',IN_AGGREGATION_5,'",G5,"Target",`Target`,"Delivered",`Delivered`,"TG Response",`TG Response`,"TG % Response",`TG % Response`,"Control Resp",`Control Resp`,"% Lift",`% Lift`,"Response Lift",`Response Lift`,"ABV",`ABV`,"Rev. Lift",`Rev Lift`,"Lift as % of TG Rev",`Lift as % of TG REV`) )),"]")into @temp_result from Z_UI_Lift_Drill_Bottom_View ',';') ;
                END IF;
			   select @Query_String;
               PREPARE statement from @Query_String;
                Execute statement;
                Deallocate PREPARE statement; 
               select @temp_result into out_result_json;
			
                end;
        WHEN "dashboard_blueprint_campaign" THEN
			begin
                 CALL S_dashboard_blueprint_campaign(in_request_json,out_result_json);
			end;
            
		WHEN "dashboard_blueprint_trigger" THEN
			begin
                 CALL S_dashboard_blueprint_trigger(in_request_json,out_result_json);
			end;
		WHEN "dashboard_blueprint_creative" THEN
			begin
                 CALL S_dashboard_blueprint_creative(in_request_json,out_result_json);
			end;
        WHEN "dashboard_blueprint_theme" THEN
			begin
                 CALL S_dashboard_blueprint_theme(in_request_json,out_result_json);
		end;
        
        WHEN "dashboard_blueprint_customer_view_config" THEN
			begin
                 CALL S_dashboard_blueprint_customer_view_config(in_request_json,out_result_json);
		end;
		
        WHEN "dashboard_campaign_list" THEN
			begin
                 CALL S_UI_Campaign_List(in_request_json,out_result_json);
		end;
        
        WHEN "dashboard_campaign_creation" THEN
			begin
                 CALL S_UI_Campaign_Event_Creation(in_request_json,out_result_json);
		end;
		WHEN "dashboard_campaign_details" THEN
			begin
                 CALL S_UI_Campaign_Details(in_request_json,out_result_json);
		end;
		WHEN "dashboard_campaign_check_coverage" THEN
			begin
                 CALL S_UI_Campaign_Check_Coverage(in_request_json,out_result_json);
		end;
		WHEN "dashboard_campaign_schedule" THEN
			begin
                 CALL S_UI_Campaign_Schedule(in_request_json,out_result_json);
		end;
        
        WHEN "dashboard_campaign_reschedule" THEN
			begin
                 CALL S_UI_Campaign_ReSchedule(in_request_json,out_result_json);
		end;
		
		WHEN "dashboard_brew_station" THEN
			begin
                 CALL S_UI_Campaign_Creation(in_request_json,out_result_json);
		end;
		
		WHEN "dashboard_brew_bulk_update" THEN
			begin
                 CALL S_UI_Campaign_Bulk_Update(in_request_json,out_result_json);
		end;
		
		WHEN "dashboard_campaign_config" THEN
			begin
                 CALL S_UI_Campaign_Configuration(in_request_json,out_result_json);
		end;
        
          WHEN "instant_campaign_list" THEN
			begin
                 CALL S_UI_Campaign_List(in_request_json,out_result_json);
		end;
          WHEN "instant_campaign_creation" THEN
			begin
                 CALL S_UI_Instant_Campaign_Creation(in_request_json,out_result_json);
		end;
         WHEN "instant_campaign_coverage" THEN
			begin
                 CALL S_UI_Instant_Campaign_Check_Coverage(in_request_json,out_result_json);
		end;
         WHEN "instant_campaign_status" THEN
			begin
                 CALL S_UI_Instant_Campaign_Status(in_request_json,out_result_json);
		end;
          WHEN "instant_campaign_readytosend_list" THEN
			begin
                 CALL S_UI_Instant_Campaign_List(in_request_json,out_result_json);
		end;
        WHEN "instant_campaign_segment" THEN
			begin
                 CALL S_UI_Instant_Campaign_Segment(in_request_json,out_result_json);
		end;
          WHEN "instant_campaign_adapter" THEN
			begin
                 CALL S_UI_Instant_Campaign_Adapter(in_request_json,out_result_json);
		end;
         WHEN "instant_brew_segment" THEN
			begin
                 CALL S_UI_Instant_Brew_Segment(in_request_json,out_result_json);
		end;
         WHEN "instant_campaign_static_url_generator" THEN
			begin
                 CALL S_UI_static_so2_generator(in_request_json,out_result_json);
		end;
        WHEN "trigger_bank" THEN
			begin
                 CALL S_UI_Trigger_Bank(in_request_json,out_result_json);
		end;
        
        WHEN "campaign_test_run" THEN
			begin
                 CALL S_UI_Test_Campaign(in_request_json,out_result_json);
		end;
        
        WHEN "campaign_ondemand_run" THEN
			begin
                 CALL S_UI_OnDemand_Campaign(in_request_json,out_result_json);
		end;
        
        
         WHEN "target_audience_campaign_segment" THEN
			begin
                 CALL S_UI_Target_Audience_Campaign_Segment(in_request_json,out_result_json);
		end;
        
         WHEN "dashboard_campaign_list" THEN
			begin
                  CALL S_UI_Campaign_List(in_request_json,out_result_json);
		end;
		
		WHEN "dashboard_campaign_custom_variable" THEN
			begin
                 CALL S_UI_Campaign_Custom_Vairable(in_request_json,out_result_json);
		end;
		 WHEN "dashboard_performance_deepdive" THEN
			begin
                 CALL S_dashboard_campaign_deep_dive(in_request_json,out_result_json);
		end;
        WHEN "dashboard_insights_repeat_cohorts" THEN
			begin
                 CALL S_dashboard_insights_repeat_cohorts(in_request_json,out_result_json);
		end;
        WHEN "dashboard_insights_glue_cohorts" THEN
			begin
                 CALL S_dashboard_insights_glue_cohorts(in_request_json,out_result_json);
		end;
		WHEN "List_Pullout" THEN
			begin
			   CALL S_UI_Customer_List_PullOut(in_request_json,out_result_json);
		end;
        WHEN "dashboard_insights_reco_nudge_efficacy" THEN
			begin
                 CALL S_dashboard_recommender_reco_nudge_efficacy(in_request_json,out_result_json);
		end;
        WHEN "dashboard_campaign_microservice_product" THEN
			begin
                 CALL S_UI_Campaign_Microservice_Product(in_request_json,out_result_json);
		end;
        WHEN "dashboard_campaign_funnel_coverage" THEN
			begin
                 CALL S_UI_Campaign_Funnel_Coverage(in_request_json,out_result_json);
		end;
        WHEN "dashboard_insights_trigger_conversions" THEN
			begin
                 CALL S_dashboard_insights_trigger_conversions(in_request_json,out_result_json);
		end;
        WHEN "dashboard_LoyaltyReports_tier" THEN
			begin
                 CALL S_dashboard_loyalty_tier(in_request_json,out_result_json);
		end;
       WHEN "dashboard_LoyaltyReports_mem" THEN
			begin
                 CALL S_dashboard_loyalty_mem(in_request_json,out_result_json);
		end;
        WHEN "dashboard_LoyaltyReports_seg" THEN
			begin
                 CALL S_dashboard_loyalty_seg(in_request_json,out_result_json);
		end;
        WHEN "dashboard_LoyaltyReports_tier_seg" THEN
			begin
                 CALL S_dashboard_loyalty_tier_seg(in_request_json,out_result_json);
		end;
        WHEN "dashboard_LoyaltyReports_category" THEN
			begin
                 CALL S_dashboard_loyalty_category(in_request_json,out_result_json);
		end;
        WHEN "dashboard_LoyaltyReports_region" THEN
			begin
                 CALL S_dashboard_loyalty_region(in_request_json,out_result_json);
		end;
        WHEN "dashboard_LoyaltyReports_leaderboard" THEN
			begin
                 CALL S_dashboard_loyalty_leaderboard(in_request_json,out_result_json);
		end;
        WHEN "dashboard_insights_conversions" THEN
			begin
                 CALL S_dashboard_insights_conversions(in_request_json,out_result_json);
		end;
        WHEN "rcore_status" THEN
			begin
                 CALL S_Get_Rcore_Status(in_request_json,out_result_json);
		end;
        WHEN "S_dashboard_QSR_seg" THEN
			begin
                 CALL S_dashboard_QSR_seg(in_request_json,out_result_json);
		end;
        WHEN "S_dashboard_QSR_reg" THEN
			begin
                 CALL S_dashboard_QSR_reg(in_request_json,out_result_json);
		end;
        WHEN "S_dashboard_QSR_platform" THEN
			begin
                 CALL S_dashboard_QSR_platform(in_request_json,out_result_json);
		end;
        WHEN "S_dashboard_QSR_loc" THEN
			begin
                 CALL S_dashboard_QSR_loc(in_request_json,out_result_json);
		end;
        WHEN "S_dashboard_QSR_DOW" THEN
			begin
                 CALL S_dashboard_QSR_DOW(in_request_json,out_result_json);
		end;
        WHEN "S_dashboard_QSR_Chan" THEN
			begin
                 CALL S_dashboard_QSR_Chan(in_request_json,out_result_json);
		end;
        WHEN "S_dashboard_QSR_Repeat" THEN
			begin
                 CALL S_dashboard_QSR_Repeat(in_request_json,out_result_json);
		end;
        WHEN "UI_dashboard_segments" THEN
			begin
                 CALL S_UI_Dashboard_Segments(in_request_json,out_result_json);
		end;
        WHEN "UI_dashboard_global_config" THEN
			begin
                 CALL S_UI_Dashboard_Global_Configs(in_request_json,out_result_json);
		end;
        WHEN "UI_dashboard_config" THEN
			begin
                 CALL S_dashboard_global_config(in_request_json,out_result_json);
		end;
        WHEN "UI_dashboard_metrics_status" THEN
			begin
                 CALL S_UI_Dashboard_Metrics_Status(in_request_json,out_result_json);
		end;
        WHEN "UI_dashboard_other_config" THEN
			begin
                 CALL S_UI_Dashboard_Banding_Configs(in_request_json,out_result_json);
		end;
        WHEN "S_dashboard_performance_whatif_analysis_One_Attr" THEN
			begin
                 CALL S_dashboard_performance_whatif_analysis_One_Attr(in_request_json,out_result_json);
		end;
        WHEN "S_dashboard_performance_whatif_analysis_N_Attr" THEN
			begin
                 CALL S_dashboard_performance_whatif_analysis_N_Attr(in_request_json,out_result_json);
		end;
        WHEN "S_dashboard_performance_whatif_analysis_Compare_Attr" THEN
			begin
                 CALL S_dashboard_performance_whatif_analysis_Compare_Attr(in_request_json,out_result_json);
		end;
        WHEN "S_dashboard_performance_whatif_analysis_Compare_Creative" THEN
			begin
                 CALL S_dashboard_performance_whatif_analysis_Compare_Creative(in_request_json,out_result_json);
		end;
        WHEN "dashboard_insights_segmentation_scheme" THEN
			begin
                 CALL S_dashboard_insights_segmentation_schemes(in_request_json,out_result_json);
		end;
        WHEN "S_dashboard_performance_whatif_analysis_Attr_Summary" THEN
			begin
                 CALL S_dashboard_performance_whatif_analysis_Attr_Summary(in_request_json,out_result_json);
		end;
        WHEN "S_dashboard_Intelligence_CMAB" THEN
			begin
                 CALL S_dashboard_Intelligence_CMAB(in_request_json,out_result_json);
		end;
        WHEN "S_dashboard_intelligence_propensity" THEN
			begin
                 CALL S_dashboard_intelligence_propensity(in_request_json,out_result_json);
		end;
         WHEN "S_Dashboard_Intelligence_Cooloff" THEN
			begin
                 CALL S_Dashboard_Intelligence_Cooloff(in_request_json,out_result_json);
		end;
         WHEN "S_GetTopNProducts" THEN
			begin
                 CALL S_GetTopNProducts(in_request_json,out_result_json);
		end;
         WHEN "S_Dashboard_practice" THEN
			begin
                 CALL S_Dashboard_practice(in_request_json ,out_result_json);
		end;
         WHEN "S_Dashboard_admin" THEN
			begin
                 CALL S_Dashboard_admin(in_request_json ,out_result_json);
		end;
        WHEN "S_GenerateSolusPrompt" THEN
			begin
                 CALL S_GenerateSolusPrompt(in_request_json ,out_result_json);
		end;
         WHEN "S_Solus_Prompt_Key" THEN
			begin
                 CALL S_Solus_Prompt_Key(in_request_json ,out_result_json);
		end;   
         WHEN "S_ChatGPT_Login" THEN
			begin
                 CALL S_ChatGPT_Login(in_request_json ,out_result_json);
		end;
         WHEN "UI_Calendar" THEN
			begin
                 CALL S_UI_Calendar(in_request_json ,out_result_json);
		end;
         WHEN "S_dashboard_intelligence_MTA" THEN
			begin
                 CALL S_dashboard_intelligence_MTA(in_request_json,out_result_json);
		end;
         WHEN "S_GetTopNProducts" THEN
			begin
                 CALL S_GetTopNProducts(in_request_json,out_result_json);
		end;
         WHEN "S_Dashboard_admin" THEN
			begin
                 CALL S_Dashboard_admin(in_request_json ,out_result_json);
		end;
        WHEN "S_dashboard_performance_Campaign_Segment" THEN
			begin
                 CALL S_dashboard_performance_Campaign_Segment(in_request_json,out_result_json);
		end;
        WHEN "S_UI_Dashboard_Global_Configs" THEN
			begin
                 CALL S_UI_Dashboard_Global_Configs(in_request_json,out_result_json);
		end;
        WHEN "S_UI_Dashboard_Propensity_Configs" THEN
			begin
                 CALL S_UI_Dashboard_Propensity_Configs(in_request_json,out_result_json);
		end;
        WHEN "S_UI_Dashboard_Segment_Sequence" THEN
			begin
                 CALL S_UI_Dashboard_Segment_Sequence(in_request_json,out_result_json);
		end;
        WHEN "S_UI_Dashboard_Response_Attr_Configs" THEN
			begin
                 CALL S_UI_Dashboard_Response_Attr_Configs(in_request_json,out_result_json);
		end;
        WHEN "Dashboard_Home_Page" THEN
			begin
                 CALL S_dashboard_performance_homepage(in_request_json,out_result_json);
		end;
        WHEN "Cross_instance_impact" THEN
			begin
                 CALL S_dashboard_cross_instance_impact(in_request_json,out_result_json);
		end;
        WHEN "Cross_instance_outreachea" THEN
			begin
                 CALL S_dashboard_cross_instance_outreach(in_request_json,out_result_json);
		end;
        WHEN "dashboard_performance_top_triggers" THEN
			begin
                 CALL S_dashboard_performance_top_triggers(in_request_json,out_result_json);
		end;
        WHEN "dashboard_automation" THEN
			begin
                 CALL S_dashboard_metrics_automation(in_request_json,out_result_json); 
		end;
        WHEN "dashboard_Story_Comment" THEN
			begin
                 CALL S_dashboard_Story_Comment(in_request_json,out_result_json);
		end;
        WHEN "dashboard_insights_homepage" THEN
			begin
                 CALL S_dashboard_insights_homepage(in_request_json,out_result_json); 
		end;
        WHEN "integration" THEN
			begin
                 CALL S_UI_Adapter_Details(in_request_json,out_result_json);
		end;
         WHEN "dashboard_Story_Board_Queries" THEN
			begin
                 CALL S_dashboard_Story_Board_Queries(in_request_json,out_result_json);
		end;
        WHEN "dashboard_StoreView" THEN
			begin
                 CALL S_dashboard_insights_StoreView(in_request_json,out_result_json);
		end;
        WHEN 'dashboard_graph_display' THEN 
			begin 
				CALL S_dashboard_graph_display(in_request_json,out_result_json);
        end;
          WHEN 'dashboard_Customer_Reco12' THEN
			begin
                 CALL S_dashboard_Customer_Reco(in_request_json,out_result_json);
		end;
          WHEN 'dashboard_Product_View' THEN
			begin
                 CALL S_dashboard_Product_View(in_request_json,out_result_json);
		end;
        WHEN 'dashboard_Product_View_Repeat' THEN
			begin
                 CALL S_dashboard_insights_ProductView_Repeat(in_request_json,out_result_json);
		end;
         WHEN 'dashboard_Campaign_List_Filter' THEN
			begin
                 CALL S_UI_Campaign_List_Filter(in_request_json,out_result_json);
		end;
         WHEN 'dashboard_Product_Vs_Cust_View' THEN
			begin
                 CALL S_dashboard_Product_Vs_Cust_View(in_request_json,out_result_json);
		end;
        WHEN 'dashboard_insights_ProductView_Association_AttrSelection' THEN
			begin
                 CALL S_dashboard_insights_ProductView_Association_AttrSelection(in_request_json,out_result_json);
		end;
        WHEN 'dashboard_Product_View_Association_Summary' THEN
			begin
                 CALL S_dashboard_insights_ProductView_Association_Summary(in_request_json,out_result_json);
		end;
        WHEN 'dashboard_Product_View_Association' THEN
			begin
                 CALL S_dashboard_insights_ProductView_Association(in_request_json,out_result_json);
		end;
        WHEN 'dashboard_insights_Product_OneView' THEN
			begin
                 CALL S_dashboard_insights_Product_OneView(in_request_json,out_result_json);
		end;
        WHEN 'dashboard_KPI_Home' THEN
			begin
                 CALL S_dashboard_KPI_Home(in_request_json,out_result_json);
		end;
        WHEN 'dashboard_conversion_deep_dive' THEN
			begin
                 CALL S_dashboard_performance_conversion_deep_dive(in_request_json,out_result_json);
		end;
       ELSE
			SET out_result_json=json_object("Status","Not Implemented Yet");
	END CASE;
    
	UPDATE log_solus_dashboard SET output_json=out_result_json WHERE id=@id AND module=page_name;
END$$
DELIMITER ;
