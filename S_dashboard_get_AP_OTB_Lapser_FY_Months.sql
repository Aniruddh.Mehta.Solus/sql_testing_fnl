DELIMITER $$
CREATE or replace PROCEDURE `S_dashboard_get_AP_OTB_Lapser_FY_Months`(OUT vAPMonth tinyint(2), OUT vOTBMonth tinyint(2), OUT vLapserMonth tinyint(2), OUT vFYMonth tinyint(2), OUT vAHF tinyint(2))
BEGIN
SET sql_safe_updates=0;
INSERT IGNORE INTO UI_Configuration_Global (config_name, value1) values ('FISCAL_STARTMONTH','4');
INSERT IGNORE INTO UI_Configuration_Global (config_name, value1) values ('OTBMonthTimesAPMonth','2');
INSERT IGNORE INTO UI_Configuration_Global (config_name, value1) values ('LapserMonthTimesAPMonth','4');
INSERT IGNORE INTO UI_Configuration_Global (config_name, value1) values ('ActiveHighFrequency','3');

/* AP MONTH */
select floor(ifnull(value,180)/30) into @vAPMonth from svoc_config  where name='active_period';

/* OTB MONTH */
set @ActiveHighFreqency=3;
select ifnull(value1,2) into @vAHF from UI_Configuration_Global where config_name='ActiveHighFrequency';

/* OTB MONTH */
set @OTBMonthTimesAPMonth=2;
select ifnull(value1,2) into @OTBMonthTimesAPMonth from UI_Configuration_Global where config_name='OTBMonthTimesAPMonth';
select @vAPMonth * cast(@OTBMonthTimesAPMonth as unsigned) into @vOTBMonth;

/* Lapser MONTH */
set @LapserMonthTimesAPMonth=4;
select ifnull(value1,2) into @LapserMonthTimesAPMonth from UI_Configuration_Global where config_name='LapserMonthTimesAPMonth';
select @vAPMonth * cast(@LapserMonthTimesAPMonth as unsigned) into @vLapserMonth;

/* FY MONTH */
set @vFYMonthTemp=null;
set @vFYMonth=null;
set @fiscalmonth=4;
select current_date() into @today;
select ifnull(value1,4) into @fiscalmonth from UI_Configuration_Global where config_name='FISCAL_STARTMONTH';

select case when date_format(@today,'%m') > cast(@fiscalmonth as unsigned) then date_format(@today,'%Y') else (date_format(@today,'%Y') - 1) end into @vFYYear;
SELECT TIMESTAMPDIFF(MONTH,concat(@vFYYear,'-',@fiscalmonth,'-01'),@today) into @vFYMonthTemp;

select case when  cast(@vFYMonthTemp as unsigned) = 12 then 0 else @vFYMonthTemp end into @vFYMonth;
select @vFYMonthTemp,@vFYMonth,@today,@fiscalmonth,@vFYYear;

SET vAPMonth=@vAPMonth;
SET vOTBMonth=@vOTBMonth;
SET vLapserMonth=@vLapserMonth;
SET vFYMonth=@vFYMonth;
SET vAHF=@vAHF;


select ' /*********************************************************** ' as '' ;
select ' Active Base    : <  vAPMonth ' as '' ;
select ' OTBBase        : between vAPMonth and vOTBMonth  ' as '' ;
select ' LapsedBase     : between vOTBMonth and  vLapserMonth ' as '' ;
select ' DeepLapsedBase : > vLapserMonth ' as '' ;
select ' ';
select  concat('AP MONTH        : ', vAPMonth) as '' ;
select  concat('OTB MONTH       : ', vOTBMonth) as '' ;
select  concat('LAPSER MONTH    : ', vLapserMonth) as '' ;
select  concat('FY MONTH        : ', vFYMonth) as '' ;
select  concat('AHF      		  : ', vAHF) as '' ;
select ' **********************************************************/ ';
END$$
DELIMITER ;
