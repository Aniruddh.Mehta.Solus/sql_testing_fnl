DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_insights_repeat_cohorts_metrics`(IN vBatchSize BIGINT)
BEGIN
DECLARE vStart_Cnt BIGINT DEFAULT 0;
DECLARE vEnd_Cnt BIGINT DEFAULT 0;
DECLARE vRec_Cnt BIGINT DEFAULT 0;
DECLARE vStart_Date BIGINT DEFAULT 0;
DECLARE vEnd_Date BIGINT DEFAULT 0;

DELETE from log_solus where module = 'S_dashboard_insights_repeat_cohorts_metrics';

SET SQL_SAFE_UPDATES=0;

SET vStart_Date = date_format(date_sub(concat(date_format(current_date(),"%Y%m"),'01'), INTERVAL 14 MONTH),"%Y%m");
SET vEnd_Date = date_format(current_date(),"%Y%m");

INSERT INTO Dashboard_Metrics_Log
(
`Screen_Name`,
`Status`
)
VALUES
('INSIGHTS - REPEAT COHORTS',
'Started');

DROP TABLE IF EXISTS Dashboard_Insights_Repeat_Cohorts_Bills_Temp;
  CREATE TABLE IF NOT EXISTS Dashboard_Insights_Repeat_Cohorts_Bills_Temp ( 
  Customer_Id bigint(20) NOT NULL,
  Bill_Date date NOT NULL,
  FTD date NOT NULL
  ) ENGINE = INNODB DEFAULT CHARACTER SET=LATIN1; 
 
  ALTER TABLE Dashboard_Insights_Repeat_Cohorts_Bills_Temp 
    ADD INDEX (Customer_Id),     
    ADD INDEX (Bill_Date), 
    ADD INDEX (FTD); 
	
DROP TABLE IF EXISTS Dashboard_Insights_Repeat_Cohorts_Bills_By_Month_Temp;
  CREATE TABLE IF NOT EXISTS Dashboard_Insights_Repeat_Cohorts_Bills_By_Month_Temp ( 
  Customer_Id bigint(20) NOT NULL,
  Bill_Year smallint(6) NOT NULL,
  Bill_Year_Month int(11) NOT NULL,
  FTD date NOT NULL
  ) ENGINE = INNODB DEFAULT CHARACTER SET=LATIN1; 
 
  ALTER TABLE Dashboard_Insights_Repeat_Cohorts_Bills_By_Month_Temp 
    ADD INDEX (Customer_Id),     
    ADD INDEX (Bill_Year), 
	ADD INDEX (Bill_Year_Month), 
    ADD INDEX (FTD); 

DROP TABLE IF EXISTS Dashboard_Insights_Repeat_Cohorts_Customers_Temp;
  CREATE TABLE IF NOT EXISTS Dashboard_Insights_Repeat_Cohorts_Customers_Temp ( 
  Customer_Id bigint(20) NOT NULL,
  Bill_Year smallint(6) NOT NULL,
  Bill_Year_Month int(11) NOT NULL,
  Bill_Date date NOT NULL,
  Is_New_Customer int(1) NOT NULL,
  Is_Existing_Customer int(1) DEFAULT NULL
  ) ENGINE = INNODB DEFAULT CHARACTER SET=LATIN1; 
 
  ALTER TABLE Dashboard_Insights_Repeat_Cohorts_Customers_Temp 
    ADD INDEX (Is_Existing_Customer,Is_New_Customer,Customer_Id,Bill_Year,Bill_Year_Month,Bill_Date)
    ;
	
DROP TABLE IF EXISTS Dashboard_Insights_Repeat_Cohorts_Customers;
  CREATE TABLE IF NOT EXISTS Dashboard_Insights_Repeat_Cohorts_Customers ( 
  AGGREGATION char(50) NOT NULL,
  Customer_Id bigint(20) NOT NULL,
  Bill_Year smallint(6) NOT NULL,
  Bill_Year_Month int(11) NOT NULL,
  Bill_Date date NOT NULL
  ) ENGINE = INNODB DEFAULT CHARACTER SET=LATIN1; 
 
  ALTER TABLE Dashboard_Insights_Repeat_Cohorts_Customers 
    ADD INDEX (AGGREGATION,Customer_Id,Bill_Year,Bill_Year_Month,Bill_Date);

SELECT 
    Customer_Id
INTO vRec_Cnt FROM
    CDM_Bill_Details
ORDER BY Customer_Id DESC
LIMIT 1;
	SET vStart_Cnt=0;
	SET vEnd_Cnt=0; 
LOOP_ALL_CUSTOMERS: LOOP
	SET vEnd_Cnt = vStart_Cnt + vBatchSize;
	SELECT vStart_Cnt, vEnd_Cnt, CURRENT_TIMESTAMP();
    IF vStart_Cnt  >= vRec_Cnt THEN
		LEAVE LOOP_ALL_CUSTOMERS;
	END IF;  
INSERT INTO log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_insights_repeat_cohorts_metrics',vStart_Cnt,vEnd_Cnt,'Loading Bills for Repeat Transactions');

INSERT INTO Dashboard_Insights_Repeat_Cohorts_Bills_Temp
(
`Customer_Id`,
`Bill_Date`,
`FTD`
)
SELECT 
DISTINCT(A.Customer_Id) AS Customer_Id, 
Bill_Date,
MIN(Bill_Date) OVER (PARTITION BY Customer_Id) AS FTD
FROM CDM_Bill_Details A
JOIN CDM_Customer_TP_Var B
ON A.Customer_Id = B.Customer_Id
WHERE A.Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt;

INSERT INTO Dashboard_Insights_Repeat_Cohorts_Bills_By_Month_Temp
(
`Customer_Id`,
`Bill_Year`,
`Bill_Year_Month`,
`FTD`
)
SELECT 
A.Customer_Id, 
Bill_Year, 
Bill_Year_Month,
MIN(Bill_Date) OVER (PARTITION BY A.Customer_Id,Bill_Year_Month) AS FTD
FROM CDM_Bill_Details A
JOIN CDM_Customer_TP_Var B
ON A.Customer_Id = B.Customer_Id
WHERE bill_date IS NOT NULL
AND A.Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt
GROUP BY 1,2,3;

INSERT INTO Dashboard_Insights_Repeat_Cohorts_Customers_Temp
(
`Customer_Id`,
`Bill_Year`,
`Bill_Year_Month`,
`Bill_Date`,
`Is_New_Customer`,
`Is_Existing_Customer`
)
SELECT DISTINCT(B.Customer_Id) AS Customer_Id,
C.Bill_Year,
C.Bill_Year_Month,
C.Bill_Date,
CASE WHEN FTD = C.Bill_Date THEN 1 ELSE 0 END AS Is_New_Customer,
CASE WHEN FTD <> C.Bill_Date THEN 1 ELSE 0 END AS Is_Existing_Customer
FROM CDM_Bill_Details C
INNER JOIN Dashboard_Insights_Repeat_Cohorts_Bills_Temp B
ON C.Customer_Id = B.Customer_Id
WHERE B.Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt;

INSERT INTO `Dashboard_Insights_Repeat_Cohorts_Customers`
(
	`AGGREGATION`,
	`Customer_Id`,
	`Bill_Year`,
	`Bill_Year_Month`,
	`Bill_Date`
)
SELECT 
	'Repeat within 7 Days' AS AGGREGATION, 
	A.Customer_Id, 
	A.Bill_Year, 
	A.Bill_Year_Month, 
	A.Bill_Date
FROM CDM_Bill_Details A
INNER JOIN Dashboard_Insights_Repeat_Cohorts_Bills_By_Month_Temp B
ON B.Customer_Id = A.Customer_Id
WHERE DATEDIFF(FTD,Bill_Date) BETWEEN 1 AND 7
AND A.Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt
GROUP BY 1,2,3,4,5;

INSERT INTO `Dashboard_Insights_Repeat_Cohorts_Customers`
(
	`AGGREGATION`,
	`Customer_Id`,
	`Bill_Year`,
	`Bill_Year_Month`,
	`Bill_Date`
)
SELECT 
	'Repeat within 15 Days' AS AGGREGATION, 
	A.Customer_Id, 
	A.Bill_Year, 
	A.Bill_Year_Month, 
	A.Bill_Date
FROM CDM_Bill_Details A
INNER JOIN Dashboard_Insights_Repeat_Cohorts_Bills_By_Month_Temp B
ON B.Customer_Id = A.Customer_Id
WHERE DATEDIFF(FTD,Bill_Date) BETWEEN 1 AND 15
AND A.Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt
GROUP BY 1,2,3,4,5;

INSERT INTO `Dashboard_Insights_Repeat_Cohorts_Customers`
(
	`AGGREGATION`,
	`Customer_Id`,
	`Bill_Year`,
	`Bill_Year_Month`,
	`Bill_Date`
)
SELECT 
	'Repeat within 30 Days' AS AGGREGATION, 
	A.Customer_Id, 
	A.Bill_Year, 
	A.Bill_Year_Month, 
	A.Bill_Date
FROM CDM_Bill_Details A
INNER JOIN Dashboard_Insights_Repeat_Cohorts_Bills_By_Month_Temp B
ON B.Customer_Id = A.Customer_Id
WHERE DATEDIFF(FTD,Bill_Date) BETWEEN 1 AND 30
AND A.Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt
GROUP BY 1,2,3,4,5;

INSERT INTO `Dashboard_Insights_Repeat_Cohorts_Customers`
(
	`AGGREGATION`,
	`Customer_Id`,
	`Bill_Year`,
	`Bill_Year_Month`,
	`Bill_Date`
)
SELECT 
	'Repeat within 60 Days' AS AGGREGATION, 
	A.Customer_Id, 
	A.Bill_Year, 
	A.Bill_Year_Month, 
	A.Bill_Date
FROM CDM_Bill_Details A
INNER JOIN Dashboard_Insights_Repeat_Cohorts_Bills_By_Month_Temp B
ON B.Customer_Id = A.Customer_Id
WHERE DATEDIFF(FTD,Bill_Date) BETWEEN 1 AND 60
AND A.Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt
GROUP BY 1,2,3,4,5;

INSERT INTO `Dashboard_Insights_Repeat_Cohorts_Customers`
(
	`AGGREGATION`,
	`Customer_Id`,
	`Bill_Year`,
	`Bill_Year_Month`,
	`Bill_Date`
)
SELECT 
	'Repeat within 90 Days' AS AGGREGATION, 
	A.Customer_Id, 
	A.Bill_Year, 
	A.Bill_Year_Month, 
	A.Bill_Date
FROM CDM_Bill_Details A
INNER JOIN Dashboard_Insights_Repeat_Cohorts_Bills_By_Month_Temp B
ON B.Customer_Id = A.Customer_Id
WHERE DATEDIFF(FTD,Bill_Date) BETWEEN 1 AND 90
AND A.Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt
GROUP BY 1,2,3,4,5;

INSERT INTO `Dashboard_Insights_Repeat_Cohorts_Customers`
(
	`AGGREGATION`,
	`Customer_Id`,
	`Bill_Year`,
	`Bill_Year_Month`,
	`Bill_Date`
)
SELECT 
	'Repeat within 180 Days' AS AGGREGATION, 
	A.Customer_Id, 
	A.Bill_Year, 
	A.Bill_Year_Month, 
	A.Bill_Date
FROM CDM_Bill_Details A
INNER JOIN Dashboard_Insights_Repeat_Cohorts_Bills_By_Month_Temp B
ON B.Customer_Id = A.Customer_Id
WHERE DATEDIFF(FTD,Bill_Date) BETWEEN 1 AND 180
AND A.Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt
GROUP BY 1,2,3,4,5;

INSERT INTO `Dashboard_Insights_Repeat_Cohorts_Customers`
(
	`AGGREGATION`,
	`Customer_Id`,
	`Bill_Year`,
	`Bill_Year_Month`,
	`Bill_Date`
)
SELECT 
	'Repeat within 365 Days' AS AGGREGATION, 
	A.Customer_Id, 
	A.Bill_Year, 
	A.Bill_Year_Month, 
	A.Bill_Date
FROM CDM_Bill_Details A
INNER JOIN Dashboard_Insights_Repeat_Cohorts_Bills_By_Month_Temp B
ON B.Customer_Id = A.Customer_Id
WHERE DATEDIFF(FTD,Bill_Date) BETWEEN 1 AND 365
AND A.Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt
GROUP BY 1,2,3,4,5;

SET vStart_Cnt = vEnd_Cnt;
END LOOP LOOP_ALL_CUSTOMERS; 


INSERT INTO log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_insights_repeat_cohorts_metrics',vStart_Cnt,vEnd_Cnt,'Loading ALL measure data');

SET vStart_Date = date_format(date_sub(concat(date_format(current_date(),"%Y%m"),'01'), INTERVAL 14 MONTH),"%Y%m");
SET vEnd_Date = date_format(current_date(),"%Y%m");

SELECT vStart_Date,vEnd_Date;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Count`
)
SELECT
'ALL' AS Measure,
'Base' AS AGGREGATION, 
Bill_Year_Month,
COUNT(DISTINCT Customer_Id) AS Customer_Count
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp
WHERE Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Count`
)
SELECT
'ALL' AS Measure, 
'Repeat within 7 Days' AS AGGREGATION, 
Bill_Year_Month,
COUNT(DISTINCT Customer_Id) AS Customer_Count
FROM Dashboard_Insights_Repeat_Cohorts_Customers
WHERE AGGREGATION = 'Repeat within 7 Days'
AND Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Count`
)
SELECT
'ALL' AS Measure,
'Repeat within 15 Days' AS AGGREGATION, 
Bill_Year_Month,
COUNT(DISTINCT Customer_Id) AS Customer_Count
FROM Dashboard_Insights_Repeat_Cohorts_Customers
WHERE AGGREGATION = 'Repeat within 15 Days'
AND Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Count`
)
SELECT
'ALL' AS Measure,
'Repeat within 30 Days' AS AGGREGATION, 
Bill_Year_Month,
COUNT(DISTINCT Customer_Id) AS Customer_Count
FROM Dashboard_Insights_Repeat_Cohorts_Customers
WHERE AGGREGATION = 'Repeat within 30 Days'
AND Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Count`
)
SELECT
'ALL' AS Measure,
'Repeat within 60 Days' AS AGGREGATION, 
Bill_Year_Month,
COUNT(DISTINCT Customer_Id) AS Customer_Count
FROM Dashboard_Insights_Repeat_Cohorts_Customers
WHERE AGGREGATION = 'Repeat within 60 Days'
AND Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Count`
)
SELECT
'ALL' AS Measure,
'Repeat within 90 Days' AS AGGREGATION, 
Bill_Year_Month,
COUNT(DISTINCT Customer_Id) AS Customer_Count
FROM Dashboard_Insights_Repeat_Cohorts_Customers
WHERE AGGREGATION = 'Repeat within 90 Days'
AND Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Count`
)
SELECT
'ALL' AS Measure,
'Repeat within 180 Days' AS AGGREGATION, 
Bill_Year_Month,
COUNT(DISTINCT Customer_Id) AS Customer_Count
FROM Dashboard_Insights_Repeat_Cohorts_Customers
WHERE AGGREGATION = 'Repeat within 180 Days'
AND Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Count`
)
SELECT
'ALL' AS Measure,
'Repeat within 365 Days' AS AGGREGATION, 
Bill_Year_Month,
COUNT(DISTINCT Customer_Id) AS Customer_Count
FROM Dashboard_Insights_Repeat_Cohorts_Customers
WHERE AGGREGATION = 'Repeat within 365 Days'
AND Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Percent`
)
SELECT
'ALL' AS Measure,
'Repeat within 7 Days%' AS AGGREGATION,
Bill_Year_Month,
Bills AS Customer_Percent
FROM
(
SELECT C.Bill_Year_Month , Bills FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills',B.Bill_Year_Month FROM
(
SELECT COUNT(DISTINCT Customer_Id) AS c1, Bill_Year_Month FROM Dashboard_Insights_Repeat_Cohorts_Customers
WHERE AGGREGATION = 'Repeat within 7 Days'
AND Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month
)A
,
(SELECT Bill_Year_Month,COUNT(DISTINCT Customer_Id) AS COUNT2 
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp
WHERE Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month 
)B
WHERE A.Bill_Year_Month = B.Bill_Year_Month
) C
GROUP BY C.Bill_Year_Month
) AS T;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Percent`
)
SELECT
'ALL' AS Measure,
'Repeat within 15 Days%' AS AGGREGATION,
Bill_Year_Month,
Bills AS Customer_Percent
FROM
(
SELECT C.Bill_Year_Month , Bills FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills',B.Bill_Year_Month FROM
(
SELECT COUNT(DISTINCT Customer_Id) AS c1, Bill_Year_Month FROM Dashboard_Insights_Repeat_Cohorts_Customers
WHERE AGGREGATION = 'Repeat within 15 Days'
AND Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month
)A
,
(SELECT Bill_Year_Month,COUNT(DISTINCT Customer_Id) AS COUNT2 
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp
WHERE Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month 
)B
WHERE A.Bill_Year_Month = B.Bill_Year_Month
) C
GROUP BY C.Bill_Year_Month
) AS T;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Percent`
)
SELECT
'ALL' AS Measure,
'Repeat within 30 Days%' AS AGGREGATION,
Bill_Year_Month,
Bills AS Customer_Percent
FROM
(
SELECT C.Bill_Year_Month , Bills FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills',B.Bill_Year_Month FROM
(
SELECT COUNT(DISTINCT Customer_Id) AS c1, Bill_Year_Month FROM Dashboard_Insights_Repeat_Cohorts_Customers
WHERE AGGREGATION = 'Repeat within 30 Days'
AND Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month
)A
,
(SELECT Bill_Year_Month,COUNT(DISTINCT Customer_Id) AS COUNT2 
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp
WHERE Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month 
)B
WHERE A.Bill_Year_Month = B.Bill_Year_Month
) C
GROUP BY C.Bill_Year_Month
) AS T;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Percent`
)
SELECT
'ALL' AS Measure,
'Repeat within 60 Days%' AS AGGREGATION,
Bill_Year_Month,
Bills AS Customer_Percent
FROM
(
SELECT C.Bill_Year_Month , Bills FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills',B.Bill_Year_Month FROM
(
SELECT COUNT(DISTINCT Customer_Id) AS c1, Bill_Year_Month FROM Dashboard_Insights_Repeat_Cohorts_Customers
WHERE AGGREGATION = 'Repeat within 60 Days'
AND Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month
)A
,
(SELECT Bill_Year_Month,COUNT(DISTINCT Customer_Id) AS COUNT2 
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp
WHERE Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month 
)B
WHERE A.Bill_Year_Month = B.Bill_Year_Month
) C
GROUP BY C.Bill_Year_Month
) AS T;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Percent`
)
SELECT
'ALL' AS Measure,
'Repeat within 90 Days%' AS AGGREGATION,
Bill_Year_Month,
Bills AS Customer_Percent
FROM
(
SELECT C.Bill_Year_Month , Bills FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills',B.Bill_Year_Month FROM
(
SELECT COUNT(DISTINCT Customer_Id) AS c1, Bill_Year_Month FROM Dashboard_Insights_Repeat_Cohorts_Customers
WHERE AGGREGATION = 'Repeat within 90 Days'
AND Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month
)A
,
(SELECT Bill_Year_Month,COUNT(DISTINCT Customer_Id) AS COUNT2 
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp
WHERE Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month 
)B
WHERE A.Bill_Year_Month = B.Bill_Year_Month
) C
GROUP BY C.Bill_Year_Month
) AS T;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Percent`
)
SELECT
'ALL' AS Measure,
'Repeat within 180 Days%' AS AGGREGATION,
Bill_Year_Month,
Bills AS Customer_Percent
FROM
(
SELECT C.Bill_Year_Month , Bills FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills',B.Bill_Year_Month FROM
(
SELECT COUNT(DISTINCT Customer_Id) AS c1, Bill_Year_Month FROM Dashboard_Insights_Repeat_Cohorts_Customers
WHERE AGGREGATION = 'Repeat within 180 Days'
AND Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month
)A
,
(SELECT Bill_Year_Month,COUNT(DISTINCT Customer_Id) AS COUNT2 
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp
WHERE Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month 
)B
WHERE A.Bill_Year_Month = B.Bill_Year_Month
) C
GROUP BY C.Bill_Year_Month
) AS T;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Percent`
)
SELECT
'ALL' AS Measure,
'Repeat within 365 Days%' AS AGGREGATION,
Bill_Year_Month,
Bills AS Customer_Percent
FROM
(
SELECT C.Bill_Year_Month , Bills FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills',B.Bill_Year_Month FROM
(
SELECT COUNT(DISTINCT Customer_Id) AS c1, Bill_Year_Month FROM Dashboard_Insights_Repeat_Cohorts_Customers
WHERE AGGREGATION = 'Repeat within 365 Days'
AND Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month
)A
,
(SELECT Bill_Year_Month,COUNT(DISTINCT Customer_Id) AS COUNT2 
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp
WHERE Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month 
)B
WHERE A.Bill_Year_Month = B.Bill_Year_Month
) C
GROUP BY C.Bill_Year_Month
) AS T;

INSERT INTO log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_insights_repeat_cohorts_metrics',vStart_Cnt,vEnd_Cnt,'Loading EXISTING measure data');

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Count`
)
SELECT 
'EXISTING' AS Measure, 
'Base' AS AGGREGATION, 
Bill_Year_Month,
COUNT(DISTINCT Customer_Id) as 'Customer_Count' 
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp
WHERE Is_Existing_Customer = 1
AND Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Count`
)
SELECT 
'EXISTING' AS Measure, 
'Repeat within 7 Days' AS AGGREGATION, 
A.Bill_Year_Month,
COUNT(DISTINCT A.Customer_Id) as 'Customer_Count' 
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp A
INNER JOIN Dashboard_Insights_Repeat_Cohorts_Customers B
ON A.Customer_Id = B.Customer_Id
AND A.Bill_Year = B.Bill_Year
AND A.Bill_Year_Month = B.Bill_Year_Month
AND A.Bill_Date = B.Bill_Date
WHERE Is_Existing_Customer = 1
AND B.AGGREGATION = 'Repeat within 7 Days'
AND B.Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY B.Bill_Year_Month;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Count`
)
SELECT 
'EXISTING' AS Measure, 
'Repeat within 15 Days' AS AGGREGATION, 
A.Bill_Year_Month,
COUNT(DISTINCT A.Customer_Id) as 'Customer_Count' 
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp A
INNER JOIN Dashboard_Insights_Repeat_Cohorts_Customers B
ON A.Customer_Id = B.Customer_Id
AND A.Bill_Year = B.Bill_Year
AND A.Bill_Year_Month = B.Bill_Year_Month
AND A.Bill_Date = B.Bill_Date
WHERE Is_Existing_Customer = 1
AND B.AGGREGATION = 'Repeat within 15 Days'
AND B.Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY B.Bill_Year_Month;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Count`
)
SELECT 
'EXISTING' AS Measure, 
'Repeat within 30 Days' AS AGGREGATION, 
A.Bill_Year_Month,
COUNT(DISTINCT A.Customer_Id) as 'Customer_Count' 
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp A
INNER JOIN Dashboard_Insights_Repeat_Cohorts_Customers B
ON A.Customer_Id = B.Customer_Id
AND A.Bill_Year = B.Bill_Year
AND A.Bill_Year_Month = B.Bill_Year_Month
AND A.Bill_Date = B.Bill_Date
WHERE Is_Existing_Customer = 1
AND B.AGGREGATION = 'Repeat within 30 Days'
AND B.Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY B.Bill_Year_Month;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Count`
)
SELECT 
'EXISTING' AS Measure, 
'Repeat within 60 Days' AS AGGREGATION, 
A.Bill_Year_Month,
COUNT(DISTINCT A.Customer_Id) as 'Customer_Count' 
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp A
INNER JOIN Dashboard_Insights_Repeat_Cohorts_Customers B
ON A.Customer_Id = B.Customer_Id
AND A.Bill_Year = B.Bill_Year
AND A.Bill_Year_Month = B.Bill_Year_Month
AND A.Bill_Date = B.Bill_Date
WHERE Is_Existing_Customer = 1
AND B.AGGREGATION = 'Repeat within 60 Days'
AND B.Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY B.Bill_Year_Month;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Count`
)
SELECT 
'EXISTING' AS Measure, 
'Repeat within 90 Days' AS AGGREGATION, 
A.Bill_Year_Month,
COUNT(DISTINCT A.Customer_Id) as 'Customer_Count' 
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp A
INNER JOIN Dashboard_Insights_Repeat_Cohorts_Customers B
ON A.Customer_Id = B.Customer_Id
AND A.Bill_Year = B.Bill_Year
AND A.Bill_Year_Month = B.Bill_Year_Month
AND A.Bill_Date = B.Bill_Date
WHERE Is_Existing_Customer = 1
AND B.AGGREGATION = 'Repeat within 90 Days'
AND B.Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY B.Bill_Year_Month;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Count`
)
SELECT 
'EXISTING' AS Measure, 
'Repeat within 180 Days' AS AGGREGATION, 
A.Bill_Year_Month,
COUNT(DISTINCT A.Customer_Id) as 'Customer_Count' 
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp A
INNER JOIN Dashboard_Insights_Repeat_Cohorts_Customers B
ON A.Customer_Id = B.Customer_Id
AND A.Bill_Year = B.Bill_Year
AND A.Bill_Year_Month = B.Bill_Year_Month
AND A.Bill_Date = B.Bill_Date
WHERE Is_Existing_Customer = 1
AND B.AGGREGATION = 'Repeat within 180 Days'
AND B.Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY B.Bill_Year_Month;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Count`
)
SELECT 
'EXISTING' AS Measure, 
'Repeat within 365 Days' AS AGGREGATION, 
A.Bill_Year_Month,
COUNT(DISTINCT A.Customer_Id) as 'Customer_Count' 
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp A
INNER JOIN Dashboard_Insights_Repeat_Cohorts_Customers B
ON A.Customer_Id = B.Customer_Id
AND A.Bill_Year = B.Bill_Year
AND A.Bill_Year_Month = B.Bill_Year_Month
AND A.Bill_Date = B.Bill_Date
WHERE Is_Existing_Customer = 1
AND B.AGGREGATION = 'Repeat within 365 Days'
AND B.Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY B.Bill_Year_Month;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Percent`
)
select 
'EXISTING' AS Measure, 
'Repeat within 7 Days%' AS AGGREGATION, 
Bill_Year_Month, 
Bills AS Customer_Percent
FROM
(
SELECT C.Bill_Year_Month , Bills FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills',B.Bill_Year_Month FROM
(
SELECT COUNT(DISTINCT A.Customer_Id) AS c1, A.Bill_Year_Month  
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp A
INNER JOIN Dashboard_Insights_Repeat_Cohorts_Customers B
ON A.Customer_Id = B.Customer_Id
AND A.Bill_Year = B.Bill_Year
AND A.Bill_Year_Month = B.Bill_Year_Month
AND A.Bill_Date = B.Bill_Date
WHERE Is_Existing_Customer = 1
AND B.AGGREGATION = 'Repeat within 7 Days'
AND B.Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY B.Bill_Year_Month
)A
,
(SELECT Bill_Year_Month,COUNT(DISTINCT Customer_Id) AS COUNT2 
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp
WHERE Is_Existing_Customer = 1
AND Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month 
)B
WHERE A.Bill_Year_Month = B.Bill_Year_Month
) C
GROUP BY C.Bill_Year_Month
) T;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Percent`
)
select 
'EXISTING' AS Measure, 
'Repeat within 15 Days%' AS AGGREGATION, 
Bill_Year_Month, 
Bills AS Customer_Percent
FROM
(
SELECT C.Bill_Year_Month , Bills FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills',B.Bill_Year_Month FROM
(
SELECT COUNT(DISTINCT A.Customer_Id) AS c1, A.Bill_Year_Month  
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp A
INNER JOIN Dashboard_Insights_Repeat_Cohorts_Customers B
ON A.Customer_Id = B.Customer_Id
AND A.Bill_Year = B.Bill_Year
AND A.Bill_Year_Month = B.Bill_Year_Month
AND A.Bill_Date = B.Bill_Date
WHERE Is_Existing_Customer = 1
AND B.AGGREGATION = 'Repeat within 15 Days'
AND B.Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY B.Bill_Year_Month
)A
,
(SELECT Bill_Year_Month,COUNT(DISTINCT Customer_Id) AS COUNT2 
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp
WHERE Is_Existing_Customer = 1
AND Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month 
)B
WHERE A.Bill_Year_Month = B.Bill_Year_Month
) C
GROUP BY C.Bill_Year_Month
) T;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Percent`
)
SELECT 
'EXISTING' AS Measure, 
'Repeat within 30 Days%' AS AGGREGATION, 
Bill_Year_Month, 
Bills AS Customer_Percent
FROM
(
SELECT C.Bill_Year_Month , Bills FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills',B.Bill_Year_Month FROM
(
SELECT COUNT(DISTINCT A.Customer_Id) AS c1, A.Bill_Year_Month  
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp A
INNER JOIN Dashboard_Insights_Repeat_Cohorts_Customers B
ON A.Customer_Id = B.Customer_Id
AND A.Bill_Year = B.Bill_Year
AND A.Bill_Year_Month = B.Bill_Year_Month
AND A.Bill_Date = B.Bill_Date
WHERE Is_Existing_Customer = 1
AND B.AGGREGATION = 'Repeat within 30 Days'
AND B.Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY B.Bill_Year_Month
)A
,
(SELECT Bill_Year_Month,COUNT(DISTINCT Customer_Id) AS COUNT2 
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp
WHERE Is_Existing_Customer = 1
AND Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month 
)B
WHERE A.Bill_Year_Month = B.Bill_Year_Month
) C
GROUP BY C.Bill_Year_Month
) T;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Percent`
)
SELECT 
'EXISTING' AS Measure, 
'Repeat within 60 Days%' AS AGGREGATION, 
Bill_Year_Month, 
Bills AS Customer_Percent
FROM
(
SELECT C.Bill_Year_Month , Bills FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills',B.Bill_Year_Month FROM
(
SELECT COUNT(DISTINCT A.Customer_Id) AS c1, A.Bill_Year_Month  
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp A
INNER JOIN Dashboard_Insights_Repeat_Cohorts_Customers B
ON A.Customer_Id = B.Customer_Id
AND A.Bill_Year = B.Bill_Year
AND A.Bill_Year_Month = B.Bill_Year_Month
AND A.Bill_Date = B.Bill_Date
WHERE Is_Existing_Customer = 1
AND B.AGGREGATION = 'Repeat within 60 Days'
AND B.Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY B.Bill_Year_Month
)A
,
(SELECT Bill_Year_Month,COUNT(DISTINCT Customer_Id) AS COUNT2 
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp
WHERE Is_Existing_Customer = 1
AND Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month 
)B
WHERE A.Bill_Year_Month = B.Bill_Year_Month
) C
GROUP BY C.Bill_Year_Month
) T;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Percent`
)
SELECT 
'EXISTING' AS Measure, 
'Repeat within 90 Days%' AS AGGREGATION, 
Bill_Year_Month, 
Bills AS Customer_Percent
FROM
(
SELECT C.Bill_Year_Month , Bills FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills',B.Bill_Year_Month FROM
(
SELECT COUNT(DISTINCT A.Customer_Id) AS c1, A.Bill_Year_Month  
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp A
INNER JOIN Dashboard_Insights_Repeat_Cohorts_Customers B
ON A.Customer_Id = B.Customer_Id
AND A.Bill_Year = B.Bill_Year
AND A.Bill_Year_Month = B.Bill_Year_Month
AND A.Bill_Date = B.Bill_Date
WHERE Is_Existing_Customer = 1
AND B.AGGREGATION = 'Repeat within 90 Days'
AND B.Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY B.Bill_Year_Month
)A
,
(SELECT Bill_Year_Month,COUNT(DISTINCT Customer_Id) AS COUNT2 
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp
WHERE Is_Existing_Customer = 1
AND Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month 
)B
WHERE A.Bill_Year_Month = B.Bill_Year_Month
) C
GROUP BY C.Bill_Year_Month
) T;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Percent`
)
SELECT 
'EXISTING' AS Measure, 
'Repeat within 180 Days%' AS AGGREGATION, 
Bill_Year_Month, 
Bills AS Customer_Percent
FROM
(
SELECT C.Bill_Year_Month , Bills FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills',B.Bill_Year_Month FROM
(
SELECT COUNT(DISTINCT A.Customer_Id) AS c1, A.Bill_Year_Month  
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp A
INNER JOIN Dashboard_Insights_Repeat_Cohorts_Customers B
ON A.Customer_Id = B.Customer_Id
AND A.Bill_Year = B.Bill_Year
AND A.Bill_Year_Month = B.Bill_Year_Month
AND A.Bill_Date = B.Bill_Date
WHERE Is_Existing_Customer = 1
AND B.AGGREGATION = 'Repeat within 180 Days'
AND B.Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY B.Bill_Year_Month
)A
,
(SELECT Bill_Year_Month,COUNT(DISTINCT Customer_Id) AS COUNT2 
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp
WHERE Is_Existing_Customer = 1
AND Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month 
)B
WHERE A.Bill_Year_Month = B.Bill_Year_Month
) C
GROUP BY C.Bill_Year_Month
) T;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Percent`
)
SELECT 
'EXISTING' AS Measure, 
'Repeat within 365 Days%' AS AGGREGATION, 
Bill_Year_Month, 
Bills AS Customer_Percent
FROM
(
SELECT C.Bill_Year_Month , Bills FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills',B.Bill_Year_Month FROM
(
SELECT COUNT(DISTINCT A.Customer_Id) AS c1, A.Bill_Year_Month  
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp A
INNER JOIN Dashboard_Insights_Repeat_Cohorts_Customers B
ON A.Customer_Id = B.Customer_Id
AND A.Bill_Year = B.Bill_Year
AND A.Bill_Year_Month = B.Bill_Year_Month
AND A.Bill_Date = B.Bill_Date
WHERE Is_Existing_Customer = 1
AND B.AGGREGATION = 'Repeat within 365 Days'
AND B.Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY B.Bill_Year_Month
)A
,
(SELECT Bill_Year_Month,COUNT(DISTINCT Customer_Id) AS COUNT2 
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp
WHERE Is_Existing_Customer = 1
AND Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month 
)B
WHERE A.Bill_Year_Month = B.Bill_Year_Month
) C
GROUP BY C.Bill_Year_Month
) T;

INSERT INTO log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_insights_repeat_cohorts_metrics',vStart_Cnt,vEnd_Cnt,'Loading NEW measure data');

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Count`
)
SELECT 
'NEW' AS Measure, 
'Base' AS AGGREGATION, 
Bill_Year_Month,
COUNT(Is_New_Customer) AS Customer_Count
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp
WHERE Is_New_Customer = 1
AND Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Count`
)
SELECT 
'NEW' AS Measure, 
'Repeat within 7 Days' AS AGGREGATION, 
A.Bill_Year_Month,
COUNT(Is_New_Customer) AS 'Customer_Count' 
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp A
INNER JOIN Dashboard_Insights_Repeat_Cohorts_Customers B
ON A.Customer_Id = B.Customer_Id
AND A.Bill_Year = B.Bill_Year
AND A.Bill_Year_Month = B.Bill_Year_Month
AND A.Bill_Date = B.Bill_Date
WHERE Is_New_Customer = 1
AND B.AGGREGATION = 'Repeat within 7 Days'
AND B.Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY B.Bill_Year_Month;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Count`
)
SELECT 
'NEW' AS Measure, 
'Repeat within 15 Days' AS AGGREGATION, 
A.Bill_Year_Month,
COUNT(Is_New_Customer) AS 'Customer_Count' 
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp A
INNER JOIN Dashboard_Insights_Repeat_Cohorts_Customers B
ON A.Customer_Id = B.Customer_Id
AND A.Bill_Year = B.Bill_Year
AND A.Bill_Year_Month = B.Bill_Year_Month
AND A.Bill_Date = B.Bill_Date
WHERE Is_New_Customer = 1
AND B.AGGREGATION = 'Repeat within 15 Days'
AND B.Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY B.Bill_Year_Month;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Count`
)
SELECT 
'NEW' AS Measure, 
'Repeat within 30 Days' AS AGGREGATION, 
A.Bill_Year_Month,
COUNT(Is_New_Customer) AS 'Customer_Count' 
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp A
INNER JOIN Dashboard_Insights_Repeat_Cohorts_Customers B
ON A.Customer_Id = B.Customer_Id
AND A.Bill_Year = B.Bill_Year
AND A.Bill_Year_Month = B.Bill_Year_Month
AND A.Bill_Date = B.Bill_Date
WHERE Is_New_Customer = 1
AND B.AGGREGATION = 'Repeat within 30 Days'
AND B.Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY B.Bill_Year_Month;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Count`
)
SELECT 
'NEW' AS Measure, 
'Repeat within 60 Days' AS AGGREGATION, 
A.Bill_Year_Month,
COUNT(Is_New_Customer) AS 'Customer_Count' 
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp A
INNER JOIN Dashboard_Insights_Repeat_Cohorts_Customers B
ON A.Customer_Id = B.Customer_Id
AND A.Bill_Year = B.Bill_Year
AND A.Bill_Year_Month = B.Bill_Year_Month
AND A.Bill_Date = B.Bill_Date
WHERE Is_New_Customer = 1
AND B.AGGREGATION = 'Repeat within 60 Days'
AND B.Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY B.Bill_Year_Month;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Count`
)
SELECT 
'NEW' AS Measure, 
'Repeat within 90 Days' AS AGGREGATION, 
A.Bill_Year_Month,
COUNT(Is_New_Customer) AS 'Customer_Count' 
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp A
INNER JOIN Dashboard_Insights_Repeat_Cohorts_Customers B
ON A.Customer_Id = B.Customer_Id
AND A.Bill_Year = B.Bill_Year
AND A.Bill_Year_Month = B.Bill_Year_Month
AND A.Bill_Date = B.Bill_Date
WHERE Is_New_Customer = 1
AND B.AGGREGATION = 'Repeat within 90 Days'
AND B.Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY B.Bill_Year_Month;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Count`
)
SELECT 
'NEW' AS Measure, 
'Repeat within 180 Days' AS AGGREGATION, 
A.Bill_Year_Month,
COUNT(Is_New_Customer) AS 'Customer_Count' 
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp A
INNER JOIN Dashboard_Insights_Repeat_Cohorts_Customers B
ON A.Customer_Id = B.Customer_Id
AND A.Bill_Year = B.Bill_Year
AND A.Bill_Year_Month = B.Bill_Year_Month
AND A.Bill_Date = B.Bill_Date
WHERE Is_New_Customer = 1
AND B.AGGREGATION = 'Repeat within 180 Days'
AND B.Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY B.Bill_Year_Month;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Count`
)
SELECT 
'NEW' AS Measure, 
'Repeat within 365 Days' AS AGGREGATION, 
A.Bill_Year_Month,
COUNT(Is_New_Customer) AS 'Customer_Count' 
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp A
INNER JOIN Dashboard_Insights_Repeat_Cohorts_Customers B
ON A.Customer_Id = B.Customer_Id
AND A.Bill_Year = B.Bill_Year
AND A.Bill_Year_Month = B.Bill_Year_Month
AND A.Bill_Date = B.Bill_Date
WHERE Is_New_Customer = 1
AND B.AGGREGATION = 'Repeat within 365 Days'
AND B.Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY B.Bill_Year_Month;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Percent`
)
SELECT 
'NEW' AS Measure, 
'Repeat within 7 Days%' AS AGGREGATION, 
Bill_Year_Month, 
Bills AS Customer_Percent
FROM
(
SELECT C.Bill_Year_Month , Bills FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills',B.Bill_Year_Month FROM
(
SELECT COUNT(Is_New_Customer) AS c1, A.Bill_Year_Month  
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp A
INNER JOIN Dashboard_Insights_Repeat_Cohorts_Customers B
ON A.Customer_Id = B.Customer_Id
AND A.Bill_Year = B.Bill_Year
AND A.Bill_Year_Month = B.Bill_Year_Month
AND A.Bill_Date = B.Bill_Date
WHERE Is_New_Customer = 1
AND B.AGGREGATION = 'Repeat within 7 Days'
AND B.Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY B.Bill_Year_Month
)A
,
(SELECT Bill_Year_Month,COUNT(DISTINCT Customer_Id) AS COUNT2 
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp
WHERE Is_New_Customer = 1
AND Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month 
)B
WHERE A.Bill_Year_Month = B.Bill_Year_Month
) C
GROUP BY C.Bill_Year_Month
) T;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Percent`
)
SELECT 
'NEW' AS Measure, 
'Repeat within 15 Days%' AS AGGREGATION, 
Bill_Year_Month, 
Bills AS Customer_Percent
FROM
(
SELECT C.Bill_Year_Month , Bills FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills',B.Bill_Year_Month FROM
(
SELECT COUNT(Is_New_Customer) AS c1, A.Bill_Year_Month  
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp A
INNER JOIN Dashboard_Insights_Repeat_Cohorts_Customers B
ON A.Customer_Id = B.Customer_Id
AND A.Bill_Year = B.Bill_Year
AND A.Bill_Year_Month = B.Bill_Year_Month
AND A.Bill_Date = B.Bill_Date
WHERE Is_New_Customer = 1
AND B.AGGREGATION = 'Repeat within 15 Days'
AND B.Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY B.Bill_Year_Month
)A
,
(SELECT Bill_Year_Month,COUNT(DISTINCT Customer_Id) AS COUNT2 
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp
WHERE Is_New_Customer = 1
AND Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month 
)B
WHERE A.Bill_Year_Month = B.Bill_Year_Month
) C
GROUP BY C.Bill_Year_Month
) T;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Percent`
)
SELECT 
'NEW' AS Measure, 
'Repeat within 30 Days%' AS AGGREGATION, 
Bill_Year_Month, 
Bills AS Customer_Percent
FROM
(
SELECT C.Bill_Year_Month , Bills FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills',B.Bill_Year_Month FROM
(
SELECT COUNT(Is_New_Customer) AS c1, A.Bill_Year_Month  
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp A
INNER JOIN Dashboard_Insights_Repeat_Cohorts_Customers B
ON A.Customer_Id = B.Customer_Id
AND A.Bill_Year = B.Bill_Year
AND A.Bill_Year_Month = B.Bill_Year_Month
AND A.Bill_Date = B.Bill_Date
WHERE Is_New_Customer = 1
AND B.AGGREGATION = 'Repeat within 30 Days'
AND B.Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY B.Bill_Year_Month
)A
,
(SELECT Bill_Year_Month,COUNT(DISTINCT Customer_Id) AS COUNT2 
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp
WHERE Is_New_Customer = 1
AND Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month 
)B
WHERE A.Bill_Year_Month = B.Bill_Year_Month
) C
GROUP BY C.Bill_Year_Month
) T;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Percent`
)
SELECT 
'NEW' AS Measure, 
'Repeat within 60 Days%' AS AGGREGATION, 
Bill_Year_Month, 
Bills AS Customer_Percent
FROM
(
SELECT C.Bill_Year_Month , Bills FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills',B.Bill_Year_Month FROM
(
SELECT COUNT(Is_New_Customer) AS c1, A.Bill_Year_Month  
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp A
INNER JOIN Dashboard_Insights_Repeat_Cohorts_Customers B
ON A.Customer_Id = B.Customer_Id
AND A.Bill_Year = B.Bill_Year
AND A.Bill_Year_Month = B.Bill_Year_Month
AND A.Bill_Date = B.Bill_Date
WHERE Is_New_Customer = 1
AND B.AGGREGATION = 'Repeat within 60 Days'
AND B.Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY B.Bill_Year_Month
)A
,
(SELECT Bill_Year_Month,COUNT(DISTINCT Customer_Id) AS COUNT2 
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp
WHERE Is_New_Customer = 1
AND Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month 
)B
WHERE A.Bill_Year_Month = B.Bill_Year_Month
) C
GROUP BY C.Bill_Year_Month
) T;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Percent`
)
SELECT 
'NEW' AS Measure, 
'Repeat within 90 Days%' AS AGGREGATION, 
Bill_Year_Month, 
Bills AS Customer_Percent
FROM
(
SELECT C.Bill_Year_Month , Bills FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills',B.Bill_Year_Month FROM
(
SELECT COUNT(Is_New_Customer) AS c1, A.Bill_Year_Month  
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp A
INNER JOIN Dashboard_Insights_Repeat_Cohorts_Customers B
ON A.Customer_Id = B.Customer_Id
AND A.Bill_Year = B.Bill_Year
AND A.Bill_Year_Month = B.Bill_Year_Month
AND A.Bill_Date = B.Bill_Date
WHERE Is_New_Customer = 1
AND B.AGGREGATION = 'Repeat within 90 Days'
AND B.Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY B.Bill_Year_Month
)A
,
(SELECT Bill_Year_Month,COUNT(DISTINCT Customer_Id) AS COUNT2 
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp
WHERE Is_New_Customer = 1
AND Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month 
)B
WHERE A.Bill_Year_Month = B.Bill_Year_Month
) C
GROUP BY C.Bill_Year_Month
) T;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Percent`
)
SELECT 
'NEW' AS Measure, 
'Repeat within 180 Days%' AS AGGREGATION, 
Bill_Year_Month, 
Bills AS Customer_Percent
FROM
(
SELECT C.Bill_Year_Month , Bills FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills',B.Bill_Year_Month FROM
(
SELECT COUNT(Is_New_Customer) AS c1, A.Bill_Year_Month  
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp A
INNER JOIN Dashboard_Insights_Repeat_Cohorts_Customers B
ON A.Customer_Id = B.Customer_Id
AND A.Bill_Year = B.Bill_Year
AND A.Bill_Year_Month = B.Bill_Year_Month
AND A.Bill_Date = B.Bill_Date
WHERE Is_New_Customer = 1
AND B.AGGREGATION = 'Repeat within 180 Days'
AND B.Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY B.Bill_Year_Month
)A
,
(SELECT Bill_Year_Month,COUNT(DISTINCT Customer_Id) AS COUNT2 
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp
WHERE Is_New_Customer = 1
AND Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month 
)B
WHERE A.Bill_Year_Month = B.Bill_Year_Month
) C
GROUP BY C.Bill_Year_Month
) T;

INSERT INTO Dashboard_Insights_Repeat_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Percent`
)
SELECT 
'NEW' AS Measure, 
'Repeat within 365 Days%' AS AGGREGATION, 
Bill_Year_Month, 
Bills AS Customer_Percent
FROM
(
SELECT C.Bill_Year_Month , Bills FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills',B.Bill_Year_Month FROM
(
SELECT COUNT(Is_New_Customer) AS c1, A.Bill_Year_Month  
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp A
INNER JOIN Dashboard_Insights_Repeat_Cohorts_Customers B
ON A.Customer_Id = B.Customer_Id
AND A.Bill_Year = B.Bill_Year
AND A.Bill_Year_Month = B.Bill_Year_Month
AND A.Bill_Date = B.Bill_Date
WHERE Is_New_Customer = 1
AND B.AGGREGATION = 'Repeat within 365 Days'
AND B.Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY B.Bill_Year_Month
)A
,
(SELECT Bill_Year_Month,COUNT(DISTINCT Customer_Id) AS COUNT2 
FROM Dashboard_Insights_Repeat_Cohorts_Customers_Temp
WHERE Is_New_Customer = 1
AND Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Bill_Year_Month 
)B
WHERE A.Bill_Year_Month = B.Bill_Year_Month
) C
GROUP BY C.Bill_Year_Month
) T;

UPDATE Dashboard_Metrics_Log
SET `Status` = 'Ended' WHERE Screen_Name = 'INSIGHTS - REPEAT COHORTS';

END$$
DELIMITER ;
