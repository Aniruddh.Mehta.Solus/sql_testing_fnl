
CREATE or replace PROCEDURE `PROC_CLM_Voucher_Base_Format`(
	IN vCommunication_Template_ID BIGINT, 
	IN vCommunication_Template VARCHAR(128), 
	IN vEvent_Name  VARCHAR(128),  
	IN vprefix  VARCHAR(128), 
	IN vvoucher_length BIGINT, 
	IN vOfferCode_qty  BIGINT, 
	IN vStart_Date DATE , 
	IN vEnd_Date DATE,
	INOUT vErrMsg TEXT,
	INOUT vSuccess TINYINT
)
BEGIN

	DECLARE vRowCnt BIGINT DEFAULT 0;
	DECLARE vSqlstmt TEXT;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN 
		SET vSuccess = 0; 
		GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
		SET vErrMsg = CONCAT(IFNULL(vErrMsg,''), "ERROR ", @errno, " (", @sqlstate, "): ", @text);
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 0, vErrMsg, 0);
	END;
	SET vErrMsg = 'Start';
	SET vSuccess = 0;
	SET @CURR_PROC_NAME = 'PROC_CLM_Voucher_Base_Format';
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 1, vErrMsg, vSuccess);
	IF vCommunication_Template_ID IS NULL OR vCommunication_Template = '' THEN
		SET vErrMsg = 'Communication_Template_ID ,Communication_Template ,prefix ,voucher_length ,OfferCode_qty ,Start_Date ,End_Date are Mandatory Field and cannot be empty or NULL';
		SET vSuccess = 0;
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 2, vErrMsg, vSuccess);
	ELSE
		IF vCommunication_Template_ID IS NULL OR vCommunication_Template <= 0 THEN
			SET vSqlstmt = CONCAT(
				'INSERT IGNORE INTO CLM_Customer_Voucher_Base_Format (
				Communication_Template_ID,
				Communication_Template,
				Event_Name, 
				prefix,
				voucher_length ,
				OfferCode_qty ,
				Start_Date ,
				End_Date
				) VALUES ('
				,'"', vCommunication_Template_ID,'"'
				,',"', vCommunication_Template,'"'
                ,',"', vEvent_Name,'"'
                ,',"', vprefix,'"'
                ,',', vvoucher_length,''
                ,',', vOfferCode_qty,''
				,',"', vStart_Date,'"'
				,',"', vEnd_Date
				, '")'
			);

			SET vErrMsg = vSqlstmt;
			SET @sql_stmt = vSqlstmt;
			PREPARE EXEC_STMT FROM @sql_stmt;
			EXECUTE EXEC_STMT;
			DEALLOCATE PREPARE EXEC_STMT;
			CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 3, vErrMsg, vSuccess);
		END IF;
	END IF;
	
	SET vSuccess = 1;
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 6, vErrMsg, vSuccess);

END

