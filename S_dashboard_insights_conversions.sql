DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_insights_conversions`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
    declare IN_AGGREGATION_4 text;
    declare IN_AGGREGATION_5 text;
    declare IN_AGGREGATION_6 text;
    declare IN_AGGREGATION_7 text;
    declare IN_AGGREGATION_8 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
    SET IN_AGGREGATION_4 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_4"));
    SET IN_AGGREGATION_5 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_5"));
    SET IN_AGGREGATION_6 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_6"));
    SET IN_AGGREGATION_7 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_7"));
    SET IN_AGGREGATION_8 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_8"));
    
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    

select Value1 into @Incremental_Revenue_Method from UI_Configuration_Global where Config_Name = 'Incremental_Revenue_Method';
    SET @Incremental_Revenue_Method=ifnull(@Incremental_Revenue_Method,'Method2');
    select @Incremental_Revenue_Method;
    IF @Incremental_Revenue_Method = 'Method1'
    THEN
		SET @incr_rev_concat='_By_Day';
        SET @group_cond1='EE_Date';
        SET @mtd_concat='';
	ELSEIF @Incremental_Revenue_Method = 'Method2'
    THEN
		SET @group_cond1='YM';
		SET @incr_rev_concat='';
        SET @mtd_concat='MTD_';
	ELSE
		SET @incr_rev_concat='';
        SET @mtd_concat='';
		SET @incr_Rev = ' round((CASE WHEN SUM(Control_Base) < 0 or SUM(Target_Delivered) = 0 then 0 WHEN (((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*100 < 0 then 0 WHEN SUM(Control_Base)>0 and SUM(Control_Responder)=0 then SUM(Target_Revenue)
					ELSE ifnull((((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*(SUM(Target_Revenue)/SUM(Target_Responder))*(SUM(Target_Delivered)),0) END),0) ';
		SET @group_cond1='YM';
	END IF;
    SELECT @mtd_concat;
    
    IF IN_AGGREGATION_3 = 'ST'
    THEN
    SET @sql1=CONCAT('
		CREATE OR REPLACE VIEW CLM_Event_Dashboard_STLT
        as 
		SELECT `CLM_Event_Dashboard`.`Revenue Center`,
			`CLM_Event_Dashboard`.`Segment`,
			`CLM_Event_Dashboard`.`Campaign`,
			`CLM_Event_Dashboard`.`EE_Date`,
			`CLM_Event_Dashboard`.`Event_Execution_Date_ID`,
			`CLM_Event_Dashboard`.`Theme`,
			`CLM_Event_Dashboard`.`Offer`,
			`CLM_Event_Dashboard`.`Channel`,
			`CLM_Event_Dashboard`.`Has_Recommendations`,
			`CLM_Event_Dashboard`.`Event_ID`,
			`CLM_Event_Dashboard`.`Control_Base_Event` as `Control_Base`,
			`CLM_Event_Dashboard`.`Target_Base`,
			`CLM_Event_Dashboard`.`Target_Delivered`,
			`CLM_Event_Dashboard`.`Target_Delivered_NA`,
			`CLM_Event_Dashboard`.`Control_Responder_Event` as `Control_Responder`,
			`CLM_Event_Dashboard`.`Target_Responder`,
			`CLM_Event_Dashboard`.`Target_Revenue`,
			`CLM_Event_Dashboard`.`Target_Discount`,
			`CLM_Event_Dashboard`.`Target_Bills`,
			`CLM_Event_Dashboard`.`Target_Open`,
			`CLM_Event_Dashboard`.`Target_CTA`,
			`CLM_Event_Dashboard`.`Control_Revenue`,
			`CLM_Event_Dashboard`.`Control_Discount`,
			`CLM_Event_Dashboard`.`Control_Bills`,
			`CLM_Event_Dashboard`.`Created_Date`,
			`CLM_Event_Dashboard`.`Total_Customer_Base`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Day`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Month`,
			`CLM_Event_Dashboard`.`YM`,
			`CLM_Event_Dashboard`.`Incremental_Revenue_ST',@incr_rev_concat,'` AS `Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_ST',@incr_rev_concat,'` AS `MTD_Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Segment_ST',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Segment`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Revenue_Center_ST',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Revenue_Center`
		FROM `CLM_Event_Dashboard`;
        ');
        select @sql1;
		PREPARE statement from @sql1;
		Execute statement;
		Deallocate PREPARE statement;
    ELSE
		SET @sql1=CONCAT('
		CREATE OR REPLACE VIEW CLM_Event_Dashboard_STLT
        as 
		SELECT `CLM_Event_Dashboard`.`Revenue Center`,
			`CLM_Event_Dashboard`.`Segment`,
			`CLM_Event_Dashboard`.`Campaign`,
			`CLM_Event_Dashboard`.`EE_Date`,
			`CLM_Event_Dashboard`.`Event_Execution_Date_ID`,
			`CLM_Event_Dashboard`.`Theme`,
			`CLM_Event_Dashboard`.`Offer`,
			`CLM_Event_Dashboard`.`Channel`,
			`CLM_Event_Dashboard`.`Has_Recommendations`,
			`CLM_Event_Dashboard`.`Event_ID`,
			`CLM_Event_Dashboard`.`Control_Base` as `Control_Base`,
			`CLM_Event_Dashboard`.`Target_Base`,
			`CLM_Event_Dashboard`.`Target_Delivered`,
			`CLM_Event_Dashboard`.`Target_Delivered_NA`,
			`CLM_Event_Dashboard`.`Control_Responder` as `Control_Responder`,
			`CLM_Event_Dashboard`.`Target_Responder`,
			`CLM_Event_Dashboard`.`Target_Revenue`,
			`CLM_Event_Dashboard`.`Target_Discount`,
			`CLM_Event_Dashboard`.`Target_Bills`,
			`CLM_Event_Dashboard`.`Target_Open`,
			`CLM_Event_Dashboard`.`Target_CTA`,
			`CLM_Event_Dashboard`.`Control_Revenue`,
			`CLM_Event_Dashboard`.`Control_Discount`,
			`CLM_Event_Dashboard`.`Control_Bills`,
			`CLM_Event_Dashboard`.`Created_Date`,
			`CLM_Event_Dashboard`.`Total_Customer_Base`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Day`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Month`,
			`CLM_Event_Dashboard`.`YM`,
			`CLM_Event_Dashboard`.`Incremental_Revenue',@incr_rev_concat,'` AS `Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue',@incr_rev_concat,'` AS `MTD_Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Segment',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Segment`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Revenue_Center',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Revenue_Center`
		FROM `CLM_Event_Dashboard`;
        ');
        select @sql1;
		PREPARE statement from @sql1;
		Execute statement;
		Deallocate PREPARE statement;
    END IF;
     SET @wherecond1 = ' 1=1 ';
	SET @wherecond2 = ' 1=1 ';
    
    IF IN_AGGREGATION_6 = 'ALLCHANNEL'
    THEN 
    SET IN_AGGREGATION_6 = '';
    END IF;
	IF IN_AGGREGATION_2 = 'ALL'
    THEN 
    SET IN_AGGREGATION_2 = '';
    END IF;
    

CREATE OR REPLACE VIEW V_Dashboard_CLM_Event_Conversions_Report AS
    SELECT 
        CASE
            WHEN DATEDIFF(End_Date_ID, Start_Date_ID) < 32 THEN 'GTM'
            ELSE 'CLM'
        END AS `Category`,
        b.Name AS `Trigger`,
        a.*
    FROM
        CLM_Event_Dashboard_STLT a
            JOIN
        Event_Master b ON a.Event_ID = b.ID;
        
CREATE OR REPLACE VIEW V_Dashboard_CLM_Campaign_Segments_Conversions_Report AS
    SELECT 
        CASE
            WHEN DATEDIFF(End_Date_ID, Start_Date_ID) < 32 THEN 'GTM'
            ELSE 'CLM'
        END AS `Category`,
        b.Name AS `Trigger`,
        a.*
    FROM
        CLM_Event_Dashboard_Campaign_Segment a
            JOIN
        Event_Master b ON a.Event_ID = b.ID;
   
	IF IN_AGGREGATION_1 = 'SEGMENTTABLE'
   THEN 
        IF IN_AGGREGATION_2 = '' or IN_AGGREGATION_2 = 'ALL'
        THEN
        SET @wherecond1 = ' 1=1 ';
        ELSE
        SET @wherecond1 = CONCAT(' Category="',IN_AGGREGATION_2,'" ');
        END IF;
        IF IN_AGGREGATION_3 = ''
        THEN
        SET @wherecond2 = ' 1=1 ';
        ELSE
        SET @wherecond2 = CONCAT(' Theme="',IN_AGGREGATION_3,'" ');
        END IF;
        IF IN_AGGREGATION_4 = ''
        THEN
        SET @wherecond3 = ' 1=1 ';
        ELSE
        SET @wherecond3 = CONCAT(' Campaign="',IN_AGGREGATION_4,'" ');
        END IF;
        IF IN_AGGREGATION_5 = ''
        THEN
        SET @wherecond4 = ' 1=1 ';
        ELSE
        SET @wherecond4 = CONCAT(' `Trigger`="',IN_AGGREGATION_5,'" ');
        END IF;
        IF IN_AGGREGATION_6 = ''
        THEN
        SET @wherecond5 = ' 1=1 ';
        ELSE
        SET @wherecond5 = CONCAT(' Channel="',IN_AGGREGATION_6,'" ');
        END IF;
   END IF;
   
   IF IN_AGGREGATION_1 = 'CAMPAIGNSEGMENTTABLE'
   THEN 
        IF IN_AGGREGATION_2 = '' or IN_AGGREGATION_2 = 'ALL'
        THEN
        SET @wherecond1 = ' 1=1 ';
        ELSE
        SET @wherecond1 = CONCAT(' Category="',IN_AGGREGATION_2,'" ');
        END IF;
        IF IN_AGGREGATION_3 = ''
        THEN
        SET @wherecond2 = ' 1=1 ';
        ELSE
        SET @wherecond2 = CONCAT(' Theme="',IN_AGGREGATION_3,'" ');
        END IF;
        IF IN_AGGREGATION_4 = ''
        THEN
        SET @wherecond3 = ' 1=1 ';
        ELSE
        SET @wherecond3 = CONCAT(' Campaign="',IN_AGGREGATION_4,'" ');
        END IF;
        IF IN_AGGREGATION_5 = ''
        THEN
        SET @wherecond4 = ' 1=1 ';
        ELSE
        SET @wherecond4 = CONCAT(' `Trigger`="',IN_AGGREGATION_5,'" ');
        END IF;
        IF IN_AGGREGATION_6 = ''
        THEN
        SET @wherecond5 = ' 1=1 ';
        ELSE
        SET @wherecond5 = CONCAT(' Channel="',IN_AGGREGATION_6,'" ');
        END IF;
   END IF;
   
   IF IN_AGGREGATION_1 = 'TRIGGERTABLE'
   THEN 
         IF IN_AGGREGATION_2 = '' or IN_AGGREGATION_2 = 'ALL'
        THEN
        SET @wherecond1 = ' 1=1 ';
        ELSE
        SET @wherecond1 = CONCAT(' Category="',IN_AGGREGATION_2,'" ');
        END IF;
        IF IN_AGGREGATION_3 = ''
        THEN
        SET @wherecond2 = ' 1=1 ';
        ELSE
        SET @wherecond2 = CONCAT(' Theme="',IN_AGGREGATION_3,'" ');
        END IF;
        IF IN_AGGREGATION_4 = ''
        THEN
        SET @wherecond3 = ' 1=1 ';
        ELSE
        SET @wherecond3 = CONCAT(' Campaign="',IN_AGGREGATION_4,'" ');
        END IF;
        IF IN_AGGREGATION_5 = ''
        THEN
        SET @wherecond4 = ' 1=1 ';
        ELSE
        SET @wherecond4 = CONCAT(' `Trigger`="',IN_AGGREGATION_5,'" ');
        END IF;
        IF IN_AGGREGATION_6 = ''
        THEN
        SET @wherecond5 = ' 1=1 ';
        ELSE
        SET @wherecond5 = CONCAT(' Channel="',IN_AGGREGATION_6,'" ');
        END IF;
   END IF;
   
   IF IN_AGGREGATION_1 = 'CHANNELTABLE'
   THEN 
         IF IN_AGGREGATION_2 = '' or IN_AGGREGATION_2 = 'ALL'
        THEN
        SET @wherecond1 = ' 1=1 ';
        ELSE
        SET @wherecond1 = CONCAT(' Category="',IN_AGGREGATION_2,'" ');
        END IF;
        IF IN_AGGREGATION_3 = ''
        THEN
        SET @wherecond2 = ' 1=1 ';
        ELSE
        SET @wherecond2 = CONCAT(' Theme="',IN_AGGREGATION_3,'" ');
        END IF;
        IF IN_AGGREGATION_4 = ''
        THEN
        SET @wherecond3 = ' 1=1 ';
        ELSE
        SET @wherecond3 = CONCAT(' Campaign="',IN_AGGREGATION_4,'" ');
        END IF;
        IF IN_AGGREGATION_5 = ''
        THEN
        SET @wherecond4 = ' 1=1 ';
        ELSE
        SET @wherecond4 = CONCAT(' `Trigger`="',IN_AGGREGATION_5,'" ');
        END IF;
        IF IN_AGGREGATION_6 = ''
        THEN
        SET @wherecond5 = ' 1=1 ';
        ELSE
        SET @wherecond5 = CONCAT(' Channel="',IN_AGGREGATION_6,'" ');
        END IF;
   END IF;
   
   IF IN_AGGREGATION_1 = 'CATEGORYTABLE'
   THEN 
        IF IN_AGGREGATION_2 = '' or IN_AGGREGATION_2 = 'ALL'
        THEN
        SET @wherecond1 = ' 1=1 ';
        ELSE
        SET @wherecond1 = CONCAT(' Category="',IN_AGGREGATION_2,'" ');
        END IF;
        IF IN_AGGREGATION_3 = ''
        THEN
        SET @wherecond2 = ' 1=1 ';
        ELSE
        SET @wherecond2 = CONCAT(' Theme="',IN_AGGREGATION_3,'" ');
        END IF;
        IF IN_AGGREGATION_4 = ''
        THEN
        SET @wherecond3 = ' 1=1 ';
        ELSE
        SET @wherecond3 = CONCAT(' Campaign="',IN_AGGREGATION_4,'" ');
        END IF;
        IF IN_AGGREGATION_5 = ''
        THEN
        SET @wherecond4 = ' 1=1 ';
        ELSE
        SET @wherecond4 = CONCAT(' `Trigger`="',IN_AGGREGATION_5,'" ');
        END IF;
        IF IN_AGGREGATION_6 = ''
        THEN
        SET @wherecond5 = ' 1=1 ';
        ELSE
        SET @wherecond5 = CONCAT(' Channel="',IN_AGGREGATION_6,'" ');
        END IF;
   END IF;
   
   IF IN_AGGREGATION_1 = 'PRODUCTTABLE'
   THEN 
        IF IN_AGGREGATION_2 = '' or IN_AGGREGATION_2 = 'ALL'
        THEN
        SET @wherecond1 = ' 1=1 ';
        ELSE
        SET @wherecond1 = CONCAT(' Category="',IN_AGGREGATION_2,'" ');
        END IF;
        IF IN_AGGREGATION_3 = ''
        THEN
        SET @wherecond2 = ' 1=1 ';
        ELSE
        SET @wherecond2 = CONCAT(' Theme="',IN_AGGREGATION_3,'" ');
        END IF;
        IF IN_AGGREGATION_4 = ''
        THEN
        SET @wherecond3 = ' 1=1 ';
        ELSE
        SET @wherecond3 = CONCAT(' Campaign="',IN_AGGREGATION_4,'" ');
        END IF;
        IF IN_AGGREGATION_5 = ''
        THEN
        SET @wherecond4 = ' 1=1 ';
        ELSE
        SET @wherecond4 = CONCAT(' `Trigger`="',IN_AGGREGATION_5,'" ');
        END IF;
        IF IN_AGGREGATION_6 = ''
        THEN
        SET @wherecond5 = ' 1=1 ';
        ELSE
        SET @wherecond5 = CONCAT(' Channel="',IN_AGGREGATION_6,'" ');
        END IF;
   END IF;
   
   IF IN_AGGREGATION_1 = 'SALESCHANNELTABLE'
   THEN 
        IF IN_AGGREGATION_2 = '' or IN_AGGREGATION_2 = 'ALL'
        THEN
        SET @wherecond1 = ' 1=1 ';
        ELSE
        SET @wherecond1 = CONCAT(' Category="',IN_AGGREGATION_2,'" ');
        END IF;
        IF IN_AGGREGATION_3 = ''
        THEN
        SET @wherecond2 = ' 1=1 ';
        ELSE
        SET @wherecond2 = CONCAT(' Theme="',IN_AGGREGATION_3,'" ');
        END IF;
        IF IN_AGGREGATION_4 = ''
        THEN
        SET @wherecond3 = ' 1=1 ';
        ELSE
        SET @wherecond3 = CONCAT(' Campaign="',IN_AGGREGATION_4,'" ');
        END IF;
        IF IN_AGGREGATION_5 = ''
        THEN
        SET @wherecond4 = ' 1=1 ';
        ELSE
        SET @wherecond4 = CONCAT(' `Trigger`="',IN_AGGREGATION_5,'" ');
        END IF;
        IF IN_AGGREGATION_6 = ''
        THEN
        SET @wherecond5 = ' 1=1 ';
        ELSE
        SET @wherecond5 = CONCAT(' Channel="',IN_AGGREGATION_6,'" ');
        END IF;
   END IF;
   
   IF IN_AGGREGATION_1 = 'STORETABLE'
   THEN 
        IF IN_AGGREGATION_2 = '' or IN_AGGREGATION_2 = 'ALL'
        THEN
        SET @wherecond1 = ' 1=1 ';
        ELSE
        SET @wherecond1 = CONCAT(' Category="',IN_AGGREGATION_2,'" ');
        END IF;
        IF IN_AGGREGATION_3 = ''
        THEN
        SET @wherecond2 = ' 1=1 ';
        ELSE
        SET @wherecond2 = CONCAT(' Theme="',IN_AGGREGATION_3,'" ');
        END IF;
        IF IN_AGGREGATION_4 = ''
        THEN
        SET @wherecond3 = ' 1=1 ';
        ELSE
        SET @wherecond3 = CONCAT(' Campaign="',IN_AGGREGATION_4,'" ');
        END IF;
        IF IN_AGGREGATION_5 = ''
        THEN
        SET @wherecond4 = ' 1=1 ';
        ELSE
        SET @wherecond4 = CONCAT(' `Trigger`="',IN_AGGREGATION_5,'" ');
        END IF;
        IF IN_AGGREGATION_6 = ''
        THEN
        SET @wherecond5 = ' 1=1 ';
        ELSE
        SET @wherecond5 = CONCAT(' Channel="',IN_AGGREGATION_6,'" ');
        END IF;
   END IF;

   IF IN_AGGREGATION_1 = 'CARD'
   THEN 
        IF IN_AGGREGATION_2 = '' or IN_AGGREGATION_2 = 'ALL'
        THEN
        SET @wherecond1 = ' 1=1 ';
        SET @selectquery1 = '';
        ELSE
        SET @wherecond1 = CONCAT(' Category="',IN_AGGREGATION_2,'" ');
        SET @selectquery1 = CONCAT('`Category`,');
        END IF;
        IF IN_AGGREGATION_3 = ''
        THEN
        SET @wherecond2 = ' 1=1 ';
        SET @selectquery2 = '';
        ELSE
        SET @wherecond2 = CONCAT(' Theme="',IN_AGGREGATION_3,'" ');
        SET @selectquery2 = CONCAT('`Theme`,');
        END IF;
        IF IN_AGGREGATION_4 = ''
        THEN
        SET @wherecond3 = ' 1=1 ';
        SET @selectquery3 = '';
        ELSE
        SET @wherecond3 = CONCAT(' Campaign="',IN_AGGREGATION_4,'" ');
        SET @selectquery3 = CONCAT('`Campaign`,');
        END IF;
        IF IN_AGGREGATION_5 = ''
        THEN
        SET @wherecond4 = ' 1=1 ';
        SET @selectquery4 = '';
        ELSE
        SET @wherecond4 = CONCAT(' `Trigger`="',IN_AGGREGATION_5,'" ');
        SET @selectquery4 = CONCAT('`Trigger`,');
        END IF;
        IF IN_AGGREGATION_6 = ''
        THEN
        SET @wherecond5 = ' 1=1 ';
        SET @selectquery5 = '';
        ELSE
        SET @wherecond5 = CONCAT(' Channel="',IN_AGGREGATION_6,'" ');
        SET @selectquery5 = CONCAT('`Channel`,');
        END IF;
   END IF;
   
   IF IN_AGGREGATION_1 = 'BASE'
   THEN 
        IF IN_AGGREGATION_2 = '' or IN_AGGREGATION_2 = 'ALL'
        THEN
        SET @wherecond1 = ' 1=1 ';
        ELSE
        SET @wherecond1 = CONCAT(' Category="',IN_AGGREGATION_2,'" ');
        END IF;
        IF IN_AGGREGATION_3 = ''
        THEN
        SET @wherecond2 = ' 1=1 ';
        ELSE
        SET @wherecond2 = CONCAT(' Theme="',IN_AGGREGATION_3,'" ');
        END IF;
        IF IN_AGGREGATION_4 = ''
        THEN
        SET @wherecond3 = ' 1=1 ';
        ELSE
        SET @wherecond3 = CONCAT(' Campaign="',IN_AGGREGATION_4,'" ');
        END IF;
        IF IN_AGGREGATION_5 = ''
        THEN
        SET @wherecond4 = ' 1=1 ';
        ELSE
        SET @wherecond4 = CONCAT(' `Trigger`="',IN_AGGREGATION_5,'" ');
        END IF;
        IF IN_AGGREGATION_6 = ''
        THEN
        SET @wherecond5 = ' 1=1 ';
        ELSE
        SET @wherecond5 = CONCAT(' Channel="',IN_AGGREGATION_6,'" ');
        END IF;
   END IF;
   
   IF IN_AGGREGATION_1 = 'OUTREACHESBYCHANNEL'
   THEN  
        IF IN_AGGREGATION_2 = '' or IN_AGGREGATION_2 = 'ALL'
        THEN
        SET @wherecond1 = ' 1=1 ';
        ELSE
        SET @wherecond1 = CONCAT(' Category="',IN_AGGREGATION_2,'" ');
        END IF;
        IF IN_AGGREGATION_3 = ''
        THEN
        SET @wherecond2 = ' 1=1 ';
        ELSE
        SET @wherecond2 = CONCAT(' Theme="',IN_AGGREGATION_3,'" ');
        END IF;
        IF IN_AGGREGATION_4 = ''
        THEN
        SET @wherecond3 = ' 1=1 ';
        ELSE
        SET @wherecond3 = CONCAT(' Campaign="',IN_AGGREGATION_4,'" ');
        END IF;
        IF IN_AGGREGATION_5 = ''
        THEN
        SET @wherecond4 = ' 1=1 ';
        ELSE
        SET @wherecond4 = CONCAT(' `Trigger`="',IN_AGGREGATION_5,'" ');
        END IF;
        IF IN_AGGREGATION_6 = ''
        THEN
        SET @wherecond5 = ' 1=1 ';
        ELSE
        SET @wherecond5 = CONCAT(' Channel="',IN_AGGREGATION_6,'" ');
        END IF;
   END IF;
   
   IF IN_AGGREGATION_1 = 'OUTREACHESBYSEGMENT'
   THEN 
        IF IN_AGGREGATION_2 = '' or IN_AGGREGATION_2 = 'ALL'
        THEN
        SET @wherecond1 = ' 1=1 ';
        ELSE
        SET @wherecond1 = CONCAT(' Category="',IN_AGGREGATION_2,'" ');
        END IF;
        IF IN_AGGREGATION_3 = ''
        THEN
        SET @wherecond2 = ' 1=1 ';
        ELSE
        SET @wherecond2 = CONCAT(' Theme="',IN_AGGREGATION_3,'" ');
        END IF;
        IF IN_AGGREGATION_4 = ''
        THEN
        SET @wherecond3 = ' 1=1 ';
        ELSE
        SET @wherecond3 = CONCAT(' Campaign="',IN_AGGREGATION_4,'" ');
        END IF;
        IF IN_AGGREGATION_5 = ''
        THEN
        SET @wherecond4 = ' 1=1 ';
        ELSE
        SET @wherecond4 = CONCAT(' `Trigger`="',IN_AGGREGATION_5,'" ');
        END IF;
        IF IN_AGGREGATION_6 = ''
        THEN
        SET @wherecond5 = ' 1=1 ';
        ELSE
        SET @wherecond5 = CONCAT(' Channel="',IN_AGGREGATION_6,'" ');
        END IF;
   END IF;
    
    IF IN_AGGREGATION_1 = 'OUTREACHESBYCHANNEL' AND IN_MEASURE = 'GRAPH' 
	THEN 
        
        set @view_stmt= CONCAT(' create or replace view V_Dashboard_Conversions_Graph as 
							select 
							A.Channel AS "Channel",
							round((A.c1*100)/B.c2,2) as "Value" 
							from 
							(
							(
							SELECT Channel,
							SUM(Target_Base) as c1
							from V_Dashboard_CLM_Event_Conversions_Report
							WHERE EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
                            AND ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'
							group by 1
							)A
							,
							(
							SELECT SUM(Target_Base) as c2
							from V_Dashboard_CLM_Event_Conversions_Report
							WHERE EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
                            AND ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'
							)B
							)
							group by A.Channel;
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`Channel`,"Value",`Value`) )),"]")into @temp_result from V_Dashboard_Conversions_Graph;' ;

	END IF;
            
    IF IN_AGGREGATION_1 = 'OUTREACHESBYSEGMENT' AND IN_MEASURE = 'GRAPH' 
	THEN 
        
        set @view_stmt= CONCAT(' create or replace view V_Dashboard_Conversions_Graph as 
							select 
							A.Segment AS "Segment",
							round((A.c1*100)/B.c2,2) as "Value" 
							from 
							(
							(
							SELECT Segment,
							SUM(Target_Base) as c1
							from V_Dashboard_CLM_Event_Conversions_Report
							WHERE EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
                            AND ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'
							group by 1
							)A
							,
							(
							SELECT SUM(Target_Base) as c2
							from V_Dashboard_CLM_Event_Conversions_Report
							WHERE EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
                            AND ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'
							)B
							)
							group by A.Segment;
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`Segment`,"Value",`Value`) )),"]")into @temp_result from V_Dashboard_Conversions_Graph;' ;

	END IF;
    
    
	IF IN_AGGREGATION_1 = 'TRIGGERTABLE' AND IN_MEASURE = 'TABLE'
	THEN 
    
		set @view_stmt= CONCAT(' create or replace view V_Dashboard_Insights_Conversions as 
							WITH GROUP1 AS
							(
							select `Trigger`, COUNT(DISTINCT Bill_Header_ID) AS Bills, SUM(Sale_Net_Val) AS Amount, SUM(Sale_Net_Val)/COUNT(DISTINCT Bill_Header_ID) AS ABV
							FROM Dashboard_Insights_Conversions
                            WHERE ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'
							AND EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
							GROUP BY 1
							)
							,
							GROUP2 AS
							(
							select 
							A.`Trigger`,
							(A.c1*100)/B.c2 as BillsPercent 
							from 
							(
							(
							SELECT `Trigger`,
							COUNT(DISTINCT Bill_Header_ID) as c1
							FROM Dashboard_Insights_Conversions
                            WHERE ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'
							AND EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
							group by 1
							)A
							,
							(
							SELECT COUNT(DISTINCT Bill_Header_ID) as c2
							from Dashboard_Insights_Conversions
                            WHERE ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'
							AND EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
							)B
							)
							group by A.`Trigger`
							)
							,
							GROUP3 AS
							(
							select 
							A.`Trigger`,
							(A.c1*100)/B.c2 as AmtPercent 
							from 
							(
							(
							SELECT `Trigger`,
							SUM(Sale_Net_Val) as c1
							FROM Dashboard_Insights_Conversions
                            WHERE ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'
							AND EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
							group by 1
							)A
							,
							(
							SELECT SUM(Sale_Net_Val) as c2
							from Dashboard_Insights_Conversions
                            WHERE ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'
							AND EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
							)B
							)
							group by A.`Trigger`
							)
							SELECT A.`Trigger`, A.Bills, A.Amount, A.ABV, BillsPercent, AmtPercent
							FROM GROUP1 A
							JOIN GROUP2 B
							ON A.`Trigger`=B.`Trigger`
							JOIN GROUP3 C
							ON B.`Trigger`=C.`Trigger`
							ORDER BY 2 DESC LIMIT 10;
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Trigger",`Trigger`,"Bills",CAST(format(ifnull(`Bills`,0),",") AS CHAR),"Amt",CAST(format(ifnull(`Amount`,0),",") AS CHAR),"ABV",round(ifnull(`ABV`,0),0),"% Bills",concat(round(ifnull(`BillsPercent`,0),2),"%"),"% Amt",concat(round(ifnull(`AmtPercent`,0),2),"%")) )),"]")into @temp_result from V_Dashboard_Insights_Conversions;' ;
            
		END IF;
    
    IF IN_AGGREGATION_1 = 'CHANNELTABLE' AND IN_MEASURE = 'TABLE'
	THEN 
    
		set @view_stmt= CONCAT(' create or replace view V_Dashboard_Insights_Conversions as 
							WITH GROUP1 AS
							(
							select `Channel`, COUNT(DISTINCT Bill_Header_ID) AS Bills, SUM(Sale_Net_Val) AS Amount, SUM(Sale_Net_Val)/COUNT(DISTINCT Bill_Header_ID) AS ABV
							FROM Dashboard_Insights_Conversions
                            WHERE ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'
							AND EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
							GROUP BY 1
							)
							,
							GROUP2 AS
							(
							select 
							A.`Channel`,
							(A.c1*100)/B.c2 as BillsPercent 
							from 
							(
							(
							SELECT `Channel`,
							COUNT(DISTINCT Bill_Header_ID) as c1
							FROM Dashboard_Insights_Conversions
                            WHERE ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'
							AND EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
							group by 1
							)A
							,
							(
							SELECT COUNT(DISTINCT Bill_Header_ID) as c2
							from Dashboard_Insights_Conversions
                            WHERE ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'
							AND EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
							)B
							)
							group by A.`Channel`
							)
							,
							GROUP3 AS
							(
							select 
							A.`Channel`,
							(A.c1*100)/B.c2 as AmtPercent 
							from 
							(
							(
							SELECT `Channel`,
							SUM(Sale_Net_Val) as c1
							FROM Dashboard_Insights_Conversions
                            WHERE ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'
							AND EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
							group by 1
							)A
							,
							(
							SELECT SUM(Sale_Net_Val) as c2
							from Dashboard_Insights_Conversions
                            WHERE ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'
							AND EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
							)B
							)
							group by A.`Channel`
							)
							SELECT A.`Channel`, A.Bills, A.Amount, A.ABV, BillsPercent, AmtPercent
							FROM GROUP1 A
							JOIN GROUP2 B
							ON A.`Channel`=B.`Channel`
							JOIN GROUP3 C
							ON B.`Channel`=C.`Channel`;
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Outreach Channel",`Channel`,"Bills",CAST(format(ifnull(`Bills`,0),",") AS CHAR),"Amt",CAST(format(ifnull(`Amount`,0),",") AS CHAR),"ABV",round(ifnull(`ABV`,0),0),"% Bills",concat(round(ifnull(`BillsPercent`,0),2),"%"),"% Amt",concat(round(ifnull(`AmtPercent`,0),2),"%")) )),"]")into @temp_result from V_Dashboard_Insights_Conversions;' ;
            
		END IF;
        
	IF IN_AGGREGATION_1 = 'SEGMENTTABLE' AND IN_MEASURE = 'TABLE'
	THEN 
    
		set @view_stmt= CONCAT(' create or replace view V_Dashboard_Insights_Conversions as 
								select Segment, SUM(Target_Base) AS Target, SUM(Target_Bills) AS Conversion, SUM(Target_Bills)*100/SUM(Target_Base) AS Conv_Percent, SUM(Target_Revenue)/SUM(Target_Bills) AS ABV, SUM(Target_Revenue) AS Amount
								FROM V_Dashboard_CLM_Event_Conversions_Report
								WHERE ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'
								AND EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
								GROUP BY 1;
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Segment",`Segment`,"Target",CAST(format(ifnull(`Target`,0),",") AS CHAR),"Conversion",CAST(format(ifnull(`Conversion`,0),",") AS CHAR),"% Conversion",concat(round(`Conv_Percent`,2),"%"),"ABV",round(ifnull(`ABV`,0),0),"Amt",CAST(format(ifnull(`Amount`,0),",") AS CHAR)) )),"]")into @temp_result from V_Dashboard_Insights_Conversions;' ;
            
		END IF;
        
	IF IN_AGGREGATION_1 = 'CAMPAIGNSEGMENTTABLE' AND IN_MEASURE = 'TABLE'
	THEN 
    
		set @view_stmt= CONCAT(' create or replace view V_Dashboard_Insights_Conversions as 
								select Segment_Name, SUM(Target_Base) AS Target, SUM(Target_Bills) AS Conversion, SUM(Target_Bills)*100/SUM(Target_Base) AS Conv_Percent, SUM(Target_Revenue)/SUM(Target_Bills) AS ABV, SUM(Target_Revenue) AS Amount
								FROM V_Dashboard_CLM_Campaign_Segments_Conversions_Report A
                                LEFT JOIN Campaign_Segment B
                                ON A.Campaign_Segment_ID = B.Segment_Id
								WHERE ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'
								AND EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
								GROUP BY 1;
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Campaign_Segment",ifnull(`Segment_Name`,"ALL"),"Target",CAST(format(ifnull(`Target`,0),",") AS CHAR),"Conversion",CAST(format(ifnull(`Conversion`,0),",") AS CHAR),"% Conversion",concat(round(`Conv_Percent`,2),"%"),"ABV",round(ifnull(`ABV`,0),0),"Amt",CAST(format(ifnull(`Amount`,0),",") AS CHAR)) )),"]")into @temp_result from V_Dashboard_Insights_Conversions;' ;
            
		END IF;
        
	IF IN_AGGREGATION_1 = 'CATEGORYTABLE' AND IN_MEASURE = 'TABLE'
	THEN 
    
		set @view_stmt= CONCAT(' create or replace view V_Dashboard_Insights_Conversions as 
							WITH GROUP1 AS
							(
							select `Product_Category`, COUNT(DISTINCT Bill_Header_ID) AS Bills, SUM(Sale_Net_Val) AS Amount, SUM(Sale_Net_Val)/COUNT(DISTINCT Bill_Header_ID) AS ABV
							FROM Dashboard_Insights_Conversions
                            WHERE ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'
							AND EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
							GROUP BY 1
							)
							,
							GROUP2 AS
							(
							select 
							A.`Product_Category`,
							(A.c1*100)/B.c2 as BillsPercent 
							from 
							(
							(
							select `Product_Category`, COUNT(DISTINCT Bill_Header_ID) AS c1
							FROM Dashboard_Insights_Conversions
                            WHERE ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'
							AND EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
							GROUP BY 1
							)A
							,
							(
							SELECT COUNT(DISTINCT Bill_Header_ID) AS c2
							FROM Dashboard_Insights_Conversions
                            WHERE ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'
							AND EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
							)B
							)
							group by A.`Product_Category`
							)
							,
							GROUP3 AS
							(
							select 
							A.`Product_Category`,
							(A.c1*100)/B.c2 as AmtPercent 
							from 
							(
							(
							select `Product_Category`, SUM(Sale_Net_Val) AS c1
							FROM Dashboard_Insights_Conversions
                            WHERE ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'
							AND EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
							GROUP BY 1
							)A
							,
							(
							SELECT SUM(Sale_Net_Val) AS c2
							FROM Dashboard_Insights_Conversions
                            WHERE ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'
							AND EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
							)B
							)
							group by A.`Product_Category`
							)
							SELECT A.`Product_Category`, A.Bills, A.Amount, A.ABV, BillsPercent, AmtPercent
							FROM GROUP1 A
							JOIN GROUP2 B
							ON A.`Product_Category`=B.`Product_Category`
							JOIN GROUP3 C
							ON B.`Product_Category`=C.`Product_Category`
                            ORDER BY 2 DESC LIMIT 10;
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Category",`Product_Category`,"Bills",CAST(format(ifnull(`Bills`,0),",") AS CHAR),"Amt",CAST(format(ifnull(`Amount`,0),",") AS CHAR),"ABV",round(ifnull(`ABV`,0),0),"% Bills",concat(round(`BillsPercent`,2),"%"),"% Amt",concat(round(`AmtPercent`,2),"%")) )),"]")into @temp_result from V_Dashboard_Insights_Conversions;' ;
            
		END IF;
        
	IF IN_AGGREGATION_1 = 'PRODUCTTABLE' AND IN_MEASURE = 'TABLE'
	THEN 
    
		set @view_stmt= CONCAT(' create or replace view V_Dashboard_Insights_Conversions as 
							WITH GROUP1 AS
							(
							select `Product`, COUNT(DISTINCT Bill_Header_ID) AS Bills, SUM(Sale_Net_Val) AS Amount, SUM(Sale_Net_Val)/COUNT(DISTINCT Bill_Header_ID) AS ABV
							FROM Dashboard_Insights_Conversions
                            WHERE ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'
							AND EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
							GROUP BY 1
							)
							,
							GROUP2 AS
							(
							select 
							A.`Product`,
							(A.c1*100)/B.c2 as BillsPercent 
							from 
							(
							(
							select `Product`, COUNT(DISTINCT Bill_Header_ID) AS c1
							FROM Dashboard_Insights_Conversions
                            WHERE ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'
							AND EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
							GROUP BY 1
							)A
							,
							(
							SELECT COUNT(DISTINCT Bill_Header_ID) AS c2
							FROM Dashboard_Insights_Conversions
                            WHERE ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'
							AND EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
							)B
							)
							group by A.`Product`
							)
							,
							GROUP3 AS
							(
							select 
							A.`Product`,
							(A.c1*100)/B.c2 as AmtPercent 
							from 
							(
							(
							select `Product`, SUM(Sale_Net_Val) AS c1
							FROM Dashboard_Insights_Conversions
                            WHERE ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'
							AND EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
							GROUP BY 1
							)A
							,
							(
							SELECT SUM(Sale_Net_Val) AS c2
							FROM Dashboard_Insights_Conversions
                            WHERE ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'
							AND EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
							)B
							)
							group by A.`Product`
							)
							SELECT A.`Product`, A.Bills, A.Amount, A.ABV, BillsPercent, AmtPercent
							FROM GROUP1 A
							JOIN GROUP2 B
							ON A.`Product`=B.`Product`
							JOIN GROUP3 C
							ON B.`Product`=C.`Product`
							ORDER BY 2 DESC LIMIT 10;
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Product",`Product`,"Bills",CAST(format(ifnull(`Bills`,0),",") AS CHAR),"Amt",CAST(format(ifnull(`Amount`,0),",") AS CHAR),"ABV",round(ifnull(`ABV`,0),0)
,"% Bills",concat(round(`BillsPercent`,2),"%"),"% Amt",concat(round(`AmtPercent`,2),"%")) )),"]")into @temp_result from V_Dashboard_Insights_Conversions;' ;
            
		END IF;
        
	IF IN_AGGREGATION_1 = 'SALESCHANNELTABLE' AND IN_MEASURE = 'TABLE'
	THEN 
    
		set @view_stmt= CONCAT(' create or replace view V_Dashboard_Insights_Conversions as 
							WITH GROUP1 AS
							(
							select `Transaction_Mode`, COUNT(DISTINCT Bill_Header_ID) AS Bills, SUM(Sale_Net_Val) AS Amount, SUM(Sale_Net_Val)/COUNT(DISTINCT Bill_Header_ID) AS ABV
							FROM Dashboard_Insights_Conversions
                            WHERE ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'
							AND EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
							GROUP BY 1
							)
							,
							GROUP2 AS
							(
							select 
							A.`Transaction_Mode`,
							(A.c1*100)/B.c2 as BillsPercent 
							from 
							(
							(
							select `Transaction_Mode`, COUNT(DISTINCT Bill_Header_ID) AS c1
							FROM Dashboard_Insights_Conversions
                            WHERE ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'
							AND EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
							GROUP BY 1
							)A
							,
							(
							SELECT COUNT(DISTINCT Bill_Header_ID) AS c2
							FROM Dashboard_Insights_Conversions
                            WHERE ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'
							AND EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
							)B
							)
							group by A.`Transaction_Mode`
							)
							,
							GROUP3 AS
							(
							select 
							A.`Transaction_Mode`,
							(A.c1*100)/B.c2 as AmtPercent 
							from 
							(
							(
							select `Transaction_Mode`, SUM(Sale_Net_Val) AS c1
							FROM Dashboard_Insights_Conversions
                            WHERE ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'
							AND EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
							GROUP BY 1
							)A
							,
							(
							SELECT SUM(Sale_Net_Val) AS c2
							FROM Dashboard_Insights_Conversions
                            WHERE ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'
							AND EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
							)B
							)
							group by A.`Transaction_Mode`
							)
							SELECT A.`Transaction_Mode`, A.Bills, A.Amount, A.ABV, BillsPercent, AmtPercent
							FROM GROUP1 A
							JOIN GROUP2 B
							ON A.`Transaction_Mode`=B.`Transaction_Mode`
							JOIN GROUP3 C
							ON B.`Transaction_Mode`=C.`Transaction_Mode`;
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Sales Channel",`Transaction_Mode`,"Bills",CAST(format(ifnull(`Bills`,0),",") AS CHAR),"Amt",CAST(format(ifnull(`Amount`,0),",") AS CHAR),"ABV",round(ifnull(`ABV`,0),0),"% Bills",concat(round(`BillsPercent`,2),"%"),"% Amt",concat(round(`AmtPercent`,2),"%")) )),"]")into @temp_result from V_Dashboard_Insights_Conversions;' ;
            
		END IF;
        
	IF IN_AGGREGATION_1 = 'STORETABLE' AND IN_MEASURE = 'TABLE'
	THEN 
    
		set @view_stmt= CONCAT(' create or replace view V_Dashboard_Insights_Conversions as 
							WITH GROUP1 AS
							(
							select `Store`, COUNT(DISTINCT Bill_Header_ID) AS Bills, SUM(Sale_Net_Val) AS Amount, SUM(Sale_Net_Val)/COUNT(DISTINCT Bill_Header_ID) AS ABV
							FROM Dashboard_Insights_Conversions
                            WHERE ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'
							AND EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
							GROUP BY 1
							)
							,
							GROUP2 AS
							(
							select 
							A.`Store`,
							(A.c1*100)/B.c2 as BillsPercent 
							from 
							(
							(
							select `Store`, COUNT(DISTINCT Bill_Header_ID) AS c1
							FROM Dashboard_Insights_Conversions
                            WHERE ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'
							AND EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
							GROUP BY 1
							)A
							,
							(
							SELECT COUNT(DISTINCT Bill_Header_ID) AS c2
							FROM Dashboard_Insights_Conversions
                            WHERE ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'
							AND EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
							)B
							)
							group by A.`Store`
							)
							,
							GROUP3 AS
							(
							select 
							A.`Store`,
							(A.c1*100)/B.c2 as AmtPercent 
							from 
							(
							(
							select `Store`, SUM(Sale_Net_Val) AS c1
							FROM Dashboard_Insights_Conversions
                            WHERE ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'
							AND EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
							GROUP BY 1
							)A
							,
							(
							SELECT SUM(Sale_Net_Val) AS c2
							FROM Dashboard_Insights_Conversions
                            WHERE ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'
							AND EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
							)B
							)
							group by A.`Store`
							)
							SELECT A.`Store`, A.Bills, A.Amount, A.ABV, BillsPercent, AmtPercent
							FROM GROUP1 A
							JOIN GROUP2 B
							ON A.`Store`=B.`Store`
							JOIN GROUP3 C
							ON B.`Store`=C.`Store`
							ORDER BY 2 DESC LIMIT 10;
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Store",`Store`,"Bills",CAST(format(ifnull(`Bills`,0),",") AS CHAR),"Amt",CAST(format(ifnull(`Amount`,0),",") AS CHAR),"ABV",round(ifnull(`ABV`,0),0),"% Bills",concat(round(`BillsPercent`,2),"%"),"% Amt",concat(round(`AmtPercent`,2),"%")) )),"]")into @temp_result from V_Dashboard_Insights_Conversions;' ;
            
		END IF;
        
	IF IN_AGGREGATION_1 = 'BASE' AND IN_MEASURE = 'CARDBASEHEADER'
    THEN
		set @view_stmt = concat('CREATE OR REPLACE VIEW V_Dashboard_Conversions_Report_Card AS
        select SUM(Target_Base) AS Outreaches FROM V_Dashboard_CLM_Event_Conversions_Report
WHERE EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
AND ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'');

		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Outreaches",CAST(format(ifnull(`Outreaches`,0),",") AS CHAR)) )),"]")into @temp_result from V_Dashboard_Conversions_Report_Card ',';') ;
		
	END IF;
    
	IF IN_AGGREGATION_1 = 'CARD' AND IN_MEASURE = 'CARDHEADER'
    THEN
		set @view_stmt1 = concat('CREATE OR REPLACE VIEW V_Dashboard_Conversions_Report_Card_1 AS
        select ',@selectquery1,'',@selectquery2,'',@selectquery3,'',@selectquery4,'',@selectquery5,'EE_Date, count(distinct Bill_Header_Id) AS `Conversions`, count(distinct Customer_Id) as `Customers`, SUM(Sale_Net_Val) AS `Sale Amt.`, SUM(Sale_Net_Val)/count(distinct Bill_Header_Id) AS `ABV`, SUM(Sale_Disc_Val) AS `Disc Amt.`, COUNT(distinct Bill_Date) AS Visits, count(distinct Bill_Details_Id) as `Items`, SUM(Sale_Qty) AS Qty, COUNT(DISTINCT Product_Id) AS Products
FROM Dashboard_Insights_Conversions
WHERE EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
AND ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'');

		SELECT @view_stmt1;
		PREPARE statement from @view_stmt1;
		Execute statement;
		Deallocate PREPARE statement;
        
        set @view_stmt2 = concat('CREATE OR REPLACE VIEW V_Dashboard_Conversions_Report_Card_2 AS
        select ',@selectquery1,'',@selectquery2,'',@selectquery3,'',@selectquery4,'',@selectquery5,'EE_Date, SUM(Target_Base) AS BASE
,MAX(MTD_Incremental_Revenue) AS Incremental_Revenue
FROM V_Dashboard_CLM_Event_Conversions_Report
WHERE EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"
AND ',@wherecond1,' and ',@wherecond2,' AND ',@wherecond3,' and ',@wherecond4,' AND ',@wherecond5,'');

		SELECT @view_stmt2;
		PREPARE statement from @view_stmt2;
		Execute statement;
		Deallocate PREPARE statement;
        
        set @view_stmt = concat('CREATE OR REPLACE VIEW V_Dashboard_Conversions_Report_Card AS
        select `Conversions`, `Conversions`*100/BASE AS `Conversion_Percent`,`Customers`, `Sale Amt.`, `ABV`, `Disc Amt.`,`Incremental_Revenue`, Visits, `Items`, Qty, Products
FROM V_Dashboard_Conversions_Report_Card_1 A, V_Dashboard_Conversions_Report_Card_2 B
WHERE A.EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"');

		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Conversions",CAST(format(ifnull(`Conversions`,0),",") AS CHAR),"%Conversion.",concat(round(`Conversion_Percent`,2),"%"),"Customers",CAST(format(ifnull(`Customers`,0),",") AS CHAR),"SaleAmt.",CAST(format(ifnull(`Sale Amt.`,0),",") AS CHAR),"ABV",round(ifnull(`ABV`,0),0),"Incremental_Revenue",CAST(format(round(ifnull(`Incremental_Revenue`,0),0),",") as char),"DiscAmt.",CAST(format(ifnull(`Disc Amt.`,0),",") AS CHAR),"Visits",CAST(format(ifnull(`Visits`,0),",") AS CHAR),"Items",CAST(format(ifnull(`Items`,0),",") AS CHAR),"Qty",CAST(format(ifnull(`Qty`,0),",") AS CHAR),"Products",CAST(format(ifnull(`Products`,0),",") AS CHAR)) )),"]")into @temp_result from V_Dashboard_Conversions_Report_Card ',';') ;
		
	END IF;
    
    IF IN_MEASURE = 'CATEGORY_NAME'
    THEN
		set @view_stmt = concat('CREATE OR REPLACE VIEW V_Dashboard_Conversions_Category_Names AS
								select CASE WHEN datediff(End_Date_ID,Start_Date_ID) < 32 THEN "GTM" else "CLM" END AS Category FROM Event_Master GROUP BY 1');
		SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Category",`Category`) )),"]")into @temp_result from V_Dashboard_Conversions_Category_Names;' ;
SELECT @Query_String;
    END IF;
   
    IF IN_MEASURE = 'THEME_NAME'
    THEN
		set @view_stmt = concat('CREATE OR REPLACE VIEW V_Dashboard_Conversions_Theme_Names AS
								select distinct Theme as CampaignThemeName from CLM_Event_Dashboard a join Event_Master b on a.Event_ID = b.ID WHERE EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"');
		SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Theme",CAST(`CampaignThemeName` AS CHAR)) )),"]") into @temp_result from V_Dashboard_Conversions_Theme_Names;' ;
SELECT @Query_String;
    END IF;
    
    IF IN_MEASURE = 'CAMPAIGN_NAME'
    THEN
		set @view_stmt = concat('CREATE OR REPLACE VIEW V_Dashboard_Conversions_Campaign_Names AS
								select distinct Campaign as CampaignName from CLM_Event_Dashboard a join Event_Master b on a.Event_ID = b.ID WHERE EE_Date between "',IN_FILTER_CURRENTMONTH,'" and "',IN_FILTER_MONTHDURATION,'"');
		SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Campaign",CAST(`CampaignName` AS CHAR)) )),"]") into @temp_result from V_Dashboard_Conversions_Campaign_Names;' ;
SELECT @Query_String;
    END IF;
    
    IF IN_MEASURE = 'TRIGGER_NAME' AND IN_AGGREGATION_7 <> "SEARCH"
    THEN
		set @view_stmt = concat('CREATE OR REPLACE VIEW V_Dashboard_Conversions_Trigger_Names AS
								select distinct b.Name as `TriggerName` from CLM_Event_Dashboard a join Event_Master b on a.Event_ID = b.ID');
		SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Trigger",CAST(`TriggerName` AS CHAR)) )),"]") into @temp_result from V_Dashboard_Conversions_Trigger_Names;' ;
SELECT @Query_String;
    END IF;
    
    IF IN_MEASURE = 'TRIGGER_NAME' AND IN_AGGREGATION_7 = "SEARCH"
    THEN
		set @view_stmt = concat('CREATE OR REPLACE VIEW V_Dashboard_Conversions_Trigger_Names AS
								select distinct b.Name as `TriggerName` 
                                from CLM_Event_Dashboard a join Event_Master b on a.Event_ID = b.ID
                                where b.Name like "%',IN_AGGREGATION_8,'"');
		SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Trigger",CAST(`TriggerName` AS CHAR)) )),"]") into @temp_result from V_Dashboard_Conversions_Trigger_Names;' ;
SELECT @Query_String;
    END IF;
    
    IF IN_MEASURE = 'CHANNEL_NAME'
    THEN
		set @view_stmt = concat('CREATE OR REPLACE VIEW V_Dashboard_Conversions_Channel_Names AS
								select distinct Channel as `ChannelName` from CLM_Event_Dashboard a join Event_Master b on a.Event_ID = b.ID');
		SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Channel",CAST(`ChannelName` AS CHAR)) )),"]") into @temp_result from V_Dashboard_Conversions_Channel_Names;' ;
SELECT @Query_String;
    END IF;



    IF IN_SCREEN <> "DOWNLOAD"
    THEN
    select @view_stmt;
	PREPARE statement from @view_stmt;
	Execute statement;
	Deallocate PREPARE statement;
	SELECT @Query_String;
	PREPARE statement from @Query_String;
	Execute statement;
	Deallocate PREPARE statement; 
    SET out_result_json = @temp_result;
    ELSE
		call S_dashboard_performance_download('V_Dashboard_Insights_Conversions',@out_result_json1);
		SELECT @out_result_json1 INTO out_result_json;
    END IF;
    
END$$
DELIMITER ;
