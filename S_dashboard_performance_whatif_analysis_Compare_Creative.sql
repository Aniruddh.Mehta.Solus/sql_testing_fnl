DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_performance_whatif_analysis_Compare_Creative`(IN in_request_json json, OUT out_result_json json)
BEGIN
    DECLARE IN_USER_NAME varchar(240); 
	DECLARE IN_SCREEN varchar(400); 
	DECLARE IN_FILTER_CURRENTMONTH text;
	DECLARE IN_FILTER_CHANNEL text; 
	DECLARE IN_FILTER_MONTHDURATION text; 
	DECLARE IN_MEASURE text; 
	DECLARE IN_NUMBER_FORMAT text;
	DECLARE IN_AGGREGATION_1 text; 
	DECLARE IN_AGGREGATION_2 text;
	DECLARE IN_AGGREGATION_3 text;
	DECLARE IN_AGGREGATION_4 text;
	DECLARE IN_AGGREGATION_5 text;
	DECLARE IN_AGGREGATION_6 text;
	DECLARE Query_String text;
    DECLARE Query_Measure_Name varchar(100);
	DECLARE IN_DEEP_FILTER text;
    
    DECLARE IN_CLM_SEGMENT text;
	DECLARE IN_CAMPAIGN_SEGMENT text;
	DECLARE IN_THEME text;
	DECLARE IN_OFFER text;
	DECLARE IN_OFFER_TYPE text;
	DECLARE IN_RECO_BASED_TRIGGER text;
	DECLARE IN_RECO_STORY_USED text;
	DECLARE IN_PERS_FIELD text;
	DECLARE IN_SO2_DYNAMIC text;
	DECLARE IN_SPECIFIC_PROD_PUSH text;
	DECLARE IN_WEEKEND_WEEKDAY text;
	DECLARE IN_DOW text;
	DECLARE IN_TRIGGER_WATERFAL_TYPE text;
	DECLARE IN_CONVERSION text;
	DECLARE IN_MIN_LIFT text;
	DECLARE IN_MIN_COVERAGE text;        
	DECLARE IN_COVERAGE text;
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
    SET IN_FILTER_CHANNEL = json_unquote(json_extract(in_request_json,"$.FILTER_CHANNEL"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_AGGREGATION_4 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_4"));
	SET IN_AGGREGATION_5 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_5"));
	SET IN_AGGREGATION_6 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_6"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    SET IN_DEEP_FILTER = json_unquote(json_extract(in_request_json,"$.DEEP_FILTER"));
    
    
	SET IN_CLM_SEGMENT = json_unquote(json_extract(in_request_json,"$.CLM_SEGMENT"));
	SET IN_CAMPAIGN_SEGMENT = json_unquote(json_extract(in_request_json,"$.CAMPAIGN_SEGMENT"));
	SET IN_THEME = json_unquote(json_extract(in_request_json,"$.THEME"));
	SET IN_OFFER = json_unquote(json_extract(in_request_json,"$.OFFER"));
	SET IN_OFFER_TYPE = json_unquote(json_extract(in_request_json,"$.OFFER_TYPE"));
	SET IN_RECO_BASED_TRIGGER = json_unquote(json_extract(in_request_json,"$.RECO_BASED_TRIGGER"));
	SET IN_RECO_STORY_USED = json_unquote(json_extract(in_request_json,"$.RECO_STORY_USED"));
	SET IN_PERS_FIELD = json_unquote(json_extract(in_request_json,"$.PERS_FIELD"));
	SET IN_SO2_DYNAMIC = json_unquote(json_extract(in_request_json,"$.SO2_DYNAMIC"));
	SET IN_SPECIFIC_PROD_PUSH = json_unquote(json_extract(in_request_json,"$.SPECIFIC_PROD_PUSH"));
	SET IN_WEEKEND_WEEKDAY = json_unquote(json_extract(in_request_json,"$.WEEKEND_WEEKDAY"));
	SET IN_DOW = json_unquote(json_extract(in_request_json,"$.DOW"));
	SET IN_TRIGGER_WATERFAL_TYPE = json_unquote(json_extract(in_request_json,"$.TRIGGER_WATERFAL_TYPE"));
	SET IN_CONVERSION = json_unquote(json_extract(in_request_json,"$.CONVERSION"));
	SET IN_MIN_LIFT = json_unquote(json_extract(in_request_json,"$.MIN_LIFT"));
	SET IN_MIN_COVERAGE = json_unquote(json_extract(in_request_json,"$.MIN_COVERAGE"));
	SET IN_COVERAGE = json_unquote(json_extract(in_request_json,"$.COVERAGE"));
    
    SET @Interval_month = CASE WHEN IN_AGGREGATION_2='M' THEN 0
							WHEN IN_AGGREGATION_2='L3M' THEN 2
                            WHEN IN_AGGREGATION_2='L12M' THEN 11 END;
                            
    
    
	SET @FY_SATRT_DATE = date_format(date_sub(concat(IN_FILTER_CURRENTMONTH,'01'), INTERVAL @Interval_month MONTH),"%Y%m");
	SET @FY_END_DATE = IN_FILTER_CURRENTMONTH;
    
    SELECT @FY_SATRT_DATE,@FY_END_DATE;
    SET @filter_cond=concat(" dash.YM between '",@FY_SATRT_DATE,"' and '",@FY_END_DATE,"'");
    
    IF IN_FILTER_CHANNEL = "ALL"
    THEN 
		SET IN_FILTER_CHANNEL = "";
	END IF;
    
    IF IN_FILTER_CHANNEL IS  NULL OR IN_FILTER_CHANNEL = ""
    THEN
		SET @filter_cond = @filter_cond;
    ELSE
		SET @filter_cond=concat(@filter_cond," AND dash.chan = '",IN_FILTER_CHANNEL,"'");
    END IF;
    SELECT @filter_cond;
    
    SET @incr_Rev =' round(SUM(Incremental_Revenue),0) ';
    
    
      select Value1 into @Incremental_Revenue_Method from UI_Configuration_Global where Config_Name = 'Incremental_Revenue_Method';
    SET @Incremental_Revenue_Method=ifnull(@Incremental_Revenue_Method,'Method2');
    select @Incremental_Revenue_Method;
    IF @Incremental_Revenue_Method = 'Method1'
    THEN
		SET @incr_rev_concat='_By_Day';
        SET @group_cond1='Event_Id,EE_Date';
        SET @mtd_concat='';
	ELSEIF @Incremental_Revenue_Method = 'Method2'
    THEN
		SET @group_cond1='Event_Id,YM';
		SET @incr_rev_concat='';
        SET @mtd_concat='MTD_';
	ELSE
		SET @incr_rev_concat='';
        SET @mtd_concat='MTD_';
		SET @incr_Rev = ' round((CASE WHEN SUM(Control_Base) < 0 or SUM(Target_Delivered) = 0 then 0 WHEN (((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*100 < 0 then 0 WHEN SUM(Control_Base)>0 and SUM(Control_Responder)=0 then SUM(Target_Revenue)
					ELSE ifnull((((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*(SUM(Target_Revenue)/SUM(Target_Responder))*(SUM(Target_Delivered)),0) END),0) ';
		SET @group_cond1='YM';
	END IF;
	
    SET @sql1=CONCAT('
		CREATE OR REPLACE VIEW Dashboard_Performance_Campaign_Insights_STLT
        as 
		SELECT
			`Client_Name`,
            `Campaign_Segment`,
            `Rcore_Story`,
            `Theme`,
            `Trigger_Type` as Waterfall, 
            `Has_Shortened_URL` as SO2 ,
            `Personalization_Score`,
			`Trigger`,
			`EE_Date`,
			`Chan`,
			`Type`,
			`Size`,
			`Recommendation`,
			`Pers`,
			`Offer`,
			`DOW_num`,
			`DOW`,
			`WEEKDAY_WEEKEND`,
			`Segment`,
			`Event_ID`,
			`Event_Execution_Date_ID`,
			`YM`,
			`Target_Base`,
			`Target_Delivered`,
			`Target_Bills`,
			`Conversion_Per`,
			`Incremental_Revenue',@incr_rev_concat,'` AS `Incremental_Revenue`,
			`',@mtd_concat,'Incremental_Revenue',@incr_rev_concat,'` AS `MTD_Incremental_Revenue`
		FROM `Dashboard_Performance_Campaign_Insights`;
        ');
        select @sql1;
		PREPARE statement from @sql1;
		Execute statement;
		Deallocate PREPARE statement;
    SET @create_view=CONCAT('CREATE OR REPLACE VIEW V_Dashboard_What_If_Incr_Rev
			as
			select 
            `Client_Name`,
            `Campaign_Segment`,
            `Rcore_Story`,
            `Theme`,
            Waterfall, 
            SO2 ,
            `Personalization_Score`,
            `Trigger`,
            `Event_Execution_Date_ID`,
			`Type`,
			`Size`,
			`Recommendation`,
			`Pers`,
			`Offer`,
			`DOW_num`,
			`DOW`,
			`WEEKDAY_WEEKEND`,
			`Segment`,
			`Conversion_Per`,
            MAX(EE_Date) as EE_Date,
            MAX(MTD_Incremental_Revenue) as Incremental_Revenue,
            SUM(Target_Delivered) as Target_Delivered,
            SUM(Target_Bills) as Target_Bills,
            SUM(Target_Base) as Target_Base,
            Event_ID,Chan,YM 
            from Dashboard_Performance_Campaign_Insights_STLT
            group by ',@group_cond1,';');
    
	SELECT @create_view;
	PREPARE statement from @create_view;
	Execute statement;
	Deallocate PREPARE statement;
    
    /*
    IF IN_DEEP_FILTER = "TRUE" AND IN_AGGREGATION_4 = 'SHOW_ALL_COLS'
    THEN
		SET @view_query = '';
        SET @json_query = '';
		IF IN_AGGREGATION_5 = "CLM_SEGMENT"
        THEN
			SET @view_query = 'SELECT DISTINCT SEGMENT FROM Dashboard_Performance_Campaign_Insights';
            SET @json_query = '"COLUMN_NAME",`SEGMENT`';
        END IF;
        IF IN_AGGREGATION_5 = "DOW"
        THEN
			SET @view_query = 'SELECT DISTINCT DOW FROM Dashboard_Performance_Campaign_Insights ORDER BY DOW_num';
            SET @json_query = '"COLUMN_NAME",`DOW`';
        END IF;
        IF IN_AGGREGATION_5 = "TRIGGER_WATERFAL_TYPE"
        THEN
			SET @view_query = 'SELECT DISTINCT Trigger_Type as Waterfall FROM Dashboard_Performance_Campaign_Insights';
            SET @json_query = '"COLUMN_NAME",`Waterfall`';
        END IF;
        IF IN_AGGREGATION_5 = "THEME"
        THEN
			SET @view_query = 'SELECT DISTINCT CampaignThemeName FROM Event_Master';
            SET @json_query = '"COLUMN_NAME",`CampaignThemeName`';
        END IF;
        
        
        SET @view_stmt = concat('
            CREATE OR REPLACE VIEW V_Dashboard_Whatif_Compare_Creative_Filter_COL_Picker 
            AS 
            (',@view_query,');');
		SET @Query_String =  concat('
            SELECT CONCAT("[",(GROUP_CONCAT(json_object(',@json_query,') )),"]")
            into @temp_result 
            from V_Dashboard_Whatif_Compare_Creative_Filter_COL_Picker;') ;
    END IF;
    
    */
    
    IF IN_DEEP_FILTER = "TRUE" AND IN_AGGREGATION_4 = 'SHOW_ALL_COLS'
    THEN
		SET @view_query = '';
        SET @json_query = '';
		IF IN_AGGREGATION_5 = "CLM_SEGMENT"
        THEN
			SET @view_query = 'SELECT DISTINCT SEGMENT FROM Dashboard_Performance_Campaign_Insights';
            SET @json_query = '"COLUMN_NAME",`SEGMENT`';
        END IF;
        IF IN_AGGREGATION_5 = "DOW"
        THEN
			SET @view_query = 'SELECT DISTINCT DOW FROM Dashboard_Performance_Campaign_Insights ORDER BY DOW_num';
            SET @json_query = '"COLUMN_NAME",`DOW`';
        END IF;
        IF IN_AGGREGATION_5 = "TRIGGER_WATERFAL_TYPE"
        THEN
			SET @view_query = 'SELECT DISTINCT Trigger_Type as Waterfall FROM Dashboard_Performance_Campaign_Insights';
            SET @json_query = '"COLUMN_NAME",`Waterfall`';
        END IF;
        IF IN_AGGREGATION_5 = "THEME"
        THEN
			SET @view_query = 'SELECT DISTINCT Theme FROM Dashboard_Performance_Campaign_Insights';
            SET @json_query = '"COLUMN_NAME",`Theme`';
        END IF;
        
        IF IN_AGGREGATION_5 = "CAMPAIGN_SEGMENT"
        THEN
			SET @view_query = 'select distinct Campaign_Segment from Dashboard_Performance_Campaign_Insights';
            SET @json_query = '"COLUMN_NAME",`Campaign_Segment`';
        END IF;
        
        IF IN_AGGREGATION_5 = "RECO_STORY"
        THEN
			SET @view_query = 'select distinct Rcore_Story from Dashboard_Performance_Campaign_Insights';
            SET @json_query = '"COLUMN_NAME",`Rcore_Story`';
        END IF;
        
        SET @view_stmt = concat('
            CREATE OR REPLACE VIEW V_Dashboard_Whatif_Compare_Creative_Filter_COL_Picker 
            AS 
            (',@view_query,');');
		SET @Query_String =  concat('
            SELECT CONCAT("[",(GROUP_CONCAT(json_object(',@json_query,') )),"]")
            into @temp_result 
            from V_Dashboard_Whatif_Compare_Creative_Filter_COL_Picker;') ;
    END IF;
    IF IN_DEEP_FILTER = "TRUE" AND IN_AGGREGATION_4 = ''
    THEN
		IF IN_CLM_SEGMENT <> "" AND IN_CLM_SEGMENT <> "DEFAULT"
        THEN
			SET IN_CLM_SEGMENT = REPLACE(IN_CLM_SEGMENT,'[','(');
            SET IN_CLM_SEGMENT = REPLACE(IN_CLM_SEGMENT,']',')');
            Select IN_CLM_SEGMENT;
			SET @CLM_cond = CONCAT(' 
            AND dash.Segment IN ',IN_CLM_SEGMENT,'');
        ELSE
			SET @CLM_cond = ' 
            and 1=1 -- CLM Segment';
        END IF;
        select @CLM_cond;
        SET @filter_cond = CONCAT(@filter_cond, @CLM_cond);
        
        IF IN_CAMPAIGN_SEGMENT <> "" AND IN_CAMPAIGN_SEGMENT <> "DEFAULT" -- TO BE DONE
        THEN
			SET IN_CAMPAIGN_SEGMENT = REPLACE(IN_CAMPAIGN_SEGMENT,'[','(');
            SET IN_CAMPAIGN_SEGMENT = REPLACE(IN_CAMPAIGN_SEGMENT,']',')');
            Select IN_CAMPAIGN_SEGMENT;
			SET @Camp_seg_cond = CONCAT(' 
            AND dash.Campaign_Segment IN ',IN_CAMPAIGN_SEGMENT,'');
        ELSE
			SET @Camp_seg_cond = ' 
            AND 1=1 -- Campaign Segemnt';
        END IF;
        select @Camp_seg_cond;
        SET @filter_cond = CONCAT(@filter_cond, @Camp_seg_cond);
        
        IF IN_THEME <> "" AND IN_THEME <> "DEFAULT"
        THEN
			SET IN_THEME = REPLACE(IN_THEME,'[','(');
            SET IN_THEME = REPLACE(IN_THEME,']',')');
            Select IN_THEME;
			SET @Theme_cond = CONCAT(' 
            AND dash.Theme IN ',IN_THEME,'');
        ELSE
			SET @Theme_cond = ' 
            AND 1=1 -- Theme';
        END IF;
        select @Theme_cond;
		SET @filter_cond = CONCAT(@filter_cond, @Theme_cond);
        
        IF IN_OFFER <> ""
        THEN
			IF IN_OFFER = "NO"
            THEN 
				SET @OFF_IN_COND = "'%NO%'";
				SET @Offer_cond = CONCAT(' 
                AND dash.offer like ',@OFF_IN_COND,'');
			ELSE 
				SET @OFF_IN_COND = "Offer";
                SET @Offer_cond = CONCAT(' 
                AND dash.offer = "',@OFF_IN_COND,'"');
            END IF;
        ELSE
			SET @Offer_cond = ' 
            AND 1=1 -- Offer';
        END IF;
        select @Offer_cond;
		SET @filter_cond = CONCAT(@filter_cond, @Offer_cond);
        
        IF IN_OFFER_TYPE <> "" 
        THEN
			SET @Offer_type_cond = CONCAT(' 
            AND dash.Offer_Type = "',IN_OFFER_TYPE,'"');
        ELSE
			SET @Offer_type_cond = ' 
            AND 1=1 -- Offer Type';
        END IF;
        select @Offer_type_cond;
		SET @filter_cond = CONCAT(@filter_cond, @Offer_type_cond);
        
        IF IN_RECO_BASED_TRIGGER <> ""
        THEN
			IF IN_RECO_BASED_TRIGGER = "YES"
            THEN 
				SET @IN_RECO_BASED_TRIGGER_IN_COND = "Reco";
			ELSE 
				SET @IN_RECO_BASED_TRIGGER_IN_COND = "Non-Reco";
            END IF;
            SET @IN_RECO_BASED_TRIGGER_cond = CONCAT(' 
                AND dash.Recommendation = "',@IN_RECO_BASED_TRIGGER_IN_COND,'"');
        ELSE
			SET @IN_RECO_BASED_TRIGGER_cond = ' 
            AND 1=1 -- RECO_BASED_TRIGGER';
        END IF;
        select @IN_RECO_BASED_TRIGGER_cond;
		SET @filter_cond = CONCAT(@filter_cond, @IN_RECO_BASED_TRIGGER_cond);
        
        IF IN_RECO_STORY_USED <> "" AND IN_RECO_STORY_USED <> "DEFAULT"-- TO BE DONE
        THEN
			SET IN_RECO_STORY_USED = REPLACE(IN_RECO_STORY_USED,'[','(');
            SET IN_RECO_STORY_USED = REPLACE(IN_RECO_STORY_USED,']',')');
            Select IN_RECO_STORY_USED;
			SET @IN_RECO_STORY_USED_cond = CONCAT(' 
            AND dash.Rcore_Story IN ',IN_RECO_STORY_USED,'');
        ELSE
			SET @IN_RECO_STORY_USED_cond = ' 
            AND 1=1 -- RECO_STORY_USED';
        END IF;
        select @IN_RECO_STORY_USED_cond;
		SET @filter_cond = CONCAT(@filter_cond, @IN_RECO_STORY_USED_cond);
        
        IF IN_PERS_FIELD <> ""
        THEN
			SET @IN_PERS_FIELD_cond = CONCAT(' 
            AND dash.Personalization_Score = ',IN_PERS_FIELD,'');
        ELSE
			SET @IN_PERS_FIELD_cond = ' 
            AND 1=1 -- PERS_FIELD';
        END IF;
        select @IN_PERS_FIELD_cond;
		SET @filter_cond = CONCAT(@filter_cond, @IN_PERS_FIELD_cond);
        
        IF IN_SO2_DYNAMIC <> "" 
        THEN
			SET @IN_SO2_DYNAMIC_cond = CONCAT(' 
            AND dash.SO2 =  "',IN_SO2_DYNAMIC,'"');
        ELSE
			SET @IN_SO2_DYNAMIC_cond = ' 
            AND 1=1 -- SO2_DYNAMIC';
        END IF;
        select @IN_SO2_DYNAMIC_cond;
		SET @filter_cond = CONCAT(@filter_cond, @IN_SO2_DYNAMIC_cond);
        
        
        
        IF IN_WEEKEND_WEEKDAY <> ""
        THEN
			SET @IN_WEEKEND_WEEKDAY_cond = CONCAT(' 
            AND dash.WEEKDAY_WEEKEND = "',IN_WEEKEND_WEEKDAY,'"');
        ELSE
			SET @IN_WEEKEND_WEEKDAY_cond = ' 
            AND 1=1 -- WEEKEND_WEEKDAY';
        END IF;
        select @IN_WEEKEND_WEEKDAY_cond;
		SET @filter_cond = CONCAT(@filter_cond, @IN_WEEKEND_WEEKDAY_cond);
        
        IF IN_DOW <> "" AND IN_DOW <> "DEFAULT"
        THEN
			SET IN_DOW = REPLACE(IN_DOW,'[','(');
            SET IN_DOW = REPLACE(IN_DOW,']',')');
            Select IN_DOW;
			SET @IN_DOW_cond = CONCAT(' 
            AND dash.DOW in ',IN_DOW,'');
        ELSE
			SET @IN_DOW_cond = ' 
            AND 1=1 -- DOW';
        END IF;
        select @IN_DOW_cond;
		SET @filter_cond = CONCAT(@filter_cond, @IN_DOW_cond);
        
        IF IN_TRIGGER_WATERFAL_TYPE <> "" AND IN_TRIGGER_WATERFAL_TYPE <> "DEFAULT"
        THEN
			SET IN_TRIGGER_WATERFAL_TYPE = REPLACE(IN_TRIGGER_WATERFAL_TYPE,'[','(');
            SET IN_TRIGGER_WATERFAL_TYPE = REPLACE(IN_TRIGGER_WATERFAL_TYPE,']',')');
            Select IN_TRIGGER_WATERFAL_TYPE;
			SET @IN_TRIGGER_WATERFAL_TYPE_cond = CONCAT(' 
            AND dash.Waterfall IN ',IN_TRIGGER_WATERFAL_TYPE,'');
        ELSE
			SET @IN_TRIGGER_WATERFAL_TYPE_cond = ' 
            AND 1=1 -- TRIGGER_WATERFAL_TYPE';
        END IF;
        select @IN_TRIGGER_WATERFAL_TYPE_cond;
		SET @filter_cond = CONCAT(@filter_cond, @IN_TRIGGER_WATERFAL_TYPE_cond);
        
        SET @new_filter_cond_final_view = concat("1=1 ");
        IF IN_MIN_LIFT <> ""
        THEN
			SET @IN_MIN_LIFT_cond = CONCAT(' 
            and Lift > ',IN_MIN_LIFT,'');
        ELSE
			SET @IN_MIN_LIFT_cond = ' 
            and 1=1 -- MIN_LIFT';
        END IF;
        select @IN_MIN_LIFT_cond;
		SET @new_filter_cond_final_view = CONCAT(@new_filter_cond_final_view, @IN_MIN_LIFT_cond);
        
        
        
        IF IN_COVERAGE <> "" 
        THEN
			SET @IN_COVERAGE_cond = CONCAT(' 
            AND  Coverage >',IN_COVERAGE,'');
        ELSE
			SET @IN_COVERAGE_cond = ' 
            AND 1=1 -- COVERAGE';
        END IF;
        select @IN_COVERAGE_cond;
		SET @new_filter_cond_final_view = CONCAT(@new_filter_cond_final_view, @IN_COVERAGE_cond);
        
        
        
        select @filter_cond as "FINAL FILTER CONDITION";
        select @new_filter_cond_final_view as "FINAL FILTER CONDITION for after view creation";
        
    END IF;
  
IF IN_MEASURE = 'TABLE' AND IN_COVERAGE = '' AND IN_MIN_LIFT = ''
    THEN
    IF IN_AGGREGATION_6 = '["Lifecycle_Segment"]' 
    THEN 
    SET @view_stmt = CONCAT('CREATE OR REPLACE VIEW V_Dashboard_Whatif_Compare_Creative
        as 
	SELECT 
    em.ID AS "ID",
    dash.Trigger AS "Trigger",
    GROUP_CONCAT(DISTINCT dash.Segment) AS "Lifecycle_Segment",
    REPLACE(em.Creative1,"null", "null ") AS "Creative"
    from
    Dashboard_Performance_Campaign_Insights_STLT dash,
    Event_Master em
WHERE
    em.Id = dash.Event_Id and ',@filter_cond,' 
    ;
        ');
        
    ELSE 
			IF IN_AGGREGATION_6 = 'DEFAULT' or IN_AGGREGATION_6 = ''
			THEN
			SET @grp_cond_view = 1;
			ELSE
			SET @grp_cond_view = '`ID`,`Lifecycle_Segment`';
			END IF;
    SET @view_stmt = CONCAT('CREATE OR REPLACE VIEW V_Dashboard_Whatif_Compare_Creative
        as 
	SELECT 
    em.ID AS "ID",
    dash.Trigger AS "Trigger",
    dash.Segment AS "Lifecycle_Segment",
    REPLACE(em.Creative1,"null", "null ") AS "Creative",
    SUM(dash.Target_Base) AS "Coverage",
    ROUND((SUM(Target_Delivered) / SUM(Target_Base)) * 100,
            2) AS "Delivery_Percent",
	dash.Waterfall AS "Waterfall",
    SUM(dash.Incremental_Revenue) AS "Lift",
    CASE
        WHEN dash.OFFER = "No Offer" THEN "No"
        ELSE "Yes"
    END AS "Is_Offer_Based",
    CASE
        WHEN dash.Recommendation = "Reco" THEN "Yes"
        ELSE "No"
    END AS "Is_Reco_Based"
FROM
    Dashboard_Performance_Campaign_Insights_STLT dash,
    Event_Master em
WHERE
    em.Id = dash.Event_Id and ',@filter_cond,' 
    
GROUP BY ',@grp_cond_view,'
ORDER BY 1 DESC;
        ');
	
    END IF;
	SET @view_used = "V_Dashboard_Whatif_Compare_Creative";


		
        
        IF IN_AGGREGATION_6 = 'DEFAULT' or IN_AGGREGATION_6 = ''
		THEN
		
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Trigger",`Trigger`,"Creative",`Creative`) )),"]")into @temp_result from ',@view_used,' ',';') ;
		END IF;
        
		IF IN_AGGREGATION_6 <> 'DEFAULT' AND IN_AGGREGATION_6 <> ''
		THEN
		SELECT IN_AGGREGATION_6;
        SET @json_select2 = '"Trigger",`Trigger`,"Creative",`Creative`';
        
		set @select_q = IN_AGGREGATION_6;
        SELECT @select_q;
        set @select_q= replace(@select_q,"[","");
        set @select_q= replace(@select_q,"]","");
        set @select_q = replace(@select_q,'"',"");
        select @select_q;
		SET @select_query2 = CONCAT(@select_query,",",@select_q);
		select (length(@select_q)-length(replace(@select_q,',','')))+1 into @num_of_channel;
		select @num_of_channel;
		set @loop_var=1;
		select @loop_var,@num_of_channel,@select_q, @json_select;
		WHILE @loop_var<=@num_of_channel
		do
			SET @selected_channel=substring_index(substring_index(concat(@select_q,',-1'),',',@loop_var),',',-1);
            set @selected_channel = TRIM(@selected_channel);
            
			set @json_select2=concat(@json_select2,',"',@selected_channel,'",`',@selected_channel,'`');
            set @loop_var=@loop_var+1;
		end while;
        select @json_select2;
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object(',@json_select2,') )),"]")into @temp_result from ',@view_used,';') ;
		
		
		
		END IF;
          
	END IF;	
IF IN_MEASURE = 'TABLE' AND IN_COVERAGE <> '' or IN_MIN_LIFT <> ''
then
    SET @view_stmt = concat('CREATE OR REPLACE VIEW V_Dashboard_Whatif_Compare_Creative_With_filter
        as
        select * from V_Dashboard_Whatif_Compare_Creative
        where ',@new_filter_cond_final_view,'
        ');
	SET @view_used = "V_Dashboard_Whatif_Compare_Creative_With_filter";
	
    IF IN_AGGREGATION_6 <> 'DEFAULT' AND IN_AGGREGATION_6 <> ''
		THEN
		SELECT IN_AGGREGATION_6;
        SET @json_select2 = '"Trigger",`Trigger`,"Creative",`Creative`';
        
		set @select_q = IN_AGGREGATION_6;
        SELECT @select_q;
        set @select_q= replace(@select_q,"[","");
        set @select_q= replace(@select_q,"]","");
        set @select_q = replace(@select_q,'"',"");
        select @select_q;
		SET @select_query2 = CONCAT(@select_query,",",@select_q);
		select (length(@select_q)-length(replace(@select_q,',','')))+1 into @num_of_channel;
		select @num_of_channel;
		set @loop_var=1;
		select @loop_var,@num_of_channel,@select_q, @json_select;
		WHILE @loop_var<=@num_of_channel
		do
			SET @selected_channel=substring_index(substring_index(concat(@select_q,',-1'),',',@loop_var),',',-1);
            set @selected_channel = TRIM(@selected_channel);
            
			set @json_select2=concat(@json_select2,',"',@selected_channel,'",`',@selected_channel,'`');
            set @loop_var=@loop_var+1;
		end while;
        select @json_select2;
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object(',@json_select2,') )),"]")into @temp_result from ',@view_used,';') ;
		
		
		
		END IF;
END IF;
	
    
    IF IN_MEASURE = 'COL_PICKER' and IN_AGGREGATION_4 = 'SHOW_ALL_COLS'
    THEN
		set @view_stmt = concat('CREATE OR REPLACE VIEW V_Dashboard_Whatif_Compare_Creative_COL_Picker
					AS (	select COLUMN_NAME from information_schema.columns
							where TABLE_NAME = "V_Dashboard_Whatif_Compare_Creative" AND COLUMN_NAME <> "ID"
                            AND COLUMN_NAME <> "Trigger"
                            AND COLUMN_NAME <> "Creative");');
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("COLUMN_NAME",`COLUMN_NAME`) )),"]")into @temp_result from V_Dashboard_Whatif_Compare_Creative_COL_Picker;') ;
        
        select @Query_String;
	END IF;
    
    
	IF IN_MEASURE <> "DOWNLOAD"
    THEN
	select @view_stmt;
	PREPARE statement from @view_stmt;
	Execute statement;
	Deallocate PREPARE statement;
    
	select @Query_String;
	PREPARE statement from @Query_String;
	Execute statement;
	Deallocate PREPARE statement; 
    
    /*
    IF @temp_result is null
    then 
		set @temp_result = "NO DATA AVAILABLE";
	end if;*/
    
    SET out_result_json = @temp_result;
    END IF;
    
END$$
DELIMITER ;
