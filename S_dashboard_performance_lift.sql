DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_performance_lift`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
    declare IN_SHOW_COL_PICKER text;
    declare IN_FILTER_CHANNEL text;
    declare IN_FILTER_CATEGORY text;
    declare IN_FILTER_REV_CENTER text;
    declare IN_FILTER_SEGMENT text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
    SET IN_FILTER_CHANNEL = json_unquote(json_extract(in_request_json,"$.FILTER_CHANNEL"));
    SET IN_FILTER_CATEGORY = json_unquote(json_extract(in_request_json,"$.FILTER_CATEGORY"));
    SET IN_FILTER_REV_CENTER = json_unquote(json_extract(in_request_json,"$.FILTER_REV_CENTER"));
    SET IN_FILTER_SEGMENT = json_unquote(json_extract(in_request_json,"$.FILTER_SEGMENT"));
    SET IN_SHOW_COL_PICKER = json_unquote(json_extract(in_request_json,"$.SHOW_COL_PICKER"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    
    SET @FY_SATRT_DATE = date_format(concat(IN_FILTER_CURRENTMONTH-100,'01'),"%Y-%m-%d");
	SET @FY_END_DATE = last_day(date_format(concat(IN_FILTER_CURRENTMONTH,'28'),"%Y-%m-%d"));
    
    select Value1 into @Incremental_Revenue_Method from UI_Configuration_Global where Config_Name = 'Incremental_Revenue_Method';
    SET @Incremental_Revenue_Method=ifnull(@Incremental_Revenue_Method,'Method2');
    select @Incremental_Revenue_Method;
    IF @Incremental_Revenue_Method = 'Method1'
    THEN
		SET @incr_rev_concat='_By_Day';
        SET @group_cond='EE_Date';
        SET @mtd_concat='';
	ELSEIF @Incremental_Revenue_Method = 'Method2'
    THEN
		SET @group_cond='YM';
		SET @incr_rev_concat='';
        SET @mtd_concat='MTD_';
	ELSE
		SET @incr_rev_concat='';
        SET @mtd_concat='';
		SET @incr_Rev = ' round((CASE WHEN SUM(Control_Base) < 0 or SUM(Target_Delivered) = 0 then 0 WHEN (((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*100 < 0 then 0 WHEN SUM(Control_Base)>0 and SUM(Control_Responder)=0 then SUM(Target_Revenue)
					ELSE ifnull((((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*(SUM(Target_Revenue)/SUM(Target_Responder))*(SUM(Target_Delivered)),0) END),0) ';
		SET @group_cond='YM';
	END IF;
    SELECT @mtd_concat;
    
    IF IN_FILTER_CHANNEL IS NULL or IN_FILTER_CHANNEL = 'ALLCHANNEL' or IN_FILTER_CHANNEL = ""
    THEN 
    SET @CHANNEL_COND = " AND 1=1";
    ELSE 
    SET @CHANNEL_COND = CONCAT(' AND `Channel` = "',IN_FILTER_CHANNEL,'"');
    END IF;
    
    IF IN_FILTER_CATEGORY IS NULL or IN_FILTER_CATEGORY = 'ALL' or IN_FILTER_CATEGORY = ""
    THEN 
    SET @CATEGORY_COND = " AND 1=1";
    ELSE 
    SET @CATEGORY_COND = CONCAT(' AND `Category` = "',IN_FILTER_CATEGORY,'"');
    END IF;
    
    IF IN_FILTER_REV_CENTER IS NULL or IN_FILTER_REV_CENTER = ""
    THEN 
    SET @REV_CENTER_COND = " AND 1=1";
    ELSE 
    SET @REV_CENTER_COND = CONCAT(' AND `Revenue Center` = "',IN_FILTER_REV_CENTER,'"');
    END IF;
    
    IF IN_FILTER_SEGMENT IS NULL or IN_FILTER_SEGMENT = ""
    THEN 
    SET @SEGMENT_COND = " AND 1=1";
    ELSE 
    SET @SEGMENT_COND = CONCAT(' AND `Segment` = "',IN_FILTER_SEGMENT,'"');
    END IF;
    
    set @new_filter_cond = concat(@CHANNEL_COND,@CATEGORY_COND,@REV_CENTER_COND,@SEGMENT_COND);
    select @new_filter_cond;
    
    
    IF IN_SHOW_COL_PICKER is not null 
    then
    IF IN_SHOW_COL_PICKER = "REV_CENTER"
    THEN
		SELECT "HERE";
		set @view_stmt = concat('CREATE OR REPLACE VIEW V_Dashboard_Impact_Overview_Filter
						AS (	SELECT distinct `Revenue Center` AS `COLUMN_NAME` FROM CLM_Event_Dashboard WHERE `Revenue Center` NOT IN ("-1", "NA") );');
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("COLUMN_NAME",`COLUMN_NAME`) )),"]")into @temp_result from V_Dashboard_Impact_Overview_Filter;') ;
    
    ELSEIF IN_SHOW_COL_PICKER = "SEGMENT"
    THEN
		set @view_stmt = concat('CREATE OR REPLACE VIEW V_Dashboard_Impact_Overview_Filter
						AS (	SELECT distinct `Segment` AS `COLUMN_NAME` FROM CLM_Event_Dashboard
);');
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("COLUMN_NAME",`COLUMN_NAME`) )),"]")into @temp_result from V_Dashboard_Impact_Overview_Filter;') ;
    END IF;
    END IF;
    IF IN_AGGREGATION_2 = 'ST'
    THEN
    SET @sql1=CONCAT('
		CREATE OR REPLACE VIEW CLM_Event_Dashboard_STLT1
        as 
		SELECT `CLM_Event_Dashboard`.`Revenue Center`,
			`CLM_Event_Dashboard`.`Segment`,
			`CLM_Event_Dashboard`.`Campaign`,
			`CLM_Event_Dashboard`.`EE_Date`,
			`CLM_Event_Dashboard`.`Event_Execution_Date_ID`,
			`CLM_Event_Dashboard`.`Theme`,
			`CLM_Event_Dashboard`.`Offer`,
			`CLM_Event_Dashboard`.`Channel`,
			`CLM_Event_Dashboard`.`Has_Recommendations`,
			`CLM_Event_Dashboard`.`Event_ID`,
			`CLM_Event_Dashboard`.`Control_Base_Event` as `Control_Base`,
			`CLM_Event_Dashboard`.`Target_Base`,
			`CLM_Event_Dashboard`.`Target_Delivered`,
			`CLM_Event_Dashboard`.`Control_Responder_Event` as `Control_Responder`,
			`CLM_Event_Dashboard`.`Target_Responder`,
			`CLM_Event_Dashboard`.`Target_Revenue`,
			`CLM_Event_Dashboard`.`Target_Discount`,
			`CLM_Event_Dashboard`.`Target_Bills`,
			`CLM_Event_Dashboard`.`Target_Open`,
			`CLM_Event_Dashboard`.`Target_CTA`,
			`CLM_Event_Dashboard`.`Control_Revenue`,
			`CLM_Event_Dashboard`.`Control_Discount`,
			`CLM_Event_Dashboard`.`Control_Bills`,
			`CLM_Event_Dashboard`.`Created_Date`,
			`CLM_Event_Dashboard`.`Total_Customer_Base`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Day`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Month`,
			`CLM_Event_Dashboard`.`YM`,
			`CLM_Event_Dashboard`.`Incremental_Revenue_ST',@incr_rev_concat,'` AS `Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_ST',@incr_rev_concat,'` AS `MTD_Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Segment_ST',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Segment`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Revenue_Center_ST',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Revenue_Center`
		FROM `CLM_Event_Dashboard`;
        ');
        select @sql1;
		PREPARE statement from @sql1;
		Execute statement;
		Deallocate PREPARE statement;
    ELSE
		SET @sql1=CONCAT('
		CREATE OR REPLACE VIEW CLM_Event_Dashboard_STLT1
        as 
		SELECT `CLM_Event_Dashboard`.`Revenue Center`,
			`CLM_Event_Dashboard`.`Segment`,
			`CLM_Event_Dashboard`.`Campaign`,
			`CLM_Event_Dashboard`.`EE_Date`,
			`CLM_Event_Dashboard`.`Event_Execution_Date_ID`,
			`CLM_Event_Dashboard`.`Theme`,
			`CLM_Event_Dashboard`.`Offer`,
			`CLM_Event_Dashboard`.`Channel`,
			`CLM_Event_Dashboard`.`Has_Recommendations`,
			`CLM_Event_Dashboard`.`Event_ID`,
			`CLM_Event_Dashboard`.`Control_Base` as `Control_Base`,
			`CLM_Event_Dashboard`.`Target_Base`,
			`CLM_Event_Dashboard`.`Target_Delivered`,
			`CLM_Event_Dashboard`.`Control_Responder` as `Control_Responder`,
			`CLM_Event_Dashboard`.`Target_Responder`,
			`CLM_Event_Dashboard`.`Target_Revenue`,
			`CLM_Event_Dashboard`.`Target_Discount`,
			`CLM_Event_Dashboard`.`Target_Bills`,
			`CLM_Event_Dashboard`.`Target_Open`,
			`CLM_Event_Dashboard`.`Target_CTA`,
			`CLM_Event_Dashboard`.`Control_Revenue`,
			`CLM_Event_Dashboard`.`Control_Discount`,
			`CLM_Event_Dashboard`.`Control_Bills`,
			`CLM_Event_Dashboard`.`Created_Date`,
			`CLM_Event_Dashboard`.`Total_Customer_Base`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Day`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Month`,
			`CLM_Event_Dashboard`.`YM`,
			`CLM_Event_Dashboard`.`Incremental_Revenue',@incr_rev_concat,'` AS `Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue',@incr_rev_concat,'` AS `MTD_Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Segment',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Segment`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Revenue_Center',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Revenue_Center`
		FROM `CLM_Event_Dashboard`;
        ');
        select @sql1;
		PREPARE statement from @sql1;
		Execute statement;
		Deallocate PREPARE statement;
    END IF;
    
    
    CREATE OR REPLACE VIEW V_Dashboard_CLM_Event_Impact_Overview_Report AS
    SELECT 
        CASE
            WHEN DATEDIFF(End_Date_ID, Start_Date_ID) < 32 THEN 'GTM'
            ELSE 'CLM'
        END AS `Category`,
        b.Name AS `Trigger`,
        a.*
    FROM
        CLM_Event_Dashboard_STLT1 a
            JOIN
        Event_Master b ON a.Event_ID = b.ID;            
    
    
    
    IF IN_AGGREGATION_1 = 'BY_DAY' 
		THEN 
			SET @filter_cond = concat(' date_format(EE_DATE,"%Y%m")=',IN_FILTER_CURRENTMONTH);
			SET @date_select = ' date_format(EE_DATE,"%d-%b") AS "Year" ';
            SET @bill_cond = 'Total_Revenue_This_Day';
            
            SET @create_view=CONCAT('CREATE OR REPLACE VIEW V_Dashboard_MTD_Incr_Rev_1
			as
			select MAX(EE_Date) as EE_Date,MAX(MTD_Incremental_Revenue) as Incremental_Revenue,SUM(Target_Revenue) as Target_Revenue,MAX(Total_Revenue_This_Month) as Total_Revenue_This_Month,MAX(Total_Revenue_This_Day) as Total_Revenue_This_Day,SUM(Target_Delivered) as Target_Delivered,SUM(Target_Bills) as Target_Bills,SUM(Control_Bills) as Control_Bills,Event_ID,Channel,Segment,`Revenue Center`, Category,YM from V_Dashboard_CLM_Event_Impact_Overview_Report group by Event_Id,',@group_cond,'');
            
            CREATE OR REPLACE VIEW V_Dashboard_MTD_Incr_Rev_1
			as
            select * from V_Dashboard_CLM_Event_Impact_Overview_Report;
            
	ELSEIF IN_AGGREGATION_1 = 'BY_WEEK' 
		THEN 
			select Value1 into @START_OF_WEEK from UI_Configuration_Global where Config_Name = 'FIRSTDAYOFWEEK';
			if @START_OF_WEEK = 'Sunday'
			then 
			SET @START_OF_WEEK= 0;
			else
			SET @START_OF_WEEK= 5;
			END IF;
            SET @date_select_graph = CONCAT('YEARWEEK(EE_Date,',@START_OF_WEEK,') AS "dt"');
        
        set @Y = concat( substring(IN_FILTER_CURRENTMONTH,1,4) );
        set @M = concat( substring(IN_FILTER_CURRENTMONTH,5,6) );
        SET @graph_in_filter_curr_month = concat(@Y,'-',@M,"-01");
        SET @graph_in_filter_curr_month = DATE_ADD(@graph_in_filter_curr_month,INTERVAL 4 WEEK);
        SET @filter_cond_graph = CONCAT('EE_DATE BETWEEN DATE_SUB("',@graph_in_filter_curr_month,'", INTERVAL 7 WEEK) AND "',@graph_in_filter_curr_month,'"');
        SET @temp_view = concat('
        create or replace View temp_Outreaches_Week as
        
        SELECT 
		YEARWEEK(EE_Date,',@START_OF_WEEK,') AS "dt"
		FROM
		CLM_Event_Dashboard dash
		WHERE
		',@filter_cond_graph,'
        GROUP BY 1
		ORDER BY EE_Date');
	
		select @temp_view;
		PREPARE statement from @temp_view;
		Execute statement;
		Deallocate PREPARE statement;
        
        create or replace view week_name as
        select date_Format(Date_Sub(max(tm.EE_date), interval 6 day), "%b%e") as week_name, temp.dt
        from temp_Outreaches_Week temp, table_date_yearweek tm 
        where temp.dt = tm.EE_Yearweek
        group by temp.dt
        order by tm.EE_date;
		
        select * from week_name;
        select group_concat(week_name) FROM week_name INTO @distinct_channel_week;
		SELECT group_concat(dt) FROM temp_Outreaches_Week INTO @distinct_channel;
		select @distinct_channel;
        select (length(@distinct_channel)-length(replace(@distinct_channel,',','')))+1 into @num_of_channel;
		set @loop_var=1;
		set @Q_M=concat("CASE ");
        select @loop_var,@num_of_channel,@distinct_channel;
		WHILE @loop_var<=@num_of_channel
		do
			SET @selected_channel=substring_index(substring_index(concat(@distinct_channel,',-1'),',',@loop_var),',',-1);
            SET @selected_channel_week = 
            substring_index(substring_index(concat(@distinct_channel_week,',-1'),',',@loop_var),',',-1);
			set @Q_M=concat(@Q_M," WHEN YEARWEEK(EE_Date,",@START_OF_WEEK,") = ",@selected_channel," Then 'Week of ",@selected_channel_week,"' 
			");
			set @json_select=concat(@json_select,',"Week of ',@selected_channel_week,'",IFNULL(`Week of ',@selected_channel_week,'`,0)');
			set @loop_var=@loop_var+1;
		end while;
		set @Q_M=concat(@Q_M," else EE_Date END AS 'w'");
		select @Q_M;
        SET @temp_view_stmt = concat('
        SELECT 
		YEARWEEK(EE_Date,',@START_OF_WEEK,') AS dt,',@Q_M,'
		FROM
		CLM_Event_Dashboard dash
		WHERE
		',@filter_cond_graph,'
        GROUP BY YEARWEEK(EE_Date,',@START_OF_WEEK,')
		ORDER BY EE_Date');
        select @temp_view_stmt;
            
            
			SET @filter_cond = concat('  EE_Date between DATE_SUB( now(), INTERVAL 7 day) and now() ');
			SET @date_select = ' date_format(EE_DATE,"%d-%b") AS "Year" ';
            SET @bill_cond = 'Total_Revenue_This_Day';
            
            SET @create_view=CONCAT('CREATE OR REPLACE VIEW V_Dashboard_MTD_Incr_Rev_1
			as
			select MAX(EE_Date) as EE_Date,MAX(MTD_Incremental_Revenue) as Incremental_Revenue,SUM(Target_Revenue) as Target_Revenue,MAX(Total_Revenue_This_Month) as Total_Revenue_This_Month,MAX(Total_Revenue_This_Day) as Total_Revenue_This_Day,SUM(Target_Delivered) as Target_Delivered,SUM(Target_Bills) as Target_Bills,SUM(Control_Bills) as Control_Bills,Event_ID,Channel,Segment,`Revenue Center`, Category,YM from V_Dashboard_CLM_Event_Impact_Overview_Report group by Event_Id,',@group_cond,'');
            
	ELSEIF IN_AGGREGATION_1 = 'BY_MONTH' 
		THEN 
			SET @filter_cond=concat(" EE_Date between '",@FY_SATRT_DATE,"' and '",@FY_END_DATE,"'");
			SET @date_select=' date_format(EE_DATE,"%b-%Y") AS "Year" ';
            SET @bill_cond = 'Total_Revenue_This_Month';
            SET @create_view=CONCAT('CREATE OR REPLACE VIEW V_Dashboard_MTD_Incr_Rev_1
			as
			select MAX(EE_Date) as EE_Date,MAX(MTD_Incremental_Revenue) as Incremental_Revenue,SUM(Target_Revenue) as Target_Revenue,MAX(Total_Revenue_This_Month) as Total_Revenue_This_Month,SUM(Target_Delivered) as Target_Delivered,SUM(Target_Bills) as Target_Bills,SUM(Control_Bills) as Control_Bills,Event_ID,Channel,Segment,`Revenue Center`, Category,YM from V_Dashboard_CLM_Event_Impact_Overview_Report group by Event_Id,',@group_cond,'');
            
	ELSEIF IN_AGGREGATION_1 = 'BY_QUARTER' 
		THEN 
			SET @FY_SATRT_DATE = date_format(concat(F_Month_Diff(IN_FILTER_CURRENTMONTH,2),'01'),"%Y-%m-%d");
			SET @filter_cond=concat(" EE_Date between '",@FY_SATRT_DATE,"' and '",@FY_END_DATE,"'");
			SET @date_select=' date_format(EE_DATE,"%b-%Y") AS "Year" ';
            SET @bill_cond = 'Total_Revenue_This_Month';
            SET @create_view=CONCAT('CREATE OR REPLACE VIEW V_Dashboard_MTD_Incr_Rev_1
			as
			select MAX(EE_Date) as EE_Date,MAX(MTD_Incremental_Revenue) as Incremental_Revenue,SUM(Target_Revenue) as Target_Revenue,MAX(Total_Revenue_This_Month) as Total_Revenue_This_Month,SUM(Target_Delivered) as Target_Delivered,SUM(Target_Bills) as Target_Bills,SUM(Control_Bills) as Control_Bills,Event_ID,Channel,Segment,`Revenue Center`, Category,YM from V_Dashboard_CLM_Event_Impact_Overview_Report group by Event_Id,',@group_cond,'');
            
	END IF;
    
    select @create_view;
	PREPARE statement from @create_view;
	Execute statement;
	Deallocate PREPARE statement;
    
    IF IN_AGGREGATION_3 = 'METHOD2' or @Incremental_Revenue_Method = 'Method3'
    THEN
		SET @incr_Rev = ' round((CASE WHEN SUM(Control_Base) < (Select `Value` from CLM_Response_Config where `Name` = "Control_Group_Min_Control_Base") or SUM(Target_Delivered) = 0 then 0 WHEN (((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*100 < 0 then 0 WHEN SUM(Control_Base)>0 and SUM(Control_Responder)=0 then SUM(Target_Revenue)
					ELSE ifnull((((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*(SUM(Target_Revenue)/SUM(Target_Responder))*(SUM(Target_Delivered)),0) END),0) ';
		CREATE OR REPLACE VIEW V_Dashboard_MTD_Incr_Rev_1
			as
            select * from V_Dashboard_CLM_Event_Impact_Overview_Report;
	ELSE
		SET @incr_Rev =' round(SUM(Incremental_Revenue),0) ';		
    END IF;
    
    IF IN_MEASURE = "LIFT" OR IN_MEASURE = "CARDS"
	THEN
		SET @Query_Measure=@incr_Rev;
		SET @lift_measure=@Query_Measure;
        SET @table_used = 'V_Dashboard_MTD_Incr_Rev_1';
	END IF;
    IF IN_MEASURE = "TGREV" OR IN_MEASURE = "CARDS"
	THEN
		SET @Query_Measure=' round(SUM(target_Revenue),0) ';
		SET @tgrev_measure=@Query_Measure;
        SET @table_used = 'CLM_Event_Dashboard';
	END IF;
	IF IN_MEASURE = "TGRESP" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=(CASE WHEN IN_NUMBER_FORMAT='NUMBER' THEN ' SUM(Target_Responder) ' ELSE ' round((SUM(Target_Responder)/SUM(Target_Delivered))*100,1) ' END);
        SET @tgresp_pct=' round((SUM(Target_Responder)/SUM(Target_Delivered))*100,1) ';
        SET @tgresp_num=' SUM(Target_Responder) ';
		SET @tgresp_measure=@Query_Measure;
        SET @table_used = 'CLM_Event_Dashboard';
	END IF;
    IF IN_MEASURE = "INCRRESP" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' SUM(Target_Bills) ';
		SET @incrresp_measure=@Query_Measure;
        SET @table_used = 'CLM_Event_Dashboard';
	END IF;
    IF IN_MEASURE = "LIFTASPTGR" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=concat(' (CASE WHEN SUM(Target_Revenue) = 0 THEN 0 ELSE round((',@incr_Rev,'/SUM(Target_Revenue))*100,2) END) ');
		SET @liftasptgr_measure=@Query_Measure;
        SET @table_used = 'V_Dashboard_MTD_Incr_Rev_1';
	END IF;
    IF IN_MEASURE = "LIFTASPTR" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=concat(' (CASE WHEN SUM(Target_Revenue) = 0 THEN 0 ELSE round((',@incr_Rev,'/MAX(',@bill_cond,'))*100,2) END) ');
		SET @liftasptr_measure=@Query_Measure;
        SET @table_used = 'V_Dashboard_MTD_Incr_Rev_1';
	END IF;
    IF IN_MEASURE = "YIELD" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=concat(' (CASE WHEN SUM(Target_Delivered) < 1 THEN 0 ELSE round((',@incr_Rev,'/SUM(Target_Delivered)),1) END) ');
		SET @yield_measure=@Query_Measure;
        SET @table_used = 'V_Dashboard_MTD_Incr_Rev_1';
	END IF;
    IF IN_MEASURE = "ROI" OR IN_MEASURE = "CARDS" 
    THEN
         SET @Query_Measure=' Cost_per_Incr_Bill ';
		SET @roi_measure=@Query_Measure;
        SET @table_used = 'Monthly_Cost_Incr_Bill_Temp';
	END IF;
    
     IF IN_MEASURE='CARDS'
    THEN
		set @view_stmt=concat('create or replace view V_Dashboard_Lifts_Card as
		(select "Incremental Revenue" as Card_Name,format(ifnull(',@lift_measure,',0),",") as `Value1`,"" as `Value2`, "" as `Value3`,"LIFT" as `Measure`  from V_Dashboard_MTD_Incr_Rev_1 WHERE YM = ',IN_FILTER_CURRENTMONTH,' ',@new_filter_cond,')
        UNION
        (select "Target Revenue" as Card_Name,format(ifnull(',@tgrev_measure,',0),",") as `Value1`,"" as `Value2`, "" as `Value3`,"TGREV" as `Measure`  from V_Dashboard_CLM_Event_Impact_Overview_Report WHERE date_format(`V_Dashboard_CLM_Event_Impact_Overview_Report`.`EE_Date`,"%Y%m") = ',IN_FILTER_CURRENTMONTH,' ',@new_filter_cond,')
		Union
		(select "TG Response" as Card_Name,concat(ifnull(',@tgresp_pct,',0),"%") as `Value1`,"" as `Value2`, "" as `Value3`,"TGRESP" as `Measure`  from V_Dashboard_CLM_Event_Impact_Overview_Report WHERE date_format(`V_Dashboard_CLM_Event_Impact_Overview_Report`.`EE_Date`,"%Y%m") = ',IN_FILTER_CURRENTMONTH,' ',@new_filter_cond,')
		Union
		(select "Target Bills" as Card_Name,format(ifnull(',@incrresp_measure,',0),",") as `Value1`,"" as `Value2`, "" as `Value3`,"INCRRESP" as `Measure`  from V_Dashboard_CLM_Event_Impact_Overview_Report WHERE date_format(`V_Dashboard_CLM_Event_Impact_Overview_Report`.`EE_Date`,"%Y%m") = ',IN_FILTER_CURRENTMONTH,' ',@new_filter_cond,')
		Union
		(select "Lift as % of TG Rev" as Card_Name,concat(ifnull(',@liftasptgr_measure,',0),"%") as `Value1`,"" as `Value2`, "" as `Value3`,"LIFTASPTGR" as `Measure`  from V_Dashboard_MTD_Incr_Rev_1 WHERE YM = ',IN_FILTER_CURRENTMONTH,' ',@new_filter_cond,')
		Union
		(select "Lift as % of Total Rev" as Card_Name,concat(ifnull(',@liftasptr_measure,',0),"%") as `Value1`,"" as `Value2`, "" as `Value3`,"LIFTASPTR" as `Measure`  from V_Dashboard_MTD_Incr_Rev_1 WHERE YM = ',IN_FILTER_CURRENTMONTH,' ',@new_filter_cond,')
        UNION
        (select "Yield/Outreach" as Card_Name,ifnull(',@yield_measure,',0) as `Value1`,"" as `Value2`, "" as `Value3`,"YIELD" as `Measure`  from V_Dashboard_MTD_Incr_Rev_1 WHERE YM = ',IN_FILTER_CURRENTMONTH,' ',@new_filter_cond,')
        UNION
        (select "Cost/Incr. Bill" as Card_Name,round(ifnull(',@roi_measure,',0),1) as `Value1`,"" as `Value2`, "" as `Value3`,"ROI" as `Measure`  from Monthly_Cost_Incr_Bill_Temp ced WHERE YM = ',IN_FILTER_CURRENTMONTH,' )
		');
        
	SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Card_Name",`Card_Name`,"Value1",`Value1`,"Value2",`Value2`,"Value3",`Value3`,"Measure",`Measure`) )),"]")into @temp_result from V_Dashboard_Lifts_Card;' ;
    ELSEIF IN_MEASURE='DOWNLOAD'
    THEN
    SET @view_stmt1=CONCAT('CREATE OR REPLACE VIEW V_Dashboard_lift_Download as       
    (  SELECT 
	`Revenue Center` as `Revenue Center`,
    EE_Date,
    Segment,
    Campaign,
	Theme,
    Offer,
    Channel,
    Event_ID,
    Control_Base,
   -- Control_Base_Event,
    Target_Base,
    Target_Delivered,
    Control_Responder,
   -- Control_Responder_Event,
    Target_Responder,
    Target_Revenue,
    Target_Discount,
    Target_Bills,
    Target_Open,
    Target_CTA,
    Control_Revenue,
    Control_Discount,
    Control_Bills,
    Total_Customer_Base,
    Total_Distinct_Customers_Targeted_This_Month,
    Total_Distinct_Customers_Targeted_This_Day   
FROM V_Dashboard_CLM_Event_Impact_Overview_Report where YM between "',  @FY_SATRT_DATE,'"  and "',@FY_END_DATE,'" ',@new_filter_cond,');') ;

    select @view_stmt1;
		PREPARE statement1 from @view_stmt1;
		Execute statement1;
		Deallocate PREPARE statement1; 
        call S_dashboard_performance_download('V_Dashboard_lift_Download',@out_result_json1);
		select @out_result_json1 into out_result_json;
    
    
    ELSEIF IN_AGGREGATION_1 <> "BY_WEEK" AND IN_SHOW_COL_PICKER IS NULL
    THEN
		select @date_select,@Query_Measure,@filter_cond;
		SET @view_stmt=concat('CREATE OR REPLACE VIEW V_Dashboard_Lifts_Graph AS
						SELECT ',@date_select,',
                         ',@Query_Measure,' as "Value"
                         from ',@table_used,' ced
                         WHERE ',@filter_cond,' ',@new_filter_cond,'
                         GROUP BY 1
                         ORDER BY EE_Date
                        ');
		SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`Year`,"Value",`Value`) )),"]")into @temp_result from V_Dashboard_Lifts_Graph;' ;
	ELSEIF IN_AGGREGATION_1 LIKE "%WEEK%"
    THEN
		select @date_select,@Query_Measure,@filter_cond;
		SET @view_stmt=concat('CREATE OR REPLACE VIEW V_Dashboard_Lifts_Graph AS
						SELECT ',@date_select,',
                         ',@Query_Measure,' as "Value"
                         from ',@table_used,' ced
                         WHERE ',@filter_cond,' ',@new_filter_cond,'
                         GROUP BY 1
                         ORDER BY EE_Date
                        ');
		set @view_stmt = CONCAT(' CREATE OR REPLACE VIEW V_Dashboard_Lifts_Graph AS
        with 
        
        GROUP1 AS
        (
			SELECT ',@date_select_graph,',
                         ',@Query_Measure,' as "Value"
                         from ',@table_used,' ced
                         WHERE ',@filter_cond_graph,' ',@new_filter_cond,'
                         GROUP BY 1
                         ORDER BY EE_Date
        ),
        GROUP2 AS
        (
			',@temp_view_stmt,'	
        )
        SELECT C.dt AS dt,C.w,ifnull(Value,0) as Value
        FROM GROUP2 C
		LEFT OUTER JOIN GROUP1 B ON B.dt=C.dt
        UNION
        SELECT C.dt,C.w,ifnull(Value,0) as Value
        FROM GROUP2 C
		RIGHT OUTER JOIN GROUP1 A ON A.dt=C.dt
        ');
		SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`w`,"Value",`Value`) )),"]")into @temp_result from V_Dashboard_Lifts_Graph;' ;
	END IF;
	IF IN_MEASURE <> "DOWNLOAD"
    THEN
	select @view_stmt;
	PREPARE statement from @view_stmt;
	Execute statement;
	Deallocate PREPARE statement;
    
	select @Query_String;
	PREPARE statement from @Query_String;
	Execute statement;
	Deallocate PREPARE statement; 
    SET out_result_json = @temp_result;
    END IF;
    
END$$
DELIMITER ;
