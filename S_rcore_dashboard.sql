/*
create or replace View RankedPickList_Summary_View
as
select productGenome as 'Product_Genome',
BU,Cat,Brand,Section,Season,Hybridizer_Rank,
count(Customer_Id) as Num_of_Customers,
Product_Id,Product_Name,
Score,Algorithm,Inter_Reco_Rank,Intra_Reco_Rank
from RankedPickList
group by productGenome,BU,Cat,Brand,Section,Season,Hybridizer_Rank;

create table RankedPickList_Summary as Select * from RankedPickList_Summary_View;

*/
