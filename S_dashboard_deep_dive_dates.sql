DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_deep_dive_dates`()
BEGIN

Select min(EE_Date) into @MinEE_Date from CLM_Event_Dashboard;
	
Select max(EE_Date) into @MaxEE_Date from CLM_Event_Dashboard;

CALL s_dashboard_filldate(@MinEE_Date,@MaxEE_Date);

END$$
DELIMITER ;
