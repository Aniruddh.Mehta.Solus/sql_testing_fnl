DELIMITER $$
CREATE  OR REPLACE PROCEDURE `S_UI_Campaign_Funnel_Coverage`(IN in_request_json json, OUT out_result_json json)
BEGIN

    -- Basic Variables
    declare Campagin_Segment text;
	declare Lifecycle_Segment text;
	declare Mobile varchar(400);
	declare Email varchar(400);
	declare DNDP varchar(400);
	
    declare Throtling Bigint;


	-- Propensity Variables
	  DECLARE Propensity_Model_Id VARCHAR(128);
	  DECLARE Propensity_Score VARCHAR(128);
	  DECLARE Propensity_Segment VARCHAR(128);
    
    -- Advanced RFMP Criteria   
	
	declare Lifecycle_SegmentReplacedQuery text;
	DECLARE Campaign_SegmentReplacedQuery text;
	DECLARE Campaign_Segment_Coverage INT(2);
	DECLARE Number_of_filters INT(2);
	DECLARE Exclusion_Filter_Condition text;
	DECLARE Exclusion_Filter_Coverage INT(2);


    -- ----------Batch Variables-------------
    DECLARE Loop_Check_Var Int Default 0;
    DECLARE vStart_Cnt BIGINT DEFAULT 0;
	DECLARE vEnd_Cnt BIGINT DEFAULT 0;
	DECLARE vRec_Cnt BIGINT DEFAULT 0;
    DECLARE vBatchSize BIGINT DEFAULT 0;
	DECLARE Result1 BIGINT DEFAULT 0;
    DECLARE Result2 BIGINT DEFAULT 0;
    DECLARE Result3 BIGINT DEFAULT 0;
    
    
    SET vBatchSize = 50000;
    SELECT Customer_Id INTO vRec_Cnt FROM Customer_Coverage_View ORDER BY Customer_Id DESC LIMIT 1;
	
	Set in_request_json = Replace(in_request_json,'\n','\\n');
 -- --------------------- Decoding the JSON -----------------------------------------------------------------------------------------------------------------------------------------------
    SET Campagin_Segment = json_unquote(json_extract(in_request_json,"$.Campagin_Segment"));
 
    SET Lifecycle_Segment = json_unquote(json_extract(in_request_json,"$.Lifecycle_Segment")); 
    
    SET Propensity_Model_Id = json_unquote(json_extract(in_request_json,"$.Propensity_Model_Id")); 
    
    SET Propensity_Score = json_unquote(json_extract(in_request_json,"$.Propensity_Score"));
    
    SET Propensity_Segment = json_unquote(json_extract(in_request_json,"$.Propensity_Segment"));
    
	
	SET Mobile = "Yes";
	SET Email = "Yes";
	SET DNDP ="Yes";
	

	SET Throtling = json_unquote(json_extract(in_request_json,"$.Throtling"));
	
	Set Lifecycle_Segment= replace (Lifecycle_Segment,"[",'');
	Set Lifecycle_Segment= replace (Lifecycle_Segment,"]",'');
	Set Lifecycle_Segment= replace (Lifecycle_Segment,'"','');
	Set Lifecycle_Segment= replace (Lifecycle_Segment,', ',',');
	
	
	Set Campagin_Segment= replace (Campagin_Segment,"[",'');
	Set Campagin_Segment= replace (Campagin_Segment,"]",'');
	Set Campagin_Segment= replace (Campagin_Segment,'"','');
	Set Campagin_Segment= replace (Campagin_Segment,', ',',');


	DROP TABLE IF EXISTS Campaign_UI_Funnel_Coverage;
    CREATE TABLE Campaign_UI_Funnel_Coverage (  NAME Text, Coverage Text);
    
	
    IF Propensity_Model_Id = '-1'
    
    THEN 
    
			-- -Lifecycle_Segment--------------------------------

			Select "Calcuating Coverage for LifeSegment";
			
			SELECT Lifecycle_Segment;
            
            /* DEFAULT VALUE */
            IF Lifecycle_Segment IS NULL OR Lifecycle_Segment  = '' 
            
            THEN 
            
				SET Lifecycle_Segment = "All_Segment";
                
			END IF;
            
			
				If Lifecycle_Segment != "All_segment"
					Then 
						Set @SQL1=Concat('Select "LifeCycleSegment_Id in (',Lifecycle_Segment,')" 
									into @Lifecycle_SegmentReplacedQuery ;
									');
						SELECT @SQL1;
						PREPARE statement from @SQL1;
						Execute statement;
						Deallocate PREPARE statement; 
						  SET Lifecycle_SegmentReplacedQuery= CONCAT('( ',@Lifecycle_SegmentReplacedQuery,' )');
														
					Else  
						SET Lifecycle_SegmentReplacedQuery='1=1';
				End If;	
				
				
				SET Loop_Check_Var = FALSE;

				
				SET vStart_Cnt=0;
				SET vEnd_Cnt=0;

				SET Result1 = 0;
				
				
				PROCESS_LOOP: LOOP
					SET vEnd_Cnt = vStart_Cnt + vBatchSize;
					
					SET @batch_result = 0;
				
				
					SET @sql_stmt = CONCAT('SELECT COUNT(1) INTO @batch_result 
															FROM Customer_Coverage_View
															WHERE   ',Lifecycle_SegmentReplacedQuery,'
															AND Customer_Id BETWEEN ',vStart_Cnt,' AND ',vEnd_Cnt,' ');
								
							   
								PREPARE stmt FROM @sql_stmt;
								EXECUTE stmt; 
								DEALLOCATE PREPARE stmt;
								
					SET Result1 = Result1 + @batch_result;
				
			   
				

					SET vStart_Cnt = vEnd_Cnt +1;
					IF vStart_Cnt  >= vRec_Cnt THEN
						LEAVE PROCESS_LOOP;
					END IF;      
					
				END LOOP PROCESS_LOOP;
				
				SET @format_stmt = CONCAT('SELECT Convert(format(',Result1,',","),CHAR) INTO @temp_result');

				PREPARE for_stmt FROM @format_stmt;
				EXECUTE for_stmt;
				DEALLOCATE PREPARE for_stmt;
				
				
                
                /*Displaying The Name Of Segment*/
                IF Lifecycle_Segment = "ALL_Segment"
                THEN 
                
					SET @LifecycleNames = 'ALL';
                    
				ELSE 
				   
                   Set @SQL1=Concat('SELECT GROUP_CONCAT(`CLMSegmentName`
									SEPARATOR ", ") INTO @LifecycleNames
									FROM CLM_Segment
								WHERE CLMSegmentId IN (',Lifecycle_Segment,')');
						SELECT @SQL1;
						PREPARE statement from @SQL1;
						Execute statement;
                  
                  
				END IF;
								
				INSERT IGNORE INTO Campaign_UI_Funnel_Coverage
				SELECT concat("LifeSegment [",@LifecycleNames,"] => ",@temp_result),@temp_result;
				
                
			
				

				
			
		-- ----------- Campaign Segment --------------------------------------------------------------------------

			Select GROUP_CONCAT('(', `Campaign_Segment`.`Segment_Condition_Solus`, ')'SEPARATOR ' OR ') into Campaign_SegmentReplacedQuery  from Campaign_Segment where Segment_Id in (Campagin_Segment);
			Select Campaign_SegmentReplacedQuery;
			IF Campaign_SegmentReplacedQuery is Null or Campaign_SegmentReplacedQuery ='' 
				Then 
					Select "NO Campaign Segment Is Selected"; 
					Set Campaign_Segment_Coverage=0;
					Set Campaign_SegmentReplacedQuery="1=1";
			
				ELSE		
			
					Select "Calcuating Coverage for  Campaign Segment";
					Set Campaign_Segment_Coverage=1;
					SET Campaign_SegmentReplacedQuery = CONCAT('( ',Campaign_SegmentReplacedQuery,' )');
					
					/* Enabling Columns in Coverage View */
					
					SET @Combined_Condition = Campaign_SegmentReplacedQuery;
					
					CALL S_UI_Coverage_View(@Combined_Condition);
					
				   
				SET Loop_Check_Var = FALSE;
			
				SET vStart_Cnt=0;
				SET vEnd_Cnt=0;
				SET Result1 = 0;
				
				PROCESS_LOOP: LOOP
					SET vEnd_Cnt = vStart_Cnt + vBatchSize;
					
					SET @batch_result = 0;

					SET @sql_stmt = CONCAT('SELECT COUNT(1) INTO @batch_result  
															FROM Customer_Coverage_View 
															WHERE   ',Lifecycle_SegmentReplacedQuery,'
															and ',Campaign_SegmentReplacedQuery,'
															 AND Customer_Id BETWEEN ',vStart_Cnt,' AND ',vEnd_Cnt,' ');
								
				
				PREPARE stmt FROM @sql_stmt;
				EXECUTE stmt; 
				DEALLOCATE PREPARE stmt;

					SET Result1 = Result1 + @batch_result;
				
					SET vStart_Cnt = vEnd_Cnt +1;
					IF vStart_Cnt  >= vRec_Cnt THEN
						LEAVE PROCESS_LOOP;
					END IF;      
					
				END LOOP PROCESS_LOOP;
				
				SET @format_stmt = CONCAT('SELECT Convert(format(',Result1,',","),CHAR) INTO @temp_result');

				PREPARE for_stmt FROM @format_stmt;
				EXECUTE for_stmt;
				DEALLOCATE PREPARE for_stmt;
                
                
                /* Displaying The Names */
                
			    IF Lifecycle_Segment = "ALL_Segment"
                THEN 
                
					SET @LifecycleNames = 'ALL';
                    
				ELSE 
				   
                   Set @SQL1=Concat('SELECT GROUP_CONCAT(`CLMSegmentName`
									SEPARATOR ", ") INTO @LifecycleNames
									FROM CLM_Segment
								WHERE CLMSegmentId IN (',Lifecycle_Segment,')');
						SELECT @SQL1;
						PREPARE statement from @SQL1;
						Execute statement;
                  
                  
				END IF;
                
                
                   Set @SQL1=Concat('SELECT GROUP_CONCAT(`Segment_Name`
									SEPARATOR ", ") INTO @CampaignSegNames
									FROM Campaign_Segment
								WHERE Segment_Id IN (',Campagin_Segment,')');
						SELECT @SQL1;
						PREPARE statement from @SQL1;
						Execute statement;
				
				
				INSERT IGNORE INTO Campaign_UI_Funnel_Coverage
				SELECT concat("LifeSegment [",@LifecycleNames,"] + Campaign_Segment [",@CampaignSegNames,"] => ",@temp_result),@temp_result;
			
			End IF;	
				
			
			
		-- ----------- EXCLUSION FILTER --------------------------------------------------------------------------
			SELECT Count(1)into Number_of_filters FROM Exclusion_Filter;
			
			IF Number_of_filters =0 
				Then 
					Select "NO Exclusion Filter Condition Is Selected"; 
					Set Exclusion_Filter_Coverage=0; 
			END IF;
			
			
				IF Number_of_filters !=0
					Then
					
						Select GROUP_CONCAT(`Condition` SEPARATOR ' and ') into Exclusion_Filter_Condition from Exclusion_Filter;
					
					/*Enabling Columns in Coverage View */
						SET @Combined_Condition = CONCAT_WS('AND',Campaign_SegmentReplacedQuery,Exclusion_Filter_Condition);
						
						CALL S_UI_Coverage_View(@Combined_Condition);
							
						
						   
					
					IF  Campaign_Segment_Coverage=0 
						THEN	
							SET Loop_Check_Var = FALSE;
							
							SET vStart_Cnt=0;
							SET vEnd_Cnt=0;
							SET Result1 = 0;
							
							PROCESS_LOOP: LOOP
								SET vEnd_Cnt = vStart_Cnt + vBatchSize;
								
								SET @batch_result = 0;
							
							
							SET @sql_stmt = CONCAT('SELECT COUNT(1) INTO @batch_result
																							FROM Customer_Coverage_View 
																							WHERE   ',Lifecycle_SegmentReplacedQuery,'
																							and Customer_Id not in (Select Customer_Id from Customer_Coverage_View where ',Exclusion_Filter_Condition,')
																							AND Customer_Id BETWEEN ',vStart_Cnt,' AND ',vEnd_Cnt,' ');
											
										
											PREPARE stmt FROM @sql_stmt;
											EXECUTE stmt; 
											DEALLOCATE PREPARE stmt;
											
								SET Result1 = Result1 + @batch_result;
							
						
							

								SET vStart_Cnt = vEnd_Cnt +1;
								IF vStart_Cnt  >= vRec_Cnt THEN
									LEAVE PROCESS_LOOP;
								END IF;      
								
							END LOOP PROCESS_LOOP;
							
							SET @format_stmt = CONCAT('SELECT Convert(format(',Result1,',","),CHAR) INTO @temp_result');

							PREPARE for_stmt FROM @format_stmt;
							EXECUTE for_stmt;
							DEALLOCATE PREPARE for_stmt;
                            
                            
                             /* Displaying The Names */
                
			    IF Lifecycle_Segment = "ALL_Segment"
                THEN 
                
					SET @LifecycleNames = 'ALL';
                    
				ELSE 
				   
                   Set @SQL1=Concat('SELECT GROUP_CONCAT(`CLMSegmentName`
									SEPARATOR ", ") INTO @LifecycleNames
									FROM CLM_Segment
								WHERE CLMSegmentId IN (',Lifecycle_Segment,')');
						SELECT @SQL1;
						PREPARE statement from @SQL1;
						Execute statement;
                  
                  
				END IF;
			

						INSERT IGNORE INTO Campaign_UI_Funnel_Coverage
						SELECT concat("LifeSegment [",@LifecycleNames,"] + Exclusion_Filter -> ",@temp_result),@temp_result;
								
						
					END IF;		
								
			
					
					IF  Campaign_Segment_Coverage!=0 
						THEN
								
								SET Loop_Check_Var = FALSE;
								
								SET vStart_Cnt=0;
								SET vEnd_Cnt=0;
								SET Result1 =0;
								
								PROCESS_LOOP: LOOP
									SET vEnd_Cnt = vStart_Cnt + vBatchSize;
									
									SET @batch_result = 0;
								
								
								SET @sql_stmt = CONCAT('SELECT COUNT(1) INTO @batch_result
																								FROM Customer_Coverage_View 
																								WHERE   ',Lifecycle_SegmentReplacedQuery,'
																								and ',Campaign_SegmentReplacedQuery,'
																								and Customer_Id not in (Select Customer_Id from Customer_Coverage_View where ',Exclusion_Filter_Condition,')
																								AND Customer_Id BETWEEN ',vStart_Cnt,' AND ',vEnd_Cnt,' ');
												
											
												PREPARE stmt FROM @sql_stmt;
												EXECUTE stmt; 
												DEALLOCATE PREPARE stmt;
												
									SET Result1 = Result1 + @batch_result;
								
							
								

									SET vStart_Cnt = vEnd_Cnt +1;
									IF vStart_Cnt  >= vRec_Cnt THEN
										LEAVE PROCESS_LOOP;
									END IF;      
									
								END LOOP PROCESS_LOOP;
								
								SET @format_stmt = CONCAT('SELECT Convert(format(',Result1,',","),CHAR) INTO @temp_result');

								PREPARE for_stmt FROM @format_stmt;
								EXECUTE for_stmt;
								DEALLOCATE PREPARE for_stmt;
                                
                                
                                 /* Displaying The Names */
                
								IF Lifecycle_Segment = "ALL_Segment"
								THEN 
								
									SET @LifecycleNames = 'ALL';
									
								ELSE 
								   
								   Set @SQL1=Concat('SELECT GROUP_CONCAT(`CLMSegmentName`
													SEPARATOR ", ") INTO @LifecycleNames
													FROM CLM_Segment
												WHERE CLMSegmentId IN (',Lifecycle_Segment,')');
										SELECT @SQL1;
										PREPARE statement from @SQL1;
										Execute statement;
								  
								  
								END IF;
								
								
								   Set @SQL1=Concat('SELECT GROUP_CONCAT(`Segment_Name`
													SEPARATOR ", ") INTO @CampaignSegNames
													FROM Campaign_Segment
												WHERE Segment_Id IN (',Campagin_Segment,')');
										SELECT @SQL1;
										PREPARE statement from @SQL1;
										Execute statement;
												
                                
									
									INSERT IGNORE INTO Campaign_UI_Funnel_Coverage
									SELECT concat("LifeSegment [",@LifecycleNames,"] + Campaign_Segment [",@CampaignSegNames,"] + Exclusion_Filter => ",@temp_result),@temp_result;
										
								
					END IF;
				
			END IF;
			

END IF;

-- -------------------------------Propensity Model --------------------------------------------

IF Propensity_Model_Id != '' AND Propensity_Segment != '' AND Propensity_Score != ''
THEN 


		SELECT ModelName
		INTO @Propensity_Model 
		FROM CLM_MCore_Model_Master
		WHERE ID = Propensity_Model_Id;

		IF Propensity_Segment = 'ALL'
					THEN 
						SET @Propensity_Model=CONCAT(@Propensity_Model,'_All');
                        
                        
						SET @Propensity_Condition=CONCAT_WS(''," ",@Propensity_Model,">= (1-",Propensity_Score,")") ;
						
					ELSE
						SET @Propensity_Model=CONCAT(@Propensity_Model,'_CLMSegment'); 
						
						SET @Propensity_Condition=CONCAT_WS(''," CLMSegmentName= '",Propensity_Segment,"' and ",@Propensity_Model,">= (1-",Propensity_Score,")") ;


		END IF;
        
         SELECT @Propensity_Condition;
		  
		 SET Loop_Check_Var = FALSE;

				
				SET vStart_Cnt=0;
				SET vEnd_Cnt=0;

				SET Result1 = 0;
				
				
				PROCESS_LOOP: LOOP
					SET vEnd_Cnt = vStart_Cnt + vBatchSize;
					
					SET @batch_result = 0;
				
				
					SET @sql_stmt = CONCAT('SELECT COUNT(1) INTO @batch_result 
															FROM CLM_MCore_Customer_Percentile 
															WHERE   ',@Propensity_Condition,'
															AND Customer_Id BETWEEN ',vStart_Cnt,' AND ',vEnd_Cnt,' ');
								
							    
								PREPARE stmt FROM @sql_stmt;
								EXECUTE stmt; 
								DEALLOCATE PREPARE stmt;
								
					SET Result1 = Result1 + @batch_result;
				
			   
				

					SET vStart_Cnt = vEnd_Cnt +1;
					IF vStart_Cnt  >= vRec_Cnt THEN
						LEAVE PROCESS_LOOP;
					END IF;      
					
				END LOOP PROCESS_LOOP;
				
				SET @format_stmt = CONCAT('SELECT Convert(format(',Result1,',","),CHAR) INTO @temp_result');

				PREPARE for_stmt FROM @format_stmt;
				EXECUTE for_stmt;
				DEALLOCATE PREPARE for_stmt;
				
                
                
                /*DisplayingNames*/
                
                SELECT Description
				INTO @Propensity_Description
				FROM CLM_MCore_Model_Master
				WHERE ID = Propensity_Model_Id;
				
								
				INSERT IGNORE INTO Campaign_UI_Funnel_Coverage
				SELECT concat("Propensity_Model [ (",@Propensity_Description,") , ",Propensity_Segment,", ",100*(Propensity_Score),"% ]  => ",@temp_result),@temp_result;

END IF;

SELECT Convert(format(MIN(cast(replace(Coverage,',','') as INT)),","),CHAR) INTO @Final_Coverage FROM Campaign_UI_Funnel_Coverage;



Drop View if EXISTS Campaign_UI_Funnel_Coverage_View;
Create Or Replace VIEW Campaign_UI_Funnel_Coverage_View AS
Select GROUP_CONCAT(NAME SEPARATOR '<br>') as Name from Campaign_UI_Funnel_Coverage;


				
	Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Coverage",Name,"Mobile_Coverage",@Final_Coverage,"Email_Coverage",@Final_Coverage) )),"]") into @temp_result from Campaign_UI_Funnel_Coverage_View;') ;
   
            SELECT @Query_String;
            PREPARE statement from @Query_String;
            Execute statement;
            Deallocate PREPARE statement; 
		
			
	


SET out_result_json = @temp_result;


SET @temp_result = '';
	

END$$
DELIMITER ;
