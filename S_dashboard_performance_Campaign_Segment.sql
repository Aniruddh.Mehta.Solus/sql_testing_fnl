DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_performance_Campaign_Segment`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text;
    declare IN_FILTER_CHANNEL text;
    declare IN_FILTER_CATEGORY text;
    declare IN_FILTER_THEME text;
    declare IN_FILTER_TRIGGER text;
    declare IN_FILTER_CAMPAIGN text;
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
    declare IN_AGGREGATION_4 text;
    declare IN_AGGREGATION_5 text;
    declare IN_AGGREGATION_6 text;
    declare IN_AGGREGATION_7 text;
    declare IN_AGGREGATION_8 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
    SET IN_FILTER_CHANNEL = json_unquote(json_extract(in_request_json,"$.FILTER_CHANNEL"));
    SET IN_FILTER_CATEGORY = json_unquote(json_extract(in_request_json,"$.FILTER_CATEGORY"));
    SET IN_FILTER_THEME = json_unquote(json_extract(in_request_json,"$.FILTER_THEME"));
    SET IN_FILTER_TRIGGER = json_unquote(json_extract(in_request_json,"$.FILTER_TRIGGER"));
    SET IN_FILTER_CAMPAIGN = json_unquote(json_extract(in_request_json,"$.FILTER_CAMPAIGN"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_AGGREGATION_4 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_4"));
    SET IN_AGGREGATION_5 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_5"));
    SET IN_AGGREGATION_6 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_6"));
    SET IN_AGGREGATION_7 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_7"));
    SET IN_AGGREGATION_8 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_8"));
    SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));

	IF IN_FILTER_CHANNEL = 'ALLCHANNEL' or IN_FILTER_CHANNEL is null
    THEN 
    SET IN_FILTER_CHANNEL = '';
    END IF;
	IF IN_FILTER_CATEGORY = 'ALL' or IN_FILTER_CATEGORY is null
    THEN 
    SET IN_FILTER_CATEGORY = '';
    END IF;
	
    IF IN_MEASURE = 'COL_PICKER' and IN_AGGREGATION_4 = 'SHOW_ALL_COLS'
    THEN
		set @view_stmt = concat('CREATE OR REPLACE VIEW V_Dashboard_Camp_seg_report_col
					AS (	select COLUMN_NAME from information_schema.columns
							where TABLE_NAME = "V_Dashboard_Camp_seg_Report" AND COLUMN_NAME <> "Campaign_Segment");');
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("COLUMN_NAME",`COLUMN_NAME`) )),"]")
        into @temp_result from V_Dashboard_Camp_seg_report_col;') ;
        
    END IF;
    
    
    select Value1 into @Incremental_Revenue_Method from UI_Configuration_Global where Config_Name = 'Incremental_Revenue_Method';
    SET @Incremental_Revenue_Method=ifnull(@Incremental_Revenue_Method,'Method2');
    select @Incremental_Revenue_Method;
    IF @Incremental_Revenue_Method = 'Method1'
    THEN
		SET @incr_rev_concat='_By_Day';
        SET @group_cond1='EE_Date';
        SET @mtd_concat='';
	ELSEIF @Incremental_Revenue_Method = 'Method2'
    THEN
		SET @group_cond1='YM';
		SET @incr_rev_concat='_By_Campaign_Segment';
        SET @mtd_concat='MTD_';
	ELSE
		SET @incr_rev_concat='';
        SET @mtd_concat='';
		SET @incr_Rev = ' round((CASE WHEN SUM(Control_Base) < 0 or SUM(Target_Delivered) = 0 then 0 WHEN (((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*100 < 0 then 0 WHEN SUM(Control_Base)>0 and SUM(Control_Responder)=0 then SUM(Target_Revenue)
					ELSE ifnull((((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*(SUM(Target_Revenue)/SUM(Target_Responder))*(SUM(Target_Delivered)),0) END),0) ';
		SET @group_cond1='YM';
	END IF;
    SELECT @mtd_concat;
    
    
		SET @sql1=CONCAT('
		CREATE OR REPLACE VIEW CLM_Event_Dashboard_STLT
        as 
		SELECT 
			`Campaign_Segment`,
			`Theme`,
			`Channel`,
			`Campaign`,
			`EE_Date`,
			`Event_Execution_Date_ID`,
			`Event_ID`,
			`Control_Base` as `Control_Base`,
			`Target_Base`,
			`Target_Delivered`,
			`Target_Delivered_NA`,
			`Control_Responder` as `Control_Responder`,
			`Target_Responder`,
			`Target_Revenue`,
			`Target_Discount`,
			`Target_Bills`,
			`Target_Open`,
			`Target_CTA`,
			`Created_Date`,
			`YM`,
			-- `Incremental_Revenue',@incr_rev_concat,'` AS `Incremental_Revenue`,
            `',@mtd_concat,'Incremental_Revenue',@incr_rev_concat,'` AS `MTD_Incremental_Revenue`
			-- `MTD_Incremental_Revenue_By_Campaign_Segment` AS `MTD_Incremental_Revenue`
		FROM `CLM_Event_Dashboard_Campaign_Segment`;
        ');
        select @sql1;
		PREPARE statement from @sql1;
		Execute statement;
		Deallocate PREPARE statement;

	SET @wherecond1 = ' 1=1 ';
	SET @wherecond2 = ' 1=1 ';
    
    

CREATE OR REPLACE VIEW V_Dashboard_CLM_Event_Campaign_Segment_Report AS
    SELECT 
        CASE
            WHEN DATEDIFF(End_Date_ID, Start_Date_ID) < 32 THEN 'GTM'
            ELSE 'CLM'
        END AS `Category`,
        b.`Name` AS `Trigger`,
        a.*
    FROM
        CLM_Event_Dashboard_STLT a
            JOIN
        Event_Master b ON a.Event_ID = b.ID;
    
    SET @create_view=CONCAT('CREATE OR REPLACE VIEW V_Dashboard_Campaign_Segment_Report_Temp
	   as
	   select 
       eve.Category,
	   eve.`Trigger`,
	   eve.`Theme`,
       eve.`Channel`,
	   eve.`Campaign`,
       eve.Campaign_Segment,
	   eve.YM,
	   eve.Event_Id,
	   eve.EE_Date,
	   SUM(`Target_Base`) AS Target_Base,
	   SUM(`Target_Delivered`) AS Target_Delivered,
	   SUM(`Target_Delivered_NA`) AS Target_Delivered_NA,
	   SUM(`Control_Base`) AS Control_Base,
	   SUM(`Target_Responder`) AS Target_Responder,
	   SUM(`Control_Responder`) AS Control_Responder,
	   SUM(Target_Revenue) AS Target_Revenue,
	   MAX(MTD_Incremental_Revenue) AS Incremental_Revenue
	   from V_Dashboard_CLM_Event_Campaign_Segment_Report eve
	   where ',@wherecond1,' and ',@wherecond2,'
	   group by Campaign_Segment,Event_Id,',@group_cond1,'');
 
   
	select @create_view;
	PREPARE statement from @create_view;
	Execute statement;
	Deallocate PREPARE statement;
    
    
    IF IN_FILTER_CATEGORY = ''  or IN_FILTER_CATEGORY is null
	THEN
	SET @wherecond1 = ' 1=1 ';
	ELSE
	SET @wherecond1 = CONCAT(' Category="',IN_FILTER_CATEGORY,'" ');
    END IF;
        
	
    IF IN_FILTER_THEME = '' or IN_FILTER_THEME is null
	THEN
	SET @wherecond2 = ' 1=1 ';
	ELSE
	SET @wherecond2 = CONCAT(' Theme="',IN_FILTER_THEME,'" ');
	END IF;
        
        
	IF IN_FILTER_CAMPAIGN = '' or IN_FILTER_CAMPAIGN is null
	THEN
	SET @wherecond3 = ' 1=1 ';
	ELSE
	SET @wherecond3 = CONCAT(' Campaign="',IN_FILTER_CAMPAIGN,'" ');
	END IF;
        
        
	IF IN_FILTER_TRIGGER = '' or IN_FILTER_TRIGGER is null
	THEN
	SET @wherecond4 = ' 1=1 ';
	ELSE
	SET @wherecond4 = CONCAT(' `Trigger`="',IN_FILTER_TRIGGER,'" ');
	END IF;
        
        
	IF IN_FILTER_CHANNEL = '' or IN_FILTER_CHANNEL is null
	THEN
	SET @wherecond5 = ' 1=1 ';
	ELSE
	SET @wherecond5 = CONCAT(' `Channel`="',IN_FILTER_CHANNEL,'" ');
	END IF;


	SET @filter_cond = concat('	',@wherecond1,' -- CATEGORY
								AND ',@wherecond2,' -- THEME
                                AND ',@wherecond3,' -- CAMPAIGN
                                AND ',@wherecond4,' -- TRIGGER
                                AND ',@wherecond5,' -- CHANNEL
                                ');
    
    SELECT @filter_cond;
    
	IF IN_MEASURE = 'TABLE' 
	THEN
    
    IF IN_AGGREGATION_7 ="Search" 
    THEN
    SET @Condition = CONCAT('Campaign_Segment LIKE  "%',IN_AGGREGATION_8,'%"');
    ELSE
    SET @Condition = ' 1=1 ';
	Select @Condition;
    END IF;
    
    set @view_stmt= concat('CREATE OR REPLACE VIEW V_Dashboard_Camp_seg_Report
					AS
					SELECT 
                    `Campaign_Segment`,
                    CAST(format(ifnull(SUM(`Target_Base`),0),",") AS CHAR) as `TG Base`,
					CASE WHEN (SUM(`Target_Delivered`) = SUM(`Target_Delivered_NA`)) then " - " ELSE 
						CAST(format(ifnull(SUM(`Target_Delivered`),0),",") AS CHAR) END AS `TG Delivered`,
                    CASE WHEN (SUM(`Target_Delivered`) = SUM(`Target_Delivered_NA`)) then " - " ELSE 
						round((SUM(`Target_Delivered`)/SUM(`Target_Base`))*100,0) END AS `Delivered%`,
                    CAST(format(ifnull(SUM(`Control_Base`),0),",") AS CHAR) AS `CG Base`,
					CAST(format(ifnull(SUM(`Target_Responder`),0),",") AS CHAR) AS `TG Responders`,
                    CAST(format(ifnull(SUM(`Control_Responder`),0),",") AS CHAR) AS `CG Responders`,
					concat(round((CASE WHEN SUM(`Target_Delivered`) <=0 THEN 0 ELSE
						(SUM(`Target_Responder`)/SUM(`Target_Delivered`))*100 END),2),"%") AS `Target Response`,
					concat(round((CASE WHEN SUM(`Control_Base`) <=0 THEN 0 ELSE 
						(SUM(`Control_Responder`)/SUM(`Control_Base`))*100 END),2),"%") AS `Control Response`,
                    CAST(format(ifnull(SUM(Target_Revenue),0),",") AS CHAR) AS `Target Revenue`,
                    CASE WHEN SUM(Target_Responder) <= 0 then 0 else CAST(format(ifnull(ROUND(SUM(Target_Revenue)/SUM(Target_Responder),0),0),",") AS CHAR) END AS `Average Spend`,
                    concat(round(CASE WHEN (CASE WHEN SUM(`Target_Delivered`) <=0 THEN 0 
                    ELSE (SUM(`Target_Responder`)/SUM(`Target_Delivered`))*100 END)-(CASE WHEN SUM(`Control_Base`) <=0 THEN 0 ELSE (SUM(`Control_Responder`)/SUM(`Control_Base`))*100 END)<0 THEN 0 ELSE
(CASE WHEN SUM(`Target_Delivered`) <=0 THEN 0 
                    ELSE (SUM(`Target_Responder`)/SUM(`Target_Delivered`))*100 END)-(CASE WHEN SUM(`Control_Base`) <=0 THEN 0 ELSE (SUM(`Control_Responder`)/SUM(`Control_Base`))*100 END)END,2),"%") AS `TG-CG Response`,
                    cast(ifnull(format(round(sum(Target_Base)*(CASE WHEN (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))<=0 THEN 0 ELSE (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))END),2)/100,","),0)as char) AS `Incr Bills`,
					CAST(format(ifnull(round(SUM(Incremental_Revenue),0),0),",") AS CHAR) AS `Rev Lift`
					from V_Dashboard_Campaign_Segment_Report_Temp 
                    where YM=',IN_FILTER_CURRENTMONTH,' and ',@filter_cond,'
                    AND ',@Condition,'
					 GROUP BY 1
                     ');
                     

		IF IN_AGGREGATION_6 = 'DEFAULT' or IN_AGGREGATION_6 = ''
		THEN		
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Campaign Segment",`Campaign_Segment`,"TG Base",`TG Base`,"CG Base",`CG Base`,"TG-CG Response",`TG-CG Response`,"Rev Lift",`Rev Lift`) )),"]")into @temp_result from V_Dashboard_Camp_seg_Report ',';') ;
		END IF;
        
		IF IN_AGGREGATION_6 <> "DEFAULT" AND IN_AGGREGATION_6 <> ''
		THEN
		SELECT IN_AGGREGATION_6;
        SELECT @select_q;
		set @select_q = IN_AGGREGATION_6;
        SELECT @select_q;
        set @select_q= replace(@select_q,"[","");
        set @select_q= replace(@select_q,"]","");
        set @select_q = replace(@select_q,'"',"");
        select @select_q;
		SET @select_query2 = CONCAT(@select_query,",",@select_q);
		select (length(@select_q)-length(replace(@select_q,',','')))+1 into @num_of_channel;
		select @num_of_channel;
		set @loop_var=1;
        set @top = '';
        set @json_select2 = concat('"Campaign Segment",`Campaign_Segment`');
		select @loop_var,@num_of_channel,@select_q, @json_select;
		WHILE @loop_var<=@num_of_channel
		do
			SET @selected_channel=substring_index(substring_index(concat(@select_q,',-1'),',',@loop_var),',',-1);
            set @selected_channel = TRIM(@selected_channel);
			set @json_select2=concat(@json_select2,',"',@selected_channel,'",`',@selected_channel,'`');
            set @top=concat(@top,',`',@selected_channel,'`');
			set @loop_var=@loop_var+1;
		end while;
        -- set @json_select = substring(@json_select, 2);
        set @top = substring(@top, 2);
        select @json_select2;
        select @top;
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object(',@json_select2,') )),"]")into @temp_result from V_Dashboard_Camp_seg_Report;') ;
		
		
		
		END IF;
    END IF;
    
    IF IN_MEASURE = 'THEME_NAME'
    THEN
		set @view_stmt = concat('CREATE OR REPLACE VIEW V_Dashboard_Theme AS
								select distinct Theme as CampaignThemeName from CLM_Event_Dashboard a join Event_Master b on a.Event_ID = b.ID');
		SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Theme",CAST(`CampaignThemeName` AS CHAR)) )),"]") into @temp_result from V_Dashboard_Theme;' ;
SELECT @Query_String;
    END IF;
    
    IF IN_MEASURE = 'CAMPAIGN_NAME'
    THEN
		set @view_stmt = concat('CREATE OR REPLACE VIEW V_Dashboard_Campaign AS
								select distinct Campaign as CampaignName from CLM_Event_Dashboard a join Event_Master b on a.Event_ID = b.ID');
		SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Campaign",CAST(`CampaignName` AS CHAR)) )),"]") into @temp_result from V_Dashboard_Campaign;' ;
SELECT @Query_String;
    END IF;
    
    IF IN_SCREEN <> "DOWNLOAD"
    THEN
    select @view_stmt;
	PREPARE statement from @view_stmt;
	Execute statement;
	Deallocate PREPARE statement;
	SELECT @Query_String;
	PREPARE statement from @Query_String;
	Execute statement;
	Deallocate PREPARE statement; 
    SET out_result_json = @temp_result;
    ELSE
		call S_dashboard_performance_download('V_Dashboard_Camp_seg_report',@out_result_json1);
		SELECT @out_result_json1 INTO out_result_json;
    END IF;
    
END$$
DELIMITER ;
