
CREATE OR REPLACE PROCEDURE `S_UI_OnDemand_Campaign`(IN in_request_json json, OUT out_result_json json)
BEGIN
    
    -- User Information

    declare IN_USER_NAME varchar(240); 
    declare IN_SCREEN varchar(400); 
    declare vSuccess  varchar(400); 
    declare vErrMsg  varchar(400);
    declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text; 
	declare IN_AGGREGATION_3 text;
    
    	

  -- --------------------- Decoding the JSON -----------------------------------------------------------------------------------------------------------------------------------------------
  
  
    Set @Err =0;
	
   
   -- User Information
   
    SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1")); 
	
    SELECT IN_AGGREGATION_1 as 'Event_Id';
    
    
    
    /*Call CLM For On Demand */
    
	CALL S_CLM_Run ( IN_AGGREGATION_1,'RUN',@no_of_customers,@no_of_customers_final,@Filename,@FileHeader,@FileLocation,@RecoFilename,@RecoFileHeader,@RecoFileRecoHeader,@RecoFileLocation,@vSuccess,@vErrMsg );
	
    /*Check If the Run Is Successful Or Not*/
    
    
		SET @Msg = 'Sucess';
	  
    	Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","',@Msg,'") )),"]") into @temp_result                       
												       ;') ;
      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;
                            
    SET out_result_json = @temp_result;


END

