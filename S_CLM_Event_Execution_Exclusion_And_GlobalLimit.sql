
CREATE or replace PROCEDURE `S_CLM_Event_Execution_Exclusion_And_GlobalLimit`()
BEGIN

-- Exclusion_Filter
CALL S_CLM_Exclusion_Filter_Execution; 

-- Overall_Limit
SELECT 
    `Value`
INTO @Overall_Limit FROM
    M_Config
WHERE
    `Name` = 'CLM_Event_Limit';
select count(1) into @Customers_Count from Event_Execution where Is_Target = 'Y';
if ifnull(@Overall_Limit,-1)>0 and ifnull(@Customers_Count,0) > @Overall_Limit
then
	SET @Overall_Limit = @Customers_Count-@Overall_Limit;
	set @sql2=concat("UPDATE Event_Execution SET Is_Target = 'X' where Is_Target = 'Y' order by RAND() LIMIT ",@Overall_Limit,"");
	select @sql2;
		prepare stmt2 from @sql2;
		execute stmt2;
		deallocate prepare stmt2;
	insert ignore into Event_Execution_Rejected
    select * from Event_Execution Where Is_Target = 'X';
    UPDATE Event_Execution_Rejected 
	SET Message=concat(@Event_ID_cur1,' : Global Limit Exceeded')
	WHERE Message is null and Is_Target ='X';
    DELETE from Event_Execution where Is_Target = 'X';
end if;

END


