DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_Product_View`(IN in_request_json JSON, OUT out_result_json JSON)
BEGIN

    DECLARE IN_USER_NAME VARCHAR(240); 
    DECLARE IN_SCREEN VARCHAR(400); 
    DECLARE temp_result JSON;
    DECLARE IN_FILTER_CURRENTMONTH TEXT; 
    DECLARE IN_FILTER_MONTHDURATION TEXT; 
    DECLARE IN_MEASURE TEXT; 
    DECLARE IN_NUMBER_FORMAT TEXT;
    DECLARE IN_AGGREGATION_1 TEXT; 
    DECLARE IN_AGGREGATION_2 TEXT;
    DECLARE IN_AGGREGATION_3 TEXT;
    DECLARE NUM_PERCENT TEXT;
    DECLARE Query_String TEXT;
    DECLARE IN_FILTER_SEGMENT TEXT;
    DECLARE IN_FILTER_PRODUCT TEXT;
    DECLARE IN_FILTER_PRODUCT_TYPE TEXT;
    
    SET IN_FILTER_CURRENTMONTH = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.FILTER_CURRENTMONTH"));
    SET IN_FILTER_MONTHDURATION = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.FILTER_MONTHDURATION"));
    SET IN_MEASURE = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.MEASURE"));
    SET IN_NUMBER_FORMAT = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.NUMBER_FORMAT"));
    SET IN_AGGREGATION_1 = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.AGGREGATION_1"));
    SET IN_AGGREGATION_2 = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.AGGREGATION_2"));
    SET IN_AGGREGATION_3 = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.AGGREGATION_3"));
   
    SET IN_FILTER_SEGMENT = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.FILTER_SEGMENT"));
    SET IN_FILTER_PRODUCT = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.FILTER_PRODUCT"));
    SET IN_FILTER_PRODUCT_TYPE = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.FILTER_PRODUCT_TYPE"));
    SET NUM_PERCENT = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.NUM_PERCENT"));
    
    SET IN_SCREEN = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.SCREEN"));
   
    SET @temp_result1 = '';
   
   IF IN_FILTER_PRODUCT_TYPE = 'Bought' OR IN_FILTER_PRODUCT_TYPE = 'Entry' THEN
    if IN_AGGREGATION_2="L6M" Then
   IF IN_MEASURE = 'Product_View' THEN
    
		IF IN_AGGREGATION_1 = 'Base' THEN
        BEGIN        
       
 if IN_FILTER_SEGMENT='' AND IN_FILTER_CURRENTMONTH<>'' THEN
        BEGIN
           
        	

			if NUM_PERCENT='PERCENT' THEN
            begin
			
SET @Query_String = CONCAT('WITH total AS (
    SELECT SUM(T6MthActive) AS total
    FROM Dashboard_Insights_Base_ByProduct
    WHERE Aggregation = \'BY_MONTH\'
    AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
    AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
  
   
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", product_name, "num", ROUND((SUM(T6MthActive) / total.total) * 100, 1)) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct, total
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
    GROUP BY product_name
     HAVING ROUND((SUM(T6MthActive) / (SELECT total FROM total)) * 100, 1) != 0
) AS result;');
end;

			
            elseif NUM_PERCENT='NUMBER' Then
            begin
            			
        	   SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", product_name, "num", (SUM(T6MthActive)) ) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
       
    GROUP BY product_name
    HAVING (SUM(T6MthActive)) != 0
) AS result;');
end;
            END if;
        
        
        
        
        END;      
        
        END IF;
        
        
 if IN_FILTER_SEGMENT<>'' AND IN_FILTER_CURRENTMONTH<>'' THEN
        BEGIN
        set @q="1";
        	

						if NUM_PERCENT='PERCENT' THEN
            begin
			
        SET @Query_String = CONCAT('WITH total AS (
        SELECT SUM(subquery.total_sum) AS total
FROM (
    SELECT SUM(T6MthActive) AS total_sum
    FROM Dashboard_Insights_Base_ByProduct
    WHERE Aggregation = \'BY_MONTH\'
    AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
    AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
     Group By CLMSegmentName="',IN_FILTER_SEGMENT,'"
     ) AS subquery
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", product_name, "num", ROUND((SUM(T6MthActive) / total.total) * 100, 1)) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct, total
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
        and CLMSegmentName="',IN_FILTER_SEGMENT,'"
    GROUP BY product_name
     HAVING ROUND((SUM(T6MthActive) / (SELECT total FROM total)) * 100, 2) != 0
) AS result;');
end;

			
            elseif NUM_PERCENT='NUMBER' Then
            begin
            			
        SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", product_name, "num", (SUM(T6MthActive))) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
        and CLMSegmentName="',IN_FILTER_SEGMENT,'"
    GROUP BY product_name
    HAVING (SUM(T6MthActive)) != 0
) AS result;');
end;
            END if;
        
        
        
        
        END;      
        
        END IF;

		END;
            end if;
            IF IN_AGGREGATION_1 = 'Bills' THEN
            begin

	

 if IN_FILTER_SEGMENT='' AND IN_FILTER_CURRENTMONTH<>'' THEN
        BEGIN
           
        	

			if NUM_PERCENT='PERCENT' THEN
            begin
			
SET @Query_String = CONCAT('WITH total AS (
    SELECT SUM(T6MthActiveBills) AS total
    FROM Dashboard_Insights_Base_ByProduct
    WHERE Aggregation = \'BY_MONTH\'
    AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
    AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
   
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", product_name, "num", ROUND((SUM(T6MthActiveBills) / total.total) * 100, 1)) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct, total
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
    GROUP BY product_name
     HAVING ROUND((SUM(T6MthActiveBills) / (SELECT total FROM total)) * 100, 1) != 0
) AS result;');
end;

			
            elseif NUM_PERCENT='NUMBER' Then
            begin
            			
        	   SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", product_name, "num", (SUM(T6MthActiveBills)) ) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
       
    GROUP BY product_name
    HAVING (SUM(T6MthActiveBills))  != 0
) AS result;');
end;
            END if;
        
        
        
        
        END;      
        
        END IF;
        
        
 if IN_FILTER_SEGMENT<>'' AND IN_FILTER_CURRENTMONTH<>'' THEN
        BEGIN
        set @q="1";
        	

						if NUM_PERCENT='PERCENT' THEN
            begin
			
        SET @Query_String = CONCAT('WITH total AS (
              SELECT SUM(subquery.total_sum) AS total
FROM (
    SELECT SUM(T6MthActiveBills) AS total_sum
    FROM Dashboard_Insights_Base_ByProduct
    WHERE Aggregation = \'BY_MONTH\'
    AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
    AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
     Group by CLMSegmentName="',IN_FILTER_SEGMENT,'"
     ) AS subquery
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", product_name, "num", ROUND((SUM(T6MthActiveBills) / total.total) * 100, 1)) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct, total
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
        and CLMSegmentName="',IN_FILTER_SEGMENT,'"
    GROUP BY product_name
     HAVING ROUND((SUM(T6MthActiveBills) / (SELECT total FROM total)) * 100, 1) != 0
) AS result;');
end;

			
            elseif NUM_PERCENT='NUMBER' Then
            begin
            			
        SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", product_name, "num", (SUM(T6MthActiveBills))) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
        and CLMSegmentName="',IN_FILTER_SEGMENT,'"
    GROUP BY product_name
    HAVING (SUM(T6MthActiveBills)) != 0
) AS result;');
end;
            END if;
        
        
        
        
        END;      
        
        END IF;

end;
            end if;
            
            IF IN_AGGREGATION_1 = 'Amount' THEN
         begin

 if IN_FILTER_SEGMENT='' AND IN_FILTER_CURRENTMONTH<>'' THEN
        BEGIN
           
        	

			if NUM_PERCENT='PERCENT' THEN
            begin
			
SET @Query_String = CONCAT('WITH total AS (
    SELECT SUM(T6MthActiveRevenue) AS total
    FROM Dashboard_Insights_Base_ByProduct
    WHERE Aggregation = \'BY_MONTH\'
    AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
    AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
   
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", product_name, "num", ROUND((SUM(T6MthActiveRevenue) / total.total) * 100, 1)) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct, total
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
    GROUP BY product_name
     HAVING ROUND((SUM(T6MthActiveRevenue) / (SELECT total FROM total)) * 100, 1) != 0
) AS result;');
end;

			
            elseif NUM_PERCENT='NUMBER' Then
            begin
            			
        	   SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", product_name, "num", (SUM(T6MthActiveRevenue)) ) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
       
    GROUP BY product_name
     HAVING (SUM(T6MthActiveRevenue)) != 0
) AS result;');
end;
            END if;
        
        
        
        
        END;      
        
        END IF;
        
        
  if IN_FILTER_SEGMENT<>'' AND IN_FILTER_CURRENTMONTH<>'' THEN
        BEGIN
        set @q="1";
        	

						if NUM_PERCENT='PERCENT' THEN
            begin
			
        SET @Query_String = CONCAT('WITH total AS (
                SELECT SUM(subquery.total_sum) AS total
FROM (
    SELECT SUM(T6MthActiveRevenue) AS total_sum
    FROM Dashboard_Insights_Base_ByProduct
    WHERE Aggregation = \'BY_MONTH\'
    AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
    AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
    Group by CLMSegmentName="',IN_FILTER_SEGMENT,'"
    ) AS subquery
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", product_name, "num", ROUND((SUM(T6MthActiveRevenue) / total.total) * 100, 1)) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct, total
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
        and CLMSegmentName="',IN_FILTER_SEGMENT,'"
    GROUP BY product_name
     HAVING ROUND((SUM(T6MthActiveRevenue) / (SELECT total FROM total)) * 100, 1) != 0
) AS result;');
end;

			
            elseif NUM_PERCENT='NUMBER' Then
            begin
            			
        SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", product_name, "num", (SUM(T6MthActiveRevenue))) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
        and CLMSegmentName="',IN_FILTER_SEGMENT,'"
    GROUP BY product_name
     HAVING (SUM(T6MthActiveRevenue)) != 0
) AS result;');
end;
            END if;
        
        
        
        
        END;      
        
        END IF;

end;


            end if;
        

            
	end if;
    
    
       IF IN_MEASURE = 'Segment_View' THEN
       
    
        IF IN_AGGREGATION_1 = 'Base' THEN
        BEGIN

 if IN_FILTER_PRODUCT='' AND IN_FILTER_CURRENTMONTH<>'' THEN
        BEGIN
        
        
			if NUM_PERCENT='PERCENT' THEN
            begin
			
SET @Query_String = CONCAT('WITH total AS (
    SELECT SUM(T6MthActive) AS total
    FROM Dashboard_Insights_Base_ByProduct
    WHERE Aggregation = \'BY_MONTH\'
    AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
    AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
   
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", CLMSegmentName, "num", ROUND((SUM(T6MthActive) / total.total) * 100, 1)) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct, total
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
    GROUP BY CLMSegmentName
     HAVING ROUND((SUM(T6MthActive) / (SELECT total FROM total)) * 100, 1) != 0
) AS result;');
end;

			
            elseif NUM_PERCENT='NUMBER' Then
            begin
            			
        	   SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", CLMSegmentName, "num", (SUM(T6MthActive)) ) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
       
    GROUP BY CLMSegmentName
     HAVING (SUM(T6MthActive)) != 0
) AS result;');
end;
            END if;
        
        
        
        END;      
        
        END IF;
        
        
 if IN_FILTER_PRODUCT<>'' AND IN_FILTER_CURRENTMONTH<>'' THEN
        BEGIN
        set @q="1";
      

					if NUM_PERCENT='PERCENT' THEN
            begin
			
SET @Query_String = CONCAT('WITH total AS (
 SELECT SUM(subquery.total_sum) AS total
FROM (
    SELECT SUM(T6MthActive) AS total_sum
    FROM Dashboard_Insights_Base_ByProduct
    WHERE Aggregation = \'BY_MONTH\'
    AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
    AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
    Group by Product_Id="',IN_FILTER_PRODUCT,'"
    ) AS subquery
   
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", CLMSegmentName, "num", ROUND((SUM(T6MthActive) / total.total) * 100, 1)) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct, total
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
        and Product_Id="',IN_FILTER_PRODUCT,'"
    GROUP BY CLMSegmentName
     HAVING ROUND((SUM(T6MthActive) / (SELECT total FROM total)) * 100, 1) != 0
) AS result;');
end;

			
            elseif NUM_PERCENT='NUMBER' Then
            begin
            			
        	   SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", CLMSegmentName, "num", (SUM(T6MthActive))) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
        and Product_Id="',IN_FILTER_PRODUCT,'"
       
    GROUP BY CLMSegmentName
     HAVING (SUM(T6MthActive)) != 0
) AS result;');
end;
            END if;
        

		END;
            end if;
            end;
            end if;
            IF IN_AGGREGATION_1 = 'Bills' THEN
            begin

 if IN_FILTER_PRODUCT='' AND IN_FILTER_CURRENTMONTH<>'' THEN
        BEGIN
        
        set @q="1";
        	
	if NUM_PERCENT='PERCENT' THEN
            begin
			
SET @Query_String = CONCAT('WITH total AS (
    SELECT SUM(T6MthActiveBills) AS total
    FROM Dashboard_Insights_Base_ByProduct
    WHERE Aggregation = \'BY_MONTH\'
    AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
    AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
   
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", CLMSegmentName, "num", ROUND((SUM(T6MthActiveBills) / total.total) * 100, 1)) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct, total
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
    GROUP BY CLMSegmentName
     HAVING ROUND((SUM(T6MthActiveBills) / (SELECT total FROM total)) * 100, 1) != 0
) AS result;');
end;

			
            elseif NUM_PERCENT='NUMBER' Then
            begin
            			
        	   SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", CLMSegmentName, "num", (SUM(T6MthActiveBills)) ) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
       
    GROUP BY CLMSegmentName
     HAVING (SUM(T6MthActiveBills))  != 0
) AS result;');
end;
            END if;
        
        
        
        END;      
        
        END IF;
        
        
        END;      
        
        END IF;
        
        
 if IN_FILTER_PRODUCT<>'' AND IN_FILTER_CURRENTMONTH<>'' THEN
        BEGIN
        set @q="1";

					if NUM_PERCENT='PERCENT' THEN
            begin
			
SET @Query_String = CONCAT('WITH total AS (
 SELECT SUM(subquery.total_sum) AS total
FROM (
    SELECT SUM(T6MthActiveBills) AS total_sum
    FROM Dashboard_Insights_Base_ByProduct
    WHERE Aggregation = \'BY_MONTH\'
    AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
    AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
     Group by Product_Id="',IN_FILTER_PRODUCT,'"
     ) AS subquery
   
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", CLMSegmentName, "num", ROUND((SUM(T6MthActiveBills) / total.total) * 100, 1)) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct, total
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
        and Product_Id="',IN_FILTER_PRODUCT,'"
    GROUP BY CLMSegmentName
     HAVING ROUND((SUM(T6MthActiveBills) / (SELECT total FROM total)) * 100, 1) != 0
) AS result;');
end;

			
            elseif NUM_PERCENT='NUMBER' Then
            begin
            			
        	   SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", CLMSegmentName, "num", (SUM(T6MthActiveBills))) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
        and Product_Id="',IN_FILTER_PRODUCT,'"
       
    GROUP BY CLMSegmentName
     HAVING (SUM(T6MthActiveBills)) != 0
) AS result;');
end;
            END if;

end;
            end if;
            IF IN_AGGREGATION_1 = 'Amount' THEN
         begin
 

 if IN_FILTER_PRODUCT='' AND IN_FILTER_CURRENTMONTH<>'' THEN
        BEGIN

					if NUM_PERCENT='PERCENT' THEN
            begin
			
SET @Query_String = CONCAT('WITH total AS (
    SELECT SUM(T6MthActiveRevenue) AS total
    FROM Dashboard_Insights_Base_ByProduct
    WHERE Aggregation = \'BY_MONTH\'
    AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
    AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
    
   
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", CLMSegmentName, "num", ROUND((SUM(T6MthActiveRevenue) / total.total) * 100, 1)) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct, total
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
       
    GROUP BY CLMSegmentName
     HAVING ROUND((SUM(T6MthActiveRevenue) / (SELECT total FROM total)) * 100, 1) != 0
) AS result;');
end;

			
            elseif NUM_PERCENT='NUMBER' Then
            begin
            			
        	   SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", CLMSegmentName, "num", (SUM(T6MthActiveRevenue))) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
       
       
    GROUP BY CLMSegmentName
     HAVING (SUM(T6MthActiveRevenue))!= 0
) AS result;');
end;
            END if;
        

		END;
            end if;
        
        
 if IN_FILTER_PRODUCT<>'' AND IN_FILTER_CURRENTMONTH<>'' THEN
        BEGIN
        set @q="1";
      

					if NUM_PERCENT='PERCENT' THEN
            begin
			
SET @Query_String = CONCAT('WITH total AS (
 SELECT SUM(subquery.total_sum) AS total
FROM (
    SELECT SUM(T6MthActiveRevenue) AS total_sum
    FROM Dashboard_Insights_Base_ByProduct
    WHERE Aggregation = \'BY_MONTH\'
    AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
    AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
     Group by Product_Id="',IN_FILTER_PRODUCT,'"
     )as subquery
   
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", CLMSegmentName, "num", ROUND((SUM(T6MthActiveRevenue) / total.total) * 100, 1)) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct, total
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
        and Product_Id="',IN_FILTER_PRODUCT,'"
    GROUP BY CLMSegmentName
     HAVING ROUND((SUM(T6MthActiveRevenue) / (SELECT total FROM total)) * 100, 1) != 0
) AS result;');
end;

			
            elseif NUM_PERCENT='NUMBER' Then
            begin
            			
        	   SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", CLMSegmentName, "num", (SUM(T6MthActiveRevenue))) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
        and Product_Id="',IN_FILTER_PRODUCT,'"
       
    GROUP BY CLMSegmentName
     HAVING (SUM(T6MthActiveRevenue)) != 0
) AS result;');
end;
            END if;
        

		END;
            end if;

end;


            end if;

            
	end if;
    
  end if;
  
  
  
   if IN_AGGREGATION_2="L12M" Then
   
   IF IN_MEASURE = 'Product_View' THEN
    
		IF IN_AGGREGATION_1 = 'Base' THEN
        BEGIN        
       
 if IN_FILTER_SEGMENT='' AND IN_FILTER_CURRENTMONTH<>'' THEN
        BEGIN
           
        	

			if NUM_PERCENT='PERCENT' THEN
            begin
			
SET @Query_String = CONCAT('WITH total AS (
    SELECT SUM(T12MthActive) AS total
    FROM Dashboard_Insights_Base_ByProduct
    WHERE Aggregation = \'BY_MONTH\'
    AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
    AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
   
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", product_name, "num", ROUND((SUM(T12MthActive) / total.total) * 100, 1)) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct, total
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
    GROUP BY product_name
     HAVING ROUND((SUM(T12MthActive) / (SELECT total FROM total)) * 100, 1) != 0
) AS result;');
end;

			
            elseif NUM_PERCENT='NUMBER' Then
            begin
            			
        	   SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", product_name, "num", (SUM(T12MthActive)) ) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
       
    GROUP BY product_name
     HAVING  (SUM(T12MthActive)) != 0
) AS result;');
end;
            END if;
        
        
        
        
        END;      
        
        END IF;
        
        
 if IN_FILTER_SEGMENT<>'' AND IN_FILTER_CURRENTMONTH<>'' THEN
        BEGIN
        set @q="1";
        	

						if NUM_PERCENT='PERCENT' THEN
            begin
			
        SET @Query_String = CONCAT('WITH total AS (
         SELECT SUM(subquery.total_sum) AS total
FROM (
    SELECT SUM(T12MthActive) AS total_sum
    FROM Dashboard_Insights_Base_ByProduct
    WHERE Aggregation = \'BY_MONTH\'
    AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
    AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
     group by CLMSegmentName="',IN_FILTER_SEGMENT,'"
     ) AS subquery
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", product_name, "num", ROUND((SUM(T12MthActive) / total.total) * 100, 1)) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct, total
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
        and CLMSegmentName="',IN_FILTER_SEGMENT,'"
    GROUP BY product_name
     HAVING ROUND((SUM(T12MthActive) / (SELECT total FROM total)) * 100, 1) != 0
) AS result;');
end;

			
            elseif NUM_PERCENT='NUMBER' Then
            begin
            			
        SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", product_name, "num", (SUM(T12MthActive))) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
        and CLMSegmentName="',IN_FILTER_SEGMENT,'"
    GROUP BY product_name
     HAVING (SUM(T12MthActive)) != 0
) AS result;');
end;
            END if;
        
        
        
        
        END;      
        
        END IF;

		END;
            end if;
            IF IN_AGGREGATION_1 = 'Bills' THEN
            begin

	

 if IN_FILTER_SEGMENT='' AND IN_FILTER_CURRENTMONTH<>'' THEN
        BEGIN
           
        	

			if NUM_PERCENT='PERCENT' THEN
            begin
			
SET @Query_String = CONCAT('WITH total AS (
    SELECT SUM(T12MthActiveBills) AS total
    FROM Dashboard_Insights_Base_ByProduct
    WHERE Aggregation = \'BY_MONTH\'
    AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
    AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
   
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", product_name, "num", ROUND((SUM(T12MthActiveBills) / total.total) * 100, 1)) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct, total
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
    GROUP BY product_name
     HAVING ROUND((SUM(T12MthActiveBills) / (SELECT total FROM total)) * 100, 1) != 0
) AS result;');
end;

			
            elseif NUM_PERCENT='NUMBER' Then
            begin
            			
        	   SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", product_name, "num", (SUM(T12MthActiveBills)) ) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
       
    GROUP BY product_name
    HAVING (SUM(T12MthActiveBills)) != 0
) AS result;');
end;
            END if;
        
        
        
        
        END;      
        
        END IF;
        
        
 if IN_FILTER_SEGMENT<>'' AND IN_FILTER_CURRENTMONTH<>'' THEN
        BEGIN
        set @q="1";
        	

						if NUM_PERCENT='PERCENT' THEN
            begin
			
        SET @Query_String = CONCAT('WITH total AS (
         SELECT SUM(subquery.total_sum) AS total
FROM (
    SELECT SUM(T12MthActiveBills) AS total_sum
    FROM Dashboard_Insights_Base_ByProduct
    WHERE Aggregation = \'BY_MONTH\'
    AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
    AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
     group by  CLMSegmentName="',IN_FILTER_SEGMENT,'"
     )as subquery
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", product_name, "num", ROUND((SUM(T12MthActiveBills) / total.total) * 100, 1)) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct, total
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
        and CLMSegmentName="',IN_FILTER_SEGMENT,'"
    GROUP BY product_name
     HAVING ROUND((SUM(T12MthActiveBills) / (SELECT total FROM total)) * 100, 1) != 0
) AS result;');
end;

			
            elseif NUM_PERCENT='NUMBER' Then
            begin
            			
        SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", product_name, "num", (SUM(T12MthActiveBills))) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
        and CLMSegmentName="',IN_FILTER_SEGMENT,'"
    GROUP BY product_name
     HAVING (SUM(T12MthActiveBills))!= 0
) AS result;');
end;
            END if;
        
        
        
        
        END;      
        
        END IF;

end;
            end if;
            
            IF IN_AGGREGATION_1 = 'Amount' THEN
         begin

 if IN_FILTER_SEGMENT='' AND IN_FILTER_CURRENTMONTH<>'' THEN
        BEGIN
           
        	

			if NUM_PERCENT='PERCENT' THEN
            begin
			
SET @Query_String = CONCAT('WITH total AS (
    SELECT SUM(T12MthActiveRevenue) AS total
    FROM Dashboard_Insights_Base_ByProduct
    WHERE Aggregation = \'BY_MONTH\'
    AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
    AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
   
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", product_name, "num", ROUND((SUM(T12MthActiveRevenue) / total.total) * 100, 1)) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct, total
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
    GROUP BY product_name
     HAVING ROUND((SUM(T12MthActiveRevenue) / (SELECT total FROM total)) * 100, 1) != 0
) AS result;');
end;

			
            elseif NUM_PERCENT='NUMBER' Then
            begin
            			
        	   SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", product_name, "num", (SUM(T12MthActiveRevenue)) ) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
       
    GROUP BY product_name
     HAVING (SUM(T12MthActiveRevenue))  != 0
) AS result;');
end;
            END if;
        
        
        
        
        END;      
        
        END IF;
        
        
  if IN_FILTER_SEGMENT<>'' AND IN_FILTER_CURRENTMONTH<>'' THEN
        BEGIN
        set @q="1";
        	

						if NUM_PERCENT='PERCENT' THEN
            begin
			
        SET @Query_String = CONCAT('WITH total AS (
         SELECT SUM(subquery.total_sum) AS total
FROM (
    SELECT SUM(T12MthActiveRevenue) AS total_sum
    FROM Dashboard_Insights_Base_ByProduct
    WHERE Aggregation = \'BY_MONTH\'
    AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
    AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
    group by CLMSegmentName="',IN_FILTER_SEGMENT,'"
    )as subquery
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", product_name, "num", ROUND((SUM(T12MthActiveRevenue) / total.total) * 100, 1)) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct, total
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
        and CLMSegmentName="',IN_FILTER_SEGMENT,'"
    GROUP BY product_name
     HAVING ROUND((SUM(T12MthActiveRevenue) / (SELECT total FROM total)) * 100, 1) != 0
) AS result;');
end;

			
            elseif NUM_PERCENT='NUMBER' Then
            begin
            			
        SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", product_name, "num", (SUM(T12MthActiveRevenue))) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
        and CLMSegmentName="',IN_FILTER_SEGMENT,'"
    GROUP BY product_name
     HAVING (SUM(T12MthActiveRevenue)) != 0
) AS result;');
end;
            END if;
        
        
        
        
        END;      
        
        END IF;

end;


            end if;
        

            
	end if;
    
    
       IF IN_MEASURE = 'Segment_View' THEN
       
    
        IF IN_AGGREGATION_1 = 'Base' THEN
        BEGIN

 if IN_FILTER_PRODUCT='' AND IN_FILTER_CURRENTMONTH<>'' THEN
        BEGIN
        
        
			if NUM_PERCENT='PERCENT' THEN
            begin
			
SET @Query_String = CONCAT('WITH total AS (
    SELECT SUM(T12MthActive) AS total
    FROM Dashboard_Insights_Base_ByProduct
    WHERE Aggregation = \'BY_MONTH\'
    AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
    AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
   
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", CLMSegmentName, "num", ROUND((SUM(T12MthActive) / (SELECT total FROM total)) * 100, 1)) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct, total
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
    GROUP BY CLMSegmentName
     HAVING ROUND((SUM(T12MthActive) / (SELECT total FROM total)) * 100, 1) != 0
) AS result;');
end;

			
            elseif NUM_PERCENT='NUMBER' Then
            begin
            			
        	   SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", CLMSegmentName, "num", (SUM(T12MthActive)) ) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
       
    GROUP BY CLMSegmentName
     HAVING  (SUM(T12MthActive)) != 0
) AS result;');
end;
            END if;
        
        
        
        END;      
        
        END IF;
        
        
 if IN_FILTER_PRODUCT<>'' AND IN_FILTER_CURRENTMONTH<>'' THEN
        BEGIN
        set @q="1";
      

					if NUM_PERCENT='PERCENT' THEN
            begin
			
SET @Query_String = CONCAT('WITH total AS (
 SELECT SUM(subquery.total_sum) AS total
FROM (
    SELECT SUM(T12MthActive) AS total_sum
    FROM Dashboard_Insights_Base_ByProduct
    WHERE Aggregation = \'BY_MONTH\'
    AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
    AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
   group by Product_Id="',IN_FILTER_PRODUCT,'") as subquery
   
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", CLMSegmentName, "num", ROUND((SUM(T12MthActive) /(SELECT total FROM total)) * 100, 1)) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct, total
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
        and Product_Id="',IN_FILTER_PRODUCT,'"
    GROUP BY CLMSegmentName
     HAVING ROUND((SUM(T12MthActive) / (SELECT total FROM total)) * 100, 1) != 0
) AS result;');
end;

			
            elseif NUM_PERCENT='NUMBER' Then
            begin
            			
        	   SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", CLMSegmentName, "num", (SUM(T12MthActive))) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
        and Product_Id="',IN_FILTER_PRODUCT,'"
       
    GROUP BY CLMSegmentName
     HAVING (SUM(T12MthActive)) != 0
) AS result;');
end;
            END if;
        

		END;
            end if;
            end;
            end if;
            IF IN_AGGREGATION_1 = 'Bills' THEN
            begin

 if IN_FILTER_PRODUCT='' AND IN_FILTER_CURRENTMONTH<>'' THEN
        BEGIN
        
        set @q="1";
        	
	if NUM_PERCENT='PERCENT' THEN
            begin
			
SET @Query_String = CONCAT('WITH total AS (
    SELECT SUM(T12MthActiveBills) AS total
    FROM Dashboard_Insights_Base_ByProduct
    WHERE Aggregation = \'BY_MONTH\'
    AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
    AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
   
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", CLMSegmentName, "num", ROUND((SUM(T12MthActiveBills) / (SELECT total FROM total)) * 100, 1)) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct, total
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
    GROUP BY CLMSegmentName
     HAVING ROUND((SUM(T12MthActiveBills) / (SELECT total FROM total)) * 100, 1) != 0
) AS result;');
end;

			
            elseif NUM_PERCENT='NUMBER' Then
            begin
            			
        	   SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", CLMSegmentName, "num", (SUM(T12MthActiveBills)) ) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
       
    GROUP BY CLMSegmentName
     HAVING (SUM(T12MthActiveBills)) != 0
) AS result;');
end;
            END if;
        
        
        
        END;      
        
        END IF;
        
        
        END;      
        
        END IF;
        
        
 if IN_FILTER_PRODUCT<>'' AND IN_FILTER_CURRENTMONTH<>'' THEN
        BEGIN
        set @q="1";

					if NUM_PERCENT='PERCENT' THEN
            begin
			
SET @Query_String = CONCAT('WITH total AS (
 SELECT SUM(subquery.total_sum) AS total
FROM (
    SELECT SUM(T12MthActiveBills) AS total_sum
    FROM Dashboard_Insights_Base_ByProduct
    WHERE Aggregation = \'BY_MONTH\'
    AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
    AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
     group by Product_Id="',IN_FILTER_PRODUCT,'") as subquery
   
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", CLMSegmentName, "num", ROUND((SUM(T12MthActiveBills) / (SELECT total FROM total)) * 100, 1)) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct, total
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
        and Product_Id="',IN_FILTER_PRODUCT,'"
    GROUP BY CLMSegmentName
     HAVING ROUND((SUM(T12MthActiveBills) / (SELECT total FROM total)) * 100, 1) != 0
) AS result;');
end;

			
            elseif NUM_PERCENT='NUMBER' Then
            begin
            			
        	   SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", CLMSegmentName, "num", (SUM(T12MthActiveBills))) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
        and Product_Id="',IN_FILTER_PRODUCT,'"
       
    GROUP BY CLMSegmentName
     HAVING (SUM(T12MthActiveBills)) != 0
) AS result;');
end;
            END if;

end;
            end if;
            IF IN_AGGREGATION_1 = 'Amount' THEN
         begin
 

 if IN_FILTER_PRODUCT='' AND IN_FILTER_CURRENTMONTH<>'' THEN
        BEGIN

					if NUM_PERCENT='PERCENT' THEN
            begin
			
SET @Query_String = CONCAT('WITH total AS (
    SELECT SUM(T12MthActiveRevenue) AS total
    FROM Dashboard_Insights_Base_ByProduct
    WHERE Aggregation = \'BY_MONTH\'
    AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
    AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
    
   
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", CLMSegmentName, "num", ROUND((SUM(T12MthActiveRevenue) / (SELECT total FROM total)) * 100, 1)) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct, total
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
       
    GROUP BY CLMSegmentName
     HAVING ROUND((SUM(T12MthActiveRevenue) / (SELECT total FROM total)) * 100, 1) != 0
) AS result;');
end;

			
            elseif NUM_PERCENT='NUMBER' Then
            begin
            			
        	   SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", CLMSegmentName, "num", (SUM(T12MthActiveRevenue))) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
       
       
    GROUP BY CLMSegmentName
     HAVING (SUM(T12MthActiveRevenue)) != 0
) AS result;');
end;
            END if;
        

		END;
            end if;
        
        
 if IN_FILTER_PRODUCT<>'' AND IN_FILTER_CURRENTMONTH<>'' THEN
        BEGIN
        set @q="1";
      

					if NUM_PERCENT='PERCENT' THEN
            begin
			
SET @Query_String = CONCAT('WITH total AS (
 SELECT SUM(subquery.total_sum) AS total
FROM (
    SELECT SUM(T12MthActiveRevenue) AS total_sum
    FROM Dashboard_Insights_Base_ByProduct
    WHERE Aggregation = \'BY_MONTH\'
    AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
    AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
     group by Product_Id="',IN_FILTER_PRODUCT,'")as subquery
   
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", CLMSegmentName, "num", ROUND((SUM(T12MthActiveRevenue) / (SELECT total FROM total)) * 100, 1)) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct, total
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
        and Product_Id="',IN_FILTER_PRODUCT,'"
    GROUP BY CLMSegmentName
     HAVING ROUND((SUM(T12MthActiveRevenue) / (SELECT total FROM total)) * 100, 1) != 0
) AS result;');
end;

			
            elseif NUM_PERCENT='NUMBER' Then
            begin
            			
        	   SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", CLMSegmentName, "num", (SUM(T12MthActiveRevenue))) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
        and Product_Id="',IN_FILTER_PRODUCT,'"
       
    GROUP BY CLMSegmentName
     HAVING (SUM(T12MthActiveRevenue)) != 0
) AS result;');
end;
            END if;
        

		END;
            end if;

end;


            end if;

            
	end if;
    
  end if;
  
  END IF;
  
  IF IN_FILTER_PRODUCT_TYPE = 'Favorite' OR IN_FILTER_PRODUCT_TYPE = 'FavMulti' THEN
     
         if IN_AGGREGATION_2="L6M" Then
   IF IN_MEASURE = 'Product_View' THEN
    
		IF IN_AGGREGATION_1 = 'Base' THEN
        BEGIN        
       
 if IN_FILTER_SEGMENT='' AND IN_FILTER_CURRENTMONTH<>'' THEN
        BEGIN
           
        	

			if NUM_PERCENT='PERCENT' THEN
            begin
			
SET @Query_String = CONCAT('WITH total AS (
    SELECT ifnull(SUM(TAPActive),1) AS total
    FROM Dashboard_Insights_Base_ByProduct
    WHERE Aggregation = \'BY_MONTH\'
    AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
    AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
   
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", product_name, "num", ROUND((  ifnull(SUM(TAPActive),0) / total.total) * 100, 1)) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct, total
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
    GROUP BY product_name
    HAVING ROUND((  ifnull(SUM(TAPActive),0) / (SELECT total FROM total)) * 100, 1) != 0
) AS result;');
end;

			
            elseif NUM_PERCENT='NUMBER' Then
            begin
            			
        	   SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", product_name, "num", ifnull(SUM(TAPActive),0) ) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
       
    GROUP BY product_name
    HAVING (SUM(TAPActive)) != 0
) AS result;');
end;
            END if;
        
        
        
        
        END;      
        
        END IF;
        
        
 if IN_FILTER_SEGMENT<>'' AND IN_FILTER_CURRENTMONTH<>'' THEN
        BEGIN
        set @q="1";
        	

						if NUM_PERCENT='PERCENT' THEN
            begin
			
        SET @Query_String = CONCAT('WITH total AS (
         SELECT SUM(subquery.total_sum) AS total
FROM (
    SELECT ifnull(SUM(TAPActive),1) AS total_sum
    FROM Dashboard_Insights_Base_ByProduct
    WHERE Aggregation = \'BY_MONTH\'
    AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
    AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
     GROUP BY CLMSegmentName="',IN_FILTER_SEGMENT,'")as subquery
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", product_name, "num", ROUND((  ifnull(SUM(TAPActive),0) / total.total) * 100, 1)) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct, total
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
        and CLMSegmentName="',IN_FILTER_SEGMENT,'"
    GROUP BY product_name
     HAVING ROUND((  ifnull(SUM(TAPActive),0) / (SELECT total FROM total)) * 100, 1) != 0
) AS result;');
end;

			
            elseif NUM_PERCENT='NUMBER' Then
            begin
            			
        SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", product_name, "num", ifnull((SUM(TAPActive)),0)  ) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
        and CLMSegmentName="',IN_FILTER_SEGMENT,'"
    GROUP BY product_name
     HAVING ifnull((SUM(TAPActive)),0) != 0
) AS result;');
end;
            END if;
        
        
        
        
        END;      
        
        END IF;

		END;
            end if;
            IF IN_AGGREGATION_1 = 'Bills' THEN
            begin

	

 if IN_FILTER_SEGMENT='' AND IN_FILTER_CURRENTMONTH<>'' THEN
        BEGIN
           
        	

			if NUM_PERCENT='PERCENT' THEN
            begin
			
SET @Query_String = CONCAT('WITH total AS (

    SELECT ifnull(SUM(TAPActiveBills),1) AS total
    FROM Dashboard_Insights_Base_ByProduct
    WHERE Aggregation = \'BY_MONTH\'
    AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
    AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
   
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", product_name, "num", ROUND((ifnull(SUM(TAPActiveBills),0) / total.total) * 100, 1)) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct, total
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
    GROUP BY product_name
    HAVING ROUND((ifnull(SUM(TAPActiveBills),0) / (SELECT total FROM total)) * 100, 1)!= 0
) AS result;');
end;

			
            elseif NUM_PERCENT='NUMBER' Then
            begin
            			
        	   SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", product_name, "num", ifnull((SUM(TAPActiveBills)),0) ) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
       
    GROUP BY product_name
    HAVING ifnull((SUM(TAPActiveBills)),0)!= 0
) AS result;');
end;
            END if;
        
        
        
        
        END;      
        
        END IF;
        
        
 if IN_FILTER_SEGMENT<>'' AND IN_FILTER_CURRENTMONTH<>'' THEN
        BEGIN
        set @q="1";
        	

						if NUM_PERCENT='PERCENT' THEN
            begin
			
        SET @Query_String = CONCAT('WITH total AS (
           SELECT SUM(subquery.total_sum) AS total
FROM (
    SELECT ifnull(SUM(TAPActiveBills),1) AS total_sum
    FROM Dashboard_Insights_Base_ByProduct
    WHERE Aggregation = \'BY_MONTH\'
    AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
    AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
     GROUP BY CLMSegmentName="',IN_FILTER_SEGMENT,'")as subquery
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", product_name, "num", ROUND((ifnull(SUM(TAPActiveBills),0) / total.total) * 100, 1)) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct, total
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
        and CLMSegmentName="',IN_FILTER_SEGMENT,'"
    GROUP BY product_name
    HAVING ROUND((ifnull(SUM(TAPActiveBills),0) / (SELECT total FROM total)) * 100, 1)!= 0
) AS result;');
end;

			
            elseif NUM_PERCENT='NUMBER' Then
            begin
            			
        SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", product_name, "num", ifnull((SUM(TAPActiveBills)),0)  ) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
        and CLMSegmentName="',IN_FILTER_SEGMENT,'"
    GROUP BY product_name
    HAVING ifnull((SUM(TAPActiveBills)),0) != 0
) AS result;');
end;
            END if;
        
        
        
        
        END;      
        
        END IF;

end;
            end if;
            
            IF IN_AGGREGATION_1 = 'Amount' THEN
         begin

 if IN_FILTER_SEGMENT='' AND IN_FILTER_CURRENTMONTH<>'' THEN
        BEGIN
           
        	

			if NUM_PERCENT='PERCENT' THEN
            begin
			
SET @Query_String = CONCAT('WITH total AS (
    SELECT ifnull(SUM(TAPActiveRevenue),1) AS total
    FROM Dashboard_Insights_Base_ByProduct
    WHERE Aggregation = \'BY_MONTH\'
    AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
    AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
   
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", product_name, "num", ROUND(( ifnull(SUM(TAPActiveRevenue),0) / total.total) * 100, 1)) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct, total
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
    GROUP BY product_name
     HAVING  ROUND(( ifnull(SUM(TAPActiveRevenue),0) / (SELECT total FROM total)) * 100, 1) != 0
) AS result;');
end;

			
            elseif NUM_PERCENT='NUMBER' Then
            begin
            			
        	   SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", product_name, "num", ifnull((SUM(TAPActiveRevenue)),0) ) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
       
    GROUP BY product_name
     HAVING  ifnull((SUM(TAPActiveRevenue)),0) != 0
    
) AS result;');
end;
            END if;
        
        
        
        
        END;      
        
        END IF;
        
        
  if IN_FILTER_SEGMENT<>'' AND IN_FILTER_CURRENTMONTH<>'' THEN
        BEGIN
        set @q="1";
        	

						if NUM_PERCENT='PERCENT' THEN
            begin
			
        SET @Query_String = CONCAT('WITH total AS (
           SELECT SUM(subquery.total_sum) AS total
FROM (
    SELECT ifnull(SUM(TAPActiveRevenue),1) AS total_sum
    FROM Dashboard_Insights_Base_ByProduct
    WHERE Aggregation = \'BY_MONTH\'
    AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
    AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
    GROUP BY CLMSegmentName="',IN_FILTER_SEGMENT,'")as subquery
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", product_name, "num", ROUND(( ifnull(SUM(TAPActiveRevenue),0) / total.total) * 100, 1)) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct, total
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
        and CLMSegmentName="',IN_FILTER_SEGMENT,'"
    GROUP BY product_name
     HAVING  ROUND(( ifnull(SUM(TAPActiveRevenue),0) / (SELECT total FROM total)) * 100, 1)!= 0
) AS result;');
end;

			
            elseif NUM_PERCENT='NUMBER' Then
            begin
            			
        SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", product_name, "num", ifnull((SUM(TAPActiveRevenue)),0)  ) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
        and CLMSegmentName="',IN_FILTER_SEGMENT,'"
    GROUP BY product_name
      HAVING  ifnull((SUM(TAPActiveRevenue)),0) != 0
) AS result;');
end;
            END if;
        
        
        
        
        END;      
        
        END IF;

end;


            end if;
        

            
	end if;
    
    
       IF IN_MEASURE = 'Segment_View' THEN
       
    
        IF IN_AGGREGATION_1 = 'Base' THEN
        BEGIN

 if IN_FILTER_PRODUCT='' AND IN_FILTER_CURRENTMONTH<>'' THEN
        BEGIN
        
        
			if NUM_PERCENT='PERCENT' THEN
            begin
			
SET @Query_String = CONCAT('WITH total AS (
    SELECT ifnull(SUM(TAPActive),1) AS total
    FROM Dashboard_Insights_Base_ByProduct
    WHERE Aggregation = \'BY_MONTH\'
    AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
    AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
   
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
     SELECT JSON_OBJECT("over", CLMSegmentName, "num", ROUND((IFNULL(SUM(TAPActive), 0) / total.total) * 100, 1)) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct, total
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
    GROUP BY CLMSegmentName
     HAVING  ROUND((IFNULL(SUM(TAPActive), 0) / (SELECT total FROM total)) * 100, 1) != 0
) AS result;');
end;

			
            elseif NUM_PERCENT='NUMBER' Then
            begin
            			
        	   SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", CLMSegmentName, "num", ifnull(SUM(TAPActive),0) ) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
       
    GROUP BY CLMSegmentName
     HAVING ifnull(SUM(TAPActive),0)!= 0
) AS result;');
end;
            END if;
        
        
        
        END;      
        
        END IF;
        
        
 if IN_FILTER_PRODUCT<>'' AND IN_FILTER_CURRENTMONTH<>'' THEN
        BEGIN
        set @q="1";
      

					if NUM_PERCENT='PERCENT' THEN
            begin
			
SET @Query_String = CONCAT('WITH total AS (
   SELECT SUM(subquery.total_sum) AS total
FROM (
    SELECT ifnull(SUM(TAPActive),1) AS total_sum
    FROM Dashboard_Insights_Base_ByProduct
    WHERE Aggregation = \'BY_MONTH\'
    AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
    AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
    group by Product_Id="',IN_FILTER_PRODUCT,'")as subquery
   
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
     SELECT JSON_OBJECT("over", CLMSegmentName, "num", ROUND((IFNULL(SUM(TAPActive), 0) / total.total) * 100, 1)) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct, total
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
        and Product_Id="',IN_FILTER_PRODUCT,'"
    GROUP BY CLMSegmentName
    HAVING  ROUND(( ifnull(SUM(TAPActive),0) / (SELECT total FROM total)) * 100, 1) != 0
) AS result;');
end;

			
            elseif NUM_PERCENT='NUMBER' Then
            begin
            			
        	   SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", CLMSegmentName, "num", ifnull(SUM(TAPActive),0)  ) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
        and Product_Id="',IN_FILTER_PRODUCT,'"
       
    GROUP BY CLMSegmentName
      HAVING  ifnull(SUM(TAPActive),0)!= 0
    
) AS result;');
end;
            END if;
        

		END;
            end if;
            end;
            end if;
            IF IN_AGGREGATION_1 = 'Bills' THEN
            begin

 if IN_FILTER_PRODUCT='' AND IN_FILTER_CURRENTMONTH<>'' THEN
        BEGIN
        
        set @q="1";
        	
	if NUM_PERCENT='PERCENT' THEN
            begin
			
SET @Query_String = CONCAT('WITH total AS (
    SELECT ifnull(SUM(TAPActiveBills),1) AS total
    FROM Dashboard_Insights_Base_ByProduct
    WHERE Aggregation = \'BY_MONTH\'
    AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
    AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
   
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
     SELECT JSON_OBJECT("over", CLMSegmentName, "num", ROUND((IFNULL(SUM(TAPActiveBills), 0) / total.total) * 100, 1)) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct, total
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
    GROUP BY CLMSegmentName
       HAVING  ROUND((IFNULL(SUM(TAPActiveBills), 0) / (SELECT total FROM total)) * 100, 1)!= 0
) AS result;');
end;

			
            elseif NUM_PERCENT='NUMBER' Then
            begin
            			
        	   SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", CLMSegmentName, "num", ifnull(SUM(TAPActiveBills),0) ) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
       
    GROUP BY CLMSegmentName
    HAVING  ifnull(SUM(TAPActiveBills),0)!= 0
) AS result;');
end;
            END if;
        
        
        
        END;      
        
        END IF;
        
        
        END;      
        
        END IF;
        
        
 if IN_FILTER_PRODUCT<>'' AND IN_FILTER_CURRENTMONTH<>'' THEN
        BEGIN
        set @q="1";

					if NUM_PERCENT='PERCENT' THEN
            begin
			
SET @Query_String = CONCAT('WITH total AS (
   SELECT SUM(subquery.total_sum) AS total
FROM (
    SELECT ifnull(SUM(TAPActiveBills),1) AS total_sum
    FROM Dashboard_Insights_Base_ByProduct
    WHERE Aggregation = \'BY_MONTH\'
    AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
    AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
     group by Product_Id="',IN_FILTER_PRODUCT,'")as subquery
   
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
     SELECT JSON_OBJECT("over", CLMSegmentName, "num", ROUND((IFNULL(SUM(TAPActiveBills), 0) / total.total) * 100, 1)) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct, total
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
        and Product_Id="',IN_FILTER_PRODUCT,'"
    GROUP BY CLMSegmentName
    HAVING  ROUND((IFNULL(SUM(TAPActiveBills), 0) / (SELECT total FROM total)) * 100, 1)!= 0
) AS result;');
end;

			
            elseif NUM_PERCENT='NUMBER' Then
            begin
            			
        	   SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", CLMSegmentName, "num", ifnull(SUM(TAPActiveBills),0)  ) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
        and Product_Id="',IN_FILTER_PRODUCT,'"
       
    GROUP BY CLMSegmentName
    HAVING  ifnull(SUM(TAPActiveBills),0)!= 0
) AS result;');
end;
            END if;

end;
            end if;
            IF IN_AGGREGATION_1 = 'Amount' THEN
         begin
 

 if IN_FILTER_PRODUCT='' AND IN_FILTER_CURRENTMONTH<>'' THEN
        BEGIN

					if NUM_PERCENT='PERCENT' THEN
            begin
			
SET @Query_String = CONCAT('WITH total AS (
    SELECT ifnull(SUM(TAPActiveRevenue),0) AS total
    FROM Dashboard_Insights_Base_ByProduct
    WHERE Aggregation = \'BY_MONTH\'
    AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
    AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
    
   
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
     SELECT JSON_OBJECT("over", CLMSegmentName, "num", ROUND((IFNULL(SUM(TAPActiveRevenue), 0) / total.total) * 100, 1)) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct, total
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
       
    GROUP BY CLMSegmentName
    HAVING  ROUND((IFNULL(SUM(TAPActiveRevenue), 0) / (SELECT total FROM total)) * 100, 1)!= 0
) AS result;');
end;

			
            elseif NUM_PERCENT='NUMBER' Then
            begin
            			
        	   SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", CLMSegmentName, "num", ifnull(SUM(TAPActiveRevenue),0)  ) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
       
       
    GROUP BY CLMSegmentName
    HAVING  ifnull(SUM(TAPActiveRevenue),0) != 0
) AS result;');
end;
            END if;
        

		END;
            end if;
        
        
 if IN_FILTER_PRODUCT<>'' AND IN_FILTER_CURRENTMONTH<>'' THEN
        BEGIN
        set @q="1";
      

					if NUM_PERCENT='PERCENT' THEN
            begin
			
SET @Query_String = CONCAT('WITH total AS (
   SELECT SUM(subquery.total_sum) AS total
FROM (
    SELECT ifnull(SUM(TAPActiveRevenue),1) AS total_sum
    FROM Dashboard_Insights_Base_ByProduct
    WHERE Aggregation = \'BY_MONTH\'
    AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
    AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
     group by Product_Id="',IN_FILTER_PRODUCT,'")as subquery
   
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
     SELECT JSON_OBJECT("over", CLMSegmentName, "num", ROUND((IFNULL(SUM(TAPActiveRevenue), 0) / total.total) * 100, 1)) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct, total
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
        and Product_Id="',IN_FILTER_PRODUCT,'"
    GROUP BY CLMSegmentName
      HAVING  ROUND((IFNULL(SUM(TAPActiveRevenue), 0) / (SELECT total FROM total)) * 100, 1) != 0
) AS result;');
end;

			
            elseif NUM_PERCENT='NUMBER' Then
            begin
            			
        	   SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
    SELECT JSON_OBJECT("over", CLMSegmentName, "num", ifnull(SUM(TAPActiveRevenue),0)  ) AS json_obj
    FROM Dashboard_Insights_Base_ByProduct
    WHERE
        Aggregation = \'BY_MONTH\'
        AND Product_Type ="',IN_FILTER_PRODUCT_TYPE,'"
        AND YYYYMM =', IN_FILTER_CURRENTMONTH,'
        and Product_Id="',IN_FILTER_PRODUCT,'"
       
    GROUP BY CLMSegmentName
    HAVING ifnull(SUM(TAPActiveRevenue),0) != 0
) AS result;');
end;
            END if;
        

		END;
            end if;

end;


            end if;

            
	end if;
    
  end if;
	
  END IF;
  
  IF IN_MEASURE = 'DROPDOWN' THEN
    IF IN_AGGREGATION_1 = 'SEGMENT_DROPDOWN' THEN
        BEGIN
            SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_row SEPARATOR \',\'), \']\') INTO @temp_result1
FROM (
  SELECT CONCAT(\'{"Segment": "\', CLMSegmentName, \'"}\') AS json_row
  FROM CLM_Segment
  where CLMSegmentName <>"Leads"
) AS subquery');
        END;
    END IF;
    
    IF IN_AGGREGATION_1 = 'PRODUCT_DROPDOWN' THEN
        BEGIN
           SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(
  JSON_OBJECT(
        \'product_id\', product_id,
    \'product_name\', product_name
  )
  SEPARATOR \',\'), \']\') into @temp_result1
FROM (
  SELECT DISTINCT(product_id),product_name
  FROM PLUM_SOLUS_PROD.Dashboard_Insights_Base_ByProduct
  WHERE AGGREGATION = \'BY_MONTH\'
  GROUP BY product_id
) AS subquery');

        END;
    END IF;
END IF;


IF IN_SCREEN = 'PRODUCT_VIEW' THEN
    PREPARE stmt FROM @Query_String;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
    IF @temp_result1 = null or @temp_result1 = '' THEN
       set @temp_result1 = '[]';
    END IF;
    SET out_result_json = @temp_result1;
END IF;



END$$
DELIMITER ;
