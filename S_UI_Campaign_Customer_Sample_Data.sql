DELIMITER $$
CREATE or replace PROCEDURE `S_UI_Campaign_Customer_Sample_Data`()
begin
		
    DECLARE done1,done2,done3,done4 INT DEFAULT FALSE;
	
    DECLARE Variable_Name,Exe_ID TEXT;
   
    DECLARE vSuccess int (4);                      
	DECLARE vErrMsg Text ; 
    Declare cur1 cursor for SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Customer_One_View';
	Declare cur2 cursor for SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Customer_Details_View';
	Declare cur3 cursor for SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'V_CLM_Bill_Detail_One_View';
	
	SET done1 = FALSE;
	SET done2 = FALSE;
	SET done3 = FALSE;
	SET done4 = FALSE;
    SET SQL_SAFE_UPDATES=0;
    set foreign_key_checks=0;
SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Customer_One_View';
	open cur1;
				BEgin
						
						new_loop_1: LOOP
								FETCH cur1 into Variable_Name;
								IF done1 THEN
										 LEAVE new_loop_1;
								END IF;

								

								SELECT Variable_Name;
								
							
									 set @sqlCust = concat( 'Update UI_SVOC_SVOT_Variables_Sample_Data 
									 Set Vairable_Sample_Data =(Select ',Variable_Name,' from Customer_One_View where Customer_Id >0 limit 1)
									 where Variable_Name="',Variable_Name,'" and Vairable_Sample_Data is null
									 ;');

														
									SELECT @sqlCust;
									prepare stmt1 from @sqlCust;
									execute stmt1;
									deallocate prepare stmt1;
									
									
									 
							
						end LOOP;
					  
				End;
	close cur1;
	
	SET done1 = FALSE;
	
open cur2;
				BEgin
						
						new_loop_1: LOOP
								FETCH cur2 into Variable_Name;
								IF done1 THEN
										 LEAVE new_loop_1;
								END IF;

								

								SELECT Variable_Name;
								
							
									 
									  set @sqlCust = concat( 'Update UI_SVOC_SVOT_Variables_Sample_Data 
									 Set Vairable_Sample_Data =(Select ',Variable_Name,' from Customer_Details_View where Customer_Id >0 limit 1)
									 where Variable_Name="',Variable_Name,'"  and Vairable_Sample_Data is null
									 ;');
														
									SELECT @sqlCust;
									prepare stmt1 from @sqlCust;
									execute stmt1;
									deallocate prepare stmt1;
									
									
									 
							
						end LOOP;
					  
				End;
	close cur2;

	
	SET done1 = FALSE;
	
open cur3;
				BEgin
						
						new_loop_1: LOOP
								FETCH cur3 into Variable_Name;
								IF done1 THEN
										 LEAVE new_loop_1;
								END IF;

								

								SELECT Variable_Name;
								
							
									

									  set @sqlCust = concat( 'Update UI_SVOC_SVOT_Variables_Sample_Data 
									 Set Vairable_Sample_Data =(Select ',Variable_Name,' from V_CLM_Bill_Detail_One_View where Customer_Id >0 limit 1)
									 where Variable_Name="',Variable_Name,'"  and Vairable_Sample_Data is null
									 ;');
											
														
									SELECT @sqlCust;
									prepare stmt1 from @sqlCust;
									execute stmt1;
									deallocate prepare stmt1;
									
									
									 
							
						end LOOP;
					  
				End;
	close cur3;
	
END$$
DELIMITER ;