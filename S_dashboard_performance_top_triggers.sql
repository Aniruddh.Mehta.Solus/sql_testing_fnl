DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_performance_top_triggers`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
    declare IN_AGGREGATION_4 text;
    declare IN_AGGREGATION_5 text;
    declare IN_AGGREGATION_6 text;
    declare IN_AGGREGATION_7 text;
    declare IN_AGGREGATION_8 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
    SET IN_AGGREGATION_4 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_4"));
    SET IN_AGGREGATION_5 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_5"));
    SET IN_AGGREGATION_6 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_6"));
    SET IN_AGGREGATION_7 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_7"));
    SET IN_AGGREGATION_8 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_8"));
    
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    
    SET @Interval_month = CASE WHEN IN_AGGREGATION_3='M' THEN 0
							WHEN IN_AGGREGATION_3='L3M' THEN 2
                            WHEN IN_AGGREGATION_3='L12M' THEN 11 
                            ELSE 0 END;
    
	SET @FY_SATRT_DATE = date_format(date_sub(concat(IN_FILTER_CURRENTMONTH,'01'), INTERVAL @Interval_month MONTH),"%Y%m");
	SET @FY_END_DATE = IN_FILTER_CURRENTMONTH;
    
    SELECT @Interval_month;
    
    SELECT @FY_SATRT_DATE,@FY_END_DATE;
    SET @filter_cond=concat(" YM between '",@FY_SATRT_DATE,"' and '",@FY_END_DATE,"'");
    

select Value1 into @Incremental_Revenue_Method from UI_Configuration_Global where Config_Name = 'Incremental_Revenue_Method';
    SET @Incremental_Revenue_Method=ifnull(@Incremental_Revenue_Method,'Method2');
    select @Incremental_Revenue_Method;
    IF @Incremental_Revenue_Method = 'Method1'
    THEN
		SET @incr_rev_concat='_By_Day';
        SET @group_cond1='EE_Date';
        SET @mtd_concat='';
	ELSEIF @Incremental_Revenue_Method = 'Method2'
    THEN
		SET @group_cond1='YM';
		SET @incr_rev_concat='';
        SET @mtd_concat='MTD_';
	ELSE
		SET @incr_rev_concat='';
        SET @mtd_concat='';
		SET @incr_Rev = ' round((CASE WHEN SUM(Control_Base) < 0 or SUM(Target_Delivered) = 0 then 0 WHEN (((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*100 < 0 then 0 WHEN SUM(Control_Base)>0 and SUM(Control_Responder)=0 then SUM(Target_Revenue)
					ELSE ifnull((((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*(SUM(Target_Revenue)/SUM(Target_Responder))*(SUM(Target_Delivered)),0) END),0) ';
		SET @group_cond1='YM';
	END IF;
    SELECT @mtd_concat;
    
    IF IN_AGGREGATION_3 = 'ST'
    THEN
    SET @sql1=CONCAT('
		CREATE OR REPLACE VIEW CLM_Event_Dashboard_STLT
        as 
		SELECT `CLM_Event_Dashboard`.`Revenue Center`,
			`CLM_Event_Dashboard`.`Segment`,
			`CLM_Event_Dashboard`.`Campaign`,
			`CLM_Event_Dashboard`.`EE_Date`,
			`CLM_Event_Dashboard`.`Event_Execution_Date_ID`,
			`CLM_Event_Dashboard`.`Theme`,
			`CLM_Event_Dashboard`.`Offer`,
			`CLM_Event_Dashboard`.`Channel`,
			`CLM_Event_Dashboard`.`Has_Recommendations`,
			`CLM_Event_Dashboard`.`Event_ID`,
			`CLM_Event_Dashboard`.`Control_Base_Event` as `Control_Base`,
			`CLM_Event_Dashboard`.`Target_Base`,
			`CLM_Event_Dashboard`.`Target_Delivered`,
			`CLM_Event_Dashboard`.`Target_Delivered_NA`,
			`CLM_Event_Dashboard`.`Control_Responder_Event` as `Control_Responder`,
			`CLM_Event_Dashboard`.`Target_Responder`,
			`CLM_Event_Dashboard`.`Target_Revenue`,
			`CLM_Event_Dashboard`.`Target_Discount`,
			`CLM_Event_Dashboard`.`Target_Bills`,
			`CLM_Event_Dashboard`.`Target_Open`,
			`CLM_Event_Dashboard`.`Target_CTA`,
			`CLM_Event_Dashboard`.`Control_Revenue`,
			`CLM_Event_Dashboard`.`Control_Discount`,
			`CLM_Event_Dashboard`.`Control_Bills`,
			`CLM_Event_Dashboard`.`Created_Date`,
			`CLM_Event_Dashboard`.`Total_Customer_Base`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Day`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Month`,
			`CLM_Event_Dashboard`.`YM`,
			`CLM_Event_Dashboard`.`Incremental_Revenue_ST',@incr_rev_concat,'` AS `Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_ST',@incr_rev_concat,'` AS `MTD_Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Segment_ST',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Segment`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Revenue_Center_ST',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Revenue_Center`
		FROM `CLM_Event_Dashboard`;
        ');
        select @sql1;
		PREPARE statement from @sql1;
		Execute statement;
		Deallocate PREPARE statement;
    ELSE
		SET @sql1=CONCAT('
		CREATE OR REPLACE VIEW CLM_Event_Dashboard_STLT
        as 
		SELECT `CLM_Event_Dashboard`.`Revenue Center`,
			`CLM_Event_Dashboard`.`Segment`,
			`CLM_Event_Dashboard`.`Campaign`,
			`CLM_Event_Dashboard`.`EE_Date`,
			`CLM_Event_Dashboard`.`Event_Execution_Date_ID`,
			`CLM_Event_Dashboard`.`Theme`,
			`CLM_Event_Dashboard`.`Offer`,
			`CLM_Event_Dashboard`.`Channel`,
			`CLM_Event_Dashboard`.`Has_Recommendations`,
			`CLM_Event_Dashboard`.`Event_ID`,
			`CLM_Event_Dashboard`.`Control_Base` as `Control_Base`,
			`CLM_Event_Dashboard`.`Target_Base`,
			`CLM_Event_Dashboard`.`Target_Delivered`,
			`CLM_Event_Dashboard`.`Target_Delivered_NA`,
			`CLM_Event_Dashboard`.`Control_Responder` as `Control_Responder`,
			`CLM_Event_Dashboard`.`Target_Responder`,
			`CLM_Event_Dashboard`.`Target_Revenue`,
			`CLM_Event_Dashboard`.`Target_Discount`,
			`CLM_Event_Dashboard`.`Target_Bills`,
			`CLM_Event_Dashboard`.`Target_Open`,
			`CLM_Event_Dashboard`.`Target_CTA`,
			`CLM_Event_Dashboard`.`Control_Revenue`,
			`CLM_Event_Dashboard`.`Control_Discount`,
			`CLM_Event_Dashboard`.`Control_Bills`,
			`CLM_Event_Dashboard`.`Created_Date`,
			`CLM_Event_Dashboard`.`Total_Customer_Base`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Day`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Month`,
			`CLM_Event_Dashboard`.`YM`,
			`CLM_Event_Dashboard`.`Incremental_Revenue',@incr_rev_concat,'` AS `Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue',@incr_rev_concat,'` AS `MTD_Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Segment',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Segment`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Revenue_Center',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Revenue_Center`
		FROM `CLM_Event_Dashboard`;
        ');
        select @sql1;
		PREPARE statement from @sql1;
		Execute statement;
		Deallocate PREPARE statement;
    END IF;
     SET @wherecond1 = ' 1=1 ';
	SET @wherecond3 = ' 1=1 ';
    
    IF IN_AGGREGATION_6 = 'ALLCHANNEL'
    THEN 
    SET IN_AGGREGATION_6 = '';
    END IF;
	IF IN_AGGREGATION_2 = 'ALL'
    THEN 
    SET IN_AGGREGATION_2 = '';
    END IF;
    

CREATE OR REPLACE VIEW V_Dashboard_Triggers_Report AS
    select Type_Of_Event AS `Category`,b.Name as `Trigger`, a.* from CLM_Event_Dashboard_STLT a, Event_Master b, Event_Master_UI c where a.Event_ID = b.ID AND b.ID = c.Event_Id;
   
	IF IN_AGGREGATION_1 = 'TOPBYYIELD'
   THEN 
        IF IN_AGGREGATION_2 = '' or IN_AGGREGATION_2 = 'ALL'
        THEN
        SET @wherecond1 = ' 1=1 ';
        ELSE
        SET @wherecond1 = CONCAT(' Category="',IN_AGGREGATION_2,'" ');
        END IF;
        IF IN_AGGREGATION_6 = ''
        THEN
        SET @wherecond3 = ' 1=1 ';
        ELSE
        SET @wherecond3 = CONCAT(' Channel="',IN_AGGREGATION_6,'" ');
        END IF;
   END IF;
   
   IF IN_AGGREGATION_1 = 'TOPBYCONVERSION'
   THEN 
        IF IN_AGGREGATION_2 = '' or IN_AGGREGATION_2 = 'ALL'
        THEN
        SET @wherecond1 = ' 1=1 ';
        ELSE
        SET @wherecond1 = CONCAT(' Category="',IN_AGGREGATION_2,'" ');
        END IF;
        IF IN_AGGREGATION_6 = ''
        THEN
        SET @wherecond3 = ' 1=1 ';
        ELSE
        SET @wherecond3 = CONCAT(' Channel="',IN_AGGREGATION_6,'" ');
        END IF;
   END IF;
   
   IF IN_AGGREGATION_1 = 'TOPBYLIFT'
   THEN 
        IF IN_AGGREGATION_2 = '' or IN_AGGREGATION_2 = 'ALL'
        THEN
        SET @wherecond1 = ' 1=1 ';
        ELSE
        SET @wherecond1 = CONCAT(' Category="',IN_AGGREGATION_2,'" ');
        END IF;
        IF IN_AGGREGATION_6 = ''
        THEN
        SET @wherecond3 = ' 1=1 ';
        ELSE
        SET @wherecond3 = CONCAT(' Channel="',IN_AGGREGATION_6,'" ');
        END IF;
   END IF;
   
       IF IN_AGGREGATION_1 = 'TOPBYYIELD'
	THEN 
    
        set @view_stmt= CONCAT(' create or replace view V_Dashboard_Triggers as 
							select 
                            Event_Id,
                            `Trigger` as "Trigger",
                            Category,
                            Channel,
							ROUND(SUM(Incremental_Revenue)/SUM(Target_Delivered),1) as "Yield",
                            ROUND(SUM(Target_Bills)*100/SUM(Target_Base),1) AS Conversion_Percent,
                            ROUND(SUM(Incremental_Revenue),0) as "Incr_Rev"
                            FROM V_Dashboard_Triggers_Report
                            WHERE ',@wherecond1,' and ',@filter_cond,' AND ',@wherecond3,'
                            GROUP BY 1
                            ORDER BY Yield DESC
                            LIMIT ',IN_AGGREGATION_5,'
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Event_Id",`Event_Id`,"Trigger",`Trigger`,"Type",`Category`,"Channel",`Channel`,"Yield",`Yield`,"% Conversion",`Conversion_Percent`,"Incr. Rev",CAST(format(ifnull(`Incr_Rev`,0),",") AS CHAR)) )),"]")into @temp_result from V_Dashboard_Triggers;' ;

END IF;
        
	IF IN_AGGREGATION_1 = 'TOPBYCONVERSION'
	THEN 
    
        set @view_stmt= CONCAT(' create or replace view V_Dashboard_Triggers as 
							select 
                            Event_Id,
                            `Trigger` as "Trigger",
                            Category,
                            Channel,
							ROUND(SUM(Incremental_Revenue)/SUM(Target_Delivered),1) as "Yield",
                            ROUND(SUM(Target_Bills)*100/SUM(Target_Base),1) AS Conversion_Percent,
                            ROUND(SUM(Incremental_Revenue),0) as "Incr_Rev"
                            FROM V_Dashboard_Triggers_Report
                            WHERE ',@wherecond1,' and ',@filter_cond,' AND ',@wherecond3,'
                            GROUP BY 1
                            ORDER BY Conversion_Percent DESC
                            LIMIT ',IN_AGGREGATION_5,'
							');
     
		 SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Event_Id",`Event_Id`,"Trigger",`Trigger`,"Type",`Category`,"Channel",`Channel`,"Yield",`Yield`,"% Conversion",`Conversion_Percent`,"Incr. Rev",CAST(format(ifnull(`Incr_Rev`,0),",") AS CHAR)) )),"]")into @temp_result from V_Dashboard_Triggers;' ;

END IF;
        
	IF IN_AGGREGATION_1 = 'TOPBYLIFT'
	THEN 
    
        set @view_stmt= CONCAT(' create or replace view V_Dashboard_Triggers as 
							select 
                            Event_Id,
                            `Trigger` as "Trigger",
                            Category,
                            Channel,
							ROUND(SUM(Incremental_Revenue)/SUM(Target_Delivered),1) as "Yield",
                            ROUND(SUM(Target_Bills)*100/SUM(Target_Base),1) AS Conversion_Percent,
                            ROUND(SUM(Incremental_Revenue),0) as "Incr_Rev"
                            FROM V_Dashboard_Triggers_Report
                            WHERE ',@wherecond1,' and ',@filter_cond,' AND ',@wherecond3,'
                            GROUP BY 1
                            ORDER BY Incr_Rev DESC
                            LIMIT ',IN_AGGREGATION_5,'
							');
     
		 SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Event_Id",`Event_Id`,"Trigger",`Trigger`,"Type",`Category`,"Channel",`Channel`,"Yield",`Yield`,"% Conversion",`Conversion_Percent`,"Incr. Rev",CAST(format(ifnull(`Incr_Rev`,0),",") AS CHAR)) )),"]")into @temp_result from V_Dashboard_Triggers;' ;

END IF;

	IF IN_AGGREGATION_1 = 'POPUP' AND IN_AGGREGATION_2 <> ''
	THEN 
    
        set @view_stmt= CONCAT(' create or replace view V_Dashboard_Triggers_popup as 
							 (Select A.Name,ChannelName,Coverage,Template,A.Communication_Cooloff
 ,Waterfall,Event_Response_Days,CampaignThemeName,CampaignName,Type_Of_Event,Header,Message,
 Case when Replaced_Query is null or Replaced_Query="" then "NA" else Replaced_Query end as Replaced_Query,
 Case when Replaced_Query_Bill is null or Replaced_Query_Bill="" then "NA" else Replaced_Query_Bill end as Replaced_Query_Bill
 ,Event_Limit,
  Case when Status_Schedule!="Schedule" then "Not Scheduled" else  Scheduled_at end as Scheduled_at
from    Event_Master A,Communication_Template B,Event_Master_UI C 
 where A.Id=C.Event_Id and  A.CreativeId1=B.id  and A.Id=',IN_AGGREGATION_2,'   
  )
							');
     
		 SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Channel",`ChannelName`,"Name",`Name`,"Target_Audience",`Coverage`,"Template",`Template`,"Cool_Off",`Communication_Cooloff`,"Priority",`Waterfall`,"Response_Window",`Event_Response_Days`,"Campaign_Theme",`CampaignThemeName`,"Campaign_Group",`CampaignName`,"Campaign_Type",`Type_Of_Event`,"File_Header",`Header`,"Message",`Message`,"Selection_Criteral",`Replaced_Query`,"Bill_Level_Selection_Criteral",`Replaced_Query_Bill`,"Throttling",`Event_Limit` ,"Schedule",`Scheduled_at`))),"]")
									    into @temp_result from  V_Dashboard_Triggers_popup
									     ;' ;

END IF;


    IF IN_SCREEN <> "DOWNLOAD"
    THEN
    select @view_stmt;
	PREPARE statement from @view_stmt;
	Execute statement;
	Deallocate PREPARE statement;
	SELECT @Query_String;
	PREPARE statement from @Query_String;
	Execute statement;
	Deallocate PREPARE statement; 
    SET out_result_json = @temp_result;
    ELSE
		call S_dashboard_performance_download('V_Dashboard_Insights_Conversions',@out_result_json1);
		SELECT @out_result_json1 INTO out_result_json;
    END IF;
    
END$$
DELIMITER ;
