DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_QSR_DOW`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    
    
     SET @FY_SATRT_DATE = date_format(date_sub(concat(IN_FILTER_CURRENTMONTH,'01'), INTERVAL 11 MONTH),"%Y%m");
	 SET @FY_END_DATE = IN_FILTER_CURRENTMONTH;
	 SET @TM = IN_FILTER_CURRENTMONTH;
	 SET @LM = F_Month_Diff(IN_FILTER_CURRENTMONTH,1);
	 SET @SMLY = F_Month_Diff(IN_FILTER_CURRENTMONTH,12);
	 SET @TY=substr(@TM,1,4);
	 SET @LY=substr(@SMLY,1,4);
	 SET @LLY = @LY - 1;
     SET @SMLLY = F_Month_Diff(IN_FILTER_CURRENTMONTH,24);
     SELECT @TM,@LM,@SMLY,@TY,@LY,@LLY,@SMLLY;
	

IF IN_AGGREGATION_1 = 'BASE_DAY'
THEN
	SET @where_cond = 'Measure = "Base"';
END IF;

IF IN_AGGREGATION_1 = 'VISIT_DAY'
THEN
	SET @where_cond = 'Measure = "Bills"';
END IF;

IF IN_AGGREGATION_1 = 'REV_DAY'
THEN 
	SET @where_cond = 'Measure = "Revenue"';
END IF;

IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_2 = 'DAYWISE'
	THEN 
			SELECT 'in table base;';
			set @view_stmt = concat("CREATE OR REPLACE VIEW V_Dashboard_QSR_DOW_Table AS
    SELECT 
        Day_of_week AS 'DOW',
        TM,
        LM,
        SMLY,
        CH_LM_PER,
        CH_SMLY_PER,
        TY,
        LY,
        LLY,
        CH_LY_PER,
        CH_LLY_PER,
        L12M,
        L24M,
        CH_L12M_PER
    FROM
        Dashboard_QSR_DOW_Temp
    WHERE
        Bill_Year_Month = ",@TM,"
            AND ",@where_cond,"
    GROUP BY 1
    ORDER BY Bill_Day_Week
							;");
		
							SET @Query_String =  
                            'SELECT CONCAT("[",(GROUP_CONCAT(json_object(
                            "DOW",`DOW`,
                            "TM",CAST(format(ifnull(`TM`,0),",") AS CHAR),
                            "LM",CAST(format(ifnull(`LM`,0),",") AS CHAR),
                            "SMLY",CAST(format(ifnull(`SMLY`,0),",") AS CHAR),
                            "% CH. LM",concat(ifnull(`CH_LM_PER`,0),"%"),
                            "% CH. SMLY",concat(ifnull(`CH_SMLY_PER`,0),"%"),
                            "TY",CAST(format(ifnull(`TY`,0),",") AS CHAR),
                            "LY",CAST(format(ifnull(`LY`,0),",") AS CHAR),
                            "LLY",CAST(format(ifnull(`LLY`,0),",") AS CHAR),
                            "% CH. LY",concat(ifnull(`CH_LY_PER`,0),"%"),
                            "% CH. LLY",concat(ifnull(`CH_LLY_PER`,0),"%"),
                            "L12M",CAST(format(ifnull(`L12M`,0),",") AS CHAR),
                            "L24M",CAST(format(ifnull(`L24M`,0),",") AS CHAR),
                            "% CH. L12M",concat(ifnull(`CH_L12M_PER`,0),"%")
                            
                            ) )),"]")into @temp_result from V_Dashboard_QSR_DOW_Table;' ;
		

END IF;


IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_2 = 'WEEKDAY_WEEKEND'
	THEN 
			SELECT "in table base;";
			set @view_stmt = concat("
            CREATE OR REPLACE VIEW V_Dashboard_QSR_DOW_Table AS
    SELECT 
        Weekday_Weekend AS 'DOW',
        sum(TM) as TM,
        sum(LM) as LM,
        sum(SMLY) as SMLY,
        (sum(TM) - sum(LM)) * 100 / sum(LM) as CH_LM_PER,
        (sum(TM) - sum(SMLY)) * 100 / sum(SMLY) as CH_SMLY_PER,
        sum(TY) as TY,
        sum(LY) as LY,
        sum(LLY) as LLY,
        (sum(TY) - sum(LY)) * 100 / sum(LY) as CH_LY_PER,
        (sum(TY) - sum(LLY)) * 100 / sum(LLY) as CH_LLY_PER,
        sum(L12M) as L12M,
        sum(L24M) as L24M,
        (sum(L12M) - sum(L24M)) * 100 / sum(L24M) as CH_L12M_PER
    FROM
        Dashboard_QSR_DOW_Temp
    WHERE
        Bill_Year_Month = ",@TM,"
            AND ",@where_cond,"
    GROUP BY 1
							;");
		
							SET @Query_String =  
                            'SELECT CONCAT("[",(GROUP_CONCAT(json_object(
                            "DOW",`DOW`,
                            "TM",CAST(format(ifnull(`TM`,0),",") AS CHAR),
                            "LM",CAST(format(ifnull(`LM`,0),",") AS CHAR),
                            "SMLY",CAST(format(ifnull(`SMLY`,0),",") AS CHAR),
                            "% CH. LM",concat(round(ifnull(`CH_LM_PER`,0),2),"%"),
                            "% CH. SMLY",concat(round(ifnull(`CH_SMLY_PER`,0),2),"%"),
                            "TY",CAST(format(ifnull(`TY`,0),",") AS CHAR),
                            "LY",CAST(format(ifnull(`LY`,0),",") AS CHAR),
                            "LLY",CAST(format(ifnull(`LLY`,0),",") AS CHAR),
                            "% CH. LY",concat(round(ifnull(`CH_LY_PER`,0),2),"%"),
                            "% CH. LLY",concat(round(ifnull(`CH_LLY_PER`,0),2),"%"),
                            "L12M",CAST(format(ifnull(`L12M`,0),",") AS CHAR),
                            "L24M",CAST(format(ifnull(`L24M`,0),",") AS CHAR),
                            "% CH. L12M",concat(round(ifnull(`CH_L12M_PER`,0),2),"%")
                            ) )),"]")into @temp_result from V_Dashboard_QSR_DOW_Table;' ;
		

END IF;

	IF IN_SCREEN <> "DOWNLOAD"
    THEN
		select @view_stmt;
		PREPARE statement from @view_stmt;
		Execute statement;
		Deallocate PREPARE statement;
		select @Query_String;
		PREPARE statement from @Query_String;
		Execute statement;
		Deallocate PREPARE statement;	
		SET out_result_json = @temp_result;
	ELSE
		call S_dashboard_performance_download('V_Dashboard_QSR_DOW_Table',@out_result_json1);
		select @out_result_json1 into out_result_json;
    END IF;
	
END$$
DELIMITER ;
