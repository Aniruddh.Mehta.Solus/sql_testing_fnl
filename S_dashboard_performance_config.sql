DELIMITER $$
CREATE or replace PROCEDURE `S_dashboard_performance_config`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
    declare IN_MEASURE text;
    declare IN_TEXT1 text;
	declare IN_TEXT2 text;
    declare IN_TEXT3 text;
    declare IN_TEXT4 text;
    declare IN_MARQUEE text;
   
    declare IN_THRESHOLD1 text;
    declare IN_THRESHOLD2 text;
	declare IN_PREFERENCE text;
    declare vSuccess text;
    
	declare IN_START_DATE text;
    declare IN_END_DATE text;
    declare IN_FIRSTDAYOFWEEK text;
    declare IN_CURRENCY text;
    declare IN_METHOD text;
    
	declare IN_NUMBERFORMAT text;
    declare IN_SEPARATOR text;
    declare IN_INFO text;
    declare IN_DECIMALPOSITION text;
    	
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
	SET IN_MEASURE=json_unquote(json_extract(in_request_json,"$.MEASURE"));
   
    
    SET vSuccess=0;
    CASE 
    WHEN IN_SCREEN="PERFORMANCE_CONFIG_NEWS" THEN
    BEGIN
   	  		
	set @view_stmt1= 'CREATE or REPLACE VIEW V_Dashboard_Performance_Text_Config AS 
    ( SELECT       
        MAX(IF(`Serial_No` = 1,
            `Content`,
            NULL)) AS `TEXT1`,
        MAX(IF(`Serial_No` = 2,
            `Content`,
            NULL)) AS `TEXT2`,
         MAX(IF(`Serial_No` = 3,
            `Content`,
            NULL)) AS `TEXT3`,
        MAX(IF(`Serial_No` = 4,
            `Content`,
            NULL)) AS `TEXT4`,
		 MAX(IF(`Serial_No` = 5,
            `Content`,
            NULL)) AS `MARQUEE`
    FROM
        `UI_Configuration_Text`);';
    select @view_stmt1;
	PREPARE statement1 from @view_stmt1;
	Execute statement1;
	Deallocate PREPARE statement1;
    
    SET @Query_String2 =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("TEXT1",`TEXT1`,"TEXT2",`TEXT2`,"TEXT3",`TEXT3`,"TEXT4",`TEXT4`,"MARQUEE",`MARQUEE`) )),"]")into @temp_result_perf_text_config from V_Dashboard_Performance_Text_Config;' ;
    select @Query_String2;
	PREPARE stm2 from @Query_String2;
	Execute stm2;
	Deallocate PREPARE stm2;   
    select @temp_result_perf_text_config into out_result_json;
    END;
     WHEN IN_SCREEN="PERFORMANCE_CONFIG_NEWS_UPDATE" THEN
        BEGIN
	SET IN_TEXT1 = json_unquote(json_extract(in_request_json,"$.TEXT1"));
    SET IN_TEXT2 = json_unquote(json_extract(in_request_json,"$.TEXT2"));
    SET IN_TEXT3 = json_unquote(json_extract(in_request_json,"$.TEXT3"));
    SET IN_TEXT4 = json_unquote(json_extract(in_request_json,"$.TEXT4"));
	SET IN_MARQUEE = json_unquote(json_extract(in_request_json,"$.MARQUEE"));
          TRUNCATE TABLE `UI_Configuration_Text`;
	      SET @Query_String3 =  concat('INSERT INTO `UI_Configuration_Text`
(`Text_Type`,`Serial_No`,`Content`,`User_Name`,`Modified_Date`,`Valid_From`,`Valid_To`) VALUES
("Text",1,"',IN_TEXT1,'","',IN_USER_NAME,'",CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP(),DATE_ADD(CURRENT_TIMESTAMP(), INTERVAL 12 MONTH)),
("Text",2,"',IN_TEXT2,'","',IN_USER_NAME,'",CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP(),DATE_ADD(CURRENT_TIMESTAMP(), INTERVAL 12 MONTH)),
("Text",3,"',IN_TEXT3,'","',IN_USER_NAME,'",CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP(),DATE_ADD(CURRENT_TIMESTAMP(), INTERVAL 12 MONTH)),
("Text",4,"',IN_TEXT4,'","',IN_USER_NAME,'",CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP(),DATE_ADD(CURRENT_TIMESTAMP(), INTERVAL 12 MONTH)),
("Marquee",5,"',IN_MARQUEE,'","',IN_USER_NAME,'",CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP(),DATE_ADD(CURRENT_TIMESTAMP(), INTERVAL 12 MONTH));'
 );
          
          select @Query_String3;
	      PREPARE stm3 from @Query_String3;
	      Execute stm3;
	      Deallocate PREPARE stm3;   
          SET VSuccess=1;
         SET out_result_json = json_object("Status",VSuccess); 
         END; 
	 WHEN IN_SCREEN="PERFORMANCE_CONFIG_THRESHOLD" THEN
        BEGIN
         SET @Query_String4 = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("THRESHOLD1",`Threshold1`,"THRESHOLD2",`Threshold2`,"PREFERENCE",`Preference`) )),"]")into @temp_result_perf_threshold_config from UI_Configuration where Measure_Name="',IN_MEASURE ,'";') ;
    select @Query_String4;
	PREPARE stm4 from @Query_String4;
	Execute stm4;
	Deallocate PREPARE stm4;   
    select @temp_result_perf_threshold_config into out_result_json;
    END;
  
	 WHEN IN_SCREEN="PERFORMANCE_CONFIG_THRESHOLD_UPDATE" THEN
	BEGIN
   	SET IN_THRESHOLD1 = json_unquote(json_extract(in_request_json,"$.THRESHOLD1"));
    SET IN_THRESHOLD2 = json_unquote(json_extract(in_request_json,"$.THRESHOLD2"));
    
    SET IN_PREFERENCE = json_unquote(json_extract(in_request_json,"$.PREFERENCE"));
  
	
    UPDATE `UI_Configuration` SET 
    `Threshold1`=IN_THRESHOLD1,
    `Threshold2`=IN_THRESHOLD2,
    
    `Preference`=IN_PREFERENCE where `Measure_Name`=IN_MEASURE;
     SET VSuccess=1;
         SET out_result_json = json_object("Status",VSuccess); 
    END;
     WHEN IN_SCREEN="PERFORMANCE_CONFIG_GLOBAL" THEN
        BEGIN
         CREATE or REPLACE VIEW `V_Dashboard_Performance_Global_Config` AS 
         ( SELECT 
           CASE WHEN isActive="true" and Config_Name="FISCAL" THEN "FISCAL" 
		  ELSE "CALENDAR" END AS `MEASURE`,
          IFNULL(max(IF(Config_Name="CALENDAR",Value1,NULL)),max(IF(Config_Name="FISCAL",Value1,NULL))) As `START_DATE`,
          IFNULL(max(IF(Config_Name="CALENDAR",Value2,NULL)),max(IF(Config_Name="FISCAL",Value2,NULL))) As `END_DATE`,
          max(IF(Config_Name="FIRSTDAYOFWEEK",Value1,NULL) ) As `FIRSTDAYOFWEEK`,
          max(IF(Config_Name="CURRENCY",Value1,NULL) ) As `CURRENCY`,
          max(IF(Config_Name="Incremental_Revenue_Method",Value1,NULL) ) As `METHOD`
          from `UI_Configuration_Global` where isActive="true");   
        
      --  select * from V_Dashboard_Performance_Global_Config;
        
        
         SET @Query_String5 = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("MEASURE",`MEASURE`,"START_DATE", STR_TO_DATE(`START_DATE`,"%Y%m"),"END_DATE",STR_TO_DATE(`END_DATE`,"%Y%m"),"FIRSTDAYOFWEEK",`FIRSTDAYOFWEEK`,"CURRENCY",`CURRENCY`,"METHOD",`METHOD`) )),"]")into @temp_result_perf_global_config from V_Dashboard_Performance_Global_Config ;') ;
		 select @Query_String5;
	     PREPARE stm5 from @Query_String5;
	     Execute stm5;
	     Deallocate PREPARE stm5;   
         select @temp_result_perf_global_config into out_result_json;
       END;
       
	 WHEN IN_SCREEN="PERFORMANCE_CONFIG_GLOBAL_UPDATE" THEN
	 BEGIN
   	SET IN_START_DATE = json_unquote(json_extract(in_request_json,"$.START_DATE"));
    SET IN_END_DATE = json_unquote(json_extract(in_request_json,"$.END_DATE"));
    SET IN_FIRSTDAYOFWEEK = json_unquote(json_extract(in_request_json,"$.FIRSTDAYOFWEEK"));
    SET IN_CURRENCY = json_unquote(json_extract(in_request_json,"$.CURRENCY"));
    SET IN_METHOD = json_unquote(json_extract(in_request_json,"$.METHOD"));
 
	
     UPDATE `UI_Configuration_Global` SET 
    `Value1`=IN_START_DATE,
    `Value2`=IN_END_DATE,
    `isActive`="true"
     where `Config_Name`=IN_MEASURE;
     
     UPDATE `UI_Configuration_Global` SET 
    `Value1`=IN_METHOD,
    `isActive`="true"
     where `Config_Name`='Incremental_Revenue_Method';     

	UPDATE `UI_Configuration_Global` SET 
    `Value1`=IN_FIRSTDAYOFWEEK,
    `isActive`="true"
     where `Config_Name`='FIRSTDAYOFWEEK';
	 UPDATE `UI_Configuration_Global` SET 
    `Value1`=IN_CURRENCY,
    `isActive`="true"
     where `Config_Name`='CURRENCY';
   
   UPDATE `UI_Configuration_Global` SET `isActive`= 
    CASE 
    WHEN `Config_Name`=IN_MEASURE THEN "true"
	WHEN `Config_Name`='FIRSTDAYOFWEEK' THEN "true"
    WHEN `Config_Name`='CURRENCY' THEN "true"
    WHEN `Config_Name`='Incremental_Revenue_Method' THEN "true"
    ELSE "false" END;   
    
    UPDATE `UI_Configuration` SET 
    `Currency`=IN_CURRENCY
    where `Measure_Name` in ('LIFTYTD','LIFTMTD');
    
        
     SET VSuccess=1;
         SET out_result_json = json_object("Status",VSuccess); 
    END;
     WHEN IN_SCREEN="PERFORMANCE_CONFIG_NUMBER" THEN
        BEGIN
         SET @Query_String7 = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("NUMBERFORMAT",`Number_Format`,"SEPARATOR",`Number_Separator`,"INFO",`Information_Text`,"DECIMALPOSITION",`Decimal_Position`) )),"]")into @temp_result_perf_number_config from UI_Configuration where Measure_Name="',IN_MEASURE ,'";') ;
    select @Query_String7;
	PREPARE stm7 from @Query_String7;
	Execute stm7;
	Deallocate PREPARE stm7;   
    select @temp_result_perf_number_config into out_result_json;
    END;
     WHEN IN_SCREEN="PERFORMANCE_CONFIG_NUMBER_UPDATE" THEN
	BEGIN
   	SET IN_NUMBERFORMAT = json_unquote(json_extract(in_request_json,"$.NUMBERFORMAT"));
    SET IN_SEPARATOR = json_unquote(json_extract(in_request_json,"$.SEPARATOR"));
    SET IN_INFO = json_unquote(json_extract(in_request_json,"$.INFO"));
    SET IN_DECIMALPOSITION = json_unquote(json_extract(in_request_json,"$.DECIMALPOSITION"));
  
    UPDATE `UI_Configuration` SET 
    `Number_Format`=IN_NUMBERFORMAT,
    `Number_Separator`=IN_SEPARATOR,
    `Information_Text`=IN_INFO,
    `Decimal_Position`=IN_DECIMALPOSITION where `Measure_Name`=IN_MEASURE;
     SET VSuccess=1;
         SET out_result_json = json_object("Status",VSuccess); 
    END;
	ELSE
			SET out_result_json=json_object("Status","Not Implemented Yet");
	END CASE;	
END$$
DELIMITER ;
