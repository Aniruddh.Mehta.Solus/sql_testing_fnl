
CREATE OR REPLACE PROCEDURE `S_UI_Coverage_View`(IN SVOC_Condition NVARCHAR(65536))
BEGIN
    DECLARE done1 INT DEFAULT FALSE;
    DECLARE Column_Name_cur1 TEXT;
    Declare cur1 cursor for
    SELECT distinct Column_Name_in_One_View from Customer_View_Config
    where In_Use_Coverage_View <> 'Mandatory'
    and Column_Name <> 'Customer_Id'
    and Table_Name not in ('Communication_Template','Customer_Recos_View');
    
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done1 = TRUE;
    SET done1 = FALSE;
    SET SQL_SAFE_UPDATES=0;
    
    
    UPDATE Customer_View_Config
	SET In_Use_Coverage_View = "N" 
	where In_Use_Coverage_View <> 'Mandatory';
	
    
    
    SELECT SVOC_Condition INTO @SVOC_Condition;
    
		open cur1;
		new_loop_1: LOOP
						FETCH cur1 into Column_Name_cur1;
						IF done1 THEN
				 LEAVE new_loop_1;
			END IF;
            IF @SVOC_Condition like concat("%",Column_Name_cur1,"%")
            THEN
				SELECT "YES";
            END IF;
            set @top_query=concat('Update Customer_View_Config
            SET In_Use_Coverage_View = "Y" 
            where Column_Name_in_One_View = "',Column_Name_cur1,'"
            and "',@SVOC_Condition,'" like "%',Column_Name_cur1,'%"');
            
			
            PREPARE stmt from @top_query;        
			EXECUTE stmt;
			DEALLOCATE PREPARE stmt;
		end LOOP;
		close cur1;
   /* Create View */
   
   CALL S_UI_Create_Coverage_View();
    
END

