DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_insights_bouncecurve_metrics`()
BEGIN
DECLARE vprevcur_NoOfDays int Default 0;
DECLARE vCheckEndOfCursor int Default 0;
DECLARE vcur_NoOfDays int default 0;
DECLARE vprevcur_NoOfDaysNTR int Default 0;
DECLARE vcur_NoOfDaysNTR int default 0;
DECLARE vStart_Date BIGINT DEFAULT 0;
DECLARE vEnd_Date BIGINT DEFAULT 0;


DECLARE cur_NoOfDays CURSOR FOR SELECT DISTINCT NoOfDays FROM Dashboard_Insights_BounceCurve where AGGREGATION='NTN1' ORDER BY NoOfDays ASC;

DECLARE cur_NoOfDaysNTR CURSOR FOR SELECT DISTINCT NoOfDays FROM Dashboard_Insights_BounceCurve where AGGREGATION='NTR' ORDER BY NoOfDays ASC;

DECLARE cur_NoOfDaysL24M CURSOR FOR SELECT DISTINCT NoOfDays FROM Dashboard_Insights_BounceCurve where AGGREGATION='NTN1' and Measure = 'L24M' ORDER BY NoOfDays ASC;

DECLARE cur_NoOfDaysNTRL24M CURSOR FOR SELECT DISTINCT NoOfDays FROM Dashboard_Insights_BounceCurve where AGGREGATION='NTR' and Measure = 'L24M' ORDER BY NoOfDays ASC;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET vCheckEndOfCursor=1;

DELETE from log_solus where module='S_dashboard_insights_bouncecurve';
SET SQL_SAFE_UPDATES=0;

INSERT INTO Dashboard_Metrics_Log
(
`Screen_Name`,
`Status`
)
VALUES
('INSIGHTS - BOUNCE CURVE',
'Started');

SET vStart_Date = date_format(date_sub(concat(date_format(current_date(),"%Y%m"),'01'), INTERVAL 23 MONTH),"%Y%m");
SET vEnd_Date = date_format(current_date(),"%Y%m");

select vStart_Date, vEnd_Date;

INSERT INTO `Dashboard_Insights_BounceCurve`
(`AGGREGATION`,
`Measure`,
`NoOfDays`,
`NoOfCustomer`,
`NoOfCustomerPercent`,
`Revenue_Center`,
`CLMSegmentName`)
SELECT 
'NTN1', 'L24M', Lt_ADGBT, COUNT(DISTINCT A.Customer_Id), - 1, 'NA', 'NA'
FROM CDM_Customer_TP_Var A
JOIN CDM_Bill_Details B
ON A.Customer_Id = B.Customer_Id
WHERE Lt_Trans > 1
AND Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY Lt_ADGBT
ORDER BY Lt_ADGBT ASC;

INSERT INTO `Dashboard_Insights_BounceCurve`
(`AGGREGATION`,
`Measure`,
`NoOfDays`,
`NoOfCustomer`,
`NoOfCustomerPercent`,
`Revenue_Center`,
`CLMSegmentName`)
SELECT 
    'NTR', 'L24M', DATEDIFF(STD,FTD) as NTR_Lt_ADGBT, COUNT(DISTINCT A.Customer_Id), - 1, 'NA', 'NA'
FROM CDM_Customer_TP_Var A
JOIN CDM_Bill_Details B
ON A.Customer_Id = B.Customer_Id
WHERE Lt_Trans > 1 and FTD <> '' and STD <> ''
AND Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
GROUP BY NTR_Lt_ADGBT
ORDER BY NTR_Lt_ADGBT ASC;

INSERT INTO `Dashboard_Insights_BounceCurve`
(`AGGREGATION`,
`Measure`,
`NoOfDays`,
`NoOfCustomer`,
`NoOfCustomerPercent`,
`Revenue_Center`,
`CLMSegmentName`)
SELECT 
    'NTN1', 'ATM', Lt_ADGBT, COUNT(Customer_Id), - 1, 'NA', 'NA'
FROM CDM_Customer_TP_Var
WHERE Lt_Trans > 1
GROUP BY Lt_ADGBT
ORDER BY Lt_ADGBT ASC;

INSERT INTO `Dashboard_Insights_BounceCurve`
(`AGGREGATION`,
`Measure`,
`NoOfDays`,
`NoOfCustomer`,
`NoOfCustomerPercent`,
`Revenue_Center`,
`CLMSegmentName`)
SELECT 
    'NTR', 'ATM', DATEDIFF(STD,FTD) as NTR_Lt_ADGBT,  COUNT(Customer_Id), - 1, 'NA', 'NA'
FROM CDM_Customer_TP_Var
WHERE Lt_Trans > 1 and FTD <> '' and STD <> ''
GROUP BY NTR_Lt_ADGBT
ORDER BY NTR_Lt_ADGBT ASC;

select 'NTN1';
OPEN cur_NoOfDays;
BEGIN
	SET vprevcur_NoOfDays=-1;
	SET vCheckEndOfCursor=0;
	LOOP_cur_NoOfDays : LOOP
		FETCH cur_NoOfDays into vcur_NoOfDays;
		IF vCheckEndOfCursor THEN    
			LEAVE LOOP_cur_NoOfDays;
		END IF;
		select concat('prev:'+vprevcur_NoOfDays+' cur :'+vcur_NoOfDays) as '';
        
        UPDATE Dashboard_Insights_BounceCurve
		set NoOfCumulativeCustomer=NoOfCustomer + (select ifnull(NoOfCumulativeCustomer,0) from Dashboard_Insights_BounceCurve where AGGREGATION = 'NTN1' and Measure = 'ATM' and NoOfDays=vprevcur_NoOfDays )
		where NoOfDays=vcur_NoOfDays 
        AND AGGREGATION = 'NTN1'
        AND Measure = 'ATM';
		SET vprevcur_NoOfDays=vcur_NoOfDays;
        
	END LOOP LOOP_cur_NoOfDays;
    
    UPDATE Dashboard_Insights_BounceCurve 
	SET NoOfCustomerPercent=100*(NoOfCumulativeCustomer/(SELECT  NoOfCumulativeCustomer FROM Dashboard_Insights_BounceCurve WHERE AGGREGATION = 'NTN1' AND Measure = 'ATM' order by NoOfDays DESC LIMIT 1))
	WHERE AGGREGATION = 'NTN1'
    and Measure = 'ATM';

	
	SELECT  NoOfCumulativeCustomer INTO @x FROM Dashboard_Insights_BounceCurve WHERE AGGREGATION = 'NTN1'order by NoOfDays DESC LIMIT 1 ;
	SELECT  NoOfDays INTO @y FROM Dashboard_Insights_BounceCurve WHERE AGGREGATION = 'NTN1'order by NoOfDays DESC LIMIT 1 ;
	insert into log_solus(module,rec_start, rec_end,msg) values ('S_dashboard_insights_bouncecurve',ifnull(@x,0), ifnull(@y,0),'NTN1 Done');
	
END;

select 'NTN1L24M';
OPEN cur_NoOfDaysL24M;
BEGIN
	SET vprevcur_NoOfDays=-1;
	SET vCheckEndOfCursor=0;
	LOOP_cur_NoOfDaysL24M : LOOP
		FETCH cur_NoOfDaysL24M into vcur_NoOfDays;
		IF vCheckEndOfCursor THEN    
			LEAVE LOOP_cur_NoOfDaysL24M;
		END IF;
		select concat('prev:'+vprevcur_NoOfDays+' cur :'+vcur_NoOfDays) as '';

		UPDATE Dashboard_Insights_BounceCurve
		set NoOfCumulativeCustomer= NoOfCustomer  + (select ifnull(NoOfCumulativeCustomer,0) from Dashboard_Insights_BounceCurve where AGGREGATION = 'NTN1' and Measure = 'L24M' and NoOfDays=vprevcur_NoOfDays )
		where NoOfDays=vcur_NoOfDays 
        AND AGGREGATION = 'NTN1'
        AND Measure = 'L24M';
        
		SET vprevcur_NoOfDays=vcur_NoOfDays;
	END LOOP LOOP_cur_NoOfDaysL24M;
    
    UPDATE Dashboard_Insights_BounceCurve 
	SET NoOfCustomerPercent=100*(NoOfCumulativeCustomer/(SELECT  NoOfCumulativeCustomer FROM Dashboard_Insights_BounceCurve WHERE AGGREGATION = 'NTN1' AND Measure = 'L24M' order by NoOfDays DESC LIMIT 1))
	WHERE AGGREGATION = 'NTN1'
    and Measure = 'L24M';
    
    END;

select 'NTR';
OPEN cur_NoOfDaysNTR;
BEGIN
	SET vprevcur_NoOfDaysNTR=-1;
	SET vCheckEndOfCursor=0;
	LOOP_cur_NoOfDaysNTR : LOOP
		FETCH cur_NoOfDaysNTR into vcur_NoOfDaysNTR;
		IF vCheckEndOfCursor THEN    
			LEAVE LOOP_cur_NoOfDaysNTR;
		END IF;
        
        UPDATE Dashboard_Insights_BounceCurve
		SET NoOfCumulativeCustomer=NoOfCustomer + (SELECT ifnull(NoOfCumulativeCustomer,0) from Dashboard_Insights_BounceCurve where  AGGREGATION = 'NTR' and Measure = 'ATM' and NoOfDays=vprevcur_NoOfDaysNTR)
		WHERE NoOfDays=vcur_NoOfDaysNTR 
        AND AGGREGATION = 'NTR'
        and Measure = 'ATM';
		SET vprevcur_NoOfDaysNTR=vcur_NoOfDaysNTR;
	END LOOP LOOP_cur_NoOfDaysNTR;
    
	select 'NTR-1';
    
    UPDATE Dashboard_Insights_BounceCurve 
	SET NoOfCustomerPercent=100*(NoOfCumulativeCustomer/
	(SELECT  NoOfCumulativeCustomer FROM Dashboard_Insights_BounceCurve WHERE AGGREGATION = 'NTR' and Measure = 'ATM' order by NoOfDays DESC LIMIT 1  ))
	where AGGREGATION = 'NTR'
    and Measure = 'ATM';
	
    select 'NTR-2';
	
	SELECT  NoOfCumulativeCustomer INTO @x FROM Dashboard_Insights_BounceCurve WHERE AGGREGATION = 'NTR'order by NoOfDays DESC LIMIT 1 ;
	SELECT  NoOfDays INTO @y FROM Dashboard_Insights_BounceCurve WHERE AGGREGATION = 'NTR'order by NoOfDays DESC LIMIT 1 ;
	insert into log_solus(module,rec_start, rec_end,msg) values ('S_dashboard_insights_bouncecurve',ifnull(@x,0), ifnull(@y,0),'NTR Done');
	
END;

select 'NTRL24M';
OPEN cur_NoOfDaysNTRL24M;
BEGIN
	SET vprevcur_NoOfDaysNTR=-1;
	SET vCheckEndOfCursor=0;
	LOOP_cur_NoOfDaysNTRL24M : LOOP
		FETCH cur_NoOfDaysNTRL24M into vcur_NoOfDaysNTR;
		IF vCheckEndOfCursor THEN    
			LEAVE LOOP_cur_NoOfDaysNTRL24M;
		END IF;
		
		UPDATE Dashboard_Insights_BounceCurve
		SET NoOfCumulativeCustomer=NoOfCustomer + (SELECT ifnull(NoOfCumulativeCustomer,0) from Dashboard_Insights_BounceCurve where  AGGREGATION = 'NTR' and Measure = 'L24M' and NoOfDays=vprevcur_NoOfDaysNTR)
		WHERE NoOfDays=vcur_NoOfDaysNTR 
        AND AGGREGATION = 'NTR'
        and Measure = 'L24M';

		SET vprevcur_NoOfDaysNTR=vcur_NoOfDaysNTR;
	END LOOP LOOP_cur_NoOfDaysNTRL24M;
    
    UPDATE Dashboard_Insights_BounceCurve 
	SET NoOfCustomerPercent=100*(NoOfCumulativeCustomer/
	(SELECT  NoOfCumulativeCustomer FROM Dashboard_Insights_BounceCurve WHERE AGGREGATION = 'NTR' and Measure = 'L24M' order by NoOfDays DESC LIMIT 1  ))
	where AGGREGATION = 'NTR'
    and Measure = 'L24M';
    
    END;
    
    UPDATE Dashboard_Metrics_Log
SET `Status` = 'Ended' WHERE Screen_Name = 'INSIGHTS - BOUNCE CURVE';
    
END$$
DELIMITER ;
