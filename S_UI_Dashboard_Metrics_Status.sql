DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_UI_Dashboard_Metrics_Status`(IN in_request_json json, OUT out_result_json json)
BEGIN
    

    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
    declare EDIT Bigint; 

	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare IN_AGGREGATION_4 text;
	declare IN_AGGREGATION_5 text;
	
	SET SQL_SAFE_UPDATES=0;
	SET foreign_key_checks=0;

	Set in_request_json = Replace(in_request_json,'\n','\\n');
  
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1")); 
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_AGGREGATION_4 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_4"));
    
	Set @temp_result =Null;
    Set @vSuccess ='';
	Set @vErrMsg='';
	Set @vSuccess='';
	Set @Err =0;
	set @a1 = '[{"Val":"Values"}]';
    
    CREATE or REPLACE VIEW `V_Dashboard_Metrics_Status` AS 
         SELECT 'PERFORMANCE - OUTREACHES' AS `Screen_Name`, date_format(MAX(Created_Date),'%d-%b-%Y %H:%i') AS `Refresh_Date`,(select COUNT(*) FROM log_solus_dashboard where module = 'dashboard_performance_outreaches' and Created_Date between DATE_SUB( now(), INTERVAL 30 day) and now()) as `Screen_Usage_Count` FROM CLM_Event_Dashboard
UNION
SELECT 'PERFORMANCE - IMPACT OVERVIEW' AS `Screen_Name`, date_format(MAX(Created_Date),'%d-%b-%Y %H:%i') AS `Refresh_Date`,(select COUNT(*) FROM log_solus_dashboard where module = 'dashboard_performance_lift' and Created_Date between DATE_SUB( now(), INTERVAL 30 day) and now()) as `Screen_Usage_Count` FROM CLM_Event_Dashboard
UNION
SELECT 'PERFORMANCE - LIFT DRILLDOWN' AS `Screen_Name`, date_format(MAX(Created_Date),'%d-%b-%Y %H:%i') AS `Refresh_Date`,(select COUNT(*) FROM log_solus_dashboard where module = 'dashboard_performance_Lift_Drill' and Created_Date between DATE_SUB( now(), INTERVAL 30 day) and now()) as `Screen_Usage_Count` FROM CLM_Event_Dashboard
UNION
SELECT 'PERFORMANCE - MONTH REPORT' AS `Screen_Name`, date_format(MAX(Created_Date),'%d-%b-%Y %H:%i') AS `Refresh_Date`,(select COUNT(*) FROM log_solus_dashboard where module = 'dashboard_performance_monthreport' and Created_Date between DATE_SUB( now(), INTERVAL 30 day) and now()) as `Screen_Usage_Count` FROM CLM_Event_Dashboard
UNION
SELECT 'PERFORMANCE - DEEP DIVE' AS `Screen_Name`, date_format(MAX(Created_Date),'%d-%b-%Y %H:%i') AS `Refresh_Date` ,(select COUNT(*) FROM log_solus_dashboard where module = 'dashboard_performance_deepdive' and Created_Date between DATE_SUB( now(), INTERVAL 30 day) and now()) as `Screen_Usage_Count` FROM CLM_Event_Dashboard
UNION
SELECT 'PERFORMANCE - FUNNEL REPORT' AS `Screen_Name`, date_format(MAX(Created_Date),'%d-%b-%Y %H:%i') AS `Refresh_Date`,(select COUNT(*) FROM log_solus_dashboard where module = 'dashboard_performance_funnelreport' and Created_Date between DATE_SUB( now(), INTERVAL 30 day) and now()) as `Screen_Usage_Count` FROM CLM_Event_Dashboard
UNION
SELECT 'PERFORMANCE - CAMPAIGNS' AS `Screen_Name`, date_format(MAX(Created_Date),'%d-%b-%Y %H:%i') AS `Refresh_Date`,(select COUNT(*) FROM log_solus_dashboard where module = 'dashboard_performance_campaigns' and Created_Date between DATE_SUB( now(), INTERVAL 30 day) and now()) as `Screen_Usage_Count` FROM CLM_Event_Dashboard
UNION
SELECT 'PERFORMANCE - LIFT DETAIL' AS `Screen_Name`, date_format(MAX(Created_Date),'%d-%b-%Y %H:%i') AS `Refresh_Date`,(select COUNT(*) FROM log_solus_dashboard where module = 'dashboard_performance_liftdetail' and Created_Date between DATE_SUB( now(), INTERVAL 30 day) and now()) as `Screen_Usage_Count` FROM CLM_Event_Dashboard
UNION
SELECT 'PERFORMANCE - DAY REPORT' AS `Screen_Name`, date_format(MAX(Created_Date),'%d-%b-%Y %H:%i') AS `Refresh_Date`,(select COUNT(*) FROM log_solus_dashboard where module = 'dashboard_performance_dayreport' and Created_Date between DATE_SUB( now(), INTERVAL 30 day) and now()) as `Screen_Usage_Count` FROM CLM_Event_Dashboard
UNION
SELECT 'PERFORMANCE - MONTHLY PERFORMANCE' AS `Screen_Name`, date_format(MAX(Created_Date),'%d-%b-%Y %H:%i') AS `Refresh_Date`,(select COUNT(*) FROM log_solus_dashboard where module = 'dashboard_performance_trigger' and Created_Date between DATE_SUB( now(), INTERVAL 30 day) and now()) as `Screen_Usage_Count` FROM CLM_Event_Dashboard
UNION
SELECT 'PERFORMANCE - YIELD COVERAGE' AS `Screen_Name`, date_format(MAX(Created_Date),'%d-%b-%Y %H:%i') AS `Refresh_Date`,(select COUNT(*) FROM log_solus_dashboard where module = 'dashboard_performance_yield' and Created_Date between DATE_SUB( now(), INTERVAL 30 day) and now()) as `Screen_Usage_Count` FROM CLM_Event_Dashboard
UNION
SELECT 'PERFORMANCE - CONVERSION' AS `Screen_Name`, date_format(MAX(Created_Date),'%d-%b-%Y %H:%i') AS `Refresh_Date`,(select COUNT(*) FROM log_solus_dashboard where module like 'dashboard_insights_conversions' and Created_Date between DATE_SUB( now(), INTERVAL 30 day) and now()) as `Screen_Usage_Count` FROM Dashboard_Insights_Conversions
UNION
SELECT 'PERFORMANCE - SEGMENT PERFORMANCE' AS `Screen_Name`, date_format(MAX(Created_Date),'%d-%b-%Y %H:%i') AS `Refresh_Date`,(select COUNT(*) FROM log_solus_dashboard where module like 'S_dashboard_performance_Campaign_Segment' and Created_Date between DATE_SUB( now(), INTERVAL 30 day) and now()) as `Screen_Usage_Count` FROM CLM_Event_Dashboard_Campaign_Segment
UNION
SELECT 'INSIGHTS - CUSTOMER DISTRIBUTION' AS `Screen_Name`, date_format(MAX(Modified_Date),'%d-%b-%Y %H:%i') AS `Refresh_Date`,(select COUNT(*) FROM log_solus_dashboard where module = 'dashboard_insights_customerdistribution' and Created_Date between DATE_SUB( now(), INTERVAL 30 day) and now()) as `Screen_Usage_Count` FROM Dashboard_Insights_Customer_Distribution
UNION
SELECT 'INSIGHTS - BUSINESS OVERVIEW' AS `Screen_Name`, date_format(MAX(Modified_Date),'%d-%b-%Y %H:%i') AS `Refresh_Date`,(select COUNT(*) FROM log_solus_dashboard where module = 'dashboard_insights_business' and Created_Date between DATE_SUB( now(), INTERVAL 30 day) and now()) as `Screen_Usage_Count` FROM Dashboard_Insights_Base
UNION
SELECT 'INSIGHTS - BASE' AS `Screen_Name`, date_format(MAX(Modified_Date),'%d-%b-%Y %H:%i') AS `Refresh_Date`,(select COUNT(*) FROM log_solus_dashboard where module = 'dashboard_insights_base' and Created_Date between DATE_SUB( now(), INTERVAL 30 day) and now()) as `Screen_Usage_Count` FROM Dashboard_Insights_Base
UNION
SELECT 'INSIGHTS - MONTH BASE' AS `Screen_Name`, date_format(MAX(Modified_Date),'%d-%b-%Y %H:%i') AS `Refresh_Date`,(select COUNT(*) FROM log_solus_dashboard where module = 'dashboard_insights_monthbase' and Created_Date between DATE_SUB( now(), INTERVAL 30 day) and now()) as `Screen_Usage_Count` FROM Dashboard_Insights_Base
UNION
SELECT 'INSIGHTS - SEGMENTS' AS `Screen_Name`, date_format(MAX(Modified_Date),'%d-%b-%Y %H:%i') AS `Refresh_Date`,(select COUNT(*) FROM log_solus_dashboard where module like 'dashboard_insights_segment' and Created_Date between DATE_SUB( now(), INTERVAL 30 day) and now()) as `Screen_Usage_Count` FROM Dashboard_Insights_Base
UNION
SELECT 'INSIGHTS - CRM KPI' AS `Screen_Name`, date_format(MAX(Modified_Date),'%d-%b-%Y %H:%i') AS `Refresh_Date`,(select COUNT(*) FROM log_solus_dashboard where module like '%CRM%' and Created_Date between DATE_SUB( now(), INTERVAL 30 day) and now()) as `Screen_Usage_Count` FROM CDM_CRM_KPIS_Dashboard
UNION
SELECT 'INSIGHTS - BOUNCE CURVE' AS `Screen_Name`, date_format(MAX(Modified_Date),'%d-%b-%Y %H:%i') AS `Refresh_Date`,(select COUNT(*) FROM log_solus_dashboard where module = 'dashboard_insights_bouncecurve' and Created_Date between DATE_SUB( now(), INTERVAL 30 day) and now()) as `Screen_Usage_Count` FROM Dashboard_Insights_BounceCurve WHERE AGGREGATION IN ('NTN1','NTR')
UNION
SELECT 'INSIGHTS - GLUE NUMBER' AS `Screen_Name`, date_format(MAX(Modified_Date),'%d-%b-%Y %H:%i') AS `Refresh_Date`,(select COUNT(*) FROM log_solus_dashboard where module like '%Bounce%' and Created_Date between DATE_SUB( now(), INTERVAL 30 day) and now()) as `Screen_Usage_Count` FROM Dashboard_Insights_BounceCurve WHERE AGGREGATION IN ('GLUE','GLUEDISTRIBUTION')
UNION
SELECT 'INSIGHTS - REPEAT COHORTS' AS `Screen_Name`, date_format(MAX(Modified_Date),'%d-%b-%Y %H:%i') AS `Refresh_Date`,(select COUNT(*) FROM log_solus_dashboard where module = 'dashboard_insights_repeat_cohorts' and Created_Date between DATE_SUB( now(), INTERVAL 30 day) and now()) as `Screen_Usage_Count` FROM Dashboard_Insights_Repeat_Cohorts
UNION
SELECT 'INSIGHTS - GLUE COHORTS' AS `Screen_Name`, date_format(MAX(Modified_Date),'%d-%b-%Y %H:%i') AS `Refresh_Date`,(select COUNT(*) FROM log_solus_dashboard where module like 'dashboard_insights_glue_cohorts' and Created_Date between DATE_SUB( now(), INTERVAL 30 day) and now()) as `Screen_Usage_Count` FROM Dashboard_Insights_Glue_Cohorts
UNION
SELECT 'INSIGHTS - SEGMENT MOVEMENT' AS `Screen_Name`, date_format(MAX(Created_Date),'%d-%b-%Y %H:%i') AS `Refresh_Date`,(select COUNT(*) FROM log_solus_dashboard where module = 'dashboard_insights_movements' and Created_Date between DATE_SUB( now(), INTERVAL 30 day) and now()) as `Screen_Usage_Count` FROM Dashboard_Insights_SegmentMovement_Summary
UNION
SELECT 'INSIGHTS - SEGMENTATION' AS `Screen_Name`, date_format(MAX(Modified_Date),'%d-%b-%Y %H:%i') AS `Refresh_Date`,(select COUNT(*) FROM log_solus_dashboard where module = 'dashboard_insights_segmentation_scheme' and Modified_Date between DATE_SUB( now(), INTERVAL 30 day) and now()) as `Screen_Usage_Count` FROM Dashboard_Insights_Segmentation_Schemes
UNION
SELECT 'INTELLIGENCE - ATTR. ANALYSIS' AS `Screen_Name`, date_format(MAX(Created_Date),'%d-%b-%Y %H:%i') AS `Refresh_Date`,(select COUNT(*) FROM log_solus_dashboard where module = 'S_dashboard_performance_whatif_analysis_One_Attr' and Created_Date between DATE_SUB( now(), INTERVAL 30 day) and now()) as `Screen_Usage_Count` FROM Dashboard_Performance_Campaign_Insights
UNION
SELECT 'INTELLIGENCE - ATTR. COMBO ANALYSIS' AS `Screen_Name`, date_format(MAX(Created_Date),'%d-%b-%Y %H:%i') AS `Refresh_Date`,(select COUNT(*) FROM log_solus_dashboard where module = 'S_dashboard_performance_whatif_analysis_N_Attr' and Created_Date between DATE_SUB( now(), INTERVAL 30 day) and now()) as `Screen_Usage_Count` FROM Dashboard_Performance_Campaign_Insights
UNION
SELECT 'INTELLIGENCE - ATTR. SUMMARY' AS `Screen_Name`, date_format(MAX(Created_Date),'%d-%b-%Y %H:%i') AS `Refresh_Date`,(select COUNT(*) FROM log_solus_dashboard where module = 'S_dashboard_performance_whatif_analysis_Attr_Summary' and Created_Date between DATE_SUB( now(), INTERVAL 30 day) and now()) as `Screen_Usage_Count` FROM Dashboard_Performance_Campaign_Insights;

IF IN_AGGREGATION_1 = 'PERFORMANCE' AND IN_AGGREGATION_2 = ''
 		THEN 
                
                Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Screen_Name",REPLACE(`Screen_Name`,"PERFORMANCE - ",""),"Metrics_Refresh_Date",ifnull(`Refresh_Date`,"Not Refreshed Yet"),"Screen_Usage_Count",`Screen_Usage_Count`) )),"]")into @temp_result from V_Dashboard_Metrics_Status 
                WHERE Screen_Name LIKE "PERFORMANCE%" ;') ;
													  
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 

						
END IF;

IF IN_AGGREGATION_1 = 'INSIGHTS' AND IN_AGGREGATION_2 = ''
 		THEN 
                
                Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Screen_Name",REPLACE(`Screen_Name`,"INSIGHTS - ",""),"Metrics_Refresh_Date",ifnull(`Refresh_Date`,"Not Refreshed Yet"),"Screen_Usage_Count",`Screen_Usage_Count`) )),"]")into @temp_result from V_Dashboard_Metrics_Status 
                WHERE Screen_Name LIKE "INSIGHTS%" ;') ;
													  
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 

						
END IF;

IF IN_AGGREGATION_1 = 'INTELLIGENCE' AND IN_AGGREGATION_2 = ''
 		THEN 
                
                Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Screen_Name",REPLACE(`Screen_Name`,"INTELLIGENCE - ",""),"Metrics_Refresh_Date",ifnull(`Refresh_Date`,"Not Refreshed Yet"),"Screen_Usage_Count",`Screen_Usage_Count`) )),"]")into @temp_result from V_Dashboard_Metrics_Status 
                WHERE Screen_Name LIKE "INTELLIGENCE%" ;') ;
													  
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 

						
END IF;

/*
IF IN_AGGREGATION_1 = 'PERFORMANCE - OUTREACHES' AND IN_AGGREGATION_2 = ''
 		THEN 
				
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Metrics_Refresh_Date",ifnull(`Refresh_Date`,"Metrics Refresh in Progress"),"Screen_Usage_Count",`Screen_Usage_Count`) )),"]")into @temp_result from V_Dashboard_Metrics_Status 
                WHERE Screen_Name = "PERFORMANCE - OUTREACHES" ;') ;
													  
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 
						
END IF;

IF IN_AGGREGATION_1 = 'PERFORMANCE - IMPACT OVERVIEW' AND IN_AGGREGATION_2 = ''
 		THEN 
				
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Metrics_Refresh_Date",ifnull(`Refresh_Date`,"Metrics Refresh in Progress"),"Screen_Usage_Count",`Screen_Usage_Count`) )),"]")into @temp_result from V_Dashboard_Metrics_Status 
                WHERE Screen_Name = "PERFORMANCE - IMPACT OVERVIEW" ;') ;
													  
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 
						
END IF;
    
IF IN_AGGREGATION_1 = 'PERFORMANCE - LIFT DRILLDOWN' AND IN_AGGREGATION_2 = ''
 		THEN 
				
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Metrics_Refresh_Date",ifnull(`Refresh_Date`,"Metrics Refresh in Progress"),"Screen_Usage_Count",`Screen_Usage_Count`) )),"]")into @temp_result from V_Dashboard_Metrics_Status 
                WHERE Screen_Name = "PERFORMANCE - LIFT DRILLDOWN" ;') ;
													  
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 
						
END IF;

IF IN_AGGREGATION_1 = 'PERFORMANCE - MONTH REPORT' AND IN_AGGREGATION_2 = ''
 		THEN 
				
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Metrics_Refresh_Date",ifnull(`Refresh_Date`,"Metrics Refresh in Progress"),"Screen_Usage_Count",`Screen_Usage_Count`) )),"]")into @temp_result from V_Dashboard_Metrics_Status 
                WHERE Screen_Name = "PERFORMANCE - MONTH REPORT" ;') ;
													  
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 
						
END IF;

IF IN_AGGREGATION_1 = 'PERFORMANCE - DEEP DIVE' AND IN_AGGREGATION_2 = ''
 		THEN 
				
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Metrics_Refresh_Date",ifnull(`Refresh_Date`,"Metrics Refresh in Progress"),"Screen_Usage_Count",`Screen_Usage_Count`) )),"]")into @temp_result from V_Dashboard_Metrics_Status 
                WHERE Screen_Name = "PERFORMANCE - DEEP DIVE" ;') ;
													  
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 
						
END IF;

IF IN_AGGREGATION_1 = 'PERFORMANCE - FUNNEL REPORT' AND IN_AGGREGATION_2 = ''
 		THEN 
				
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Metrics_Refresh_Date",ifnull(`Refresh_Date`,"Metrics Refresh in Progress"),"Screen_Usage_Count",`Screen_Usage_Count`) )),"]")into @temp_result from V_Dashboard_Metrics_Status 
                WHERE Screen_Name = "PERFORMANCE - FUNNEL REPORT" ;') ;
													  
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 
						
END IF;

IF IN_AGGREGATION_1 = 'PERFORMANCE - CAMPAIGNS' AND IN_AGGREGATION_2 = ''
 		THEN 
				
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Metrics_Refresh_Date",ifnull(`Refresh_Date`,"Metrics Refresh in Progress"),"Screen_Usage_Count",`Screen_Usage_Count`) )),"]")into @temp_result from V_Dashboard_Metrics_Status 
                WHERE Screen_Name = "PERFORMANCE - CAMPAIGNS" ;') ;
													  
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 
						
END IF;

IF IN_AGGREGATION_1 = 'PERFORMANCE - LIFT DETAIL' AND IN_AGGREGATION_2 = ''
 		THEN 
				
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Metrics_Refresh_Date",ifnull(`Refresh_Date`,"Metrics Refresh in Progress"),"Screen_Usage_Count",`Screen_Usage_Count`) )),"]")into @temp_result from V_Dashboard_Metrics_Status 
                WHERE Screen_Name = "PERFORMANCE - LIFT DETAIL" ;') ;
													  
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 
						
END IF;

IF IN_AGGREGATION_1 = 'PERFORMANCE - DAY REPORT' AND IN_AGGREGATION_2 = ''
 		THEN 
				
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Metrics_Refresh_Date",ifnull(`Refresh_Date`,"Metrics Refresh in Progress"),"Screen_Usage_Count",`Screen_Usage_Count`) )),"]")into @temp_result from V_Dashboard_Metrics_Status 
                WHERE Screen_Name = "PERFORMANCE - DAY REPORT" ;') ;
													  
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 
						
END IF;

IF IN_AGGREGATION_1 = 'PERFORMANCE - MONTHLY PERFORMANCE' AND IN_AGGREGATION_2 = ''
 		THEN 
				
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Metrics_Refresh_Date",ifnull(`Refresh_Date`,"Metrics Refresh in Progress"),"Screen_Usage_Count",`Screen_Usage_Count`) )),"]")into @temp_result from V_Dashboard_Metrics_Status 
                WHERE Screen_Name = "PERFORMANCE - MONTHLY PERFORMANCE" ;') ;
													  
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 
						
END IF;

IF IN_AGGREGATION_1 = 'PERFORMANCE - YIELD COVERAGE' AND IN_AGGREGATION_2 = ''
 		THEN 
				
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Metrics_Refresh_Date",ifnull(`Refresh_Date`,"Metrics Refresh in Progress"),"Screen_Usage_Count",`Screen_Usage_Count`) )),"]")into @temp_result from V_Dashboard_Metrics_Status 
                WHERE Screen_Name = "PERFORMANCE - YIELD COVERAGE" ;') ;
													  
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 
						
END IF;

IF IN_AGGREGATION_1 = 'PERFORMANCE - CONVERSION' AND IN_AGGREGATION_2 = ''
 		THEN 
				
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Metrics_Refresh_Date",ifnull(`Refresh_Date`,"Metrics Refresh in Progress"),"Screen_Usage_Count",`Screen_Usage_Count`) )),"]")into @temp_result from V_Dashboard_Metrics_Status 
                WHERE Screen_Name = "PERFORMANCE - CONVERSION" ;') ;
													  
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 
						
END IF;

IF IN_AGGREGATION_1 = 'PERFORMANCE - TRIGGER CONVERSION' AND IN_AGGREGATION_2 = ''
 		THEN 
				
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Metrics_Refresh_Date",ifnull(`Refresh_Date`,"Metrics Refresh in Progress"),"Screen_Usage_Count",`Screen_Usage_Count`) )),"]")into @temp_result from V_Dashboard_Metrics_Status 
                WHERE Screen_Name = "PERFORMANCE - TRIGGER CONVERSION" ;') ;
													  
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 
						
END IF;

IF IN_AGGREGATION_1 = 'INSIGHTS - CUSTOMER DISTRIBUTION' AND IN_AGGREGATION_2 = ''
 		THEN 
				
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Metrics_Refresh_Date",ifnull(`Refresh_Date`,"Metrics Refresh in Progress"),"Screen_Usage_Count",`Screen_Usage_Count`) )),"]")into @temp_result from V_Dashboard_Metrics_Status 
                WHERE Screen_Name = "INSIGHTS - CUSTOMER DISTRIBUTION" ;') ;
													  
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 
						
END IF;

IF IN_AGGREGATION_1 = 'INSIGHTS - BUSINESS OVERVIEW' AND IN_AGGREGATION_2 = ''
 		THEN 
				
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Metrics_Refresh_Date",ifnull(`Refresh_Date`,"Metrics Refresh in Progress"),"Screen_Usage_Count",`Screen_Usage_Count`) )),"]")into @temp_result from V_Dashboard_Metrics_Status 
                WHERE Screen_Name = "INSIGHTS - BUSINESS OVERVIEW" ;') ;
													  
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 
						
END IF;

IF IN_AGGREGATION_1 = 'INSIGHTS - BASE' AND IN_AGGREGATION_2 = ''
 		THEN 
				
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Metrics_Refresh_Date",ifnull(`Refresh_Date`,"Metrics Refresh in Progress"),"Screen_Usage_Count",`Screen_Usage_Count`) )),"]")into @temp_result from V_Dashboard_Metrics_Status 
                WHERE Screen_Name = "INSIGHTS - BASE" ;') ;
													  
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 
						
END IF;

IF IN_AGGREGATION_1 = 'INSIGHTS - MONTH BASE' AND IN_AGGREGATION_2 = ''
 		THEN 
				
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Metrics_Refresh_Date",ifnull(`Refresh_Date`,"Metrics Refresh in Progress"),"Screen_Usage_Count",`Screen_Usage_Count`) )),"]")into @temp_result from V_Dashboard_Metrics_Status 
                WHERE Screen_Name = "INSIGHTS - MONTH BASE" ;') ;
													  
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 
						
END IF;

IF IN_AGGREGATION_1 = 'INSIGHTS - SEGMENTS' AND IN_AGGREGATION_2 = ''
 		THEN 
				
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Metrics_Refresh_Date",ifnull(`Refresh_Date`,"Metrics Refresh in Progress"),"Screen_Usage_Count",`Screen_Usage_Count`) )),"]")into @temp_result from V_Dashboard_Metrics_Status 
                WHERE Screen_Name = "INSIGHTS - SEGMENTS" ;') ;
													  
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 
						
END IF;

IF IN_AGGREGATION_1 = 'INSIGHTS - CRM KPI' AND IN_AGGREGATION_2 = ''
 		THEN 
				
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Metrics_Refresh_Date",ifnull(`Refresh_Date`,"Metrics Refresh in Progress"),"Screen_Usage_Count",`Screen_Usage_Count`) )),"]")into @temp_result from V_Dashboard_Metrics_Status 
                WHERE Screen_Name = "INSIGHTS - CRM KPI" ;') ;
													  
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 
						
END IF;

IF IN_AGGREGATION_1 = 'INSIGHTS - BOUNCE CURVE' AND IN_AGGREGATION_2 = ''
 		THEN 
				
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Metrics_Refresh_Date",ifnull(`Refresh_Date`,"Metrics Refresh in Progress"),"Screen_Usage_Count",`Screen_Usage_Count`) )),"]")into @temp_result from V_Dashboard_Metrics_Status 
                WHERE Screen_Name = "INSIGHTS - BOUNCE CURVE" ;') ;
													  
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 
						
END IF;

IF IN_AGGREGATION_1 = 'INSIGHTS - GLUE NUMBER' AND IN_AGGREGATION_2 = ''
 		THEN 
				
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Metrics_Refresh_Date",ifnull(`Refresh_Date`,"Metrics Refresh in Progress"),"Screen_Usage_Count",`Screen_Usage_Count`) )),"]")into @temp_result from V_Dashboard_Metrics_Status 
                WHERE Screen_Name = "INSIGHTS - GLUE NUMBER" ;') ;
													  
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 
						
END IF;

IF IN_AGGREGATION_1 = 'INSIGHTS - REPEAT COHORTS' AND IN_AGGREGATION_2 = ''
 		THEN 
				
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Metrics_Refresh_Date",ifnull(`Refresh_Date`,"Metrics Refresh in Progress"),"Screen_Usage_Count",`Screen_Usage_Count`) )),"]")into @temp_result from V_Dashboard_Metrics_Status 
                WHERE Screen_Name = "INSIGHTS - REPEAT COHORTS" ;') ;
													  
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 
						
END IF;

IF IN_AGGREGATION_1 = 'INSIGHTS - GLUE COHORTS' AND IN_AGGREGATION_2 = ''
 		THEN 
				
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Metrics_Refresh_Date",ifnull(`Refresh_Date`,"Metrics Refresh in Progress"),"Screen_Usage_Count",`Screen_Usage_Count`) )),"]")into @temp_result from V_Dashboard_Metrics_Status 
                WHERE Screen_Name = "INSIGHTS - GLUE COHORTS" ;') ;
													  
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 
						
END IF;

IF IN_AGGREGATION_1 = 'INSIGHTS - SEGMENT MOVEMENT' AND IN_AGGREGATION_2 = ''
 		THEN 
				
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Metrics_Refresh_Date",ifnull(`Refresh_Date`,"Metrics Refresh in Progress"),"Screen_Usage_Count",`Screen_Usage_Count`) )),"]")into @temp_result from V_Dashboard_Metrics_Status 
                WHERE Screen_Name = "INSIGHTS - SEGMENT MOVEMENT" ;') ;
													  
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 
						
END IF;

 /*   
if IN_AGGREGATION_1 = 'Metrics_Status_List' 
 		THEN 
		
        select Screen_Name into @Screen from Dashboard_Metrics_Log where Screen_Name = IN_AGGREGATION_2 group by 1 order by ID Desc limit 1;
    select `Status` into @Refresh_Status from Dashboard_Metrics_Log where Screen_Name = IN_AGGREGATION_2 group by 1 order by ID Desc limit 1;
    
    IF @Refresh_Status = 'Started' THEN 
				
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Disable","Screen_Name",`Screen_Name`,"Metrics_Refresh_Date",`Refresh_Date`) )),"]")into @temp_result from V_Dashboard_Metrics_Status ;') ;
													  
				SELECT @Query_String;
								PREPARE statement from @Query_String;
								Execute statement;
								Deallocate PREPARE statement; 
                                
	ELSEIF @Refresh_Status = 'Ended' THEN 
				
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Enable","Screen_Name",`Screen_Name`,"Metrics_Refresh_Date",`Refresh_Date`) )),"]")into @temp_result from V_Dashboard_Metrics_Status ;') ;
													  
				SELECT @Query_String;
								PREPARE statement from @Query_String;
								Execute statement;
								Deallocate PREPARE statement; 
													  
	END IF;

END IF;
*/
/*
if IN_AGGREGATION_1 = 'Metrics_Status_List' AND IN_AGGREGATION_2 <> ''
 		THEN 
		
        select Screen_Name into @Screen from Dashboard_Metrics_Log where Screen_Name = IN_AGGREGATION_2 group by 1 order by ID Desc limit 1;
    select `Status` into @Refresh_Status from Dashboard_Metrics_Log where Screen_Name = IN_AGGREGATION_2 group by 1 order by ID Desc limit 1;
    
    IF @Refresh_Status = 'Started' THEN 
				
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Disable","Screen_Name",`Screen_Name`,"Metrics_Refresh_Date",`Refresh_Date`) )),"]")into @temp_result from V_Dashboard_Metrics_Status where Screen_Name = ',IN_AGGREGATION_2,' ;') ;
													  
				SELECT @Query_String;
								PREPARE statement from @Query_String;
								Execute statement;
								Deallocate PREPARE statement; 
                                
	ELSEIF @Refresh_Status = 'Ended' THEN 
				
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Enable","Screen_Name",`Screen_Name`,"Metrics_Refresh_Date",`Refresh_Date`) )),"]")into @temp_result from V_Dashboard_Metrics_Status ;') ;
													  
				SELECT @Query_String;
								PREPARE statement from @Query_String;
								Execute statement;
								Deallocate PREPARE statement; 

													  
	END IF;

END IF;
*/

if IN_AGGREGATION_1 = 'Refresh_Metrics_Status' AND IN_AGGREGATION_2 <> ''
 		THEN 
        
	select Screen_Name into @Screen from Dashboard_Metrics_Log where Screen_Name = IN_AGGREGATION_2 group by 1 order by ID Desc limit 1;
    select `Status` into @Refresh_Status from Dashboard_Metrics_Log where Screen_Name = IN_AGGREGATION_2 group by 1 order by ID Desc limit 1;
    
    select @Screen, @Refresh_Status;
    
    IF @Refresh_Status = 'Started' THEN 
    
    Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Disable", "Status","Refreshing") )),"]") into @temp_result                       
												  ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
                            
    ELSE IF @Refresh_Status = 'Ended' THEN
    
    Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Enable") )),"]") into @temp_result                       
												  ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;
                            
	ELSE IF @Refresh_Status = 'Failed' THEN
    
    Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Execution Failed") )),"]") into @temp_result                       
												  ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;
                            
	END IF;
                            
	END IF;
    
    END IF;

END IF;

	If @temp_result is null
		Then
								
							Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Metrics execution never done for ',IN_AGGREGATION_2,'") )),"]") into @temp_result                       
												  ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
						
	End if;
							

    SET out_result_json = @temp_result;                                          
  


END$$
DELIMITER ;
