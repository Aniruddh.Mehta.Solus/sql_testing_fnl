
CREATE or replace PROCEDURE `S_UI_static_so2_generator`(IN in_request_json json, OUT out_result_json json)
BEGIN
    
    

    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
    declare EDIT Bigint; 
    

	declare website_url text;
    declare AGGREGATION_1 text;
	declare UTM_Channel varchar(128);
    declare UTM_Campaign varchar(128);
    declare UTM_Source varchar(128);
    
	SET SQL_SAFE_UPDATES=0;
	SET foreign_key_checks=0;

	Set in_request_json = Replace(in_request_json,'\n','\\n');
  
   
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));

	
    
SET AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
    SET website_url = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
    SET UTM_Campaign = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
    SET UTM_Channel = json_unquote(json_extract(in_request_json,"$.AGGREGATION_4"));
SET UTM_Source = json_unquote(json_extract(in_request_json,"$.AGGREGATION_5"));

	Set @temp_result =Null;
    Set @vSuccess ='';
	Set @vErrMsg='';
	Set @vSuccess='';
	Set @Err =0;
    set @url=website_url;
	set sql_safe_updates=0;
	set @UTM_Campaign = UTM_Campaign;
    set @UTM_Channel = UTM_Channel;
    set @UTM_Source = UTM_Source;
    
    if   AGGREGATION_1 = 'Config'
    then
    
    update M_Config
    set `Value`= @url
    where `Name` = 'Shorten_URL_Prefix';
    
    Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","so2 configured successfully") )),"]") 
														into @temp_result                       
												      ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
    
    
        elseif AGGREGATION_1 = 'display_config'
 then   
		select `value` into @field1 from M_Config where `Name` = 'Shorten_URL_Prefix';
    
     Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message",','"',@field1,'"',') )),"]") 
														into @temp_result                       
												      ;') ;
   
     

   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
    
    elseif AGGREGATION_1 = 'display_list'
 then   
		
    SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Website_URL",Website_URL,"Shorten_URL",Shorten_URL,"UTM_Campaign",UTM_Campaign,"UTM_Channel",UTM_Channel,"UTM_Source",UTM_Source))),"]")
				    into @temp_result from so2_shorten_URL_mapping order by Modified_Date desc;') ;
                    

   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
    
    elseif AGGREGATION_1 = 'Generator'
    then
    
      SELECT 
    `Value`
INTO @url_prefix FROM
    M_Config
WHERE
    `Name` = 'Shorten_URL_Prefix';
      
	if @url != '' 
    then
    
    insert ignore into so2_shorten_URL_mapping(Website_URL) select @url;
  
  UPDATE so2_shorten_URL_mapping 
SET 
	URL_Code = CONVERTINTEGERTOBASE36(Id)
    where 
    Website_URL = @url;
    
 set @UpdateQuery1=concat('
UPDATE so2_shorten_URL_mapping 
SET 
    Shorten_URL = CONCAT(','"',@url_prefix,'"',', URL_Code),
    UTM_Campaign = ','"',@UTM_Campaign,'"',',
    UTM_Channel = ','"',@UTM_Channel,'"',',
    UTM_Source = ','"',@UTM_Source,'"','
WHERE
    Website_URL = ','"',@url,'"',';');
           

      PREPARE statement from @UpdateQuery1;
							Execute statement;
							Deallocate PREPARE statement; 
     
   
   set @Query1=concat('select Shorten_URL into @field1 from so2_shorten_URL_mapping where Website_URL = ','"',@url,'"',';');
   PREPARE statement from @Query1;
							Execute statement;
							Deallocate PREPARE statement; 
	
        	Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message",','"',@field1,'"',') )),"]") 
														into @temp_result                       
												      ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
                    
             else
             
             Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","URL is Empty") )),"]") 
														into @temp_result                       
												      ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
	end if;
    
    end if;
    
    if @temp_result is null
		Then
								
							Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Invalid Parameters") )),"]") 
														into @temp_result                       
												      ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
						
	End if;
							
    SET out_result_json = @temp_result;


END

