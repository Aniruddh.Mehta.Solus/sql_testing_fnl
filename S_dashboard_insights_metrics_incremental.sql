DELIMITER $$
CREATE or replace PROCEDURE `S_dashboard_insights_metrics_incremental`(IN vBatchSize bigint(10), in vStartCntInput bigint(10), IN vAPMonth int(2),IN vLapserMonth int(2), IN vFYMonth int(2))
BEGIN
DECLARE vStart_Cnt 		BIGINT DEFAULT 0;
DECLARE vEnd_Cnt 		BIGINT;
DECLARE vCurYYYYMM 		int(6);
DECLARE vCurYYYY 		int(4);
DECLARE vYYYYMM_StartMonth 		int(6);
DECLARE vCheckEndOfCursor int Default 0;

DECLARE curALL_MONTHS CURSOR FOR 
SELECT DISTINCT 
	Bill_Year_Month
FROM
	CDM_Bill_Details
where Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt
and Bill_Year_Month >= vYYYYMM_StartMonth 
ORDER BY Bill_Year_Month ASC;
/*
DECLARE curALL_MONTHS CURSOR FOR 
SELECT YM FROM
(select distinct YYYYMM AS YM from Dashboard_Insights_Base order by YYYYMM DESC LIMIT 2)
ORDER BY YM ASC;
*/
DECLARE CONTINUE HANDLER FOR NOT FOUND SET vCheckEndOfCursor=1;
SET SQL_SAFE_UPDATES=0;
Delete from log_solus where module='S_dashboard_insights_metrics';
select distinct YYYYMM into vYYYYMM_StartMonth from Dashboard_Insights_Base order by YYYYMM DESC LIMIT 1,1;
select vYYYYMM_StartMonth;
SELECT 
    Customer_Id
INTO @Rec_Cnt FROM
    CDM_Customer_Master
ORDER BY Customer_Id DESC
LIMIT 1;
insert into log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_insights_metrics',-1,-1,'INITIALIZE');
select 'Version No : 17-Apr 11.42 PM' as '';
select 'select * from log_solus where module=\'S_dashboard_insights_metrics\' order by id desc;' as '';

INSERT INTO Dashboard_Metrics_Log
(
`Screen_Name`,
`Status`
)
VALUES
('INSIGHTS - DASHBOARD',
'Started');

CREATE OR REPLACE TABLE All_Dates as select distinct bill_date from CDM_Bill_Details;
CREATE OR REPLACE TABLE All_Revenue_Center as select distinct Revenue_Center from CDM_Revenue_Center;
CREATE OR REPLACE TABLE All_CLMSegmentName as select distinct CLMSegmentName from CDM_Customer_Segment;
delete from All_Revenue_Center where Revenue_Center='NA';
insert into All_Revenue_Center values ('NA');
delete from All_CLMSegmentName where CLMSegmentName='NA';
insert into All_CLMSegmentName values ('NA');
CREATE OR REPLACE TABLE Dashboard_Insights_Base_CURRENTRUN LIKE Dashboard_Insights_Base;

INSERT INTO Dashboard_Insights_Base_CURRENTRUN
SELECT * from  Dashboard_Insights_Base;

select distinct YYYYMM into @last_month from Dashboard_Insights_Base order by YYYYMM DESC LIMIT 1;
select distinct YYYYMM into @last2nd_month from Dashboard_Insights_Base order by YYYYMM DESC LIMIT 1,1;

SELECT @last_month,@last2nd_month;
delete from Dashboard_Insights_Base_CURRENTRUN where YYYYMM in (@last_month,@last2nd_month);
select distinct YYYYMM from Dashboard_Insights_Base_CURRENTRUN order by YYYYMM DESC LIMIT 2;

INSERT IGNORE INTO Dashboard_Insights_Base_CURRENTRUN
		(
			AGGREGATION,
			YYYYMMDD,
			YYYYMM,
			YYYY,
			Revenue_Center,
            CLMSegmentName
		)
SELECT
	'BY_DAY',
	date_format(bill_date,'%Y%m%d') as YYYYMMDD,
	date_format(bill_date,'%Y%m') as YYYYMM,
	date_format(bill_date,'%Y') as YYYY,
	Revenue_Center,
	CLMSegmentName
FROM All_Dates, All_Revenue_Center, All_CLMSegmentName;
INSERT IGNORE INTO Dashboard_Insights_Base_CURRENTRUN
		(
			AGGREGATION,
			YYYYMMDD,
			YYYYMM,
			YYYY,
			Revenue_Center,
            CLMSegmentName
		)
SELECT
	'BY_MONTH',
	-1 as YYYYMMDD,
	date_format(bill_date,'%Y%m') as YYYYMM,
	date_format(bill_date,'%Y') as YYYY,
	Revenue_Center,
	CLMSegmentName
FROM All_Dates, All_Revenue_Center, All_CLMSegmentName;
   
INSERT IGNORE INTO Dashboard_Insights_Base_CURRENTRUN
		(
			AGGREGATION,
			YYYYMMDD,
			YYYYMM,
			YYYY,
			Revenue_Center,
            CLMSegmentName
		)
SELECT
	'BY_YEAR',
	-1 as YYYYMMDD,
	-1 as YYYYMM,
	date_format(bill_date,'%Y') as YYYY,
	Revenue_Center,
	CLMSegmentName
FROM All_Dates, All_Revenue_Center, All_CLMSegmentName;

insert into log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_insights_metrics',-1,-1,'Dashboard Metrics Initialized');

-- SET vStart_Cnt=0;
SET vStart_Cnt=vStartCntInput;
LOOP_ALL_CUSTOMERS: LOOP
	SET vEnd_Cnt = vStart_Cnt + vBatchSize;
	IF vStart_Cnt  >= @Rec_Cnt THEN
		LEAVE LOOP_ALL_CUSTOMERS;
	END IF;  
    
	select vStart_Cnt, vEnd_Cnt, current_timestamp();
    insert into log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_insights_metrics',vStart_Cnt,vEnd_Cnt,'STARTING BATCH');
    
	TRUNCATE Dashboard_CDM_Bill_Details_ForOneBatch;
		INSERT INTO Dashboard_CDM_Bill_Details_ForOneBatch 
	(
	YYYYMMDD,
	YYYYMM,
	YYYY,
	Customer_Id,
	Lineitems,
	Bills, 
	Visits, 
	Revenue, 
	Discount
	)
	SELECT
		date_format(bill_date, '%Y%m%d') 			as YYYYMMDD,
		Bill_Year_Month 		as YYYYMM,
		Bill_Year  				as YYYY,
		bill.Customer_Id		as Customer_Id,
		count(distinct bill.Bill_Details_Id) 		as Lineitems,
		count(distinct bill.Bill_Header_Id) 		as Bills,
		count(distinct bill.Bill_Date) 				as Visits,
		sum(bill.Sale_Net_Val) 						as Revenue,
		sum(bill.Sale_Disc_Val) 					as Discount
	FROM
		CDM_Bill_Details bill
	WHERE
		bill.Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt
		and bill_date is not null
	group by Customer_Id, bill_date;

	IF (select Value1 from UI_Configuration_Global where Config_Name = "Override_Insights_Revenue_From_BillHeader")="Y"
	THEN
				TRUNCATE Dashboard_CDM_Bill_Header_ForOneBatch;
		
        INSERT INTO Dashboard_CDM_Bill_Header_ForOneBatch
        (select Customer_Id,CAST(date_format(bill_date, '%Y%m%d') AS SIGNED) as YYYYMMDD,SUM(Bill_Total_Val) as Revenue,SUM(Bill_Disc) AS Discount from CDM_Bill_Header where Customer_Id between vStart_Cnt AND vEnd_Cnt
		and Bill_Date is not null
        group by Customer_Id, Bill_Date);
        
        UPDATE Dashboard_CDM_Bill_Details_ForOneBatch a,Dashboard_CDM_Bill_Header_ForOneBatch b
        SET a.Revenue=b.Revenue,
        a.Discount=b.Discount
        where a.Customer_Id=b.Customer_Id
        and a.YYYYMMDD=b.YYYYMMDD;
    END IF;
    
	insert into log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_insights_metrics',vStart_Cnt,vEnd_Cnt,'Load Bills');
    
    UPDATE Dashboard_CDM_Bill_Details_ForOneBatch dashboard
    LEFT JOIN CDM_Revenue_Center revcenter ON dashboard.Customer_Id=revcenter.Customer_Id
    SET dashboard.Revenue_Center=ifnull(revcenter.Revenue_Center,'NA');

	UPDATE Dashboard_CDM_Bill_Details_ForOneBatch dashboard 
    LEFT JOIN CDM_Customer_Segment seg ON dashboard.Customer_Id=seg.Customer_Id
    SET dashboard.CLMSegmentName=ifnull(seg.CLMSegmentName,'NA');
    
    insert into log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_insights_metrics',vStart_Cnt,vEnd_Cnt,'Load Revenue Center and CLM Segment');

	SET vCheckEndOfCursor=0;
	OPEN curALL_MONTHS;
	LOOP_ALL_MONTHS : LOOP
		FETCH curALL_MONTHS into vCurYYYYMM;
		IF vCheckEndOfCursor THEN    
			LEAVE LOOP_ALL_MONTHS;
		END IF;
        
		set vCurYYYY			=	substring(vCurYYYYMM,1,4);
		set @vAPYYYYMM			=	F_Month_Diff(vCurYYYYMM,(vAPMonth-1));
		set @vLapserYYYYMM		=	F_Month_Diff(vCurYYYYMM,(vLapserMonth-1));
		set @vFYYYYYMM			=	F_Month_Diff(vCurYYYYMM,(vFYMonth-1)); 
		set @vQtrYYYYMM			=	F_Month_Diff(vCurYYYYMM,2);
		set @v12MthsYYYYMM		=	F_Month_Diff(vCurYYYYMM,11);
	insert into log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_insights_metrics',vStart_Cnt,vCurYYYYMM,'BY_MONTH Base');
	UPDATE Dashboard_Insights_Base_CURRENTRUN dashboard,
    (SELECT 
        'Base',
            COUNT(distinct Customer_Id) AS count,
            SUM(Revenue) AS Revenuex,
            SUM(Discount) AS Discountx,
            SUM(Bills) AS Billsx,
			SUM(Lineitems) AS Lineitems,
            SUM(Visits) AS visitsx,
            Revenue_Center,
            CLMSegmentName
    FROM
        Dashboard_CDM_Bill_Details_ForOneBatch
    WHERE
        YYYYMM <= vCurYYYYMM
    GROUP BY Revenue_Center, CLMSegmentName) temp 
	SET 
		dashboard.Base = dashboard.Base + temp.count,
		dashboard.BaseRevenue = dashboard.BaseRevenue + temp.Revenuex,
		dashboard.BaseDiscount = dashboard.BaseDiscount + temp.Discountx,
        dashboard.BaseLineitems = dashboard.BaseLineitems + temp.Lineitems,
		dashboard.BaseBills = dashboard.BaseBills + temp.Billsx,
		dashboard.BaseVisits = dashboard.BaseVisits + temp.Visitsx
	WHERE
    dashboard.YYYYMM = vCurYYYYMM
        AND AGGREGATION = 'BY_MONTH'
        AND dashboard.Revenue_Center = temp.Revenue_Center
        AND dashboard.CLMSegmentName = temp.CLMSegmentName;
        
	insert into log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_insights_metrics',vStart_Cnt,vCurYYYYMM,'BY_MONTH MonthlyActive');

	UPDATE Dashboard_Insights_Base_CURRENTRUN dashboard,
    (SELECT 
        'MonthlyActive',
            COUNT(DISTINCT Customer_Id) AS count,
            SUM(Revenue) AS Revenuex,
            SUM(Discount) AS Discountx,
			SUM(Lineitems) AS Lineitems,
            SUM(Bills) AS Billsx,
            SUM(Visits) AS visitsx,
            Revenue_Center,
            CLMSegmentName
    FROM
        Dashboard_CDM_Bill_Details_ForOneBatch
    WHERE
       YYYYMM = vCurYYYYMM
    GROUP BY Revenue_Center , CLMSegmentName) temp 
	SET 
		dashboard.MonthlyActive 		= dashboard.MonthlyActive 			+ temp.count,
		dashboard.MonthlyActiveRevenue 	= dashboard.MonthlyActiveRevenue	+ temp.Revenuex,
		dashboard.MonthlyActiveDiscount = dashboard.MonthlyActiveDiscount 	+ temp.Discountx,
        dashboard.MonthlyActiveLineitems = dashboard.MonthlyActiveLineitems + temp.Lineitems,
		dashboard.MonthlyActiveBills 	= dashboard.MonthlyActiveBills 		+ temp.Billsx,
		dashboard.MonthlyActiveVisits 	= dashboard.MonthlyActiveVisits 	+ temp.Visitsx
	WHERE
    dashboard.YYYYMM = vCurYYYYMM
        AND AGGREGATION = 'BY_MONTH'
        AND dashboard.Revenue_Center = temp.Revenue_Center
        AND dashboard.CLMSegmentName = temp.CLMSegmentName;

	insert into log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_insights_metrics',vStart_Cnt,vCurYYYYMM,'BY_MONTH ActiveBase');

	UPDATE Dashboard_Insights_Base_CURRENTRUN dashboard,
    (SELECT 
        'ActiveBase',
            COUNT(DISTINCT Customer_Id) AS count,
            Revenue_Center,
            CLMSegmentName
    FROM
        Dashboard_CDM_Bill_Details_ForOneBatch
    WHERE YYYYMM BETWEEN @vAPYYYYMM AND vCurYYYYMM 
    GROUP BY Revenue_Center , CLMSegmentName) temp 
	SET 
		dashboard.ActiveBase = dashboard.ActiveBase + temp.count
	WHERE
    dashboard.YYYYMM = vCurYYYYMM
        AND AGGREGATION = 'BY_MONTH'
        AND dashboard.Revenue_Center = temp.Revenue_Center
        AND dashboard.CLMSegmentName = temp.CLMSegmentName;
	insert into log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_insights_metrics',vStart_Cnt,vCurYYYYMM,'BY_MONTH QuarterlyActive');

	UPDATE Dashboard_Insights_Base_CURRENTRUN dashboard,
    (SELECT 
        'QuarterlyActive',
            COUNT(DISTINCT Customer_Id) AS count,
            Revenue_Center,
            CLMSegmentName
    FROM
        Dashboard_CDM_Bill_Details_ForOneBatch
    WHERE  YYYYMM BETWEEN @vQtrYYYYMM AND vCurYYYYMM GROUP BY Revenue_Center , CLMSegmentName) temp 
	SET 
		dashboard.QuarterlyActive = dashboard.QuarterlyActive + temp.count
	WHERE
    dashboard.YYYYMM = vCurYYYYMM
        AND AGGREGATION = 'BY_MONTH'
        AND dashboard.Revenue_Center = temp.Revenue_Center
        AND dashboard.CLMSegmentName = temp.CLMSegmentName;
        
	insert into log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_insights_metrics',vStart_Cnt,vCurYYYYMM,'BY_MONTH T12MthActive');
	UPDATE Dashboard_Insights_Base_CURRENTRUN dashboard,
    (SELECT 
        'T12MthActive',
            COUNT(DISTINCT Customer_Id) AS count,
            SUM(Revenue) AS Revenuex,
            SUM(Discount) AS Discountx,
            SUM(Bills) AS Billsx,
            SUM(Visits) AS visitsx,
            Revenue_Center,
            CLMSegmentName
    FROM
        Dashboard_CDM_Bill_Details_ForOneBatch
    WHERE YYYYMM BETWEEN @v12MthsYYYYMM AND vCurYYYYMM GROUP BY Revenue_Center , CLMSegmentName) temp 
	SET 
		dashboard.T12MthActive 		= dashboard.T12MthActive + temp.count,
		dashboard.T12MthActiveRevenue 	= dashboard.T12MthActiveRevenue	+ temp.Revenuex,
		dashboard.T12MthActiveDiscount 	= dashboard.T12MthActiveDiscount 	+ temp.Discountx,
		dashboard.T12MthActiveBills 	= dashboard.T12MthActiveBills 		+ temp.Billsx,
		dashboard.T12MthActiveVisits 	= dashboard.T12MthActiveVisits 	+ temp.Visitsx
	WHERE
    dashboard.YYYYMM = vCurYYYYMM
        AND AGGREGATION = 'BY_MONTH'
        AND dashboard.Revenue_Center = temp.Revenue_Center
        AND dashboard.CLMSegmentName = temp.CLMSegmentName;

insert into log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_insights_metrics',vStart_Cnt,vCurYYYYMM,'BY_MONTH T12MthNew');

UPDATE Dashboard_Insights_Base_CURRENTRUN dashboard,
    (SELECT 
        'T12MthNew',
            COUNT(DISTINCT Customer_Id) AS count,
            SUM(Revenue) AS Revenue,
            Revenue_Center,
            CLMSegmentName
    FROM Dashboard_CDM_Bill_Details_ForOneBatch a
    WHERE a.YYYYMM BETWEEN @v12MthsYYYYMM AND vCurYYYYMM
            AND NOT EXISTS( SELECT 
                Customer_Id
            FROM
                Dashboard_CDM_Bill_Details_ForOneBatch b
            WHERE
                a.Customer_Id = b.Customer_Id
                    AND b.YYYYMM < @v12MthsYYYYMM)
    GROUP BY Revenue_Center , CLMSegmentName) temp 
	SET 
		dashboard.T12MthNewBase = dashboard.T12MthNewBase + temp.count,
		dashboard.T12MthNewRevenue = dashboard.T12MthNewRevenue + temp.revenue
	WHERE
    dashboard.YYYYMM = vCurYYYYMM
        AND AGGREGATION = 'BY_MONTH'
        AND dashboard.Revenue_Center = temp.Revenue_Center
        AND dashboard.CLMSegmentName = temp.CLMSegmentName;

	insert into log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_insights_metrics',vStart_Cnt,vCurYYYYMM,'BY_MONTH T12MthExisting');

	UPDATE Dashboard_Insights_Base_CURRENTRUN dashboard,
    (SELECT 
        'T12MthExisting',
            COUNT(DISTINCT Customer_Id) AS count,
            SUM(Revenue) AS Revenue,
            Revenue_Center,
            CLMSegmentName
    FROM
        Dashboard_CDM_Bill_Details_ForOneBatch a
    WHERE
       a.YYYYMM BETWEEN @v12MthsYYYYMM AND vCurYYYYMM
            AND EXISTS( SELECT 
                Customer_Id
            FROM
                Dashboard_CDM_Bill_Details_ForOneBatch b
            WHERE
                a.Customer_Id = b.Customer_Id
                    AND b.YYYYMM < @v12MthsYYYYMM)
    GROUP BY Revenue_Center , CLMSegmentName) temp 
	SET 
		dashboard.T12MthExistingBase 	= dashboard.T12MthExistingBase + temp.count,
		dashboard.T12MthExistingRevenue = dashboard.T12MthExistingRevenue + temp.revenue
	WHERE
    dashboard.YYYYMM = vCurYYYYMM
        AND AGGREGATION = 'BY_MONTH'
        AND dashboard.Revenue_Center = temp.Revenue_Center
        AND dashboard.CLMSegmentName = temp.CLMSegmentName;
        
	insert into log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_insights_metrics',vStart_Cnt,vCurYYYYMM,'BY_MONTH T12MthMulti');
	UPDATE Dashboard_Insights_Base_CURRENTRUN dashboard,
		(SELECT 
        'T12MthMulti',
            COUNT(Customer_Id) AS count,
            SUM(Revenue) AS Revenuex,
            SUM(Discount) AS Discountx,
            SUM(Bills) AS Billsx,
            SUM(Visits) AS visitsx,
            Revenue_Center,
            CLMSegmentName
			FROM (
			SELECT 
			Customer_Id,
			SUM(Revenue) AS Revenue,
			SUM(Discount) AS Discount,
			SUM(Bills) AS Bills,
			SUM(Visits) AS Visits,
			YYYYMM,
			Revenue_Center, 
			CLMSegmentName
		FROM
			Dashboard_CDM_Bill_Details_ForOneBatch
		WHERE
			YYYYMM BETWEEN @v12MthsYYYYMM AND vCurYYYYMM
		GROUP BY Customer_Id
		HAVING SUM(visits) > 1) temp1 
		 WHERE YYYYMM BETWEEN @v12MthsYYYYMM AND vCurYYYYMM GROUP BY Revenue_Center, CLMSegmentName) temp 
			SET 
				dashboard.T12MthMulti 		= dashboard.T12MthMulti + temp.count,
				dashboard.T12MthMultiRevenue 	= dashboard.T12MthMultiRevenue	+ temp.Revenuex,
				dashboard.T12MthMultiDiscount 	= dashboard.T12MthMultiDiscount 	+ temp.Discountx,
				dashboard.T12MthMultiBills 	= dashboard.T12MthMultiBills 		+ temp.Billsx,
				dashboard.T12MthMultiVisits 	= dashboard.T12MthMultiVisits 	+ temp.Visitsx
			WHERE
			dashboard.YYYYMM = vCurYYYYMM
				AND AGGREGATION = 'BY_MONTH'
				AND dashboard.Revenue_Center = temp.Revenue_Center
				AND dashboard.CLMSegmentName = temp.CLMSegmentName;

		update Dashboard_Insights_Base_CURRENTRUN dashboard
			SET 
				dashboard.T12MthSingle 				= dashboard.T12MthActive -  dashboard.T12MthMulti,
				dashboard.T12MthSingleRevenue 		= dashboard.T12MthActiveRevenue -  dashboard.T12MthMultiRevenue,
				dashboard.T12MthSingleDiscount 		= dashboard.T12MthActiveDiscount -  dashboard.T12MthMultiDiscount,
				dashboard.T12MthSingleBills 		= dashboard.T12MthActiveBills -  dashboard.T12MthMultiBills,
                dashboard.T12MthSingleVisits 		= dashboard.T12MthActiveVisits -  dashboard.T12MthMultiVisits
		where dashboard.YYYYMM = vCurYYYYMM
				AND AGGREGATION = 'BY_MONTH';
     
 

/*
UPDATE Dashboard_Insights_Base_CURRENTRUN dashboard,
    (SELECT 
        'YTDNew',
            COUNT(DISTINCT Customer_Id) AS count,
            SUM(Revenue) AS Revenue,
            Revenue_Center,
            CLMSegmentName
    FROM Dashboard_CDM_Bill_Details_ForOneBatch a
    WHERE a.YYYYMM = vCurYYYYMM
            AND NOT EXISTS( SELECT 
                Customer_Id
            FROM
                Dashboard_CDM_Bill_Details_ForOneBatch b
            WHERE
                a.Customer_Id = b.Customer_Id
                    AND b.YYYYMM < vCurYYYYMM)
    GROUP BY Revenue_Center , CLMSegmentName) temp 
	SET 
		dashboard.YTDNewBase = dashboard.YTDNewBase + temp.count,
		dashboard.YTDNewRevenue = dashboard.YTDNewRevenue + temp.revenue
	WHERE
    dashboard.YYYYMM = vCurYYYYMM
        AND AGGREGATION = 'BY_MONTH'
        AND dashboard.Revenue_Center = temp.Revenue_Center
        AND dashboard.CLMSegmentName = temp.CLMSegmentName;
*/
        
	insert into log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_insights_metrics',vStart_Cnt,vCurYYYYMM,'BY_MONTH FYBase');

	UPDATE Dashboard_Insights_Base_CURRENTRUN dashboard,
    (SELECT 
        'FYBase',
            COUNT(DISTINCT Customer_Id) AS count,
			SUM(Revenue) AS Revenuex,
            SUM(Discount) AS Discountx,
            SUM(Lineitems) AS Lineitems,
            SUM(Bills) AS Billsx,
            SUM(Visits) AS visitsx,
            Revenue_Center,
            CLMSegmentName
    FROM
        Dashboard_CDM_Bill_Details_ForOneBatch
    WHERE
        YYYYMM BETWEEN @vFYYYYYMM AND vCurYYYYMM
    GROUP BY Revenue_Center , CLMSegmentName) temp 
	SET 
		dashboard.FYBase 			= dashboard.FYBase + temp.count,
		dashboard.FYBaseRevenue 	= dashboard.FYBaseRevenue	+ temp.Revenuex,
		dashboard.FYBaseDiscount 	= dashboard.FYBaseDiscount 	+ temp.Discountx,
        dashboard.FYBaseLineItems 	= dashboard.FYBaseLineItems  	+ temp.LineItems ,
		dashboard.FYBaseBills 		= dashboard.FYBaseBills 	+ temp.Billsx,
		dashboard.FYBaseVisits 		= dashboard.FYBaseVisits 	+ temp.Visitsx
	WHERE
    dashboard.YYYYMM = vCurYYYYMM
        AND AGGREGATION = 'BY_MONTH'
        AND dashboard.Revenue_Center = temp.Revenue_Center
        AND dashboard.CLMSegmentName = temp.CLMSegmentName;
        
	insert into log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_insights_metrics',vStart_Cnt,vCurYYYYMM,'BY_MONTH InactiveBase');

	UPDATE Dashboard_Insights_Base_CURRENTRUN dashboard,
    (SELECT 
        'InactiveBase',
            COUNT(DISTINCT Customer_Id) AS count,
            Revenue_Center,
            CLMSegmentName
    FROM
        Dashboard_CDM_Bill_Details_ForOneBatch a
    WHERE
        a.YYYYMM BETWEEN @vLapserYYYYMM AND @vAPYYYYMM
            AND NOT EXISTS( SELECT 
                Customer_Id
            FROM
                Dashboard_CDM_Bill_Details_ForOneBatch b
            WHERE
                a.Customer_Id = b.Customer_Id
                    AND b.YYYYMM BETWEEN @vAPYYYYMM AND vCurYYYYMM)
    GROUP BY Revenue_Center , CLMSegmentName) temp 
	SET 
		dashboard.InactiveBase = dashboard.InactiveBase + temp.count
	WHERE
    dashboard.YYYYMM = vCurYYYYMM
        AND AGGREGATION = 'BY_MONTH'
        AND dashboard.Revenue_Center = temp.Revenue_Center
        AND dashboard.CLMSegmentName = temp.CLMSegmentName;
        
	insert into log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_insights_metrics',vStart_Cnt,vCurYYYYMM,'BY_MONTH YTDNew');

    UPDATE Dashboard_Insights_Base_CURRENTRUN
    Set LapsedBase=Base-InactiveBase-ActiveBase
    WHERE YYYYMM = vCurYYYYMM
	AND AGGREGATION = 'BY_MONTH';

	UPDATE Dashboard_Insights_Base_CURRENTRUN dashboard,
    (SELECT 
        'YTDNew',
            COUNT(DISTINCT Customer_Id) AS count,
            SUM(Revenue) AS Revenue,
            Revenue_Center,
            CLMSegmentName
    FROM Dashboard_CDM_Bill_Details_ForOneBatch a
    WHERE a.YYYYMM = vCurYYYYMM
            AND NOT EXISTS( SELECT 
                Customer_Id
            FROM
                Dashboard_CDM_Bill_Details_ForOneBatch b
            WHERE
                a.Customer_Id = b.Customer_Id
                    AND b.YYYYMM < vCurYYYYMM)
    GROUP BY Revenue_Center , CLMSegmentName) temp 
	SET 
		dashboard.YTDNewBase = dashboard.YTDNewBase + temp.count,
		dashboard.YTDNewRevenue = dashboard.YTDNewRevenue + temp.revenue
	WHERE
    dashboard.YYYYMM = vCurYYYYMM
        AND AGGREGATION = 'BY_MONTH'
        AND dashboard.Revenue_Center = temp.Revenue_Center
        AND dashboard.CLMSegmentName = temp.CLMSegmentName;

	insert into log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_insights_metrics',vStart_Cnt,vCurYYYYMM,'BY_MONTH YTDExisting');

	UPDATE Dashboard_Insights_Base_CURRENTRUN dashboard,
    (SELECT 
        'YTDExisting',
            COUNT(DISTINCT Customer_Id) AS count,
            SUM(Revenue) AS Revenue,
            Revenue_Center,
            CLMSegmentName
    FROM
        Dashboard_CDM_Bill_Details_ForOneBatch a
    WHERE
        a.YYYYMM = vCurYYYYMM
            AND EXISTS( SELECT 
                Customer_Id
            FROM
                Dashboard_CDM_Bill_Details_ForOneBatch b
            WHERE
                a.Customer_Id = b.Customer_Id
                    AND b.YYYYMM < vCurYYYYMM)
    GROUP BY Revenue_Center , CLMSegmentName) temp 
	SET 
		dashboard.YTDExistingBase 	= dashboard.YTDExistingBase + temp.count,
		dashboard.YTDExistingRevenue = dashboard.YTDExistingRevenue + temp.revenue
	WHERE
    dashboard.YYYYMM = vCurYYYYMM
        AND AGGREGATION = 'BY_MONTH'
        AND dashboard.Revenue_Center = temp.Revenue_Center
        AND dashboard.CLMSegmentName = temp.CLMSegmentName;
	insert into log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_insights_metrics',vStart_Cnt,vCurYYYYMM,'BY_DAY MonthlyActive');       

	UPDATE Dashboard_Insights_Base_CURRENTRUN dashboard,
    (SELECT 
			'BY_DAY' 							as AGGREGATION,
			YYYYMMDD,
			YYYYMM,
			substring(vCurYYYYMM,1,4) 				as YYYY,
			Revenue_Center,
            CLMSegmentName,
			count(distinct Customer_Id) 			as `MonthlyActive`,
			sum(Revenue) 							as `MonthlyActiveRevenue`,
            sum(Lineitems) 							as `MonthlyActiveLineitems`,
			sum(Bills) 								as `MonthlyActiveBills`,
            sum(Visits) 							as `MonthlyActiveVisits`,
			sum(Discount)  							as `MonthlyActiveDiscount`
		FROM Dashboard_CDM_Bill_Details_ForOneBatch bills 
		WHERE bills.YYYYMM = vCurYYYYMM
        GROUP BY YYYYMMDD, Revenue_Center , CLMSegmentName ) temp 
	SET 
		dashboard.MonthlyActive 		= dashboard.MonthlyActive 			+ temp.`MonthlyActive`,
		dashboard.MonthlyActiveRevenue 	= dashboard.MonthlyActiveRevenue	+ temp.`MonthlyActiveRevenue`,
		dashboard.MonthlyActiveDiscount = dashboard.MonthlyActiveDiscount 	+ temp.`MonthlyActiveDiscount`,
		dashboard.MonthlyActiveLineitems = dashboard.MonthlyActiveLineitems + temp.`MonthlyActiveLineitems`,
		dashboard.MonthlyActiveBills 	= dashboard.MonthlyActiveBills 		+ temp.`MonthlyActiveBills`,
		dashboard.MonthlyActiveVisits 	= dashboard.MonthlyActiveVisits 	+ temp.`MonthlyActiveVisits`
	WHERE dashboard.YYYYMMDD = temp.YYYYMMDD
	AND dashboard.AGGREGATION = 'BY_DAY'
	AND dashboard.Revenue_Center = temp.Revenue_Center
    AND dashboard.CLMSegmentName = temp.CLMSegmentName;

	END LOOP LOOP_ALL_MONTHS;
    COMMIT;
	CLOSE curALL_MONTHS;
    
SET vStart_Cnt = vEnd_Cnt;
END LOOP LOOP_ALL_CUSTOMERS; 

CALL S_dashboard_insights_metrics_initialize(vYYYYMM_StartMonth);

select concat('Refreshing Main Table Starts :',current_timestamp());
CREATE OR REPLACE TABLE Dashboard_Insights_Base_LASTRUN LIKE Dashboard_Insights_Base;
TRUNCATE Dashboard_Insights_Base_LASTRUN;
INSERT INTO Dashboard_Insights_Base_LASTRUN SELECT * FROM Dashboard_Insights_Base;
TRUNCATE Dashboard_Insights_Base;
INSERT INTO Dashboard_Insights_Base SELECT * FROM Dashboard_Insights_Base_CURRENTRUN;
select YYYYMM,sum(MonthlyActiveRevenue),sum(MonthlyActive) from Dashboard_Insights_Base_CURRENTRUN group by YYYYMM;
select YYYYMM,sum(MonthlyActiveRevenue),sum(MonthlyActive) from Dashboard_Insights_Base_LASTRUN group by YYYYMM;
select YYYYMM,sum(MonthlyActiveRevenue),sum(MonthlyActive) from Dashboard_Insights_Base group by YYYYMM;

select concat('Refreshing Main Table End Table :',current_timestamp());

UPDATE Dashboard_Metrics_Log
SET `Status` = 'Ended' WHERE Screen_Name = 'INSIGHTS - DASHBOARD';

END$$
DELIMITER ;
