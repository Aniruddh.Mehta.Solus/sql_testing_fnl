DELIMITER $$
CREATE or replace PROCEDURE `S_UI_Campaign_Configuration`(IN in_request_json json, OUT out_result_json json)
BEGIN
   


    -- User Information

    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 

    -- Target Audience	
    declare AGGREGATION_1 varchar(400);
    declare Table_Name varchar(400);
	declare Column_Name varchar(400);
	declare In_Use_Customer_One_View varchar(400);	
	declare In_Use_Customer_Details_View varchar(400);
    declare Update_value varchar(400);
	 declare Updated_Column varchar(400);
    declare Description varchar(400);	
	declare Col_Username varchar(400);
	

	-- User Information
	  
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));

    -- AGGREGATIONS

	SET AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET Update_value = json_unquote(json_extract(in_request_json,"$.Updated_Value"));
	SET Description = json_unquote(json_extract(in_request_json,"$.Description"));
	SET Column_Name = json_unquote(json_extract(in_request_json,"$.Column_Name"));
	
	
	SET Table_Name = json_unquote(json_extract(in_request_json,"$.Table_Name"));
	SET Updated_Column = json_unquote(json_extract(in_request_json,"$.Updated_Column"));
	SET In_Use_Customer_One_View = json_unquote(json_extract(in_request_json,"$.In_Use_Customer_One_View"));
	SET In_Use_Customer_Details_View = json_unquote(json_extract(in_request_json,"$.In_Use_Customer_Details_View"));
	
    SET @temp_result = Null;
	
	SET SQL_SAFE_UPDATES=0;
	
	
	-- Creating  table for the ui config history for changes
	
						CREATE TABLE IF NOT EXISTS UI_Configuration_History (id INT NOT NULL AUTO_INCREMENT,Col_Name VARCHAR(100),Update_value VARCHAR(100),
                    Comments TEXT,User_Name VARCHAR(100),Last_Changed datetime DEFAULT current_timestamp(),PRIMARY KEY (ID));
	
	
	If AGGREGATION_1='Configuration'
	    then 
		
							IF Table_Name IS NULL OR Table_Name =" "
							THEN 
							Set @Query_String1= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Please Fill Table_Name ") )),"]") 
																					into @temp_result                       
																				  ;') ;
							   
														SELECT @Query_String1;
														PREPARE statement from @Query_String1;
														Execute statement;
														Deallocate PREPARE statement; 
														
							ELSE IF Update_value IS NULL OR Update_value =" "
							THEN 
							Set @Query_String1= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Please Fill Update_value ") )),"]") 
																					into @temp_result                       
																				  ;') ;
							   
														SELECT @Query_String1;
														PREPARE statement from @Query_String1;
														Execute statement;
														Deallocate PREPARE statement; 
														
							ELSE IF Updated_Column IS NULL OR Updated_Column =" "
							THEN 
							Set @Query_String1= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Please Fill Column_Name") )),"]") 
																					into @temp_result                       
																				  ;') ;
							   
														SELECT @Query_String1;
														PREPARE statement from @Query_String1;
														Execute statement;
														Deallocate PREPARE statement;
														
						    else
							
							IF Description IS NULL OR Description =" "
							THEN SET Description ="ABC";
							END IF;
                             
                             Set @Query_String = Concat('Update ',Table_Name,'
							Set Value="',Update_value,'",
							Description ="',Description,'"
							where Name="',Updated_Column,'";');
							SELECT @Query_String;
							
                            PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;
							
							
							 Set @Query_String1 = Concat('INSERT INTO UI_Configuration_History(Col_Name,Update_value,Comments,User_Name)VALUES
							                            ("',Updated_Column,'","',Update_value,'","',Description,'","',IN_USER_NAME,'")');
							SELECT @Query_String1;
							
                            PREPARE statement from @Query_String1;
							Execute statement;
							Deallocate PREPARE statement;
							
							
	                         Set @Query_String2= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Success") )),"]") 
																into @temp_result                       
															  ;') ;
		   
									SELECT @Query_String2;
									PREPARE statement from @Query_String2;
									Execute statement;
									Deallocate PREPARE statement; 														
														
							 END IF;
							 END IF;
							 END IF;
									
		
		
	
	
		  
		
		 
        

     End If;
	 
	If AGGREGATION_1='Configuration_Value'
	    then 
		
		 Set @Query_String1= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message",Value,"Description",Description) )),"]") 
																into @temp_result from ',Table_Name,' where Name="',Updated_Column,'"                   
															  ;') ;
		   
									SELECT @Query_String1;
									PREPARE statement from @Query_String1;
									Execute statement;
									Deallocate PREPARE statement;
		

		

	End If;
	
	If AGGREGATION_1='Configuration_Value1'
	    then 
		
		 Set @Query_String1= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Update_value",Update_value,"Comments",Comments,"User_Name",User_Name,"Last_Changed",Last_Changed) order by Last_Changed  desc  limit 5)),"]") 
																into @temp_result from UI_Configuration_History where Col_Name="',Updated_Column,'"
                                                        	
															
															  ;') ;
		   
									SELECT @Query_String1;
									PREPARE statement from @Query_String1;
									Execute statement;
									Deallocate PREPARE statement;
		

		

	End If;

		If AGGREGATION_1='UI_Configuration_Global'
	    then 
		
							IF Table_Name IS NULL OR Table_Name =" "
							THEN 
							Set @Query_String1= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Please Fill Table_Name ") )),"]") 
																					into @temp_result                       
																				  ;') ;
							   
														SELECT @Query_String1;
														PREPARE statement from @Query_String1;
														Execute statement;
														Deallocate PREPARE statement; 
														
							ELSE IF Update_value IS NULL OR Update_value =" "
							THEN 
							Set @Query_String1= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Please Fill Update_value ") )),"]") 
																					into @temp_result                       
																				  ;') ;
							   
														SELECT @Query_String1;
														PREPARE statement from @Query_String1;
														Execute statement;
														Deallocate PREPARE statement; 
														
							ELSE IF Updated_Column IS NULL OR Updated_Column =" "
							THEN 
							Set @Query_String1= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Please Fill Column_Name") )),"]") 
																					into @temp_result                       
																				  ;') ;
							   
														SELECT @Query_String1;
														PREPARE statement from @Query_String1;
														Execute statement;
														Deallocate PREPARE statement;
														
						    else

                             Set @Query_String = Concat('Update ',Table_Name,'
							Set Value1="',Update_value,'"
							where Config_Name="',Updated_Column,'";');
							SELECT @Query_String;
							
                            PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;
							
							
	                         Set @Query_String1= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Success") )),"]") 
																into @temp_result                       
															  ;') ;
		   
									SELECT @Query_String1;
									PREPARE statement from @Query_String1;
									Execute statement;
									Deallocate PREPARE statement; 														
														
							 END IF;
							 END IF;
							 END IF;
									
		
		
	
	
		  
		
		 
        

     End If;
	 
	If AGGREGATION_1='UI_Configuration_Global_Value'
	    then 
		
		 Set @Query_String1= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message",Value1) )),"]") into @temp_result from ',Table_Name,' where Config_Name="',Updated_Column,'"                   
															  ;') ;
		   
									SELECT @Query_String1;
									PREPARE statement from @Query_String1;
									Execute statement;
									Deallocate PREPARE statement;
		

		

	End If;
	

	
	If AGGREGATION_1='Lifecycle_Segment' 
	    then 
		    select Value into @Values from M_Config where Name = "Live/Onboarding_Client";
            select @Values;
			
			                IF @Values ="Y" 
							THEN
		
							IF Table_Name IS NULL OR Table_Name =" "
							THEN 
							Set @Query_String1= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Please Fill Table_Name ") )),"]") 
																					into @temp_result                       
																				  ;') ;
							   
														SELECT @Query_String1;
														PREPARE statement from @Query_String1;
														Execute statement;
														Deallocate PREPARE statement; 
														
							ELSE IF Update_value IS NULL OR Update_value =" "
							THEN 
							Set @Query_String1= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Please Fill Update_value ") )),"]") 
																					into @temp_result                       
																				  ;') ;
							   
														SELECT @Query_String1;
														PREPARE statement from @Query_String1;
														Execute statement;
														Deallocate PREPARE statement; 
														
							ELSE IF Updated_Column IS NULL OR Updated_Column =" "
							THEN 
							Set @Query_String1= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Please Fill Column_Name") )),"]") 
																					into @temp_result                       
																				  ;') ;
							   
														SELECT @Query_String1;
														PREPARE statement from @Query_String1;
														Execute statement;
														Deallocate PREPARE statement;
														
						    else

                             Set @Query_String = Concat('Update ',Table_Name,'
							Set CLMSegmentReplacedQuery ="',Update_value,'"
							where CLMSegmentName="',Updated_Column,'";');
							SELECT @Query_String;
							
                            PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;
							
							
	                         Set @Query_String1= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Success","Task","Y") )),"]") 
																into @temp_result                       
															  ;') ;
		   
									SELECT @Query_String1;
									PREPARE statement from @Query_String1;
									Execute statement;
									Deallocate PREPARE statement; 														
														
							 END IF;
							 END IF;
							 END IF;
							 
							 ELSE 
							 Set @Query_String1= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","As you are Live client you cant be able to edit","Task","N") )),"]") 
																					into @temp_result                       
																				  ;') ;
							   
														SELECT @Query_String1;
														PREPARE statement from @Query_String1;
														Execute statement;
														Deallocate PREPARE statement;
									
		          
		
	                         END IF;
	
		  
		
		 
        

     End If;
	 
	If AGGREGATION_1='Lifecycle_Segment_Value' 
	    then 
		
		 Set @Query_String1= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message",CLMSegmentReplacedQuery,"Task","N") )),"]") 
																into @temp_result from ',Table_Name,' where CLMSegmentName="',Updated_Column,'"                   
															  ;') ;
		   
									SELECT @Query_String1;
									PREPARE statement from @Query_String1;
									Execute statement;
									Deallocate PREPARE statement;
		

		

	End If;
	
	
	
	If AGGREGATION_1='Cutsomer_Views' 
       Then


		 Set @Sql0=Concat('Update Customer_View_Config
							Set In_Use_Customer_One_View="',In_Use_Customer_One_View,'",
							In_Use_Customer_Details_View ="',In_Use_Customer_Details_View,'"
							Where Table_Name="',Table_Name,'" and Column_Name="',Column_Name,'";');
									SELECT @Sql0;
									PREPARE statement from @Sql0;
									Execute statement;
									Deallocate PREPARE statement;
									
		 CALL S_CLM_Create_View();
 
		  
		Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Success") )),"]") 
																into @temp_result                       
															  ;') ;
		   
									SELECT @Query_String;
									PREPARE statement from @Query_String;
									Execute statement;
									Deallocate PREPARE statement; 
	   
	End If;
	
	
	
	
	
   	if @temp_result is null
		Then
								
							Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Invalid Parameters") )),"]") 
														into @temp_result                       
												      ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
						
	End if;
							
    SET out_result_json = @temp_result;

 END$$
DELIMITER ;
