DELIMITER $$
CREATE or replace PROCEDURE `PROC_UI_Create_Event`(

	IN vUserId BIGINT,   
	IN vCampaignThemeId BIGINT, 
	IN vCLMSegmentId  VARCHAR(128),-- Changed   
	IN vCampaignId BIGINT, 
	IN vMicroSegmentId BIGINT, 
	IN vChannelId BIGINT, 
	IN vOfferTypeId BIGINT,  

	IN vOfferName VARCHAR(128), 
	IN vOfferDesc VARCHAR(1024),
	IN vOfferDef VARCHAR(2048),
	IN vOfferStartDate DATE,
	IN vOfferEndDate DATE,
	IN vOfferRedemptionDays INTEGER,
	IN vOfferIsAtCustomerLevel TINYINT,

	IN vCampTriggerName VARCHAR(128), 
	IN vCampTriggerDesc VARCHAR(1024),
	IN vPrecedingTriggerName VARCHAR(128), 
	IN vPrecedingTriggerId BIGINT,
	IN vDaysOffsetFromPrecedingTrigger SMALLINT,
	IN vCTState TINYINT,
	IN vCTIsMandatory TINYINT,
	IN vIsAReminder TINYINT,
	IN vCTReplacedQueryAsInUI TEXT,
	IN vCTReplacedQuery TEXT,
	IN vCTIsValidSVOCCondition TINYINT,
	IN vCTReplacedQueryBillAsInUI TEXT,
	IN vCTReplacedQueryBill TEXT,
	IN vCTIsValidBillCondition TINYINT,
	IN vCTUsesGoodTimeModel	TINYINT,

	IN vCreativeName VARCHAR(128), 
	IN vExtCreativeKey VARCHAR(128),
    IN vSenderKey VARCHAR(255),
    IN vTimeslot_Id BIGINT(20),
	IN vCreativeDesc VARCHAR(1024),
	IN vCreative TEXT,
	IN vHeader TEXT,
    IN vMicrosite_Creative TEXT,
    IN vMicrosite_Header TEXT,

	IN vExclude_Recommended_SameAP_NotLT_FavCat TINYINT,
	IN vExclude_Recommended_SameLTD_FavCat TINYINT,
	IN vExclude_Recommended_SameLT_FavCat TINYINT,
	IN vRecommendation_Column VARCHAR(2048),
	IN vRecommendation_Type VARCHAR(2048),
	IN vExclude_Stock_out TINYINT,
	IN vExclude_Transaction_X_Days TINYINT,
	IN vExclude_Recommended_X_Days TINYINT,
	IN vExclude_Never_Brought TINYINT,
	IN vExclude_No_Image TINYINT,
	IN vNumber_Of_Recommendation INTEGER,
	IN vRecommendation_Filter_Logic VARCHAR(2048),
	IN vRecommendation_Column_Header VARCHAR(2048),
	IN vIs_Recommended_SameLT_FavCat TINYINT,
	IN vIs_Recommended_SameLTD_Cat TINYINT,
	IN vIs_Recommended_SameLTD_Dep TINYINT,
	IN vIs_Recommended_NewLaunch_Product TINYINT,
	IN vEvent_Limit  BIGINT,
    IN vCommunication_Cooloff BIGINT,

	INOUT vCampTriggerId BIGINT,
	INOUT vOfferId BIGINT,
	INOUT vCreativeId BIGINT,
	INOUT vEventId BIGINT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN
	DECLARE vResult TINYINT;
	DECLARE vInputValues TEXT;
    
    
    DECLARE vCustom_Attr1 VARCHAR(255) DEFAULT '\"\"';
    DECLARE vCustom_Attr2 VARCHAR(255) DEFAULT '\"\"';
    DECLARE vCustom_Attr3 VARCHAR(255) DEFAULT '\"\"';
    DECLARE vCustom_Attr4 VARCHAR(255) DEFAULT '\"\"';
    DECLARE vCustom_Attr5 VARCHAR(255) DEFAULT '\"\"';
	SET vSuccess = 1;
	SET vErrMsg = '';

	SET vInputValues  = CONCAT(
			'UserId: ', IFNULL(vUserId,'NULL VALUE')
			,' | CampaignThemeId: ',IFNULL(vCampaignThemeId,'NULL VALUE')
			,' | CLMSegmentId: ',IFNULL(vCLMSegmentId,'NULL VALUE')
			,' | CampaignId: ',IFNULL(vCampaignId,'NULL VALUE')
			,' | MicroSegmentId: ',IFNULL(vMicroSegmentId,'NULL VALUE')
			,' | ChannelId: ',IFNULL(vChannelId,'NULL VALUE')
			,' | OfferTypeId: ',IFNULL(vOfferTypeId ,'NULL VALUE')
			,' | OfferName: ',IFNULL(vOfferName,'NULL VALUE')
			,' | OfferDesc: ',IFNULL(vOfferDesc,'NULL VALUE')
			,' | OfferDef: ',IFNULL(vOfferDef,'NULL VALUE')
			,' | OfferStartDate: ',IFNULL(vOfferStartDate,'NULL VALUE')
			,' | OfferEndDate: ',IFNULL(vOfferEndDate,'NULL VALUE')
			,' | OfferRedemptionDays: ',IFNULL(vOfferRedemptionDays,'NULL VALUE')
			,' | OfferIsAtCustomerLevel: ',IFNULL(vOfferIsAtCustomerLevel,'NULL VALUE')
			,' | CampTriggerName: ',IFNULL(vCampTriggerName,'NULL VALUE')
			,' | CampTriggerDesc: ',IFNULL(vCampTriggerDesc,'NULL VALUE')
			,' | PrecedingTriggerName: ',IFNULL(vPrecedingTriggerName,'NULL VALUE')
			,' | PrecedingTriggerId: ',IFNULL(vPrecedingTriggerId,'NULL VALUE')
			,' | DaysOffsetFromPrecedingTrigger: ',IFNULL(vDaysOffsetFromPrecedingTrigger,'NULL VALUE')
			,' | CTState: ',IFNULL(vCTState,'NULL VALUE')
			,' | CTIsMandatory: ',IFNULL(vCTIsMandatory,'NULL VALUE')
			,' | IsAReminder: ',IFNULL(vIsAReminder,'NULL VALUE')
			,' | CTReplacedQueryAsInUI: ',IFNULL(vCTReplacedQueryAsInUI,'NULL VALUE')
			,' | CTReplacedQuery: ',IFNULL(vCTReplacedQuery,'NULL VALUE')
			,' | CTIsValidSVOCCondition: ',IFNULL(vCTIsValidSVOCCondition,'NULL VALUE')
			,' | CTReplacedQueryBillAsInUI: ',IFNULL(vCTReplacedQueryBillAsInUI,'NULL VALUE')
			,' | CTReplacedQueryBill: ',IFNULL(vCTReplacedQueryBill,'NULL VALUE')
			,' | CTIsValidBillCondition: ',IFNULL(vCTIsValidBillCondition,'NULL VALUE')
			,' | CTUsesGoodTimeModel: ',IFNULL(vCTUsesGoodTimeModel,'NULL VALUE')
			,' | CreativeName: ',IFNULL(vCreativeName,'NULL VALUE')
			,' | ExtCreativeKey: ',IFNULL(vExtCreativeKey,'NULL VALUE')
			,' | CreativeDesc: ',IFNULL(vCreativeDesc,'NULL VALUE')
			,' | Creative: ',IFNULL(vCreative,'NULL VALUE')
			,' | Header: ',IFNULL(vHeader,'NULL VALUE')
			,' | Exclude_Recommended_SameAP_NotLT_FavCat: ',IFNULL(vExclude_Recommended_SameAP_NotLT_FavCat,'NULL VALUE')
			,' | Exclude_Recommended_SameLTD_FavCat: ',IFNULL(vExclude_Recommended_SameLTD_FavCat,'NULL VALUE')
			,' | Exclude_Recommended_SameLT_FavCat: ',IFNULL(vExclude_Recommended_SameLT_FavCat,'NULL VALUE')
			,' | Recommendation_Column: ',IFNULL(vRecommendation_Column,'NULL VALUE')
			,' | Recommendation_Type: ',IFNULL(vRecommendation_Type,'NULL VALUE')
			,' | Exclude_Stock_out: ',IFNULL(vExclude_Stock_out,'NULL VALUE')
			,' | Exclude_Transaction_X_Days: ',IFNULL(vExclude_Transaction_X_Days,'NULL VALUE')
			,' | Exclude_Recommended_X_Days: ',IFNULL(vExclude_Recommended_X_Days,'NULL VALUE')
			,' | Exclude_Never_Brought: ',IFNULL(vExclude_Never_Brought,'NULL VALUE')
			,' | Exclude_No_Image: ',IFNULL(vExclude_No_Image,'NULL VALUE')
			,' | Number_Of_Recommendation: ',IFNULL(vNumber_Of_Recommendation,'NULL VALUE')
			,' | Recommendation_Filter_Logic: ',IFNULL(vRecommendation_Filter_Logic,'NULL VALUE')
			,' | Recommendation_Column_Header: ',IFNULL(vRecommendation_Column_Header,'NULL VALUE')
			,' | Is_Recommended_SameLT_FavCat: ',IFNULL(vIs_Recommended_SameLT_FavCat,'NULL VALUE')
			,' | Is_Recommended_SameLTD_Cat: ',IFNULL(vIs_Recommended_SameLTD_Cat,'NULL VALUE')
			,' | Is_Recommended_SameLTD_Dep: ',IFNULL(vIs_Recommended_SameLTD_Dep,'NULL VALUE')
			,' | Is_Recommended_NewLaunch_Product: ',IFNULL(vIs_Recommended_NewLaunch_Product,'NULL VALUE')
			,' | CampTriggerId: ',IFNULL(vCampTriggerId,'NULL VALUE')
			,' | OfferId: ',IFNULL(vOfferId,'NULL VALUE')
			,' | CreativeId: ',IFNULL(vCreativeId,'NULL VALUE')
			,' | EventId: ',IFNULL(vEventId,'NULL VALUE')
			);

	IF vTimeslot_Id IS NULL or vTimeslot_Id = ''
    THEN
		SET vTimeslot_Id = -1;
	END IF;
    
    IF vSenderKey is NULL or vSenderKey = ''
    THEN
		SET vSenderKey = 'SOLUS';
	END IF;
        
	IF vUserId IS NULL OR vUserId <= 0 THEN
		SET vErrMsg = CONCAT('User Id cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vCampaignThemeId IS NULL OR vCampaignThemeId <= 0 THEN
		SET vErrMsg = CONCAT(vErrMsg,'\r\n','Campaign Theme cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vCLMSegmentId IS NULL OR vCLMSegmentId < 0 THEN
		SET vErrMsg = CONCAT(vErrMsg,'\r\n','Life Cycle segment cannot be empty.');
		SET vSuccess = 0;
	END IF;
	
	IF vCampaignId IS NULL OR vCampaignId <= 0 THEN
		SET vErrMsg = CONCAT(vErrMsg,'\r\n','Campaign cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vMicroSegmentId IS NULL OR vMicroSegmentId <= 0 THEN
		SET vErrMsg = CONCAT(vErrMsg,'\r\n','Micro Segment cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vChannelId IS NULL OR vChannelId <= 0 THEN
		SET vErrMsg = CONCAT(vErrMsg,'\r\n','Channel cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vOfferTypeId IS NULL OR vOfferTypeId <= 0 THEN
		SET vErrMsg = CONCAT(vErrMsg,'\r\n','Offer Type cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vCampTriggerId IS NULL OR vCampTriggerId <= 0 THEN
		
		IF vCampTriggerName IS NULL OR vCampTriggerName = '' THEN
			SET vErrMsg = CONCAT(vErrMsg,'\r\n','Trigger name cannot be empty.');
			SET vSuccess = 0;
		ELSE 
			SET vResult = 0;
			SELECT 1 INTO vResult FROM CLM_Campaign_Trigger WHERE CampTriggerName = vCampTriggerName;
			IF vResult = 1 THEN
				SET vErrMsg = CONCAT(vErrMsg,'\r\n','A Trigger by the name ', vCampTriggerName, ' already exists.');
				SET vSuccess = 0;
			END IF;			
		END IF;
		IF (vCTReplacedQuery IS NULL OR vCTReplacedQuery = '')AND (vCTReplacedQueryBill IS NULL OR vCTReplacedQueryBill = '') THEN
			SET vErrMsg = CONCAT(vErrMsg,'\r\n','Both the SVOC criteria as well as Bill Criteria are empty. Please make sure at least one of them is valid.');
			SET vSuccess = 0;
		END IF;
		IF vCTState IS NULL OR vCTState < 0 THEN
			SET vErrMsg = CONCAT(vErrMsg,'\r\n','The Event status value is missing. This needs to be either enabled or disabled.');
			SET vSuccess = 0;
		END IF;
		IF vChannelId IS NULL OR vChannelId <= 0 THEN
			SET vErrMsg = CONCAT(vErrMsg,'\r\n','Please select a Channel for communication.');
			SET vSuccess = 0;
		END IF;
		IF vOfferTypeId > 1 THEN
			IF (vOfferId IS NULL OR vOfferId = 0) THEN
				IF vOfferName IS NULL OR vOfferName = '' THEN
					SET vErrMsg = CONCAT(vErrMsg,'\r\n','Please provide an Offer Name.');
					SET vSuccess = 0;
				
				
				END IF;
				IF vOfferStartDate IS NULL THEN 
					SET vErrMsg = CONCAT(vErrMsg,'\r\n','The Offer Start Date cannot be empty.');
					SET vSuccess = 0;
				ELSE
					IF vOfferStartDate < CURRENT_DATE() THEN
						SET vErrMsg = CONCAT(vErrMsg,'\r\n','The Offer Start Date has to be greater than Today.');
						SET vSuccess = 0;
					END IF;
				END IF;
				IF vOfferEndDate IS NULL THEN 
					SET vErrMsg = CONCAT(vErrMsg,'\r\n','The Offer End Date cannot be empty.');
					SET vSuccess = 0;
				ELSE
					IF vOfferStartDate < vOfferStartDate THEN
						SET vErrMsg = CONCAT(vErrMsg,'\r\n','The Offer End Date cannot be less than Offer');
						SET vSuccess = 0;
					END IF;
				END IF;
			END IF;
		END IF;

		IF vCreativeId	> 0 THEN
			IF vCreativeName IS NULL OR vCreativeName = '' THEN
				SET vErrMsg = CONCAT(vErrMsg,'\r\n','Please provide the Creative Name.');
				SET vSuccess = 0;
			ELSE
				SET vResult = 0;
				SELECT 1 INTO vResult FROM CLM_Creative WHERE CreativeName = vCreativeName AND CreativeId <> vCreativeId;
				IF vResult = 1 THEN
					SET vErrMsg = CONCAT(vErrMsg,'\r\n','A Creative by the name ', vCreativeName, ' already exists.');						
					SET vSuccess = 0;
				END IF;
				
				SET vResult = 0;
				SELECT 1 INTO vResult FROM CLM_Campaign_Events WHERE CreativeId = vCreativeId AND Has_Records_In_Execution_Hist = 1;
				IF vResult = 1 THEN							
					SET vErrMsg = CONCAT(vErrMsg,'\r\n','A Creative by the name ', vCreativeName, ' already exists and has been used in a SOLUS Run and hence cannot be modified.');
					SET vSuccess = 0;
				END IF;
			END IF;
		ELSE 
			IF vCreativeName IS NULL OR vCreativeName = '' THEN
				SET vErrMsg = CONCAT(vErrMsg,'\r\n','Please provide the Creative Name.');
				SET vSuccess = 0;
			ELSE
				SET vResult = 0;
				SELECT 1 INTO vResult FROM CLM_Creative WHERE CreativeName = vCreativeName;
				IF vResult = 1 THEN
					SET vErrMsg = CONCAT(vErrMsg,'\r\n','A Creative by the name ', vCreativeName, ' already exists.');						
					SET vSuccess = 0;
				END IF;
			END IF;
		END IF;

		if 	vNumber_Of_Recommendation >0 then
		
		IF (vRecommendation_Type IS NULL OR vRecommendation_Type <= 0) THEN
		
			IF vNumber_Of_Recommendation IS NOT NULL THEN
				SET vSuccess = 0;
			END IF;
			
			
			IF vSuccess = 0 THEN
				SET vErrMsg = CONCAT(vErrMsg,'\r\n','Please select a Recommendation Type.');
			END IF;
		END IF;
		End if;
	ELSE 
		
		SET vResult = 0;
		SELECT EXISTS(
			SELECT 1 
			FROM CLM_Campaign_Events
			WHERE 
				CampTriggerId = vCampTriggerId
				AND Has_Records_In_Execution_Hist = 1
		) INTO vResult;
		IF vResult > 0 THEN
			SET vErrMsg = 'This Event has already been executed at least once as part of SOLUS run. Hence modification of the Event is not permitted.';
			SET vSuccess = 0;
		ELSE
			IF (vCTReplacedQuery IS NULL OR vCTReplacedQuery = '') AND (vCTReplacedQueryBill IS NULL OR vCTReplacedQueryBill = '') THEN
				SET vErrMsg = CONCAT(vErrMsg,'\r\n','Both the SVOC criteria as well as Bill Criteria are empty. Please make sure at least one of them is valid.');
				SET vSuccess = 0;
			END IF;
			IF vCTState IS NULL OR vCTState < 0 THEN
				SET vErrMsg = CONCAT(vErrMsg,'\r\n','The Event status value is missing. This needs to be either enabled or disabled.');
				SET vSuccess = 0;
			END IF;
			IF vChannelId IS NULL OR vChannelId <= 0 THEN
				SET vErrMsg = CONCAT(vErrMsg,'\r\n','Please select a Channel for communication.');
				SET vSuccess = 0;
			END IF;
			IF vOfferTypeId > 0 THEN
				IF (vOfferId IS NULL OR vOfferId <= 0) THEN
					IF vOfferName IS NULL OR vOfferName = '' THEN
						SET vErrMsg = CONCAT(vErrMsg,'\r\n','Please provide an Offer Name.');
						SET vSuccess = 0;
							
					END IF;
					IF vOfferStartDate IS NULL THEN 
						SET vErrMsg = CONCAT(vErrMsg,'\r\n','The Offer Start Date cannot be empty.');
						SET vSuccess = 0;
					ELSE
						IF vOfferStartDate < CURRENT_DATE() THEN
							SET vErrMsg = CONCAT(vErrMsg,'\r\n','The Offer Start Date has to be greater than Today.');
							SET vSuccess = 0;
						END IF;
					END IF;
					IF vOfferEndDate IS NULL THEN 
						SET vErrMsg = CONCAT(vErrMsg,'\r\n','The Offer End Date cannot be empty.');
						SET vSuccess = 0;
					ELSE
						IF vOfferStartDate < vOfferStartDate THEN
							SET vErrMsg = CONCAT(vErrMsg,'\r\n','The Offer End Date cannot be less than Offer');
							SET vSuccess = 0;
						END IF;
					END IF;
				ELSE
					SET vResult = 0;
					SELECT EXISTS(
						SELECT 1 
						FROM CLM_Offer, CLM_Creative, CLM_Campaign_Events
						WHERE 
							CLM_Offer.OfferId = CLM_Creative.OfferId
							AND CLM_Creative.CreativeId = CLM_Campaign_Events.CreativeId
							AND CLM_Offer.OfferId = vOfferId
							AND CLM_Campaign_Events.Has_Records_In_Execution_Hist = 1
                             AND OfferName not  in ('CUSTOMER_LEVEL_OFFER','NO_OFFER')
					) INTO vResult;
					IF vResult > 0 THEN
						SET vErrMsg = 'This Offer has already been part of an execution at least once. Hence modification of the Offer is not permitted.';
						SET vSuccess = 0;
					END IF;
				END IF;
			END IF;
		END IF;

		IF vCreativeId	> 0 THEN
			IF vCreativeName IS NULL OR vCreativeName = '' THEN
				SET vErrMsg = CONCAT(vErrMsg,'\r\n','Please provide the Creative Name.');
				SET vSuccess = 0;
			ELSE
				SET vResult = 0;
				SELECT 1 INTO vResult FROM CLM_Creative WHERE CreativeName = vCreativeName AND CreativeId <> vCreativeId;
				IF vResult = 1 THEN
					SET vErrMsg = CONCAT(vErrMsg,'\r\n','A Creative by the name ', vCreativeName, ' already exists.');						
					SET vSuccess = 0;
				END IF;
				
				SET vResult = 0;
				SELECT 1 INTO vResult FROM CLM_Campaign_Events WHERE CreativeId = vCreativeId AND Has_Records_In_Execution_Hist = 1;
				IF vResult = 1 THEN							
					SET vErrMsg = CONCAT(vErrMsg,'\r\n','A Creative by the name ', vCreativeName, ' already exists and has been used in a SOLUS Run and hence cannot be modified.');
					SET vSuccess = 0;
				END IF;
			END IF;
		ELSE 
			IF vCreativeName IS NULL OR vCreativeName = '' THEN
				SET vErrMsg = CONCAT(vErrMsg,'\r\n','Please provide the Creative Name.');
				SET vSuccess = 0;
			ELSE
				SET vResult = 0;
				SELECT 1 INTO vResult FROM CLM_Creative WHERE CreativeName = vCreativeName;
				IF vResult = 1 THEN
					SET vErrMsg = CONCAT(vErrMsg,'\r\n','A Creative by the name ', vCreativeName, ' already exists.');						
					SET vSuccess = 0;
				END IF;
			END IF;
		END IF;

		if 	vNumber_Of_Recommendation >0 then
		
		IF (vRecommendation_Type IS NULL OR vRecommendation_Type <= 0) THEN
			
			IF vNumber_Of_Recommendation IS NOT NULL THEN
				SET vSuccess = 0;
			END IF;
			
			IF vSuccess = 0 THEN
				SET vErrMsg = CONCAT(vErrMsg,'\r\n','Please select a Recommendation Type.');
			END IF;
		END IF;
		End if;


	END IF;

	IF vSuccess = 1 THEN
		SET vSuccess = 0;
		CALL PROC_CLM_Create_Event(
		
			vUserId,   
			vCampaignThemeId, 
			vCLMSegmentId, 
			vCampaignId, 
			vMicroSegmentId, 
			vChannelId, 
			vOfferTypeId,
		
			IFNULL(vOfferName,''),
			IFNULL(vOfferDesc,''),
			IFNULL(vOfferDef,''),
			IFNULL(vOfferStartDate,''),
			IFNULL(vOfferEndDate,''),
			IFNULL(vOfferRedemptionDays,0),
			IFNULL(vOfferIsAtCustomerLevel, 0),
		
			vCampTriggerName,
			IFNULL(vCampTriggerDesc,''),
			IFNULL(vPrecedingTriggerName,''),
			IFNULL(vPrecedingTriggerId ,0),
			IFNULL(vDaysOffsetFromPrecedingTrigger,0),
			IFNULL(vCTState, 0),
			IFNULL(vCTIsMandatory, 0),
			IFNULL(vIsAReminder, 0),
			IFNULL(vCTReplacedQueryAsInUI,''),
			IFNULL(vCTReplacedQuery,''),
			IFNULL(vCTIsValidSVOCCondition, 0),
			IFNULL(vCTReplacedQueryBillAsInUI,''),
			IFNULL(vCTReplacedQueryBill,''),
			IFNULL(vCTIsValidBillCondition, 0),
			IFNULL(vCTUsesGoodTimeModel,0),
		
			vCreativeName,
			IFNULL(vExtCreativeKey,''),
            IFNULL(vSenderKey,''),
            IFNULL(vTimeslot_Id,-1),
			IFNULL(vCreativeDesc,''),
			IFNULL(vCreative,''),
			IFNULL(vHeader,''),
            IFNULL(vMicrosite_Creative,''),
            IFNULL(vMicrosite_Header,''),
		
			IFNULL(vExclude_Recommended_SameAP_NotLT_FavCat, 0),
			IFNULL(vExclude_Recommended_SameLTD_FavCat, 0),
			IFNULL(vExclude_Recommended_SameLT_FavCat, 0),
			IFNULL(vRecommendation_Column,''),
			IFNULL(vRecommendation_Type,''),
			IFNULL(vExclude_Stock_out, 0),
			IFNULL(vExclude_Transaction_X_Days, 0),
			IFNULL(vExclude_Recommended_X_Days, 0),
			IFNULL(vExclude_Never_Brought, 0),
			IFNULL(vExclude_No_Image, 0),
			IFNULL(vNumber_Of_Recommendation,0),
			IFNULL(vRecommendation_Filter_Logic,''),
			IFNULL(vRecommendation_Column_Header,''),
			IFNULL(vIs_Recommended_SameLT_FavCat, 0),
			IFNULL(vIs_Recommended_SameLTD_Cat, 0),
			IFNULL(vIs_Recommended_SameLTD_Dep, 0),
			IFNULL(vIs_Recommended_NewLaunch_Product, 0),
			IFNULL(vEvent_Limit,0),
            IFNULL(vCommunication_Cooloff,0),
            IFNULL(vCustom_Attr1,''),
            IFNULL(vCustom_Attr2,''),
            IFNULL(vCustom_Attr3,''),
            IFNULL(vCustom_Attr4,''),
            IFNULL(vCustom_Attr5,''),
            
		
			vCampTriggerId,
			vOfferId,
			vCreativeId,
			vEventId,
			vErrMsg,
			vSuccess
		);
	END IF;

	IF vSuccess = 0 THEN 
	SET vErrMsg = CONCAT(vErrMsg , '\r\n', vInputValues);

	END IF;

END$$
DELIMITER ;
