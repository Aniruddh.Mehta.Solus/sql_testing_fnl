DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_loyalty_region_metrics`()
BEGIN
DECLARE Cursor_Check_Var Int Default 0;
DECLARE cur_Execution_Date_ID INT(11);
DECLARE cur_Segment CHAR(20);
DECLARE cur_Category CHAR(40);
DECLARE cur_Tier CHAR(20);

DECLARE curALL_Tiers
    CURSOR For SELECT DISTINCT
    Tier
FROM
    Dashboard_Loyalty_Metrics_Temp
	WHERE Tier IS NOT NULL
	ORDER BY Tier ASC;

DECLARE curALL_Categories
    CURSOR For SELECT DISTINCT
    Category
FROM
    Dashboard_Loyalty_Metrics_Cat_Temp
	WHERE Category IS NOT NULL
	ORDER BY Category ASC;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET Cursor_Check_Var=1;

DELETE from log_solus where module = 'S_dashboard_loyalty_region_metrics';
SET SQL_SAFE_UPDATES=0;

SELECT `Value1` into @TYSM from UI_Configuration_Global where `Config_Name` = 'FISCAL';
SELECT @TYSM;

SELECT `Value2` into @TYEM from UI_Configuration_Global where `Config_Name` = 'FISCAL';
SELECT @TYEM;

SET @LYSM = F_Month_Diff(@TYSM,12);
SET @LYEM = F_Month_Diff(@TYEM,12);

SELECT @LYSM,@LYEM;

SET @LLYSM = F_Month_Diff(@LYSM,12);
SET @LLYEM = F_Month_Diff(@LYEM,12);

SELECT @LLYSM,@LLYEM;

SET @Start_Date = date_format(date_sub(concat(date_format(current_date(),"%Y%m"),'01'), INTERVAL 14 MONTH),"%Y%m");
SET @End_Date = date_format(current_date(),"%Y%m");

SET @TM = (SELECT date_format(current_date(),"%Y%m"));
SET @LM = F_Month_Diff(@TM,1);
SET @SMLY = F_Month_Diff(@TM,12);
SET @TY=substr(@TM,1,4);
SET @LY=substr(@SMLY,1,4);
SET @LLY = @LY - 1;

SELECT @TM,@LM,@SMLY,@TY,@LY,@LLY;
	
DROP TABLE IF EXISTS Dashboard_Loyalty_Region_Temp;
  CREATE TABLE IF NOT EXISTS Dashboard_Loyalty_Region_Temp ( 
	`Tier` varchar(50),
	`TierPercent` varchar(50),
	`Region` varchar(20),
	`Segment` varchar(128),
	`Category` varchar(128),
	`Bill_Year_Month` int(11),
    `Membership_Status` varchar(3),
	`Member_desc` varchar(20),
    `Member_count` int(10),
	`Measure` varchar(50),
	`MemberPercent` decimal(10,2),
	`CategoryPercent` decimal(10,2),
	`Enrollment_Percent` decimal(10,2),
	`Tagged_Percent` decimal(10,2),
	`EmailcapPercent` decimal(10,2),
	`AddresscapPercent` decimal(10,2),
	`MobilecapPercent` decimal(10,2),
	`NamecapPercent` decimal(10,2),
	`DOBcapPercent` decimal(10,2),
	`GendercapPercent` decimal(10,2),
	`DOAcapPercent` decimal(10,2),
	`Non_Members` int(10),
	`New_Members` int(10)
  ) ENGINE = INNODB DEFAULT CHARACTER SET=LATIN1; 
  
 
  ALTER TABLE Dashboard_Loyalty_Region_Temp    
    ADD INDEX (Segment), 
    ADD INDEX (Tier),
    ADD INDEX (Measure),
	ADD INDEX (Category),
	ADD INDEX (Bill_Year_Month),
    ADD INDEX (Membership_Status),
	ADD INDEX (Region);

INSERT INTO log_solus(module, msg) values ('S_dashboard_loyalty_region_metrics', 'Loading Region wise data');

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Tier,
Region,
Membership_Status,
Measure,
Bill_Year_Month,
Member_count
)		
SELECT
Tier,
Region,
Membership_Status,
"BYMONTH" AS Measure,
Bill_Year_Month,
count(distinct(Customer_id)) AS Member_count
FROM Dashboard_Loyalty_Metrics_Temp
WHERE Membership_status = "Y" 
AND Bill_Year_Month BETWEEN @Start_Date AND @End_Date
GROUP BY 1,2,3,4,5;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Tier,
Region,
Measure,
Membership_Status,
Member_count
)		
SELECT
Tier,
Region,
"OVERALL" AS Measure,
Membership_Status,
count(distinct(Customer_id)) AS Member_count
FROM Dashboard_Loyalty_Metrics_Temp
WHERE Membership_status = "Y" 
GROUP BY 1,2,3,4;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Tier,
Region,
Measure,
Membership_Status,
Member_count
)		
SELECT
Tier,
Region,
"TY" AS Measure,
Membership_Status,
count(distinct(Customer_id)) AS Member_count
FROM Dashboard_Loyalty_Metrics_Temp
WHERE Bill_Year_Month BETWEEN @TYSM AND @TYEM
AND Membership_status = "Y" 
GROUP BY 1,2,3,4;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Tier,
Region,
Measure,
Membership_Status,
Member_count
)		
SELECT
Tier,
Region,
"L12M" AS Measure,
Membership_Status,
count(distinct(Customer_id)) AS Member_count
FROM Dashboard_Loyalty_Metrics_Temp
WHERE Bill_Year_Month BETWEEN @SMLY AND @TM
AND Membership_status = "Y" 
GROUP BY 1,2,3,4;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Membership_Status,
Measure,
Bill_Year_Month,
Non_Members
)		
SELECT
Region,
Membership_Status,
"BYMONTH" AS Measure,
Bill_Year_Month,
count(distinct(Customer_id)) AS Non_Members
FROM Dashboard_Loyalty_Metrics_Temp
WHERE Membership_status = "N" 
AND Bill_Year_Month BETWEEN @Start_Date AND @End_Date
GROUP BY 1,2,3,4;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Membership_Status,
Measure,
Non_Members
)		
SELECT
Region,
Membership_Status,
"OVERALL" AS Measure,
count(distinct(Customer_id)) AS Non_Members
FROM Dashboard_Loyalty_Metrics_Temp
WHERE Membership_status = "N" 
GROUP BY 1,2,3;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Membership_Status,
Measure,
Non_Members
)		
SELECT
Region,
Membership_Status,
"TY" AS Measure,
count(distinct(Customer_id)) AS Non_Members
FROM Dashboard_Loyalty_Metrics_Temp
WHERE Membership_status = "N" 
AND Bill_Year_Month BETWEEN @TYSM AND @TYEM
GROUP BY 1,2,3;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Membership_Status,
Measure,
Non_Members
)		
SELECT
Region,
Membership_Status,
"L12M" AS Measure,
count(distinct(Customer_id)) AS Non_Members
FROM Dashboard_Loyalty_Metrics_Temp
WHERE Membership_status = "N" 
AND Bill_Year_Month BETWEEN @SMLY AND @TM
GROUP BY 1,2,3;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Membership_Status,
Measure,
Bill_Year_Month,
New_Members
)		
SELECT
Region,
Membership_Status,
"BYMONTH" AS Measure,
Bill_Year_Month,
count(distinct(A.Customer_id)) AS New_Members
FROM Dashboard_Loyalty_Metrics_Temp A
JOIN CDM_Customer_TP_Var B
ON A.Customer_Id = B.Customer_Id
WHERE Membership_status = "Y" 
AND Lt_Num_of_Visits = 1
AND Bill_Year_Month BETWEEN @Start_Date and @End_Date
GROUP BY 1,2,3,4;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Membership_Status,
Measure,
New_Members
)		
SELECT
Region,
Membership_Status,
"TY" AS Measure,
count(distinct(A.Customer_id)) AS New_Members
FROM Dashboard_Loyalty_Metrics_Temp A
JOIN CDM_Customer_TP_Var B
ON A.Customer_Id = B.Customer_Id
WHERE Membership_status = "Y" 
AND Lt_Num_of_Visits = 1
AND Bill_Year_Month BETWEEN @TYSM AND @TYEM
GROUP BY 1,2,3;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Membership_Status,
Measure,
New_Members
)		
SELECT
Region,
Membership_Status,
"OVERALL" AS Measure,
count(distinct(A.Customer_id)) AS New_Members
FROM Dashboard_Loyalty_Metrics_Temp A
JOIN CDM_Customer_TP_Var B
ON A.Customer_Id = B.Customer_Id
WHERE Membership_status = "Y" 
AND Lt_Num_of_Visits = 1
GROUP BY 1,2,3;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Membership_Status,
Measure,
New_Members
)		
SELECT
Region,
Membership_Status,
"L12M" AS Measure,
count(distinct(A.Customer_id)) AS New_Members
FROM Dashboard_Loyalty_Metrics_Temp A
JOIN CDM_Customer_TP_Var B
ON A.Customer_Id = B.Customer_Id
WHERE Membership_status = "Y" 
AND Lt_Num_of_Visits = 1
AND Bill_Year_Month BETWEEN @SMLY AND @TM
GROUP BY 1,2,3;


SET Cursor_Check_Var = FALSE;
OPEN curALL_Tiers;
LOOP_ALL_DATES: LOOP

	FETCH curALL_Tiers into cur_Tier;
	IF Cursor_Check_Var THEN
	   LEAVE LOOP_ALL_DATES;
	END IF;

		Select cur_Tier,now();

INSERT INTO Dashboard_Loyalty_Region_Temp
(
TierPercent,
Region,
Measure,
MemberPercent
)		
SELECT
concat('% ',Tier) AS Tier,
Region,
"TY" AS Measure,
MemberPercent
FROM
(
SELECT C.Region, C.Tier, Bills AS MemberPercent FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills', A.Region, A.Tier FROM
(
select Region, Tier, SUM(Member_Count) AS c1
from Dashboard_Loyalty_Region_Temp
where Tier is not NULL and Tier <> "NOT-CAPTUR"
and Tier = cur_Tier
and Bill_Year_Month BETWEEN @TYSM AND @TYEM
group by 1,2
)A
,
(
select Region, SUM(Member_Count) AS COUNT2
from Dashboard_Loyalty_Region_Temp
where Bill_Year_Month BETWEEN @TYSM AND @TYEM
group by Region
)B
WHERE A.Region = B.Region
) C
GROUP BY C.Region
) AS T;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
TierPercent,
Region,
Measure,
MemberPercent
)		
SELECT
concat('% ',Tier) AS Tier,
Region,
"OVERALL" AS Measure,
MemberPercent
FROM
(
SELECT C.Region, C.Tier, Bills AS MemberPercent FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills', A.Region, A.Tier FROM
(
select Region, Tier, SUM(Member_Count) AS c1
from Dashboard_Loyalty_Region_Temp
where Tier is not NULL and Tier <> "NOT-CAPTUR"
and Tier = cur_Tier
group by 1,2
)A
,
(
select Region, SUM(Member_Count) AS COUNT2
from Dashboard_Loyalty_Region_Temp
group by Region
)B
WHERE A.Region = B.Region
) C
GROUP BY C.Region
) AS T;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
TierPercent,
Region,
Measure,
MemberPercent
)		
SELECT
concat('% ',Tier) AS Tier,
Region,
"L12M" AS Measure,
MemberPercent
FROM
(
SELECT C.Region, C.Tier, Bills AS MemberPercent FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills', A.Region, A.Tier FROM
(
select Region, Tier, SUM(Member_Count) AS c1
from Dashboard_Loyalty_Region_Temp
where Tier is not NULL and Tier <> "NOT-CAPTUR"
and Tier = cur_Tier
and Bill_Year_Month BETWEEN @SMLY AND @TM
group by 1,2
)A
,
(
select Region, SUM(Member_Count) AS COUNT2
from Dashboard_Loyalty_Region_Temp
where Bill_Year_Month BETWEEN @SMLY AND @TM
group by Region
)B
WHERE A.Region = B.Region
) C
GROUP BY C.Region
) AS T;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
TierPercent,
Region,
Measure,
Bill_Year_Month,
MemberPercent
)		
SELECT
concat('% ',Tier) AS Tier,
Region,
"BYMONTH" AS Measure,
Bill_Year_Month,
MemberPercent
FROM
(
SELECT C.Region, C.Tier, C.Bill_Year_Month, Bills AS MemberPercent FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills', A.Region, A.Tier, A.Bill_Year_Month FROM
(
select Region, Tier, Bill_Year_Month, SUM(Member_Count) AS c1
from Dashboard_Loyalty_Region_Temp
where Tier is not NULL and Tier <> "NOT-CAPTUR"
and Tier = cur_Tier
and Bill_Year_Month BETWEEN @Start_Date AND @End_Date
group by 1,2,3
)A
,
(
select Region, Bill_Year_Month, SUM(Member_Count) AS COUNT2
from Dashboard_Loyalty_Region_Temp
where Bill_Year_Month BETWEEN @Start_Date AND @End_Date
group by 1,2
)B
WHERE A.Region = B.Region
AND A.Bill_Year_Month = B.Bill_Year_Month
) C
GROUP BY C.Region, C.Bill_Year_Month
) AS T;

END LOOP LOOP_ALL_DATES;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Measure,
Enrollment_Percent
)		
select 
A.Region,
'OVERALL' AS Measure,
(A.Member*100)/B.Total as Enrollment_Percent 
from 
(
(select Region,
count(distinct(Customer_Id)) as Member 
from Dashboard_Loyalty_Metrics_Temp 
where Membership_Status = "Y" 
group by Region)A
,
(select Region, 
count(distinct(Customer_Id)) as Total 
from Dashboard_Loyalty_Metrics_Temp 
group by Region)B
)
where A.Region = B.Region
and A.Region <> ""
group by A.region;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Measure,
Enrollment_Percent
)		
select 
A.Region,
'TY' AS Measure,
(A.Member*100)/B.Total as Enrollment_Percent 
from 
(
(select Region,
count(distinct(Customer_Id)) as Member 
from Dashboard_Loyalty_Metrics_Temp 
where Membership_Status = "Y" 
and Bill_Year_Month BETWEEN @TYSM AND @TYEM
group by Region)A
,
(select Region, 
count(distinct(Customer_Id)) as Total 
from Dashboard_Loyalty_Metrics_Temp 
WHERE Bill_Year_Month BETWEEN @TYSM AND @TYEM
group by Region)B
)
where A.Region = B.Region
and A.Region <> ""
group by A.region;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Measure,
Enrollment_Percent
)		
select 
A.Region,
'L12M' AS Measure,
(A.Member*100)/B.Total as Enrollment_Percent 
from 
(
(select Region,
count(distinct(Customer_Id)) as Member 
from Dashboard_Loyalty_Metrics_Temp 
where Membership_Status = "Y" 
and Bill_Year_Month BETWEEN @SMLY AND @TM
group by Region)A
,
(select Region, 
count(distinct(Customer_Id)) as Total 
from Dashboard_Loyalty_Metrics_Temp 
WHERE Bill_Year_Month BETWEEN @SMLY AND @TM
group by Region)B
)
where A.Region = B.Region
and A.Region <> ""
group by A.region;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Measure,
Bill_Year_Month,
Enrollment_Percent
)		
select 
A.Region,
'BYMONTH' AS Measure,
A.Bill_Year_Month,
(A.Member*100)/B.Total as Enrollment_Percent 
from 
(
(select Region,Bill_Year_Month,
count(distinct(Customer_Id)) as Member 
from Dashboard_Loyalty_Metrics_Temp 
where Membership_Status = "Y" 
and Bill_Year_Month BETWEEN @Start_Date AND @End_Date
group by 1,2)A
,
(select Region, Bill_Year_Month,
count(distinct(Customer_Id)) as Total 
from Dashboard_Loyalty_Metrics_Temp 
WHERE Bill_Year_Month BETWEEN @Start_Date AND @End_Date
group by 1,2)B
)
where A.Region = B.Region
and A.Bill_Year_Month = B.Bill_Year_Month
and A.Region <> ""
group by A.region,A.Bill_Year_Month;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Measure,
Tagged_Percent
)		
select 
A.Region,
'OVERALL' AS Measure,
(A.Tagged_Bill*100)/B.Total_Bill as Tagged_Percent 
from 
(
(select Region,
sum(visits) as Tagged_Bill
from Dashboard_Loyalty_Metrics_Temp 
where Membership_Status = "Y" 
group by Region)A
,
(select Region, 
sum(visits) as Total_Bill
from Dashboard_Loyalty_Metrics_Temp 
group by Region)B
)
where A.Region = B.Region
and A.Region <> ""
group by A.region;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Measure,
Tagged_Percent
)		
select 
A.Region,
'TY' AS Measure,
(A.Tagged_Bill*100)/B.Total_Bill as Tagged_Percent 
from 
(
(select Region,
sum(visits) as Tagged_Bill
from Dashboard_Loyalty_Metrics_Temp 
where Membership_Status = "Y" 
AND Bill_Year_Month BETWEEN @TYSM AND @TYEM
group by Region)A
,
(select Region, 
sum(visits) as Total_Bill
from Dashboard_Loyalty_Metrics_Temp 
WHERE Bill_Year_Month BETWEEN @TYSM AND @TYEM
group by Region)B
)
where A.Region = B.Region
and A.Region <> ""
group by A.region;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Measure,
Tagged_Percent
)		
select 
A.Region,
'L12M' AS Measure,
(A.Tagged_Bill*100)/B.Total_Bill as Tagged_Percent 
from 
(
(select Region,
sum(visits) as Tagged_Bill
from Dashboard_Loyalty_Metrics_Temp 
where Membership_Status = "Y" 
AND Bill_Year_Month BETWEEN @SMLY AND @TM
group by Region)A
,
(select Region, 
sum(visits) as Total_Bill
from Dashboard_Loyalty_Metrics_Temp 
WHERE Bill_Year_Month BETWEEN @SMLY AND @TM
group by Region)B
)
where A.Region = B.Region
and A.Region <> ""
group by A.region;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Measure,
Bill_Year_Month,
Tagged_Percent
)		
select 
A.Region,
'BYMONTH' AS Measure,
A.Bill_Year_Month,
(A.Tagged_Bill*100)/B.Total_Bill as Tagged_Percent 
from 
(
(select Region,Bill_Year_Month,
sum(visits) as Tagged_Bill
from Dashboard_Loyalty_Metrics_Temp 
where Membership_Status = "Y" 
and Bill_Year_Month BETWEEN @Start_Date AND @End_Date
group by 1,2)A
,
(select Region, Bill_Year_Month,
sum(visits) as Total_Bill
from Dashboard_Loyalty_Metrics_Temp 
WHERE Bill_Year_Month BETWEEN @Start_Date AND @End_Date
group by 1,2)B
)
where A.Region = B.Region
and A.Bill_Year_Month = B.Bill_Year_Month
and A.Region <> ""
group by A.region,A.Bill_Year_Month;

SET Cursor_Check_Var = FALSE;
OPEN curALL_Categories;
LOOP_ALL_CATS: LOOP

	FETCH curALL_Categories into cur_Category;
	IF Cursor_Check_Var THEN
	   LEAVE LOOP_ALL_CATS;
	END IF;

		Select cur_Category,now();

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Category,
Region,
Measure,
CategoryPercent
)		
SELECT
concat('% ',Category) AS Category,
Region,
"OVERALL" AS Measure,
CategoryPercent
FROM
(
SELECT C.Region, C.Category, Bills AS CategoryPercent FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills', A.Region, A.Category FROM
(
select Region, Category, COUNT(DISTINCT Customer_Id) AS c1
from Dashboard_Loyalty_Metrics_Cat_Temp
Where Category = Cur_Category
group by 1,2
)A
,
(
select Region, COUNT(DISTINCT Customer_Id) AS COUNT2
from Dashboard_Loyalty_Metrics_Cat_Temp
group by Region
)B
WHERE A.Region = B.Region
) C
GROUP BY C.Region
) AS T;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Category,
Region,
Measure,
CategoryPercent
)		
SELECT
concat('% ',Category) AS Category,
Region,
"TY" AS Measure,
CategoryPercent
FROM
(
SELECT C.Region, C.Category, Bills AS CategoryPercent FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills', A.Region, A.Category FROM
(
select Region, Category, COUNT(DISTINCT Customer_Id) AS c1
from Dashboard_Loyalty_Metrics_Cat_Temp
Where Category = Cur_Category
and Bill_Year_Month BETWEEN @TYSM AND @TYEM
group by 1,2
)A
,
(
select Region, COUNT(DISTINCT Customer_Id) AS COUNT2
from Dashboard_Loyalty_Metrics_Cat_Temp
where Bill_Year_Month BETWEEN @TYSM AND @TYEM
group by Region
)B
WHERE A.Region = B.Region
) C
GROUP BY C.Region
) AS T;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Category,
Region,
Measure,
CategoryPercent
)		
SELECT
concat('% ',Category) AS Category,
Region,
"L12M" AS Measure,
CategoryPercent
FROM
(
SELECT C.Region, C.Category, Bills AS CategoryPercent FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills', A.Region, A.Category FROM
(
select Region, Category, COUNT(DISTINCT Customer_Id) AS c1
from Dashboard_Loyalty_Metrics_Cat_Temp
Where Category = Cur_Category
and Bill_Year_Month BETWEEN @SMLY AND @TM
group by 1,2
)A
,
(
select Region, COUNT(DISTINCT Customer_Id) AS COUNT2
from Dashboard_Loyalty_Metrics_Cat_Temp
where Bill_Year_Month BETWEEN @SMLY AND @TM
group by Region
)B
WHERE A.Region = B.Region
) C
GROUP BY C.Region
) AS T;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Category,
Region,
Measure,
Bill_Year_Month,
CategoryPercent
)		
SELECT
concat('% ',Category) AS Category,
Region,
"BYMONTH" AS Measure,
Bill_Year_Month,
CategoryPercent
FROM
(
SELECT C.Region, C.Category, C.Bill_Year_Month, Bills AS CategoryPercent FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills', A.Region, A.Category, A.Bill_Year_Month FROM
(
select Region, Category, Bill_Year_Month, COUNT(DISTINCT Customer_Id) AS c1
from Dashboard_Loyalty_Metrics_Cat_Temp
where Category = cur_Category
and Bill_Year_Month BETWEEN @Start_Date AND @End_Date
group by 1,2,3
)A
,
(
select Region, Bill_Year_Month, COUNT(DISTINCT Customer_Id) AS COUNT2
from Dashboard_Loyalty_Metrics_Cat_Temp
where Bill_Year_Month BETWEEN @Start_Date AND @End_Date
group by 1,2
)B
WHERE A.Region = B.Region
AND A.Bill_Year_Month = B.Bill_Year_Month
) C
GROUP BY C.Region, C.Bill_Year_Month
) AS T;

END LOOP LOOP_ALL_CATS;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Measure,
Bill_Year_Month,
EmailcapPercent
)		
select 
A.Region,
'BYMONTH' AS Measure,
A.Bill_Year_Month,
(A.c1*100)/B.c2 as EmailcapPercent 
from 
(
(select Region,Bill_Year_Month,
COUNT(DISTINCT Customer_Id) as c1
from Dashboard_Loyalty_Metrics_Temp 
where Email_Capture = "Y" 
and Bill_Year_Month BETWEEN @Start_Date AND @End_Date
group by 1,2)A
,
(select Region, Bill_Year_Month,
COUNT(DISTINCT Customer_Id) as c2
from Dashboard_Loyalty_Metrics_Temp 
WHERE Bill_Year_Month BETWEEN @Start_Date AND @End_Date
group by 1,2)B
)
where A.Region = B.Region
and A.Bill_Year_Month = B.Bill_Year_Month
and A.Region <> ""
group by A.region,A.Bill_Year_Month;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Measure,
Bill_Year_Month,
AddresscapPercent
)		
select 
A.Region,
'BYMONTH' AS Measure,
A.Bill_Year_Month,
(A.c1*100)/B.c2 as AddresscapPercent 
from 
(
(select Region,Bill_Year_Month,
COUNT(DISTINCT Customer_Id) as c1
from Dashboard_Loyalty_Metrics_Temp 
where Address_Capture = "Y" 
and Bill_Year_Month BETWEEN @Start_Date AND @End_Date
group by 1,2)A
,
(select Region, Bill_Year_Month,
COUNT(DISTINCT Customer_Id) as c2
from Dashboard_Loyalty_Metrics_Temp 
WHERE Bill_Year_Month BETWEEN @Start_Date AND @End_Date
group by 1,2)B
)
where A.Region = B.Region
and A.Bill_Year_Month = B.Bill_Year_Month
and A.Region <> ""
group by A.region,A.Bill_Year_Month;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Measure,
Bill_Year_Month,
MobilecapPercent
)		
select 
A.Region,
'BYMONTH' AS Measure,
A.Bill_Year_Month,
(A.c1*100)/B.c2 as MobilecapPercent 
from 
(
(select Region,Bill_Year_Month,
COUNT(DISTINCT Customer_Id) as c1
from Dashboard_Loyalty_Metrics_Temp 
where Mobile_Capture = "Y" 
and Bill_Year_Month BETWEEN @Start_Date AND @End_Date
group by 1,2)A
,
(select Region, Bill_Year_Month,
COUNT(DISTINCT Customer_Id) as c2
from Dashboard_Loyalty_Metrics_Temp 
WHERE Bill_Year_Month BETWEEN @Start_Date AND @End_Date
group by 1,2)B
)
where A.Region = B.Region
and A.Bill_Year_Month = B.Bill_Year_Month
and A.Region <> ""
group by A.region,A.Bill_Year_Month;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Measure,
Bill_Year_Month,
NamecapPercent
)		
select 
A.Region,
'BYMONTH' AS Measure,
A.Bill_Year_Month,
(A.c1*100)/B.c2 as NamecapPercent 
from 
(
(select Region,Bill_Year_Month,
COUNT(DISTINCT Customer_Id) as c1
from Dashboard_Loyalty_Metrics_Temp 
where Name_Capture = "Y" 
and Bill_Year_Month BETWEEN @Start_Date AND @End_Date
group by 1,2)A
,
(select Region, Bill_Year_Month,
COUNT(DISTINCT Customer_Id) as c2
from Dashboard_Loyalty_Metrics_Temp 
WHERE Bill_Year_Month BETWEEN @Start_Date AND @End_Date
group by 1,2)B
)
where A.Region = B.Region
and A.Bill_Year_Month = B.Bill_Year_Month
and A.Region <> ""
group by A.region,A.Bill_Year_Month;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Measure,
Bill_Year_Month,
DOBcapPercent
)		
select 
A.Region,
'BYMONTH' AS Measure,
A.Bill_Year_Month,
(A.c1*100)/B.c2 as DOBcapPercent 
from 
(
(select Region,Bill_Year_Month,
COUNT(DISTINCT Customer_Id) as c1
from Dashboard_Loyalty_Metrics_Temp 
where DOB_Capture = "Y" 
and Bill_Year_Month BETWEEN @Start_Date AND @End_Date
group by 1,2)A
,
(select Region, Bill_Year_Month,
COUNT(DISTINCT Customer_Id) as c2
from Dashboard_Loyalty_Metrics_Temp 
WHERE Bill_Year_Month BETWEEN @Start_Date AND @End_Date
group by 1,2)B
)
where A.Region = B.Region
and A.Bill_Year_Month = B.Bill_Year_Month
and A.Region <> ""
group by A.region,A.Bill_Year_Month;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Measure,
Bill_Year_Month,
DOAcapPercent
)		
select 
A.Region,
'BYMONTH' AS Measure,
A.Bill_Year_Month,
(A.c1*100)/B.c2 as DOAcapPercent 
from 
(
(select Region,Bill_Year_Month,
COUNT(DISTINCT Customer_Id) as c1
from Dashboard_Loyalty_Metrics_Temp 
where Anniversary_Capture = "Y" 
and Bill_Year_Month BETWEEN @Start_Date AND @End_Date
group by 1,2)A
,
(select Region, Bill_Year_Month,
COUNT(DISTINCT Customer_Id) as c2
from Dashboard_Loyalty_Metrics_Temp 
WHERE Bill_Year_Month BETWEEN @Start_Date AND @End_Date
group by 1,2)B
)
where A.Region = B.Region
and A.Bill_Year_Month = B.Bill_Year_Month
and A.Region <> ""
group by A.region,A.Bill_Year_Month;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Measure,
Bill_Year_Month,
GendercapPercent
)		
select 
A.Region,
'BYMONTH' AS Measure,
A.Bill_Year_Month,
(A.c1*100)/B.c2 as GendercapPercent 
from 
(
(select Region,Bill_Year_Month,
COUNT(DISTINCT Customer_Id) as c1
from Dashboard_Loyalty_Metrics_Temp 
where Gender_Capture = "Y" 
and Bill_Year_Month BETWEEN @Start_Date AND @End_Date
group by 1,2)A
,
(select Region, Bill_Year_Month,
COUNT(DISTINCT Customer_Id) as c2
from Dashboard_Loyalty_Metrics_Temp 
WHERE Bill_Year_Month BETWEEN @Start_Date AND @End_Date
group by 1,2)B
)
where A.Region = B.Region
and A.Bill_Year_Month = B.Bill_Year_Month
and A.Region <> ""
group by A.region,A.Bill_Year_Month;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Measure,
EmailcapPercent
)		
select 
A.Region,
'TY' AS Measure,
(A.c1*100)/B.c2 as EmailcapPercent 
from 
(
(select Region,
COUNT(DISTINCT Customer_Id) as c1
from Dashboard_Loyalty_Metrics_Temp 
where Email_Capture = "Y" 
AND Bill_Year_Month BETWEEN @TYSM AND @TYEM
group by Region)A
,
(select Region, 
COUNT(DISTINCT Customer_Id) as c2
from Dashboard_Loyalty_Metrics_Temp 
WHERE Bill_Year_Month BETWEEN @TYSM AND @TYEM
group by Region)B
)
where A.Region = B.Region
and A.Region <> ""
group by A.region;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Measure,
AddresscapPercent
)		
select 
A.Region,
'TY' AS Measure,
(A.c1*100)/B.c2 as AddresscapPercent 
from 
(
(select Region,
COUNT(DISTINCT Customer_Id) as c1
from Dashboard_Loyalty_Metrics_Temp 
where Address_Capture = "Y" 
AND Bill_Year_Month BETWEEN @TYSM AND @TYEM
group by Region)A
,
(select Region, 
COUNT(DISTINCT Customer_Id) as c2
from Dashboard_Loyalty_Metrics_Temp 
WHERE Bill_Year_Month BETWEEN @TYSM AND @TYEM
group by Region)B
)
where A.Region = B.Region
and A.Region <> ""
group by A.region;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Measure,
MobilecapPercent
)		
select 
A.Region,
'TY' AS Measure,
(A.c1*100)/B.c2 as MobilecapPercent 
from 
(
(select Region,
COUNT(DISTINCT Customer_Id) as c1
from Dashboard_Loyalty_Metrics_Temp 
where Mobile_Capture = "Y" 
AND Bill_Year_Month BETWEEN @TYSM AND @TYEM
group by Region)A
,
(select Region, 
COUNT(DISTINCT Customer_Id) as c2
from Dashboard_Loyalty_Metrics_Temp 
WHERE Bill_Year_Month BETWEEN @TYSM AND @TYEM
group by Region)B
)
where A.Region = B.Region
and A.Region <> ""
group by A.region;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Measure,
NamecapPercent
)		
select 
A.Region,
'TY' AS Measure,
(A.c1*100)/B.c2 as NamecapPercent 
from 
(
(select Region,
COUNT(DISTINCT Customer_Id) as c1
from Dashboard_Loyalty_Metrics_Temp 
where Name_Capture = "Y" 
AND Bill_Year_Month BETWEEN @TYSM AND @TYEM
group by Region)A
,
(select Region, 
COUNT(DISTINCT Customer_Id) as c2
from Dashboard_Loyalty_Metrics_Temp 
WHERE Bill_Year_Month BETWEEN @TYSM AND @TYEM
group by Region)B
)
where A.Region = B.Region
and A.Region <> ""
group by A.region;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Measure,
DOBcapPercent
)		
select 
A.Region,
'TY' AS Measure,
(A.c1*100)/B.c2 as DOBcapPercent 
from 
(
(select Region,
COUNT(DISTINCT Customer_Id) as c1
from Dashboard_Loyalty_Metrics_Temp 
where DOB_Capture = "Y" 
AND Bill_Year_Month BETWEEN @TYSM AND @TYEM
group by Region)A
,
(select Region, 
COUNT(DISTINCT Customer_Id) as c2
from Dashboard_Loyalty_Metrics_Temp 
WHERE Bill_Year_Month BETWEEN @TYSM AND @TYEM
group by Region)B
)
where A.Region = B.Region
and A.Region <> ""
group by A.region;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Measure,
GendercapPercent
)		
select 
A.Region,
'TY' AS Measure,
(A.c1*100)/B.c2 as GendercapPercent 
from 
(
(select Region,
COUNT(DISTINCT Customer_Id) as c1
from Dashboard_Loyalty_Metrics_Temp 
where Gender_Capture = "Y" 
AND Bill_Year_Month BETWEEN @TYSM AND @TYEM
group by Region)A
,
(select Region, 
COUNT(DISTINCT Customer_Id) as c2
from Dashboard_Loyalty_Metrics_Temp 
WHERE Bill_Year_Month BETWEEN @TYSM AND @TYEM
group by Region)B
)
where A.Region = B.Region
and A.Region <> ""
group by A.region;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Measure,
DOAcapPercent
)		
select 
A.Region,
'TY' AS Measure,
(A.c1*100)/B.c2 as DOAcapPercent 
from 
(
(select Region,
COUNT(DISTINCT Customer_Id) as c1
from Dashboard_Loyalty_Metrics_Temp 
where Anniversary_Capture = "Y" 
AND Bill_Year_Month BETWEEN @TYSM AND @TYEM
group by Region)A
,
(select Region, 
COUNT(DISTINCT Customer_Id) as c2
from Dashboard_Loyalty_Metrics_Temp 
WHERE Bill_Year_Month BETWEEN @TYSM AND @TYEM
group by Region)B
)
where A.Region = B.Region
and A.Region <> ""
group by A.region;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Measure,
EmailcapPercent
)		
select 
A.Region,
'L12M' AS Measure,
(A.c1*100)/B.c2 as EmailcapPercent 
from 
(
(select Region,
COUNT(DISTINCT Customer_Id) as c1
from Dashboard_Loyalty_Metrics_Temp 
where Email_Capture = "Y" 
AND Bill_Year_Month BETWEEN @SMLY AND @TM
group by Region)A
,
(select Region, 
COUNT(DISTINCT Customer_Id) as c2
from Dashboard_Loyalty_Metrics_Temp 
WHERE Bill_Year_Month BETWEEN @SMLY AND @TM
group by Region)B
)
where A.Region = B.Region
and A.Region <> ""
group by A.region;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Measure,
AddresscapPercent
)		
select 
A.Region,
'L12M' AS Measure,
(A.c1*100)/B.c2 as AddresscapPercent 
from 
(
(select Region,
COUNT(DISTINCT Customer_Id) as c1
from Dashboard_Loyalty_Metrics_Temp 
where Address_Capture = "Y" 
AND Bill_Year_Month BETWEEN @SMLY AND @TM
group by Region)A
,
(select Region, 
COUNT(DISTINCT Customer_Id) as c2
from Dashboard_Loyalty_Metrics_Temp 
WHERE Bill_Year_Month BETWEEN @SMLY AND @TM
group by Region)B
)
where A.Region = B.Region
and A.Region <> ""
group by A.region;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Measure,
MobilecapPercent
)		
select 
A.Region,
'L12M' AS Measure,
(A.c1*100)/B.c2 as MobilecapPercent 
from 
(
(select Region,
COUNT(DISTINCT Customer_Id) as c1
from Dashboard_Loyalty_Metrics_Temp 
where Mobile_Capture = "Y" 
AND Bill_Year_Month BETWEEN @SMLY AND @TM
group by Region)A
,
(select Region, 
COUNT(DISTINCT Customer_Id) as c2
from Dashboard_Loyalty_Metrics_Temp 
WHERE Bill_Year_Month BETWEEN @SMLY AND @TM
group by Region)B
)
where A.Region = B.Region
and A.Region <> ""
group by A.region;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Measure,
NamecapPercent
)		
select 
A.Region,
'L12M' AS Measure,
(A.c1*100)/B.c2 as NamecapPercent 
from 
(
(select Region,
COUNT(DISTINCT Customer_Id) as c1
from Dashboard_Loyalty_Metrics_Temp 
where Name_Capture = "Y" 
AND Bill_Year_Month BETWEEN @SMLY AND @TM
group by Region)A
,
(select Region, 
COUNT(DISTINCT Customer_Id) as c2
from Dashboard_Loyalty_Metrics_Temp 
WHERE Bill_Year_Month BETWEEN @SMLY AND @TM
group by Region)B
)
where A.Region = B.Region
and A.Region <> ""
group by A.region;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Measure,
DOBcapPercent
)		
select 
A.Region,
'L12M' AS Measure,
(A.c1*100)/B.c2 as DOBcapPercent 
from 
(
(select Region,
COUNT(DISTINCT Customer_Id) as c1
from Dashboard_Loyalty_Metrics_Temp 
where DOB_Capture = "Y" 
AND Bill_Year_Month BETWEEN @SMLY AND @TM
group by Region)A
,
(select Region, 
COUNT(DISTINCT Customer_Id) as c2
from Dashboard_Loyalty_Metrics_Temp 
WHERE Bill_Year_Month BETWEEN @SMLY AND @TM
group by Region)B
)
where A.Region = B.Region
and A.Region <> ""
group by A.region;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Measure,
GendercapPercent
)		
select 
A.Region,
'L12M' AS Measure,
(A.c1*100)/B.c2 as GendercapPercent 
from 
(
(select Region,
COUNT(DISTINCT Customer_Id) as c1
from Dashboard_Loyalty_Metrics_Temp 
where Gender_Capture = "Y" 
AND Bill_Year_Month BETWEEN @SMLY AND @TM
group by Region)A
,
(select Region, 
COUNT(DISTINCT Customer_Id) as c2
from Dashboard_Loyalty_Metrics_Temp 
WHERE Bill_Year_Month BETWEEN @SMLY AND @TM
group by Region)B
)
where A.Region = B.Region
and A.Region <> ""
group by A.region;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Measure,
DOAcapPercent
)		
select 
A.Region,
'L12M' AS Measure,
(A.c1*100)/B.c2 as DOAcapPercent 
from 
(
(select Region,
COUNT(DISTINCT Customer_Id) as c1
from Dashboard_Loyalty_Metrics_Temp 
where Anniversary_Capture = "Y" 
AND Bill_Year_Month BETWEEN @SMLY AND @TM
group by Region)A
,
(select Region, 
COUNT(DISTINCT Customer_Id) as c2
from Dashboard_Loyalty_Metrics_Temp 
WHERE Bill_Year_Month BETWEEN @SMLY AND @TM
group by Region)B
)
where A.Region = B.Region
and A.Region <> ""
group by A.region;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Measure,
EmailcapPercent
)		
select 
A.Region,
'OVERALL' AS Measure,
(A.c1*100)/B.c2 as EmailcapPercent 
from 
(
(select Region,
COUNT(DISTINCT Customer_Id) as c1
from Dashboard_Loyalty_Metrics_Temp 
where Email_Capture = "Y" 
group by Region)A
,
(select Region, 
COUNT(DISTINCT Customer_Id) as c2
from Dashboard_Loyalty_Metrics_Temp 
group by Region)B
)
where A.Region = B.Region
and A.Region <> ""
group by A.region;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Measure,
AddresscapPercent
)		
select 
A.Region,
'OVERALL' AS Measure,
(A.c1*100)/B.c2 as AddresscapPercent 
from 
(
(select Region,
COUNT(DISTINCT Customer_Id) as c1
from Dashboard_Loyalty_Metrics_Temp 
where Address_Capture = "Y" 
group by Region)A
,
(select Region, 
COUNT(DISTINCT Customer_Id) as c2
from Dashboard_Loyalty_Metrics_Temp 
group by Region)B
)
where A.Region = B.Region
and A.Region <> ""
group by A.region;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Measure,
MobilecapPercent
)		
select 
A.Region,
'OVERALL' AS Measure,
(A.c1*100)/B.c2 as MobilecapPercent 
from 
(
(select Region,
COUNT(DISTINCT Customer_Id) as c1
from Dashboard_Loyalty_Metrics_Temp 
where Mobile_Capture = "Y" 
group by Region)A
,
(select Region, 
COUNT(DISTINCT Customer_Id) as c2
from Dashboard_Loyalty_Metrics_Temp 
group by Region)B
)
where A.Region = B.Region
and A.Region <> ""
group by A.region;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Measure,
NamecapPercent
)		
select 
A.Region,
'OVERALL' AS Measure,
(A.c1*100)/B.c2 as NamecapPercent 
from 
(
(select Region,
COUNT(DISTINCT Customer_Id) as c1
from Dashboard_Loyalty_Metrics_Temp 
where Name_Capture = "Y" 
group by Region)A
,
(select Region, 
COUNT(DISTINCT Customer_Id) as c2
from Dashboard_Loyalty_Metrics_Temp 
group by Region)B
)
where A.Region = B.Region
and A.Region <> ""
group by A.region;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Measure,
DOBcapPercent
)		
select 
A.Region,
'OVERALL' AS Measure,
(A.c1*100)/B.c2 as DOBcapPercent 
from 
(
(select Region,
COUNT(DISTINCT Customer_Id) as c1
from Dashboard_Loyalty_Metrics_Temp 
where DOB_Capture = "Y" 
group by Region)A
,
(select Region, 
COUNT(DISTINCT Customer_Id) as c2
from Dashboard_Loyalty_Metrics_Temp 
group by Region)B
)
where A.Region = B.Region
and A.Region <> ""
group by A.region;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Measure,
GendercapPercent
)		
select 
A.Region,
'OVERALL' AS Measure,
(A.c1*100)/B.c2 as GendercapPercent 
from 
(
(select Region,
COUNT(DISTINCT Customer_Id) as c1
from Dashboard_Loyalty_Metrics_Temp 
where Gender_Capture = "Y" 
group by Region)A
,
(select Region, 
COUNT(DISTINCT Customer_Id) as c2
from Dashboard_Loyalty_Metrics_Temp 
group by Region)B
)
where A.Region = B.Region
and A.Region <> ""
group by A.region;

INSERT INTO Dashboard_Loyalty_Region_Temp
(
Region,
Measure,
DOAcapPercent
)		
select 
A.Region,
'OVERALL' AS Measure,
(A.c1*100)/B.c2 as DOAcapPercent 
from 
(
(select Region,
COUNT(DISTINCT Customer_Id) as c1
from Dashboard_Loyalty_Metrics_Temp 
where Anniversary_Capture = "Y" 
group by Region)A
,
(select Region, 
COUNT(DISTINCT Customer_Id) as c2
from Dashboard_Loyalty_Metrics_Temp 
group by Region)B
)
where A.Region = B.Region
and A.Region <> ""
group by A.region;

END$$
DELIMITER ;
