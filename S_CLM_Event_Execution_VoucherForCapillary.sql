
CREATE OR REPLACE PROCEDURE `S_CLM_Event_Execution_VoucherForCapillary`(IN Event_ID_cur1 BIGINT(20),IN Communication_Template_ID_cur1 BIGINT(20),IN Event_Execution_Date_ID VARCHAR(64))
begin
set @Event_ID_cur1=Event_ID_cur1;
set @Communication_Template_ID_cur1=Communication_Template_ID_cur1;
set @Event_Execution_Date_ID=Event_Execution_Date_ID;

select Offer_Type_ID into @OfferType_ID FROM
    Communication_Template
WHERE
    ID = @Communication_Template_ID_cur1;

select OfferTypeId into @External_Offer_Type_ID from CLM_OfferType where OfferTypeName = 'External_Customer_Vouchers';

if @OfferType_ID = @External_Offer_Type_ID
THEN
	
	INSERT INTO Event_Execution_Voucher_Pending
    SELECT * from Event_Execution
    where Communication_Template_ID = @Communication_Template_ID_cur1;
    SELECT 
			`Value`
		INTO @Out_FileLocation FROM
			M_Config
		WHERE
			`Name` = 'File_Path';
	select Offer_Code into @Offer from Communication_Template where ID =@Communication_Template_ID_cur1;
    SET @Offer = replace(@Offer,' ','_');
	SET @Out_FileName = concat(@Out_FileLocation,'/CLM_Event_Execution_Voucher_Pending_',@Offer,'_',date_format(Current_Date(),"%Y%m%d"),'.csv');
    
    DELETE from Event_Execution
    where Communication_Template_ID = @Communication_Template_ID_cur1;    
    
    SET @s1 = Concat(' (Select b.Customer_Key, a.Offer_Code from Event_Execution_Voucher_Pending a JOIN CDM_Customer_Key_Lookup b ON a.Customer_Id = b.Customer_Id and a.Communication_Template_ID = ',@Communication_Template_ID_cur1,' )');
    SET @s2=concat(' SELECT "Customer_Key","Offer_Key" UNION ALL ',@s1,' INTO OUTFILE "',@Out_FileName,'" FIELDS TERMINATED BY "|" escaped by "\" LINES TERMINATED BY "\n"');
		PREPARE stmt from @s2;        
		execute stmt;
		deallocate prepare stmt;
END IF;

END

