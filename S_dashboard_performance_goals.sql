DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_performance_goals`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    
    SET IN_FILTER_CURRENTMONTH=substring(IN_FILTER_CURRENTMONTH,1,4);
    
    SET @FY_SATRT_DATE = date_format(concat(date_format(current_date(),"%Y%m")-100,'01'),"%Y-%m-%d");
	SET @FY_END_DATE = last_day(date_format(current_date(),"%Y-%m-%d"));
    
    CREATE OR REPLACE VIEW V_Dashboard_MTD_Incr_Rev
			as
			(select MAX(`EE_Date`) as `EE_Date`,MAX(`MTD_Incremental_Revenue`) as `Incremental_Revenue`,SUM(`Target_Revenue`) as `Target_Revenue`,MAX(`Total_Revenue_This_Month`) as `Total_Revenue_This_Month`,SUM(`Target_Delivered`) as `Target_Delivered`,SUM(`Target_Bills`) as `Target_Bills`,SUM(`Control_Bills`) as `Control_Bills`,`Event_ID`,`Channel`,`YM` from CLM_Event_Dashboard group by `Event_Id`,`YM`);
    
    
    IF IN_MEASURE="CARDS"
    THEN
		SET IN_NUMBER_FORMAT="PERCENT";
    END IF;
    
    IF IN_MEASURE = "NTR" OR IN_MEASURE = "CARDS"
	THEN
		SET @Query_Measure=(CASE WHEN IN_NUMBER_FORMAT='NUMBER' THEN ' ifnull(All_New_To_Repeat_N,0) ' ELSE ' round(ifnull(MAX(All_New_To_Repeat_P),0),1) ' END);
		SET @ntr_measure=@Query_Measure;
        SET @ntr_goal=(select Goal from Dashboard_CRM_Goals where Measure='NTR');
	END IF;
	IF IN_MEASURE = "IYR" OR IN_MEASURE = "CARDS"
	THEN
		SET @Query_Measure=(CASE WHEN IN_NUMBER_FORMAT='NUMBER' THEN ' ifnull(All_In_Year_Repeat_N,0) ' ELSE ' round(ifnull(MAX(All_In_Year_Repeat_P),0),1) ' END);
		SET @iyr_measure=@Query_Measure;
        SET @iyr_goal=(select Goal from Dashboard_CRM_Goals where Measure='IYR');
	END IF;
    IF IN_MEASURE = "RETENTION" OR IN_MEASURE = "CARDS"
	THEN
		SET @Query_Measure=(CASE WHEN IN_NUMBER_FORMAT='NUMBER' THEN ' ifnull(All_Active_Retention_N,0) ' ELSE ' round(ifnull(MAX(All_Active_Retention_P),0),1) ' END);
		SET @retention_measure=@Query_Measure;
        SET @retention_goal=(select Goal from Dashboard_CRM_Goals where Measure='Retention');
	END IF;
    IF IN_MEASURE = "WINBACK" OR IN_MEASURE = "CARDS"
	THEN
		SET @Query_Measure=(CASE WHEN IN_NUMBER_FORMAT='NUMBER' THEN ' ifnull(All_Dormant_Winback_N,0) ' ELSE ' round(ifnull(MAX(All_Dormant_Winback_P),0),1) ' END);
		SET @winback_measure=@Query_Measure;
        SET @winback_goal=(select Goal from Dashboard_CRM_Goals where Measure='Winback');
	END IF;
    IF IN_MEASURE = "MULTI" OR IN_MEASURE = "CARDS"
	THEN
		SET @Query_Measure=(CASE WHEN IN_NUMBER_FORMAT='NUMBER' THEN ' ifnull(All_Multi_N,0) ' ELSE ' round(ifnull(MAX(All_Multi_P),0),1) ' END);
		SET @multi_measure=@Query_Measure;
        SET @multi_goal=(select Goal from Dashboard_CRM_Goals where Measure='Multi');
	END IF;
    IF IN_MEASURE = "FREQUENCY" OR IN_MEASURE = "CARDS"
	THEN
		SET @Query_Measure=' round(ifnull(MAX(All_ATF),0),1) ';
		SET @frequency_measure=@Query_Measure;
        SET @frequency_goal=(select Goal from Dashboard_CRM_Goals where Measure='Frequency');
	END IF;
    IF IN_MEASURE = "ABV" OR IN_MEASURE = "CARDS"
	THEN
		SET @Query_Measure=' round(ifnull(MAX(All_ABV),0),0) ';
		SET @abv_measure=@Query_Measure;
        SET @abv_goal=(select Goal from Dashboard_CRM_Goals where Measure='ABV');
	END IF;
    IF IN_MEASURE = "LIFT" OR IN_MEASURE = "CARDS"
	THEN
		SET @IN_FILTER_CURRENTMONTH1=date_format(current_date(),"%Y%m");
			select @FY_SATRT_DATE,@FY_END_DATE;
            SET @filter_cond=concat(" EE_Date between '",@FY_SATRT_DATE,"' and '",@FY_END_DATE,"'");
			SET @date_select=' date_format(EE_DATE,"%b-%Y") AS "Year" ';
            SET @bill_cond = 'Total_Revenue_This_Month';
            SET @incr_Rev =' round(SUM(Incremental_Revenue),0) ';
		
        SET @lift_goal=(select Goal from Dashboard_CRM_Goals where Measure='Lift');
        SET @Query_Measure=concat(' (CASE WHEN SUM(Target_Revenue) = 0 THEN 0 ELSE round((',@incr_Rev,'/MAX(',@bill_cond,'))*100,2) END) ');
		SET @lift_measure=@Query_Measure;
        SET @table_used = 'V_Dashboard_MTD_Incr_Rev';
	END IF;
    
    
     IF IN_MEASURE='CARDS'
    THEN
		SELECT @ntr_measure,@ntr_goal,@iyr_measure,@iyr_goal,@retention_measure,@retention_goal,@winback_measure,@winback_goal,@multi_measure,@multi_goal,@frequency_measure,@frequency_goal,@abv_measure,@abv_goal,@lift_measure,@lift_goal;
		
        
        set @view_stmt=concat('create or replace view V_Dashboard_Goals_Card as
		(select "NTR" as Card_Name,concat(round(',@ntr_measure,',1),"%") as `Value1`,"',@ntr_goal,'" as `Value2`, "" as `Value3`,"NTR" as `Measure`  from CDM_CRM_KPIS_Dashboard WHERE KPI_Year_Fiscal = ',IN_FILTER_CURRENTMONTH,' ORDER BY KPI_Month_Fiscal DESC LIMIT 1)
		Union
		(select "IYR" as Card_Name,concat(round(',@iyr_measure,',1),"%") as `Value1`,"',@iyr_goal,'" as `Value2`, "" as `Value3`,"IYR" as `Measure`  from CDM_CRM_KPIS_Dashboard WHERE KPI_Year_Fiscal = ',IN_FILTER_CURRENTMONTH,' ORDER BY KPI_Month_Fiscal DESC LIMIT 1) 
		Union
		(select "Retention" as Card_Name,concat(round(',@retention_measure,',1),"%") as `Value1`,"',@retention_goal,'" as `Value2`, "" as `Value3`,"RETENTION" as `Measure`  from CDM_CRM_KPIS_Dashboard WHERE KPI_Year_Fiscal = ',IN_FILTER_CURRENTMONTH,' ORDER BY KPI_Month_Fiscal DESC LIMIT 1)
		Union
		(select "Winback" as Card_Name,concat(round(',@winback_measure,',1),"%") as `Value1`,"',@winback_goal,'" as `Value2`, "" as `Value3`,"WINBACK" as `Measure`  from CDM_CRM_KPIS_Dashboard WHERE KPI_Year_Fiscal = ',IN_FILTER_CURRENTMONTH,' ORDER BY KPI_Month_Fiscal DESC LIMIT 1)
		Union
		(select "Multi" as Card_Name,concat(round(',@multi_measure,',1),"%") as `Value1`,"',@multi_goal,'" as `Value2`, "" as `Value3`,"MULTI" as `Measure`  from CDM_CRM_KPIS_Dashboard WHERE KPI_Year_Fiscal = ',IN_FILTER_CURRENTMONTH,' ORDER BY KPI_Month_Fiscal DESC LIMIT 1)
		Union
		(select "Frequency" as Card_Name,',@frequency_measure,' as `Value1`,"',@frequency_goal,'" as `Value2`, "" as `Value3`,"FREQUENCY" as `Measure`  from CDM_CRM_KPIS_Dashboard WHERE KPI_Year_Fiscal = ',IN_FILTER_CURRENTMONTH,' ORDER BY KPI_Month_Fiscal DESC LIMIT 1)
		Union
		(select "ABV" as Card_Name,format(',@abv_measure,',",") as `Value1`,"',@abv_goal,'" as `Value2`, "" as `Value3`,"ABV" as `Measure`  from CDM_CRM_KPIS_Dashboard WHERE KPI_Year_Fiscal = ',IN_FILTER_CURRENTMONTH,' ORDER BY KPI_Month_Fiscal DESC LIMIT 1)
        Union
		(select "Lift as % of Total Rev" as Card_Name,concat(round(',@lift_measure,',1),"%") as `Value1`,"',@lift_goal,'" as `Value2`, "" as `Value3`,"LIFT" as `Measure`  from V_Dashboard_MTD_Incr_Rev WHERE YM = ',@IN_FILTER_CURRENTMONTH1,' ORDER BY YM DESC LIMIT 1)
		');
        
        
	SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Card_Name",`Card_Name`,"Value1",`Value1`,"Value2",`Value2`,"Value3",`Value3`,"Measure",`Measure`) )),"]")into @temp_result from V_Dashboard_Goals_Card;' ;
    ELSEIF IN_MEASURE='LIFT' THEN
		SELECT @date_select,@Query_Measure,@table_used,@filter_cond;
		SET @view_stmt=concat('CREATE OR REPLACE VIEW V_Dashboard_Lifts_Graph AS
						SELECT ',@date_select,',
                         ',@Query_Measure,' as "Value1",
                         ifnull((select ',@Query_Measure,'  from ',@table_used,' ced2
                         WHERE ',@filter_cond,' and ced2.YM=ced.YM-100 group by YM),0) as "Value2"
                         from ',@table_used,' ced
                         WHERE ',@filter_cond,'
                         GROUP BY 1
                         ORDER BY EE_Date
                        ');
		SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("time",`Year`,"TY",`Value1`,"LY",`Value2`) )),"]")into @temp_result from V_Dashboard_Lifts_Graph;' ;
       
    ELSE
    SELECT IN_FILTER_CURRENTMONTH,concat(KPI_Year,CASE WHEN KPI_Month<10 then concat(0,KPI_Month) else KPI_Month END),date_format(current_date(),"%Y%m") from CDM_CRM_KPIS_Dashboard
                         WHERE KPI_Year_Fiscal = IN_FILTER_CURRENTMONTH
                         and KPI_Month_Fiscal=12;
    SET @view_stmt=concat('CREATE OR REPLACE VIEW V_Dashboard_Goals_Graph AS
						
                        (SELECT date_format(date(concat("',IN_FILTER_CURRENTMONTH,'-",KPI_Month,"-01")),"%b-%y") as "Year",
                         case when concat(KPI_Year,CASE WHEN KPI_Month<10 then concat(0,KPI_Month) else KPI_Month END)>date_format(current_date(),"%Y%m") then -1 ELSE ',@Query_Measure,' END as "TY",
                         (select ',@Query_Measure,' from CDM_CRM_KPIS_Dashboard WHERE KPI_Year_Fiscal = ',IN_FILTER_CURRENTMONTH,'-1 and KPI_Month_Fiscal=1)as "LY"
                         from CDM_CRM_KPIS_Dashboard
                         WHERE KPI_Year_Fiscal = ',IN_FILTER_CURRENTMONTH,'
                         and KPI_Month_Fiscal=1)
                        UNION
                         (SELECT date_format(date(concat("',IN_FILTER_CURRENTMONTH,'-",KPI_Month,"-01")),"%b-%y") as "Year",
                         case when concat(KPI_Year,CASE WHEN KPI_Month<10 then concat(0,KPI_Month) else KPI_Month END)>date_format(current_date(),"%Y%m") then -1 ELSE ',@Query_Measure,' END as "TY",
                         (select ',@Query_Measure,' from CDM_CRM_KPIS_Dashboard WHERE KPI_Year_Fiscal = ',IN_FILTER_CURRENTMONTH,'-1 and KPI_Month_Fiscal=2)as "LY"
                         from CDM_CRM_KPIS_Dashboard
                         WHERE KPI_Year_Fiscal = ',IN_FILTER_CURRENTMONTH,'
                         and KPI_Month_Fiscal=2)
                         UNION
                         (SELECT date_format(date(concat("',IN_FILTER_CURRENTMONTH,'-",KPI_Month,"-01")),"%b-%y") as "Year",
                         case when concat(KPI_Year,CASE WHEN KPI_Month<10 then concat(0,KPI_Month) else KPI_Month END)>date_format(current_date(),"%Y%m") then -1 ELSE ',@Query_Measure,' END as "TY",
                         (select ',@Query_Measure,' from CDM_CRM_KPIS_Dashboard WHERE KPI_Year_Fiscal = ',IN_FILTER_CURRENTMONTH,'-1 and KPI_Month_Fiscal=3)as "LY"
                         from CDM_CRM_KPIS_Dashboard
                         WHERE KPI_Year_Fiscal = ',IN_FILTER_CURRENTMONTH,'
                         and KPI_Month_Fiscal=3)
                         UNION
                         (SELECT date_format(date(concat("',IN_FILTER_CURRENTMONTH,'-",KPI_Month,"-01")),"%b-%y") as "Year",
                         case when concat(KPI_Year,CASE WHEN KPI_Month<10 then concat(0,KPI_Month) else KPI_Month END)>date_format(current_date(),"%Y%m") then -1 ELSE ',@Query_Measure,' END as "TY",
                         (select ',@Query_Measure,' from CDM_CRM_KPIS_Dashboard WHERE KPI_Year_Fiscal = ',IN_FILTER_CURRENTMONTH,'-1 and KPI_Month_Fiscal=4)as "LY"
                         from CDM_CRM_KPIS_Dashboard
                         WHERE KPI_Year_Fiscal = ',IN_FILTER_CURRENTMONTH,'
                         and KPI_Month_Fiscal=4)
                         UNION
                         (SELECT date_format(date(concat("',IN_FILTER_CURRENTMONTH,'-",KPI_Month,"-01")),"%b-%y") as "Year",
                         case when concat(KPI_Year,CASE WHEN KPI_Month<10 then concat(0,KPI_Month) else KPI_Month END)>date_format(current_date(),"%Y%m") then -1 ELSE ',@Query_Measure,' END as "TY",
                         (select ',@Query_Measure,' from CDM_CRM_KPIS_Dashboard WHERE KPI_Year_Fiscal = ',IN_FILTER_CURRENTMONTH,'-1 and KPI_Month_Fiscal=5)as "LY"
                         from CDM_CRM_KPIS_Dashboard
                         WHERE KPI_Year_Fiscal = ',IN_FILTER_CURRENTMONTH,'
                         and KPI_Month_Fiscal=5)
                         UNION
                         (SELECT date_format(date(concat("',IN_FILTER_CURRENTMONTH,'-",KPI_Month,"-01")),"%b-%y") as "Year",
                         case when concat(KPI_Year,CASE WHEN KPI_Month<10 then concat(0,KPI_Month) else KPI_Month END)>date_format(current_date(),"%Y%m") then -1 ELSE ',@Query_Measure,' END as "TY",
                         (select ',@Query_Measure,' from CDM_CRM_KPIS_Dashboard WHERE KPI_Year_Fiscal = ',IN_FILTER_CURRENTMONTH,'-1 and KPI_Month_Fiscal=6)as "LY"
                         from CDM_CRM_KPIS_Dashboard
                         WHERE KPI_Year_Fiscal = ',IN_FILTER_CURRENTMONTH,'
                         and KPI_Month_Fiscal=6)
                         UNION
                         (SELECT date_format(date(concat("',IN_FILTER_CURRENTMONTH,'-",KPI_Month,"-01")),"%b-%y") as "Year",
                         case when concat(KPI_Year,CASE WHEN KPI_Month<10 then concat(0,KPI_Month) else KPI_Month END)>date_format(current_date(),"%Y%m") then -1 ELSE ',@Query_Measure,' END as "TY",
                         (select ',@Query_Measure,' from CDM_CRM_KPIS_Dashboard WHERE KPI_Year_Fiscal = ',IN_FILTER_CURRENTMONTH,'-1 and KPI_Month_Fiscal=7)as "LY"
                         from CDM_CRM_KPIS_Dashboard
                         WHERE KPI_Year_Fiscal = ',IN_FILTER_CURRENTMONTH,'
                         and KPI_Month_Fiscal=7)
                         UNION
                         (SELECT date_format(date(concat("',IN_FILTER_CURRENTMONTH,'-",KPI_Month,"-01")),"%b-%y") as "Year",
                         case when concat(KPI_Year,CASE WHEN KPI_Month<10 then concat(0,KPI_Month) else KPI_Month END)>date_format(current_date(),"%Y%m") then -1 ELSE ',@Query_Measure,' END as "TY",
                         (select ',@Query_Measure,' from CDM_CRM_KPIS_Dashboard WHERE KPI_Year_Fiscal = ',IN_FILTER_CURRENTMONTH,'-1 and KPI_Month_Fiscal=8)as "LY"
                         from CDM_CRM_KPIS_Dashboard
                         WHERE KPI_Year_Fiscal = ',IN_FILTER_CURRENTMONTH,'
                         and KPI_Month_Fiscal=8)
                         UNION
                         (SELECT date_format(date(concat("',IN_FILTER_CURRENTMONTH,'-",KPI_Month,"-01")),"%b-%y") as "Year",
                         case when concat(KPI_Year,CASE WHEN KPI_Month<10 then concat(0,KPI_Month) else KPI_Month END)>date_format(current_date(),"%Y%m") then -1 ELSE ',@Query_Measure,' END as "TY",
                         (select ',@Query_Measure,' from CDM_CRM_KPIS_Dashboard WHERE KPI_Year_Fiscal = ',IN_FILTER_CURRENTMONTH,'-1 and KPI_Month_Fiscal=9)as "LY"
                         from CDM_CRM_KPIS_Dashboard
                         WHERE KPI_Year_Fiscal = ',IN_FILTER_CURRENTMONTH,'
                         and KPI_Month_Fiscal=9)
                         UNION
                         (SELECT date_format(date(concat("',IN_FILTER_CURRENTMONTH+1,'-",KPI_Month,"-01")),"%b-%y") as "Year",
                         case when concat(KPI_Year,CASE WHEN KPI_Month<10 then concat(0,KPI_Month) else KPI_Month END)>date_format(current_date(),"%Y%m") then -1 ELSE ',@Query_Measure,' END as "TY",
                         (select ',@Query_Measure,' from CDM_CRM_KPIS_Dashboard WHERE KPI_Year_Fiscal = ',IN_FILTER_CURRENTMONTH,'-1 and KPI_Month_Fiscal=10)as "LY"
                         from CDM_CRM_KPIS_Dashboard
                         WHERE KPI_Year_Fiscal = ',IN_FILTER_CURRENTMONTH,'
                         and KPI_Month_Fiscal=10)
                         UNION
                         (SELECT date_format(date(concat("',IN_FILTER_CURRENTMONTH+1,'-",KPI_Month,"-01")),"%b-%y") as "Year",
                         case when concat(KPI_Year,CASE WHEN KPI_Month<10 then concat(0,KPI_Month) else KPI_Month END)>date_format(current_date(),"%Y%m") then -1 ELSE ',@Query_Measure,' END as "TY",
                         (select ',@Query_Measure,' from CDM_CRM_KPIS_Dashboard WHERE KPI_Year_Fiscal = ',IN_FILTER_CURRENTMONTH,'-1 and KPI_Month_Fiscal=11)as "LY"
                         from CDM_CRM_KPIS_Dashboard
                         WHERE KPI_Year_Fiscal = ',IN_FILTER_CURRENTMONTH,'
                         and KPI_Month_Fiscal=11)
                         UNION
                         (SELECT date_format(date(concat("',IN_FILTER_CURRENTMONTH+1,'-",KPI_Month,"-01")),"%b-%y") as "Year",
                         case when concat(KPI_Year,CASE WHEN KPI_Month<10 then concat(0,KPI_Month) else KPI_Month END)>date_format(current_date(),"%Y%m") then -1 ELSE ',@Query_Measure,' END as "TY",
                         (select ',@Query_Measure,' from CDM_CRM_KPIS_Dashboard WHERE KPI_Year_Fiscal = ',IN_FILTER_CURRENTMONTH,'-1 and KPI_Month_Fiscal=12)as "LY"
                         from CDM_CRM_KPIS_Dashboard
                         WHERE KPI_Year_Fiscal = ',IN_FILTER_CURRENTMONTH,'
                         and KPI_Month_Fiscal=12)
                        ');
	SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("time",`Year`,"TY",`TY`,"LY",`LY`) )),"]")into @temp_result from V_Dashboard_Goals_Graph;' ;
	END IF;
    select @view_stmt;
	PREPARE statement from @view_stmt;
	Execute statement;
	Deallocate PREPARE statement;
    IF IN_SCREEN <> "DOWNLOAD"
    THEN
		select @Query_String;
		PREPARE statement from @Query_String;
		Execute statement;
		Deallocate PREPARE statement; 
		SET out_result_json = @temp_result;
	ELSE
		call S_dashboard_performance_download('V_Dashboard_Goals_Graph',@out_result_json1);
		select @out_result_json1 into out_result_json;
    END IF;
    
    
END$$
DELIMITER ;
