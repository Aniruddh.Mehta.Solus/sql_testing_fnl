DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_QSR_Repeat`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    
    
     SET @FY_SATRT_DATE = date_format(date_sub(concat(IN_FILTER_CURRENTMONTH,'01'), INTERVAL 11 MONTH),"%Y%m");
	 SET @FY_END_DATE = IN_FILTER_CURRENTMONTH;
	 SET @TM = IN_FILTER_CURRENTMONTH;
	 SET @LM = F_Month_Diff(IN_FILTER_CURRENTMONTH,1);
	 SET @SMLY = F_Month_Diff(IN_FILTER_CURRENTMONTH,12);
	 SET @TY=substr(@TM,1,4);
	 SET @LY=substr(@SMLY,1,4);
	 SET @LLY = @LY - 1;
     SET @SMLLY = F_Month_Diff(IN_FILTER_CURRENTMONTH,24);
     SELECT @TM,@LM,@SMLY,@TY,@LY,@LLY,@SMLLY;
	
	IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_1 = 'NEW_EXISTING'
	THEN 
			SELECT "in TABLE NEW_EXISTING";
			set @view_stmt = concat("
            create or replace  View V_Dashboard_QSR_Repeat_Table as
							SELECT Platform as 'Platform', New_count as 'NEW', Existing_count as 'EXISITNG', 
                            new_per as '%NEW', existing_per as '%EXISTING'
							from Dashboard_QSR_Repeat
							where Bill_Year_Month = ",@TM,"
							GROUP BY 1
							;");
		
							SET @Query_String =  
                            'SELECT CONCAT("[",(GROUP_CONCAT(json_object(
                            "Platform",`Platform`,
                            "NEW",CAST(format(ifnull(`NEW`,0),",") AS CHAR),
                            "EXISITNG",CAST(format(ifnull(`EXISITNG`,0),",") AS CHAR),
                            "%NEW",concat(round(ifnull(`%NEW`,0),2),"%"),
                            "%EXISTING",concat(round(ifnull(`%EXISTING`,0),2),"%")
                            
                            ) )),"]")into @temp_result from V_Dashboard_QSR_Repeat_Table;' ;
		

END IF;

IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_1 = 'BASE_REPEAT'
then
	SELECT "in TABLE NEW_EXISTING";
			set @view_stmt = concat("
            create or replace  View V_Dashboard_QSR_Repeat_Table as
							select * from Dashboard_QSR_Base
							;");
		
							SET @Query_String =  
                            'SELECT CONCAT("[",(GROUP_CONCAT(json_object(
                            "Platform",`Platform`,
                            "BASE",CAST(format(ifnull(`Base_count`,0),",") AS CHAR),
                            "REPEAT",CAST(format(ifnull(`Repeat_count`,0),",") AS CHAR),
                            "%REPEAT",concat(round(ifnull(`Repeat_per`,0),2),"%")
                            
                            ) )),"]")into @temp_result from V_Dashboard_QSR_Repeat_Table;' ;
END IF;
	
	IF IN_SCREEN <> "DOWNLOAD"
    THEN
		select @view_stmt;
		PREPARE statement from @view_stmt;
		Execute statement;
		Deallocate PREPARE statement;
		select @Query_String;
		PREPARE statement from @Query_String;
		Execute statement;
		Deallocate PREPARE statement;	
		SET out_result_json = @temp_result;
	ELSE
		call S_dashboard_performance_download('V_Dashboard_QSR_Repeat_Table',@out_result_json1);
		select @out_result_json1 into out_result_json;
    END IF;
	
END$$
DELIMITER ;
