DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_performance_trigger`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    
    
    
    
    
    
    
    
	
    
    select date_format(date_sub(CURRENT_DATE(),interval 12 month),"%Y%m") into @FY_SATRT_DATE;
	SET @FY_END_DATE =date_format(current_date(),"%Y%m");
    
     select Value1 into @Incremental_Revenue_Method from UI_Configuration_Global where Config_Name = 'Incremental_Revenue_Method';
    SET @Incremental_Revenue_Method=ifnull(@Incremental_Revenue_Method,'Method2');
    select @Incremental_Revenue_Method;
    IF @Incremental_Revenue_Method = 'Method1'
    THEN
		SET @incr_rev_concat='_By_Day';
        SET @group_cond1='EE_Date';
        SET @mtd_concat='';
	ELSEIF @Incremental_Revenue_Method = 'Method2'
    THEN
		SET @group_cond1='YM';
		SET @incr_rev_concat='';
        SET @mtd_concat='MTD_';
	ELSE
		SET @incr_rev_concat='';
        SET @mtd_concat='';
		SET @incr_Rev = ' round((CASE WHEN SUM(Control_Base) < 0 or SUM(Target_Delivered) = 0 then 0 WHEN (((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*100 < 0 then 0 WHEN SUM(Control_Base)>0 and SUM(Control_Responder)=0 then SUM(Target_Revenue)
					ELSE ifnull((((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*(SUM(Target_Revenue)/SUM(Target_Responder))*(SUM(Target_Delivered)),0) END),0) ';
		SET @group_cond1='YM';
	END IF;
    SELECT @mtd_concat;
   
   SET @sql1=CONCAT('
		CREATE OR REPLACE VIEW CLM_Event_Dashboard_STLT
        as 
		SELECT `CLM_Event_Dashboard`.`Revenue Center`,
			`CLM_Event_Dashboard`.`Segment`,
			`CLM_Event_Dashboard`.`Campaign`,
			`CLM_Event_Dashboard`.`EE_Date`,
			`CLM_Event_Dashboard`.`Event_Execution_Date_ID`,
			`CLM_Event_Dashboard`.`Theme`,
			`CLM_Event_Dashboard`.`Offer`,
			`CLM_Event_Dashboard`.`Channel`,
			`CLM_Event_Dashboard`.`Has_Recommendations`,
			`CLM_Event_Dashboard`.`Event_ID`,
			`CLM_Event_Dashboard`.`Control_Base` as `Control_Base`,
			`CLM_Event_Dashboard`.`Target_Base`,
			`CLM_Event_Dashboard`.`Target_Delivered`,
			`CLM_Event_Dashboard`.`Control_Responder` as `Control_Responder`,
			`CLM_Event_Dashboard`.`Target_Responder`,
			`CLM_Event_Dashboard`.`Target_Revenue`,
			`CLM_Event_Dashboard`.`Target_Discount`,
			`CLM_Event_Dashboard`.`Target_Bills`,
			`CLM_Event_Dashboard`.`Target_Open`,
			`CLM_Event_Dashboard`.`Target_CTA`,
			`CLM_Event_Dashboard`.`Control_Revenue`,
			`CLM_Event_Dashboard`.`Control_Discount`,
			`CLM_Event_Dashboard`.`Control_Bills`,
			`CLM_Event_Dashboard`.`Created_Date`,
			`CLM_Event_Dashboard`.`Total_Customer_Base`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Day`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Month`,
			`CLM_Event_Dashboard`.`YM`,
			`CLM_Event_Dashboard`.`Incremental_Revenue',@incr_rev_concat,'` AS `Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue',@incr_rev_concat,'` AS `MTD_Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Segment',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Segment`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Revenue_Center',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Revenue_Center`
		FROM `CLM_Event_Dashboard`;
        ');
        select @sql1;
		PREPARE statement from @sql1;
		Execute statement;
		Deallocate PREPARE statement;
 
 
    CREATE OR REPLACE VIEW V_Dashboard_CLM_Event_Trigger_Report AS
   select CASE WHEN datediff(End_Date_ID,Start_Date_ID) < 32 THEN 'GTM' else 'CLM' END AS `Category`,b.Name as `Trigger`, b.CampaignName as `CampaignName`, a.* from CLM_Event_Dashboard_STLT a left join Event_Master b on a.Event_ID = b.ID;
   
   
   
   SET @create_view=CONCAT('CREATE OR REPLACE VIEW V_Dashboard_CLM_Event_Trigger_Report_Temp
   as
   select Category,
   Theme,
   CampaignName,
   `Trigger`,
	`Channel`,
   YM,
   Event_Id,
   EE_Date,
   SUM(`Target_Base`) AS Target_Base,
   SUM(`Target_Delivered`) AS Target_Delivered,
   SUM(`Control_Base`) AS Control_Base,
   SUM(`Target_Responder`) AS Target_Responder,
   SUM(`Control_Responder`) AS Control_Responder,
   SUM(Target_Revenue) AS Target_Revenue,
   MAX(MTD_Incremental_Revenue) AS Incremental_Revenue
   from V_Dashboard_CLM_Event_Trigger_Report
   group by Event_Id,',@group_cond1,'');
   
   select @create_view;
	PREPARE statement from @create_view;
	Execute statement;
	Deallocate PREPARE statement;
   
   
    
    
    
    SET @tgcg_measure=' round(CASE WHEN (CASE WHEN SUM(`Target_Delivered`) <=0 THEN 0 
                    ELSE (SUM(`Target_Responder`)/SUM(`Target_Delivered`))*100 END)-(CASE WHEN SUM(`Control_Base`) <=0 THEN 0 ELSE (SUM(`Control_Responder`)/SUM(`Control_Base`))*100 END)<0 THEN 0 ELSE
(CASE WHEN SUM(`Target_Delivered`) <=0 THEN 0 
                    ELSE (SUM(`Target_Responder`)/SUM(`Target_Delivered`))*100 END)-(CASE WHEN SUM(`Control_Base`) <=0 THEN 0 ELSE (SUM(`Control_Responder`)/SUM(`Control_Base`))*100 END)END,2) ';
    
    IF IN_MEASURE = "TG RESPONSE" OR IN_MEASURE = "CARDS"
	THEN
		SET @Query_Measure=' CASE WHEN SUM(Target_Base)>0 THEN round((SUM(Target_Responder)/SUM(Target_Delivered))*100,2) ELSE "-" END';
		SET @lift_measure=@Query_Measure;
        SET @table_used = 'V_Dashboard_CLM_Event_Trigger_Report';
	END IF;
    IF IN_MEASURE = "TG-CG" OR IN_MEASURE = "CARDS"
	THEN
		SET @Query_Measure=' round(CASE WHEN (CASE WHEN SUM(`Target_Delivered`) <=0 THEN 0 
                    ELSE (SUM(`Target_Responder`)/SUM(`Target_Delivered`))*100 END)-(CASE WHEN SUM(`Control_Base`) <=0 THEN 0 ELSE (SUM(`Control_Responder`)/SUM(`Control_Base`))*100 END)<0 THEN 0 ELSE
(CASE WHEN SUM(`Target_Delivered`) <=0 THEN 0 
                    ELSE (SUM(`Target_Responder`)/SUM(`Target_Delivered`))*100 END)-(CASE WHEN SUM(`Control_Base`) <=0 THEN 0 ELSE (SUM(`Control_Responder`)/SUM(`Control_Base`))*100 END)END,2) ';
		SET @tgcg_measure=@Query_Measure;
        SET @table_used = 'V_Dashboard_CLM_Event_Trigger_Report';
	END IF;
	IF IN_MEASURE = "INCR REVENUE" OR IN_MEASURE = "CARDS" 
	THEN
   
		SET @Query_Measure=' CASE WHEN SUM(Target_Base) >0 THEN round(SUM(Incremental_Revenue),0) else "-" END ';
        SET @tgresp_pct=' round((SUM(Target_Responder)/SUM(Target_Delivered))*100,1) ';
        SET @tgresp_num=' SUM(Target_Responder) ';
		SET @tgresp_measure=@Query_Measure;
        SET @table_used = 'V_Dashboard_CLM_Event_Trigger_Report';
	END IF;
    
        SET @top_query = '';
        select @FY_SATRT_DATE , @FY_END_DATE;
        select group_concat(DISTINCT DATE_FORMAT(EE_Date,"%b-%y") order by EE_Date) into @distinct_rc from V_Dashboard_CLM_Event_Trigger_Report where YM between @FY_SATRT_DATE and @FY_END_DATE ORDER BY YM;
        select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
		set @loop_var=1;
		set @json_select='';
		select @loop_var,@num_of_rc,@distinct_rc;
		WHILE @loop_var<=@num_of_rc
		do
        SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
		set @top_query=concat(@top_query,",MAX(CASE WHEN date_format(EE_Date,'%b-%y') = '",@selected_rc,"' Then pct else '-' END) AS `",@selected_rc,"`");
			set @json_select=concat(@json_select,',"',@selected_rc,'",CASE WHEN `',@selected_rc,'` <> "-" and `',@selected_rc,'` >100 THEN format(`',@selected_rc,'` ,",") ELSE `',@selected_rc,'` END');
			set @loop_var=@loop_var+1;
		end while;
        
		SET @view_stmt=concat('create or replace view Dashboard_Trigger_drill as
		select `Type`,`Theme`,`Campaign`,`Trigger`,`Channel`,round(ifnull((select count(distinct YM) from (select YM,',@tgcg_measure,' AS qm,`Trigger` from V_Dashboard_CLM_Event_Trigger_Report group by Event_Id,YM)f where f.`Trigger`=C.`Trigger` and f.qm>0)/(select count(distinct YM) from (select YM,SUM(Target_Base) as qm,`Trigger` from V_Dashboard_CLM_Event_Trigger_Report group by Event_Id,YM) g where g.`Trigger`=C.`Trigger` and g.qm>0 ),0)*100,0) AS "Sucess_Rate"',@top_query,' from 
		(select EE_Date,YM, (',@Query_Measure,') pct,`Category` AS "Type",`Theme`,`CampaignName` as "Campaign",`Trigger`,`Channel` from V_Dashboard_CLM_Event_Trigger_Report_Temp a
		where YM between ',@FY_SATRT_DATE,' and ',@FY_END_DATE,'
		group by YM,Event_Id
        )C
		group by `Trigger`
        order by YM');
        

        
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Type",`Type`,"Theme",`Theme`,"Campaign",`Campaign`,"Trigger",`Trigger`,"Channel",`Channel`,"Success",CAST(concat(`Sucess_Rate`,"%") AS CHAR)',@json_select,') )),"]")into @temp_result from Dashboard_Trigger_drill where `Trigger` is not null;') ;
	
    
    IF IN_MEASURE <> "DOWNLOAD"
    THEN
	select @view_stmt;
	PREPARE statement from @view_stmt;
	Execute statement;
	Deallocate PREPARE statement;
    
	select @Query_String;
	PREPARE statement from @Query_String;
	Execute statement;
	Deallocate PREPARE statement; 
    SET out_result_json = @temp_result;
    END IF;
    
END$$
DELIMITER ;
