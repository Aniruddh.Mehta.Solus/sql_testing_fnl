DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_performance_month_report`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
    declare IN_AGGREGATION_4 text;
    declare IN_AGGREGATION_5 text;
    declare IN_AGGREGATION_6 text;
    declare IN_AGGREGATION_7 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
    SET IN_AGGREGATION_4 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_4"));
    SET IN_AGGREGATION_5 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_5"));
    SET IN_AGGREGATION_6 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_6"));
    SET IN_AGGREGATION_7 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_7"));
    
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    
    select Value1 into @Incremental_Revenue_Method from UI_Configuration_Global where Config_Name = 'Incremental_Revenue_Method';
    SET @Incremental_Revenue_Method=ifnull(@Incremental_Revenue_Method,'Method2');
    select @Incremental_Revenue_Method;
    IF @Incremental_Revenue_Method = 'Method1'
    THEN
		SET @incr_rev_concat='_By_Day';
        SET @group_cond1='EE_Date';
        SET @mtd_concat='';
	ELSEIF @Incremental_Revenue_Method = 'Method2'
    THEN
		SET @group_cond1='YM';
		SET @incr_rev_concat='';
        SET @mtd_concat='MTD_';
	ELSE
		SET @incr_rev_concat='';
        SET @mtd_concat='';
		SET @incr_Rev = ' round((CASE WHEN SUM(Control_Base) < 0 or SUM(Target_Delivered) = 0 then 0 WHEN (((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*100 < 0 then 0 WHEN SUM(Control_Base)>0 and SUM(Control_Responder)=0 then SUM(Target_Revenue)
					ELSE ifnull((((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*(SUM(Target_Revenue)/SUM(Target_Responder))*(SUM(Target_Delivered)),0) END),0) ';
		SET @group_cond1='YM';
	END IF;
    SELECT @mtd_concat;
    
    IF IN_AGGREGATION_3 = 'ST'
    THEN
    SET @sql1=CONCAT('
		CREATE OR REPLACE VIEW CLM_Event_Dashboard_STLT
        as 
		SELECT `CLM_Event_Dashboard`.`Revenue Center`,
			`CLM_Event_Dashboard`.`Segment`,
			`CLM_Event_Dashboard`.`Campaign`,
			`CLM_Event_Dashboard`.`EE_Date`,
			`CLM_Event_Dashboard`.`Event_Execution_Date_ID`,
			`CLM_Event_Dashboard`.`Theme`,
			`CLM_Event_Dashboard`.`Offer`,
			`CLM_Event_Dashboard`.`Channel`,
			`CLM_Event_Dashboard`.`Has_Recommendations`,
			`CLM_Event_Dashboard`.`Event_ID`,
			`CLM_Event_Dashboard`.`Control_Base_Event` as `Control_Base`,
			`CLM_Event_Dashboard`.`Target_Base`,
			`CLM_Event_Dashboard`.`Target_Delivered`,
			`CLM_Event_Dashboard`.`Target_Delivered_NA`,
			`CLM_Event_Dashboard`.`Control_Responder_Event` as `Control_Responder`,
			`CLM_Event_Dashboard`.`Target_Responder`,
			`CLM_Event_Dashboard`.`Target_Revenue`,
			`CLM_Event_Dashboard`.`Target_Discount`,
			`CLM_Event_Dashboard`.`Target_Bills`,
			`CLM_Event_Dashboard`.`Target_Open`,
			`CLM_Event_Dashboard`.`Target_CTA`,
			`CLM_Event_Dashboard`.`Control_Revenue`,
			`CLM_Event_Dashboard`.`Control_Discount`,
			`CLM_Event_Dashboard`.`Control_Bills`,
			`CLM_Event_Dashboard`.`Created_Date`,
			`CLM_Event_Dashboard`.`Total_Customer_Base`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Day`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Month`,
			`CLM_Event_Dashboard`.`YM`,
			`CLM_Event_Dashboard`.`Incremental_Revenue_ST',@incr_rev_concat,'` AS `Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_ST',@incr_rev_concat,'` AS `MTD_Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Segment_ST',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Segment`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Revenue_Center_ST',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Revenue_Center`
		FROM `CLM_Event_Dashboard`;
        ');
        select @sql1;
		PREPARE statement from @sql1;
		Execute statement;
		Deallocate PREPARE statement;
    ELSE
		SET @sql1=CONCAT('
		CREATE OR REPLACE VIEW CLM_Event_Dashboard_STLT
        as 
		SELECT `CLM_Event_Dashboard`.`Revenue Center`,
			`CLM_Event_Dashboard`.`Segment`,
			`CLM_Event_Dashboard`.`Campaign`,
			`CLM_Event_Dashboard`.`EE_Date`,
			`CLM_Event_Dashboard`.`Event_Execution_Date_ID`,
			`CLM_Event_Dashboard`.`Theme`,
			`CLM_Event_Dashboard`.`Offer`,
			`CLM_Event_Dashboard`.`Channel`,
			`CLM_Event_Dashboard`.`Has_Recommendations`,
			`CLM_Event_Dashboard`.`Event_ID`,
			`CLM_Event_Dashboard`.`Control_Base` as `Control_Base`,
			`CLM_Event_Dashboard`.`Target_Base`,
			`CLM_Event_Dashboard`.`Target_Delivered`,
			`CLM_Event_Dashboard`.`Target_Delivered_NA`,
			`CLM_Event_Dashboard`.`Control_Responder` as `Control_Responder`,
			`CLM_Event_Dashboard`.`Target_Responder`,
			`CLM_Event_Dashboard`.`Target_Revenue`,
			`CLM_Event_Dashboard`.`Target_Discount`,
			`CLM_Event_Dashboard`.`Target_Bills`,
			`CLM_Event_Dashboard`.`Target_Open`,
			`CLM_Event_Dashboard`.`Target_CTA`,
			`CLM_Event_Dashboard`.`Control_Revenue`,
			`CLM_Event_Dashboard`.`Control_Discount`,
			`CLM_Event_Dashboard`.`Control_Bills`,
			`CLM_Event_Dashboard`.`Created_Date`,
			`CLM_Event_Dashboard`.`Total_Customer_Base`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Day`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Month`,
			`CLM_Event_Dashboard`.`YM`,
			`CLM_Event_Dashboard`.`Incremental_Revenue',@incr_rev_concat,'` AS `Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue',@incr_rev_concat,'` AS `MTD_Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Segment',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Segment`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Revenue_Center',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Revenue_Center`
		FROM `CLM_Event_Dashboard`;
        ');
        select @sql1;
		PREPARE statement from @sql1;
		Execute statement;
		Deallocate PREPARE statement;
    END IF;
     SET @wherecond1 = ' 1=1 ';
	SET @wherecond2 = ' 1=1 ';
    
    
    
    CREATE OR REPLACE VIEW V_Dashboard_CLM_Event_Month_Report AS
   select Type_Of_Event AS `Category`,b.Name as `Trigger`, b.CampaignName as `CampaignName`,a.Segment AS `CLMSegmentName`, a.* from CLM_Event_Dashboard_STLT a, Event_Master b, Event_Master_UI c where a.Event_ID = b.ID AND b.ID = c.Event_Id;
   
   IF IN_AGGREGATION_2 = 'CG'
   THEN
		SET @cg_clause='100';
	ELSE
		SET @cg_clause='0';
	END IF;
		

   set @json_select2 = '';
   IF IN_AGGREGATION_1 = 'Category'
   THEN 
		SET @select_query='Category';
        set @group_cond='1';
        set @json_select = '"Category",`Category`';
        set @json_select2 = '"Category",`Category`'; 
        SET @attr = 'Category';
        SET @wherecond1 = ' 1=1 ';
        SET @wherecond2 = ' 1=1 ';
        
   END IF;
   
   IF IN_AGGREGATION_1 = 'Theme'
   THEN 
		SET @select_query='Category,Theme';
        set @group_cond='1,2';
        set @json_select = '"Category",`Category`,"Theme",`Theme`';
        set @json_select2 = '"Theme",`Theme`';
        SET @attr = 'Theme';
        SET @wherecond1 = ' 1=1 ';
        SET @wherecond2 = ' 1=1 ';
   END IF;
   
   IF IN_AGGREGATION_1 = 'Campaign'
   THEN 
		SET @select_query='Category,Theme,CampaignName';
        set @group_cond='1,2,3';
        set @json_select = '"Category",`Category`,"Theme",`Theme`,"Campaign",`CampaignName`';
        set @json_select2 = '"Campaign",`CampaignName`';
        SET @attr = 'CampaignName';
        SET @wherecond1 = ' 1=1 ';
        SET @wherecond2 = ' 1=1 ';
   END IF;
   
   IF IN_AGGREGATION_1 = 'Trigger'
   THEN 
		SET @select_query='Category,Theme,CampaignName,`Trigger`';
        set @group_cond='1,2,3,4';
        set @json_select = '"Category",`Category`,"Theme",`Theme`,"Campaign",`CampaignName`,"Trigger",`Trigger`';
        set @json_select2 = '"Trigger",`Trigger`';
        SET @attr = '`Trigger`';
        IF IN_AGGREGATION_4 = ''
        THEN
        SET @wherecond1 = ' 1=1 ';
        ELSE
        SET @wherecond1 = CONCAT(' CLMSegmentName="',IN_AGGREGATION_4,'" ');
        END IF;
        IF IN_AGGREGATION_5 = ''
        THEN
        SET @wherecond2 = ' 1=1 ';
        ELSE
        SET @wherecond2 = CONCAT(' Channel="',IN_AGGREGATION_5,'" ');
        END IF;
   END IF;
   
   IF IN_AGGREGATION_1 = 'REVCENTER'
   THEN 
		SET @select_query='`Revenue Center` AS `Revenue Center`';
        set @group_cond='1';
        set @json_select2 = '"Revenue Center",`Revenue Center`';
        SET @attr = '`Revenue Center`';
        SET @wherecond1 = ' 1=1 ';
        SET @wherecond2 = ' 1=1 ';
   END IF;
   
   IF IN_AGGREGATION_1 = 'SEGMENT'
   THEN 
		SET @select_query='`Segment` AS `Segment`';
        set @group_cond='1';
        set @json_select2 = '"Segment",`Segment`';
        SET @attr = '`Segment`';
        SET @wherecond1 = ' 1=1 ';
        SET @wherecond2 = ' 1=1 ';
   END IF;
   
   IF IN_AGGREGATION_1 = 'CHANNEL'
   THEN 
		SET @select_query='`Channel` AS `Channel`';
        set @group_cond='1';
        set @json_select2 = '"Channel",`Channel`';
        SET @attr = '`Channel`';
        SET @wherecond1 = ' 1=1 ';
        SET @wherecond2 = ' 1=1 ';
   END IF;
  
   IF IN_AGGREGATION_1 = 'REVCENTER'
   THEN
   SET @create_view=CONCAT('CREATE OR REPLACE VIEW V_Dashboard_Month_Report_Temp
   as
   select
   `Revenue Center`,
   YM,
   Event_Id,
   EE_Date,
   Category,
   Theme,
   CampaignName,
   `Trigger`,
   SUM(`Target_Base`) AS Target_Base,
   SUM(`Target_Delivered`) AS Target_Delivered,
   SUM(`Target_Delivered_NA`) AS Target_Delivered_NA,
   SUM(`Control_Base`) AS Control_Base,
   SUM(`Target_Responder`) AS Target_Responder,
   SUM(`Control_Responder`) AS Control_Responder,
   SUM(Target_Revenue) AS Target_Revenue,
   MAX(MTD_Incremental_Revenue_By_Revenue_Center) AS Incremental_Revenue
   from V_Dashboard_CLM_Event_Month_Report
   group by `Revenue Center`,Event_Id,',@group_cond1,'');
   
   ELSEIF IN_AGGREGATION_1 = 'SEGMENT'
   THEN
      SET @create_view=CONCAT('CREATE OR REPLACE VIEW V_Dashboard_Month_Report_Temp
   as
   select
   `Segment`,
   YM,
   Event_Id,
   EE_Date,
   Category,
   Theme,
   Channel,
   CampaignName,
   `Trigger`,
   SUM(`Target_Base`) AS Target_Base,
   SUM(`Target_Delivered`) AS Target_Delivered,
   SUM(`Target_Delivered_NA`) AS Target_Delivered_NA,
   SUM(`Control_Base`) AS Control_Base,
   SUM(`Target_Responder`) AS Target_Responder,
   SUM(`Control_Responder`) AS Control_Responder,
   SUM(Target_Revenue) AS Target_Revenue,
   MAX(MTD_Incremental_Revenue_By_Segment) AS Incremental_Revenue
   from V_Dashboard_CLM_Event_Month_Report
   group by `Segment`,Event_Id,',@group_cond1,'');
   
   ELSE
   SET @create_view=CONCAT('CREATE OR REPLACE VIEW V_Dashboard_Month_Report_Temp
   as
   select Category,
   Theme,
   Channel,
   CampaignName,
   CLMSegmentName,
   `Trigger`,
   YM,
   Event_Id,
   EE_Date,
   SUM(`Target_Base`) AS Target_Base,
   SUM(`Target_Delivered`) AS Target_Delivered,
   SUM(`Target_Delivered_NA`) AS Target_Delivered_NA,
   SUM(`Control_Base`) AS Control_Base,
   SUM(`Target_Responder`) AS Target_Responder,
   SUM(`Control_Responder`) AS Control_Responder,
   SUM(Target_Revenue) AS Target_Revenue,
   MAX(MTD_Incremental_Revenue) AS Incremental_Revenue
   from V_Dashboard_CLM_Event_Month_Report
   where ',@wherecond1,' and ',@wherecond2,'
   group by Event_Id,',@group_cond1,'');
   END IF;
   
   select @create_view;
	PREPARE statement from @create_view;
	Execute statement;
	Deallocate PREPARE statement;
   
   IF IN_MEASURE = 'TABLE' 
   THEN
   set @view_stmt= concat('CREATE OR REPLACE VIEW V_Dashboard_Month_Report
					AS
					(SELECT ',@select_query,',
                    CAST(format(ifnull(SUM(`Target_Base`),0),",") AS CHAR) as `TG Base`,
					CASE WHEN (SUM(`Target_Delivered`) = SUM(`Target_Delivered_NA`)) then " - " ELSE 
						CAST(format(ifnull(SUM(`Target_Delivered`),0),",") AS CHAR) END AS `TG Delivered`,
                    CASE WHEN (SUM(`Target_Delivered`) = SUM(`Target_Delivered_NA`)) then " - " ELSE 
						round((SUM(`Target_Delivered`)/SUM(`Target_Base`))*100,0) END AS `Delivered%`,
                    CAST(format(ifnull(SUM(`Control_Base`),0),",") AS CHAR) AS `CG Base`,
					CAST(format(ifnull(SUM(`Target_Responder`),0),",") AS CHAR) AS `TG Responders`,
                    CAST(format(ifnull(SUM(`Control_Responder`),0),",") AS CHAR) AS `CG Responders`,
					concat(round((CASE WHEN SUM(`Target_Delivered`) <=0 THEN 0 ELSE
						(SUM(`Target_Responder`)/SUM(`Target_Delivered`))*100 END),2),"%") AS `Target Response`,
					concat(round((CASE WHEN SUM(`Control_Base`) <=0 THEN 0 ELSE 
						(SUM(`Control_Responder`)/SUM(`Control_Base`))*100 END),2),"%") AS `Control Response`,
                    CAST(format(ifnull(SUM(Target_Revenue),0),",") AS CHAR) AS `Target Revenue`,
                    CASE WHEN SUM(Target_Responder) <= 0 then 0 else CAST(format(ifnull(ROUND(SUM(Target_Revenue)/SUM(Target_Responder),0),0),",") AS CHAR) END AS `Average Spend`,
                    concat(round(CASE WHEN (CASE WHEN SUM(`Target_Delivered`) <=0 THEN 0 
                    ELSE (SUM(`Target_Responder`)/SUM(`Target_Delivered`))*100 END)-(CASE WHEN SUM(`Control_Base`) <=0 THEN 0 ELSE (SUM(`Control_Responder`)/SUM(`Control_Base`))*100 END)<0 THEN 0 ELSE
(CASE WHEN SUM(`Target_Delivered`) <=0 THEN 0 
                    ELSE (SUM(`Target_Responder`)/SUM(`Target_Delivered`))*100 END)-(CASE WHEN SUM(`Control_Base`) <=0 THEN 0 ELSE (SUM(`Control_Responder`)/SUM(`Control_Base`))*100 END)END,2),"%") AS `TG-CG Response`,
                    cast(ifnull(format(round(sum(Target_Delivered)*(CASE WHEN (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))<=0 THEN 0 ELSE (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))END),2)/100,","),0)as char) AS `Incr Bills`,
					CAST(format(ifnull(round(SUM(Incremental_Revenue),0),0),",") AS CHAR) AS `Rev Lift`
					from V_Dashboard_Month_Report_Temp where YM=',IN_FILTER_CURRENTMONTH,'
					 GROUP BY ',@group_cond,'
                     having SUM(Target_Base)>0 and SUM(Control_Base)>=',@cg_clause,')');
					 
		IF IN_AGGREGATION_6 = 'DEFAULT' or IN_AGGREGATION_6 = ''
		THEN		
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object(',@json_select2,',"TG Base",`TG Base`,"CG Base",`CG Base`,"TG-CG Response",`TG-CG Response`,"Incr Bills",`Incr Bills`,"Rev Lift",`Rev Lift`) )),"]")into @temp_result from V_Dashboard_Month_Report ',';') ;
		END IF;
        
		IF IN_AGGREGATION_6 <> "DEFAULT" AND IN_AGGREGATION_6 <> ''
		THEN
		SELECT IN_AGGREGATION_6;
        SELECT @select_q;
		set @select_q = IN_AGGREGATION_6;
        SELECT @select_q;
        set @select_q= replace(@select_q,"[","");
        set @select_q= replace(@select_q,"]","");
        set @select_q = replace(@select_q,'"',"");
        select @select_q;
		SET @select_query2 = CONCAT(@select_query,",",@select_q);
		select (length(@select_q)-length(replace(@select_q,',','')))+1 into @num_of_channel;
		select @num_of_channel;
		set @loop_var=1;
        set @top = '';
		select @loop_var,@num_of_channel,@select_q, @json_select;
		WHILE @loop_var<=@num_of_channel
		do
			SET @selected_channel=substring_index(substring_index(concat(@select_q,',-1'),',',@loop_var),',',-1);
            set @selected_channel = TRIM(@selected_channel);
			set @json_select2=concat(@json_select2,',"',@selected_channel,'",`',@selected_channel,'`');
            set @top=concat(@top,',`',@selected_channel,'`');
			set @loop_var=@loop_var+1;
		end while;
        -- set @json_select = substring(@json_select, 2);
        set @top = substring(@top, 2);
        select @json_select2;
        select @top;
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object(',@json_select2,') )),"]")into @temp_result from V_Dashboard_Month_Report;') ;
		
		
		
		END IF;
	
	END IF;
           
	IF IN_MEASURE = 'CARD'
    THEN
		set @view_stmt = concat('CREATE OR REPLACE VIEW V_Dashboard_Month_Report_Card AS 
        select SUM(cnt) as `Count`, MAX(`# With Lift`) as "# With Lift",MAX(`Incremental Revenue`) as `Incremental Revenue` from
        (
        select count(distinct ',@attr,') as `cnt`,(select count(distinct ',@attr,') as "# With Lift" from (select ',@attr,', round(SUM(Incremental_Revenue),2) as Lift from V_Dashboard_Month_Report_Temp where YM = ',IN_FILTER_CURRENTMONTH,' group by ',@attr,' having SUM(`Control_Base`) > ',@cg_clause,')A where Lift>0) as "# With Lift",(select round(SUM(Incremental_Revenue),0) AS `Incremental_Revenue` from(select round(SUM(Incremental_Revenue),1) AS `Incremental_Revenue` from V_Dashboard_Month_Report_Temp where YM = ',IN_FILTER_CURRENTMONTH,' group by ',@attr,' having SUM(`Control_Base`) > ',@cg_clause,')B) AS `Incremental Revenue` from V_Dashboard_Month_Report_Temp where YM = ',IN_FILTER_CURRENTMONTH,' group by ',@attr,' having SUM(`Control_Base`) > ',@cg_clause,')A');
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Count",ifnull(`Count`,0),"# With Lift",ifnull(`# With Lift`,0),"Incremental Revenue",format(ifnull(`Incremental Revenue`,0),",")) )),"]")into @temp_result from V_Dashboard_Month_Report_Card ',';') ;
		
	END IF;
    
    IF IN_MEASURE = 'SEGMENT_NAME'
    THEN
		set @view_stmt = concat('CREATE OR REPLACE VIEW V_Dashboard_MonthReport_Segmnet_Names AS
								select distinct Segment as CLMSegmentName from CLM_Event_Dashboard A JOIN Event_Master B ON A.Event_Id = B.Id');
		SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Segment",`CLMSegmentName`) )),"]")into @temp_result from V_Dashboard_MonthReport_Segmnet_Names;' ;
        select @Query_String;
    END IF;
   
    IF IN_MEASURE = 'THEME_NAME'
    THEN
		set @view_stmt = concat('CREATE OR REPLACE VIEW V_Dashboard_MonthReport_Theme_Names AS
								select distinct CampaignThemeName as CampaignThemeName from Event_Master');
		SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Theme",CAST(`CampaignThemeName` AS CHAR)) )),"]") into @temp_result from V_Dashboard_MonthReport_Theme_Names;' ;
        select @Query_String;
    END IF;
	
	
	IF IN_MEASURE = 'COL_PICKER' and IN_AGGREGATION_4 = 'SHOW_ALL_COLS'
    THEN
		set @view_stmt = concat('CREATE OR REPLACE VIEW V_Dashboard_Month_Report_COL_Picker
					AS (	select COLUMN_NAME from information_schema.columns
							where TABLE_NAME = "V_Dashboard_Month_Report");');
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("COLUMN_NAME",`COLUMN_NAME`) )),"]")into @temp_result from V_Dashboard_Month_Report_COL_Picker;') ;
        
        select @Query_String;
    END IF;
    
    

	
   

    IF IN_SCREEN <> "DOWNLOAD"
    THEN
    select @view_stmt;
	PREPARE statement from @view_stmt;
	Execute statement;
	Deallocate PREPARE statement;
	select @Query_String;
	PREPARE statement from @Query_String;
	Execute statement;
	Deallocate PREPARE statement; 
    SET out_result_json = @temp_result;
    ELSE
		call S_dashboard_performance_download('V_Dashboard_Month_Report',@out_result_json1);
		select @out_result_json1 into out_result_json;
    END IF;
    
END$$
DELIMITER ;
