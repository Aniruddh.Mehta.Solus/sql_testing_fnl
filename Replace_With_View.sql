DELIMITER $$
CREATE OR REPLACE PROCEDURE `Replace_With_View`(IN viewName VARCHAR(255), IN dbName VARCHAR(255))
dqc_bill:BEGIN

SET @ViewName = viewName;
SET @FinalDBName = dbName;
IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_name = @ViewName and table_schema=@FinalDBName ) THEN
  SET @renameStatement = CONCAT('CREATE OR REPLACE VIEW  ',@ViewName,' AS SELECT * FROM  ', @FinalDBName,'.',@ViewName );
 		PREPARE stmt FROM @renameStatement;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
		SELECT 'View Created Successfully';
ELSE
  SELECT 'Table does not exist.';
END IF;

END$$
DELIMITER ;

