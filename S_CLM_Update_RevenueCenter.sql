
CREATE OR REPLACE PROCEDURE `S_CLM_Update_RevenueCenter`()
begin
DECLARE vStart_Cnt BIGINT DEFAULT 0;
DECLARE vEnd_Cnt BIGINT DEFAULT 0;
DECLARE vBatchSize BIGINT DEFAULT 100000;
DECLARE done1 int DEFAULT FALSE;
DECLARE Load_Exe_ID BIGINT DEFAULT 0;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done1 = TRUE;
BEGIN
	GET DIAGNOSTICS CONDITION 1 @errsqlstate=RETURNED_SQLSTATE, @errno=MYSQL_ERRNO, @errtext = MESSAGE_TEXT;
	SET @logmsg=CONCAT(ifnull(@errsqlstate,'ERROR') , ifnull(@errno,'1'), ifnull(@errtext,'ERROR MESSAGE'));
	INSERT into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
		values ('S_Response_Update_Revenue_Center', @logmsg ,now(),null,@errno, ifnull(Load_Exe_ID,1));
	SELECT 'S_Response_Update_Revenue_Center : Error Message :' as '', @logmsg as '';
END;
SET Load_Exe_ID = F_Get_Load_Execution_Id();
SET SQL_SAFE_UPDATES=0;
INSERT into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
	values ('S_Response_Update_CLM_Segment ', ' ' ,now(),null,'Started', Load_Exe_ID);
SELECT 'S_Response_Update_CLM_Segment : STARTED ' as '', NOW() as '' ;


    SET SQL_SAFE_UPDATES=0;
    set foreign_key_checks=0;
	SELECT Customer_Id INTO @Rec_Cnt FROM CDM_Customer_Master ORDER BY Customer_Id DESC LIMIT 1;
	SET vStart_Cnt = 0;
PROCESS_LOOP: LOOP
			SET vEnd_Cnt = vStart_Cnt + vBatchSize;
        insert into log_solus(module,rec_start,rec_end)values('Update_Revenue_Center',vStart_Cnt,vEnd_Cnt);
        select vStart_Cnt,vEnd_Cnt;
        select current_timestamp();

        Update Event_Execution_History a,CDM_Revenue_Center b
        set a.Revenue_Center=b.Revenue_Center
        where a.Customer_Id = b.Customer_Id
        and a.Customer_Id between vStart_Cnt and vEnd_Cnt
        and a.Revenue_Center = -1
        and b.Customer_Id between vStart_Cnt and vEnd_Cnt;
				SET vStart_Cnt=vEnd_Cnt+1;
        IF vStart_Cnt  > @Rec_Cnt THEN
            LEAVE PROCESS_LOOP;
        END IF;         
 END LOOP PROCESS_LOOP;
 UPDATE ETL_Execution_Details SET Status = 'Succeeded', End_Time = NOW() WHERE Procedure_Name = 'S_Response_Update_Revenue_Center' AND Load_Execution_ID = Load_Exe_ID;        
select 'S_Response_Update_Revenue_Center : COMPLETED ' as '', NOW() as '' ;
END

