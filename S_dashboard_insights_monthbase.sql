DELIMITER $$
CREATE or replace PROCEDURE `S_dashboard_insights_monthbase`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
        
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    
    
    /*Select IFNULL(max(IF(Config_Name="CALENDAR",Value1,NULL)),max(IF(Config_Name="FISCAL",Value1,NULL))) into @FY_SATRT_DATE
    from UI_Configuration_Global where isActive="true";
    
    Select IFNULL(max(IF(Config_Name="CALENDAR",Value2,NULL)),max(IF(Config_Name="FISCAL",Value2,NULL))) into @FY_END_DATE
    from UI_Configuration_Global where isActive="true";
    */
    select date_format(date_sub(concat(IN_FILTER_CURRENTMONTH,"01"),interval 6 month),"%Y%m") into @FY_SATRT_DATE;
	SET @FY_END_DATE =IN_FILTER_CURRENTMONTH;
    
    select @FY_END_DATE,@FY_SATRT_DATE;
    
    SET @filter_cond=concat(" YYYYMM between '",@FY_SATRT_DATE,"' and '",@FY_END_DATE,"' and AGGREGATION = 'BY_MONTH' ");
	SET @date_select=' date_format(concat(YYYYMM,"01"),"%b-%Y") AS "Year" ';
    
	
    IF IN_AGGREGATION_1 = 'BASE'
	THEN 
		IF IN_MEASURE = 'MOMGRAPH'
		THEN
			SET @select_query = CASE WHEN IN_NUMBER_FORMAT = 'NUMBER' THEN ' SUM(YTDNewBase) as "NewBase",SUM(YTDExistingBase) as "RepeatBase"'
								ELSE ' round((SUM(YTDNewBase)/(SUM(YTDNewBase)+SUM(YTDExistingBase)))*100,1) as "NewBase", round((SUM(YTDExistingBase)/(SUM(YTDNewBase)+SUM(YTDExistingBase)))*100,1) as "RepeatBase" ' END;
			set @view_stmt= CONCAT(' create or replace view V_Dashboard_MonthBase as 
							select ',@date_select,',',@select_query,'
							from Dashboard_Insights_Base
                            where ',@filter_cond,'
							group by 1
                            order by YYYYMM;
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`Year`,"New Base",`NewBase`,"Repeat Base",`RepeatBase`) )),"]")into @temp_result from V_Dashboard_MonthBase;' ;
		ELSEIF IN_MEASURE = 'L12M'
        THEN
			set @select_query1 = case when IN_NUMBER_FORMAT = 'NUMBER' THEN 'SUM(T12MthNewBase)' ELSE ' round((SUM(T12MthNewBase)/(SUM(T12MthNewBase)+SUM(T12MthExistingBase)))*100,1)' END;
            set @select_query2 = case when IN_NUMBER_FORMAT = 'NUMBER' THEN 'SUM(T12MthExistingBase)' ELSE ' round((SUM(T12MthExistingBase)/(SUM(T12MthNewBase)+SUM(T12MthExistingBase)))*100,1)' END;
			set @view_stmt= CONCAT(' create or replace view V_Dashboard_MonthBase as 
							(select "New Base" as `Name`, ',@select_query1,' as `Value`
							from Dashboard_Insights_Base
                            where YYYYMM=',IN_FILTER_CURRENTMONTH,')
							UNION
                            (select "Repeat Base" as `Name`, ',@select_query2,' as `Value`
							from Dashboard_Insights_Base
                            where YYYYMM=',IN_FILTER_CURRENTMONTH,')
                            ;
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("name",`Name`,"value",`Value`) )),"]")into @temp_result from V_Dashboard_MonthBase;' ;
		ELSEIF IN_MEASURE = 'L12MSM'
        THEN
			set @select_query1 = case when IN_NUMBER_FORMAT = 'NUMBER' THEN 'SUM(T12MthSingle)' ELSE ' round((SUM(T12MthSingle)/(SUM(T12MthSingle)+SUM(T12MthMulti)))*100,1)' END;
            set @select_query2 = case when IN_NUMBER_FORMAT = 'NUMBER' THEN 'SUM(T12MthMulti)' ELSE ' round((SUM(T12MthMulti)/(SUM(T12MthSingle)+SUM(T12MthMulti)))*100,1)' END;
			set @view_stmt= CONCAT(' create or replace view V_Dashboard_MonthBase as 
							(select "Single Cust" as `Name`, ',@select_query1,' as `Value`
							from Dashboard_Insights_Base
                            where YYYYMM=',IN_FILTER_CURRENTMONTH,')
							UNION
                            (select "Multi Cust" as `Name`, ',@select_query2,' as `Value`
							from Dashboard_Insights_Base
                            where YYYYMM=',IN_FILTER_CURRENTMONTH,')
                            ;
							');
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("name",`Name`,"value",`Value`) )),"]")into @temp_result from V_Dashboard_MonthBase;' ;
		END IF;
	ELSEIF IN_AGGREGATION_1 = 'REVENUE'
    THEN
		IF IN_MEASURE = 'MOMGRAPH'
		THEN
			SET @select_query = CASE WHEN IN_NUMBER_FORMAT = 'NUMBER' THEN ' SUM(YTDNewRevenue) as "NewRev",SUM(YTDExistingRevenue) as "RepeatRev"'
								ELSE ' round((SUM(YTDNewRevenue)/(SUM(YTDNewRevenue)+SUM(YTDExistingRevenue)))*100,1) as "Newrev", round((SUM(YTDExistingRevenue)/(SUM(YTDNewRevenue)+SUM(YTDExistingRevenue)))*100,1) as "RepeatRev" ' END;
			set @view_stmt= CONCAT(' create or replace view V_Dashboard_MonthBase as 
							select ',@date_select,',',@select_query,'
							from Dashboard_Insights_Base
                            where ',@filter_cond,'
							group by 1
                            order by YYYYMM;
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`Year`,"New Revenue",`NewRev`,"Repeat Revenue",`RepeatRev`) )),"]")into @temp_result from V_Dashboard_MonthBase;' ;
		ELSEIF IN_MEASURE = 'L12M'
        THEN
			set @select_query1 = case when IN_NUMBER_FORMAT = 'NUMBER' THEN 'SUM(T12MthNewRevenue)' ELSE ' round((SUM(T12MthNewRevenue)/(SUM(T12MthNewRevenue)+SUM(T12MthExistingRevenue)))*100,1)' END;
            set @select_query2 = case when IN_NUMBER_FORMAT = 'NUMBER' THEN 'SUM(T12MthExistingRevenue)' ELSE ' round((SUM(T12MthExistingRevenue)/(SUM(T12MthNewRevenue)+SUM(T12MthExistingRevenue)))*100,1)' END;
			set @view_stmt= CONCAT(' create or replace view V_Dashboard_MonthBase as 
							(select "New Rev" as `Name`, ',@select_query1,' as `Value`
							from Dashboard_Insights_Base
                            where YYYYMM=',IN_FILTER_CURRENTMONTH,')
							UNION
                            (select "Repeat Rev" as `Name`, ',@select_query2,' as `Value`
							from Dashboard_Insights_Base
                            where YYYYMM=',IN_FILTER_CURRENTMONTH,')
                            ;
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("name",`Name`,"value",`Value`) )),"]")into @temp_result from V_Dashboard_MonthBase;' ;
		ELSEIF IN_MEASURE = 'L12MSM'
        THEN
			set @select_query1 = case when IN_NUMBER_FORMAT = 'NUMBER' THEN 'SUM(T12MthSingleRevenue)' ELSE ' round((SUM(T12MthSingleRevenue)/(SUM(T12MthSingleRevenue)+SUM(T12MthMultiRevenue)))*100,1)' END;
            set @select_query2 = case when IN_NUMBER_FORMAT = 'NUMBER' THEN 'SUM(T12MthMultiRevenue)' ELSE ' round((SUM(T12MthMultiRevenue)/(SUM(T12MthSingleRevenue)+SUM(T12MthMultiRevenue)))*100,1)' END;
			set @view_stmt= CONCAT(' create or replace view V_Dashboard_MonthBase as 
							(select "Single Cust Rev" as `Name`, ',@select_query1,' as `Value`
							from Dashboard_Insights_Base
                            where YYYYMM=',IN_FILTER_CURRENTMONTH,')
							UNION
                            (select "Multi Cust Rev" as `Name`, ',@select_query2,' as `Value`
							from Dashboard_Insights_Base
                            where YYYYMM=',IN_FILTER_CURRENTMONTH,')
                            ;
							');
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("name",`Name`,"value",`Value`) )),"]")into @temp_result from V_Dashboard_MonthBase;' ;
		END IF;
    END IF;
    
  
   
	IF IN_MEASURE <> "DOWNLOAD"
    THEN
	select @view_stmt;
	PREPARE statement from @view_stmt;
	Execute statement;
	Deallocate PREPARE statement;

	select @Query_String;
	PREPARE statement from @Query_String;
	Execute statement;
	Deallocate PREPARE statement; 
    SET out_result_json = @temp_result;
    END IF;
    
END$$
DELIMITER ;
