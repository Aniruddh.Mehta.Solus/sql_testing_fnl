DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_UI_Campaign_Creation`(IN in_request_json json, OUT out_result_json json)
BEGIN


    -- User Information

    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	 DECLARE IN_ACTION  TEXT;

    -- Element Details	

	declare Element_Creation text; 
	declare Element_Name text;
	declare Element_Type text;
	declare SVOC_Replaced_Query text;
	declare SVOT_Replaced_Query text;	
	DECLARE Element_ThemeId BIGINT;
	DECLARE Element_SegmentId BIGINT;
	DECLARE Element_CGPERCENTAGE BIGINT;
	DECLARE Element_RESPONSEDAYS BIGINT;
	DECLARE Element_STARTDATE TEXT;
    DECLARE Element_ENDDATE  TEXT;
	DECLARE Condition_Type  TEXT;
	DECLARE Condition_Type_Div  TEXT;
	DECLARE Conditions  TEXT;
	declare SegmentId text;
	declare InUse text;
	declare Channel_ID text;
	   
	-- Calcucaltion Vairables
	
	DECLARE vStart_Cnt BIGINT DEFAULT 0;
    DECLARE vEnd_Cnt BIGINT DEFAULT 0;
	
    DECLARE done1,done2,done3,done4 INT DEFAULT FALSE;
	
	-- LifeCycle Segment Master Details
    DECLARE LifeCycleId_cur1,Exe_ID int;
    DECLARE LifeCycleReplacedQuery_cur1 TEXT;
	
	-- Campaign Segment Master Details
	DECLARE CampaignId_cur1,Exe_ID1 int;
    DECLARE CampaignReplacedQuery_cur1 TEXT;
	DECLARE CampaignReplacedBillQuery_cur1 TEXT;
	
	-- Region Master Details
	DECLARE RegionId_cur1,Exe_ID2 int;
    DECLARE RegionReplacedQuery_cur1 TEXT;
	DECLARE RegionReplacedBillQuery_cur1 TEXT;
	
    DECLARE vSuccess int (4);                      
	DECLARE vErrMsg Text ; 

				
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER_NAME"));
	SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
	SET IN_ACTION = json_unquote(json_extract(in_request_json,"$.ACTION"));
	SET Element_Creation = json_unquote(json_extract(in_request_json,"$.Element_Creation"));
	SET Element_Name = json_unquote(json_extract(in_request_json,"$.Element_Name"));
	SET Element_Type = json_unquote(json_extract(in_request_json,"$.Element_Type"));
	SET SVOC_Replaced_Query = json_unquote(json_extract(in_request_json,"$.SVOC_Replaced_Query"));
	SET SVOT_Replaced_Query = json_unquote(json_extract(in_request_json,"$.SVOT_Replaced_Query"));
	SET Element_ThemeId = json_unquote(json_extract(in_request_json,"$.Element_ThemeId"));
	SET Element_SegmentId = json_unquote(json_extract(in_request_json,"$.Element_SegmentId"));
	SET Element_CGPERCENTAGE = json_unquote(json_extract(in_request_json,"$.Element_CGPERCENTAGE"));
	SET Element_RESPONSEDAYS = json_unquote(json_extract(in_request_json,"$.Element_RESPONSEDAYS"));
	SET Element_STARTDATE = json_unquote(json_extract(in_request_json,"$.Element_STARTDATE"));
	SET Element_ENDDATE = json_unquote(json_extract(in_request_json,"$.Element_ENDDATE"));
	SET Condition_Type = json_unquote(json_extract(in_request_json,"$.Condition_Type"));
	SET Condition_Type_Div = json_unquote(json_extract(in_request_json,"$.Condition_Type_Div"));
	SET Conditions = json_unquote(json_extract(in_request_json,"$.conditions"));
	SET SegmentId = json_unquote(json_extract(in_request_json,"$.SegmentId"));
	SET InUse = json_unquote(json_extract(in_request_json,"$.InUse"));
	SET Channel_ID = json_unquote(json_extract(in_request_json,"$.Channel_ID"));
	SET @temp_result = Null;
	
	
	Select Element_Creation;
	
	CREATE TABLE IF NOT EXISTS SVOC_UI_CONDITION_MAKER
    (
		CONDITION_PAGE 		VARCHAR(200), 
        CONDITION_NAME 		VARCHAR(200), 
        CONDITION_TYPE 		VARCHAR(200),
		CONDITION_DIV  		BIGINT,
		AGG_3 				VARCHAR(400),
       UNIQUE KEY (CONDITION_PAGE, CONDITION_NAME, CONDITION_TYPE, CONDITION_DIV)
	);
	
	IF IN_ACTION = "APPEND"
		THEN
			INSERT  INTO SVOC_UI_CONDITION_MAKER (CONDITION_PAGE, CONDITION_NAME, CONDITION_TYPE, CONDITION_DIV, AGG_3)
				VALUES (Element_Creation, Element_Name, Condition_Type, Condition_Type_Div, Conditions)
                ON DUPLICATE KEY UPDATE AGG_3 = Conditions;
				
				
			SELECT CONCAT(
					"[",
					(GROUP_CONCAT(json_object(	"CONDITION_PAGE",`CONDITION_PAGE`,
												"CONDITION_NAME",`CONDITION_NAME`,
												"CONDITION_TYPE",`CONDITION_TYPE`,
												"CONDITION_DIV",`CONDITION_DIV`,
												"AGG_3",`AGG_3`
												))),
					"]"
					)into @temp_result from SVOC_UI_CONDITION_MAKER
					where CONDITION_PAGE=Element_Creation and CONDITION_NAME= Element_Name
					and CONDITION_TYPE =Condition_Type and  CONDITION_DIV=Condition_Type_Div;
	End If;	
	
	IF IN_ACTION = "REMOVE"
		THEN
			Delete From SVOC_UI_CONDITION_MAKER
			Where  CONDITION_PAGE=Element_Creation and CONDITION_NAME= Element_Name
			and CONDITION_TYPE =Condition_Type and  CONDITION_DIV=Condition_Type_Div;
			
			Set @temp_result="[]";
	End If;	
		
	IF IN_ACTION = "CANCEL"
		THEN
			
			Delete From SVOC_UI_CONDITION_MAKER
			Where  CONDITION_PAGE=Element_Creation  and CONDITION_NAME= Element_Name ;
			
			Set @temp_result="[]";
	End If;	
	 
	IF IN_ACTION = "SHOW"
		THEN
	
			SET @view_stmt=concat(	
			'
				Create or Replace view SVOC_Condition_Save_View as
				(
					SELECT 
						 CONDITION_PAGE, CONDITION_NAME, CONDITION_TYPE, 
						(	SELECT GROUP_CONCAT(AGG_3 SEPARATOR " ") FROM SVOC_UI_CONDITION_MAKER WHERE CONDITION_PAGE = "',Element_Creation,'" 
							and CONDITION_NAME = "',Element_Name,'" and CONDITION_TYPE = "',Condition_Type,'") as Final_Condition 
					FROM 
						SVOC_UI_CONDITION_MAKER
					LIMIT 1
				)
			'
			);
			
			
		SELECT @view_stmt;	 
		PREPARE stmt3 FROM @view_stmt;
		EXECUTE stmt3;
		DEALLOCATE PREPARE stmt3;
		SELECT CONCAT(
		"[",
		(GROUP_CONCAT(json_object("Message",`Final_Condition`))),
		"]")into @temp_result from SVOC_Condition_Save_View;	
		
		If @temp_result is NULL
		THen Set  @temp_result="[]";
		End if;
	End If;	
	
	
	
	If Element_Creation ="CAMPAIGNSEGMENT" AND (IN_ACTION IS  NULL or IN_ACTION ='') and (@SVOC_UI_CONDITION_MAKER is not null)
	   THEN
	   
			
					
					
		    Set @vUserId='1';
			Set	@vMicroSegmentName='';
			Set	@vMicroSegmentDesc='';
			Set	@vMSReplacedQueryAsInUI='';
			Set@vMSReplacedQuery='';
			Set @vIsValidSVOCCondition='';
			Set @vMSReplacedQueryBillAsInUI='';
	        Set	@vMSReplacedQueryBill='';
			Set @vIsValidBillCondition='';
			Set	@vMicroSegmentId='';
			Set	@vErrMsg='';
			Set	@vSuccess='';
		
			Set	@vMicroSegmentName=Element_Name;
			Set	@vMicroSegmentDesc=Element_Type;
			Set	@vMSReplacedQueryAsInUI=SVOC_Replaced_Query;
			Set @vMSReplacedQuery=SVOC_Replaced_Query;
			Set @vIsValidSVOCCondition='1';
			Set @vMSReplacedQueryBillAsInUI=SVOT_Replaced_Query;
	        Set	@vMSReplacedQueryBill=SVOT_Replaced_Query;
			Set @vIsValidBillCondition='1';
			Select MicroSegmentId into @vMicroSegmentId From CLM_MicroSegment where MicroSegmentName=Element_Name ;
			Set	@vErrMsg='';
			Set	@vSuccess='';
			SET @MicroSegmentCount='';
			
				
			
			Set @SVOC_UI_CONDITION_MAKER= concat(' Delete from SVOC_UI_CONDITION_MAKER 
			where CONDITION_PAGE="CAMPAIGNSEGMENT" and CONDITION_NAME="',@vMicroSegmentName,'"                      
													  ;') ;
			SELECT @SVOC_UI_CONDITION_MAKER;
			PREPARE statement from @SVOC_UI_CONDITION_MAKER;
			Execute statement;
			Deallocate PREPARE statement;			
			
			IF  @vMSReplacedQueryBill IS NULL OR @vMSReplacedQueryBill = ''
				THEN
					SET @SVOT_Condition='SELECT 1 from DUAL';
			ELSE
				SET @SVOT_Condition = CONCAT('SELECT 1 from V_CLM_Bill_Detail_One_View svot WHERE ',@vMSReplacedQueryBill,'');
			END IF;	
			if @vMSReplacedQuery='' then Set @vMSReplacedQuery='1=1'; End If;
			
			Set @Sql1=CONCAT('Select Count(1) into @MicroSegmentCount from Customer_One_View where ',@vMSReplacedQuery,' AND EXISTS (',@SVOT_Condition,')');
			SELECT @Sql1;
			prepare stmt1 from @Sql1;
			execute stmt1;
			deallocate prepare stmt1;
			
			If @MicroSegmentCount !=''
				THEN
				
					CALL PROC_UI_CU_CLM_MicroSegment(@vUserId, @vMicroSegmentName, @vMicroSegmentDesc, @vMSReplacedQueryAsInUI,@vMSReplacedQuery,@vIsValidSVOCCondition,@vMSReplacedQueryBillAsInUI, @vMSReplacedQueryBill,@vIsValidBillCondition, @vMicroSegmentId, @vErrMsg,@vSuccess);
						
					
					IF @vSuccess!=''
					   THEN
											
							Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Camapign Segment Is Created") )),"]") into @temp_result   ;') ;
			   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
									
										
							Select MicroSegmentId into @vMicroSegmentId From CLM_MicroSegment where MicroSegmentName=Element_Name;
							
							Set @Sql1=CONCAT('Update CLM_MicroSegment 
															Set Coverage = ',@MicroSegmentCount,',
															In_Use ="Yes"
																
													where MicroSegmentId=',@vMicroSegmentId,';');
													
							SELECT @Sql1;
							prepare stmt1 from @Sql1;
							execute stmt1;
							deallocate prepare stmt1;
										
					End If;			
							
		     
			
					IF @vSuccess =''
					   THEN
											
										Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message",@vErrMsg) )),"]") into @temp_result                       
															  ;') ;
			   
										SELECT @Query_String;
										PREPARE statement from @Query_String;
										Execute statement;
										Deallocate PREPARE statement; 
									
					End if; 
			
			Delete From SVOC_UI_CONDITION_MAKER
			Where  CONDITION_PAGE=Element_Creation and CONDITION_NAME= Element_Name;
			
			End If;
	END IF;
	
	If Element_Creation ="CAMPAIGNSEGMENT" and (SegmentId is not Null or SegmentId !='') 
		THEN
				IF IN_ACTION="Yes"
				THEN
					Update CLM_MicroSegment
					Set In_Use="Yes"
					Where MicroSegmentId=SegmentId;
					
					--  INSERT into UI_Campaign_Task_List (UI_Campaign_Task, Run_Mode, Event_Id,Status,CreatedDate) 
						--					   values ('CampaignSegment_RUN',"",SegmentId,"START", current_timestamp());
											   
					Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Campaign Segment Is Enabled") )),"]") into @temp_result                       
													  ;') ;
	   
								SELECT @Query_String;
								PREPARE statement from @Query_String;
								Execute statement;
								Deallocate PREPARE statement; 							
				End If;	

				IF IN_ACTION="No"
				THEN
						Update CLM_MicroSegment
					Set In_Use="No"
					Where MicroSegmentId=SegmentId;
					
					--  INSERT into UI_Campaign_Task_List (UI_Campaign_Task, Run_Mode, Event_Id,Status,CreatedDate) 
						--					   values ('CampaignSegment_Delete',"",SegmentId,"START", current_timestamp());
											   
						Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Campaign Segment Is Disabled") )),"]") into @temp_result                       
													  ;') ;
	   
								SELECT @Query_String;
								PREPARE statement from @Query_String;
								Execute statement;
								Deallocate PREPARE statement;
				End If;							   				
	
    End If;	
	
	If Element_Creation ="CAMPAIGNSEGMENT_DETAILS" and (SegmentId is not Null or SegmentId !='') 
		THEN
		
			Delete from SVOC_UI_CONDITION_MAKER 
				where CONDITION_PAGE="CAMPAIGNSEGMENT" and CONDITION_NAME in (Select MicroSegmentName  FROM CLM_MicroSegment where MicroSegmentId =SegmentId );
				
				
			Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Element_ID",MicroSegmentId,"Element_Name",MicroSegmentName,"SVOC_Replaced_Query",MSReplacedQuery,"SVOT_Replaced_Query",MSReplacedQueryBill) )),"]") into @temp_result  
                             FROM CLM_MicroSegment where MicroSegmentId =',SegmentId,'						
													  ;') ;
	   
								SELECT @Query_String;
								PREPARE statement from @Query_String;
								Execute statement;
								Deallocate PREPARE statement;
										   				
	
    End If;	
	
	If Element_Creation ="LIFECYCLESEGMENT"  AND (IN_ACTION IS  NULL or IN_ACTION ='')
	  THEN
		    Set @vUserId='1';
			Set	@vCLMSegmentName='';
			Set	@vCLMSegmentDesc='';
			Set	@vCLMSegmentReplacedQueryAsInUI='';
			Set @vCLMSegmentReplacedQuery='';
			Set @vIsValidCondition='';
			Set	@vCLMSegmentId='';
			Set	@vErrMsg='';
			Set	@vSuccess='';
		
			Set	@vCLMSegmentName=Element_Name;
			Set	@vCLMSegmentDesc=Element_Type;
			Set	@vCLMSegmentReplacedQueryAsInUI=SVOC_Replaced_Query;
			Set @vCLMSegmentReplacedQuery=SVOC_Replaced_Query;
			Set @vIsValidCondition='1';
			Select CLMSegmentId into @vCLMSegmentId From CLM_Segment where CLMSegmentName=Element_Name ;
			Set	@vErrMsg='';
			Set	@vSuccess='';
			Set	@LIFECYCLESEGMENT_Count='';
			
			
			Set @SVOC_UI_CONDITION_MAKER= concat(' Delete from SVOC_UI_CONDITION_MAKER 
			where CONDITION_PAGE="LIFECYCLESEGMENT" and CONDITION_NAME="',@vCLMSegmentName,'"                      
													  ;') ;
			SELECT @SVOC_UI_CONDITION_MAKER;
			PREPARE statement from @SVOC_UI_CONDITION_MAKER;
			Execute statement;
			Deallocate PREPARE statement;			
			
			If @vCLMSegmentReplacedQuery='' then Set @vCLMSegmentReplacedQuery='1=1'; End If;
			Set @Sql1=CONCAT('Select Count(1) into @LIFECYCLESEGMENT_Count from Customer_One_View where ',@vCLMSegmentReplacedQuery,';');
			SELECT @Sql1;
			PREPARE statement from @Sql1;
			Execute statement;
			Deallocate PREPARE statement; 						
							
			If @LIFECYCLESEGMENT_Count !=''
				THEN
					
					
					Select @vUserId, @vCLMSegmentName, @vCLMSegmentDesc,@vCLMSegmentReplacedQueryAsInUI, @vCLMSegmentReplacedQuery,@vIsValidCondition,@vCLMSegmentId,@vErrMsg,@vSucces;
					CALL PROC_UI_CU_CLM_Segment(@vUserId, @vCLMSegmentName, @vCLMSegmentDesc,@vCLMSegmentReplacedQueryAsInUI, @vCLMSegmentReplacedQuery,@vIsValidCondition,@vCLMSegmentId,@vErrMsg,@vSuccess);

					IF @vSuccess!=''
					   THEN
											
							Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message"," Life Cycle Segment Is Createrd") )),"]") into @temp_result                       
													  ;') ;
			   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
							
							Select CLMSegmentId into @vCLMSegmentId From CLM_Segment where CLMSegmentName=Element_Name;
							
							
							if @vCLMSegmentReplacedQuery='' then Set @vCLMSegmentReplacedQuery='1=1'; End If;
							Set @Sql1=CONCAT('Update CLM_Segment 
												Set Coverage = ',@LIFECYCLESEGMENT_Count,'
													,In_Use ="Yes"
											where CLMSegmentId=',@vCLMSegmentId,';');
													
							SELECT @Sql1;
							prepare stmt1 from @Sql1;
							execute stmt1;
							deallocate prepare stmt1;
							
									
					End if; 
					
					IF @vSuccess =''
					   THEN
											
							Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message",@vErrMsg) )),"]") into @temp_result                       
													  ;') ;
			   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
									
					End if; 
			End If;
			Delete From SVOC_UI_CONDITION_MAKER
			Where  CONDITION_PAGE=Element_Creation and CONDITION_NAME= Element_Name;
	
	END IF;
	
	
	If Element_Creation ="LIFECYCLESEGMENT" and (SegmentId is not Null or SegmentId !='') 
		THEN
				IF IN_ACTION="Yes"
					THEN
						Update CLM_Segment
						Set In_Use="Yes"
						Where CLMSegmentId=SegmentId;
						
						-- INSERT into UI_Campaign_Task_List (UI_Campaign_Task, Run_Mode, Event_Id,Status,CreatedDate) 
							--					   values ('LifeCycleSegment_RUN',"",SegmentId,"START", current_timestamp());
												   
						Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Life Cycle  Segment Is Enabled") )),"]") into @temp_result                       
														  ;') ;
		   
						SELECT @Query_String;
						PREPARE statement from @Query_String;
						Execute statement;
						Deallocate PREPARE statement; 											   
				End If;	

				IF IN_ACTION="No"
					THEN
						Update CLM_Segment
						Set In_Use="No"
						Where CLMSegmentId=SegmentId;
						
							
					--	INSERT into UI_Campaign_Task_List (UI_Campaign_Task, Run_Mode, Event_Id,Status,CreatedDate) 
						-- values ('LifeCycleSegment_Delete',"",SegmentId,"START", current_timestamp());
												   
						Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Life Cycle Segment Is Disabled") )),"]") into @temp_result                       
														  ;') ;
		   
						SELECT @Query_String;
						PREPARE statement from @Query_String;
						Execute statement;
						Deallocate PREPARE statement; 
						
				End If;							   				
	
    End If;	
	
	
	If Element_Creation ="LIFECYCLESEGMENT_DETAILS" and (SegmentId is not Null or SegmentId !='') 
		THEN
			
			Delete from SVOC_UI_CONDITION_MAKER 
			where CONDITION_PAGE="LIFECYCLESEGMENT" and CONDITION_NAME in (Select CLMSegmentName  FROM CLM_Segment where CLMSegmentId =SegmentId );
				
			Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Element_ID",CLMSegmentId,"Element_Name",CLMSegmentName,"SVOC_Replaced_Query",CLMSegmentReplacedQuery) )),"]") into @temp_result  
                             FROM CLM_Segment where CLMSegmentId =',SegmentId,'						
													  ;') ;
	   
								SELECT @Query_String;
								PREPARE statement from @Query_String;
								Execute statement;
								Deallocate PREPARE statement;
										   				
	
    End If;	
	
	
	
	If Element_Creation ="REGION"  AND (IN_ACTION IS  NULL or IN_ACTION ='')
	   THEN
		    Set @vUserId='1';
			Set	@vRegionSegmentName='';
			Set	@vRegionSegmentDesc='';
			Set	@vRegionReplacedQueryAsInUI='';
			Set @vRegionReplacedQuery='';
			Set @vIsValidSVOCCondition='';
			Set @vRegionReplacedQueryBillAsInUI='';
	        Set	@vRegionReplacedQueryBill='';
			Set @vIsValidBillCondition='';
			Set	@vRegionSegmentId='';
			Set	@vErrMsg='';
			Set	@vSuccess='';
		
			Set	@vRegionSegmentName=Element_Name;
			Set	@vRegionSegmentDesc=Element_Type;
			Set	@vRegionReplacedQueryAsInUI=SVOC_Replaced_Query;
			Set @vRegionReplacedQuery=SVOC_Replaced_Query;
			Set @vIsValidSVOCCondition='1';
			Set @vRegionReplacedQueryBillAsInUI=SVOT_Replaced_Query;
	        Set	@vRegionReplacedQueryBill=SVOT_Replaced_Query;
			Set @vIsValidBillCondition='1';
			Select RegionSegmentId into @RegionSegmentId From CLM_RegionSegment where RegionSegmentName=Element_Name ;
			Set	@vErrMsg='';
			Set	@vSuccess='';
			Set @REGION_COUNT ='';
		
			
			
			Set @SVOC_UI_CONDITION_MAKER= concat(' Delete from SVOC_UI_CONDITION_MAKER 
			where CONDITION_PAGE="REGION" and CONDITION_NAME="',@vRegionSegmentName,'"                      
													  ;') ;
			SELECT @SVOC_UI_CONDITION_MAKER;
			PREPARE statement from @SVOC_UI_CONDITION_MAKER;
			Execute statement;
			Deallocate PREPARE statement;			
			
			IF  @vRegionReplacedQueryBill IS NULL OR @vRegionReplacedQueryBill = ''
								THEN
									SET @SVOT_Condition='SELECT 1 from DUAL';
							ELSE
								SET @SVOT_Condition = CONCAT('SELECT 1 from V_CLM_Bill_Detail_One_View svot WHERE  svot.Customer_Id =Customer_One_View.Customer_Id and ',@vRegionReplacedQueryBill,'');
							END IF;
									
			if @vRegionReplacedQuery='' or @vRegionReplacedQuery IS NULL then Set @vRegionReplacedQuery='1=1'; End If;			
			Set @Sql1=CONCAT('Select Count(1) into @REGION_COUNT from Customer_One_View where ',@vRegionReplacedQuery,' AND EXISTS (',@SVOT_Condition,');');
											
			SELECT @Sql1;
			prepare stmt1 from @Sql1;
			execute stmt1;
			deallocate prepare stmt1;	
			
			If @REGION_COUNT !=''
				THEN
				
					CALL PROC_UI_CU_CLM_RegionSegment(@vUserId, @vRegionSegmentName, @vRegionSegmentDesc, @vRegionReplacedQueryAsInUI,@vRegionReplacedQuery,@vIsValidSVOCCondition,@vRegionReplacedQueryBillAsInUI, @vRegionReplacedQueryBill,@vIsValidBillCondition, @RegionSegmentId, @vErrMsg,@vSuccess);

					IF @vSuccess!=''
						THEN
											
							Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Region Segment Is Created") )),"]") into @temp_result  ;') ;
			   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
									
							Select RegionSegmentId into @RegionSegmentId From CLM_RegionSegment where RegionSegmentName=Element_Name ;
							
							IF  @vRegionReplacedQueryBill IS NULL OR @vRegionReplacedQueryBill = ''
								THEN
									SET @SVOT_Condition='SELECT 1 from DUAL';
							ELSE
								SET @SVOT_Condition = CONCAT('SELECT 1 from V_CLM_Bill_Detail_One_View svot WHERE ',@vRegionReplacedQueryBill,'');
							END IF;
									
							
							if @vRegionReplacedQuery='' then Set @vRegionReplacedQuery='1=1'; End If;			
							Set @Sql1=CONCAT('Update CLM_RegionSegment 
															Set Coverage = ',@REGION_COUNT,',
																In_Use ="Yes"
													where RegionSegmentId=',@RegionSegmentId,';');
													
							SELECT @Sql1;
							prepare stmt1 from @Sql1;
							execute stmt1;
							deallocate prepare stmt1;
										   
					End if; 
					
					IF @vSuccess =''
						THEN
											
										Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message",@vErrMsg) )),"]") into @temp_result                       
															  ;') ;
			   
										SELECT @Query_String;
										PREPARE statement from @Query_String;
										Execute statement;
										Deallocate PREPARE statement; 
									
					End if; 
			End If;		
			Delete From SVOC_UI_CONDITION_MAKER
			Where  CONDITION_PAGE=Element_Creation and CONDITION_NAME= Element_Name;
	END IF;
	
	
	If Element_Creation ="REGION" and (SegmentId is not Null or SegmentId='') 
		THEN
				IF IN_ACTION="Yes"
				THEN
					Update CLM_RegionSegment
					Set In_Use="Yes"
					Where RegionSegmentId=SegmentId;
					
					-- INSERT into UI_Campaign_Task_List (UI_Campaign_Task, Run_Mode, Event_Id,Status,CreatedDate) 
						--					   values ('RegionSegment_RUN',"",SegmentId,"START", current_timestamp());

					Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Region Segment Is Enabled") )),"]") into @temp_result                       
													  ;') ;
	   
								SELECT @Query_String;
								PREPARE statement from @Query_String;
								Execute statement;
								Deallocate PREPARE statement; 								
				End If;	

				IF IN_ACTION="No"
				THEN
					Update CLM_RegionSegment
					Set In_Use="No"
					Where RegionSegmentId=SegmentId;
					
					-- INSERT into UI_Campaign_Task_List (UI_Campaign_Task, Run_Mode, Event_Id,Status,CreatedDate) 
					-- values ('RegionSegment_Delete',"",SegmentId,"START", current_timestamp());
					
					Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Region Segment Is Disabled") )),"]") into @temp_result                       
													  ;') ;
	   
					SELECT @Query_String;
					PREPARE statement from @Query_String;
					Execute statement;
					Deallocate PREPARE statement; 
								
								
				End If;							   				
	
    End If;	
	
	If Element_Creation ="REGION_DETAILS" and (SegmentId is not Null or SegmentId='') 
		THEN
		
			
			Delete from SVOC_UI_CONDITION_MAKER 
			where CONDITION_PAGE="REGION" and CONDITION_NAME in (Select RegionSegmentName  FROM CLM_RegionSegment where RegionSegmentId =SegmentId );
				
			Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Element_ID",RegionSegmentId,"Element_Name",RegionSegmentName,"SVOC_Replaced_Query",RegionReplacedQuery,"SVOT_Replaced_Query",RegionReplacedQueryBill) )),"]") into @temp_result  
                             FROM CLM_RegionSegment where RegionSegmentId= ',SegmentId,'						
													  ;') ;
	   
								SELECT @Query_String;
								PREPARE statement from @Query_String;
								Execute statement;
								Deallocate PREPARE statement;
										   				
	
    End If;	
	
	If Element_Creation ="CAMPAIGN"  AND (IN_ACTION IS  NULL or IN_ACTION ='')
	    THEN
        
			INSERT IGNORE INTO CLM_Campaign(CampaignName)
            VALUES(Element_Name);
            
			SET @vSuccess="Success";
	        
			IF @vSuccess!=''
		       THEN
								
							SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Campaign Is Created ","Campaign_Id",CampaignId,"Campaign_Name",CampaignName) )),"]")
										    into @temp_result from CLM_Campaign
                                            WHERE CampaignName = "',Element_Name,'"
                                            LIMIT 1
										   ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
						
	    End if; 
		
		
		
	
		IF @vSuccess =''
	    	THEN
								
							Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Campaign Not Created") )),"]") into @temp_result                       
												  ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
						
	    End if; 
	

	
	END IF;
	
	
	Select Element_Creation;
		If Element_Creation ="CAMPAIGN_DETAILS" and (SegmentId is not Null or SegmentId !='') 
		THEN
		
				
				
			Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Element_ID",CampaignId,"Element_Name",CampaignName,"Element_Type",CampaignDesc,"Element_ThemeName",CampaignThemeName,
			"Element_ThemeId",A.CampaignThemeId,"Element_SegmentName",CLMSegmentName,"Element_SegmentId",A.CLMSegmentId,"Element_CGPERCENTAGE",CampaignCGPercentage,"Element_RESPONSEDAYS",CampaignResponseDays,			"Element_STARTDATE",CampaignStartDate,"Element_ENDDATE",CampaignEndDate) )),"]") into @temp_result  
                             FROM CLM_Campaign A,CLM_CampaignTheme B,CLM_Segment C
							 where CampaignId= ',SegmentId,'	and  A.CampaignThemeId=B.CampaignThemeId and A.CLMSegmentId= C.CLMSegmentId					
													  ;') ;
													  


														  
													  
	   
								SELECT @Query_String;
								PREPARE statement from @Query_String;
								Execute statement;
								Deallocate PREPARE statement;
										   				
	
    End If;	
	
	
	If Element_Creation ="THEME"  AND (IN_ACTION IS  NULL or IN_ACTION ='')
	   THEN
			SET @vUserId='1';
			SET @vCampaignThemeName='';
			SET @vCampaignThemeDesc='';
			SET @vExecution_Sequence='1';
			SET @vCampaignThemeId='';
			SET @vErrMsg='';
			SET @vSuccess='';
			SET @vUserId='1';
			
			SET @vCampaignThemeName=Element_Name;
			SET @vCampaignThemeDesc=Element_Type;
			SET @vExecution_Sequence='1';
			Select CampaignThemeId into @vCampaignThemeId From CLM_CampaignTheme where CampaignThemeName=Element_Name ;
			SET @vErrMsg='';
			SET @vSuccess='';
			CALL PROC_UI_CU_CLM_CampaignTheme(@vUserId,@vCampaignThemeName, @vCampaignThemeDesc,@vExecution_Sequence,@vCampaignThemeId,@vErrMsg,@vSuccess);

			
			IF @vSuccess!=''
		       THEN
								
							Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","THEME IS CREATED") )),"]") into @temp_result                       
												  ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
						
	       End if; 
		
			IF @vSuccess =''
			   THEN
									
								Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message",@vErrMsg) )),"]") into @temp_result                       
													  ;') ;
	   
								SELECT @Query_String;
								PREPARE statement from @Query_String;
								Execute statement;
								Deallocate PREPARE statement; 
							
		    End if; 
		

	
	END IF;
	
	IF Element_Creation ="CAMPAIGNSEGMENT_DISPLAY"
	THEN 
			Set @View_STM= Concat('Create or replace View CAMPAIGNSEGMENT_DISPLAY AS(Select MicroSegmentId,MicroSegmentName,concat(MSReplacedQuery,case when (MSReplacedQueryBill is null or MSReplacedQueryBill="") then " " else concat(" and ",MSReplacedQueryBill) end )as `Condition` ,Convert(format(Coverage,","),CHAR) as Coverage,In_Use, 
			SUBSTRING(ModifiedDate,6,11) as ModifiedDate From CLM_MicroSegment where In_Use ="',InUse,'" );');
			
			SELECT @View_STM;
			PREPARE statement from @View_STM;
			Execute statement;
			Deallocate PREPARE statement; 
			
			Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object(  "Id",`MicroSegmentId`,"Name",`MicroSegmentName`,"Condition",`Condition`,"Coverage",Coverage,"Date",ModifiedDate,"In_Use",In_Use)order by ModifiedDate desc)),"]") into @temp_result 
										FROM CAMPAIGNSEGMENT_DISPLAY
												  ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
	END IF;
			
	IF Element_Creation ="LIFECYCLESEGMENT_DISPLAY"
		THEN 
			Set @View_STM= Concat('Create or replace View SEGMENT_DISPLAY AS(Select CLMSegmentId,CLMSegmentName,CLMSegmentReplacedQuery,Convert(format(Coverage,","),CHAR) as Coverage,In_Use, 
			SUBSTRING(ModifiedDate,6,11) as ModifiedDate From CLM_Segment  where In_Use = "',InUse,'");');
			
			SELECT @View_STM;
			PREPARE statement from @View_STM;
			Execute statement;
			Deallocate PREPARE statement; 
				
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object(  "Id",`CLMSegmentId`,"Name",`CLMSegmentName`,"Condition",`CLMSegmentReplacedQuery`,"Coverage",Coverage,"Date",ModifiedDate,"In_Use",In_Use)order by ModifiedDate desc)),"]")  into @temp_result 
											FROM SEGMENT_DISPLAY
													  ;') ;
													  
				SELECT @Query_String;
								PREPARE statement from @Query_String;
								Execute statement;
								Deallocate PREPARE statement; 
													  
	END IF;

	IF Element_Creation ="REGION_DISPLAY"
		THEN 
		
				Set @View_STM= Concat('Create or replace View REGIONSEGMENT_DISPLAY AS(Select RegionSegmentId,RegionSegmentName,concat(RegionReplacedQuery,case when (RegionReplacedQueryBill is null or RegionReplacedQueryBill ="") then "" else concat (" and ",RegionReplacedQueryBill)end ) as `Condition`,Convert(format(Coverage,","),CHAR) as Coverage,In_Use, 
			SUBSTRING(ModifiedDate,6,11) as ModifiedDate From CLM_RegionSegment where In_Use = "',InUse,'");');
			
			SELECT @View_STM;
			PREPARE statement from @View_STM;
			Execute statement;
			Deallocate PREPARE statement; 
			
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object(  "Id",`RegionSegmentId`,"Name",`RegionSegmentName`,"Condition",`Condition`,"Coverage",Coverage,"Date",ModifiedDate,"In_Use",In_Use)order by ModifiedDate desc)),"]")  into @temp_result 
											FROM REGIONSEGMENT_DISPLAY
													  ;') ;
	   
								SELECT @Query_String;
								PREPARE statement from @Query_String;
								Execute statement;
								Deallocate PREPARE statement; 
	END IF;

	IF Element_Creation ="CAMPAIGN_DISPLAY"
	THEN 
	
		 SET @Query_String1= Concat('create or replace view CAMPAIGN_DISPLAY as   SELECT 
							`CLM_Campaign`.`CampaignId` As `Campaign ID`,
						 `CLM_Campaign`.`CampaignName` As `Campaign Name`,
						 CASE
							WHEN `CLM_Campaign`.`CampaignState` = 1 THEN "Enabled"
						ELSE "Disabled"
						END AS `Status`,
						`CLM_CampaignTheme`.`CampaignThemeName` As `Campaign Theme`,
						`CLM_Segment`.`CLMSegmentName` As `Lifecycle Segment`,
						count(`CLM_Campaign_Events`.`CampTriggerId`) As `# Triggers`,
						 count(`CLM_Campaign_Events`.`CreativeId`) As `# Creatives`,
						concat(`CLM_Campaign`.`CampaignResponseDays`, " days") AS `Attribution`,
						date_format(`CLM_Campaign`.`CampaignStartDate`,"%d-%m-%Y") AS `From`,
						 date_format(`CLM_Campaign`.`CampaignEndDate`,"%d-%m-%Y") AS `To`,
						SUBSTRING(CLM_Campaign.ModifiedDate,6,11) as ModifiedDate 
						 FROM
						(((`CLM_CampaignTheme`
						JOIN `CLM_Segment`)
						JOIN `CLM_Campaign`)
						Left JOIN `CLM_Campaign_Events` on  `CLM_Campaign_Events`.`CampaignId` = `CLM_Campaign`.`CampaignId`)
					
						where CLM_Campaign.CampaignThemeId=CLM_CampaignTheme.CampaignThemeId
						and CLM_Campaign.CLMSegmentId=CLM_Segment.CLMSegmentId
						
						group by `CLM_Campaign`.`CampaignId`, `CLM_Campaign`.`CampaignName`
						
						order by `CLM_Campaign`.ModifiedDate desc;');
							
				select @Query_String1;
				PREPARE statement1 from @Query_String1;
				Execute statement1;
				Deallocate PREPARE statement1; 
				
			Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Campaign_Id",`Campaign ID`,"Campaign_Name",`Campaign Name`,"Campaign_Theme",`Campaign Theme`,"Lifecycle_Segment",`Lifecycle Segment`,"No_Triggers",`# Triggers`,"No_Creatives",`# Creatives`,"Attribution",`Attribution`,"Date",`ModifiedDate`  ))),"]") into @temp_result 
										FROM CAMPAIGN_DISPLAY
												  ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
	END IF;
	
	IF Element_Creation ="THEME_DISPLAY"
	THEN 
	
			
			  SET @Query_String1= Concat('create or replace view Campaign_Theme_Display as SELECT 
			                   `CLM_CampaignTheme`.`CampaignThemeId` AS `Campaign Theme ID`,
								`CLM_CampaignTheme`.`CampaignThemeName` AS `Campaign Theme Name`,
								SUBSTRING(CLM_CampaignTheme.ModifiedDate,6,11) AS `Date Created`,
								count( `CLM_Campaign`.`CampaignName`) As `# Campaigns`,
							   (select count( `CLM_Campaign`.`CampaignName`) from `CLM_Campaign` where `CampaignState`=1 and `CLM_Campaign`.`CampaignThemeId`=`CLM_CampaignTheme`.`CampaignThemeId`) As `# Active Campaigns`
								FROM
								`CLM_CampaignTheme`
								left join `CLM_Campaign` on (`CLM_Campaign`.`CampaignThemeId` = `CLM_CampaignTheme`.`CampaignThemeId`)     
							
								
								group by CLM_CampaignTheme.`CampaignThemeId`,CLM_CampaignTheme.CampaignThemeName
								
								order by CLM_CampaignTheme.ModifiedDate desc;');
               select @Query_String1;
                PREPARE statement1 from @Query_String1;
                Execute statement1;
                Deallocate PREPARE statement1; 
				
				
			Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Campaign_Theme_Id",`Campaign Theme ID`,"Campaign_Theme_Name",`Campaign Theme Name`,"Date",`Date Created`,"No_Campaigns",`# Campaigns`,"No_Active_Campaigns",`# Active Campaigns` ))),"]") into @temp_result 
										FROM Campaign_Theme_Display
												  ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
	END IF;
	
	
	IF Element_Creation ="TRIGGER_DISPLAY"
		THEN 
						
				Create or replace View ALl_Trigger as (
					Select ChannelName,CLMSegmentName,Segment_Name as Campaign_Segment,Trigger_Name,
					Case when RegionSegmentName is null then "NA" else RegionSegmentName end as Region_Segment,
					CampaignThemeName,CampaignName,Performance_Based,Event_Sequence,Replace(Replace(Creative,"<SOLUS_PFIELD>","#"),"</SOLUS_PFIELD>","#") as Creative,
					Case when RankedPickList_Stories_Name is null Then "NA" else RankedPickList_Stories_Name end as Stories_Name,
					Favourite_Day,Additional_Condition,
					Case when Communication_Cooloff_Days is null then "NA" else Communication_Cooloff_Days end as Communication_Cooloff_Days,
					`State`,
					Case when Scheduled is null then "NA" else Scheduled end as Scheduled

					From
							(Select
							 ChannelName,CLMSegmentName,Segment_Name,RegionSegmentName,
							 CampaignThemeName,CampaignName,CampTriggerName as Trigger_Name,
							 
							 Case when TriggerUsesWaterfall =100 then "Yes"  else "NO" end as Performance_Based,
							 row_number()over(order by CLM_Campaign_Events.Execution_Sequence)as Event_Sequence ,Creative,
							 RankedPickList_Stories_Master.Name as RankedPickList_Stories_Name,
							
							 Case when CTReplacedQuery Like "%Lt_Fav_Day_Of_Week%" THEN Concat("Favourite_Day: YES")
								else "NA"
								end as Favourite_Day,
								Case when CTReplacedQuery != "1=1" and  CTReplacedQuery Not Like "%Lt_Fav_Day_Of_Week =weekDay(now())" THEN 
										CTReplacedQuery
									when CTReplacedQuery != "1=1" and  CTReplacedQuery = "Lt_Fav_Day_Of_Week =weekDay(now())" THEN  "NA" 	
								 
								when CTReplacedQuery != "1=1" and  CTReplacedQuery  Like "%Lt_Fav_Day_Of_Week =weekDay(now())" THEN 
												Concat(Replace(CTReplacedQuery," and Lt_Fav_Day_Of_Week =weekDay(now())  ",""))   
							   
								else "NA"
								end as Additional_Condition,
							 Website_URL,
							 Case when TriggerUsesWaterfall =99 then "NoCoolOff" 
									 else Communication_Cooloff
								end as Communication_Cooloff_Days,
								CASE
										WHEN `CLM_Campaign_Trigger`.`CTState` = 1 THEN 'Enabled'
										WHEN `CLM_Campaign_Trigger`.`CTState` = 2 THEN 'Testing'
										WHEN `CLM_Campaign_Trigger`.`CTState` = 3 THEN 'Draft'
										WHEN `CLM_Campaign_Trigger`.`CTState` = 4 THEN 'Verified'
										WHEN `CLM_Campaign_Trigger`.`CTState` = 5 THEN 'Disabled'
										ELSE 'Disabled'
									END AS `State`,
							   concat("From: ",TriggerStartDate," To ",TriggerEndDate )as Scheduled
							   
								
								
								FROM
										((((((((((((`CLM_CampaignTheme`
										JOIN `CLM_Segment`)
										JOIN `CLM_Campaign`)
										JOIN `CLM_Campaign_Trigger`)
										JOIN `CLM_MicroSegment`)
										JOIN `CLM_Channel`)
										JOIN `CLM_Campaign_Events`)
										JOIN `CLM_Creative`)
										Join Event_Master_UI)
										
										LEFT JOIN `CLM_Offer` `CO` ON (`CLM_Creative`.`OfferId` = `CO`.`OfferId`))
										Left Join CLM_RegionSegment on (`CLM_Campaign_Events`.`RegionSegmentId` = `CLM_RegionSegment`.`RegionSegmentId` ))
										Left Join Campaign_Segment on (`CLM_Campaign_Events`.`Campaign_SegmentId` = `Campaign_Segment`.`Segment_Id` ))
										Left Join RankedPickList_Stories_Master on (RankedPickList_Stories_Master.Story_Id=CLM_Creative.RankedPickListStory_Id))
									WHERE
										`CLM_Campaign_Events`.`CampaignThemeId` = `CLM_CampaignTheme`.`CampaignThemeId`
											AND `CLM_Campaign_Events`.`CLMSegmentId` = `CLM_Segment`.`CLMSegmentId`
											AND `CLM_Campaign_Events`.`CampaignId` = `CLM_Campaign`.`CampaignId`
											AND `CLM_Campaign_Events`.`CampTriggerId` = `CLM_Campaign_Trigger`.`CampTriggerId`
											AND `CLM_Campaign_Events`.`MicroSegmentId` = `CLM_MicroSegment`.`MicroSegmentId`
											AND `CLM_Campaign_Events`.`ChannelId` = `CLM_Channel`.`ChannelId`
											AND `CLM_Campaign_Events`.`CreativeId` = `CLM_Creative`.`CreativeId`
											  AND `CLM_Campaign_Events`.`EventId` = `Event_Master_UI`.`Event_ID`
					)A
				Order by `State`	
				);


				  call S_dashboard_performance_download('ALl_Trigger',@out_result_json1);
			
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object(  "Channel",`ChannelName`,"Segment",`CLMSegmentName`,"Theme",CampaignThemeName,"Campaign",CampaignName,"Trigger",Trigger_Name,"WaterFall",Event_Sequence,"Performance_Based",Performance_Based,"Stories_Name",Stories_Name,"Creative",Creative,"Additional_Condition",Additional_Condition,"Favourite_Day",Favourite_Day,"Campaign_Segment",`Campaign_Segment`,"Region_Segment",Region_Segment,"Communication_Cooloff_Days",Communication_Cooloff_Days,"State",`State`,"Scheduled",Scheduled))),"]")  into @temp_result 
											FROM ALl_Trigger
													  ;') ;
	   
								SELECT @Query_String;
								PREPARE statement from @Query_String;
								Execute statement;
								Deallocate PREPARE statement; 
	END IF;
	
	IF Element_Creation ="TRIGGER_DISPLAY_DOWNLOAD"
		THEN 
						
				Create or replace View ALl_Trigger as (
					Select ChannelName,CLMSegmentName,Segment_Name as Campaign_Segment,Trigger_Name,
					Case when RegionSegmentName is null then "NA" else RegionSegmentName end as Region_Segment,
					CampaignThemeName,CampaignName,Performance_Based,Event_Sequence,Creative as Creative,
					Case when RankedPickList_Stories_Name is null Then "NA" else RankedPickList_Stories_Name end as Stories_Name,
					Favourite_Day,Additional_Condition,
					Case when Communication_Cooloff_Days is null then "NA" else Communication_Cooloff_Days end as Communication_Cooloff_Days,
					`State`,
					Case when Scheduled is null then "NA" else Scheduled end as Scheduled

					From
							(Select
							 ChannelName,CLMSegmentName,Segment_Name,RegionSegmentName,
							 CampaignThemeName,CampaignName,CampTriggerName as Trigger_Name,
							 
							 Case when TriggerUsesWaterfall =100 then "Yes"  else "NO" end as Performance_Based,
							 row_number()over(order by CLM_Campaign_Events.Execution_Sequence)as Event_Sequence ,Creative,
							 RankedPickList_Stories_Master.Name as RankedPickList_Stories_Name,
							
							 Case when CTReplacedQuery Like "%Lt_Fav_Day_Of_Week%" THEN Concat("Favourite_Day: YES")
								else "NA"
								end as Favourite_Day,
								Case when CTReplacedQuery != "1=1" and  CTReplacedQuery Not Like "%Lt_Fav_Day_Of_Week =weekDay(now())" THEN 
										CTReplacedQuery
									when CTReplacedQuery != "1=1" and  CTReplacedQuery = "Lt_Fav_Day_Of_Week =weekDay(now())" THEN  "NA" 	
								 
								when CTReplacedQuery != "1=1" and  CTReplacedQuery  Like "%Lt_Fav_Day_Of_Week =weekDay(now())" THEN 
												Concat(Replace(CTReplacedQuery," and Lt_Fav_Day_Of_Week =weekDay(now())  ",""))   
							   
								else "NA"
								end as Additional_Condition,
							 Website_URL,
							 Case when TriggerUsesWaterfall =99 then "NoCoolOff" 
									 else Communication_Cooloff
								end as Communication_Cooloff_Days,
								CASE
										WHEN `CLM_Campaign_Trigger`.`CTState` = 1 THEN 'Enabled'
										WHEN `CLM_Campaign_Trigger`.`CTState` = 2 THEN 'Testing'
										WHEN `CLM_Campaign_Trigger`.`CTState` = 3 THEN 'Draft'
										WHEN `CLM_Campaign_Trigger`.`CTState` = 4 THEN 'Verified'
										WHEN `CLM_Campaign_Trigger`.`CTState` = 5 THEN 'Disabled'
										ELSE 'Disabled'
									END AS `State`,
							   concat("From: ",TriggerStartDate," To ",TriggerEndDate )as Scheduled
							   
								
								
								FROM
										((((((((((((`CLM_CampaignTheme`
										JOIN `CLM_Segment`)
										JOIN `CLM_Campaign`)
										JOIN `CLM_Campaign_Trigger`)
										JOIN `CLM_MicroSegment`)
										JOIN `CLM_Channel`)
										JOIN `CLM_Campaign_Events`)
										JOIN `CLM_Creative`)
										Join Event_Master_UI)
										
										LEFT JOIN `CLM_Offer` `CO` ON (`CLM_Creative`.`OfferId` = `CO`.`OfferId`))
										Left Join CLM_RegionSegment on (`CLM_Campaign_Events`.`RegionSegmentId` = `CLM_RegionSegment`.`RegionSegmentId` ))
										Left Join Campaign_Segment on (`CLM_Campaign_Events`.`Campaign_SegmentId` = `Campaign_Segment`.`Segment_Id` ))
										Left Join RankedPickList_Stories_Master on (RankedPickList_Stories_Master.Story_Id=CLM_Creative.RankedPickListStory_Id))
									WHERE
										`CLM_Campaign_Events`.`CampaignThemeId` = `CLM_CampaignTheme`.`CampaignThemeId`
											AND `CLM_Campaign_Events`.`CLMSegmentId` = `CLM_Segment`.`CLMSegmentId`
											AND `CLM_Campaign_Events`.`CampaignId` = `CLM_Campaign`.`CampaignId`
											AND `CLM_Campaign_Events`.`CampTriggerId` = `CLM_Campaign_Trigger`.`CampTriggerId`
											AND `CLM_Campaign_Events`.`MicroSegmentId` = `CLM_MicroSegment`.`MicroSegmentId`
											AND `CLM_Campaign_Events`.`ChannelId` = `CLM_Channel`.`ChannelId`
											AND `CLM_Campaign_Events`.`CreativeId` = `CLM_Creative`.`CreativeId`
											  AND `CLM_Campaign_Events`.`EventId` = `Event_Master_UI`.`Event_ID`
					)A
				Order by `State`	
				);


				  call S_dashboard_performance_download('ALl_Trigger',@out_result_json1);
			
	END IF;

		IF Element_Creation = "EXCLUSION_FILTER"  AND (IN_ACTION IS  NULL or IN_ACTION ='')
	   THEN
		    
			Set	@vEFName='';
			Set	@vEFDesc='';
			Set	@vEFChannelId='';
			Set @vEFTReplacedQueryAsInUI='';
			Set @vEFReplacedQuery='';
			Set	@vEFReplacedQueryBillAsInUI='';
			Set @vEFReplacedQueryBill='';
			set @vEFId='';
			Set @vUserId='1';
			Set	@vEFName=Element_Name;
			Set	@vEFDesc=Element_Type;
			Set	@vEFChannelId=Channel_ID;
			Set @vEFTReplacedQueryAsInUI=SVOC_Replaced_Query;
			Set @vEFReplacedQuery=SVOC_Replaced_Query;
			Set @vIsValidSVOCCondition='1';
	        Set	@vEFReplacedQueryBillAsInUI=SVOT_Replaced_Query;
			Set @vEFReplacedQueryBill=SVOT_Replaced_Query;
			Set	@vIsValidBillCondition='1';
			Set	@vEFState='1';
			select max(EFSequence)+1 into @vEFSequence from CLM_Exclusion_Filter;
		   
			set @vErrMsg='';
			set @vSuccess='';
			
			 Set @SQL1=Concat('Select EFId into @vEFId From CLM_Exclusion_Filter where EFName="',Element_Name,'"') ;
			SELECT @SQL1;
			PREPARE statement from @SQL1;
			Execute statement;
			Deallocate PREPARE statement;
			
			Set @SVOC_UI_CONDITION_MAKER= concat(' Delete from SVOC_UI_CONDITION_MAKER 
			where CONDITION_PAGE="EXCLUSION_FILTER" and CONDITION_NAME="',@vEFName,'"                      
													  ;') ;
			SELECT @SVOC_UI_CONDITION_MAKER;
			PREPARE statement from @SVOC_UI_CONDITION_MAKER;
			Execute statement;
			Deallocate PREPARE statement;	
			
			if @vEFId is null then Set @vEFId =0; end if;
			if @vEFSequence is null then Set @vEFSequence =1; end if;	
			CALL PROC_CLM_CU_CLM_Exclusion_Filter(@vUserId,@vEFName, @vEFDesc, @vEFChannelId ,@vEFTReplacedQueryAsInUI , @vEFReplacedQuery, @vIsValidSVOCCondition, @vEFReplacedQueryBillAsInUI , @vEFReplacedQueryBill , @vIsValidBillCondition , @vEFState , @vEFSequence , @vEFId ,@vErrMsg , @vSuccess );

		Select  @vSuccess;

					IF @vSuccess=0
						THEN
											
							Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Exculsion Filter is Created") )),"]") into @temp_result  ;') ;
			   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
									
							
					End if; 
					
					IF @vSuccess!=0
						THEN
											
										Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message",@vErrMsg) )),"]") into @temp_result                       
															  ;') ;
			   
										SELECT @Query_String;
										PREPARE statement from @Query_String;
										Execute statement;
										Deallocate PREPARE statement; 
									
					End if; 
				
			Delete From SVOC_UI_CONDITION_MAKER
			Where  CONDITION_PAGE=Element_Creation and CONDITION_NAME= Element_Name;
	END IF;

	
	If Element_Creation ="EXCLUSION_FILTER" and (SegmentId is not Null or SegmentId='') 
		THEN
				IF IN_ACTION="Yes"
				THEN
					
					Update CLM_Exclusion_Filter
					Set EFState = '1'
					Where EFId=SegmentId;
					
					Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Exclusion Filter Is Enabled") )),"]") into @temp_result  ;') ;
	   
								SELECT @Query_String;
								PREPARE statement from @Query_String;
								Execute statement;
								Deallocate PREPARE statement; 								
				End If;	

				IF IN_ACTION="No"
				THEN
				
					Update CLM_Exclusion_Filter
					Set EFState='0'
					Where EFId=SegmentId;
					
					
					Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Exclusion Filter Is Disabled") )),"]") into @temp_result ;') ;
	   
					SELECT @Query_String;
					PREPARE statement from @Query_String;
					Execute statement;
					Deallocate PREPARE statement; 
								
								
				End If;							   				
	
    End If;	
	
	If Element_Creation ="EXCLUSION_FILTER_DETAILS" and (SegmentId is not Null or SegmentId='') 
		THEN
		
			
			Delete from SVOC_UI_CONDITION_MAKER 
			where CONDITION_PAGE="EXCLUSION_FILTER" and CONDITION_NAME in (Select EFName  FROM CLM_Exclusion_Filter where EFId =SegmentId );
				
			Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Element_ID",EFId,"Element_Name",EFName,"Element_Type",EFDesc,"SVOC_Replaced_Query",EFReplacedQuery,"SVOT_Replaced_Query",EFReplacedQueryBill,"Channel",ChannelName,"Channel_Id",EFChannelId) )),"]") into @temp_result  
                             FROM CLM_Exclusion_Filter A, CLM_Channel B where   A.EFChannelId=B.ChannelId and EFId= ',SegmentId,'						
													  ;') ;
	   
								SELECT @Query_String;
								PREPARE statement from @Query_String;
								Execute statement;
								Deallocate PREPARE statement;
										   				
	
    End If;	
	
	
	IF Element_Creation ="EXCLUSION_FILTER_DISPLAY"
		THEN 
			If InUse ="Yes"
				THEN Set InUse ='EFState = "1"';
				
			ELSE Set InUse ='EFState != "1"';
			
			End If;
			
			Set @View_STM= Concat('Create or replace View Exclusion_DISPLAY AS(Select EFId,EFName,concat(EFReplacedQuery,case when (EFReplacedQueryBill is null or 		EFReplacedQueryBill ="") then "" else concat (" and ",EFReplacedQueryBill)end ) as `Condition`, ChannelName,EFSequence,
			SUBSTRING(A.ModifiedDate,6,11) as ModifiedDate,Case when EFState ="1" then "Yes" else "No"	 end as In_Use	From CLM_Exclusion_Filter A, CLM_Channel B
			WHERE  A.EFChannelId=B.ChannelId and  ',InUse,');');
			
			SELECT @View_STM;
			PREPARE statement from @View_STM;
			Execute statement;
			Deallocate PREPARE statement; 
			
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object(  "Id",`EFId`,"Name",`EFName`,"Condition",`Condition`,"Channel",ChannelName,"Sequence",EFSequence,"Date",ModifiedDate,"In_Use",In_Use)order by ModifiedDate desc)),"]")  into @temp_result 
											FROM Exclusion_DISPLAY
													  ;') ;
	   
								SELECT @Query_String;
								PREPARE statement from @Query_String;
								Execute statement;
								Deallocate PREPARE statement; 
	END IF;
	
	
	
		
	
	IF Element_Creation ="CHANNEL_DISPLAY"
		THEN 
			
			
			
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Id",ChannelId,"Name",ChannelName,"Descripition",`ChannelDesc`,"Date",SUBSTRING(ModifiedDate,6,11))order by ModifiedDate desc)),"]")  into @temp_result 
											FROM CLM_Channel
													  ;') ;
	   
								SELECT @Query_String;
								PREPARE statement from @Query_String;
								Execute statement;
								Deallocate PREPARE statement; 
	END IF;


	
	If @temp_result is null
		Then
								
							Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Invalid Parameters") )),"]") into @temp_result                       
												  ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
						
	End if;
							

    SET out_result_json = @temp_result;
	
END$$
DELIMITER ;
