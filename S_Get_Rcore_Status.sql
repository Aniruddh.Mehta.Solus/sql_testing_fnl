drop procedure if exists S_Get_Rcore_Status;

delimiter $$
create procedure S_Get_Rcore_Status (out rcoreStatus json)
begin
	select  CONCAT("[",(GROUP_CONCAT(json_object("Stage",Stage,
					"Segment",Segment,
                    "Source",Src,
                    "Algp",Algo,
                    "AlgoType",AlgoType,
                    "CompletionStatus",CompletionStatus,
                    "Error",Error,
                    "ExecutionTime",ExecutionTime,
                    "LTD",LTD))),"]") into rcoreStatus from Rcore_Status;
end$$
delimiter ;