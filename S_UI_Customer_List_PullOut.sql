DELIMITER $$
CREATE or replace PROCEDURE `S_UI_Customer_List_PullOut`(IN in_request_json json, OUT out_result_json json)
BEGIN
    
    -- User Information

    declare IN_USER_NAME varchar(240); 
    declare IN_SCREEN varchar(400); 
    declare vSuccess  varchar(400); 
    declare vErrMsg  varchar(400);
    declare vSQLCondition Text;
    declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text; 
	declare IN_AGGREGATION_3 text;
	declare Name text;
    -- Target Audience  
  
    declare Campaign_Segment varchar(400);
    declare Lifecycle_Segment varchar(400);
	declare Lifecycle_SegmentCondition varchar(400) Default '';
    declare Region varchar(400) Default '';
    -- Advanced RFMP Criteria
    declare Valid_Mobile varchar(400);
	declare Valid_Email varchar(400);
	declare DND varchar(400);
	declare Favourite_Day Varchar(128);
    declare Throtling Bigint;
    
    declare Recency_Start Bigint;
    declare Recency_End Bigint;
    declare Vintage Bigint;
    declare Visit Bigint;
    declare Monetary_Value Bigint;
    declare Last_Bought_Category Bigint;
    declare Favourite_Category Bigint;
	declare SVOC_Condition text;
	declare SVOT_Condition text;
	declare Start_Event_Date text;
    declare End_Event_Date text; 	
	declare Event_Id text; 
    	

  -- --------------------- Decoding the JSON -----------------------------------------------------------------------------------------------------------------------------------------------
  
  
    
	Set in_request_json = Replace(in_request_json,'\n','\\n');
   
   -- User Information
   
    SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1")); 
    SET Name = json_unquote(json_extract(in_request_json,"$.Name")); 
    SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2")); 
	
	Set IN_AGGREGATION_2 = Replace(Replace(IN_AGGREGATION_2,"[",''),"]",'');
 
    SET Campaign_Segment = json_unquote(json_extract(in_request_json,"$.Campaign_Segment"));
    
    -- Advanced RFMP Criteria
    SET SVOC_Condition = json_unquote(json_extract(in_request_json,"$.SVOC_Condition"));
	 SET SVOT_Condition = json_unquote(json_extract(in_request_json,"$.SVOT_Condition"));
	 
	 
	Set SVOC_Condition =replace (SVOC_Condition,"\n",'');
	Set SVOT_Condition =replace (SVOT_Condition,"\n",'');

	
	SET Start_Event_Date = json_unquote(json_extract(in_request_json,"$.Event_Start_Date"));
	SET End_Event_Date = json_unquote(json_extract(in_request_json,"$.Event_End_Date"));
	SET Event_Id = json_unquote(json_extract(in_request_json,"$.Event_Name"));
	SET @Start_Event_Date = date_format(Start_Event_Date,"%Y%m%d");
	SET @End_Event_Date = date_format(End_Event_Date,"%Y%m%d");
	SET @Event_Id = Event_Id;
	SET @temp_result = Null;
	
	SET @Campaign_SegmentReplacedQuery = ' ';
	



    
    -- Creating the Replace Queary
   SET @Replaced_Query="1=1";	
   
	
	If Event_ID is not null or Event_ID !=''
	Then SET @Replaced_Query=concat(@Replaced_Query," AND Customer_Id in (select Customer_Id from Event_Execution_History where Event_ID =
	     ",@Event_Id," AND Event_Execution_Date_ID between ",@Start_Event_Date," and " ,End_Event_Date,")
	   ");	
	End If;
	
	Select SVOC_Condition;
	
	If SVOC_Condition != ' '
	Then SET @Replaced_Query=concat(@Replaced_Query," AND ",SVOC_Condition);	
	End If;
	select @Replaced_Query;
	
Select IN_AGGREGATION_1,SVOC_Condition;

	IF IN_AGGREGATION_1 ="Coverage" 
	          Then    
	           
	             
	          
						Select Segment_Condition_Solus into @Campaign_SegmentReplacedQuery  from Campaign_Segment where Segment_Id=Campaign_Segment;
						
						If @Campaign_SegmentReplacedQuery is Null or @Campaign_SegmentReplacedQuery ='' 
							Then Set @Campaign_SegmentReplacedQuery="1=1";
						End If;	
						
						Select @Campaign_SegmentReplacedQuery;
						If @Replaced_Query is Null or @Replaced_Query =''
						THEN SET @Replaced_Query="1=1";	
						end If;	
						
	                    
	                    set vSQLCondition= concat(@Replaced_Query,'  and ', @Campaign_SegmentReplacedQuery) ;
	                    
	                    Select vSQLCondition;
						
						
						IF  SVOT_Condition IS  NULL OR SVOT_Condition =''
							THEN
								SET @Advance_SVOT_Condition='SELECT 1 from DUAL';
						ELSE		
								Set @TriggerSVOT_Condition=SVOT_Condition;
								SET @Advance_SVOT_Condition = CONCAT('SELECT 1 from V_CLM_Bill_Detail_One_View svot 
								WHERE svot.Customer_id=svoc.Customer_id and ',@TriggerSVOT_Condition,')');
						END IF; 

	                    SET @sql_stmt = CONCAT('create or replace view Customer_List_PullOut as (SELECT Customer_Id
													FROM V_CLM_Customer_One_View svoc
													WHERE  ',vSQLCondition,' 
														AND EXISTS (',@Advance_SVOT_Condition,')
														
														)
														
												');
	                    
	                    Select @sql_stmt;
	                    PREPARE stmt FROM @sql_stmt;
	                    EXECUTE stmt; 
	                    DEALLOCATE PREPARE stmt;
						
						Set @File_Path='';
						SET @sql_stmt1 = CONCAT('select value into @File_Path from M_Config where Name="File_Path";');
	                    
						Select @sql_stmt1;
						PREPARE stmt FROM @sql_stmt1;
	                    EXECUTE stmt; 
	                    DEALLOCATE PREPARE stmt;
						
						Select Name,@File_Path;
						
						Select Concat('', @File_Path,'/',Name,'.csv') into @Out_FileName_SQL;
						Select @Out_FileName_SQL;
						SELECT `Value` into @Out_FileCharset from M_Config where `Name`='Default_Character_Set';
						SELECT `Value` into @fields_config_check from M_Config where `Name`='Enclose_Output_in_Double_Quotes';
						
						IF @fields_config_check='Y' 
							THEN set @fields_config='FIELDS enclosed by \'\"\' escaped by \"\"';
							ELSE set @fields_config='FIELDS escaped by \"\"' ;
						END IF;

						Set @IN_AGGREGATION_2 = Replace(IN_AGGREGATION_2,'"','');
						
						SET @sqlGenerateFile=concat(' SELECT * INTO OUTFILE "',@Out_FileName_SQL,'" FIELDS TERMINATED BY "," escaped by "\" LINES TERMINATED BY "\n"
														FROM
														(
															Select ',IN_AGGREGATION_2,'
																		UNION all
																		SELECT ',@IN_AGGREGATION_2,'
																		FROM Customer_List_PullOut a, Customer_Details_View b
																		where a.Customer_Id =b.Customer_Id 
														) A
													');
													 
													 Select  @sqlGenerateFile;
	                    PREPARE stmt FROM @sqlGenerateFile;
	                    EXECUTE stmt; 
	                    DEALLOCATE PREPARE stmt;
						
				    Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Downloaded") )),"]") into @temp_result;') ;
   
					SELECT @Query_String;
					PREPARE statement from @Query_String;
					Execute statement;
					Deallocate PREPARE statement; 
                        
                    SET out_result_json= @temp_result;
						
				
				
				
			

	        End If;
			
			
			If @temp_result is null
		Then
								
							Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Invalid Parameters") )),"]") into @temp_result                       
												  ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
						
	End if;
	
	
	
	 SET out_result_json = @temp_result;  

END$$
DELIMITER ;
