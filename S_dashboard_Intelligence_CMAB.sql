DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_Intelligence_CMAB`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text;
	declare IN_FILTER_CHANNEL text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
    SET IN_FILTER_CHANNEL = json_unquote(json_extract(in_request_json,"$.FILTER_CHANNEL"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));

	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    
    IF IN_AGGREGATION_1 = '13W'
    THEN
		set @date_select_query = concat('YEARWEEK(EE_Date,5)');
		set @filter_cond = CONCAT('EE_Date BETWEEN DATE_SUB("',IN_FILTER_CURRENTMONTH,'", INTERVAL 12 WEEK) AND "',IN_FILTER_CURRENTMONTH,'"');
        
        SET @temp_view = concat('
        create or replace View temp_intelligence_CMAB as
        
        SELECT 
		CAST(YEARWEEK(EE_Date) AS UNSIGNED) AS wek
		FROM
		CLM_Event_Dashboard dash
		WHERE
		',@filter_cond,'
        GROUP BY YEARWEEK(EE_Date)
		ORDER BY EE_Date');
	
		select @temp_view;
		PREPARE statement from @temp_view;
		Execute statement;
		Deallocate PREPARE statement;
	
		SELECT group_concat(wek) FROM temp_intelligence_CMAB INTO @distinct_channel;
		select @distinct_channel;
		
		select (length(@distinct_channel)-length(replace(@distinct_channel,',','')))+1 into @num_of_channel;
		set @loop_var=1;
		set @Query_Measure=concat("CASE ");
        select @loop_var,@num_of_channel,@distinct_channel;
		WHILE @loop_var<=@num_of_channel
		do
			SET @selected_channel=substring_index(substring_index(concat(@distinct_channel,',-1'),',',@loop_var),',',-1);
			set @Query_Measure=concat(@Query_Measure," WHEN YEARWEEK(EE_Date,5) = ",@selected_channel," Then 'W",@loop_var,"' 
			");
			set @json_select=concat(@json_select,',"W',@loop_var,'",IFNULL(`W',@loop_var,'`,0)');
			set @loop_var=@loop_var+1;
		end while;
		set @Query_Measure=concat(@Query_Measure," else EE_Date END AS 'w'");
		select @Query_Measure;
        SET @temp_view_stmt = concat('
        SELECT 
		YEARWEEK(EE_Date,5) AS dt,',@Query_Measure,'
		FROM
		CLM_Event_Dashboard dash
		WHERE
		',@filter_cond,'
        GROUP BY YEARWEEK(EE_Date,5)
		ORDER BY EE_Date');
    END IF;
    
    IF IN_AGGREGATION_1 = '26W'
    THEN
		set @date_select_query = concat('YEARWEEK(EE_Date,5)');
		set @filter_cond = CONCAT('EE_Date BETWEEN DATE_SUB("',IN_FILTER_CURRENTMONTH,'", INTERVAL 25 WEEK) AND "',IN_FILTER_CURRENTMONTH,'"');
        SET @temp_view = concat('
        create or replace View temp_intelligence_CMAB as
        
        SELECT 
		CAST(YEARWEEK(EE_Date) AS UNSIGNED) AS wek
		FROM
		CLM_Event_Dashboard dash
		WHERE
		',@filter_cond,'
        GROUP BY YEARWEEK(EE_Date)
		ORDER BY EE_Date');
	
		select @temp_view;
		PREPARE statement from @temp_view;
		Execute statement;
		Deallocate PREPARE statement;
	
		SELECT group_concat(wek) FROM temp_intelligence_CMAB INTO @distinct_channel;
		select @distinct_channel;
		
		select (length(@distinct_channel)-length(replace(@distinct_channel,',','')))+1 into @num_of_channel;
		set @loop_var=1;
		set @Query_Measure=concat("CASE ");
        select @loop_var,@num_of_channel,@distinct_channel;
		WHILE @loop_var<=@num_of_channel
		do
			SET @selected_channel=substring_index(substring_index(concat(@distinct_channel,',-1'),',',@loop_var),',',-1);
			set @Query_Measure=concat(@Query_Measure," WHEN YEARWEEK(EE_Date,5) = ",@selected_channel," Then 'W",@loop_var,"' 
			");
			set @json_select=concat(@json_select,',"W',@loop_var,'",IFNULL(`W',@loop_var,'`,0)');
			set @loop_var=@loop_var+1;
		end while;
		set @Query_Measure=concat(@Query_Measure," else EE_Date END AS 'w'");
		select @Query_Measure;
        SET @temp_view_stmt = concat('
        SELECT 
		YEARWEEK(EE_Date,5) AS dt,',@Query_Measure,'
		FROM
		CLM_Event_Dashboard dash
		WHERE
		',@filter_cond,'
        GROUP BY YEARWEEK(EE_Date,5)
		ORDER BY EE_Date');
    END IF;
    
    IF IN_AGGREGATION_1 = '1Y'
    THEN
		select date_format(DATE_SUB(IN_FILTER_CURRENTMONTH, INTERVAL 11 MONTH),"%Y%m") into @start_date;
		select @start_date;
		select date_format(IN_FILTER_CURRENTMONTH,"%Y%m") into @end_date;
		select @end_date;
		set @filter_cond = CONCAT('YM BETWEEN "',@start_date,'" and "',@end_date,'"');
		set @date_select_query = concat('date_format(EE_DATE,"%b-%Y")');
        SET @temp_view_stmt = concat('
        select date_format(EE_DATE,"%b-%Y") AS dt  
        from CLM_Event_Dashboard
        where ',@filter_cond,'
        GROUP BY 1
		ORDER BY EE_Date');
    END IF;
    
    IF IN_AGGREGATION_1 = '2Y'
    THEN
		select date_format(DATE_SUB(IN_FILTER_CURRENTMONTH, INTERVAL 23 MONTH),"%Y%m") into @start_date;
		select @start_date;
		select date_format(IN_FILTER_CURRENTMONTH,"%Y%m") into @end_date;
		select @end_date;
		set @filter_cond = CONCAT('YM BETWEEN "',@start_date,'" and "',@end_date,'"');
        set @date_select_query = concat('date_format(EE_DATE,"%b-%Y")');
        SET @temp_view_stmt = concat('
        select date_format(EE_DATE,"%b-%Y") AS dt  
        from CLM_Event_Dashboard
        where ',@filter_cond,'
        GROUP BY 1
		ORDER BY EE_Date');
    END IF;
    
    
    select @filter_cond;
	select @temp_view_stmt;
    
    
    IF IN_MEASURE ='TABLE' AND (IN_AGGREGATION_1 = '13W' OR IN_AGGREGATION_1 = '26W')
    THEN
	SET @temp_view = concat('
        create or replace View temp_intelligence_CMAB as
        
        SELECT 
		CAST(YEARWEEK(EE_Date) AS UNSIGNED) AS wek
		FROM
		CLM_Event_Dashboard dash
		WHERE
		',@filter_cond,'
        GROUP BY YEARWEEK(EE_Date)
		ORDER BY EE_Date');
	
    select @temp_view;
	PREPARE statement from @temp_view;
	Execute statement;
	Deallocate PREPARE statement;
	
    SELECT group_concat(wek) FROM temp_intelligence_CMAB INTO @distinct_channel;
    select @distinct_channel;
    
    select (length(@distinct_channel)-length(replace(@distinct_channel,',','')))+1 into @num_of_channel;
	set @loop_var=1;
	set @Query_Measure='';
	set @json_select='';
	select @loop_var,@num_of_channel,@distinct_channel;
	WHILE @loop_var<=@num_of_channel
	do
		SET @selected_channel=substring_index(substring_index(concat(@distinct_channel,',-1'),',',@loop_var),',',-1);
		set @Query_Measure=concat(@Query_Measure,",MAX(CASE WHEN `wek` = ",@selected_channel," Then Conversions else '0.00%' END) AS 'W",@loop_var,"'
        ");
		set @json_select=concat(@json_select,',"W',@loop_var,'",IFNULL(`W',@loop_var,'`,0)');
		set @loop_var=@loop_var+1;
	end while;
    select @Query_Measure, @json_select;
    set @view_stmt = concat(
    '
    CREATE OR REPLACE VIEW S_dashboard_Intelligence_CMAB_Table AS 
SELECT
"Perf_Based" AS "ATTRIBUTE"
',@Query_Measure,'
from 
(
SELECT 
    CAST(YEARWEEK(EE_Date,5) as unsigned) as wek,
    CONCAT(ROUND(IFNULL(((SUM(Target_Bills) / SUM(Target_Base)) * 100),
                    0),
            2),"%") AS Conversions
FROM
    CLM_Event_Dashboard dash, Event_Master em
WHERE
	em.Id = dash.Event_Id and
	em.Waterfall like "%CMAB%" and
    ',@filter_cond,'
GROUP BY YEARWEEK(EE_Date,5)
ORDER BY EE_Date
)C

UNION

select "Non_Perf_Based" AS "ATTRIBUTE"
',@Query_Measure,'
from 
(
SELECT 
    CAST(YEARWEEK(EE_Date) as unsigned) as wek,
    CONCAT(ROUND(IFNULL(((SUM(Target_Bills) / SUM(Target_Base)) * 100),
                    0),
            2),"%") AS Conversions
FROM
    CLM_Event_Dashboard dash, Event_Master em
WHERE
em.Id = dash.Event_Id and
em.Waterfall not like "%CMAB%" and
',@filter_cond,'
GROUP BY YEARWEEK(EE_Date)
ORDER BY EE_Date
)C
;
'
);
set @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("ATTRIBUTE",`ATTRIBUTE`',@json_select,') )),"]")into @temp_result from S_dashboard_Intelligence_CMAB_Table;');
    END IF;
    
    
	IF IN_MEASURE ='TABLE' AND (IN_AGGREGATION_1 = '1Y' OR IN_AGGREGATION_1 = '2Y')
    THEN
	SET @temp_view = concat('
        create or replace View temp_intelligence_CMAB as
        
        select date_format(EE_DATE,"%b-%Y") AS "Year"  
        from CLM_Event_Dashboard
        where ',@filter_cond,'
        GROUP BY 1
		ORDER BY EE_Date');
	
    select @temp_view;
	PREPARE statement from @temp_view;
	Execute statement;
	Deallocate PREPARE statement;
	
    SELECT group_concat(`Year`) FROM temp_intelligence_CMAB INTO @distinct_channel;
    select @distinct_channel;
    
    select (length(@distinct_channel)-length(replace(@distinct_channel,',','')))+1 into @num_of_channel;
	set @loop_var=1;
	set @Query_Measure='';
	set @json_select='';
	select @loop_var,@num_of_channel,@distinct_channel;
	WHILE @loop_var<=@num_of_channel
	do
		SET @selected_channel=substring_index(substring_index(concat(@distinct_channel,',-1'),',',@loop_var),',',-1);
		set @Query_Measure=concat(@Query_Measure,",MAX(CASE WHEN `Year` = '",@selected_channel,"' Then Conversions else '0.00%' END) AS '",@selected_channel,"'
        ");
		set @json_select=concat(@json_select,',"',@selected_channel,'",CASE WHEN `',@selected_channel,'` is null then "0.00%" else `',@selected_channel,'` END');
		set @loop_var=@loop_var+1;
	end while;
    select @Query_Measure, @json_select;
    set @view_stmt = concat(
    '
    CREATE OR REPLACE VIEW S_dashboard_Intelligence_CMAB_Table AS 
SELECT
"Perf_Based" AS "ATTRIBUTE"
',@Query_Measure,'
from 
(
SELECT 
    date_format(EE_DATE,"%b-%Y") AS "Year",
    CONCAT(ROUND(IFNULL(((SUM(Target_Bills) / SUM(Target_Base)) * 100),
                    0),
            2),"%") AS Conversions
FROM
    CLM_Event_Dashboard dash, Event_Master em
WHERE
	em.Id = dash.Event_Id and
	em.Waterfall like "%CMAB%" and
    ',@filter_cond,'
GROUP BY 1
ORDER BY EE_Date
)C

UNION

select "Non_Perf_Based" AS "ATTRIBUTE"
',@Query_Measure,'
from 
(
SELECT 
    date_format(EE_DATE,"%b-%Y") AS "Year",
    CONCAT(ROUND(IFNULL(((SUM(Target_Bills) / SUM(Target_Base)) * 100),
                    0),
            2),"%") AS Conversions
FROM
    CLM_Event_Dashboard dash, Event_Master em
WHERE
	em.Id = dash.Event_Id and
	em.Waterfall not like "%CMAB%" and
    ',@filter_cond,'
GROUP BY 1
ORDER BY EE_Date
)C
;
'
);
set @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("ATTRIBUTE",`ATTRIBUTE`',@json_select,') )),"]")into @temp_result from S_dashboard_Intelligence_CMAB_Table;');
    END IF;
    
    
    IF IN_MEASURE ='GRAPH' AND (IN_AGGREGATION_1 = '13W' OR IN_AGGREGATION_1 = '26W')
    THEN
		
		set @view_stmt = CONCAT(' CREATE OR REPLACE VIEW dashboard_Intelligence_CMAB_Graph AS
        with 
        GROUP1 AS 
        (
			SELECT 
				',@date_select_query,'  AS "dt",
				ROUND(IFNULL(((SUM(Target_Bills) / SUM(Target_Base)) * 100),
								0),
						2) AS PERF_Conversions
			FROM
				CLM_Event_Dashboard dash, Event_Master em
			WHERE
				em.Id = dash.Event_Id and
				em.Waterfall like "%CMAB%" and
				',@filter_cond,'
			GROUP BY 1
			ORDER BY EE_Date
        )
        ,
        GROUP2 AS
        (
			SELECT 
				',@date_select_query,' AS "dt",
				ROUND(IFNULL(((SUM(Target_Bills) / SUM(Target_Base)) * 100),
								0),
						2) AS NON_PERF_Conversions
			FROM
				CLM_Event_Dashboard dash, Event_Master em
			WHERE
				em.Id = dash.Event_Id and
				em.Waterfall not like "%CMAB%" and
				',@filter_cond,'
			GROUP BY 1
			ORDER BY EE_Date
        ),
        GROUP3 AS
        (
			',@temp_view_stmt,'	
        )
        SELECT C.dt AS dt,C.w,PERF_Conversions,NON_PERF_Conversions
        FROM GROUP3 C
		LEFT OUTER JOIN GROUP2 B ON B.dt=C.dt
		LEFT OUTER JOIN GROUP1 A ON A.dt=C.dt 
        UNION
        SELECT C.dt,C.w,PERF_Conversions,NON_PERF_Conversions
        FROM GROUP3 C
		RIGHT OUTER JOIN GROUP2 B ON B.dt=C.dt
		RIGHT OUTER JOIN GROUP1 A ON A.dt=C.dt
        ');
        
        select @view_stmt;
		PREPARE statement from @view_stmt;
		Execute statement;
		Deallocate PREPARE statement;
        
        Select CONCAT((GROUP_CONCAT(json_object("name","Perf. Based","values",PriceJson) ))) 
				From dashboard_Intelligence_CMAB_Graph A,(Select B.dt, CONCAT('[',(GROUP_CONCAT(json_object("date",`w`,"price",CASE WHEN `PERF_Conversions` is null THEN "0.00" else `PERF_Conversions` end ) )),']') as PriceJson
				From dashboard_Intelligence_CMAB_Graph B)json1
                where A.dt = json1.dt into @temp_json1;
          
        Select CONCAT((GROUP_CONCAT(json_object("name","Non Perf. Based","values",PriceJson) ))) 
				From dashboard_Intelligence_CMAB_Graph A,(Select B.dt, CONCAT('[',(GROUP_CONCAT(json_object("date",`w`,"price",CASE WHEN `NON_PERF_Conversions` is null THEN "0.00" else `NON_PERF_Conversions` end ) )),']') as PriceJson
				From dashboard_Intelligence_CMAB_Graph B)json1
                where A.dt = json1.dt into @temp_json2;
          
		 SET @temp_result = concat('[',@temp_json1,',',@temp_json2,']');  
         SET @temp_result = Replace(@temp_result,'\\',"");
         SET @temp_result = Replace(@temp_result,'"[',"["); 
         SET @temp_result = Replace(@temp_result,']"',"]"); 
        
        select @temp_result;
		/*set @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object(
        "X_Data",`w`,
        "value1",CASE WHEN `PERF_Conversions` is null THEN "0.00%" else `PERF_Conversions` end ,
        "value2",CASE WHEN `NON_PERF_Conversions` is null THEN "0.00%" else `NON_PERF_Conversions` end 
        ) )),"]")into @temp_result from S_dashboard_Intelligence_CMAB_Graph;') ; */
    END IF;
    
    IF IN_MEASURE ='GRAPH' AND (IN_AGGREGATION_1 = '1Y' OR IN_AGGREGATION_1 = '2Y')
    THEN
		set @view_stmt = CONCAT(' CREATE OR REPLACE VIEW S_dashboard_Intelligence_CMAB_Graph AS
        with 
        GROUP1 AS 
        (
			SELECT 
				',@date_select_query,'  AS "dt",
				ROUND(IFNULL(((SUM(Target_Bills) / SUM(Target_Base)) * 100),
								0),
						2) AS PERF_Conversions
			FROM
				CLM_Event_Dashboard dash, Event_Master em
			WHERE
				em.Id = dash.Event_Id and
				em.Waterfall like "%CMAB%" and
				',@filter_cond,'
			GROUP BY 1
			ORDER BY EE_Date
        )
        ,
        GROUP2 AS
        (
			SELECT 
				',@date_select_query,' AS "dt",
				ROUND(IFNULL(((SUM(Target_Bills) / SUM(Target_Base)) * 100),
								0),
						2) AS NON_PERF_Conversions
			FROM
				CLM_Event_Dashboard dash, Event_Master em
			WHERE
				em.Id = dash.Event_Id and
				em.Waterfall not like "%CMAB%" and
				',@filter_cond,'
			GROUP BY 1
			ORDER BY EE_Date
        ),
        GROUP3 AS
        (
			',@temp_view_stmt,'	
        )
        SELECT C.dt AS dt,PERF_Conversions,NON_PERF_Conversions
        FROM GROUP3 C
		LEFT OUTER JOIN GROUP2 B ON B.dt=C.dt
		LEFT OUTER JOIN GROUP1 A ON A.dt=C.dt 
        UNION
        SELECT C.dt,PERF_Conversions,NON_PERF_Conversions
        FROM GROUP3 C
		RIGHT OUTER JOIN GROUP2 B ON B.dt=C.dt
		RIGHT OUTER JOIN GROUP1 A ON A.dt=C.dt
        ');
        
        select @view_stmt;
		PREPARE statement from @view_stmt;
		Execute statement;
		Deallocate PREPARE statement;
        
		Select CONCAT((GROUP_CONCAT(json_object("name","Perf. Based","values",PriceJson) ))) 
				From S_dashboard_Intelligence_CMAB_Graph A,(Select B.dt, CONCAT('[',(GROUP_CONCAT(json_object("date",`dt`,"price",CASE WHEN `PERF_Conversions` is null THEN "0.00" else `PERF_Conversions` end ) )),']') as PriceJson
				From S_dashboard_Intelligence_CMAB_Graph B)json1
                where A.dt = json1.dt into @temp_json1;
          
        Select CONCAT((GROUP_CONCAT(json_object("name","Non Perf. Based","values",PriceJson) ))) 
				From S_dashboard_Intelligence_CMAB_Graph A,(Select B.dt, CONCAT('[',(GROUP_CONCAT(json_object("date",`dt`,"price",CASE WHEN `NON_PERF_Conversions` is null THEN "0.00" else `NON_PERF_Conversions` end ) )),']') as PriceJson
				From S_dashboard_Intelligence_CMAB_Graph B)json1
                where A.dt = json1.dt into @temp_json2;  
          
		SET @temp_result = concat('[',@temp_json1,',',@temp_json2,']');  
        SET @temp_result = Replace(@temp_result,"\\","");
        SET @temp_result = Replace(@temp_result,'"[',"["); 
        SET @temp_result = Replace(@temp_result,']"',"]"); 
        select @temp_result;
		/*set @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object(
        "X_Data",`dt`,
        "value1",CASE WHEN `PERF_Conversions` is null THEN "0.00%" else `PERF_Conversions` end ,
        "value2",CASE WHEN `NON_PERF_Conversions` is null THEN "0.00%" else `NON_PERF_Conversions` end
        ) )),"]")into @temp_result from S_dashboard_Intelligence_CMAB_Graph;') ; */
    END IF;
    
	
	
	
	IF IN_MEASURE <> "DOWNLOAD"
    THEN
	select @view_stmt;
	PREPARE statement from @view_stmt;
	Execute statement;
	Deallocate PREPARE statement;
    
    IF IN_MEASURE = "TABLE"
    THEN
	   select @Query_String;
	   PREPARE statement from @Query_String;
	   Execute statement;
	   Deallocate PREPARE statement; 
    END IF;   
    
    IF @temp_result is null
    then 
		set @temp_result = "NO DATA AVAILABLE";
	end if;
    
    SET out_result_json = @temp_result;
    END IF;
    
END$$
DELIMITER ;
