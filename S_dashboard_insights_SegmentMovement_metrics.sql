DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_insights_SegmentMovement_metrics`(IN vBatchSize bigint(10), in vStartCntInput bigint(10),IN vAHF int(2), IN vAP_Month int(2),IN vOTB_Month decimal(4,2), IN vLapser_Month int(2))
BEGIN

DECLARE vCheckEndOfCursor int Default 0;
DECLARE curALL_MONTHS CURSOR FOR 
SELECT DISTINCT 
	Bill_Year_Month
FROM
    CDM_Bill_Details
where Customer_Id BETWEEN @vStart_Cnt AND @vEnd_Cnt
and Bill_Year_Month >= vYYYYMM_StartMonth 
ORDER BY Bill_Year_Month ASC;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET vCheckEndOfCursor=1;

select  vBatchSize ,  vStartCntInput , vAHF ,  vAP_Month , vOTB_Month ,  vLapser_Month ;

INSERT INTO Dashboard_Metrics_Log
(
`Screen_Name`,
`Status`
)
VALUES
('INSIGHTS - SEGMENT MOVEMENT',
'Started');


SET SQL_SAFE_UPDATES=0;
DELETE FROM log_solus 
WHERE
    module = 'S_Dashboard_Insight_SegmentMovement_metrics';
insert into log_solus(module, rec_start,rec_end,msg) values ('S_Dashboard_Insight_SegmentMovement_metrics',-1,-1,'INITIALIZE');
SELECT 'Version No : 10-Feb' AS '';
SELECT 'select * from log_solus where module=\'S_Dashboard_Insight_SegmentMovement_metrics\' order by id desc;' AS '';

DROP TABLE IF EXISTS CDM_Bill_Details_SM_Temp;
CREATE OR REPLACE TABLE `CDM_Bill_Details_SM_Temp` (
  `Customer_Id` bigint(20) NOT NULL,
  `Bill_Header_Id` bigint(20) DEFAULT NULL,
  `YYYYMM` int(6) DEFAULT NULL,
  `Bill_Date` date DEFAULT NULL,
  `Sale_Net_Val` bigint(20) DEFAULT NULL,
  `Created_Date` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  KEY `Bill_Date` (`Bill_Date`),
  KEY `YYYYMM` (`YYYYMM`),
  KEY `Customer_Id` (`Customer_Id`),
  KEY `Created_Date` (`Created_Date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS  Dashboard_Insights_SM_SVOC_TEMP;
CREATE OR REPLACE TABLE `Dashboard_Insights_SM_SVOC_TEMP` (
  `Customer_Id` bigint(20) NOT NULL,
  `SEGMENT` varchar(16) DEFAULT NULL,
  `RECENCY` int(5) DEFAULT NULL,
  `LT_REV` bigint(20) DEFAULT 0,
  `LT_NUM_OF_VISITS` int(4) DEFAULT 0,
  `AP_NUM_OF_VISITS` int(4) DEFAULT 0,
  `Created_Date` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  KEY `RECENCY` (`RECENCY`),
  KEY `SEGMENT` (`SEGMENT`),
  KEY `LT_REV` (`LT_REV`),
  KEY `LT_NUM_OF_VISITS` (`LT_NUM_OF_VISITS`),
  KEY `AP_NUM_OF_VISITS` (`AP_NUM_OF_VISITS`),
  KEY `Customer_Id` (`Customer_Id`),
  KEY `Created_Date` (`Created_Date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE OR REPLACE TABLE `Dashboard_SM_CLMSegments_Temp` (
  `CLMSegmentName` varchar(16) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
INSERT  INTO Dashboard_SM_CLMSegments_Temp (CLMSegmentName)
VALUES
('LEADS'),
('FTR'),
('ALF'),
('AHF'),
('OTB'),
('LAPSER'),
('DEEP LAPSED')
;

TRUNCATE Dashboard_Insights_SegmentMovement_Summary;
INSERT IGNORE INTO Dashboard_Insights_SegmentMovement_Summary 
( 
AGGREGATION, 
ThisPeriodSegment, 
LastPeriodSegment, 
Base,
RevChange 
)
SELECT 
    'TMLM', T1.CLMSegmentName, T2.CLMSegmentName, 0, 0
FROM
    (SELECT DISTINCT
        CLMSegmentName
    FROM
        Dashboard_SM_CLMSegments_Temp) T1,
    (SELECT DISTINCT
        CLMSegmentName
    FROM
        Dashboard_SM_CLMSegments_Temp) T2;

INSERT IGNORE INTO Dashboard_Insights_SegmentMovement_Summary 
( 
AGGREGATION, 
ThisPeriodSegment, 
LastPeriodSegment, 
Base, 
RevChange
)
SELECT 
    'TQLQ', T1.CLMSegmentName, T2.CLMSegmentName, 0, 0
FROM
    (SELECT DISTINCT
        CLMSegmentName
    FROM
        Dashboard_SM_CLMSegments_Temp) T1,
    (SELECT DISTINCT
        CLMSegmentName
    FROM
        Dashboard_SM_CLMSegments_Temp) T2;
        
INSERT IGNORE INTO Dashboard_Insights_SegmentMovement_Summary 
( 
AGGREGATION, 
ThisPeriodSegment, 
LastPeriodSegment, 
Base,
RevChange 
)
SELECT 
    'TYLY', T1.CLMSegmentName, T2.CLMSegmentName, 0, 0
FROM
    (SELECT DISTINCT
        CLMSegmentName
    FROM
        Dashboard_SM_CLMSegments_Temp) T1,
    (SELECT DISTINCT
        CLMSegmentName
    FROM
        Dashboard_SM_CLMSegments_Temp) T2;


/****************************
* Calculate Variables
*****************************/
SET @vAHF=vAHF;
SET @vAPDays=vAP_Month*30; 
SET @vOTBDays=vOTB_Month*30;
SET @vLapserDays=vLapser_Month*30; 
SELECT 
    @vAHF,
	@vAPDays,
    @vOTBDays,
	@vLapserDays,
    vAP_Month,
    vOTB_Month,
	vLapser_Month;

/***************
* This Month, Last Month, Last Quarter, Last Year 
*******************/
/* This Month */
SET @vTMYYYYMM=(select date_format(current_date(),'%Y%m'));
SET @vTMMM=substr(@vTMYYYYMM,5,2);
SET @vTMYYYY=substr(@vTMYYYYMM,1,4);
/* Last Month */
SET @vLMYYYYMM=F_Month_Diff(@vTMYYYYMM,1);
/* Last Qtr */
SET @NoOfMonth=(CASE WHEN @vTMMM%3=0 THEN 3 ELSE @vTMMM%3 END);
SET @vLQYYYYMM=F_Month_Diff(@vTMYYYYMM,@NoOfMonth);
/* Last Year */
-- SET @vLYYYYYMM=concat(@vTMYYYY-1,12);
SET @vLYYYYYMM=(select date_format(date_sub(current_date(),INTERVAL 12 MONTH),'%Y%m'));
SELECT @vTMYYYYMM, @vLMYYYYMM, @vLQYYYYMM,@vLYYYYYMM;
   
/* AP Months */
SET @vTMAP_YYYYMM=F_Month_Diff(@vTMYYYYMM,vAP_Month);
SET @vLMAP_YYYYMM=F_Month_Diff(@vLMYYYYMM,vAP_Month);
SET @vLQAP_YYYYMM=F_Month_Diff(@vLQYYYYMM,vAP_Month);
SET @vLYAP_YYYYMM=F_Month_Diff(@vLYYYYYMM,vAP_Month);
 
SELECT
    @vTMAP_YYYYMM,
    @vLMAP_YYYYMM,
    @vLQAP_YYYYMM,
    @vLYAP_YYYYMM;

/****************************
*******************************
* Start Processing in a Batch
*******************************
*****************************/
SELECT 
    Customer_Id
INTO @Rec_Cnt FROM
    CDM_Customer_Master
ORDER BY Customer_Id DESC
LIMIT 1;
SET @vStart_Cnt=0; 
LOOP_ALL_CUSTOMERS: LOOP
	SET @vEnd_Cnt = @vStart_Cnt + vBatchSize;
	SELECT @vStart_Cnt, @vEnd_Cnt, CURRENT_TIMESTAMP();
    IF @vStart_Cnt  >= @Rec_Cnt THEN
		LEAVE LOOP_ALL_CUSTOMERS;
	END IF;  
INSERT INTO log_solus(module, rec_start,rec_end,msg) values ('S_Dashboard_Insight_SegmentMovement_metrics',@vStart_Cnt,@vEnd_Cnt,'Loading Bills');

TRUNCATE CDM_Bill_Details_SM_Temp;
INSERT INTO `CDM_Bill_Details_SM_Temp`
(
Customer_Id,
Bill_Header_Id,
Bill_Date,
YYYYMM,
Sale_Net_Val
)
SELECT
	Customer_Id,
    Bill_Header_Id,
    Bill_Date,
	date_format(Bill_Date, '%Y%m') as YYYYMM,
	Sale_Net_Val
FROM CDM_Bill_Details 
WHERE Customer_Id between @vStart_Cnt and @vEnd_Cnt;

INSERT INTO `CDM_Bill_Details_SM_Temp`
(
Customer_Id,
Bill_Header_Id,
Bill_Date,
YYYYMM,
Sale_Net_Val
)
SELECT
	Customer_Id,
    -1,
    '9999-99-99',
	date_format(Created_Date, '%Y%m') as YYYYMM,
	0
FROM 
	CDM_Customer_Master
WHERE Customer_Id between @vStart_Cnt and @vEnd_Cnt
AND NOT EXISTS (select customer_id from CDM_Bill_Details_SM_Temp where CDM_Customer_Master.Customer_Id = CDM_Bill_Details_SM_Temp.Customer_Id);

TRUNCATE Dashboard_Insights_SegmentMovement_SVOC;
INSERT IGNORE INTO Dashboard_Insights_SegmentMovement_SVOC(Customer_Id)
SELECT Customer_Id 
FROM CDM_Bill_Details_SM_Temp 
GROUP BY  Customer_Id;

select count(1) from Dashboard_Insights_SegmentMovement_SVOC;
select count(1) from Dashboard_Insights_SM_SVOC_TEMP;
/**********************
*Load LY
**************************/

insert into log_solus(module, rec_start,rec_end,msg) values ('S_Dashboard_Insight_SegmentMovement_metrics',@vStart_Cnt,@vEnd_Cnt,'LY');

/* 	'LY' */
TRUNCATE Dashboard_Insights_SM_SVOC_TEMP;
INSERT IGNORE INTO Dashboard_Insights_SM_SVOC_TEMP(
Customer_Id,
RECENCY,
LT_NUM_OF_VISITS,
LT_REV
)
SELECT 
   Customer_Id,
   case 
		when MAX(Bill_Date) = '9999-99-99' then -1  
		else datediff(concat(@vLYYYYYMM,'01'), Max(Bill_Date)) 
   end as RECENCY,
   case 
		when MAX(Bill_Date) = '9999-99-99' then 0  
		else count(distinct Bill_Header_Id) 				
   end as LT_NUM_OF_VISITS,
   sum(Sale_Net_Val) 							as LT_REV
FROM 
	CDM_Bill_Details_SM_Temp
WHERE YYYYMM <=  @vLYYYYYMM
GROUP BY Customer_Id ;

UPDATE Dashboard_Insights_SM_SVOC_TEMP dashboard,
    (SELECT 
        'LY',
            Customer_Id,
               case 
					when Max(Bill_Date) = '9999-99-99' then 0  
					else count(distinct Bill_Header_Id) 				
				end as AP_NUM_OF_VISITS
       FROM
        CDM_Bill_Details_SM_Temp
    WHERE
        YYYYMM BETWEEN @vLYAP_YYYYMM AND @vLYYYYYMM
    GROUP BY Customer_Id) temp 
SET 
    dashboard.AP_NUM_OF_VISITS = temp.AP_NUM_OF_VISITS
WHERE
    dashboard.Customer_Id = temp.Customer_Id;

UPDATE Dashboard_Insights_SegmentMovement_SVOC dashboard,
    (SELECT 
        Customer_Id,
            AP_NUM_OF_VISITS,
            LT_NUM_OF_VISITS,
            LT_REV,
            RECENCY,
            CASE
                WHEN LT_NUM_OF_VISITS = 0 THEN 'LEADS'
                WHEN LT_NUM_OF_VISITS = 1 and RECENCY < @vAPDays THEN 'FTR'
                WHEN
                    RECENCY < @vAPDays
                        AND LT_NUM_OF_VISITS > 1 AND  AP_NUM_OF_VISITS < @vAHF
                THEN
                    'ALF'
                WHEN
                    RECENCY < @vAPDays
                        AND AP_NUM_OF_VISITS > @vAHF
                THEN
                    'AHF'
                WHEN RECENCY BETWEEN @vAPDays AND @vOTBDays THEN 'OTB'
                WHEN RECENCY BETWEEN @vOTBDays AND @vLapserDays THEN 'LAPSER'
                WHEN RECENCY > @vLapserDays THEN 'DEEP LAPSED'
                ELSE 'NA'
            END AS SEGMENT
    FROM
        Dashboard_Insights_SM_SVOC_TEMP) temp 
SET 
    dashboard.LY_SEGMENT = temp.SEGMENT,
    dashboard.LY_AP_NUM_OF_VISITS = temp.AP_NUM_OF_VISITS,
    dashboard.LY_LT_NUM_OF_VISITS = temp.LT_NUM_OF_VISITS,
    dashboard.LY_RECENCY = temp.RECENCY,
    dashboard.LY_LT_REV = temp.LT_REV
WHERE
    dashboard.Customer_Id BETWEEN @vStart_Cnt AND @vEnd_Cnt
        AND dashboard.Customer_Id = temp.Customer_Id; 


/**********************
*Load LQ
**************************/
insert into log_solus(module, rec_start,rec_end,msg) values ('S_Dashboard_Insight_SegmentMovement_metrics',@vStart_Cnt,@vEnd_Cnt,'LQ');
TRUNCATE Dashboard_Insights_SM_SVOC_TEMP;
INSERT IGNORE INTO Dashboard_Insights_SM_SVOC_TEMP(
Customer_Id,
RECENCY,
LT_NUM_OF_VISITS,
LT_REV
)
SELECT 
   Customer_Id,
   case 
		when MAX(Bill_Date) = '9999-99-99' then -1  
		else datediff(concat(@vLQYYYYMM,'01'), Max(Bill_Date)) 
   end as RECENCY,
   case 
		when MAX(Bill_Date) = '9999-99-99' then 0  
		else count(distinct Bill_Header_Id) 				
   end as LT_NUM_OF_VISITS,
   sum(Sale_Net_Val) 							as LT_REV
FROM 
	CDM_Bill_Details_SM_Temp
WHERE YYYYMM <=  @vLQYYYYMM
GROUP BY Customer_Id ;

UPDATE Dashboard_Insights_SM_SVOC_TEMP dashboard,
( 
SELECT 
	'LQ',
   Customer_Id,
    case 
		when MAX(Bill_Date) = '9999-99-99' then 0  
		else count(distinct Bill_Header_Id) 				
	end as AP_NUM_OF_VISITS
FROM 
	CDM_Bill_Details_SM_Temp
WHERE YYYYMM between @vLQAP_YYYYMM and  @vLQYYYYMM
GROUP BY Customer_Id 
) temp
SET 
	dashboard.AP_NUM_OF_VISITS	=temp.AP_NUM_OF_VISITS
WHERE dashboard.Customer_Id=temp.Customer_Id;

UPDATE Dashboard_Insights_SegmentMovement_SVOC dashboard,
    (SELECT 
        Customer_Id,
            AP_NUM_OF_VISITS,
            LT_NUM_OF_VISITS,
            LT_REV,
            RECENCY,
            CASE
                WHEN LT_NUM_OF_VISITS = 0 THEN 'LEADS'
                WHEN LT_NUM_OF_VISITS = 1 and RECENCY < @vAPDays THEN 'FTR'
                WHEN
                    RECENCY < @vAPDays
                        AND LT_NUM_OF_VISITS > 1 AND  AP_NUM_OF_VISITS < @vAHF
                THEN
                    'ALF'
                WHEN
                    RECENCY < @vAPDays
                        AND AP_NUM_OF_VISITS > @vAHF
                THEN
                    'AHF'
                WHEN RECENCY BETWEEN @vAPDays AND @vOTBDays THEN 'OTB'
                WHEN RECENCY BETWEEN @vOTBDays AND @vLapserDays THEN 'LAPSER'
                WHEN RECENCY > @vLapserDays THEN 'DEEP LAPSED'
            END AS SEGMENT
    FROM
        Dashboard_Insights_SM_SVOC_TEMP) temp 
SET 
    dashboard.LQ_SEGMENT = temp.SEGMENT,
    dashboard.LQ_AP_NUM_OF_VISITS = temp.AP_NUM_OF_VISITS,
    dashboard.LQ_LT_NUM_OF_VISITS = temp.LT_NUM_OF_VISITS,
    dashboard.LQ_RECENCY = temp.RECENCY,
    dashboard.LQ_LT_REV = temp.LT_REV
WHERE
    dashboard.Customer_Id BETWEEN @vStart_Cnt AND @vEnd_Cnt
        AND dashboard.Customer_Id = temp.Customer_Id; 


/**********************
*Load LM
**************************/
insert into log_solus(module, rec_start,rec_end,msg) values ('S_Dashboard_Insight_SegmentMovement_metrics',@vStart_Cnt,@vEnd_Cnt,'LM');
TRUNCATE Dashboard_Insights_SM_SVOC_TEMP;
INSERT IGNORE INTO Dashboard_Insights_SM_SVOC_TEMP(
Customer_Id,
RECENCY,
LT_NUM_OF_VISITS,
LT_REV
)

SELECT 
   Customer_Id,
   case 
		when MAX(Bill_Date) = '9999-99-99' then -1  
		else datediff(concat(@vLMYYYYMM,'01'), Max(Bill_Date)) 
   end as RECENCY,
   case 
		when MAX(Bill_Date) = '9999-99-99' then 0  
		else count(distinct Bill_Header_Id) 				
   end as LT_NUM_OF_VISITS,
   sum(Sale_Net_Val) 							as LT_REV
FROM 
	CDM_Bill_Details_SM_Temp
WHERE YYYYMM <=  @vLMYYYYMM
GROUP BY Customer_Id ;


UPDATE Dashboard_Insights_SM_SVOC_TEMP dashboard,
( 
SELECT 
	'LM',
   Customer_Id,
       case 
		when MAX(Bill_Date) = '9999-99-99' then 0  
		else count(distinct Bill_Header_Id) 				
	end as AP_NUM_OF_VISITS
FROM 
	CDM_Bill_Details_SM_Temp
WHERE YYYYMM between @vLMAP_YYYYMM and @vLMYYYYMM
GROUP BY Customer_Id 
) temp
SET 
	dashboard.AP_NUM_OF_VISITS	=temp.AP_NUM_OF_VISITS
WHERE dashboard.Customer_Id=temp.Customer_Id;

UPDATE Dashboard_Insights_SegmentMovement_SVOC dashboard,
    (SELECT 
        Customer_Id,
            AP_NUM_OF_VISITS,
            LT_NUM_OF_VISITS,
            LT_REV,
            RECENCY,
            CASE
                WHEN LT_NUM_OF_VISITS = 0 THEN 'LEADS'
                WHEN LT_NUM_OF_VISITS = 1 and RECENCY < @vAPDays THEN 'FTR'
                WHEN
                    RECENCY < @vAPDays
                        AND LT_NUM_OF_VISITS > 1 AND  AP_NUM_OF_VISITS < @vAHF
                THEN
                    'ALF'
                WHEN
                    RECENCY < @vAPDays
                        AND AP_NUM_OF_VISITS > @vAHF
                THEN
                    'AHF'
                WHEN RECENCY BETWEEN @vAPDays AND @vOTBDays THEN 'OTB'
                WHEN RECENCY BETWEEN @vOTBDays AND @vLapserDays THEN 'LAPSER'
                WHEN RECENCY > @vLapserDays THEN 'DEEP LAPSED'
            END AS SEGMENT
    FROM
        Dashboard_Insights_SM_SVOC_TEMP) temp 
SET 
    dashboard.LM_SEGMENT = temp.SEGMENT,
    dashboard.LM_AP_NUM_OF_VISITS = temp.AP_NUM_OF_VISITS,
    dashboard.LM_LT_NUM_OF_VISITS = temp.LT_NUM_OF_VISITS,
    dashboard.LM_RECENCY = temp.RECENCY,
    dashboard.LM_LT_REV = temp.LT_REV
WHERE
    dashboard.Customer_Id BETWEEN @vStart_Cnt AND @vEnd_Cnt
        AND dashboard.Customer_Id = temp.Customer_Id; 

/**********************
*Load TM
**************************/
insert into log_solus(module, rec_start,rec_end,msg) values ('S_Dashboard_Insight_SegmentMovement_metrics',@vStart_Cnt,@vEnd_Cnt,'TM');
TRUNCATE Dashboard_Insights_SM_SVOC_TEMP;
INSERT IGNORE INTO Dashboard_Insights_SM_SVOC_TEMP(
Customer_Id,
RECENCY,
LT_NUM_OF_VISITS,
LT_REV
)
SELECT 
   Customer_Id,
   case 
		when MAX(Bill_Date) = '9999-99-99' then -1  
		else datediff(current_date(), Max(Bill_Date)) 
   end as RECENCY,
   case 
		when MAX(Bill_Date) = '9999-99-99' then 0  
		else count(distinct Bill_Header_Id) 				
   end as LT_NUM_OF_VISITS,
   sum(Sale_Net_Val) 							as LT_REV
FROM 
	CDM_Bill_Details_SM_Temp
WHERE YYYYMM <=  @vTMYYYYMM
GROUP BY Customer_Id ;

UPDATE Dashboard_Insights_SM_SVOC_TEMP dashboard,
( 
SELECT 
	'TM',
   Customer_Id,
       case 
		when MAX(Bill_Date) = '9999-99-99' then 0  
		else count(distinct Bill_Header_Id) 				
	end as AP_NUM_OF_VISITS
FROM 
	CDM_Bill_Details_SM_Temp
WHERE YYYYMM between @vTMAP_YYYYMM and @vTMYYYYMM
GROUP BY Customer_Id 
) temp
SET 
	dashboard.AP_NUM_OF_VISITS	=temp.AP_NUM_OF_VISITS
WHERE dashboard.Customer_Id=temp.Customer_Id;
UPDATE Dashboard_Insights_SegmentMovement_SVOC dashboard,
    (SELECT 
        Customer_Id,
            AP_NUM_OF_VISITS,
            LT_NUM_OF_VISITS,
            LT_REV,
            RECENCY,
            CASE
                WHEN LT_NUM_OF_VISITS = 0 THEN 'LEADS'
                WHEN LT_NUM_OF_VISITS = 1 and RECENCY < @vAPDays THEN 'FTR'
                WHEN
                    RECENCY < @vAPDays
                        AND LT_NUM_OF_VISITS > 1 AND  AP_NUM_OF_VISITS < @vAHF
                THEN
                    'ALF'
                WHEN
                    RECENCY < @vAPDays
                        AND AP_NUM_OF_VISITS > @vAHF
                THEN
                    'AHF'
                WHEN RECENCY BETWEEN @vAPDays AND @vOTBDays THEN 'OTB'
                WHEN RECENCY BETWEEN @vOTBDays AND @vLapserDays THEN 'LAPSER'
                WHEN RECENCY > @vLapserDays THEN 'DEEP LAPSED'
            END AS SEGMENT
    FROM
        Dashboard_Insights_SM_SVOC_TEMP) temp 
SET 
    dashboard.TM_SEGMENT = temp.SEGMENT,
    dashboard.TM_AP_NUM_OF_VISITS = temp.AP_NUM_OF_VISITS,
    dashboard.TM_LT_NUM_OF_VISITS = temp.LT_NUM_OF_VISITS,
    dashboard.TM_RECENCY = temp.RECENCY,
    dashboard.TM_LT_REV = temp.LT_REV
WHERE
    dashboard.Customer_Id BETWEEN @vStart_Cnt AND @vEnd_Cnt
        AND dashboard.Customer_Id = temp.Customer_Id;

/************************************
*
* Calcuate SEGMENT MOVEMENT
*
*****************************************/



UPDATE Dashboard_Insights_SegmentMovement_SVOC
SET 
    LM_RevChange = TM_LT_REV - LM_LT_REV,
    LQ_RevChange = TM_LT_REV - LQ_LT_REV,
    LY_RevChange = TM_LT_REV - LY_LT_REV;


select * from Dashboard_Insights_SegmentMovement_SVOC;
SET SQL_SAFE_UPDATES=0;
UPDATE Dashboard_Insights_SegmentMovement_Summary dashboard,
    (SELECT 
			'TMLM' AS AGGREGATION,
            TM_SEGMENT AS ThisPeriodSegment,
            LM_Segment AS LastPeriodSegment,
            COUNT(1) AS Base,
            SUM(LM_RevChange) as RevChange -- set revChange
    FROM
        Dashboard_Insights_SegmentMovement_SVOC
    GROUP BY TM_SEGMENT , LM_Segment) temp 
SET 
    dashboard.Base = dashboard.Base + temp.Base,
    dashboard.RevChange = temp.RevChange
WHERE
			dashboard.AGGREGATION = temp.AGGREGATION
        AND dashboard.ThisPeriodSegment = temp.ThisPeriodSegment
        AND dashboard.LastPeriodSegment = temp.LastPeriodSegment
        ;


UPDATE Dashboard_Insights_SegmentMovement_Summary dashboard,
    (SELECT 
			'TQLQ' AS AGGREGATION,
            TM_SEGMENT AS ThisPeriodSegment,
            LQ_Segment AS LastPeriodSegment,
            COUNT(1) AS Base,
            SUM(LQ_RevChange) as RevChange
    FROM
        Dashboard_Insights_SegmentMovement_SVOC
    GROUP BY TM_SEGMENT , LQ_Segment) temp 
SET 
    dashboard.Base = dashboard.Base + temp.Base,
    dashboard.RevChange = temp.RevChange
WHERE
			dashboard.AGGREGATION = temp.AGGREGATION
        AND dashboard.ThisPeriodSegment = temp.ThisPeriodSegment
        AND dashboard.LastPeriodSegment = temp.LastPeriodSegment;
    
UPDATE Dashboard_Insights_SegmentMovement_Summary dashboard,
    (SELECT 
        'TYLY' AS AGGREGATION,
            TM_SEGMENT AS ThisPeriodSegment,
            LY_Segment AS LastPeriodSegment,
            COUNT(1) AS Base,
            SUM(LY_RevChange) as RevChange
    FROM
        Dashboard_Insights_SegmentMovement_SVOC
    GROUP BY TM_SEGMENT , LY_Segment) temp 
SET 
    dashboard.Base = dashboard.Base + temp.Base,
    dashboard.RevChange = temp.RevChange
WHERE
			dashboard.AGGREGATION = temp.AGGREGATION
        AND dashboard.ThisPeriodSegment = temp.ThisPeriodSegment
        AND dashboard.LastPeriodSegment = temp.LastPeriodSegment;

SET @vStart_Cnt = @vEnd_Cnt;
END LOOP LOOP_ALL_CUSTOMERS; 
  


/************************************
*
* CALCULATE Segment Movement PERCENT
*
*****************************************/
SET SQL_SAFE_UPDATES=0;
UPDATE Dashboard_Insights_SegmentMovement_Summary dashboard,
    (SELECT 
        t2.AGGREGATION,
            t2.ThisPeriodSegment,
            t2.LastPeriodSegment,
            t2.totalBaseCELL AS Base,
            (t2.totalBaseCELL / t1.totalBaseRow) * 100 AS Percent
    FROM
        (SELECT 
        AGGREGATION, LastPeriodSegment, SUM(base) AS totalBaseRow
    FROM
        Dashboard_Insights_SegmentMovement_Summary
    GROUP BY AGGREGATION , LastPeriodSegment) t1, (SELECT 
        AGGREGATION,
            ThisPeriodSegment,
            LastPeriodSegment,
            SUM(base) AS totalBaseCELL
    FROM
        Dashboard_Insights_SegmentMovement_Summary
    GROUP BY AGGREGATION , ThisPeriodSegment , LastPeriodSegment) t2
    WHERE
        t1.AGGREGATION = t2.AGGREGATION
            AND t1.LastPeriodSegment = t2.LastPeriodSegment
    GROUP BY t2.AGGREGATION , t2.ThisPeriodSegment , t2.LastPeriodSegment) t6 
SET 
    dashboard.Percent = t6.Percent
WHERE
    dashboard.AGGREGATION = t6.AGGREGATION
        AND dashboard.ThisPeriodSegment = t6.ThisPeriodSegment
        AND dashboard.LastPeriodSegment = t6.LastPeriodSegment;
        
UPDATE Dashboard_Metrics_Log
SET `Status` = 'Ended' WHERE Screen_Name = 'INSIGHTS - SEGMENT MOVEMENT';

END$$
DELIMITER ;
