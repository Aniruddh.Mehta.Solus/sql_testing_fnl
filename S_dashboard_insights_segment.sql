DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_insights_segment`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    
    
    Select IFNULL(max(IF(Config_Name="CALENDAR",Value1,NULL)),max(IF(Config_Name="FISCAL",Value1,NULL))) into @FY_START_DATE
    from UI_Configuration_Global where isActive="true";
    
    Select IFNULL(max(IF(Config_Name="CALENDAR",Value2,NULL)),max(IF(Config_Name="FISCAL",Value2,NULL))) into @FY_END_DATE
    from UI_Configuration_Global where isActive="true";
    
    
	
    
    IF IN_AGGREGATION_1 = 'BY_DAY' OR IN_FILTER_MONTHDURATION = 1
		THEN 
			SET @filter_cond = concat(' YYYYMM =',IN_FILTER_CURRENTMONTH);
			SET @date_select = ' date_format(YYYYMMDD,"%d-%b") AS "Year" ';
	ELSEIF IN_AGGREGATION_1 = 'BY_MONTH' or IN_FILTER_MONTHDURATION = 12
		THEN 
			SET @filter_cond=concat(" YYYYMM between '",@FY_SATRT_DATE,"' and '",@FY_END_DATE,"'");
			SET @date_select=' date_format(concat(YYYYMM,"01"),"%b-%Y") AS "Year" ';
	END IF;
    
    IF IN_MEASURE = 'LAST12M'
    THEN
		SET @view_stmt = concat("CREATE OR REPLACE VIEW V_Dashboard_Segments_Table as
								select Dashboard_Insights_Base.CLMSegmentName,format(SUM(Base),',') as 'Customers',
								round((SUM(Base)/(select SUM(Base) from Dashboard_Insights_Base where AGGREGATION = 'BY_MONTH' and YYYYMM = ",IN_FILTER_CURRENTMONTH,"))*100,2) as '%Base',
								round((SUM(T12MthActiveRevenue)/(select SUM(T12MthActiveRevenue) from Dashboard_Insights_Base where AGGREGATION = 'BY_MONTH' and YYYYMM = ",IN_FILTER_CURRENTMONTH,"))*100,2) as '%Rev',
								round(SUM(T12MthActiveBills)/SUM(T12MthActive),2) as 'Frequency',
								format(round(SUM(T12MthActiveRevenue)/SUM(T12MthActiveBills),0),',') as 'ABV',
								format(round(SUM(T12MthActiveRevenue)/SUM(T12MthActive),0),',') as 'Val/Cust'
								from Dashboard_Insights_Base join CDM_Customer_Segment_Sequence b on Dashboard_Insights_Base.CLMSegmentName=b.CLMSegmentName 
                                where AGGREGATION = 'BY_MONTH' and Dashboard_Insights_Base.CLMSegmentName<> 'NA' AND Dashboard_Insights_Base.CLMSegmentName<> 'All'
								and Dashboard_Insights_Base.YYYYMM = ",IN_FILTER_CURRENTMONTH,"
								group by Dashboard_Insights_Base.CLMSegmentName
                                having SUM(T12MthActive)>0
                                order by b.sequence ASC;
								");
		SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Segments",`CLMSegmentName`,"#Cust",CAST(`Customers` AS CHAR),"% Base",`%Base`,"% Rev",`%Rev`,"Freq/Cust",`Frequency`,"ABV",`ABV`,"Val/Cust",`Val/Cust`) )),"]")into @temp_result from V_Dashboard_Segments_Table;' ;
        -- SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Segments",`CLMSegmentName`,"Cust",`Customers`,"Base",`%Base`,"Rev",`%Rev`,"Freq_Cust",`Frequency`,"ABV",`ABV`,"Val_Cust",`Val/Cust`,"Discount_Cust",`Discount/Cust`) )),"]")into @temp_result from V_Dashboard_Segments_Table;') ;
        
	ELSEIF IN_MEASURE = 'FYYTD'
    THEN
		SET @view_stmt = concat("CREATE OR REPLACE VIEW V_Dashboard_Segments_Table as
								select Dashboard_Insights_Base.CLMSegmentName,format(SUM(Base),',') as 'Customers',
								round((SUM(Base)/(select SUM(Base) from Dashboard_Insights_Base where AGGREGATION = 'BY_MONTH' and YYYYMM = ",IN_FILTER_CURRENTMONTH,"))*100,2) as '%Base',
								round((SUM(FYBaseRevenue)/(select SUM(FYBaseRevenue) from Dashboard_Insights_Base where AGGREGATION = 'BY_MONTH' and YYYYMM = ",IN_FILTER_CURRENTMONTH,"))*100,2) as '%Rev',
								round(SUM(FYBaseBills)/SUM(FYBase),2) as 'Frequency',
								format(round(SUM(FYBaseRevenue)/SUM(FYBaseBills),0),',') as 'ABV',
								format(round(SUM(FYBaseRevenue)/SUM(FYBase),0),',') as 'Val/Cust'
								from Dashboard_Insights_Base join CDM_Customer_Segment_Sequence b on Dashboard_Insights_Base.CLMSegmentName=b.CLMSegmentName 
                                where AGGREGATION = 'BY_MONTH' AND Dashboard_Insights_Base.CLMSegmentName<> 'NA' AND Dashboard_Insights_Base.CLMSegmentName<> 'All'
								and Dashboard_Insights_Base.YYYYMM = ",IN_FILTER_CURRENTMONTH,"
								group by Dashboard_Insights_Base.CLMSegmentName
                                having SUM(FYBase)>0
                                order by b.sequence ASC;
								");
		SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Segments",`CLMSegmentName`,"#Cust",CAST(`Customers` AS CHAR),"% Base",`%Base`,"% Rev",`%Rev`,"Freq/Cust",`Frequency`,"ABV",`ABV`,"Val/Cust",`Val/Cust`) )),"]")into @temp_result from V_Dashboard_Segments_Table;' ;
		
	ELSE    
		/*SET @view_stmt = concat("CREATE OR REPLACE VIEW V_Dashboard_Segments_Table as
								select Dashboard_Insights_Base.CLMSegmentName,SUM(FYBase) as 'Customers',
								round((SUM(FYBase)/(select SUM(FYBase) from Dashboard_Insights_Base where AGGREGATION = 'BY_MONTH' and YYYYMM = ",IN_FILTER_CURRENTMONTH,"))*100,2) as '%Base',
								round((SUM(MonthlyActiveRevenue)/(select SUM(MonthlyActiveRevenue) from Dashboard_Insights_Base where AGGREGATION = 'BY_MONTH' and YYYYMM = ",IN_FILTER_CURRENTMONTH,"))*100,2) as '%Rev',
								round(SUM(FYBaseVisits)/SUM(FYBase),2) as 'Frequency',
								round(SUM(FYBaseRevenue)/SUM(FYBaseVisits),2) as 'ABV',
								round(SUM(FYBaseRevenue)/SUM(FYBase),2) as 'Val/Cust',
								round(SUM(FYBaseDiscount)/SUM(FYBase),2) as 'Discount/Cust'
								from Dashboard_Insights_Base
                                where AGGREGATION = 'BY_MONTH' AND CLMSegmentName<> 'NA' AND CLMSegmentName<> 'All'
								and Dashboard_Insights_Base.YYYYMM = ",IN_FILTER_CURRENTMONTH,"
								group by CLMSegmentName
                                having SUM(FYBase)>0;
								");*/
		SET @view_stmt = concat("CREATE OR REPLACE VIEW V_Dashboard_Segments_Table as
								select Dashboard_Insights_Base.CLMSegmentName,SUM(T12MthActive) as 'Customers',
								round((SUM(Base)/(select SUM(Base) from Dashboard_Insights_Base where AGGREGATION = 'BY_MONTH' and YYYYMM = ",IN_FILTER_CURRENTMONTH,"))*100,2) as '%Base',
								round((SUM(MonthlyActiveRevenue)/(select SUM(MonthlyActiveRevenue) from Dashboard_Insights_Base where AGGREGATION = 'BY_MONTH' and YYYYMM = ",IN_FILTER_CURRENTMONTH,"))*100,2) as '%Rev',
								round(SUM(T12MthActiveBills)/SUM(T12MthActive),2) as 'Frequency',
								format(round(SUM(T12MthActiveRevenue)/SUM(T12MthActiveBills),0),',') as 'ABV',
								format(round(SUM(T12MthActiveRevenue)/SUM(T12MthActive),0),',') as 'Val/Cust'
								from Dashboard_Insights_Base join CDM_Customer_Segment_Sequence b on Dashboard_Insights_Base.CLMSegmentName=b.CLMSegmentName 
                                where AGGREGATION = 'BY_MONTH' and Dashboard_Insights_Base.CLMSegmentName<> 'NA' AND Dashboard_Insights_Base.CLMSegmentName<> 'All'
								and Dashboard_Insights_Base.YYYYMM = ",IN_FILTER_CURRENTMONTH,"
								group by Dashboard_Insights_Base.CLMSegmentName
                                having SUM(T12MthActive)>0
                                order by b.sequence ASC;
								");
     
		SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("displayName",CAST(`CLMSegmentName` AS CHAR),"value1",`Frequency`,"value2",`Customers`) )),"]")into @temp_result from V_Dashboard_Segments_Table ;' ;
    END IF;
   
	IF IN_MEASURE <> "DOWNLOAD"
    THEN
	select @view_stmt;
	PREPARE statement from @view_stmt;
	Execute statement;
	Deallocate PREPARE statement;

	select @Query_String;
	PREPARE statement from @Query_String;
	Execute statement;
	Deallocate PREPARE statement; 
    SET out_result_json = @temp_result;
    END IF;
    
	
	/*
	IF IN_SCREEN <> "DOWNLOAD"
    THEN
		select @Query_String;
		PREPARE statement from @Query_String;
		Execute statement;
		Deallocate PREPARE statement; 
		SET out_result_json = @temp_result;
	ELSE
		call S_dashboard_performance_download('V_Dashboard_Segments_Table',@out_result_json1);
		select @out_result_json1 into out_result_json;
    END IF;
	*/
END$$
DELIMITER ;
