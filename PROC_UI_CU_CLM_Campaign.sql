
CREATE or replace PROCEDURE `PROC_UI_CU_CLM_Campaign`(
	IN vUserId BIGINT,
	IN vCampaignName VARCHAR(128),
	IN vCampaignDesc VARCHAR(1024),
	IN vCampaignThemeId BIGINT,
	IN vCLMSegmentId BIGINT,
	IN vCampaignCGPercentage DECIMAL(5,2),
	IN vCampaignResponseDays BIGINT,
	IN vCampaignState TINYINT,
	IN vCampaignSmartPtyState TINYINT,
	IN vCampaignIsOnDemand TINYINT,
	IN vCampaignUsesWaterfall TINYINT,
	IN vCampaignIsPerpetual TINYINT,
	IN vCampaignStartDate DATE,
	IN vCampaignEndDate DATE,
	IN vExecution_Sequence BIGINT,
	INOUT vCampaignId BIGINT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN
	DECLARE vResult TINYINT;
	DECLARE vInputValues TEXT;
	
	SET vSuccess = 1;
	SET vErrMsg = '';

	SET vInputValues  = CONCAT(
			'UserId: ', IFNULL(vUserId,'NULL VALUE')
			,' | CampaignName: ',IFNULL(vCampaignName,'NULL VALUE')
			,' | CampaignDesc: ',IFNULL(vCampaignDesc,'NULL VALUE')
			,' | CampaignThemeId: ',IFNULL(vCampaignThemeId,'NULL VALUE')
			,' | CLMSegmentId: ',IFNULL(vCLMSegmentId,'NULL VALUE')
			,' | CampaignCGPercentage: ',IFNULL(vCampaignCGPercentage,'NULL VALUE')
			,' | CampaignResponseDays: ',IFNULL(vCampaignResponseDays,'NULL VALUE')
			,' | CampaignState: ',IFNULL(vCampaignState,'NULL VALUE')
			,' | CampaignSmartPtyState: ',IFNULL(vCampaignSmartPtyState,'NULL VALUE')
			,' | CampaignIsOnDemand: ',IFNULL(vCampaignIsOnDemand,'NULL VALUE')
			,' | CampaignUsesWaterfall: ',IFNULL(vCampaignUsesWaterfall,'NULL VALUE')
			,' | CampaignIsPerpetual: ',IFNULL(vCampaignIsPerpetual,'NULL VALUE')
			,' | CampaignStartDate: ',IFNULL(vCampaignStartDate,'NULL VALUE')
			,' | CampaignEndDate: ',IFNULL(vCampaignEndDate,'NULL VALUE')
			,' | Execution_Sequence: ',IFNULL(vExecution_Sequence,'NULL VALUE')
		);

	IF vUserId IS NULL OR vUserId <= 0 THEN
		SET vErrMsg = CONCAT('User Id cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vCampaignThemeId IS NULL OR vCampaignThemeId <= 0 THEN
		SET vErrMsg = CONCAT('CampaignThemeId is not valid.');
		SET vSuccess = 0;
	END IF;

	IF vCLMSegmentId IS NULL OR vCLMSegmentId <= 0 THEN
		SET vErrMsg = CONCAT('Life Cycle Segment is not valid.');
		SET vSuccess = 0;
	END IF;

	IF vExecution_Sequence IS NULL OR vExecution_Sequence <= 0 THEN
		SELECT MAX(Execution_Sequence) INTO vExecution_Sequence FROM CLM_Campaign;
	END IF;
    
    IF vCampaignIsPerpetual =1
    THEN
		SET vCampaignStartDate = '0000-00-00';
		SET vCampaignEndDate = '9999-12-31';
	END IF;

	IF vSuccess = 1 THEN 
	CALL PROC_CLM_CU_CLM_Campaign(
			vUserId,
			vCampaignName,
		IFNULL(vCampaignDesc, ''),
			vCampaignThemeId,
			vCLMSegmentId,
		IFNULL(vCampaignCGPercentage, 0),
		IFNULL(vCampaignResponseDays ,0),
		IFNULL(vCampaignState, 0),
		IFNULL(vCampaignSmartPtyState, 0),
		IFNULL(vCampaignIsOnDemand, 0),
			IFNULL(vCampaignUsesWaterfall, 1),
			IFNULL(vCampaignIsPerpetual, 1),
			IFNULL(vCampaignStartDate, CURRENT_DATE()),
			IFNULL(vCampaignEndDate, '9999-12-31'),
			vExecution_Sequence,
		vCampaignId,
		vErrMsg,
		vSuccess
	);
	END IF;
	IF vSuccess = 0 THEN 
		SET vErrMsg = CONCAT(vErrMsg , '\r\n', vInputValues);	
	END IF;

END

