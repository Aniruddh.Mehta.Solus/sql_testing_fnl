DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_CLM_CRM_Dashboard`()
BEGIN
IF date_format(Current_Date(),'%d')='01' or date_format(Current_Date(),'%d') = 15
THEN
	TRUNCATE CLM_CRM_Dashboard;
	INSERT INTO CLM_CRM_Dashboard(MonthYear,`Month`,`Year`,New_Customers,New_Customer_Revenue)
	(select DATE_FORMAT(Bill_Date,'%Y%m'),MONTHNAME(Bill_Date),YEAR(BILL_DATE),count(DISTINCT Customer_Id) AS New_Customers, SUM(Sale_Net_Val) As New_Customer_Revenue from CDM_Bill_Details a
	where Bill_Date>date_sub(CURRENT_DATE(),INTERVAL 356 DAY)
	AND
	Customer_Id not in (SELECT DISTINCT Customer_Id from CDM_Bill_Details c where DATE_FORMAT(c.Bill_Date,'%Y%m') <  DATE_FORMAT(a.Bill_Date,'%Y%m'))
	group by MONTH(Bill_Date),YEAR(Bill_Date));

	UPDATE CLM_CRM_Dashboard a,(select DATE_FORMAT(Bill_Date,'%Y%m') as MonthYear,MONTHNAME(Bill_Date),YEAR(BILL_DATE),count(DISTINCT Customer_Id) AS Existing_Customers, SUM(Sale_Net_Val) As Existing_Customer_Revenue from CDM_Bill_Details a
	where Bill_Date>date_sub(CURRENT_DATE(),INTERVAL 356 DAY)
	AND
	Customer_Id in (SELECT DISTINCT Customer_Id from CDM_Bill_Details c where DATE_FORMAT(c.Bill_Date,'%Y%m') <  DATE_FORMAT(a.Bill_Date,'%Y%m'))
	group by MONTH(Bill_Date),YEAR(Bill_Date)) b
	SET a.Existing_Customers=b.Existing_Customers,
	a.Existing_Customer_Revenue=b.Existing_Customer_Revenue
	WHERE a.MonthYear=b.MonthYear;
END IF;
end$$
DELIMITER ;