DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_UI_Dashboard_Banding_Configs`(IN in_request_json json, OUT out_result_json json)
BEGIN
    

    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
    declare EDIT Bigint; 

	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare IN_AGGREGATION_4 text;
	declare IN_AGGREGATION_5 text;
    declare IN_NAME text;
    declare IN_LOWER_LIMIT INT(20);
    declare IN_UPPER_LIMIT INT(20);
    declare IN_VALUE_NUM INT(20);
    declare IN_GOAL text;
    declare IN_VALUE1 INT(20);
    declare IN_VALUE2 INT(20);
	
	SET SQL_SAFE_UPDATES=0;
	SET foreign_key_checks=0;

	Set in_request_json = Replace(in_request_json,'\n','\\n');
  
   
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1")); 
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_AGGREGATION_4 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_4"));
    SET IN_NAME = json_unquote(json_extract(in_request_json,"$.UPDATED_BAND_NAME"));
    SET IN_LOWER_LIMIT = json_unquote(json_extract(in_request_json,"$.UPDATED_BAND_LOWER_LIMIT"));
    SET IN_UPPER_LIMIT = json_unquote(json_extract(in_request_json,"$.UPDATED_BAND_UPPER_LIMIT"));
    SET IN_VALUE_NUM = json_unquote(json_extract(in_request_json,"$.VALUE_NUM"));
    SET IN_GOAL = json_unquote(json_extract(in_request_json,"$.GOAL"));
    SET IN_VALUE1 = json_unquote(json_extract(in_request_json,"$.VALUE1"));
    SET IN_VALUE2 = json_unquote(json_extract(in_request_json,"$.VALUE2"));
    
    
    
	Set @temp_result =Null;
    Set @vSuccess ='';
	Set @vErrMsg='';
	Set @vSuccess='';
	Set @Err =0;
	set @a1 = '[{"Val":"Values"}]';
    
    IF EXISTS (select COLUMN_NAME from information_schema.columns
							where TABLE_NAME = "Dashboard_Insights_Customer_Distribution" AND COLUMN_NAME = "Id" )
	THEN 
		SET @A = 1;
	ELSE
		alter table Dashboard_Insights_Customer_Distribution
		add column Id int(10) primary key auto_increment; 
	END IF;
    IF IN_AGGREGATION_1 ="DISPLAY_BASE"
    THEN
		Set @View_STM= Concat("CREATE or REPLACE VIEW `V_Dashboard_base_config` AS 
		SELECT Config_Name,Value1 
		FROM `UI_Configuration_Global` 
        WHERE Config_Name in ('OTBMonthTimesAPMonth','ActiveHighFrequency','LapserMonthTimesAPMonth','FISCAL_STARTMONTH')
		;");
			
		SELECT @View_STM;
		PREPARE statement from @View_STM;
		Execute statement;
		Deallocate PREPARE statement; 
			
		Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object(
		"Name",`Config_Name`,
		"Value1",`Value1`
		) )),"]")into @temp_result from V_Dashboard_base_config ;') ;
											  
		SELECT @Query_String;
		PREPARE statement from @Query_String;
		Execute statement;
		Deallocate PREPARE statement;
    END IF;
    IF IN_AGGREGATION_1 ="EDIT_BASE"
    THEN
		UPDATE UI_Configuration_Global
        SET Value1 = IN_VALUE_NUM
        where Config_Name = IN_AGGREGATION_2;
        
		Set @View_STM= Concat("CREATE or REPLACE VIEW `V_Dashboard_base_config` AS 
		SELECT Config_Name,Value1 
		FROM `UI_Configuration_Global` 
        WHERE Config_Name in ('OTBMonthTimesAPMonth','ActiveHighFrequency','LapserMonthTimesAPMonth','FISCAL_STARTMONTH')
		;");
			
		SELECT @View_STM;
		PREPARE statement from @View_STM;
		Execute statement;
		Deallocate PREPARE statement; 
			
		Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object(
		"Name",`Config_Name`,
		"Value1",`Value1`
		) )),"]")into @temp_result from V_Dashboard_base_config ;') ;
											  
		SELECT @Query_String;
		PREPARE statement from @Query_String;
		Execute statement;
		Deallocate PREPARE statement;
    END IF;
			   
	IF IN_AGGREGATION_1 ="DISPLAY"
		THEN 
			Set @View_STM= Concat("CREATE or REPLACE VIEW `V_Dashboard_Banding_Config` AS 
         select Display, lower_limit, upper_limit, Id from Dashboard_Insights_Customer_Distribution WHERE Display = 'Frequency'
UNION
select Display, lower_limit, upper_limit, Id from Dashboard_Insights_Customer_Distribution WHERE Display = 'Recency'
UNION
select Display, lower_limit, upper_limit, Id from Dashboard_Insights_Customer_Distribution WHERE Display = 'ABV'
UNION
select Display, lower_limit, upper_limit, Id from Dashboard_Insights_Customer_Distribution WHERE Display = 'Range'
UNION
select Display, lower_limit, upper_limit, Id from Dashboard_Insights_Customer_Distribution WHERE Display = 'Discount%';");
			
			SELECT @View_STM;
			PREPARE statement from @View_STM;
			Execute statement;
			Deallocate PREPARE statement; 
				
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Name",`Display`,"Lower_Limit",`Lower_Limit`,"Upper_Limit",`Upper_Limit`,"Id",`Id`) )),"]")into @temp_result from V_Dashboard_Banding_Config ;') ;
													  
				SELECT @Query_String;
								PREPARE statement from @Query_String;
								Execute statement;
								Deallocate PREPARE statement; 
													  
	END IF;
		
	IF IN_AGGREGATION_1 ="EDIT"
    THEN
		UPDATE Dashboard_Insights_Customer_Distribution
        SET Display = IN_NAME, 
        lower_limit = IN_LOWER_LIMIT,
        upper_limit = IN_UPPER_LIMIT
        WHERE Id = IN_AGGREGATION_2;
        Set @View_STM= Concat("CREATE or REPLACE VIEW `V_Dashboard_Banding_Config` AS 
         select Display, lower_limit, upper_limit, Id from Dashboard_Insights_Customer_Distribution WHERE Display = 'Frequency'
UNION
select Display, lower_limit, upper_limit, Id from Dashboard_Insights_Customer_Distribution WHERE Display = 'Recency'
UNION
select Display, lower_limit, upper_limit, Id from Dashboard_Insights_Customer_Distribution WHERE Display = 'ABV'
UNION
select Display, lower_limit, upper_limit, Id from Dashboard_Insights_Customer_Distribution WHERE Display = 'Range'
UNION
select Display, lower_limit, upper_limit, Id from Dashboard_Insights_Customer_Distribution WHERE Display = 'Discount%';");
			
			SELECT @View_STM;
			PREPARE statement from @View_STM;
			Execute statement;
			Deallocate PREPARE statement; 
				
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Name",`Display`,"Lower_Limit",`Lower_Limit`,"Upper_Limit",`Upper_Limit`,"Id",`Id`) )),"]")into @temp_result from V_Dashboard_Banding_Config ;') ;
													  
				SELECT @Query_String;
								PREPARE statement from @Query_String;
								Execute statement;
								Deallocate PREPARE statement; 
        
    END IF;
    
    IF IN_AGGREGATION_1 ="DISPLAY_GOALS"
		THEN 
			Set @View_STM= Concat("CREATE or REPLACE VIEW `V_Dashboard_Goals_Config` AS 
         select Measure, Goal from Dashboard_CRM_Goals;");
			
			SELECT @View_STM;
			PREPARE statement from @View_STM;
			Execute statement;
			Deallocate PREPARE statement; 
				
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Measure",`Measure`,"Goal",`Goal`) )),"]")into @temp_result from V_Dashboard_Goals_Config ;') ;
													  
				SELECT @Query_String;
								PREPARE statement from @Query_String;
								Execute statement;
								Deallocate PREPARE statement; 
													  
	END IF;
		
	IF IN_AGGREGATION_1 ="EDIT_GOALS"
    THEN
		UPDATE Dashboard_CRM_Goals
        SET Goal = IN_GOAL
        WHERE Measure = IN_AGGREGATION_3;
        Set @View_STM= Concat("CREATE or REPLACE VIEW `V_Dashboard_Goals_Config` AS 
         select Measure, Goal from Dashboard_CRM_Goals;");
			
			SELECT @View_STM;
			PREPARE statement from @View_STM;
			Execute statement;
			Deallocate PREPARE statement; 
				
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Measure",`Measure`,"Goal",`Goal`) )),"]")into @temp_result from V_Dashboard_Goals_Config ;') ;
													  
								SELECT @Query_String;
								PREPARE statement from @Query_String;
								Execute statement;
								Deallocate PREPARE statement; 
        
    END IF;
    
    IF IN_AGGREGATION_1 ="DISPLAY_TRIGGER"
		THEN 
			Set @View_STM= Concat("CREATE or REPLACE VIEW `V_Dashboard_Trigger_Config` AS 
         select Config_Name, Value1, Value2 from UI_Configuration_Global WHERE Config_Name = 'Trigger_Size_XL'
UNION
select Config_Name, Value1, Value2 from UI_Configuration_Global WHERE Config_Name = 'Trigger_Size_L'
UNION
select Config_Name, Value1, Value2 from UI_Configuration_Global WHERE Config_Name = 'Trigger_Size_M'
UNION
select Config_Name, Value1, Value2 from UI_Configuration_Global WHERE Config_Name = 'Trigger_Size_S'
UNION
select Config_Name, Value1, Value2 from UI_Configuration_Global WHERE Config_Name = 'Trigger_Size_XS';");
			
			SELECT @View_STM;
			PREPARE statement from @View_STM;
			Execute statement;
			Deallocate PREPARE statement; 
				
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Name",`Config_Name`,"Value1",`Value1`,"Value2",`Value2`) )),"]")into @temp_result from V_Dashboard_Trigger_Config ;') ;
													  
				SELECT @Query_String;
								PREPARE statement from @Query_String;
								Execute statement;
								Deallocate PREPARE statement; 
													  
	END IF;
		
	IF IN_AGGREGATION_1 ="EDIT_TRIGGER"
    THEN
		UPDATE UI_Configuration_Global
        SET Value1 = IN_VALUE1, 
        Value2 = IN_VALUE2
        WHERE Config_Name = IN_AGGREGATION_3;
        Set @View_STM= Concat("CREATE or REPLACE VIEW `V_Dashboard_Banding_Config` AS 
         select Config_Name, Value1, Value2 from UI_Configuration_Global WHERE Config_Name = 'Trigger_Size_XL'
UNION
select Config_Name, Value1, Value2 from UI_Configuration_Global WHERE Config_Name = 'Trigger_Size_L'
UNION
select Config_Name, Value1, Value2 from UI_Configuration_Global WHERE Config_Name = 'Trigger_Size_M'
UNION
select Config_Name, Value1, Value2 from UI_Configuration_Global WHERE Config_Name = 'Trigger_Size_S'
UNION
select Config_Name, Value1, Value2 from UI_Configuration_Global WHERE Config_Name = 'Trigger_Size_XS';");
			
			SELECT @View_STM;
			PREPARE statement from @View_STM;
			Execute statement;
			Deallocate PREPARE statement; 
				
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Name",`Config_Name`,"Value1",`Value1`,"Value2",`Value2`) )),"]")into @temp_result from V_Dashboard_Trigger_Config ;') ;
													  
				SELECT @Query_String;
								PREPARE statement from @Query_String;
								Execute statement;
								Deallocate PREPARE statement; 
        
    END IF;
							

    SET out_result_json = @temp_result;                                          
  


END$$
DELIMITER ;
