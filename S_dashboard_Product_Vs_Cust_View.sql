DELIMITER $$
CREATE or replace PROCEDURE `S_dashboard_Product_Vs_Cust_View`(IN in_request_json JSON, OUT out_result_json JSON)
BEGIN

    DECLARE IN_USER_NAME VARCHAR(240); 
    DECLARE IN_SCREEN VARCHAR(400); 
    DECLARE temp_result JSON;
    DECLARE IN_FILTER_CURRENTMONTH TEXT; 
    DECLARE IN_FILTER_MONTHDURATION TEXT; 
    DECLARE IN_MEASURE TEXT; 
    DECLARE IN_NUMBER_FORMAT TEXT;
    DECLARE IN_AGGREGATION_1 TEXT; 
    DECLARE IN_AGGREGATION_2 TEXT;
    DECLARE IN_AGGREGATION_3 TEXT;
    DECLARE NUM_PERCENT TEXT;
    DECLARE Query_String TEXT;
    DECLARE IN_FILTER_SEGMENT TEXT;
    DECLARE IN_FILTER_PRODUCT TEXT;
    
    
    
	SET IN_FILTER_CURRENTMONTH = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.FILTER_CURRENTMONTH"));
    SET IN_FILTER_MONTHDURATION = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.FILTER_MONTHDURATION"));
    SET IN_MEASURE = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.MEASURE"));
    SET IN_NUMBER_FORMAT = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.NUMBER_FORMAT"));
    SET IN_AGGREGATION_1 = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.AGGREGATION_1"));
    SET IN_AGGREGATION_2 = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.AGGREGATION_2"));
    SET IN_AGGREGATION_3 = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.AGGREGATION_3"));
   
    SET IN_FILTER_SEGMENT = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.FILTER_SEGMENT"));
    SET IN_FILTER_PRODUCT = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.FILTER_PRODUCT"));
    SET NUM_PERCENT = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.NUM_PERCENT"));
    
    SET IN_SCREEN = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.SCREEN"));
    
    if IN_SCREEN = 'PRODUCT_VIEW' THEN
      if IN_MEASURE = 'DROPDOWN' THEN
         SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') into @temp_result
FROM (
SELECT 
    JSON_OBJECT("Segment",CLMSegmentName) AS json_obj
FROM
    Dashboard_Insights_Products_Customer
WHERE CLMSegmentName NOT LIKE "%Leads%" AND CLMSegmentName NOT LIKE "%NA%"
AND    Time_Period = "Ever"
GROUP BY CLMSegmentName
) AS result');
      END IF;
    END IF;
    
     if IN_MEASURE ='Product_Cust' Then 
     begin
		
     if IN_FILTER_SEGMENT=''  THEN
        BEGIN
			if NUM_PERCENT='PERCENT' THEN
            begin
	SET @Query_String = CONCAT('WITH total AS (
    SELECT SUM(Customer_Count) AS total
    FROM Dashboard_Insights_Products_Customer
    WHERE Time_Period="',IN_AGGREGATION_1,'"
   
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') into @temp_result
FROM (
    SELECT JSON_OBJECT("X_Data", AGGREGATION, "Value", ROUND((SUM(Customer_Count) / total.total) * 100, 2)) AS json_obj
    FROM Dashboard_Insights_Products_Customer, total
    WHERE
        Time_Period="',IN_AGGREGATION_1,'"
        and AGGREGATION<8
        group by AGGREGATION
      
) AS result;');



end;

			
            elseif NUM_PERCENT='NUMBER' Then
            begin
            				SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') into @temp_result
FROM (
    SELECT JSON_OBJECT("X_Data", AGGREGATION, "Value", (SUM(Customer_Count))) AS json_obj
    FROM Dashboard_Insights_Products_Customer
    WHERE
        Time_Period="',IN_AGGREGATION_1,'"
        and AGGREGATION<8
        group by AGGREGATION
      
) AS result;');


end;
            END if;
        END;
        
        elseif IN_FILTER_SEGMENT<>'' Then
        begin
        	if NUM_PERCENT='PERCENT' THEN
            begin
	SET @Query_String = CONCAT('WITH total AS (
    SELECT SUM(Customer_Count) AS total
    FROM Dashboard_Insights_Products_Customer
    WHERE Time_Period="',IN_AGGREGATION_1,'" and CLMSegmentName="',IN_FILTER_SEGMENT,'"
   
)
SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') into @temp_result
FROM (
    SELECT JSON_OBJECT("X_Data", AGGREGATION, "Value", ROUND((SUM(Customer_Count) / total.total) * 100, 2)) AS json_obj
    FROM Dashboard_Insights_Products_Customer, total
    WHERE
        Time_Period="',IN_AGGREGATION_1,'" and
        CLMSegmentName="',IN_FILTER_SEGMENT,'"
        and AGGREGATION<8
        group by AGGREGATION
      
) AS result;');



end;

			
          elseif NUM_PERCENT='NUMBER' Then
            begin
            				SET @Query_String = CONCAT('SELECT CONCAT(\'[\', GROUP_CONCAT(json_obj SEPARATOR \',\'), \']\') into @temp_result
FROM (
    SELECT JSON_OBJECT("X_Data", AGGREGATION, "Value", (SUM(Customer_Count))) AS json_obj
    FROM Dashboard_Insights_Products_Customer
    WHERE
        Time_Period="',IN_AGGREGATION_1,'" and
        CLMSegmentName="',IN_FILTER_SEGMENT,'"
        and AGGREGATION<8
        group by AGGREGATION
      
) AS result;');


end;
            END if;
        end;
        END IF;
        
        
           
        
        
       
        
        
         
        end;
        
        end if;
    
    
    
    
    
    
    
    
    IF IN_SCREEN = 'PRODUCT_VIEW' THEN
    PREPARE stmt FROM @Query_String;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
    SET out_result_json = @temp_result;
END IF;



END$$
DELIMITER ;
