drop procedure if exists S_Rcore_Upload_Story;

delimiter $$
create procedure S_Rcore_Upload_Story (in jsonData json,out result json)
begin
	  declare nameValue text;
	  declare templateValue varchar(50);
    declare paramValue json;
    declare hashValue char(32);
    declare hashCount  int;
    declare storyId int;
    declare recoPField1Input varchar(70);
    declare recoPField1Value varchar(70);
    
    -- initialize result object
    set result = json_object();
    
    -- extract name and params from input 
    set nameValue = json_unquote(json_extract(jsonData,'$.name'));
    set templateValue = json_unquote(json_extract(jsonData,'$.template'));
    set paramValue = json_extract(jsonData,'$.params');
    set storyId = cast(json_extract(jsonData,'$.story_id') as integer);
    set recoPField1Input = json_unquote(json_extract(json_extract(jsonData,'$.params'),'$.addon'));
    
    if recoPField1Input = 'NA' then
      set recoPField1Value='';
    else 
      set recoPField1Value = recoPField1Input;
    end if;
    
    select count(1) into hashCount from RankedPickList_Stories_Master where Name = nameValue or Story_Id = storyId;
    
    if hashCount = 0 then
		  set hashValue = md5(json_unquote(json_extract(paramValue,'$')));
      select count(1) into hashCount from RankedPickList_Stories_Master where Params_Hash = hashValue;
        
      if hashCount = 0 then
        if storyId = 0 then
		      insert into RankedPickList_Stories_Master (Name,Template,RecoPField1,Params,Params_Hash) values (nameValue,templateValue,recoPField1Value,paramValue,hashValue);
		      set result = json_set(result,'$.success',TRUE);
		    else 
		      insert into RankedPickList_Stories_Master (Story_Id,Name,Template,RecoPField1,Params,Params_Hash) values (storyId,nameValue,templateValue,recoPField1Value,paramValue,hashValue);
		      set result = json_set(result,'$.success',TRUE);
		    end if;
      else
			  set result = json_set(result,'$.success',FALSE);
            set result = json_set(result,'$.reason','Story with same params already exists');
      end if;
	else
		set result = json_set(result,'$.success',FALSE);
		set result = json_set(result,'$.reason','Name or Story_Id already present');
  end if;
    
end$$
delimiter ;