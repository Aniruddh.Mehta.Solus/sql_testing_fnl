
CREATE or REPLACE PROCEDURE `PROC_CLM_CU_CLM_Timeslot`(
	IN vTimeslot_Name VARCHAR(128), 
	IN vSend_Time int(4), 
	INOUT vTimeslot_Id BIGINT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN
	DECLARE vRowCnt BIGINT DEFAULT 0;
	DECLARE vSqlstmt TEXT;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN 
		SET vSuccess = 0; 
		GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
		SET vErrMsg = CONCAT(IFNULL(vErrMsg,''), "ERROR ", @errno, " (", @sqlstate, "): ", @text);
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 0, vErrMsg, 0);
	END;

	SET vErrMsg = 'Start';
	SET vSuccess = 0;
	SET @CURR_PROC_NAME = 'PROC_CLM_CU_CLM_Timeslot';
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 1, vErrMsg, vSuccess);

	IF vTimeslot_Name IS NULL OR vTimeslot_Name = '' THEN
		SET vErrMsg = 'Offer Name is a Mandatory Field and cannot be empty or NULL';
		SET vSuccess = 0;
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 2, vErrMsg, vSuccess);
	ELSE
		IF vTimeslot_Id IS NULL OR vTimeslot_Id <= 0 THEN
			SET vSqlstmt = CONCAT(
				'INSERT IGNORE INTO CLM_Timeslot_Master (
					Timeslot_Name, 
					Send_Time
				) VALUES ('
				,'"', vTimeslot_Name,'"'
				,',', vSend_Time, ''
				, ')'
			);

			SET vErrMsg = vSqlstmt;

			SET @sql_stmt = vSqlstmt;
			PREPARE EXEC_STMT FROM @sql_stmt;
			EXECUTE EXEC_STMT;
			DEALLOCATE PREPARE EXEC_STMT;
			CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 3, vErrMsg, vSuccess);
		ELSE
				
			IF vRowCnt = 0 THEN
				SET vSqlstmt = CONCAT(
					'INSERT IGNORE INTO CLM_Timeslot_Master (
						Timeslot_Id,
                        Timeslot_Name,
                        Send_Time
					) VALUES ('
					, vTimeslot_Id
					,', "', vTimeslot_Name,'"'
					,',"', vSend_Time, '"'
					,') ON DUPLICATE KEY UPDATE '
					,' Timeslot_Name = "', vTimeslot_Name, '"'
					,', Send_Time = ', vSend_Time
				);
				
				SET vErrMsg = vSqlstmt;
				CALL CDM_Update_Process_Log('PROC_CLM_CU_CLM_Offer', 5, vErrMsg , vRowCnt );                
				SET @sql_stmt = vSqlstmt;
                select @sql_stmt;
				PREPARE EXEC_STMT FROM @sql_stmt;
				EXECUTE EXEC_STMT;
				DEALLOCATE PREPARE EXEC_STMT;
				CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 4, vErrMsg, vSuccess);
			ELSE
				SET vErrMsg = 'The Timeslot already exists and has been used in Campaign execution and hence cannot be modified.';
				SET vSuccess = 0;
				CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 5, vErrMsg, vSuccess);

			END IF;
		END IF;	
	END IF;
	    
	SELECT Timeslot_Id INTO vTimeslot_Id
	FROM CLM_Timeslot_Master
	WHERE Timeslot_Name = vTimeslot_Name
	LIMIT 1;
 
	SET vSuccess = 1;
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 6, vErrMsg, vSuccess);

END

