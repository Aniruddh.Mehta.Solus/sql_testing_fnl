DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_insights_bouncecurve`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
	insert into log_solus(module,input_json) values ('S_dashboard_insights_Base',in_request_json);
    select id into @id from log_solus order by id desc limit 1;

	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    
    
    Select IFNULL(max(IF(Config_Name="CALENDAR",Value1,NULL)),max(IF(Config_Name="FISCAL",Value1,NULL))) into @FY_SATRT_DATE
    from UI_Configuration_Global where isActive="true";
    
    Select IFNULL(max(IF(Config_Name="CALENDAR",Value2,NULL)),max(IF(Config_Name="FISCAL",Value2,NULL))) into @FY_END_DATE
    from UI_Configuration_Global where isActive="true";
	   
    set session group_concat_max_len = 10048576;
    
    if IN_MEASURE not in ('GLUE','GLUEDISTRIBUTION') AND IN_AGGREGATION_2 = 'ATM'
    THEN
    set @view_stmt= CONCAT(' create or replace view V_Dashboard_BounceCurve as 
							select NoOfCustomerPercent as "basePercent",NoOfDays as "days"
							from Dashboard_Insights_BounceCurve
                            where AGGREGATION = "',IN_MEASURE,'"
                            AND Measure = "ATM"
							');
	
	SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("basePercent",`basePercent`,"days",`days`) )),"]")into @temp_result from V_Dashboard_BounceCurve;' ;
    elseif IN_MEASURE not in ('GLUE','GLUEDISTRIBUTION') AND IN_AGGREGATION_2 = 'L24M'
    THEN
    set @view_stmt= CONCAT(' create or replace view V_Dashboard_BounceCurve as 
							select NoOfCustomerPercent as "basePercent",NoOfDays as "days"
							from Dashboard_Insights_BounceCurve
                            where AGGREGATION = "',IN_MEASURE,'"
                            AND Measure = "L24M"
							');
	
	SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("basePercent",`basePercent`,"days",`days`) )),"]")into @temp_result from V_Dashboard_BounceCurve;' ;
    ELSEIF IN_MEASURE = 'GLUE' AND IN_AGGREGATION_2 = 'L24M'
    THEN
		set @view_stmt= CONCAT(' create or replace view V_Dashboard_BounceCurve as 
									select concat(NoOfDays," to ",NoOfDays+1) as X_Data,round(NoOfCustomerPercent,1) as v from Dashboard_Insights_BounceCurve where AGGREGATION = "GLUE" and Measure = "L24M" AND NoOfDays between 1 and 9 order by NoOfDays; ');
		SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`X_Data`,"Value",`v`) )),"]")into @temp_result from V_Dashboard_BounceCurve;' ;
	ELSEIF IN_MEASURE = 'GLUE' AND IN_AGGREGATION_2 = 'ATM'
    THEN
		set @view_stmt= CONCAT(' create or replace view V_Dashboard_BounceCurve as 
									select concat(NoOfDays," to ",NoOfDays+1) as X_Data,round(NoOfCustomerPercent,1) as v from Dashboard_Insights_BounceCurve where AGGREGATION = "GLUE" and Measure = "ATM" AND NoOfDays between 1 and 9 order by NoOfDays; ');
		SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`X_Data`,"Value",`v`) )),"]")into @temp_result from V_Dashboard_BounceCurve;' ;
	ELSEIF IN_MEASURE = 'GLUEDISTRIBUTION' AND IN_AGGREGATION_2 = 'L24M'
    THEN
		set @view_stmt= CONCAT(' create or replace view V_Dashboard_BounceCurve as 
									select NoOfDays as X_Data,round(NoOfCustomerPercent,1) as v from Dashboard_Insights_BounceCurve where AGGREGATION = "GLUEDISTRIBUTION" and Measure = "L24M" and NoOfDays between 1 and 9 order by NoOfDays; ');
		SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`X_Data`,"Value",`v`) )),"]")into @temp_result from V_Dashboard_BounceCurve;' ;
	ELSEIF IN_MEASURE = 'GLUEDISTRIBUTION' AND IN_AGGREGATION_2 = 'ATM'
    THEN
		set @view_stmt= CONCAT(' create or replace view V_Dashboard_BounceCurve as 
									select NoOfDays as X_Data,round(NoOfCustomerPercent,1) as v from Dashboard_Insights_BounceCurve where AGGREGATION = "GLUEDISTRIBUTION" and Measure = "ATM" and NoOfDays between 1 and 9 order by NoOfDays; ');
		SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`X_Data`,"Value",`v`) )),"]")into @temp_result from V_Dashboard_BounceCurve;' ;
    END IF;
    
    IF IN_AGGREGATION_1 = 'TABLE'
    THEN
    
		SET @Interval_month = CASE WHEN IN_AGGREGATION_2='L24M' THEN 'L24M'
							WHEN IN_AGGREGATION_2='ATM' THEN 'ATM' END;
                            
		CREATE OR REPLACE TABLE Dashboard_BounceCurve_Summary (pct decimal(10,2), days int(4), AGGREGATION varchar(10) ) ;
		SET SQL_SAFE_UPDATES=0;
		DELETE FROM Dashboard_BounceCurve_Summary;
		insert into Dashboard_BounceCurve_Summary (pct,days,AGGREGATION)
		SELECT 
			50 AS pct, NoOfDays AS days, AGGREGATION
		FROM
			Dashboard_Insights_BounceCurve
		WHERE
			AGGREGATION = 'NTN1'
				AND NoOfCustomerPercent >= 50
                AND Measure = @Interval_month
                order by NoOfCustomerPercent asc limit 1;
				
		insert into Dashboard_BounceCurve_Summary (pct,days,AGGREGATION)
		SELECT 
			80 AS pct, NoOfDays AS days, AGGREGATION
		FROM
			Dashboard_Insights_BounceCurve
		WHERE
			AGGREGATION = 'NTN1'
				AND NoOfCustomerPercent >= 80
                AND Measure = @Interval_month
                order by NoOfCustomerPercent asc limit 1;
				
		insert into Dashboard_BounceCurve_Summary (pct,days,AGGREGATION)
		SELECT 
			90 AS pct, NoOfDays AS days, AGGREGATION
		FROM
			Dashboard_Insights_BounceCurve
		WHERE
			AGGREGATION = 'NTN1'
				AND NoOfCustomerPercent >= 90
                AND Measure = @Interval_month
                order by NoOfCustomerPercent asc limit 1;
				
				
		insert into Dashboard_BounceCurve_Summary (pct,days,AGGREGATION)
		SELECT 
			50 AS pct, NoOfDays AS days, AGGREGATION
		FROM
			Dashboard_Insights_BounceCurve
		WHERE
			AGGREGATION = 'NTR'
				AND NoOfCustomerPercent >= 50
                AND Measure = @Interval_month
                order by NoOfCustomerPercent asc limit 1;
				
		insert into Dashboard_BounceCurve_Summary (pct,days,AGGREGATION)
		SELECT 
			80 AS pct, NoOfDays AS days, AGGREGATION
		FROM
			Dashboard_Insights_BounceCurve
		WHERE
			AGGREGATION = 'NTR'
				AND NoOfCustomerPercent >= 80
                AND Measure = @Interval_month
                order by NoOfCustomerPercent asc limit 1;
				
		insert into Dashboard_BounceCurve_Summary (pct,days,AGGREGATION)
		SELECT 
			90 AS pct, NoOfDays AS days, AGGREGATION
		FROM
			Dashboard_Insights_BounceCurve
		WHERE
			AGGREGATION = 'NTR'
				AND NoOfCustomerPercent >= 90
                AND Measure = @Interval_month
                order by NoOfCustomerPercent asc limit 1;
                
                
				
				
		insert into Dashboard_BounceCurve_Summary (pct,days,AGGREGATION)
		SELECT 
			50 AS pct, NoOfDays AS days, AGGREGATION
		FROM
			Dashboard_Insights_BounceCurve
		WHERE
			AGGREGATION = 'GLUE'
				AND NoOfCustomerPercent >= 50
                AND Measure = @Interval_month
                order by NoOfCustomerPercent asc limit 1;
				
		insert into Dashboard_BounceCurve_Summary (pct,days,AGGREGATION)
		SELECT 
			80 AS pct, NoOfDays AS days, AGGREGATION
		FROM
			Dashboard_Insights_BounceCurve
		WHERE
			AGGREGATION = 'GLUE'
				AND NoOfCustomerPercent >= 80
                AND Measure = @Interval_month
                order by NoOfCustomerPercent asc limit 1;
				
		insert into Dashboard_BounceCurve_Summary (pct,days,AGGREGATION)
		SELECT 
			90 AS pct, NoOfDays AS days, AGGREGATION
		FROM
			Dashboard_Insights_BounceCurve
		WHERE
			AGGREGATION = 'GLUE'
				AND NoOfCustomerPercent >= 90
                AND Measure = @Interval_month
                order by NoOfCustomerPercent asc limit 1;
      
      				
		insert into Dashboard_BounceCurve_Summary (pct,days,AGGREGATION)
		SELECT 
			50 AS pct, NoOfDays AS days, 'GLUEDISTRIBUTION'
		FROM
			Dashboard_Insights_BounceCurve
		WHERE
			AGGREGATION = 'GLUE'
				AND NoOfCustomerPercent >= 50
                AND Measure = @Interval_month
                order by NoOfCustomerPercent asc limit 1;
				
		insert into Dashboard_BounceCurve_Summary (pct,days,AGGREGATION)
		SELECT 
			80 AS pct, NoOfDays AS days, 'GLUEDISTRIBUTION'
		FROM
			Dashboard_Insights_BounceCurve
		WHERE
			AGGREGATION = 'GLUE'
				AND NoOfCustomerPercent >= 80
                AND Measure = @Interval_month
                order by NoOfCustomerPercent asc limit 1;
				
		insert into Dashboard_BounceCurve_Summary (pct,days,AGGREGATION)
		SELECT 
			90 AS pct, NoOfDays AS days, 'GLUEDISTRIBUTION'
		FROM
			Dashboard_Insights_BounceCurve
		WHERE
			AGGREGATION = 'GLUE'
				AND NoOfCustomerPercent >= 90
                AND Measure = @Interval_month
                order by NoOfCustomerPercent asc limit 1;
                            
		set @view_stmt= CONCAT(' create or replace view V_Dashboard_BounceCurve1 as 
							select concat(round(pct,0),"%") as pct, days as "days" from Dashboard_BounceCurve_Summary 
                            where AGGREGATION = "',IN_MEASURE,'"');
	
    IF IN_MEASURE like '%GLUE%'
    THEN
		SET @Query_String =  
		'SELECT CONCAT("[",(GROUP_CONCAT(json_object("P(Next Txn)",concat ("> ", `pct`),"#Txn",`days`) )),"]")into @temp_result from V_Dashboard_BounceCurve1 ;';
    ELSE
		SET @Query_String =  
		'SELECT CONCAT("[",(GROUP_CONCAT(json_object("%Base",`pct`,"Days",`days`) )),"]")into @temp_result from V_Dashboard_BounceCurve1 ;';
	END IF;

	END IF;
    
	IF IN_MEASURE <> "DOWNLOAD"
    THEN
	select @view_stmt;
	PREPARE statement from @view_stmt;
	Execute statement;
	Deallocate PREPARE statement;

	select @Query_String;
	PREPARE statement from @Query_String;
	Execute statement;
	Deallocate PREPARE statement; 
    SET out_result_json = @temp_result;
    END IF;
    
    Update log_solus
    set output_json=out_result_json
    where id=@id and module='S_dashboard_insights_Base';
    
END$$
DELIMITER ;
