DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_UI_Campaign_ReSchedule`(IN in_request_json json, OUT out_result_json json)
BEGIN
   

    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare Action_To_Take,Start_Date text;
	-- declare Favourite_Day text;
	declare Event_Id varchar(400); 
    
    
    
    SET Action_To_Take = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	
	SET Event_Id = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));

	SET Start_Date = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));

    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    
    Select Action_To_Take;

	Select Event_Id;
    
	Set Event_Id= replace (Event_Id,'"','');
	Set Event_Id= replace (Event_Id,', ',',');
	
	
	Set @Event_Id=Event_Id;
	Set   @vSuccess ='0';
	SET SQL_SAFE_UPDATES=0;
	SET foreign_key_checks=0;
	SET @temp_result = Null;
	
    

IF Action_To_Take = "SCHEDULEINFO" AND @Event_Id != ''
THEN 
    
    
    SET @Query_String0 = Concat('SELECT Type_Of_Event INTO @Event_Type
				FROM Event_Master_UI
				WHERE Event_Id in (',@Event_Id,');');
				
			SELECT @Query_String0;
			PREPARE statement from @Query_String0;
			Execute statement;
			Deallocate PREPARE statement;	
    
    SELECT @Event_Type;
    
    SET @Query_String0 = Concat('SELECT DATE(Start_Date_Id) INTO @Event_Date
				FROM Event_Master
				WHERE EventId in (',@Event_Id,');');
				
			SELECT @Query_String0;
			PREPARE statement from @Query_String0;
			Execute statement;
			Deallocate PREPARE statement;
    
    SELECT @Event_Date; 
    

    SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Event_Type","',@Event_Type,'","Event_Start_Date","',@Event_Date,'") )),"]")
									    into @temp_result
									     ;') ;
			      
					SELECT @Query_String;
					PREPARE statement from @Query_String;
					Execute statement;
					Deallocate PREPARE statement;
    
END IF;


IF Action_To_Take = "RESCHEDULE" AND @Event_Id != '' AND Start_Date != ''
THEN 
    
    
     SET @Query_String0 = Concat('SELECT Type_Of_Event INTO @Event_Type
				FROM Event_Master_UI
				WHERE Event_Id in (',@Event_Id,');');
				
			SELECT @Query_String0;
			PREPARE statement from @Query_String0;
			Execute statement;
			Deallocate PREPARE statement;	
    
    SELECT @Event_Type;
    
    IF @Event_Type = "GTM"
    
    THEN 
    
		SET @Query_String3 = Concat('Update CLM_Campaign_Trigger A, CLM_Campaign_Events B
								  Set A.TriggerStartDate = "',Start_Date,'",
									  A.TriggerEndDate = "',Start_Date,'"
								  where A.CampTriggerId = B.CampTriggerId and B.EventId in (',@Event_Id,');');
																	
									SELECT @Query_String3;
									PREPARE statement from @Query_String3;
									Execute statement;
									Deallocate PREPARE statement;
                                    
	ELSE
		
        SET @Query_String3 = Concat('Update CLM_Campaign_Trigger A, CLM_Campaign_Events B
								  Set A.TriggerStartDate = "',Start_Date,'"
								  where A.CampTriggerId = B.CampTriggerId and B.EventId in (',@Event_Id,');');
																	
									SELECT @Query_String3;
									PREPARE statement from @Query_String3;
									Execute statement;
									Deallocate PREPARE statement;
                                    
	END IF;
		
                                    
	
                                
	Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","The Event Is Rescheduled") )),"]") into @temp_result ;') ;
   
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement;
                                
	
END IF;


IF @temp_result is null
		
		Then
								
			Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Invalid Parameters") )),"]") into @temp_result ;') ;
   
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement; 
						
	End if;
							
	SET out_result_json = @temp_result;
								
 END$$
DELIMITER ;
