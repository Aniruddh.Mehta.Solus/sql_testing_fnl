DELIMITER $$
CREATE or replace PROCEDURE `PROC_CLM_IS_VALID_SVOBILL_CONDITION`(
	IN vSQLCondition TEXT,
    IN vSVOCSQLCondition TEXT,
    In vSegmentid VARCHAR(128),-- Changed  
    IN vCheckValidityOnly TINYINT,
	OUT vSuccess BIGINT
)
BEGIN
	DECLARE vSqlstmt TEXT;
	DECLARE vErrMsg TEXT;
    
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN 
		SET vSuccess = 0; 
		GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
		SET vErrMsg = CONCAT(IFNULL(vErrMsg,''), "ERROR ", @errno, " (", @sqlstate, "): ", @text);
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 0, vErrMsg, 0);
	END;

	SET vErrMsg = 'Start';
	SET vSuccess = 0;
	SET @CURR_PROC_NAME = 'PROC_CLM_IS_VALID_SVOBILL_CONDITION';
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 1, vErrMsg, vSuccess);
    

			Set @Query_String= concat('Select Concat("(",Replace(group_concat("(",CLMSegmentReplacedQuery,")"),","," or "),")") into @CLMSegmentReplacedQuery 
	from CLM_Segment where CLMSegmentId in (',vSegmentid,') ;
  ;') ;
											   
													SELECT @Query_String;
													PREPARE statement from @Query_String;
													Execute statement;
													Deallocate PREPARE statement; 
				-- Changed  
    select vSegmentid,@CLMSegmentReplacedQuery;
    
    
    IF vSVOCSQLCondition='' or vSVOCSQLCondition is null
		THEN	
			set vSQLCondition= concat(vSQLCondition,' and ', @CLMSegmentReplacedQuery) ;
			SELECT vSQLCondition;
    ELSE
			set vSQLCondition= concat(vSQLCondition,' and Customer_Id IN ( SELECT Customer_Id FROM V_CLM_Customer_One_View     WHERE ',vSVOCSQLCondition ,'and ',@CLMSegmentReplacedQuery,')') ;
			SELECT vSQLCondition;	
    END IF;
    
    
    
	IF vCheckValidityOnly = 1 THEN
		IF (vSQLCondition IS NOT NULL) AND (vSQLCondition <> '') THEN
			SET vSqlstmt = CONCAT('SELECT Customer_Id INTO @Temp FROM V_CLM_Bill_Detail_One_View WHERE ',vSQLCondition, ' LIMIT 1');
			
		ELSE
			SET vSqlstmt = CONCAT('SELECT Customer_Id INTO @Temp FROM V_CLM_Bill_Detail_One_View LIMIT 1');
			
		END IF;

		SET @sql_stmt = vSqlstmt;
		PREPARE stmt FROM @sql_stmt;
		EXECUTE stmt; 
		DEALLOCATE PREPARE stmt;
		SET vSuccess = 1;
	ELSE
		IF (vSQLCondition IS NOT NULL) AND (vSQLCondition <> '') THEN
			SET vSqlstmt = CONCAT('SELECT COUNT(1) INTO  @Cust_Cnt FROM V_CLM_Bill_Detail_One_View WHERE ',vSQLCondition);
			
		ELSE
			SET vSqlstmt = 'SELECT COUNT(1) INTO  @Cust_Cnt FROM V_CLM_Bill_Detail_One_View';
			
        END IF;
		SET @sql_stmt = vSqlstmt;
		PREPARE stmt FROM @sql_stmt;
		EXECUTE stmt; 
		DEALLOCATE PREPARE stmt;
		SET vSuccess = @Cust_Cnt;
	END IF;
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 2, vErrMsg, vSuccess);
END$$
DELIMITER ;
