DELIMITER $$
CREATE or replace PROCEDURE `S_DIY_UserMgmt`(IN in_request_json text,OUT out_result_json json)
BEGIN
DECLARE vActionValidValues VARCHAR(64) DEFAULT ' INSERT, UPDATE, DELETE, LOGIN, SELECT ';
DECLARE ProcName VARCHAR(128) DEFAULT 'S_DIY_UserMgmt';
DECLARE ADMIN_USER VARCHAR(128) DEFAULT 'admin@solus.ai';
DECLARE ADMIN_PASSWORD VARCHAR(128) DEFAULT 'solus@1231';
DECLARE vAction text;
DECLARE vEmail text; 
DECLARE vPassword text;
DECLARE vUserName text; 
DECLARE vCreatedBy text; 
DECLARE vErrMsg text;
DECLARE vSuccess text;    
DECLARE vResultMsg text; 
declare user_result json;   
DECLARE EXIT HANDLER FOR SQLEXCEPTION 
BEGIN 
	SET vSuccess = 0; 
	GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
	SET vErrMsg = CONCAT(IFNULL(vErrMsg,''), "ERROR ", @errno, " (", @sqlstate, "): ", @text);
    SET vResultMsg=concat(' vUserName=',ifnull(vUserName,'NA'),' vEmail= ',ifnull(vEmail,'NA'),' vPassword=',ifnull(vPassword,'NA'),' vCreatedBy=',ifnull(vCreatedBy,'NA'));
    SET vResultMsg=concat(' vSuccess=',vSuccess,' vErrMsg=',vErrMsg, ' ', vResultMsg);
    insert into log_solus (module, msg)  Values  (ProcName,vResultMsg);
END;


    
SET vErrMsg='';
SET vSuccess=1;
SET user_result="[{}]" ;
SET vResultMsg=concat(' vUserName=',ifnull(vUserName,'NA'),' vEmail= ',ifnull(vEmail,'NA'),' vPassword=',ifnull(vPassword,'NA'),' vCreatedBy=',ifnull(vCreatedBy,'NA'));
-- insert into log_solus (module, msg)  Values  (ProcName,vResultMsg);
	
SET vAction =json_unquote(json_extract(in_request_json,"$.MEASURE"));
SET vEmail =json_unquote(json_extract(in_request_json,"$.AGGREGATION_2")); 
SET vPassword =json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
SET vUserName =json_unquote(json_extract(in_request_json,"$.AGGREGATION_1")); 
SET vCreatedBy =json_unquote(json_extract(in_request_json,"$.USER")); 

-- create log
insert into log_solus(module,input_json) values (vAction,in_request_json);
select id into @id from log_solus order by id desc limit 1;

-- CREATE ADMIN USER
select substring_index(`Value`,'_',1) into @clientname from M_Config where `Name` = 'Tenant_Name';
select date_format(current_date(), '%m%d') into @today;
SET ADMIN_USER =concat(ifnull(@clientname,'CART'),'admin@solus.ai');
SET ADMIN_PASSWORD =concat(ifnull(@clientname,'CART'),'solus$',@today);
insert ignore into DIY_User(Email,UserName, `Password`) values (ADMIN_USER, 'Admin', ADMIN_PASSWORD);


IF vAction= 'INSERT' AND vCreatedBy=ADMIN_USER THEN
select LOCATE ('admin@solus.ai',vUserName) into @checkadminuserflg;

IF @checkadminuserflg > 0 
THEN
    SET vErrMsg=concat('ERROR :DIY-003. admin@solus.ai is a reserved user. You are NOT allowed to create a user that contains admin@solus.ai' );
	SET vSuccess=0; 
ELSE
		INSERT INTO DIY_User
		(
		`Email`,
		`UserName`,
		`Password`,
		`CreatedBy`,
		`ModifiedBy`)
		VALUES
		(
		vEmail,
		vUserName,
		vPassword,
		vCreatedBy,
		vCreatedBy);
END IF;
ELSEIF vAction= 'UPDATE' AND ( vCreatedBy=ADMIN_USER or vCreatedBy=vEmail)  THEN
		UPDATE DIY_User SET
		`UserName` = vUserName,
		`Password` = vPassword,
		`ModifiedBy` = vCreatedBy
		WHERE `Email` = vEmail;

      /*  
ELSEIF vAction= 'LOGIN'  AND vEmail=ADMIN_USER THEN    


    IF vPassword <> ADMIN_PASSWORD THEN
        SET vErrMsg=concat('ERROR :DIY-002. Invalid Password. ',' vEmail= ',ifnull(vEmail,'NA'),' vPassword=',ifnull(vPassword,'NA'));
		SET vSuccess=0; 
    END IF;
    */
    
ELSEIF vAction= 'LOGIN'  THEN
	SET @isValidPassword=0;
	SELECT 1 into @isValidPassword FROM DIY_User WHERE `Email` = vEmail and `Password`= vPassword ;
    IF @isValidPassword <> 1 THEN
        SET vErrMsg=concat('ERROR :DIY-002. Invalid Password. ',' vEmail= ',ifnull(vEmail,'NA'),' vPassword=',ifnull(vPassword,'NA'));
		SET vSuccess=0; 
    END IF;
    
ELSEIF vAction = 'DELETE' AND vCreatedBy=ADMIN_USER THEN
		DELETE FROM DIY_User WHERE `Email` = vEmail;
ELSEIF vAction = 'SELECT' AND vCreatedBy=ADMIN_USER THEN
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Email",`Email`,"Username",`UserName`) )),"]")into @temp_result_user from DIY_User;') ;
        select @Query_String;
	    PREPARE statement from @Query_String;
	    Execute statement;
	    Deallocate PREPARE statement;
       select @temp_result_user into @user_result;
ELSE
    SET vErrMsg=concat('ERROR :DIY-001. vAction has Invalid Value. ',vAction, ' List of Possible Values are  :',vActionValidValues);
	SET vSuccess=0; 
END IF;

SET out_result_json = json_object("Status",VSuccess,"vErrMsg",vErrMsg,"vData",@user_result);

SET vResultMsg=concat(' vSuccess=',vSuccess,' vErrMsg=',vErrMsg, ' ', vResultMsg);
-- insert into log_solus (module, msg)  Values  (ProcName,vResultMsg);
UPDATE log_solus SET output_json=out_result_json WHERE id=@id AND module=vAction;

END$$
DELIMITER ;
