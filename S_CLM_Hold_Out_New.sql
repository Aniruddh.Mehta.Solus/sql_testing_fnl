
CREATE OR REPLACE PROCEDURE `S_CLM_Hold_Out_New`()
BEGIN
DECLARE v,v_lt,v_st bigint(20);
DECLARE currdate date;
DECLARE Exe_ID int;


SET sql_safe_updates=0;
SET foreign_key_checks=0;
SET currdate=current_date();

INSERT into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
							values ('S_Hold_Out_New', '',now(),null,'Started', 010222);
SET Exe_ID = F_Get_Execution_Id();        
select * from ETL_Execution_Details Order by Execution_Id DESC LIMIT 1;


SELECT `Value` into @lt_percentage from Control_Group_Config where `Name`='Long_Term_Percent';
SELECT `Value` into @st_percentage from Control_Group_Config where `Name`='Short_Term_Percent';
SELECT `Value` into @lt_days from Control_Group_Config where `Name`='Long_Term_Refresh_Days';
SELECT `Value` into @st_days from Control_Group_Config where `Name`='Short_Term_Refresh_Days';
SELECT `Value` into @cool_off_cycle from Control_Group_Config where `Name`='Cool_Off_Cycle';



SELECT `Value` into @cg_start_date from Control_Group_Config where `Name`='Hold_Out_Process_Start_Date';
SET @lt_date_diff=datediff(current_date(),@cg_start_date);
IF @lt_date_diff>@lt_days/2 THEN
SET @lt_percentage=@lt_percentage*2;
END IF;

SET @st_date_diff=datediff(current_date(),@cg_start_date);
IF @st_date_diff>@st_days/2 THEN
SET @st_percentage=@st_percentage*2;
END IF;




set sql_safe_updates=0;
update Hold_Out 
set Load_Execution_ID=99
where (
(datediff(current_date(),Long_Term_Insert_Date) >@lt_days and datediff(current_date(),Short_Term_Insert_Date) >@st_days) OR
(datediff(current_date(),Long_Term_Insert_Date) >@lt_days and Short_Term_Insert_Date is NULL) OR 
(datediff(current_date(),Short_Term_Insert_Date) >@st_days and Long_Term_Insert_Date is NULL));
    

insert into Hold_Out_Archive select * from Hold_Out where Load_Execution_ID=99;
delete from Hold_Out where Load_Execution_ID=99;

set sql_safe_updates=0;
update Hold_Out 
set Long_Term_Insert_Date =NULL where datediff(current_date(),Long_Term_Insert_Date) >@lt_days;

set sql_safe_updates=0;
update Hold_Out 
set Short_Term_Insert_Date =NULL where datediff(current_date(),Short_Term_Insert_Date) >@st_days;

SELECT round((100*((select count(1) from Hold_Out where Long_Term_Insert_Date is not NULL)/(select count(1) from V_CLM_Customer_One_View))),2) into @curr_lt;
SELECT round((100*((select count(1) from Hold_Out where Short_Term_Insert_Date is not NULL)/(select count(1) from V_CLM_Customer_One_View))),2) into @curr_st;
SET @lt_days2=@lt_days*@cool_off_cycle;
SET @st_days2=@st_days*@cool_off_cycle;
SET @lt_req=round((@lt_percentage-@curr_lt),2);
SET @st_req=round((@st_percentage-@curr_st),2);
SELECT count(Customer_Id) into v from V_CLM_Customer_One_View;
SELECT round(v*(@lt_req/100)) into v_lt;

SET @sql1=concat('insert ignore into Hold_Out(Customer_Id,Long_Term_Insert_Date,Long_Term_Exit_Date,Load_Execution_ID,Inserted_On)','(
    select cov.Customer_Id,current_date(),date_add(Current_Date(),interval @lt_days day),1,now() 
    FROM V_CLM_Customer_One_View cov
    where Customer_Id not in
    (select Customer_ID from Hold_Out_Archive where(
    (datediff(current_date(),Long_Term_Insert_Date)<=@lt_days2) OR (datediff(current_date(),Short_Term_Insert_Date)<=@st_days2)))
    and Customer_Id not in (select Customer_ID from Hold_Out)
    ORDER BY RAND() limit ',v_lt,
    ')');
    
prepare stmt from @sql1;
execute stmt;
deallocate prepare stmt;



SELECT round(v*(@st_req/100)) into v_st;

SET @sql2=concat('insert ignore into Hold_Out(Customer_Id,Short_Term_Insert_Date,Short_Term_Exit_Date,Load_Execution_ID,Inserted_On)','(
    select cov.Customer_Id,current_date(),date_add(Current_Date(),interval @st_days day),1,now() 
    FROM V_CLM_Customer_One_View cov
    where Customer_Id not in
    (select Customer_ID from Hold_Out_Archive where(
    (datediff(current_date(),Short_Term_Exit_Date)<=@st_days2) OR (datediff(current_date(),Long_Term_Exit_Date)<=@lt_days2)))
    and Customer_Id not in (select Customer_ID from Hold_Out)
    ORDER BY RAND() limit ',v_st,
    ')');
    
select @sql2;
prepare stmt from @sql2;
execute stmt;
deallocate prepare stmt;

update ETL_Execution_Details
set Status ='Succeeded',
End_Time = now()
where Procedure_Name='S_Hold_Out_New' 
and Execution_ID = Exe_ID;
select 'S_CLM_Hold_Out : COMPLETED ' as '', now() as '';

END

