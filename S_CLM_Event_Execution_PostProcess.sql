
CREATE or replace PROCEDURE `S_CLM_Event_Execution_PostProcess`(IN Event_ID_cur1 BIGINT(20),IN Communication_Template_ID_cur1 BIGINT(20),IN Event_Execution_Date_ID VARCHAR(64))
begin
set @Event_ID_cur1=Event_ID_cur1;
set @Communication_Template_ID_cur1=Communication_Template_ID_cur1;
set @Event_Execution_Date_ID=Event_Execution_Date_ID;

select Offer_Type_ID into @OfferType_ID FROM
    Communication_Template
WHERE
    ID = @Communication_Template_ID_cur1;

select OfferTypeId into @External_Offer_Type_ID from CLM_OfferType where OfferTypeName = 'External_Customer_Vouchers';

if @OfferType_ID = @External_Offer_Type_ID
THEN
	
	INSERT INTO Event_Execution_Voucher_Pending
    SELECT * from Event_Execution_Stg
    where Communication_Template_ID = @Communication_Template_ID_cur1;
    SELECT 
			`Value`
		INTO @Out_FileLocation FROM
			M_Config
		WHERE
			`Name` = 'File_Path';
	select Offer_Code into @Offer from Communication_Template where ID =@Communication_Template_ID_cur1;
    SET @Offer = replace(@Offer,' ','_');
	SET @Out_FileName = concat(@Out_FileLocation,'/CLM_Event_Execution_Voucher_Pending_',@Offer,'_',date_format(Current_Date(),"%Y%m%d"),'.csv');
    select @Out_FileName;
    DELETE from Event_Execution_Stg
    where Communication_Template_ID = @Communication_Template_ID_cur1;    
    
    SET @s1 = Concat(' (Select b.Customer_Key, a.Offer_Code from Event_Execution_Voucher_Pending a JOIN CDM_Customer_Key_Lookup b ON a.Customer_Id = b.Customer_Id and a.Communication_Template_ID = ',@Communication_Template_ID_cur1,' )');
    SET @s2=concat(' SELECT "Customer_Key","Offer_Key" UNION ALL ',@s1,' INTO OUTFILE "',@Out_FileName,'" FIELDS TERMINATED BY "|" escaped by "\" LINES TERMINATED BY "\n"');
		PREPARE stmt from @s2;        
		execute stmt;
		deallocate prepare stmt;
END IF;

-- Microsite Starts
select Microsite_Template into @Microsite_Template FROM
    Communication_Template
WHERE
    ID = @Communication_Template_ID_cur1;

if @Microsite_Template <> '' AND @Microsite_Template <>'""' AND @Microsite_Template IS NOT NULL
THEN
SET @Client_Code=(SELECT `Value` from M_Config where `Name` ='Client_Code');
SET @Client_Code=IFNULL(@Client_Code,'UNKNOWN');
UPDATE Event_Execution_Stg
SET Microsite_URL=concat('s00.ai/',@Client_Code,'/',ConvertIntegerToBase36(replace(Event_Execution_Id,'-','')));
END IF;






select count(1) into @Customer_Count from Event_Execution_Stg Where Event_Id = @Event_ID_cur1;
IF @vEvent_Limit > 0 AND @vEvent_Limit <= 1
THEN
	SET @vEvent_Limit = round(@Customer_Count*@vEvent_Limit);
	SET @limit_conditions = concat( " order by GoodTime_Score desc LIMIT ",@vEvent_Limit);
ELSEIF @vEvent_Limit > 1 
THEN
	SET  @limit_conditions = concat( " order by GoodTime_Score desc LIMIT ",@vEvent_Limit);
ELSE 
	SET @limit_conditions = '';
END IF;

-- select @vEvent_Limit;

SET @SQL3=concat('INSERT IGNORE INTO Event_Execution
select ees.* from Event_Execution_Stg ees, V_CLM_Customer_One_View SVOC
WHERE ees.Customer_Id = SVOC.Customer_Id
and ees.Event_Id = ',@Event_ID_cur1,'
and ees.Is_Target="Y" 
',@limit_conditions
);
PREPARE stmt1 from @SQL3;
-- SELECT @SQL3;
EXECUTE stmt1;
DEALLOCATE PREPARE stmt1;

SET @SQL4=concat('insert ignore into Event_Execution_Rejected
(SELECT * from Event_Execution_Stg ees
WHERE ees.Customer_Id not in (select Customer_Id from Event_Execution ee where Event_Id = ',@Event_ID_cur1,' and Event_Execution_Date_ID = ',@Event_Execution_Date_ID,'))'
);
PREPARE stmt1 from @SQL4;
-- SELECT @SQL4;
EXECUTE stmt1;
DEALLOCATE PREPARE stmt1;
UPDATE Event_Execution_Rejected 
SET Message=concat(@Event_ID_cur1,' : Event Limit Exceeded')
WHERE Message is null and Is_Target <>'N'
and Event_Id=@Event_ID_cur1;
UPDATE Event_Execution_Rejected 
SET Message=concat(@Event_ID_cur1,' : Reccomendation Not Available')
WHERE Message is null and Is_Target ='N'
and Event_Id=@Event_ID_cur1;


set sql_safe_updates=0;
select `Value` into @New_Hold_Out_Flag FROM M_Config where `Name`='New_Control_Group_Mechanism';
IF IFNULL(@New_Hold_Out_Flag,'N')='Y' THEN
	
	select Event_Response_Days into @reponse_days from Event_Master where ID = @Event_ID_cur1;
    SET @reponse_days = ifnull(@reponse_days,7);
	
	UPDATE Event_Execution ee, Hold_Out ho 
	SET ee.In_Control_Group = "Y",ee.Is_Target = "Y"
	WHERE ee.Customer_Id = ho.Customer_Id 
	AND ho.Long_Term_Insert_Date is not NULL
	AND ee.Event_Id=@Event_ID_cur1;

	UPDATE Event_Execution ee, Hold_Out ho 
	SET ee.In_Event_Control_Group = "Y",ee.Is_Target = "Y" 
	WHERE ee.Customer_Id = ho.Customer_Id 
	AND ho.Short_Term_Insert_Date is not NULL
	AND ee.Event_Id=@Event_ID_cur1;
    
    UPDATE Event_Execution ee, Hold_Out ho
    SET ee.LT_Control_Covers_Response_Days = 'Y'
    WHERE ee.Customer_Id = ho.Customer_Id
    AND ee.In_Control_Group='Y'
    and ho.Long_Term_Exit_Date>=date_add(ee.event_Execution_Date_ID,interval @reponse_days day);
    
    UPDATE Event_Execution ee, Hold_Out ho
    SET ee.ST_Control_Covers_Response_Days = 'Y'
    WHERE ee.Customer_Id = ho.Customer_Id
    AND ee.In_Event_Control_Group='Y'
    and ho.Short_Term_Exit_Date>=date_add(ee.event_Execution_Date_ID,interval @reponse_days day);
ELSE
	UPDATE Event_Execution ee, Hold_Out ho 
	SET ee.In_Control_Group = "Y",
    ee.Is_Target = "Y",
    ee.LT_Control_Covers_Response_Days="Y",
    ee.ST_Control_Covers_Response_Days="Y"
	WHERE ee.Customer_Id = ho.Customer_Id  
	AND ee.Customer_Id = ho.Customer_Id
	AND ee.Event_Id=@Event_ID_cur1;
END IF;

END

