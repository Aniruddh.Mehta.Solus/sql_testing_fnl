DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_loyalty_leaderboard_metrics`()
BEGIN

DELETE from log_solus where module = 'S_dashboard_loyalty_leaderboard_metrics';
SET SQL_SAFE_UPDATES=0;

SET @Start_Date = date_format(date_sub(concat(date_format(current_date(),"%Y%m"),'01'), INTERVAL 14 MONTH),"%Y%m");
SET @End_Date = date_format(current_date(),"%Y%m");

SET @TM = (SELECT date_format(current_date(),"%Y%m"));
SET @LM = F_Month_Diff(@TM,1);
SET @SMLY = F_Month_Diff(@TM,12);
SET @TY=substr(@TM,1,4);
SET @LY=substr(@SMLY,1,4);
SET @LLY = @LY - 1;

SELECT @TM,@LM,@SMLY,@TY,@LY,@LLY;
	
DROP TABLE IF EXISTS Dashboard_Loyalty_Leaderboard_Temp;
  CREATE TABLE IF NOT EXISTS Dashboard_Loyalty_Leaderboard_Temp ( 
	`Region` varchar(20),
	`Store_Id` varchar(128),
	`Store_Name` varchar(128),
	`City` varchar(128),
	`Region_Rank` int(11),
    `Nation_Rank` int(11),
	`Enrollment_Percent` decimal(10,2),
	`Tagged_Percent` decimal(10,2),
	`EmailcapPercent` decimal(10,2),
	`MobilecapPercent` decimal(10,2)
  ) ENGINE = INNODB DEFAULT CHARACTER SET=LATIN1; 
  
 
  ALTER TABLE Dashboard_Loyalty_Leaderboard_Temp    
    ADD INDEX (Store_Id), 
    ADD INDEX (City),
	ADD INDEX (Region);

INSERT INTO log_solus(module, msg) values ('S_dashboard_loyalty_leaderboard_metrics', 'Loading Store wise data');

INSERT INTO Dashboard_Loyalty_Leaderboard_Temp
(
Store_Id,
Enrollment_Percent
)		
select 
A.Store_Id,
(A.c1*100)/B.c2 as Enrollment_Percent 
from 
(
(select Store_Id,
count(distinct(Customer_Id)) as c1 
from Dashboard_Loyalty_Metrics_Temp 
where Membership_Status = "Y" 
group by Store_Id)A
,
(select Store_Id, 
count(distinct(Customer_Id)) as c2 
from Dashboard_Loyalty_Metrics_Temp 
group by Store_Id)B
)
where A.Store_Id = B.Store_Id
group by A.Store_Id;

INSERT INTO Dashboard_Loyalty_Leaderboard_Temp
(
Store_Id,
Tagged_Percent
)		
select 
A.Store_Id,
(A.Tagged_Bill*100)/B.Total_Bill as Tagged_Percent 
from 
(
(select Store_Id,
sum(visits) as Tagged_Bill
from Dashboard_Loyalty_Metrics_Temp 
where Membership_Status = "Y" 
group by Store_Id)A
,
(select Store_Id, 
sum(visits) as Total_Bill
from Dashboard_Loyalty_Metrics_Temp 
group by Store_Id)B
)
where A.Store_Id = B.Store_Id
group by A.Store_Id;

INSERT INTO Dashboard_Loyalty_Leaderboard_Temp
(
Store_Id,
EmailcapPercent
)		
select 
A.Store_Id,
(A.c1*100)/B.c2 as EmailcapPercent 
from 
(
(select Store_Id,
COUNT(DISTINCT Customer_Id) as c1
from Dashboard_Loyalty_Metrics_Temp 
where Email_Capture = "Y" 
group by Store_Id)A
,
(select Store_Id, 
COUNT(DISTINCT Customer_Id) as c2
from Dashboard_Loyalty_Metrics_Temp 
group by Store_Id)B
)
where A.Store_Id = B.Store_Id
group by A.Store_Id;

INSERT INTO Dashboard_Loyalty_Leaderboard_Temp
(
Store_Id,
MobilecapPercent
)		
select 
A.Store_Id,
(A.c1*100)/B.c2 as MobilecapPercent 
from 
(
(select Store_Id,
COUNT(DISTINCT Customer_Id) as c1
from Dashboard_Loyalty_Metrics_Temp 
where Mobile_Capture = "Y" 
group by Store_Id)A
,
(select Store_Id, 
COUNT(DISTINCT Customer_Id) as c2
from Dashboard_Loyalty_Metrics_Temp 
group by Store_Id)B
)
where A.Store_Id = B.Store_Id
group by A.Store_Id;

UPDATE Dashboard_Loyalty_Leaderboard_Temp A, Dashboard_Loyalty_Metrics_Temp B
SET A.Region = B.Store_Region, 
A.Store_Name = B.Store_Name,
A.City = B.Store_City,
A.Region_Rank = B.Region_Rank,
A.Nation_Rank = B.National_rank
WHERE A.Store_Id=B.Store_Id;

END$$
DELIMITER ;
