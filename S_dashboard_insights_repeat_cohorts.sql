DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_insights_repeat_cohorts`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    
    
     SET @FY_SATRT_DATE = date_format(date_sub(concat(IN_FILTER_CURRENTMONTH,'01'), INTERVAL 11 MONTH),"%Y%m");
	 SET @FY_END_DATE = IN_FILTER_CURRENTMONTH;

			SET @filter_cond=concat(" Bill_Year_Month = '",@FY_END_DATE,"'");
			SET @date_select=' date_format(concat(Bill_Year_Month,"01"),"%b-%Y") AS "Year" ';
	
	
	IF IN_AGGREGATION_2 = 'ALL'
	THEN 
		IF IN_MEASURE = 'FLOWGRAPH' AND IN_NUMBER_FORMAT = 'NUMBER'
		THEN
			SELECT @date_select,@filter_cond;
			SET @select_query = CASE WHEN IN_NUMBER_FORMAT = 'NUMBER' THEN 'CAST(ifnull(Customer_Count,0) AS CHAR)'
								WHEN IN_NUMBER_FORMAT = 'PERCENT' THEN 'ifnull(Customer_Percent,0)' END;
			set @view_stmt= CONCAT(' create or replace view V_Dashboard_Repeat_Cohorts_Graph as 
							SELECT "Base" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "ALL"
                            AND AGGREGATION  = "Base"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "7 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "ALL"
                            AND AGGREGATION  = "Repeat within 7 Days"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "15 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "ALL"
                            AND AGGREGATION  = "Repeat within 15 Days"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "30 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "ALL"
                            AND AGGREGATION  = "Repeat within 30 Days"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "60 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "ALL"
                            AND AGGREGATION  = "Repeat within 60 Days"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "90 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "ALL"
                            AND AGGREGATION  = "Repeat within 90 Days"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "180 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "ALL"
                            AND AGGREGATION  = "Repeat within 180 Days"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "365 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "ALL"
                            AND AGGREGATION  = "Repeat within 365 Days"
							AND ',@filter_cond,'
							group by 2
							order by "Name" ASC
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("displayName",`Name`,"value1","","value2",`Value`) )),"]")into @temp_result from V_Dashboard_Repeat_Cohorts_Graph;' ;
													
			
		ELSEIF IN_MEASURE = 'FLOWGRAPH' AND IN_NUMBER_FORMAT = 'PERCENT'
		THEN
			SELECT @date_select,@filter_cond;
			SET @select_query = CASE WHEN IN_NUMBER_FORMAT = 'NUMBER' THEN 'CAST(ifnull(Customer_Count,0) AS CHAR)'
								WHEN IN_NUMBER_FORMAT = 'PERCENT' THEN 'ifnull(Customer_Percent,0)' END;
			set @view_stmt= CONCAT(' create or replace view V_Dashboard_Repeat_Cohorts_Graph as
							SELECT "7 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "ALL"
                            AND AGGREGATION  = "Repeat within 7 Days%"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "15 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "ALL"
                            AND AGGREGATION  = "Repeat within 15 Days%"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "30 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "ALL"
                            AND AGGREGATION  = "Repeat within 30 Days%"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "60 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "ALL"
                            AND AGGREGATION  = "Repeat within 60 Days%"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "90 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "ALL"
                            AND AGGREGATION  = "Repeat within 90 Days%"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "180 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "ALL"
                            AND AGGREGATION  = "Repeat within 180 Days%"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "365 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "ALL"
                            AND AGGREGATION  = "Repeat within 365 Days%"
							AND ',@filter_cond,'
							group by 2
							order by "Name" ASC
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("displayName",`Name`,"value1",`Value`,"value2","") )),"]")into @temp_result from V_Dashboard_Repeat_Cohorts_Graph;' ;

		ELSEIF IN_MEASURE = 'TABLE' AND IN_NUMBER_FORMAT = 'NUMBER'
		THEN
        SELECT group_concat(distinct Bill_Year_Month order by Bill_Year_Month) into @distinct_rc from Dashboard_Insights_Repeat_Cohorts where Bill_Year_Month between @FY_SATRT_DATE and @FY_END_DATE;
		    select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
			set @loop_var=1;
			set @json_select='';
            set @top_query='';
			select @loop_var,@num_of_rc,@distinct_rc;
			WHILE @loop_var<=@num_of_rc
			do
			SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
            SET @alias_Name = date_format(concat(@selected_rc,'01'),'%b-%y');
            
			set @top_query=concat(@top_query,",MAX(CASE WHEN Bill_Year_Month = '",@selected_rc,"' Then Bills else 0 END) AS `",@alias_Name,"`");
				set @json_select=concat(@json_select,',"',@alias_Name,'",`',@alias_Name,'`');
				set @loop_var=@loop_var+1;
			end while;
			
			
			SELECT @json_select,@top_query;
			set @view_stmt = concat("create or replace  View V_Dashboard_Repeat_Cohorts_Table as
			(select 'Base' as 'Transaction Month' 
			",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM', CAST(format(ifnull(Customer_Count,0),',') AS CHAR) AS 'Bills'  FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'ALL'
			AND AGGREGATION = 'Base'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
			)A)
			UNION
			(select 'Repeat within 7 Days' as 'Transaction Month' 
			",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM',CAST(format(ifnull(Customer_Count,0),',') AS CHAR) as 'Bills' FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'ALL'
			AND AGGREGATION = 'Repeat within 7 Days'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
			)A)
			UNION
			(select 'Repeat within 15 Days' as 'Transaction Month'
            ",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM',CAST(format(ifnull(Customer_Count,0),',') AS CHAR) as 'Bills' FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'ALL'
			AND AGGREGATION = 'Repeat within 15 Days'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
			)A)
			UNION
			(select 'Repeat within 30 Days' as 'Transaction Month'
            ",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM',CAST(format(ifnull(Customer_Count,0),',') AS CHAR) as 'Bills' FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'ALL'
			AND AGGREGATION = 'Repeat within 30 Days'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
			)A)
			UNION
			(select 'Repeat within 60 Days' as 'Transaction Month'
            ",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM',CAST(format(ifnull(Customer_Count,0),',') AS CHAR) as 'Bills' FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'ALL'
			AND AGGREGATION = 'Repeat within 60 Days'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
			)A)
			UNION
			(select 'Repeat within 90 Days' as 'Transaction Month'
            ",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM',CAST(format(ifnull(Customer_Count,0),',') AS CHAR) as 'Bills' FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'ALL'
			AND AGGREGATION = 'Repeat within 90 Days'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
			)A)
			UNION
			(select 'Repeat within 180 Days' as 'Transaction Month'
            ",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM',CAST(format(ifnull(Customer_Count,0),',') AS CHAR) as 'Bills' FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'ALL'
			AND AGGREGATION = 'Repeat within 180 Days'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
			)A)
			UNION
			(select 'Repeat within 365 Days' as 'Transaction Month'
            ",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM',CAST(format(ifnull(Customer_Count,0),',') AS CHAR) as 'Bills' FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'ALL'
			AND AGGREGATION = 'Repeat within 365 Days'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
			)A)
			;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Transaction Month",`Transaction Month`',@json_select,') )),"]")into @temp_result from V_Dashboard_Repeat_Cohorts_Table;') ;
		
		ELSEIF IN_MEASURE = 'TABLE' AND IN_NUMBER_FORMAT='PERCENT'
		THEN
        SELECT group_concat(distinct Bill_Year_Month order by Bill_Year_Month) into @distinct_rc from Dashboard_Insights_Repeat_Cohorts where Bill_Year_Month between @FY_SATRT_DATE and @FY_END_DATE;
		    select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
			set @loop_var=1;
			set @json_select='';
            set @top_query='';
			select @loop_var,@num_of_rc,@distinct_rc;
			WHILE @loop_var<=@num_of_rc
			do
			SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
            SET @alias_Name = date_format(concat(@selected_rc,'01'),'%b-%y');
            
			set @top_query=concat(@top_query,",MAX(CASE WHEN Bill_Year_Month = '",@selected_rc,"' Then Bills else 0 END) AS `",@alias_Name,"`");
				set @json_select=concat(@json_select,',"',@alias_Name,'",`',@alias_Name,'`');
				set @loop_var=@loop_var+1;
			end while;
			
			
			SELECT @json_select,@top_query;
			set @view_stmt = concat("create or replace  View V_Dashboard_Repeat_Cohorts_Table as
			(select 'Repeat within 7 Days' as 'Transaction Month' 
			",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM', concat(ifnull(Customer_Percent,0),'%') AS 'Bills' FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'ALL'
			AND AGGREGATION = 'Repeat within 7 Days%'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
            )A)
			UNION
			(select 'Repeat within 15 Days' as 'Transaction Month'
            ",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM', concat(ifnull(Customer_Percent,0),'%') AS 'Bills'  FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'ALL'
			AND AGGREGATION = 'Repeat within 15 Days%'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
            )A)
			UNION
			(select 'Repeat within 30 Days' as 'Transaction Month'
            ",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM', concat(ifnull(Customer_Percent,0),'%') AS 'Bills'  FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'ALL'
			AND AGGREGATION = 'Repeat within 30 Days%'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
            )A)
			UNION
			(select 'Repeat within 60 Days' as 'Transaction Month'
            ",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM', concat(ifnull(Customer_Percent,0),'%') AS 'Bills'  FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'ALL'
			AND AGGREGATION = 'Repeat within 60 Days%'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
            )A)
			UNION
			(select 'Repeat within 90 Days' as 'Transaction Month'
            ",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM', concat(ifnull(Customer_Percent,0),'%') AS 'Bills'  FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'ALL'
			AND AGGREGATION = 'Repeat within 90 Days%'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
            )A)
			UNION
			(select 'Repeat within 180 Days' as 'Transaction Month'
            ",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM', concat(ifnull(Customer_Percent,0),'%') AS 'Bills'  FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'ALL'
			AND AGGREGATION = 'Repeat within 180 Days%'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
            )A)
			UNION
			(select 'Repeat within 365 Days' as 'Transaction Month'
            ",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM', concat(ifnull(Customer_Percent,0),'%') AS 'Bills'  FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'ALL'
			AND AGGREGATION = 'Repeat within 365 Days%'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
            )A)
			;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Transaction Month",`Transaction Month`',@json_select,') )),"]")into @temp_result from V_Dashboard_Repeat_Cohorts_Table;') ;
		END IF;

END IF;
		
	IF IN_AGGREGATION_2 = 'NEW'
	THEN 
		IF IN_MEASURE = 'FLOWGRAPH' AND IN_NUMBER_FORMAT = 'NUMBER'
		THEN
			SELECT @date_select,@filter_cond;
			SET @select_query = CASE WHEN IN_NUMBER_FORMAT = 'NUMBER' THEN 'CAST(ifnull(Customer_Count,0) AS CHAR)'
								WHEN IN_NUMBER_FORMAT = 'PERCENT' THEN 'ifnull(Customer_Percent,0)' END;
			set @view_stmt= CONCAT(' create or replace view V_Dashboard_Repeat_Cohorts_Graph as 
							SELECT "Base" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "NEW"
                            AND AGGREGATION  = "Base"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "7 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "NEW"
                            AND AGGREGATION  = "Repeat within 7 Days"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "15 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "NEW"
                            AND AGGREGATION  = "Repeat within 15 Days"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "30 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "NEW"
                            AND AGGREGATION  = "Repeat within 30 Days"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "60 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "NEW"
                            AND AGGREGATION  = "Repeat within 60 Days"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "90 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "NEW"
                            AND AGGREGATION  = "Repeat within 90 Days"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "180 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "NEW"
                            AND AGGREGATION  = "Repeat within 180 Days"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "365 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "NEW"
                            AND AGGREGATION  = "Repeat within 365 Days"
							AND ',@filter_cond,'
							group by 2
							order by "Name" ASC
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("displayName",`Name`,"value1","","value2",`Value`) )),"]")into @temp_result from V_Dashboard_Repeat_Cohorts_Graph;' ;
													
			
		ELSEIF IN_MEASURE = 'FLOWGRAPH' AND IN_NUMBER_FORMAT = 'PERCENT'
		THEN
			SELECT @date_select,@filter_cond;
			SET @select_query = CASE WHEN IN_NUMBER_FORMAT = 'NUMBER' THEN 'CAST(ifnull(Customer_Count,0) AS CHAR)'
								WHEN IN_NUMBER_FORMAT = 'PERCENT' THEN 'ifnull(Customer_Percent,0)' END;
			set @view_stmt= CONCAT(' create or replace view V_Dashboard_Repeat_Cohorts_Graph as
							SELECT "7 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "NEW"
                            AND AGGREGATION  = "Repeat within 7 Days%"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "15 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "NEW"
                            AND AGGREGATION  = "Repeat within 15 Days%"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "30 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "NEW"
                            AND AGGREGATION  = "Repeat within 30 Days%"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "60 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "NEW"
                            AND AGGREGATION  = "Repeat within 60 Days%"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "90 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "NEW"
                            AND AGGREGATION  = "Repeat within 90 Days%"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "180 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "NEW"
                            AND AGGREGATION  = "Repeat within 180 Days%"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "365 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "NEW"
                            AND AGGREGATION  = "Repeat within 365 Days%"
							AND ',@filter_cond,'
							group by 2
							order by "Name" ASC
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("displayName",`Name`,"value1",`Value`,"value2","") )),"]")into @temp_result from V_Dashboard_Repeat_Cohorts_Graph;' ;
			
		ELSEIF IN_MEASURE = 'TABLE' AND IN_NUMBER_FORMAT = 'NUMBER'
        THEN
			SELECT group_concat(distinct Bill_Year_Month order by Bill_Year_Month) into @distinct_rc from Dashboard_Insights_Repeat_Cohorts where Bill_Year_Month between @FY_SATRT_DATE and @FY_END_DATE;
		    select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
			set @loop_var=1;
			set @json_select='';
            set @top_query='';
			select @loop_var,@num_of_rc,@distinct_rc;
			WHILE @loop_var<=@num_of_rc
			do
			SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
            SET @alias_Name = date_format(concat(@selected_rc,'01'),'%b-%y');
            
			set @top_query=concat(@top_query,",MAX(CASE WHEN Bill_Year_Month = '",@selected_rc,"' Then Bills else 0 END) AS `",@alias_Name,"`");
				set @json_select=concat(@json_select,',"',@alias_Name,'",`',@alias_Name,'`');
				set @loop_var=@loop_var+1;
			end while;
			
			
			SELECT @json_select,@top_query;
			set @view_stmt = concat("create or replace  View V_Dashboard_Repeat_Cohorts_Table as
			(select 'Base' as 'Transaction Month' 
			",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM',CAST(format(ifnull(Customer_Count,0),',') AS CHAR) as 'Bills' FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'NEW'
			AND AGGREGATION = 'Base'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
			)A)
			UNION
			(select 'Repeat within 7 Days' as 'Transaction Month' 
			",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM',CAST(format(ifnull(Customer_Count,0),',') AS CHAR) as 'Bills' FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'NEW'
			AND AGGREGATION = 'Repeat within 7 Days'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
			)A)
			UNION
			(select 'Repeat within 15 Days' as 'Transaction Month'
            ",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM',CAST(format(ifnull(Customer_Count,0),',') AS CHAR) as 'Bills' FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'NEW'
			AND AGGREGATION = 'Repeat within 15 Days'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
			)A)
			UNION
			(select 'Repeat within 30 Days' as 'Transaction Month'
            ",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM',CAST(format(ifnull(Customer_Count,0),',') AS CHAR) as 'Bills' FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'NEW'
			AND AGGREGATION = 'Repeat within 30 Days'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
			)A)
			UNION
			(select 'Repeat within 60 Days' as 'Transaction Month'
            ",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM',CAST(format(ifnull(Customer_Count,0),',') AS CHAR) as 'Bills' FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'NEW'
			AND AGGREGATION = 'Repeat within 60 Days'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
			)A)
			UNION
			(select 'Repeat within 90 Days' as 'Transaction Month'
            ",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM',CAST(format(ifnull(Customer_Count,0),',') AS CHAR) as 'Bills' FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'NEW'
			AND AGGREGATION = 'Repeat within 90 Days'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
			)A)
			UNION
			(select 'Repeat within 180 Days' as 'Transaction Month'
            ",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM',CAST(format(ifnull(Customer_Count,0),',') AS CHAR) as 'Bills' FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'NEW'
			AND AGGREGATION = 'Repeat within 180 Days'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
			)A)
			UNION
			(select 'Repeat within 365 Days' as 'Transaction Month'
            ",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM',CAST(format(ifnull(Customer_Count,0),',') AS CHAR) as 'Bills' FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'NEW'
			AND AGGREGATION = 'Repeat within 365 Days'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
			)A)
			;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Transaction Month",`Transaction Month`',@json_select,') )),"]")into @temp_result from V_Dashboard_Repeat_Cohorts_Table;') ;

		ELSEIF IN_MEASURE = 'TABLE' AND IN_NUMBER_FORMAT='PERCENT'
        THEN
			SELECT group_concat(distinct Bill_Year_Month order by Bill_Year_Month) into @distinct_rc from Dashboard_Insights_Repeat_Cohorts where Bill_Year_Month between @FY_SATRT_DATE and @FY_END_DATE;
		    select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
			set @loop_var=1;
			set @json_select='';
            set @top_query='';
			select @loop_var,@num_of_rc,@distinct_rc;
			WHILE @loop_var<=@num_of_rc
			do
			SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
            SET @alias_Name = date_format(concat(@selected_rc,'01'),'%b-%y');
            
			set @top_query=concat(@top_query,",MAX(CASE WHEN Bill_Year_Month = '",@selected_rc,"' Then Bills else 0 END) AS `",@alias_Name,"`");
				set @json_select=concat(@json_select,',"',@alias_Name,'",`',@alias_Name,'`');
				set @loop_var=@loop_var+1;
			end while;
			
			
			SELECT @json_select,@top_query;
			set @view_stmt = concat("create or replace  View V_Dashboard_Repeat_Cohorts_Table as
			(select 'Repeat within 7 Days' as 'Transaction Month' 
			",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM', concat(ifnull(Customer_Percent,0),'%') AS 'Bills'  FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'NEW'
			AND AGGREGATION = 'Repeat within 7 Days%'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
            )A)
			UNION
			(select 'Repeat within 15 Days' as 'Transaction Month'
            ",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM', concat(ifnull(Customer_Percent,0),'%') AS 'Bills'  FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'NEW'
			AND AGGREGATION = 'Repeat within 15 Days%'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
            )A)
			UNION
			(select 'Repeat within 30 Days' as 'Transaction Month'
            ",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM', concat(ifnull(Customer_Percent,0),'%') AS 'Bills'  FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'NEW'
			AND AGGREGATION = 'Repeat within 30 Days%'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
            )A)
			UNION
			(select 'Repeat within 60 Days' as 'Transaction Month'
            ",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM', concat(ifnull(Customer_Percent,0),'%') AS 'Bills'  FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'NEW'
			AND AGGREGATION = 'Repeat within 60 Days%'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
            )A)
			UNION
			(select 'Repeat within 90 Days' as 'Transaction Month'
            ",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM', concat(ifnull(Customer_Percent,0),'%') AS 'Bills'  FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'NEW'
			AND AGGREGATION = 'Repeat within 90 Days%'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
            )A)
			UNION
			(select 'Repeat within 180 Days' as 'Transaction Month'
            ",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM', concat(ifnull(Customer_Percent,0),'%') AS 'Bills'  FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'NEW'
			AND AGGREGATION = 'Repeat within 180 Days%'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
            )A)
			UNION
			(select 'Repeat within 365 Days' as 'Transaction Month'
            ",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM', concat(ifnull(Customer_Percent,0),'%') AS 'Bills'  FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'NEW'
			AND AGGREGATION = 'Repeat within 365 Days%'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
            )A)
			;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Transaction Month",`Transaction Month`',@json_select,') )),"]")into @temp_result from V_Dashboard_Repeat_Cohorts_Table;') ;
		END IF;
END IF;
		
	IF IN_AGGREGATION_2 = 'EXISTING'
	THEN 
		IF IN_MEASURE = 'FLOWGRAPH' AND IN_NUMBER_FORMAT = 'NUMBER'
		THEN
			SELECT @date_select,@filter_cond;
			SET @select_query = CASE WHEN IN_NUMBER_FORMAT = 'NUMBER' THEN 'CAST(ifnull(Customer_Count,0) AS CHAR)'
								WHEN IN_NUMBER_FORMAT = 'PERCENT' THEN 'ifnull(Customer_Percent,0)' END;
			set @view_stmt= CONCAT(' create or replace view V_Dashboard_Repeat_Cohorts_Graph as 
							SELECT "Base" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "EXISTING"
                            AND AGGREGATION  = "Base"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "7 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "EXISTING"
                            AND AGGREGATION  = "Repeat within 7 Days"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "15 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "EXISTING"
                            AND AGGREGATION  = "Repeat within 15 Days"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "30 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "EXISTING"
                            AND AGGREGATION  = "Repeat within 30 Days"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "60 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "EXISTING"
                            AND AGGREGATION  = "Repeat within 60 Days"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "90 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "EXISTING"
                            AND AGGREGATION  = "Repeat within 90 Days"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "180 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "EXISTING"
                            AND AGGREGATION  = "Repeat within 180 Days"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "365 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "EXISTING"
                            AND AGGREGATION  = "Repeat within 365 Days"
							AND ',@filter_cond,'
							group by 2
							order by "Name" ASC
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("displayName",`Name`,"value1","","value2",`Value`) )),"]")into @temp_result from V_Dashboard_Repeat_Cohorts_Graph;' ;
													
			
		ELSEIF IN_MEASURE = 'FLOWGRAPH' AND IN_NUMBER_FORMAT = 'PERCENT'
		THEN
			SELECT @date_select,@filter_cond;
			SET @select_query = CASE WHEN IN_NUMBER_FORMAT = 'NUMBER' THEN 'CAST(ifnull(Customer_Count,0) AS CHAR)'
								WHEN IN_NUMBER_FORMAT = 'PERCENT' THEN 'ifnull(Customer_Percent,0)' END;
			set @view_stmt= CONCAT(' create or replace view V_Dashboard_Repeat_Cohorts_Graph as
							SELECT "7 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "EXISTING"
                            AND AGGREGATION  = "Repeat within 7 Days%"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "15 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "EXISTING"
                            AND AGGREGATION  = "Repeat within 15 Days%"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "30 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "EXISTING"
                            AND AGGREGATION  = "Repeat within 30 Days%"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "60 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "EXISTING"
                            AND AGGREGATION  = "Repeat within 60 Days%"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "90 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "EXISTING"
                            AND AGGREGATION  = "Repeat within 90 Days%"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "180 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "EXISTING"
                            AND AGGREGATION  = "Repeat within 180 Days%"
							AND ',@filter_cond,'
							group by 2
							UNION
							SELECT "365 Days" AS "Name", `Bill_Year_Month`,
                            ',@select_query,' as "Value"
							from Dashboard_Insights_Repeat_Cohorts
                            where Measure = "EXISTING"
                            AND AGGREGATION  = "Repeat within 365 Days%"
							AND ',@filter_cond,'
							group by 2
							order by "Name" ASC
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("displayName",`Name`,"value1",`Value`,"value2","") )),"]")into @temp_result from V_Dashboard_Repeat_Cohorts_Graph;' ;

		ELSEIF IN_MEASURE = 'TABLE' AND IN_NUMBER_FORMAT = 'NUMBER'
        THEN
			SELECT group_concat(distinct Bill_Year_Month order by Bill_Year_Month) into @distinct_rc from Dashboard_Insights_Repeat_Cohorts where Bill_Year_Month between @FY_SATRT_DATE and @FY_END_DATE;
		    select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
			set @loop_var=1;
			set @json_select='';
            set @top_query='';
			select @loop_var,@num_of_rc,@distinct_rc;
			WHILE @loop_var<=@num_of_rc
			do
			SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
            SET @alias_Name = date_format(concat(@selected_rc,'01'),'%b-%y');
            
			set @top_query=concat(@top_query,",MAX(CASE WHEN Bill_Year_Month = '",@selected_rc,"' Then Bills else 0 END) AS `",@alias_Name,"`");
				set @json_select=concat(@json_select,',"',@alias_Name,'",`',@alias_Name,'`');
				set @loop_var=@loop_var+1;
			end while;
			
			
			SELECT @json_select,@top_query;
			set @view_stmt = concat("create or replace  View V_Dashboard_Repeat_Cohorts_Table as
			(select 'Base' as 'Transaction Month' 
			",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM', CAST(format(ifnull(Customer_Count,0),',') AS CHAR) AS 'Bills'  FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'EXISTING'
			AND AGGREGATION = 'Base'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
			)A)
			UNION
			(select 'Repeat within 7 Days' as 'Transaction Month' 
			",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM', CAST(format(ifnull(Customer_Count,0),',') AS CHAR) AS 'Bills'  FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'EXISTING'
			AND AGGREGATION = 'Repeat within 7 Days'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
			)A)
			UNION
			(select 'Repeat within 15 Days' as 'Transaction Month'
            ",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM', CAST(format(ifnull(Customer_Count,0),',') AS CHAR) AS 'Bills'  FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'EXISTING'
			AND AGGREGATION = 'Repeat within 15 Days'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
			)A)
			UNION
			(select 'Repeat within 30 Days' as 'Transaction Month'
            ",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM', CAST(format(ifnull(Customer_Count,0),',') AS CHAR) AS 'Bills'  FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'EXISTING'
			AND AGGREGATION = 'Repeat within 30 Days'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
			)A)
			UNION
			(select 'Repeat within 60 Days' as 'Transaction Month'
            ",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM', CAST(format(ifnull(Customer_Count,0),',') AS CHAR) AS 'Bills'  FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'EXISTING'
			AND AGGREGATION = 'Repeat within 60 Days'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
			)A)
			UNION
			(select 'Repeat within 90 Days' as 'Transaction Month'
            ",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM', CAST(format(ifnull(Customer_Count,0),',') AS CHAR) AS 'Bills'  FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'EXISTING'
			AND AGGREGATION = 'Repeat within 90 Days'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
			)A)
			UNION
			(select 'Repeat within 180 Days' as 'Transaction Month'
            ",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM', CAST(format(ifnull(Customer_Count,0),',') AS CHAR) AS 'Bills'  FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'EXISTING'
			AND AGGREGATION = 'Repeat within 180 Days'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
			)A)
			UNION
			(select 'Repeat within 365 Days' as 'Transaction Month'
            ",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM', CAST(format(ifnull(Customer_Count,0),',') AS CHAR) AS 'Bills' FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'EXISTING'
			AND AGGREGATION = 'Repeat within 365 Days'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
			)A)
			;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Transaction Month",`Transaction Month`',@json_select,') )),"]")into @temp_result from V_Dashboard_Repeat_Cohorts_Table;') ;
		
		ELSEIF IN_MEASURE = 'TABLE' AND IN_NUMBER_FORMAT='PERCENT'
        THEN
			SELECT group_concat(distinct Bill_Year_Month order by Bill_Year_Month) into @distinct_rc from Dashboard_Insights_Repeat_Cohorts where Bill_Year_Month between @FY_SATRT_DATE and @FY_END_DATE;
		    select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
			set @loop_var=1;
			set @json_select='';
            set @top_query='';
			select @loop_var,@num_of_rc,@distinct_rc;
			WHILE @loop_var<=@num_of_rc
			do
			SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
            SET @alias_Name = date_format(concat(@selected_rc,'01'),'%b-%y');
            
			set @top_query=concat(@top_query,",MAX(CASE WHEN Bill_Year_Month = '",@selected_rc,"' Then Bills else 0 END) AS `",@alias_Name,"`");
				set @json_select=concat(@json_select,',"',@alias_Name,'",`',@alias_Name,'`');
				set @loop_var=@loop_var+1;
			end while;
			
			
			SELECT @json_select,@top_query;
			set @view_stmt = concat("create or replace  View V_Dashboard_Repeat_Cohorts_Table as
			(select 'Repeat within 7 Days' as 'Transaction Month' 
			",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM', concat(ifnull(Customer_Percent,0),'%') AS 'Bills'  FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'EXISTING'
			AND AGGREGATION = 'Repeat within 7 Days%'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
            )A)
			UNION
			(select 'Repeat within 15 Days' as 'Transaction Month'
            ",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM', concat(ifnull(Customer_Percent,0),'%') AS 'Bills' FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'EXISTING'
			AND AGGREGATION = 'Repeat within 15 Days%'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
            )A)
			UNION
			(select 'Repeat within 30 Days' as 'Transaction Month'
            ",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM', concat(ifnull(Customer_Percent,0),'%') AS 'Bills'  FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'EXISTING'
			AND AGGREGATION = 'Repeat within 30 Days%'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
            )A)
			UNION
			(select 'Repeat within 60 Days' as 'Transaction Month'
            ",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM', concat(ifnull(Customer_Percent,0),'%') AS 'Bills'  FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'EXISTING'
			AND AGGREGATION = 'Repeat within 60 Days%'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
            )A)
			UNION
			(select 'Repeat within 90 Days' as 'Transaction Month'
            ",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM', concat(ifnull(Customer_Percent,0),'%') AS 'Bills'  FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'EXISTING'
			AND AGGREGATION = 'Repeat within 90 Days%'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
            )A)
			UNION
			(select 'Repeat within 180 Days' as 'Transaction Month'
            ",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM', concat(ifnull(Customer_Percent,0),'%') AS 'Bills'  FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'EXISTING'
			AND AGGREGATION = 'Repeat within 180 Days%'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
            )A)
			UNION
			(select 'Repeat within 365 Days' as 'Transaction Month'
            ",@top_query,"
			from 
			(SELECT Bill_Year_Month,date_format(concat(Bill_Year_Month,'01'),'%b') as 'MM', concat(ifnull(Customer_Percent,0),'%') AS 'Bills'  FROM Dashboard_Insights_Repeat_Cohorts
			WHERE Measure  = 'EXISTING'
			AND AGGREGATION = 'Repeat within 365 Days%'
			AND Bill_Year_Month BETWEEN ",@FY_SATRT_DATE," AND ",@FY_END_DATE,"
			GROUP BY Bill_Year_Month
            )A)
			;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Transaction Month",`Transaction Month`',@json_select,') )),"]")into @temp_result from V_Dashboard_Repeat_Cohorts_Table;') ;
		END IF;
		
END IF;
	
	IF IN_SCREEN <> "DOWNLOAD"
    THEN
		select @view_stmt;
		PREPARE statement from @view_stmt;
		Execute statement;
		Deallocate PREPARE statement;
		select @Query_String;
		PREPARE statement from @Query_String;
		Execute statement;
		Deallocate PREPARE statement;	
		SET out_result_json = @temp_result;
	ELSE
		call S_dashboard_performance_download('V_Dashboard_Repeat_Cohorts_Table',@out_result_json1);
		select @out_result_json1 into out_result_json;
    END IF;
	
END$$
DELIMITER ;
