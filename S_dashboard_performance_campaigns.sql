DELIMITER $$
CREATE or replace PROCEDURE `S_dashboard_performance_campaigns`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
    declare IN_FILTER_CURRENTMONTH text;
    declare IN_MEASURE text;
	
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    
    Select IFNULL(max(IF(Config_Name="CALENDAR",Value1,NULL)),max(IF(Config_Name="FISCAL",Value1,NULL))) into @FY_SATRT_DATE
    from UI_Configuration_Global where isActive="true";
    
    Select IFNULL(max(IF(Config_Name="CALENDAR",Value2,NULL)),max(IF(Config_Name="FISCAL",Value2,NULL))) into @FY_END_DATE
    from UI_Configuration_Global where isActive="true";
  
	CASE IN_SCREEN
    WHEN "CAMPAIGNS_SUMMARY" THEN
    BEGIN
    
    SET IN_MEASURE=json_unquote(json_extract(in_request_json,"$.MEASURE"));
	
    SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
    SET @Query_String1=concat('create or replace view V_Dashboard_Performance_Campaign_Summary as  SELECT 
        `a`.`YM` AS `YM`,
        count(distinct `a`.`Theme`) AS `no_of_theme`,
        count(distinct `a`.`Campaign`) AS `no_of_campaigns`,
        count(distinct `Trigger`) AS `no_of_triggers`,               
        round((SUM(CASE WHEN `a`.`Offer` NOT IN ("NA","NO_OFFER","NO OFFER") THEN `a`.`Outreaches` ELSE 0 END)/SUM(`a`.`Outreaches`))*100,1) As `offer_led`,        
	     round((SUM(CASE WHEN `a`.`Personalization` >0 THEN `a`.`Outreaches` ELSE 0 END)/SUM(`a`.`Outreaches`))*100,1) As `personalized`
	 FROM
      `V_Dashboard_Performance_Campaign` `a` where `a`.`YM`="',IN_FILTER_CURRENTMONTH,'"
    GROUP BY `a`.`YM`;');
               select @Query_String1;
                PREPARE statement1 from @Query_String1;
                Execute statement1;
                Deallocate PREPARE statement1; 
				
			    SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("no_of_theme",`no_of_theme`,"no_of_campaigns",`no_of_campaigns`,"no_of_triggers",`no_of_triggers`,"offer_led",concat(round(`offer_led`),"%"),"personalized",concat(round(`personalized`),"%")) )),"]")into @temp_result from V_Dashboard_Performance_Campaign_Summary where `YM`="',IN_FILTER_CURRENTMONTH,'";') ;
			
               select @Query_String;
                PREPARE statement from @Query_String;
                Execute statement;
                Deallocate PREPARE statement; 
                select @temp_result into out_result_json;
                                      
    END;
    WHEN "CAMPAIGNS" THEN
        BEGIN
            SET IN_MEASURE=json_unquote(json_extract(in_request_json,"$.MEASURE"));
	        SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
              SET @Query_String1=concat('create or replace view V_Dashboard_Performance_Campaign_Group as (SELECT (json_object("campaign_theme",p.`campaign_theme`,"no_of_campaigns",p.`no_of_campaigns`,"no_of_triggers",p.`no_of_triggers`,"outreaches_per_trigger",
    p.`outreaches_per_trigger`,"outreaches",p.`outreaches`,"cust_reached",p.`cust_reached`,"Coverage",p.`Coverage`,"campaigns",(SELECT CONCAT("[",(GROUP_CONCAT(json_object("campaign_name",c.`campaign_name`,"no_of_triggers",c.`no_of_triggers`,
    "outreaches_per_trigger",c.`outreaches_per_trigger`,"outreaches",c.`outreaches`,"cust_reached",c.`cust_reached`,"Coverage",c.`Coverage`,"triggers",(SELECT CONCAT("[",(GROUP_CONCAT(json_object("trigger_name",`trigger_name`,"outreaches_per_trigger",
    `outreaches_per_trigger`,"outreaches",`outreaches`,"cust_reached",`cust_reached`,"Coverage",t.`Coverage`) )),"]") from V_Dashboard_Performance_Trigger_Layer t where t.campaign_name =c.campaign_name and t.`YM`="',IN_FILTER_CURRENTMONTH,'")) )),"]")  
    from V_Dashboard_Performance_Campaign_Layer c where c.campaign_theme =p.campaign_theme and c.`YM`="',IN_FILTER_CURRENTMONTH,'")))  as `Campaign_Group`
    from V_Dashboard_Performance_Theme_Layer p  where p.`YM`="',IN_FILTER_CURRENTMONTH,'");');
               select @Query_String1;
                PREPARE statement1 from @Query_String1;
                Execute statement1;
                Deallocate PREPARE statement1; 
      
    SET @Query_String='select concat("[",group_concat(`Campaign_Group`),"]")into @temp_result from V_Dashboard_Performance_Campaign_Group;';
			   select @Query_String;
               PREPARE statement from @Query_String;
                Execute statement;
                Deallocate PREPARE statement; 
                select @temp_result into out_result_json;
                end; 
                
	WHEN "CAMPAIGNS_DOWNLOAD" THEN
        BEGIN   
        SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
           SET @Query_String1=concat('CREATE or REPLACE
    VIEW `V_Dashboard_Performance_Trigger_Layer_Download` AS
    SELECT 
        `a`.`EE_Date` AS `EE_Date`,
        `a`.`Theme` AS `campaign_theme`,
        `a`.`Campaign` AS `Campaign_Name`,
        `a`.`Trigger` AS `trigger_name`,
        ROUND(SUM(`a`.`Outreaches`), 2) AS `Outreaches_Per_Trigger`,
        ROUND(SUM(`a`.`Outreaches`), 0) AS `Outreaches`,
        ROUND(SUM(`a`.`Customer_Reached`), 0) AS `Customer_Reached`,
		CONCAT(ROUND(CASE
                            WHEN `a`.`Total_Customer_Base` = 0 THEN 0
                            ELSE SUM(`a`.`Outreaches`) / `a`.`Total_Customer_Base` * 100
                        END,
                        1),
                "%") AS `Coverage`
    FROM
        `V_Dashboard_Performance_Campaign` `a` where `a`.YM = "',  IN_FILTER_CURRENTMONTH,'" 
       GROUP BY `a`.`YM` , `a`.`Theme` , `a`.`Campaign` , `a`.`Trigger`;');
			   select @Query_String1;
               PREPARE statement1 from @Query_String1;
                Execute statement1;
                Deallocate PREPARE statement1; 
           call S_dashboard_performance_download('V_Dashboard_Performance_Trigger_Layer_Download',@out_result_json1);
		select @out_result_json1 into out_result_json;  
	    END;
	   ELSE
			SET out_result_json=json_object("Status","Not Implemented Yet");
	    END CASE;	
END$$
DELIMITER ;
