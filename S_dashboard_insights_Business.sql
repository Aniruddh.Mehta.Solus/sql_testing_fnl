DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_insights_Business`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    
    
    
     SET @FY_SATRT_DATE = date_format(date_sub(concat(IN_FILTER_CURRENTMONTH,'01'), INTERVAL 11 MONTH),"%Y%m");
	 SET @FY_END_DATE = IN_FILTER_CURRENTMONTH;
	 
	SELECT Value1 into @Currency from UI_Configuration_Global where Config_Name='CURRENCY';
	SELECT @Currency;
    
    IF @Currency = 'INR' THEN SET @CurrencyMeasure = 'L'; SET @CurrencyDenom = '100000'; ELSE SET @CurrencyMeasure = 'Mn'; SET @CurrencyDenom = '1000000';
    END IF;
    SELECT @CurrencyMeasure;
    SELECT @CurrencyDenom;
    
    IF IN_AGGREGATION_1 = 'BY_DAY'
		THEN 
			SET @filter_cond = concat(' YYYYMM =',IN_FILTER_CURRENTMONTH);
            SET @filter_cond2= ' AGGREGATION = "BY_DAY" ';
			SET @date_select = ' date_format(B.`Date`,"%d-%b") AS "Year" ';
	ELSEIF IN_AGGREGATION_1 = 'BY_MONTH'
		THEN 
			SET @filter_cond=concat(" YYYYMM between '",@FY_SATRT_DATE,"' and '",@FY_END_DATE,"'");
			SET @date_select=' date_format(concat(YYYYMM,"01"),"%b-%Y") AS "Year" ';
            SET @filter_cond2= ' AGGREGATION = "BY_MONTH" ';
	ELSEIF IN_AGGREGATION_1 = 'BY_YEAR'
		THEN
			SET @filter_cond = ' 1=1 ';
            SET @date_select = ' YYYY as "Year"';
            SET @filter_cond2 = ' AGGREGATION = "BY_YEAR" ';
	END IF;
    
    
    IF IN_MEASURE = "NO_BILLS" OR IN_MEASURE = "CARDS"
	THEN
		SET @Query_Measure=' (SUM(MonthlyActiveBills)) ';
		SET @nobills_measure=@Query_Measure;
	END IF;
	IF IN_MEASURE = "NO_CUSTOMERS" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' SUM(MonthlyActive) ';
		SET @nocustomers_measure=@Query_Measure;
	END IF;
    IF IN_MEASURE = "REVENUE" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' SUM(MonthlyActiveRevenue) ';
		SET @revenue_measure=@Query_Measure;
	END IF;
    IF IN_MEASURE = "TAGGED_BILLS" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' MonthlyActiveBillsNonTagged ';
		SET @taggedbills_measure=@Query_Measure;
	END IF;
    IF IN_MEASURE = "ABV" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' round(SUM(MonthlyActiveRevenue)/SUM(MonthlyActiveBills),0) ';
		SET @abv_measure=@Query_Measure;
	END IF;
    IF IN_MEASURE = "BILLS_CUST" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' round(SUM(MonthlyActiveBills)/SUM(MonthlyActive),1) ';
		SET @billscust_measure=@Query_Measure;
	END IF;
    IF IN_MEASURE = "VISISTS_CUST" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' round(SUM(MonthlyActiveVisits)/SUM(MonthlyActive),1) ';
		SET @visitscust_measure=@Query_Measure;
	END IF;
    IF IN_MEASURE = "LINEITEMS_CUST" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' round(SUM(MonthlyActiveLineitems)/SUM(MonthlyActive),1) ';
		SET @lineitemscust_measure=@Query_Measure;
	END IF;
    IF IN_MEASURE = "DISCOUNT" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' SUM(MonthlyActiveDiscount) ';
		SET @discount_measure=@Query_Measure;
	END IF;
	
    IF IN_AGGREGATION_2 = "DRILL1" and IN_AGGREGATION_3 <> 'TABLE'
    THEN
		IF IN_AGGREGATION_1 = 'BY_DAY' 
		THEN 
			SET @filter_cond = concat(' YYYYMM =',IN_FILTER_CURRENTMONTH);
            SET @filter_cond2= ' AGGREGATION = "BY_DAY" ';
			SET @date_select = ' date_format(YYYYMMDD,"%d-%b-%Y") AS "Year" ';
            SET @ordering = ' YYYYMMDD ';
		ELSEIF IN_AGGREGATION_1 = 'BY_MONTH' 
		THEN 
			SET @filter_cond=concat(" YYYYMM between '",@FY_SATRT_DATE,"' and '",@FY_END_DATE,"'");
			SET @date_select=' date_format(concat(YYYYMM,"01"),"%d-%b-%Y") AS "Year" ';
            SET @filter_cond2= ' AGGREGATION = "BY_MONTH" ';
            SET @ordering = ' YYYYMM ';
		ELSEIF IN_AGGREGATION_1 = 'BY_YEAR'
		THEN
			SET @filter_cond = ' 1=1 ';
            SET @date_select = ' date_format(concat(YYYY,"0101"),"%d-%b-%Y") as "Year"';
            SET @filter_cond2 = ' AGGREGATION = "BY_YEAR" ';
            SET @ordering = ' YYYYMM ';
		END IF;
		SET IN_MEASURE= concat(IN_MEASURE,IN_AGGREGATION_2);
        SET @top_query = '';
        select group_concat(DISTINCT `Revenue_Center`) into @distinct_rc from Dashboard_Insights_Base where YYYYMM=IN_FILTER_CURRENTMONTH and AGGREGATION = IN_AGGREGATION_1 and Revenue_Center <> '';
        select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
		set @loop_var=1;
		set @json_select='';
		select @loop_var,@num_of_rc,@distinct_rc;
		WHILE @loop_var<=@num_of_rc
		do
        SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
		set @top_query=concat(@top_query,",MAX(CASE WHEN `Revenue_Center` = '",@selected_rc,"' Then pct else 0 END) AS `",@selected_rc,"`");
			set @json_select=concat(@json_select,',"',@selected_rc,'",`',@selected_rc,'`');
			set @loop_var=@loop_var+1;
		end while;
        
		SET @view_stmt=concat('create or replace view Dashboard_business_drill as
		select `Year`',@top_query,' from 
		(select max(',@ordering,') as YM,',@date_select,',`Revenue_Center`, (',@Query_Measure,') pct from Dashboard_Insights_Base a
		where ',@filter_cond,'
        and ',@filter_cond2,'
		group by 2,`Revenue_Center`
        having pct > 0)C
		group by `Year`
        order by YM');
        
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`Year`',@json_select,') )),"]")into @temp_result from Dashboard_business_drill;') ;
	
    END IF;
    
    IF IN_AGGREGATION_2 = "DRILL1" and IN_AGGREGATION_3 = 'TABLE'
    THEN
		IF IN_AGGREGATION_1 = 'BY_DAY' 
		THEN 
			SET @filter_cond = concat(' YYYYMM =',IN_FILTER_CURRENTMONTH);
            SET @filter_cond2= ' AGGREGATION = "BY_DAY" ';
			SET @date_select = ' date_format(YYYYMMDD,"%d-%b") ';
            SET @ordering = 'YYYYMMDD';
		ELSEIF IN_AGGREGATION_1 = 'BY_MONTH' 
		THEN 
			SET @filter_cond=concat(" YYYYMM between '",@FY_SATRT_DATE,"' and '",@FY_END_DATE,"'");
			SET @date_select=' date_format(concat(YYYYMM,"01"),"%b-%Y") ';
            SET @filter_cond2= ' AGGREGATION = "BY_MONTH" ';
            SET @ordering = 'YYYYMM';
		ELSEIF IN_AGGREGATION_1 = 'BY_YEAR'
		THEN
			SET @filter_cond = ' 1=1 ';
            SET @date_select = ' YYYY ';
            SET @filter_cond2 = ' AGGREGATION = "BY_YEAR" ';
            SET @ordering = 'YYYYMM';
		END IF;
        
		SET IN_MEASURE= concat(IN_MEASURE,IN_AGGREGATION_2);
        SET @top_query = '';
        set @sql1 = concat('select group_concat(DISTINCT ',@date_select,') into @distinct_rc from Dashboard_Insights_Base where ',@filter_cond,' and AGGREGATION = "',IN_AGGREGATION_1,'";');
        PREPARE statement from @sql1;
		Execute statement;
		Deallocate PREPARE statement;
        
        select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
		set @loop_var=1;
		set @json_select='';
		select @loop_var,@num_of_rc,@distinct_rc;
		WHILE @loop_var<=@num_of_rc
		do
        SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
		set @top_query=concat(@top_query,",MAX(CASE WHEN `Year` = '",@selected_rc,"' Then pct else 0 END) AS `",@selected_rc,"`");
			set @json_select=concat(@json_select,',"',@selected_rc,'",`',@selected_rc,'`');
			set @loop_var=@loop_var+1;
		end while;
        
		SET @view_stmt=concat('create or replace view Dashboard_business_drill_table as
		select `Revenue_Center`',@top_query,' from 
		(select max(',@ordering,') as YM,',@date_select,' as `Year`,`Revenue_Center`, (',@Query_Measure,') pct from Dashboard_Insights_Base a
		where ',@filter_cond,'
        and ',@filter_cond2,'
		group by 2,`Revenue_Center`
        having pct > 0)C
		group by `Revenue_Center`
        order by YM');
        
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Revenue Center",`Revenue_Center`',@json_select,') )),"]")into @temp_result from Dashboard_business_drill_table;') ;
	
    END IF;
    
     IF IN_MEASURE='CARDS'
    THEN
		select @nobills_measure,IN_FILTER_CURRENTMONTH,@nocustomers_measure,@revenue_measure,@taggedbills_measure,@abv_measure,@billscust_measure,@visitscust_measure,@discount_measure;
		set @view_stmt=concat('create or replace view V_Dashboard_Business_Card as
		(select "Bills" as Card_Name,format(round(',@nobills_measure,',0),",") as `Value1`,"" as `Value2`, "" as `Value3`,"NO_BILLS" as `Measure`  from Dashboard_Insights_Base WHERE YYYYMM = ',IN_FILTER_CURRENTMONTH,' and AGGREGATION = "BY_MONTH")
		Union
		(select "Customers" as Card_Name,format(round(',@nocustomers_measure,',0),",") as `Value1`,"" as `Value2`, "" as `Value3`,"NO_CUSTOMERS" as `Measure`  from Dashboard_Insights_Base WHERE YYYYMM = ',IN_FILTER_CURRENTMONTH,' and AGGREGATION = "BY_MONTH")
		Union
		(select "Revenue" as Card_Name,concat(concat("',@Currency,'"," "),format(round(',@revenue_measure,',0)/',@CurrencyDenom,',","), " ',@CurrencyMeasure,'") as `Value1`,"" as `Value2`, "" as `Value3`,"REVENUE" as `Measure`  from Dashboard_Insights_Base WHERE YYYYMM = ',IN_FILTER_CURRENTMONTH,' and AGGREGATION = "BY_MONTH")
		Union
		(select "NonTagged Bills" as Card_Name,format(round(',@taggedbills_measure,',0),",") as `Value1`,"" as `Value2`, "" as `Value3`,"TAGGED_BILLS" as `Measure`  from Dashboard_Insights_Base WHERE YYYYMM = ',IN_FILTER_CURRENTMONTH,' and AGGREGATION = "BY_MONTH")
		Union
		(select "ABV" as Card_Name,round(',@abv_measure,',0) as `Value1`,"" as `Value2`, "" as `Value3`,"ABV" as `Measure`  from Dashboard_Insights_Base WHERE YYYYMM = ',IN_FILTER_CURRENTMONTH,' and AGGREGATION = "BY_MONTH")
        Union
		(select "Bills/Cust" as Card_Name,round(',@billscust_measure,',2) as `Value1`,"" as `Value2`, "" as `Value3`,"BILLS_CUST" as `Measure`  from Dashboard_Insights_Base WHERE YYYYMM = ',IN_FILTER_CURRENTMONTH,' and AGGREGATION = "BY_MONTH")
        Union
		(select "Visits/Cust" as Card_Name,round(',@visitscust_measure,',2) as `Value1`,"" as `Value2`, "" as `Value3`,"VISISTS_CUST" as `Measure`  from Dashboard_Insights_Base WHERE YYYYMM = ',IN_FILTER_CURRENTMONTH,' and AGGREGATION = "BY_MONTH")
        Union
        (select "Lineitems/Cust" as Card_Name,round(',@lineitemscust_measure,',2) as `Value1`,"" as `Value2`, "" as `Value3`,"LINEITEMS_CUST" as `Measure`  from Dashboard_Insights_Base WHERE YYYYMM = ',IN_FILTER_CURRENTMONTH,' and AGGREGATION = "BY_MONTH")
		');
     
  
        
	SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Card_Name",`Card_Name`,"Value1",`Value1`,"Value2",`Value2`,"Value3",`Value3`,"Measure",`Measure`) )),"]")into @temp_result from V_Dashboard_Business_Card;' ;
    
    ELSEIF IN_MEASURE='TABLE'
	THEN
			SELECT group_concat(distinct YYYYMM order by YYYYMM) into @distinct_rc from Dashboard_Insights_Base where YYYYMM between @FY_SATRT_DATE and @FY_END_DATE;
		    select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
			set @loop_var=1;
			set @json_select='';
            set @top_query='';
			select @loop_var,@num_of_rc,@distinct_rc;
			WHILE @loop_var<=@num_of_rc
			do
			SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
            SET @alias_Name = date_format(concat(@selected_rc,'01'),'%b-%y');
            
			set @top_query=concat(@top_query,",MAX(CASE WHEN YYYYMM = '",@selected_rc,"' Then Bills else 0 END) AS `",@alias_Name,"`");
				set @json_select=concat(@json_select,',"',@alias_Name,'",`',@alias_Name,'`');
				set @loop_var=@loop_var+1;
			end while;
			
			
			SELECT @json_select,@top_query;
			set @view_stmt = concat("create or replace  View V_Dashboard_Business_Table as
			(select 'Bills' as 'Overview' 
			",@top_query,"
			from 
			(select YYYYMM,date_format(concat(YYYYMM,'01'),'%b') as 'MM',format(round(SUM(MonthlyActiveBills),0),',') as 'Bills' from Dashboard_Insights_Base
			where YYYYMM between ",@FY_SATRT_DATE," and ",@FY_END_DATE,"
            and AGGREGATION = 'BY_MONTH'
			group by YYYYMM
			)A)
			UNION
			(select 'Customers' as 'Overview'
            ",@top_query,"
			from 
			(select YYYYMM,date_format(concat(YYYYMM,'01'),'%b') as 'MM',format(round(SUM(MonthlyActive),0),',') as 'Bills' from Dashboard_Insights_Base
			where YYYYMM between ",@FY_SATRT_DATE," and ",@FY_END_DATE,"
            and AGGREGATION = 'BY_MONTH'
			group by YYYYMM
			)A)
			UNION
			(select concat('Revenue',' (",@CurrencyMeasure,")') as 'Overview'
            ",@top_query,"
			from 
			(select YYYYMM,date_format(concat(YYYYMM,'01'),'%b') as 'MM',format(round(SUM(MonthlyActiveRevenue)/",@CurrencyDenom,",0),',') as 'Bills' from Dashboard_Insights_Base
			where YYYYMM between ",@FY_SATRT_DATE," and ",@FY_END_DATE,"
            and AGGREGATION = 'BY_MONTH'
			group by YYYYMM
			)A)
			UNION
			(select 'ABV' as 'Overview'
            ",@top_query,"
			from 
			(select YYYYMM,date_format(concat(YYYYMM,'01'),'%b') as 'MM',format(round(SUM(MonthlyActiveRevenue)/SUM(MonthlyActiveBills),0),',') as 'Bills' from Dashboard_Insights_Base
			where YYYYMM between ",@FY_SATRT_DATE," and ",@FY_END_DATE,"
            and AGGREGATION = 'BY_MONTH'
			group by YYYYMM
			)A)
			UNION
			(select 'Bills/Cust' as 'Overview'
            ",@top_query,"
			from 
			(select YYYYMM,date_format(concat(YYYYMM,'01'),'%b') as 'MM',round(SUM(MonthlyActiveBills)/SUM(MonthlyActive),2) as 'Bills' from Dashboard_Insights_Base
			where YYYYMM between ",@FY_SATRT_DATE," and ",@FY_END_DATE,"
            and AGGREGATION = 'BY_MONTH'
			group by YYYYMM
			)A)
			UNION
			(select 'Visits/Cust' as 'Overview'
            ",@top_query,"
			from 
			(select YYYYMM,date_format(concat(YYYYMM,'01'),'%b') as 'MM',round(SUM(MonthlyActiveVisits)/SUM(MonthlyActive),2) as 'Bills' from Dashboard_Insights_Base
			where YYYYMM between ",@FY_SATRT_DATE," and ",@FY_END_DATE,"
            and AGGREGATION = 'BY_MONTH'
			group by YYYYMM
			)A)
			UNION
			(select 'Lineitems/Cust' as 'Overview'
            ",@top_query,"
			from 
			(select YYYYMM,date_format(concat(YYYYMM,'01'),'%b') as 'MM',round(SUM(MonthlyActiveLineitems)/SUM(MonthlyActive),1) as 'Bills' from Dashboard_Insights_Base
			where YYYYMM between ",@FY_SATRT_DATE," and ",@FY_END_DATE,"
            and AGGREGATION = 'BY_MONTH'
			group by YYYYMM
			)A)
            UNION
			(select 'NonTagged Bills' as 'Overview'
            ",@top_query,"
			from 
			(select YYYYMM,date_format(concat(YYYYMM,'01'),'%b') as 'MM',format(round(MonthlyActiveBillsNonTagged,0),',') as 'Bills' from Dashboard_Insights_Base
			where YYYYMM between ",@FY_SATRT_DATE," and ",@FY_END_DATE,"
            and AGGREGATION = 'BY_MONTH'
			group by YYYYMM
			)A)
			;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Overview",`Overview`',@json_select,') )),"]")into @temp_result from V_Dashboard_Business_Table;') ;
	
        ELSEIF IN_AGGREGATION_2 = '' AND IN_AGGREGATION_1 <> 'BY_DAY'
		THEN
		select @date_select,@Query_Measure,@filter_cond;
		SET @view_stmt=concat('CREATE OR REPLACE VIEW V_Dashboard_Business_Graph AS
						SELECT ',@date_select,',
                         ',@Query_Measure,' as "Value"
                         from Dashboard_Insights_Base
                         WHERE ',@filter_cond,'
                         and ',@filter_cond2,'
                         GROUP BY 1
                         ORDER BY YYYYMM
                        ');
		SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`Year`,"Value",`Value`) )),"]")into @temp_result from V_Dashboard_Business_Graph;' ;
        
        ELSEIF IN_AGGREGATION_2 = '' AND IN_AGGREGATION_1 = 'BY_DAY'
		THEN
		select @date_select,@Query_Measure,@filter_cond;
		SET @view_stmt=concat('CREATE OR REPLACE VIEW V_Dashboard_Business_Graph AS
						SELECT 
    ',@date_select,', IFNULL(A.`Value`, 0) as `Value`
FROM
    (SELECT 
        YYYYMMDD AS `Date`,
         ',@Query_Measure,'  as "Value"
    FROM
        Dashboard_Insights_Base
    WHERE ',@filter_cond,'
	and  ',@filter_cond2,' 
    GROUP BY 1) A
        RIGHT JOIN
    (SELECT 
        date_format(EE_Date,"%Y%m%d") as `Date`
    FROM
        tablenew
    WHERE
       date_format(EE_Date,"%Y%m") = ',IN_FILTER_CURRENTMONTH,') B 
       ON B.`Date` = A.`Date`
                        ');
		SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`Year`,"Value",`Value`) )),"]")into @temp_result from V_Dashboard_Business_Graph;' ;
		END IF;
	
	
	
	IF IN_SCREEN <> "DOWNLOAD"
    THEN
		select @view_stmt;
		PREPARE statement from @view_stmt;
		Execute statement;
		Deallocate PREPARE statement;
		select @Query_String;
		PREPARE statement from @Query_String;
		Execute statement;
		Deallocate PREPARE statement; 
		SET out_result_json = @temp_result;
	ELSE
		call S_dashboard_performance_download('V_Dashboard_Business_Table',@out_result_json1);
		select @out_result_json1 into out_result_json;
    END IF;
	
END$$
DELIMITER ;
