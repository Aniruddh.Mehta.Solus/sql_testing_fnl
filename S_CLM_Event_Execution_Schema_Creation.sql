
CREATE OR REPLACE PROCEDURE `S_CLM_Event_Execution_Schema_Creation`()
BEGIN
DROP TABLE Event_Execution;
CREATE TABLE `Event_Execution` (
  `Event_Execution_ID` char(50) NOT NULL,
  `Event_ID` int(11) NOT NULL,
  `Event_Execution_Date_ID` int(11) NOT NULL,
  `Customer_Id` bigint(20) NOT NULL,
  `Is_Target` char(1) DEFAULT NULL,
  `Exclusion_Filter_ID` int(11) DEFAULT NULL,
  `Communication_Template_ID` int(11) DEFAULT NULL,
  `Communication_Channel` varchar(50) DEFAULT NULL,
  `Message` text DEFAULT NULL,
  `PN_Message` text DEFAULT NULL,
  `Email_Message` text DEFAULT NULL,
  `In_Control_Group` char(1) DEFAULT NULL,
  `In_Event_Control_Group` varchar(1) DEFAULT NULL,
  `LT_Control_Covers_Response_Days` varchar(8) DEFAULT 'N',
  `ST_Control_Covers_Response_Days` varchar(8) DEFAULT 'N',
  `Offer_Code` varchar(20) DEFAULT NULL,
  `Campaign_Key` varchar(50) DEFAULT NULL,
  `Segment_Id` bigint(20) DEFAULT -1,
  `Revenue_Center` varchar(64) DEFAULT '-1',
  `Recommendation_Product_Id_1` bigint(20) DEFAULT -1,
  `Recommendation_Product_Id_2` bigint(20) DEFAULT -1,
  `Recommendation_Product_Id_3` bigint(20) DEFAULT -1,
  `Recommendation_Product_Id_4` bigint(20) DEFAULT -1,
  `Recommendation_Product_Id_5` bigint(20) DEFAULT -1,
  `Recommendation_Product_Id_6` bigint(20) DEFAULT -1,
  `Run_Mode` varchar(10) DEFAULT NULL,
  `Microsite_URL` varchar(256) DEFAULT '-1',
  NumRecos bigint(2),
  NumCust  bigint(2),
  RecoPField1 varchar(128),
  RecoPField2 varchar(128),
  `Created_Date` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`Event_Execution_ID`),
  KEY `Event_Execution_Date_ID` (`Event_Execution_Date_ID`),
  KEY `Event_ID` (`Event_ID`),
  KEY `Communication_Template_ID` (`Communication_Template_ID`),
  KEY `Customer_Id` (`Customer_Id`),
  KEY `In_Control_Group` (`In_Control_Group`),
  KEY `In_Event_Control_Group` (`In_Event_Control_Group`),
  KEY `Communication_Channel` (`Communication_Channel`),
  KEY `Performance_Index` (`Customer_Id`,`Event_ID`,`Is_Target`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE Event_Execution_Rejected;
CREATE TABLE Event_Execution_Rejected like Event_Execution;
DROP TABLE Event_Execution_Current_Run;
CREATE  TABLE Event_Execution_Current_Run like Event_Execution;

CREATE 
  or REPLACE
VIEW `Customer_Recos_View` AS
    (SELECT 
        `EE`.`Customer_Id` AS `Customer_Id_2`,
        `R1CPM`.`Product_Genome` AS `Reco1_Genome`,
        `R2CPM`.`Product_Genome` AS `Reco2_Genome`,
        `R3CPM`.`Product_Genome` AS `Reco3_Genome`,
        `R4CPM`.`Product_Genome` AS `Reco4_Genome`,
        `R5CPM`.`Product_Genome` AS `Reco5_Genome`,
        `R6CPM`.`Product_Genome` AS `Reco6_Genome`,
        `R1CPM`.`Product_Name` AS `Reco1_ProductName`,
        `R2CPM`.`Product_Name` AS `Reco2_ProductName`,
        `R3CPM`.`Product_Name` AS `Reco3_ProductName`,
        `R4CPM`.`Product_Name` AS `Reco4_ProductName`,
        `R5CPM`.`Product_Name` AS `Reco5_ProductName`,
        `R6CPM`.`Product_Name` AS `Reco6_ProductName`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R1CPM`.`Product_LOV_Id`) AS `Reco1_ProductCommName`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R2CPM`.`Product_LOV_Id`) AS `Reco2_ProductCommName`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R3CPM`.`Product_LOV_Id`) AS `Reco3_ProductCommName`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R4CPM`.`Product_LOV_Id`) AS `Reco4_ProductCommName`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R5CPM`.`Product_LOV_Id`) AS `Reco5_ProductCommName`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R6CPM`.`Product_LOV_Id`) AS `Reco6_ProductCommName`,
        `R1CPM`.`Product_URL` AS `Reco1_ProductURL`,
        `R2CPM`.`Product_URL` AS `Reco2_ProductURL`,
        `R3CPM`.`Product_URL` AS `Reco3_ProductURL`,
        `R4CPM`.`Product_URL` AS `Reco4_ProductURL`,
        `R5CPM`.`Product_URL` AS `Reco5_ProductURL`,
        `R6CPM`.`Product_URL` AS `Reco6_ProductURL`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(`R1CPM`.`Product_Genome`, '____'),
                        '____',
                        1),
                '____',
                - 1) AS `Reco1_GenomeAttr1`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(`R1CPM`.`Product_Genome`, '____'),
                        '____',
                        2),
                '____',
                - 1) AS `Reco1_GenomeAttr2`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(`R1CPM`.`Product_Genome`, '____'),
                        '____',
                        3),
                '____',
                - 1) AS `Reco1_GenomeAttr3`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(`R1CPM`.`Product_Genome`, '____'),
                        '____',
                        4),
                '____',
                - 1) AS `Reco1_GenomeAttr4`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(`R1CPM`.`Product_Genome`, '____'),
                        '____',
                        5),
                '____',
                - 1) AS `Reco1_GenomeAttr5`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(`R1CPM`.`Product_Genome`, '____'),
                        '____',
                        6),
                '____',
                - 1) AS `Reco1_GenomeAttr6`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(`R2CPM`.`Product_Genome`, '____'),
                        '____',
                        1),
                '____',
                - 1) AS `Reco2_GenomeAttr1`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(`R2CPM`.`Product_Genome`, '____'),
                        '____',
                        2),
                '____',
                - 1) AS `Reco2_GenomeAttr2`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(`R2CPM`.`Product_Genome`, '____'),
                        '____',
                        3),
                '____',
                - 1) AS `Reco2_GenomeAttr3`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(`R2CPM`.`Product_Genome`, '____'),
                        '____',
                        4),
                '____',
                - 1) AS `Reco2_GenomeAttr4`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(`R2CPM`.`Product_Genome`, '____'),
                        '____',
                        5),
                '____',
                - 1) AS `Reco2_GenomeAttr5`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(`R2CPM`.`Product_Genome`, '____'),
                        '____',
                        6),
                '____',
                - 1) AS `Reco2_GenomeAttr6`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(`R3CPM`.`Product_Genome`, '____'),
                        '____',
                        1),
                '____',
                - 1) AS `Reco3_GenomeAttr1`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(`R3CPM`.`Product_Genome`, '____'),
                        '____',
                        2),
                '____',
                - 1) AS `Reco3_GenomeAttr2`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(`R3CPM`.`Product_Genome`, '____'),
                        '____',
                        3),
                '____',
                - 1) AS `Reco3_GenomeAttr3`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(`R3CPM`.`Product_Genome`, '____'),
                        '____',
                        4),
                '____',
                - 1) AS `Reco3_GenomeAttr4`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(`R3CPM`.`Product_Genome`, '____'),
                        '____',
                        5),
                '____',
                - 1) AS `Reco3_GenomeAttr5`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(`R3CPM`.`Product_Genome`, '____'),
                        '____',
                        6),
                '____',
                - 1) AS `Reco3_GenomeAttr6`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(`R4CPM`.`Product_Genome`, '____'),
                        '____',
                        1),
                '____',
                - 1) AS `Reco4_GenomeAttr1`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(`R4CPM`.`Product_Genome`, '____'),
                        '____',
                        2),
                '____',
                - 1) AS `Reco4_GenomeAttr2`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(`R4CPM`.`Product_Genome`, '____'),
                        '____',
                        3),
                '____',
                - 1) AS `Reco4_GenomeAttr3`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(`R4CPM`.`Product_Genome`, '____'),
                        '____',
                        4),
                '____',
                - 1) AS `Reco4_GenomeAttr4`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(`R4CPM`.`Product_Genome`, '____'),
                        '____',
                        5),
                '____',
                - 1) AS `Reco4_GenomeAttr5`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(`R4CPM`.`Product_Genome`, '____'),
                        '____',
                        6),
                '____',
                - 1) AS `Reco4_GenomeAttr6`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(`R5CPM`.`Product_Genome`, '____'),
                        '____',
                        1),
                '____',
                - 1) AS `Reco5_GenomeAttr1`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(`R5CPM`.`Product_Genome`, '____'),
                        '____',
                        2),
                '____',
                - 1) AS `Reco5_GenomeAttr2`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(`R5CPM`.`Product_Genome`, '____'),
                        '____',
                        3),
                '____',
                - 1) AS `Reco5_GenomeAttr3`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(`R5CPM`.`Product_Genome`, '____'),
                        '____',
                        4),
                '____',
                - 1) AS `Reco5_GenomeAttr4`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(`R5CPM`.`Product_Genome`, '____'),
                        '____',
                        5),
                '____',
                - 1) AS `Reco5_GenomeAttr5`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(`R5CPM`.`Product_Genome`, '____'),
                        '____',
                        6),
                '____',
                - 1) AS `Reco5_GenomeAttr6`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(`R6CPM`.`Product_Genome`, '____'),
                        '____',
                        1),
                '____',
                - 1) AS `Reco6_GenomeAttr1`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(`R6CPM`.`Product_Genome`, '____'),
                        '____',
                        2),
                '____',
                - 1) AS `Reco6_GenomeAttr2`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(`R6CPM`.`Product_Genome`, '____'),
                        '____',
                        3),
                '____',
                - 1) AS `Reco6_GenomeAttr3`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(`R6CPM`.`Product_Genome`, '____'),
                        '____',
                        4),
                '____',
                - 1) AS `Reco6_GenomeAttr4`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(`R6CPM`.`Product_Genome`, '____'),
                        '____',
                        5),
                '____',
                - 1) AS `Reco6_GenomeAttr5`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(`R6CPM`.`Product_Genome`, '____'),
                        '____',
                        6),
                '____',
                - 1) AS `Reco6_GenomeAttr6`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R1CPM`.`Brand_LOV_Id`) AS `Reco1_Brand`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R1CPM`.`Div_LOV_Id`) AS `Reco1_Div`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R1CPM`.`Dep_LOV_Id`) AS `Reco1_Dep`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R1CPM`.`Cat_LOV_Id`) AS `Reco1_Cat`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R1CPM`.`Season_LOV_Id`) AS `Reco1_Season`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R1CPM`.`BU_LOV_Id`) AS `Reco1_BU`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R1CPM`.`Section_LOV_Id`) AS `Reco1_Section`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R1CPM`.`Manuf_LOV_Id`) AS `Reco1_Manuf`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R1CPM`.`Custom_Tag1_LOV_Id`) AS `Reco1_Custom_Tag1`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R1CPM`.`Custom_Tag2_LOV_Id`) AS `Reco1_Custom_Tag2`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R1CPM`.`Custom_Tag3_LOV_Id`) AS `Reco1_Custom_Tag3`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R1CPM`.`Custom_Tag4_LOV_Id`) AS `Reco1_Custom_Tag4`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R1CPM`.`Image_LOV_Id`) AS `Reco1_Image_Link`,
		 (SELECT 
               so2_code
            FROM
                `CLM_so2_Product_Master`
            WHERE
                `CLM_so2_Product_Master`.`Product_Id` = `R1CPM`.`Product_Id`) AS `Reco1_PDP`,		
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R2CPM`.`Brand_LOV_Id`) AS `Reco2_Brand`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R2CPM`.`Div_LOV_Id`) AS `Reco2_Div`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R2CPM`.`Dep_LOV_Id`) AS `Reco2_Dep`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R2CPM`.`Cat_LOV_Id`) AS `Reco2_Cat`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R2CPM`.`Season_LOV_Id`) AS `Reco2_Season`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R2CPM`.`BU_LOV_Id`) AS `Reco2_BU`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R2CPM`.`Section_LOV_Id`) AS `Reco2_Section`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R2CPM`.`Manuf_LOV_Id`) AS `Reco2_Manuf`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R2CPM`.`Custom_Tag1_LOV_Id`) AS `Reco2_Custom_Tag1`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R2CPM`.`Custom_Tag2_LOV_Id`) AS `Reco2_Custom_Tag2`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R2CPM`.`Custom_Tag3_LOV_Id`) AS `Reco2_Custom_Tag3`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R2CPM`.`Custom_Tag4_LOV_Id`) AS `Reco2_Custom_Tag4`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R2CPM`.`Image_LOV_Id`) AS `Reco2_Image_Link`,
		
		(SELECT 
               so2_code
            FROM
                `CLM_so2_Product_Master`
            WHERE
                `CLM_so2_Product_Master`.`Product_Id` = `R2CPM`.`Product_Id`) AS `Reco2_PDP`,	
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R3CPM`.`Brand_LOV_Id`) AS `Reco3_Brand`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R3CPM`.`Div_LOV_Id`) AS `Reco3_Div`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R3CPM`.`Dep_LOV_Id`) AS `Reco3_Dep`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R3CPM`.`Cat_LOV_Id`) AS `Reco3_Cat`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R3CPM`.`Season_LOV_Id`) AS `Reco3_Season`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R3CPM`.`BU_LOV_Id`) AS `Reco3_BU`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R3CPM`.`Section_LOV_Id`) AS `Reco3_Section`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R3CPM`.`Manuf_LOV_Id`) AS `Reco3_Manuf`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R3CPM`.`Custom_Tag1_LOV_Id`) AS `Reco3_Custom_Tag1`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R3CPM`.`Custom_Tag2_LOV_Id`) AS `Reco3_Custom_Tag2`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R3CPM`.`Custom_Tag3_LOV_Id`) AS `Reco3_Custom_Tag3`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R3CPM`.`Custom_Tag4_LOV_Id`) AS `Reco3_Custom_Tag4`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R3CPM`.`Image_LOV_Id`) AS `Reco3_Image_Link`,
		
		(SELECT 
               so2_code
            FROM
                `CLM_so2_Product_Master`
            WHERE
                `CLM_so2_Product_Master`.`Product_Id` = `R3CPM`.`Product_Id`) AS `Reco3_PDP`,			
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R4CPM`.`Brand_LOV_Id`) AS `Reco4_Brand`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R4CPM`.`Div_LOV_Id`) AS `Reco4_Div`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R4CPM`.`Dep_LOV_Id`) AS `Reco4_Dep`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R4CPM`.`Cat_LOV_Id`) AS `Reco4_Cat`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R4CPM`.`Season_LOV_Id`) AS `Reco4_Season`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R4CPM`.`BU_LOV_Id`) AS `Reco4_BU`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R4CPM`.`Section_LOV_Id`) AS `Reco4_Section`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R4CPM`.`Manuf_LOV_Id`) AS `Reco4_Manuf`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R4CPM`.`Custom_Tag1_LOV_Id`) AS `Reco4_Custom_Tag1`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R4CPM`.`Custom_Tag2_LOV_Id`) AS `Reco4_Custom_Tag2`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R4CPM`.`Custom_Tag3_LOV_Id`) AS `Reco4_Custom_Tag3`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R4CPM`.`Custom_Tag4_LOV_Id`) AS `Reco4_Custom_Tag4`,
		
		(SELECT 
               so2_code
            FROM
                `CLM_so2_Product_Master`
            WHERE
                `CLM_so2_Product_Master`.`Product_Id` = `R4CPM`.`Product_Id`) AS `Reco4_PDP`,			
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R4CPM`.`Image_LOV_Id`) AS `Reco4_Image_Link`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R5CPM`.`Brand_LOV_Id`) AS `Reco5_Brand`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R5CPM`.`Div_LOV_Id`) AS `Reco5_Div`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R5CPM`.`Dep_LOV_Id`) AS `Reco5_Dep`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R5CPM`.`Cat_LOV_Id`) AS `Reco5_Cat`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R5CPM`.`Season_LOV_Id`) AS `Reco5_Season`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R5CPM`.`BU_LOV_Id`) AS `Reco5_BU`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R5CPM`.`Section_LOV_Id`) AS `Reco5_Section`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R5CPM`.`Manuf_LOV_Id`) AS `Reco5_Manuf`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R5CPM`.`Custom_Tag1_LOV_Id`) AS `Reco5_Custom_Tag1`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R5CPM`.`Custom_Tag2_LOV_Id`) AS `Reco5_Custom_Tag2`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R5CPM`.`Custom_Tag3_LOV_Id`) AS `Reco5_Custom_Tag3`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R5CPM`.`Custom_Tag4_LOV_Id`) AS `Reco5_Custom_Tag4`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R5CPM`.`Image_LOV_Id`) AS `Reco5_Image_Link`,
		(SELECT 
               so2_code
            FROM
                `CLM_so2_Product_Master`
            WHERE
                `CLM_so2_Product_Master`.`Product_Id` = `R5CPM`.`Product_Id`) AS `Reco5_PDP`,			
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
               `CDM_LOV_Master`.`LOV_Id` = `R6CPM`.`Brand_LOV_Id`) AS `Reco6_Brand`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R6CPM`.`Div_LOV_Id`) AS `Reco6_Div`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
               `CDM_LOV_Master`.`LOV_Id` = `R6CPM`.`Dep_LOV_Id`) AS `Reco6_Dep`,
        (SELECT 
             `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R6CPM`.`Cat_LOV_Id`) AS `Reco6_Cat`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R6CPM`.`Season_LOV_Id`) AS `Reco6_Season`,
        (SELECT 
              `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
               `CDM_LOV_Master`.`LOV_Id` = `R6CPM`.`BU_LOV_Id`) AS `Reco6_BU`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R6CPM`.`Section_LOV_Id`) AS `Reco6_Section`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
               `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R6CPM`.`Manuf_LOV_Id`) AS `Reco6_Manuf`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
               `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R6CPM`.`Custom_Tag1_LOV_Id`) AS `Reco6_Custom_Tag1`,
        (SELECT 
               `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
               `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R6CPM`.`Custom_Tag2_LOV_Id`) AS `Reco6_Custom_Tag2`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `R6CPM`.`Custom_Tag3_LOV_Id`) AS `Reco6_Custom_Tag3`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
               `CDM_LOV_Master`
            WHERE
               `CDM_LOV_Master`.`LOV_Id` = `R6CPM`.`Custom_Tag4_LOV_Id`) AS `Reco6_Custom_Tag4`,
        (SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
               `CDM_LOV_Master`.`LOV_Id` = `R6CPM`.`Image_LOV_Id`) AS `Reco6_Image_Link`,
			   
		(SELECT 
               so2_code
            FROM
                `CLM_so2_Product_Master`
            WHERE
                `CLM_so2_Product_Master`.`Product_Id` = `R6CPM`.`Product_Id`) AS `Reco6_PDP`	   
    FROM
        ((((((`Event_Execution` `EE`
        LEFT JOIN `CDM_Product_Master` `R1CPM` ON (`EE`.`Recommendation_Product_Id_1` = `R1CPM`.`Product_Id`))
        LEFT JOIN `CDM_Product_Master` `R2CPM` ON (`EE`.`Recommendation_Product_Id_2` = `R2CPM`.`Product_Id`))
        LEFT JOIN `CDM_Product_Master` `R3CPM` ON (`EE`.`Recommendation_Product_Id_3` = `R3CPM`.`Product_Id`))
        LEFT JOIN `CDM_Product_Master` `R4CPM` ON (`EE`.`Recommendation_Product_Id_4` = `R4CPM`.`Product_Id`))
        LEFT JOIN `CDM_Product_Master` `R5CPM` ON (`EE`.`Recommendation_Product_Id_5` = `R5CPM`.`Product_Id`))
        LEFT JOIN `CDM_Product_Master` `R6CPM` ON (`EE`.`Recommendation_Product_Id_6` = `R6CPM`.`Product_Id`))
	
		
		);

CALL S_CLM_Create_View();

END

