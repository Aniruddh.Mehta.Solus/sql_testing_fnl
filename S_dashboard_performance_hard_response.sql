DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_performance_hard_response`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    
    
    -- select * from Communication_Template;
    
   --  select SUBSTRING(Hard_Recommendation)
    
    CREATE OR REPLACE VIEW V_Dashboard_CLM_Event_Month_Hard_resp AS
   select CASE WHEN datediff(End_Date_ID,Start_Date_ID) < 32 THEN 'GTM' else 'CLM' END AS `Category`,b.Name as `Trigger`, b.CampaignName as `CampaignName`,Theme,
   concat(substring_Index(substring_index(Hard_Recommendation,"::",1),"::",-1),CASE WHEN substring_Index(substring_index(Hard_Recommendation,"|",2),"|",-1) = Hard_Recommendation THEN '' ELSE concat(' AND ',substring_Index(substring_index(substring_Index(substring_index(Hard_Recommendation,"|",2),"|",-1),"::",1),"::",-1)) END) AS 'Recommended',
   Event_Execution_Date_Id,YM,EE_Date,Event_ID, Target_Base,Control_Base,Target_Delivered,Target_Responder,Target_Revenue,Hard_Responder_Target,Hard_Revenue_Target from CLM_Event_Dashboard a join Event_Master b on a.Event_ID = b.ID
   join Communication_Template c on c.ID=b.Communication_Template_Id
   where c.Hard_Recommendation <> 'NA';
   
   /*IF IN_AGGREGATION_2 = 'CG'
   THEN
		SET @cg_clause='100';
	ELSE
		SET @cg_clause='0';
	END IF;
		

   
   IF IN_AGGREGATION_1 = 'Category'
   THEN 
		SET @select_query='Category';
        set @group_cond='1';
        set @json_select = '"Category",`Category`';
        SET @attr = 'Category';
        
   END IF;
   
   IF IN_AGGREGATION_1 = 'Theme'
   THEN 
		SET @select_query='Category,Theme';
        set @group_cond='1,2';
        set @json_select = '"Category",`Category`,"Theme",`Theme`';
        SET @attr = 'Theme';
   END IF;
   
   IF IN_AGGREGATION_1 = 'Campaign'
   THEN 
		SET @select_query='Category,Theme,CampaignName as `Campaign`';
        set @group_cond='1,2,3';
        set @json_select = '"Category",`Category`,"Theme",`Theme`,"Campaign",`Campaign`';
        SET @attr = 'CampaignName';
   END IF;
   
   IF IN_AGGREGATION_1 = 'Trigger'
   THEN 
		SET @select_query='Category,Theme,CampaignName AS `Campaign`,`Trigger`';
        set @group_cond='1,2,3,4';
        set @json_select = '"Category",`Category`,"Theme",`Theme`,"Campaign",`Campaign`,"Trigger",`Trigger`';
        SET @attr = '`Trigger`';
   END IF;
   */
   CREATE OR REPLACE VIEW V_Dashboard_Month_Hard_Resp_Temp
   as
   select Category,
   Theme,
   CampaignName,
   `Trigger`,
   `Recommended`,
   YM,
   Event_Execution_Date_Id,
   EE_Date,
   Event_Id,
   SUM(`Target_Base`) AS Target_Base,
   SUM(`Target_Delivered`) AS Target_Delivered,
   SUM(`Control_Base`) AS Control_Base,
   SUM(`Target_Responder`) AS Target_Responder,
   SUM(Target_Revenue) AS Target_Revenue,
   MAX(Hard_Responder_Target) AS Hard_Responder_Target,
   MAX(Hard_Revenue_Target) AS Hard_Revenue_Target
   from V_Dashboard_CLM_Event_Month_Hard_resp
   group by Event_Id,Event_Execution_Date_Id;
   
   
   IF IN_MEASURE = 'TABLE'
   THEN
   set @view_stmt= concat('CREATE OR REPLACE VIEW V_Dashboard_Month_Hard_Response
					AS
					(SELECT `Trigger` as "Campaign",
                    SUM(`Target_Base`) as `TG Base`,
					SUM(`Target_Delivered`) AS `TG Delivered`,
                    round((SUM(`Target_Delivered`)/SUM(`Target_Base`))*100,0) AS `Delivered%`,
                    SUM(`Control_Base`) AS `CG Base`,
                    `Recommended`,
					concat(round((CASE WHEN SUM(`Target_Delivered`) <=0 THEN 0 ELSE (SUM(`Target_Responder`)/SUM(`Target_Delivered`))*100 END),2),"%") AS `Any Response`,
                    SUM(Target_Revenue) AS `Any Revenue`,
                    concat(round((CASE WHEN SUM(`Target_Delivered`) <=0 THEN 0 ELSE (SUM(`Hard_Responder_Target`)/SUM(`Target_Delivered`))*100 END),2),"%") AS `Hard Response`,
                    SUM(Hard_Revenue_Target) AS `Hard Revenue`
					from V_Dashboard_Month_Hard_Resp_Temp where YM=',IN_FILTER_CURRENTMONTH,'
					GROUP BY 1
                    having SUM(`Target_Base`) > 0
                     )');
                     
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Campaign",`Campaign`,"Recommended",`Recommended`,"TG Base",CAST(format(ifnull(`TG Base`,0),",") AS CHAR),"TG Delivered",format(ifnull(`TG Delivered`,0),","),"Delivered%",concat(format(ifnull(`Delivered%`,0),","),"%"),"CG Base",format(ifnull(`CG Base`,0),","),"Any Response",ifnull(`Any Response`,0),"Hard Response",ifnull(`Hard Response`,0),"Any Revenue",format(`Any Revenue`,","),"Hard Revenue",format(ifnull(`Hard Revenue`,0),",")) )),"]")into @temp_result from V_Dashboard_Month_Hard_Response ',';') ;
	END IF;
           
   
	IF IN_SCREEN <> "DOWNLOAD"
    THEN
	select @view_stmt;
	PREPARE statement from @view_stmt;
	Execute statement;
	Deallocate PREPARE statement;
    
    
	select @Query_String;
	PREPARE statement from @Query_String;
	Execute statement;
	Deallocate PREPARE statement; 
    SET out_result_json = @temp_result;
    END IF;
    
END$$
DELIMITER ;
