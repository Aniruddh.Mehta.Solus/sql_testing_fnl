DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_UI_Campaign_List`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_MEASURE text; 
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare IN_AGGREGATION_4 text;
	declare IN_AGGREGATION_5 text;
	declare IN_AGGREGATION_6 text;
	declare IN_AGGREGATION_7 text;
	declare IN_AGGREGATION_8 text;
	declare IN_AGGREGATION_9 text;
	declare IN_AGGREGATION_10 text;
	declare Filter_Date text;
	declare Filter_Trigger_Name text;
	Declare InputJson Json;
	Declare UIEvent_Id text DEFAULT -1;
	Declare Display_Formate TEXT Default "Message";
	Declare Sorting_Order Text;

	
	

	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1")); 
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_AGGREGATION_4 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_4"));
	SET IN_AGGREGATION_5 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_5"));
	SET IN_AGGREGATION_6 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_6"));
	SET IN_AGGREGATION_7 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_7"));
	SET IN_AGGREGATION_8 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_8"));
	SET IN_AGGREGATION_9 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_9"));
	SET IN_AGGREGATION_10 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_10"));
	SET Filter_Date = json_unquote(json_extract(in_request_json,"$.DATE_FILTER"));
	SET Filter_Trigger_Name = json_unquote(json_extract(in_request_json,"$.Filter_Trigger_Name"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
	SET Display_Formate = json_unquote(json_extract(in_request_json,"$.Display_formate"));
	SET Sorting_Order = json_unquote(json_extract(in_request_json,"$.Sorting_Order"));
	


	SET SQL_SAFE_UPDATES=0;
	SET foreign_key_checks=0;
	SET @temp_result = Null;

	
	
	
	Set IN_AGGREGATION_3= replace (IN_AGGREGATION_3,"[",'');
	
	Set IN_AGGREGATION_3= replace (IN_AGGREGATION_3,"]",'');
	Set IN_AGGREGATION_3= replace (IN_AGGREGATION_3,'"','');
	Set IN_AGGREGATION_3= replace (IN_AGGREGATION_3,', ',',');
	
	
	Set @IN_SCREEN=IN_SCREEN;
    Set @Type_of_Channel=IN_AGGREGATION_1;
    Set @Type_of_Event=IN_AGGREGATION_2;
    Set @Event_ID=IN_AGGREGATION_3;
	Set @Display_Formate=Display_Formate;

    Select IN_AGGREGATION_3;

	If IN_SCREEN ="Draft"
		 Then 
			Set @IN_SCREEN=3;
		End If;

		 If IN_SCREEN ="TESTED"
		 Then 
				Set @IN_SCREEN=2;
		End If;


		 If IN_SCREEN ="VERIFIED"
		 Then 
			Set @IN_SCREEN=4;
		End If;
		
		If IN_SCREEN ="SCHEDULETRIGGERS"
		 Then 
				Set @IN_SCREEN=5;
		End If;	
		
		 If IN_SCREEN ="Active"
		 Then 
			  Set @IN_SCREEN=1;
		End If;

		 If IN_SCREEN ="Inactive"
		 Then 
				Set @IN_SCREEN=0;
		End If;

	
If (IN_SCREEN ="Draft" and IN_AGGREGATION_4 !='TEST_CLM') or  IN_SCREEN ="TESTED"
		THEN
			
			Update CLM_Campaign_Trigger A, CLM_Campaign_Events B
						Set  A.CTState = 3
						Where A.CampTriggerId= B.CampTriggerId
						and  B.EventId  in  (Select Event_Id from Event_Master_UI A , Event_Master B  where A.Event_Id=B.Id and B.State="TESTING" and Status_Tested<>"OKTESTED");
		End IF;	
		
		
		If (IN_SCREEN ="Draft")
		THEN
			
			Update CLM_Campaign_Trigger A, CLM_Campaign_Events B
						Set  A.CTState = 2
						Where A.CampTriggerId= B.CampTriggerId
						and  B.EventId  in  (Select Event_Id from Event_Master_UI A , Event_Master B  where A.Event_Id=B.Id and B.State="Draft" and Status_DLT="DLT" and Status_Tested="TESTED");
		End IF;	
		
		
		If (IN_SCREEN ="Draft")
		THEN
			
			Update CLM_Campaign_Trigger A, CLM_Campaign_Events B
						Set  A.CTState = 4
						Where A.CampTriggerId= B.CampTriggerId
						and  B.EventId  in  (Select Event_Id from Event_Master_UI A , Event_Master B  where A.Event_Id=B.Id and B.State="Draft" and Status_QC="QC" and Status_DLT="DLT" and Status_Tested="TESTED");
		End IF;	
		
		If (IN_SCREEN ="Draft")
		THEN
			
			Update CLM_Campaign_Trigger A, CLM_Campaign_Events B
						Set  A.CTState = 5
						Where A.CampTriggerId= B.CampTriggerId
						and  B.EventId  in  (Select Event_Id from Event_Master_UI A , Event_Master B  where A.Event_Id=B.Id and B.State="Draft" and Status_QC="QC" and Status_DLT="DLT" and Status_Tested="TESTED" and Status_Schedule="Schedule");
		End IF;	
		
		If (IN_SCREEN ="Draft")
		THEN
			
			Update CLM_Campaign_Trigger A, CLM_Campaign_Events B
						Set  A.CTState = 1
						Where A.CampTriggerId= B.CampTriggerId
						and  B.EventId  in  (Select Event_Id from Event_Master_UI A , Event_Master B  where A.Event_Id=B.Id and B.State="Draft" and Status_Active ="Active"and Status_QC="QC" and Status_DLT="DLT" and Status_Tested="TESTED");
		End IF;	
		
		
		If (IN_SCREEN ="Draft")
		THEN
			
			Update CLM_Campaign_Trigger A, CLM_Campaign_Events B
						Set  A.CTState = 0
						Where A.CampTriggerId= B.CampTriggerId
						and  B.EventId  in  (Select Event_Id from Event_Master_UI A , Event_Master B  where A.Event_Id=B.Id and B.State="Draft" 
						and Status_Active ="InActive"and Status_QC="QC" and Status_DLT="DLT" and Status_Tested="TESTED");
		End IF;	
		
		
		
		
		
		If Filter_Trigger_Name is null 
			then Set @Filter_Trigger_Name="";
		else Set @Filter_Trigger_Name=Filter_Trigger_Name;
		end if;	
		
		If @Display_Formate is null 
			then Set @Display_Formate="Message";
		end if;	
		If @Type_of_Event is null
			then Set @Type_of_Event="ALL";
		end if;	
		
		Select @IN_SCREEN;
		If @IN_SCREEN is null or @IN_SCREEN<0
			then Select 1;Set @IN_SCREEN=3;
		end if;	
		
		If @Type_of_Channel is null 
			then Set @Type_of_Channel='SMS';
		end if;	
		
		If Sorting_Order = 'ID_ASCENDING'
		THEN	Set @Sorting_Order='Id Asc';
		End If;
		If Sorting_Order = 'ID_DESCENDING'
		THEN	Set @Sorting_Order = 'Id Desc';
		End If;
		If Sorting_Order = 'COVERAGE_ASCENDING'
		THEN	Set @Sorting_Order = 'Coverage Asc';
		End If;
		If Sorting_Order = 'COVERAGE_DESCENDING'
		THEN	Set @Sorting_Order = 'Coverage Desc';
		End If;
		If Sorting_Order is NULL or Sorting_Order=''
		THEN	Set @Sorting_Order = 'Event_Master_UI.ModifiedDate DESC,Execution_Sequence asc,Id desc';
		End If;
		
		Select Value into @GlobalCoolOff from M_Config where Name='Communication_CoolOff_Days';
		Select Value into @CLM_Event_Limit from M_Config where Name='CLM_Event_Limit';
		
		IF @Type_of_Channel ="SMS"
		THEN Select Value into @ChannelCoolOff from M_Config where Name='SMS_Communication_CoolOff_Days';
		END IF;
		
		IF @Type_of_Channel ="Email"
		THEN Select Value into @ChannelCoolOff from M_Config where Name='Email_Communication_CoolOff_Days';
		END IF;
		
		IF @Type_of_Channel ="WhatsApp"
		THEN Select Value into @ChannelCoolOff from M_Config where Name='WhatsApp_Communication_CoolOff_Days';
		END IF;
		
		IF @Type_of_Channel ="PN"
		THEN Select Value into @ChannelCoolOff from M_Config where Name='PN_Communication_CoolOff_Days';
		END IF;
		if @GlobalCoolOff ='' then Set @GlobalCoolOff =0; end if;
		if @ChannelCoolOff ='' or @ChannelCoolOff is null then Set @ChannelCoolOff =0; end if;
		if @CLM_Event_Limit ='' then Set @CLM_Event_Limit =0; end if;
		Select @Display_Formate,@Type_of_Event, @IN_SCREEN,@Type_of_Channel,@GlobalCoolOff,@ChannelCoolOff,@CLM_Event_Limit,@Sorting_Order;
		
		-- ----Type_Of_Event_Radio_Button-------


		IF  @Type_of_Event = "ALL"
		THEN 

			SET @Event_Type_Filter = "1=1";
            
		END IF;
            
		
		IF @Type_of_Event = "CLM"
        THEN

			SET @Event_Type_Filter = "Event_Master_UI.Type_Of_Event='CLM'";
		
        END IF;
        
		
		IF @Type_of_Event = "GTM"
        THEN 

			SET @Event_Type_Filter = "Event_Master_UI.Type_Of_Event='GTM'";
		
		END IF;
        
        IF @Type_of_Event = "ALL" THEN
    SET @Type_of_Event = '"CLM", "GTM"';
END IF;

SET @Type_of_Event = TRIM(BOTH '"' FROM @Type_of_Event);
        
		


		
	 	
	 SET @view_stmt=concat('CREATE OR REPLACE VIEW V_UI_Campaign_List AS(
	 Select Id,Name,CLMSegmentName,ModifiedDate,Coverage,Scheduled_at,CampaignName,
	 "" as More_Info,
	 
	 Status_Tested,Status_QC,Status_DLT,Status_Schedule,Status_Active,Message,CampaignUsesWaterfall,Execution_Sequence,Critical_Trigger
	From

	(Select 
	`CLM_Campaign_Events`.`EventId` as ID, `CLM_Campaign_Trigger`.`CampTriggerName` AS `Name`,
	replace(replace(`EventSegmentName`,"(",""),")","") as CLMSegmentName,CampaignName, 
	Case when  Message!="" then Replace(Replace(Message,"<SOLUS_PFIELD>","<b> #"),"</SOLUS_PFIELD>","# </b>") else Replace(Replace(`CLM_Creative`.Creative,"<SOLUS_PFIELD>","<b> #"),"</SOLUS_PFIELD>","# </b>") end as Message,
	Case when Event_Master_UI.Coverage <=0 then "NA" else Convert(format(Event_Master_UI.Coverage,","),CHAR) END as Coverage,
	date_format(Event_Master_UI.ModifiedDate,"%d-%m %H:%i") as ModifiedDate,
	
	 CLM_Campaign_Trigger.TriggerStartDate as Scheduled_at,
	
	Case when Communication_Cooloff <=0 then "Mandatory" 
		else  Communication_Cooloff
	end as Communication_Cooloff_Days,
   
	Case  WHEN`CLM_Campaign_Trigger`.`TriggerUsesWaterfall` IS NULL 
			THEN 
					CASE WHEN `CLM_Campaign`.`CampaignUsesWaterfall` = 100 THEN "CMABPerfBased "	  
						WHEN CLM_Campaign_Events.Execution_Sequence=(Select Min(Execution_Sequence) from Event_Master) then "Top_Priority"
						WHEN row_number()over(order by CLM_Campaign_Events.Execution_Sequence) =1 then "Top_Priority" 
				ELSE row_number()over(order by CLM_Campaign_Events.Execution_Sequence)
				END
			ELSE
					CASE	WHEN `CLM_Campaign_Trigger`.`TriggerUsesWaterfall` = 100 THEN "CMABPerfBased "
						WHEN CLM_Campaign_Events.Execution_Sequence=(Select Min(Execution_Sequence) from Event_Master) then "Top_Priority"
						WHEN row_number()over(order by CLM_Campaign_Events.Execution_Sequence) =1 then "Top_Priority" 
				ELSE row_number()over(order by CLM_Campaign_Events.Execution_Sequence)
				END
				
	END as Priority,
	Case WHEN Event_Limit <=0 THEN "NA"
		
		 Else Concat(" &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Throttling:",Event_Limit)
	end as Event_Limit,
	Case when Segment_Query_Condition != "1=1" THEN Concat(" &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CampaigncSegment Name: ",Segment_Name)
	else "NA"
	end as Campaign_SegmentName
	,
	Case when Segment_Query_Condition != "1=1" THEN Segment_Query_Condition
	else "NA"
	end as Campaign_SegmentCondition
	,
	Case when RegionReplacedQuery != "1=1" THEN Concat(" &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Region Segment Name: ",RegionSegmentName)
	else "NA"
	end as Region_SegmentName
	,
	Case when RegionReplacedQuery != "1=1" THEN RegionReplacedQuery
	else "NA"
	end as Region_SegmentCondition
	,
	
    Case when CTReplacedQuery Like "%Lt_Fav_Day_Of_Week%" THEN Concat(" &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Favourite_Day: YES")
	else "NA"
	end as Favourite_Day,
	Case when CTReplacedQuery != "1=1" and  CTReplacedQuery Not Like "%Lt_Fav_Day_Of_Week =weekDay(now())" THEN 
			Concat(" &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Additional_Criterai_SVOC: ",CTReplacedQuery)
        when CTReplacedQuery != "1=1" and  CTReplacedQuery = "Lt_Fav_Day_Of_Week =weekDay(now())" THEN  "NA" 	
     
    when CTReplacedQuery != "1=1" and  CTReplacedQuery  Like "%Lt_Fav_Day_Of_Week =weekDay(now())" THEN 
					Concat(" &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Additional_Criterai_SVOC: ",Replace(CTReplacedQuery," and Lt_Fav_Day_Of_Week =weekDay(now())  ",""))   
   
	else "NA"
	end as Additional_Criterai_Replaced_Query,
	Case when  CTReplacedQueryBill != " "  Then Concat(" &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Additional_Criterai_Bill: ",CTReplacedQueryBill)
		else "NA"
	end as Additional_Criterai_Bill_Query,
	
	Status_Tested,Status_QC,Status_DLT,Status_Schedule,Status_Active ,CampaignUsesWaterfall,Critical_Trigger, CLM_Campaign_Events.Execution_Sequence
	FROM
			(((((((((((`CLM_CampaignTheme`
			
			JOIN `CLM_Campaign`)
			JOIN `CLM_Campaign_Trigger`)
			JOIN `CLM_MicroSegment`)
			JOIN `CLM_Channel`)
			JOIN `CLM_Campaign_Events`)
			Join Event_Master_UI)
			JOIN `CLM_Creative`)
			JOIN CLM_Segment)
			
			LEFT JOIN `CLM_Offer` `CO` ON (`CLM_Creative`.`OfferId` = `CO`.`OfferId`))
			Left Join CLM_RegionSegment on (`CLM_Campaign_Events`.`RegionSegmentId` = `CLM_RegionSegment`.`RegionSegmentId` ))
			Left Join Campaign_Segment on (`CLM_Campaign_Events`.`Campaign_SegmentId` = `Campaign_Segment`.`Segment_Id` ))
		WHERE
			`CLM_Campaign_Events`.`CampaignThemeId` = `CLM_CampaignTheme`.`CampaignThemeId`
				
				AND `CLM_Campaign_Events`.`CampaignId` = `CLM_Campaign`.`CampaignId`
				AND `CLM_Campaign_Events`.`CampTriggerId` = `CLM_Campaign_Trigger`.`CampTriggerId`
				AND `CLM_Campaign_Events`.`MicroSegmentId` = `CLM_MicroSegment`.`MicroSegmentId`
				AND `CLM_Campaign_Events`.`ChannelId` = `CLM_Channel`.`ChannelId`
				AND `CLM_Campaign_Events`.`CreativeId` = `CLM_Creative`.`CreativeId`
				AND `CLM_Campaign_Events`.`EventId` = `Event_Master_UI`.`Event_ID`
				And    FIND_IN_SET(CLM_Segment.CLMSegmentId,CLM_Campaign_Events.CLMSegmentId)
				-- AND `CLM_Campaign_Events`.`RegionSegmentId` = `CLM_RegionSegment`.`RegionSegmentId` 
			AND ',@Event_Type_Filter,' AND CLM_Channel.ChannelName="',@Type_of_Channel,' " AND `CLM_Campaign_Trigger`.`CTState`= "',@IN_SCREEN,'"
			AND `CLM_Campaign_Trigger`.`CampTriggerName` like "%',@Filter_Trigger_Name,'%"
		   GROUP BY EventId
	ORDER BY ',@Sorting_Order,')A
	);');
					   
						SELECT @view_stmt;
						PREPARE statement from @view_stmt;
						Execute statement;
						Deallocate PREPARE statement;
						
-- ---------------------------------------------- DRAFT SCREEN CODE ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 If IN_SCREEN ="Draft"
 Then 
 	Set @IN_SCREEN=IN_SCREEN;

 	

 	-- 	DISPLAY SCREEN

		 	if IN_AGGREGATION_3 ='' 
		 	Then	
				
					
								
				    SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Draft_ID",`ID`,"Draft_name",`Name`,"Draft_Campaign",`CampaignName`,"Draft_msg",`Message`,"Draft_segment",`CLMSegmentName`,"Draft_comms",`Coverage`,"Draft_date",ModifiedDate,"Status_Tested",`Status_Tested`,"More_Info",More_Info))),"]")
				    into @temp_result from V_UI_Campaign_List ;') ;
				      
					SELECT @Query_String;
					PREPARE statement from @Query_String;
					Execute statement;
					Deallocate PREPARE statement; 
					
					if @temp_result is null
						then set @temp_result="[]";
					End if;
					
				    SET out_result_json = @temp_result;
			End if;
			
	-- 	TEST BUTTON CLICKED

		 	if IN_AGGREGATION_3 !='' AND IN_AGGREGATION_4 ='TEST'
		 	Then	

				   

					   Select `End` into @CLM_Status from CLM_Process_Log order by Id Desc limit 1;
					   
					   If @CLM_Status ='ENDED' or @CLM_Status ='FAILED'
							THEN
							
							
								Set UIEvent_Id=IN_AGGREGATION_3;
							    -- Changing the Event Status to Testing

							   
								set @UI_JOB_NAME='';
								set @UI_JOB_NAME=concat("Starting the CLM for Event_ID: ",UIEvent_Id);
								
								Set @Query_String0= concat('
								Delete from ETL_Execution_Details
								where Job_Name like "',@UI_JOB_NAME,'%";') ;
   
								SELECT @Query_String0;
								PREPARE statement from @Query_String0;
								Execute statement;
								Deallocate PREPARE statement; 
								
				                INSERT into UI_Campaign_Task_List (UI_Campaign_Task, Run_Mode, Event_Id,Status,CreatedDate) 
                                   values ('S_CLM_Run',"TEST",UIEvent_Id,"START", current_timestamp());
			
								INSERT into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
                                   values ('S_CLM_Run',concat("Starting the CLM for Event_ID: ",UIEvent_Id," From UI ") ,now(),now(),'Started',@Event_ID);
								
								-- INSERT into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
                                 --   values ('S_CLM_Run',"CLM will start in a minute " ,now(),now(),'Started',@Event_ID);
																
										
								Drop View if EXISTS  CLM_TEST_Status;
								Drop View if EXISTS  V_UI_Campaign_List_Output;
								Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","CLM Started") )),"]") into @temp_result;') ;
   
								SELECT @Query_String;
								PREPARE statement from @Query_String;
								Execute statement;
								Deallocate PREPARE statement; 
						ELSE		
								Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","CLM is Already running") )),"]") into @temp_result;') ;
   
								SELECT @Query_String;
								PREPARE statement from @Query_String;
								Execute statement;
								Deallocate PREPARE statement;

						END IF;		
				    			      
					SET out_result_json = @temp_result  ;
			End if;

	-- OUTPUT MESSAGE DISPLAY		

	if IN_AGGREGATION_4 ='TEST_CLM'
		 	Then	
		 			

				    SET @Query_String =  'SELECT job_name
				    into @clmresult from  ETL_Execution_Details
					where Procedure_Name = "S_CLM_Event_Execution" or Procedure_Name = "S_CLM_Run"
					or Procedure_Name ="S_CLM_Communication_File" or Procedure_Name ="S_CLM_Communication_File_Gen_CustLoop"
					order by Execution_ID desc
					Limit 1;' ;
					
				      
					SELECT @Query_String;
					PREPARE statement from @Query_String;
					Execute statement;
					Deallocate PREPARE statement; 
                    
                    SELECT IN_AGGREGATION_3 as '';
                    
                    SET @Event_ID = '';

					SELECT IN_AGGREGATION_3 INTO @Event_ID;

					
									
					Select Count(1) into @Customer_Count from Event_Execution_Current_Run where Is_Target ="Y";
					Select `End` into @CLM_Status from CLM_Process_Log order by Id Desc limit 1;
					
					Select @CLM_Status,@clmresult,@Customer_Count;
					
					if (@CLM_Status ='ENDED' or @CLM_Status ='FAILED') and (@Customer_Count > 0 or @Customer_Count <= 0)  and (@clmresult ="CLM Completed !!" or @clmresult Like"%No data - zero rows fetched, selected, or processed%"  or @clmresult Like"% does not exist%" or @clmresult Like"% doesn't exist%" or @clmresult like "%'field list'%" or @clmresult Like "%Incorrect number of arguments%" or @clmresult Like "%ERROR : Hold Out has less customers than the specified value%")
					THEN		
							Select @CLM_Status;
							INSERT into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
                                   values ('S_CLM_Run',concat("Completed the CLM for Event_ID: ",@Event_ID," From UI ") ,now(),now(),'CLM_Completed',@Event_ID);
								   
					End  IF;
					
						SELECT Execution_ID into @minExecution_ID 
						from  ETL_Execution_Details
						where job_name like "%Starting the CLM for Event_ID:%"
						order by Execution_ID desc
						Limit 1;
						
						SELECT Execution_ID into @maxExecution_ID 
						from  ETL_Execution_Details
						where job_name like "%Completed the CLM for Event_ID:%"
						order by Execution_ID desc
						Limit 1;
						
						if @maxExecution_ID is null 
							then Set @maxExecution_ID=0; end if;
						if @minExecution_ID is null then Set @minExecution_ID=0; end if;							
						If @maxExecution_ID is NULL or (@maxExecution_ID <=@minExecution_ID)
						Then SET @stmt=concat('SELECT max(Execution_ID)+1 into @maxExecution_ID FROM ETL_Execution_Details  ORDER BY Execution_ID DESC LIMIT 1;');
							select @stmt;
							PREPARE statement from @stmt;
							Execute statement;
							Deallocate PREPARE statement;
						End If;
						
						if @maxExecution_ID is null 
							then Set @maxExecution_ID=0; end if;
						Select @minExecution_ID,@maxExecution_ID;
						
					
							 SET @view_stmt=concat('Create or replace View CLM_TEST_Status AS
														(	Select Distinct job_name,row_number() over(order by Execution_ID asc) as `S_NO`,Procedure_Name,Start_Time,End_Time,Status
															From 
															
															(SELECT Execution_ID,
																Procedure_Name,
																CASE
																	WHEN job_name like 	"Starting the CLM for Event_ID:%" Then Concat(Replace(Replace (job_name ,"Starting the CLM for Event_ID:","Campaign Started for the SOLUS_Id"),"From UI ","at "),date_format(Start_Time,"%d-%m %h:%i"))
																	WHEN Procedure_Name = "S_CLM_Event_Execution" and (job_name  like "%TEST%") THEN "Selecting the Customers for the Campaign..."
																	WHEN Procedure_Name = "S_CLM_Event_Execution" and ( job_name  Like  "%TotalRejected%") THEN job_name
																	WHEN job_name = "" THEN Concat("Customer Selection Completed  Successfully !! Starting Construction of Messages at ",date_format(Start_Time,"%d-%m %h:%i"),"....")
																	WHEN Procedure_Name = "S_CLM_Communication_File_Gen_CustLoop" THEN "Constructing Message"
																	WHEN Procedure_Name = "S_CLM_Communication_File" THEN "Constructing Message"
																	WHEN job_name like  "%CLM IS COMPLETE%" THEN concat("Message Generated Successfully at ",date_format(Start_Time,"%d-%m %h:%i"))
																	WHEN job_name like  "%Completed the CLM for Event_ID%" THEN "Campaign Generated Successfully !!"
																	WHEN job_name Like "% does not exist%" THEN Concat("CLM Failed due to ",job_name," .Please connect with the Team ")
																	WHEN job_name Like "% does %" THEN Concat("CLM Failed due to ",job_name," .Please connect with the Team ")
																	WHEN job_name like "% field list %" THEN Concat("CLM Failed due to ",job_name," .Please connect with the Team ")
																	WHEN job_name Like "%Incorrect number of arguments%" THEN Concat("CLM Failed due to ",job_name," .Please connect with the Team ")
																	WHEN job_name Like "%Failed%" THEN Concat("CLM Failed due to ",job_name," .Please connect with the Team ")
																	WHEN job_name Like "%CLM is already running%" THEN "CLM is already running"
																	WHEN job_name Like "%error%" THEN Concat("CLM Failed due to ",job_name," .Please connect with the Team ")
	
	
																ELSE job_name
																END AS job_name,
																date_format(Start_Time,"%d-%m %h:%i") as Start_Time,
																Case when job_name like  "%No data - zero rows fetched, selected, or processed%" Then date_format(Start_Time,"%d-%m %h:%i")
																	Else date_format(End_Time,"%d-%m %h:%m")
																End as End_Time,
																Case when Status ="" then "Succeeded" Else Status end as Status
															FROM ETL_Execution_Details
															WHERE (Procedure_Name like "S_CLM%" or Procedure_Name like "S_Exclusion_Filter_Execution")
															 and Procedure_Name <> "S_CLM_Event_Attribution"
															and ((job_name is not null) and( job_name not like "%No data - zero rows fetched, selected, or processed%")
																	and (job_name !="Started"))
															and
															Execution_ID between ',@minExecution_ID,' and ',@maxExecution_ID,'
															
															
															ORDER BY Execution_ID ASC)A
															Group by job_name 
															Order by S_NO
														)

						    ');
							
							select @view_stmt;
							PREPARE statement from @view_stmt;
							Execute statement;
							Deallocate PREPARE statement;

						SELECT CONCAT("[",(GROUP_CONCAT(json_object("s_no",S_NO,"proc_name",Procedure_Name,"Message",job_name,"start_time",Start_Time,"end_time",End_Time,"status",`Status`,"ID",@Event_ID) )),"]")into @temp_result
						 From
						 (Select "S.No" as S_NO,"Step" as Procedure_Name, "<b>Message</b>" as job_name,"Start Time(DD-MM HR:MM)" as Start_Time,"End Time(DD-MM HR:MM)" as End_Time,   "Status" as Status
								Union 
								Select S_NO,Procedure_Name,job_name,Start_Time,End_Time,Status from CLM_TEST_Status
								

								)A;
						
						Select @temp_result;
							
					
					
						
					if @CLM_Status ='ENDED' and @Customer_Count > 0  and ( @clmresult Like"Completed the CLM for Event_ID:%")
					Then 
									Select 1;
							 SET @view_stmt=concat('CREATE OR REPLACE VIEW V_UI_Campaign_List_Output AS
							(SELECT  
							B.Customer_Id as Customer
							,A.message as Message from  Event_Execution_Current_Run A,CDM_Customer_PII_Master B
								Where A.Customer_Id=B.Customer_Id and Is_Target="Y" and A.message is not null
								limit 1
							 )');
							select @view_stmt;
							PREPARE statement from @view_stmt;
							Execute statement;
							Deallocate PREPARE statement;
								
							
						SELECT Trim(SUBSTRING_INDEX(SUBSTRING_INDEX(job_name,":",-1),"From",1))  into @Event_ID
								 from  ETL_Execution_Details
									where Procedure_Name Like "S_CLM_Run"
									and job_name like "%Starting the CLM for Event_ID:%"
									order by Execution_ID desc
									Limit 1;
									
						Select @Event_ID;
									
								
							SELECT CONCAT("[",(GROUP_CONCAT(json_object("Customer",Customer,"Message",message,"status","CLM_Completed","event_id",@Event_ID,"ID",@Event_ID) )),"]") into @temp_result
							 From
							 (
								Select "Customer" as Customer, "Message" as message
									UNION 
								Select *from V_UI_Campaign_List_Output where Customer<>''
								)A ;
							
							Select @temp_result;
						
											      
						
					End if;
					
					If @CLM_Status ='ENDED' and @Customer_Count <= 0  and (@clmresult Like"Completed the CLM for Event_ID:%")
					

					Then   
							
						SELECT Trim(SUBSTRING_INDEX(SUBSTRING_INDEX(job_name,":",-1),"From",1))  into @Event_ID
								 from  ETL_Execution_Details
									where Procedure_Name Like "S_CLM_Run"
									and job_name like "%Starting the CLM for Event_ID:%"
									order by Execution_ID desc
									Limit 1;
									
						Select @Event_ID;
						Set @CurDate=CURDATE();
								
						
						Select Creative1 into @Comm_Template_Creative from Event_Master where Id=@Event_ID;
						
						Select Header into @Comm_Header from Event_Master A, Communication_Template B where A.CreativeId1=B.Id and A.Id=@Event_ID;
						
						CALL PROC_CLM_IS_VALID_Template(@Comm_Template_Creative, @Comm_Template_Header, @Comm_Message);
								
						SELECT CONCAT("[",(GROUP_CONCAT(json_object("customer_detail",Customer_Details,"Message",message,"created_date",End_Time,"status","CLM_Completed","event_id",@Event_ID,"ID",@Event_ID) )),"]") into @temp_result
							 From
							 (Select "Customer Details" as Customer_Details, "Message" as message,"Current Time" as End_Time
									UNION 
								Select "0" as Customer_Details, "Zero Customer Got Selected.Please mark 'Test OK' to proceed to next steps " as message,Current_Time() as End_Time
								)A ;
							
							
								Select @temp_result;
							
							 Update Event_Master_UI
							set
							 Event_Execution_Date_ID = (Select Event_Execution_Date_ID from Event_Execution where Event_Id=@Event_ID limit 1)
							where Event_Id=@Event_ID;
						
						
						set @Comm_Error_Message='';
						Select "YES" into @Comm_Error_Message From DUAL where @Comm_Message Like "%error%";
						
						Select @Comm_Error_Message ;
						if @Comm_Error_Message != "YES" then
						
						Select "YES"  into @Comm_Error_Message From DUAL where @Comm_Message Like "%'field list'%";
							
						end if;
						
						if @Comm_Error_Message is null then
						
						Select "YES"  into @Comm_Error_Message From DUAL where @Comm_Message Like "%'field list'%";
							
						end if;
						
						
						IF  @Comm_Error_Message !="YES"
							Then
								Update Event_Master_UI
									Set Message =@Comm_Message
								Where Event_Id =@Event_ID;
						End If;			
									  
					
					End if;
					
					
					Select @clmresult;
					If  (@CLM_Status ='FAILED')  and @Customer_Count <= 0  and (@clmresult Like"Completed the CLM for Event_ID:%" or @clmresult Like "%Failed%" or @clmresult Like "%CLM is already running%" or @clmresult Like "%error%" or @clmresult Like"% does not exist%" or @clmresult Like"% doesn't exist%" or @clmresult like "%field list%" or @clmresult Like "%Incorrect number of arguments%"  or @clmresult Like "%ERROR : Hold Out has less customers than the specified value%")

					Then 
									Select 3;
								SELECT Trim(SUBSTRING_INDEX(SUBSTRING_INDEX(job_name,":",-1),"From",1))  into @Event_ID
								 from  ETL_Execution_Details
									where Procedure_Name Like "S_CLM_Run"
									and job_name like "%Starting the CLM for Event_ID:%"
									order by Execution_ID desc
									Limit 1;
									
						Select @Event_ID;
					
							
							SELECT CONCAT("[",(GROUP_CONCAT(json_object("customer_detail",Customer_Details,"Message",message,"created_date",End_Time,"status","CLM_Completed","event_id",IN_AGGREGATION_3,"ID",IN_AGGREGATION_3) )),"]") into @temp_result
							 From
							 (Select "Customer Details" as Customer_Details, "Message" as message,"Current Time" as End_Time
									UNION 
								Select "" as Customer_Details, "Testing Failed due to the following reasons" as message,"" as End_Time
								UNION 
								Select "" as Customer_Details, "<b>The SOLUS Personalized are not avaliable or Hold Out has less customers than the specified value</b>" as message,"" as End_Time
								UNION 
								Select "" as Customer_Details, "<b>The SOLUS Software is not updated.</b>" as message,"" as End_Time
								 
								
								)A ;
							
				
					
						Select @temp_result;
									
				
						
					End if;
	         SET out_result_json = @temp_result;
					if @temp_result is null
						then set @temp_result="[]";
					End if;
					
				    
			End if;
			
 	-- 	OK TESTED CLICKED

		 	if IN_AGGREGATION_3 !='' AND IN_AGGREGATION_4 ='OK_TESTED'
		 		Then	
						Select IN_AGGREGATION_3;
						Select @Event_ID;
						
			 			 SET @Update_Event = concat('Update CLM_Campaign_Trigger A, CLM_Campaign_Events B
									      Set  A.CTState = 5  
									Where A.CampTriggerId= B.CampTriggerId
								      and  B.EventId in (',@Event_ID,');') ;
									  

                        SELECT @Update_Event;
						PREPARE statement from @Update_Event;
						Execute statement;
						Deallocate PREPARE statement;
						
						 SET @Update_Event = concat('Update Event_Master_UI
								Set Status_Tested ="OKTESTED"
								Where Event_Id  in (',@Event_ID,');') ;
								  SELECT @Update_Event;
						PREPARE statement from @Update_Event;
						Execute statement;
						Deallocate PREPARE statement;
						
			 			 SET @Update_Event = concat(' Update Event_Master_UI
					    	Set Status_DLT ="DLT"
					    	Where Event_Id  in (',@Event_ID,');') ;
								
							  SELECT @Update_Event;
						PREPARE statement from @Update_Event;
						Execute statement;
						Deallocate PREPARE statement;
						
			 			 SET @Update_Event = concat(' Update Event_Master_UI
					    	Set ModifiedDate =current_timestamp()
					    	Where Event_Id  in (',@Event_ID,');') ;
						
						SELECT @Update_Event;
						PREPARE statement from @Update_Event;
						Execute statement;
						Deallocate PREPARE statement;
						
				    SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Draft_ID",`ID`,"Draft_name",`Name`,"Draft_Campaign",`CampaignName`,"Draft_msg",`Message`,"Draft_segment",`CLMSegmentName`,"Draft_comms",`Coverage`,"Draft_date",ModifiedDate,"Status_Tested",`Status_Tested`,"More_Info",More_Info))),"]")
				    into @temp_result from V_UI_Campaign_List ;') ;
				      
					SELECT @Query_String;
					PREPARE statement from @Query_String;
					Execute statement;
					Deallocate PREPARE statement; 

									      
					    
						 
                                    if @temp_result is null
									  then set @temp_result="[]";
									  End if;	

					    SET out_result_json = @temp_result;
				End if;
                
  -- For DEACTIVATE in draft screen          
                
  if  IN_AGGREGATION_3 !='' and IN_AGGREGATION_4 ='DEACTIVATE_OK'
		 		Then	

			 		
			
			 			 SET @Update_Event = concat('Update CLM_Campaign_Trigger A, CLM_Campaign_Events B
									      Set  A.CTState = 0  
									Where A.CampTriggerId= B.CampTriggerId
								      and  B.EventId in(',@Event_ID,');') ;
									  

                        SELECT @Update_Event;
						PREPARE statement from @Update_Event;
						Execute statement;
						Deallocate PREPARE statement;
						
						
						
			 			 SET @Update_Event = concat(' Update Event_Master_UI
					    	Set Status_Active ="InActive"
					    	Where Event_Id in (',@Event_ID,');') ;
						
						SELECT @Update_Event;
						PREPARE statement from @Update_Event;
						Execute statement;
						Deallocate PREPARE statement;
							
					
			 			 SET @Update_Event = concat(' Update Event_Master_UI
					    	Set ModifiedDate =current_timestamp()
					    	Where Event_Id  in (',@Event_ID,');') ;
						
						SELECT @Update_Event;
						PREPARE statement from @Update_Event;
						Execute statement;
						Deallocate PREPARE statement;
						
							
					 SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Activate_ID",`ID`,"Activate_name",`Name`,"Activat_Campaign",`CampaignName`,"Activate_msg",Message,"Activate_segment",`CLMSegmentName`,"Activate_comms",`Coverage`,"Activate_date",ModifiedDate,"Status_QC",`Status_QC`,"Status_DLT",`Status_DLT`,"Status_Schedule",`Status_Schedule`,"Status_Active",`Status_Active`,"More_Info",`More_Info`,"Critical_Trigger",Critical_Trigger,"Scheduled_at",`Scheduled_at`) )),"]")
											into @temp_result from V_UI_Campaign_List;') ;
					  
					SELECT @Query_String;
					PREPARE statement from @Query_String;
					Execute statement;
					Deallocate PREPARE statement; 

                                    if @temp_result is null
									  then set @temp_result="[]";
									  End if;	

					    SET out_result_json = @temp_result;
				End if;			

    
		
		    
end if;


-- ---------------------------------------------- TESTED SCREEN CODE ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
 
 if IN_SCREEN ="TESTED"
 Then 
 	
 	Set @IN_SCREEN="Testing";

 	-- DISPLAY SCRREN

 	if IN_AGGREGATION_3 =''
		 	Then	
			 	
					

							    SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Tested_ID",`ID`,"Tested_name",`Name`,"Tested_Campaign",`CampaignName`,"Tested_msg",Message,"Tested_segment",`CLMSegmentName`,"Tested_comms",`Coverage`,"Tested_date",ModifiedDate,"Status_Tested",`Status_Tested`,"Status_QC",`Status_QC`,"Status_DLT",`Status_DLT`,"More_Info",`More_Info`)  )),"]")
													    into @temp_result from  V_UI_Campaign_List;') ;
									      
								SELECT @Query_String;
								PREPARE statement from @Query_String;
								Execute statement;
								Deallocate PREPARE statement; 
								 
								 if @temp_result is null
									 then set @temp_result="[]";
				     			  End if;

			    SET out_result_json = @temp_result;
	End if;		    

	
	
	-- 	DLT OK  CLICKED
 	
		 	if IN_AGGREGATION_3 !='' and IN_AGGREGATION_4 ='DLT_OK'
		 		Then	
							
							-- Updating the Status in Event_Master_UI
						SET @Update_Event = concat(' Update Event_Master_UI
					    	Set Status_DLT ="DLT"
					    	Where Event_Id  in (',@Event_ID,');') ;
								
							  SELECT @Update_Event;
						PREPARE statement from @Update_Event;
						Execute statement;
						Deallocate PREPARE statement;
						
			 			 SET @Update_Event = concat(' Update Event_Master_UI
					    	Set ModifiedDate =current_timestamp()
					    	Where Event_Id  in (',@Event_ID,');') ;
						
						SELECT @Update_Event;
						PREPARE statement from @Update_Event;
						Execute statement;
						Deallocate PREPARE statement;
								
							    SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Tested_ID",`ID`,"Tested_name",`Name`,"Tested_Campaign",`CampaignName`,"Tested_msg",Message,"Tested_segment",`CLMSegmentName`,"Tested_comms",`Coverage`,"Tested_date",ModifiedDate,"Status_Tested",`Status_Tested`,"Status_QC",`Status_QC`,"Status_DLT",`Status_DLT`,"More_Info",`More_Info`)  )),"]")
													    into @temp_result from  V_UI_Campaign_List;') ;
									      
								SELECT @Query_String;
								PREPARE statement from @Query_String;
								Execute statement;
								Deallocate PREPARE statement; 
						



                                    if @temp_result is null
									  then set @temp_result="[]";
									  End if;		

					    SET out_result_json = @temp_result;
				End if;			
    
		-- 	QC OK  CLICKED
 	
		 	if  IN_AGGREGATION_3 !='' and IN_AGGREGATION_4 ='QC_OK'
		 		Then	

			 			
						
			 			 SET @Update_Event = concat('Update CLM_Campaign_Trigger A, CLM_Campaign_Events B
									      Set  A.CTState = 4  
									Where A.CampTriggerId= B.CampTriggerId
								      and  B.EventId in (',@Event_ID,');') ;
									  

                        SELECT @Update_Event;
						PREPARE statement from @Update_Event;
						Execute statement;
						Deallocate PREPARE statement;

							
						-- Updating the Status in Event_Master_UI

						 SET @Update_Event = concat(' Update Event_Master_UI
					    	Set Status_QC ="QC"
					    	Where Event_Id  in (',@Event_ID,');') ;
						
						SELECT @Update_Event;
						PREPARE statement from @Update_Event;
						Execute statement;
						Deallocate PREPARE statement;

							
			 			 SET @Update_Event = concat(' Update Event_Master_UI
					    	Set ModifiedDate =current_timestamp()
					    	Where Event_Id  in (',@Event_ID,');') ;
						
						SELECT @Update_Event;
						PREPARE statement from @Update_Event;
						Execute statement;
						Deallocate PREPARE statement;

							    SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Tested_ID",`ID`,"Tested_name",`Name`,"Tested_Campaign",`CampaignName`,"Tested_msg",Message,"Tested_segment",`CLMSegmentName`,"Tested_comms",`Coverage`,"Tested_date",ModifiedDate,"Status_Tested",`Status_Tested`,"Status_QC",`Status_QC`,"Status_DLT",`Status_DLT`,"More_Info",`More_Info`)  )),"]")
													    into @temp_result from  V_UI_Campaign_List;') ;
									      
								SELECT @Query_String;
								PREPARE statement from @Query_String;
								Execute statement;
								Deallocate PREPARE statement; 




                                    if @temp_result is null
									  then set @temp_result="[]";
									  End if;		

					    SET out_result_json = @temp_result;
				End if;				


end if;


-- ---------------------------------------------- VERIFIED SCREEN CODE ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
 
 if IN_SCREEN ="VERIFIED"
 Then 
    

    -- DISPLAY SCRREN
 	
 	if IN_AGGREGATION_3 =''
		 	Then	
			
			     SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Verified_ID",`ID`,"Verified_name",`Name`,"Verified_Campaign",`CampaignName`,"Verified_msg",Message,"Verified_segment",`CLMSegmentName`,"Verified_comms",`Coverage`,"Verified_date",ModifiedDate,"Status_QC",`Status_QC`,"Status_DLT",`Status_DLT`,"Status_Schedule",`Status_Schedule`,"More_Info",`More_Info`) )),"]")
									    into @temp_result from  V_UI_Campaign_List
									     ;') ;
			      
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 


                 if @temp_result is null
				  then set @temp_result="[]";
				 End if;


			    SET out_result_json = @temp_result;
    
    End If;


	-- 	Schedule OK  CLICKED
 	
		 	if  IN_AGGREGATION_3 !='' and IN_AGGREGATION_4 ='SCHEDULE_OK'
		 		Then	

			 			 SET @Update_Event = concat('Update CLM_Campaign_Trigger A, CLM_Campaign_Events B
									      Set  A.CTState = 5  
									Where A.CampTriggerId= B.CampTriggerId
								      and  B.EventId in (',@Event_ID,');') ;
								
								

                        SELECT @Update_Event;
						PREPARE statement from @Update_Event;
						Execute statement;
						Deallocate PREPARE statement;
						
						 	
			 			 SET @Update_Event = concat(' Update Event_Master_UI
					    	Set ModifiedDate =current_timestamp()
					    	Where Event_Id  in (',@Event_ID,');') ;
						
						SELECT @Update_Event;
						PREPARE statement from @Update_Event;
						Execute statement;
						Deallocate PREPARE statement;
								
					
			     SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Verified_ID",`ID`,"Verified_name",`Name`,"Verified_Campaign",`CampaignName`,"Verified_msg",Message,"Verified_segment",`CLMSegmentName`,"Verified_comms",`Coverage`,"Verified_date",ModifiedDate,"Status_QC",`Status_QC`,"Status_DLT",`Status_DLT`,"Status_Schedule",`Status_Schedule`,"More_Info",`More_Info`) )),"]")
									    into @temp_result from  V_UI_Campaign_List
									     ;') ;
			      
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 

						
                                    if @temp_result is null
									  then set @temp_result="[]";
									  End if;		

					    SET out_result_json = @temp_result;
				End if;			


end if;



-- ---------------------------------------------- SCHEDULE SCREEN CODE ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


 IF IN_SCREEN ="SCHEDULETRIGGERS" or IN_SCREEN ="SCHEDULE"
 Then 

 	

 	-- DISPLAY SCRREN
 	
 	if IN_AGGREGATION_3 =''
		 	Then	
			
		

			     SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Schedule_ID",`ID`,"Schedule_name",`Name`,"Schedule_Campaign",`CampaignName`,"Schedule_msg",Message,"Schedule_segment",`CLMSegmentName`,"Schedule_comms",`Coverage`,"Schedule_date",ModifiedDate,"Status_Schedule",`Status_Schedule`,"More_Info",`More_Info`,"Scheduled_at",`Scheduled_at`,"Critical_Trigger",Critical_Trigger) )),"]")
									    into @temp_result from V_UI_Campaign_List ;') ;
			      
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 

                  if @temp_result is null
					then set @temp_result="[]";
				 End if;	


			    SET out_result_json = @temp_result;
   End if;

 -- For DEACTIVATE in Ready to launch screen         
                
  if  IN_AGGREGATION_3 !='' and IN_AGGREGATION_4 ='DEACTIVATE_OK'
		 		Then	

			 		
			
			 			 SET @Update_Event = concat('Update CLM_Campaign_Trigger A, CLM_Campaign_Events B
									      Set  A.CTState = 0  
									Where A.CampTriggerId= B.CampTriggerId
								      and  B.EventId in(',@Event_ID,');') ;
									  

                        SELECT @Update_Event;
						PREPARE statement from @Update_Event;
						Execute statement;
						Deallocate PREPARE statement;
						
						
						
			 			 SET @Update_Event = concat(' Update Event_Master_UI
					    	Set Status_Active ="InActive"
					    	Where Event_Id in (',@Event_ID,');') ;
						
						SELECT @Update_Event;
						PREPARE statement from @Update_Event;
						Execute statement;
						Deallocate PREPARE statement;
							
					
			 			 SET @Update_Event = concat(' Update Event_Master_UI
					    	Set ModifiedDate =current_timestamp()
					    	Where Event_Id  in (',@Event_ID,');') ;
						
						SELECT @Update_Event;
						PREPARE statement from @Update_Event;
						Execute statement;
						Deallocate PREPARE statement;
						
							
					 SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Activate_ID",`ID`,"Activate_name",`Name`,"Activat_Campaign",`CampaignName`,"Activate_msg",Message,"Activate_segment",`CLMSegmentName`,"Activate_comms",`Coverage`,"Activate_date",ModifiedDate,"Status_QC",`Status_QC`,"Status_DLT",`Status_DLT`,"Status_Schedule",`Status_Schedule`,"Status_Active",`Status_Active`,"More_Info",`More_Info`,"Critical_Trigger",Critical_Trigger,"Scheduled_at",`Scheduled_at`) )),"]")
											into @temp_result from V_UI_Campaign_List;') ;
					  
					SELECT @Query_String;
					PREPARE statement from @Query_String;
					Execute statement;
					Deallocate PREPARE statement; 

                                    if @temp_result is null
									  then set @temp_result="[]";
									  End if;	

					    SET out_result_json = @temp_result;
				End if;	


   
	-- 	ACTIVE OK  CLICKED
 	
		 	if IN_AGGREGATION_3 !='' and IN_AGGREGATION_4 ='ACTIVATE_OK'
		 		Then	
					SET @Update_Event = concat('Update CLM_Campaign_Trigger A, CLM_Campaign_Events B
									      Set  A.CTState = 1 
									Where A.CampTriggerId= B.CampTriggerId
								      and  B.EventId in (',@Event_ID,');') ;
									  

                    SELECT @Update_Event;
					PREPARE statement from @Update_Event;
					Execute statement;
					Deallocate PREPARE statement;
                   
				    SET @Update_Event = concat('Update Event_Master_UI
						Set Status_Active ="Active"
					Where Event_Id in (',@Event_ID,');') ;
						
						SELECT @Update_Event;
						PREPARE statement from @Update_Event;
						Execute statement;
						Deallocate PREPARE statement;
								
								
						
			 			 SET @Update_Event = concat(' Update Event_Master_UI
					    	Set ModifiedDate =current_timestamp()
					    	Where Event_Id  in (',@Event_ID,');') ;
						
						SELECT @Update_Event;
						PREPARE statement from @Update_Event;
						Execute statement;
						Deallocate PREPARE statement;
								
			     SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Schedule_ID",`ID`,"Schedule_name",`Name`,"Schedule_Campaign",`CampaignName`,"Schedule_msg",Message,"Schedule_segment",`CLMSegmentName`,"Schedule_comms",`Coverage`,"Schedule_date",ModifiedDate,"Status_Schedule",`Status_Schedule`,"More_Info",`More_Info`,"Scheduled_at",`Scheduled_at`,"Critical_Trigger",Critical_Trigger) )),"]")
									    into @temp_result from V_UI_Campaign_List ;') ;
			      
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 

                  if @temp_result is null
					then set @temp_result="[]";
				 End if;	
			
			END IF;
		
			    SET out_result_json = @temp_result;

end if;





-- ---------------------------------------------- ACTIVATE SCREEN CODE ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 

 if IN_SCREEN ="Active"
 Then 
    
    Set @IN_SCREEN="Enabled";
   
    -- DISPLAY SCRREN
 	
 	if IN_AGGREGATION_3 =''
		 	Then	
			
					
			     SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Activate_ID",`ID`,"Activate_name",`Name`,"Activat_Campaign",`CampaignName`,"Activate_msg",Message,"Activate_segment",`CLMSegmentName`,"Activate_comms",`Coverage`,"Activate_date",ModifiedDate,"Status_QC",`Status_QC`,"Status_DLT",`Status_DLT`,"Status_Schedule",`Status_Schedule`,"Status_Active",`Status_Active`,"More_Info",`More_Info`,"Scheduled_at",`Scheduled_at`,"Critical_Trigger",Critical_Trigger) )),"]")
									    into @temp_result from V_UI_Campaign_List;') ;
			      
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 


                if @temp_result is null
				  then set @temp_result="[]";
			    End if;	

			    SET out_result_json = @temp_result;

   End if;




-- 	EXECUTE BUTTON CLICKED

		 	if IN_AGGREGATION_3 !='' AND IN_AGGREGATION_4 ='EXECUTE'
		 	Then	

				    Set @Event_ID=IN_AGGREGATION_3;

					   
					
									
								-- Running the CLM for On Demand Event

								/*Insert The Record Into EEH_Summary*/
								INSERT INTO Event_Execution_History_Summary(Event_Id,Event_Execution_Date_Id,Start_Time,UI_Status)
                                SELECT @Event_ID,CAST(REPLACE(CURRENT_DATE(), '-', '') AS UNSIGNED),now(),'Submitted';	
								
								INSERT into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
								values ('S_CLM_Run',concat("Starting the CLM for Event_ID: ",@Event_ID," From UI ") ,now(),now(),'Started',@Event_ID);
											   
								INSERT into UI_Campaign_Task_List (UI_Campaign_Task, Run_Mode, Event_Id,Status,CreatedDate) 
								values ('S_CLM_Run',"RUN",@Event_ID,"START", current_timestamp());

								
								Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Request To Run CLM For This Event Has Been Submitted") )),"]") into @temp_result ;') ;
			   
								SELECT @Query_String;
								PREPARE statement from @Query_String;
								Execute statement;
								Deallocate PREPARE statement;
							
						
						SET out_result_json = @temp_result; 
				 				    
			End if;



	-- 	DEACTIVE OK  CLICKED
 	
		 	if  IN_AGGREGATION_3 !='' and IN_AGGREGATION_4 ='DEACTIVATE_OK'
		 		Then	

			 		
								

			 			 SET @Update_Event = concat('Update CLM_Campaign_Trigger A, CLM_Campaign_Events B
									      Set  A.CTState = 0  
									Where A.CampTriggerId= B.CampTriggerId
								      and  B.EventId in(',@Event_ID,');') ;
									  

                        SELECT @Update_Event;
						PREPARE statement from @Update_Event;
						Execute statement;
						Deallocate PREPARE statement;
						
						
						
			 			 SET @Update_Event = concat(' Update Event_Master_UI
					    	Set Status_Active ="InActive"
					    	Where Event_Id in (',@Event_ID,');') ;
						
						SELECT @Update_Event;
						PREPARE statement from @Update_Event;
						Execute statement;
						Deallocate PREPARE statement;
							
					
			 			 SET @Update_Event = concat(' Update Event_Master_UI
					    	Set ModifiedDate =current_timestamp()
					    	Where Event_Id  in (',@Event_ID,');') ;
						
						SELECT @Update_Event;
						PREPARE statement from @Update_Event;
						Execute statement;
						Deallocate PREPARE statement;
						
							
					 SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Activate_ID",`ID`,"Activate_name",`Name`,"Activat_Campaign",`CampaignName`,"Activate_msg",Message,"Activate_segment",`CLMSegmentName`,"Activate_comms",`Coverage`,"Activate_date",ModifiedDate,"Status_QC",`Status_QC`,"Status_DLT",`Status_DLT`,"Status_Schedule",`Status_Schedule`,"Status_Active",`Status_Active`,"More_Info",`More_Info`,"Critical_Trigger",Critical_Trigger,"Scheduled_at",`Scheduled_at`) )),"]")
											into @temp_result from V_UI_Campaign_List;') ;
					  
					SELECT @Query_String;
					PREPARE statement from @Query_String;
					Execute statement;
					Deallocate PREPARE statement; 

                                    if @temp_result is null
									  then set @temp_result="[]";
									  End if;	

					    SET out_result_json = @temp_result;
				End if;			

    
end if;





-- ---------------------------------------------- INACTIVATE SCREEN CODE ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
 if IN_SCREEN ="Inactive"
 Then 

 	Set @IN_SCREEN="Disabled";

 	-- DISPLAY SCRREN
 	
 	if IN_AGGREGATION_3 =''
		 	Then	
			
		

			     SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Inactivate_ID",`ID`,"Inactivate_name",`Name`,"Inactivate_Campaign",`CampaignName`,"Inactivate_msg",Message,"Inactivate_segment",`CLMSegmentName`,"Inactivate_comms",`Coverage`,"Inactivate_date",ModifiedDate,"Status_Schedule",`Status_Schedule`,"More_Info",`More_Info`,"Critical_Trigger",Critical_Trigger,"Scheduled_at",`Scheduled_at`) )),"]")
									    into @temp_result from V_UI_Campaign_List;') ;
			      
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 

                  if @temp_result is null
					then set @temp_result="[]";
				 End if;	


			    SET out_result_json = @temp_result;
   End if;



   
	-- 	ACTIVE OK  CLICKED
 	
		 	if IN_AGGREGATION_3 !='' and IN_AGGREGATION_4 ='ACTIVATE_OK'
		 		Then	
 
			 			
			 			 SET @Update_Event = concat('Update CLM_Campaign_Trigger A, CLM_Campaign_Events B
									      Set  A.CTState = 1 
									Where A.CampTriggerId= B.CampTriggerId
								      and  B.EventId in (',@Event_ID,');') ;
									  

                        SELECT @Update_Event;
						PREPARE statement from @Update_Event;
						Execute statement;
						Deallocate PREPARE statement;
                   
				    
			 			 SET @Update_Event = concat('Update Event_Master_UI
					    	Set Status_Active ="Active"
					    	Where Event_Id  in (',@Event_ID,');') ;
						
						SELECT @Update_Event;
						PREPARE statement from @Update_Event;
						Execute statement;
						Deallocate PREPARE statement;
							
					
					
			 			 SET @Update_Event = concat(' Update Event_Master_UI
					    	Set ModifiedDate =current_timestamp()
					    	Where Event_Id  in (',@Event_ID,');') ;
						
						SELECT @Update_Event;
						PREPARE statement from @Update_Event;
						Execute statement;
						Deallocate PREPARE statement;
							
			     SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Inactivate_ID",`ID`,"Inactivate_name",`Name`,"Inactivate_Campaign",`CampaignName`,"Inactivate_msg",Message,"Inactivate_segment",`CLMSegmentName`,"Inactivate_comms",`Coverage`,"Inactivate_date",ModifiedDate,"Status_Schedule",`Status_Schedule`,"More_Info",`More_Info`,"Critical_Trigger",Critical_Trigger,"Scheduled_at",`Scheduled_at`) )),"]")
									    into @temp_result from V_UI_Campaign_List;') ;
			      
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 

                                    if @temp_result is null
									  then set @temp_result="[]";
									  End if;	

					    SET out_result_json = @temp_result;
				End if;	

end if;



-- ------------------------------------------------------------ RUNNING SCREEN --------------------------------------------------------------------------------------------------------------

 If IN_SCREEN = "RUNNING" AND IN_AGGREGATION_3 !="CLM_Logs"
 Then
          

        SELECT SUBSTRING_INDEX(job_name,":",-1) into @Event_ID
		from  ETL_Execution_Details
		where Procedure_Name = "S_CLM_Event_Execution" 
		and job_name  like "RUN:%"
		order by Execution_ID desc
		Limit 1;



        	SELECT date_format(Created_Date,"%d-%m %h:%i") into @Start_Time
		from  Event_Execution_Current_Run
		Limit 1;
		
		If @Start_Time is Null or @Start_Time=''
		Then
		Set @Start_Time= date_format(current_Timestamp(),"%d-%m %h:%i");
		End If;

		Select `End` into @CLM_Status from CLM_Process_Log order by Id Desc limit 1;
	
		
		Select max(Event_Execution_Date_Id)into @Event_Execution_Date_Id  from Event_Execution_Current_Run;
		
		If @Event_Execution_Date_Id='' or @Event_Execution_Date_Id is null
		  THEN 	Select replace(current_date(),'-','')into @Event_Execution_Date_Id;
		End If;

		Select  @CLM_Status,@Event_ID,@Event_Execution_Date_Id,@Start_Time;
       	
		If 	 @CLM_Status is null 
			Then	
					Drop View if EXISTS V_UI_Campaign_Running_Screen;
					
				SET @view_stmt3=concat('CREATE OR REPLACE VIEW V_UI_Campaign_Running_Screen AS
									(Select Case when Id Is Null then "" else ID end as Id,
									Case when Id Is Null then "" else Name end as Name,
									Case when Id Is Null then "" else CampaignName end as CampaignName,	
									Case when Id Is Null then "" else Creative1 end as Message,
									Case when Id Is Null then "" else CLMSegmentName end as CLMSegmentName,
									-- Case when Id Is Null then "" else Convert(format(Count(Event_Execution_Id),","),CHAR)  end as Coverage,
									Case when Id Is Null then "" else "SOLUS Engine is Running "  end as Coverage,
									Case when Id Is Null then "" else "Running" end as Status,
									Case when Id Is Null then "" else "',@Start_Time,'" end as Started_At
									from  Event_Master
									where Id=',@Event_ID,'
									Group by Id
								  )');
								  Select @view_stmt3;
  								PREPARE statement from @view_stmt3;
								Execute statement;
								Deallocate PREPARE statement; 

						
		End If;

	
		If 	 @CLM_Status= "ENDED"
			Then	
			
				Drop View if EXISTS V_UI_Campaign_Running_Screen;
				 
				SET @view_stmt1=concat('CREATE OR REPLACE VIEW V_UI_Campaign_Running_Screen AS
										( Select Case when Id Is Null then "" else ID end as Id,
											Case when Id Is Null then "" else Name end as Name,
											Case when Id Is Null then "" else CampaignName end as CampaignName,			
											Case when Id Is Null then "" else Creative1 end as Message,
											Case when Id Is Null then "" else CLMSegmentName end as CLMSegmentName,
											Case when Id Is Null then "" else No_Of_Communication_Sent end as Coverage,
											Case when Id Is Null then "" else "Completed" end as Status,
											Case when Id Is Null then "" else "',@Start_Time,'" end as Started_At
											from  Event_Master A,Event_Master_UI_Temp B 
											where A.Id=B.Event_ID 
											and Event_Execution_Date_Id=',@Event_Execution_Date_Id,'
											Group by Id
										)
										');
				Select @view_stmt1;
  				PREPARE statement from @view_stmt1;
				Execute statement;
				Deallocate PREPARE statement; 

						
		End If;
		
		If 	 @CLM_Status= "FAILED"
			Then	
			
					Drop View if EXISTS V_UI_Campaign_Running_Screen;
				SET @view_stmt2=concat('CREATE OR REPLACE VIEW V_UI_Campaign_Running_Screen AS
										( Select Case when Id Is Null then "" else ID end as Id,
											Case when Id Is Null then "" else Name end as Name,
											Case when Id Is Null then "" else CampaignName end as CampaignName,	
											Case when Id Is Null then "" else Creative1 end as Message,
											Case when Id Is Null then "" else CLMSegmentName end as CLMSegmentName,
											Case when Id Is Null then "" else "NA" end as Coverage,
											Case when Id Is Null then "" else "FAILED" end as Status,
											Case when Id Is Null then "" else "',@Start_Time,'" end as Started_At
											from  Event_Master A,Event_Master_UI B  
											where A.Id=B.Event_ID and Id=', @Event_ID,'
											Group by Id
										)
										');
				Select @view_stmt2;
  				PREPARE statement from @view_stmt2;
				Execute statement;
				Deallocate PREPARE statement; 

						
		End If;

				

		SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Running_ID",`ID`,"Running_name",`Name`,"Running_Campaign",`CampaignName`,"Running_msg",`Message`,"Running_segment",`CLMSegmentName`,"Running_comms",`Coverage`,"Started_At",Started_At,"Status_Progress",`Status`) )),"]")
							    into @temp_result from V_UI_Campaign_Running_Screen ;') ;
							      
		SELECT @Query_String;
		PREPARE statement from @Query_String;
		Execute statement;
		Deallocate PREPARE statement; 
								
							 
		if @temp_result is null
			then set @temp_result="[]";
		End if;
	
		SET out_result_json = @temp_result;		
	END IF;
	
	IF IN_SCREEN = "RUNNING" AND IN_AGGREGATION_3 ="CLM_Logs"
		THEN
			SET @Query_String =  'SELECT job_name
				    into @clmresult from  ETL_Execution_Details
					where Procedure_Name = "S_CLM_Event_Execution" or Procedure_Name = "S_CLM_Run"
					or Procedure_Name ="S_CLM_Communication_File" or Procedure_Name ="S_CLM_Communication_File_Gen_CustLoop"
					order by Execution_ID desc
					Limit 1;' ;
					
				      
					SELECT @Query_String;
					PREPARE statement from @Query_String;
					Execute statement;
					Deallocate PREPARE statement; 
					
					SELECT SUBSTRING_INDEX(job_name,":",-1) into @Event_ID
						from  ETL_Execution_Details
						where Procedure_Name = "S_CLM_Event_Execution" 
						and job_name  like "RUN:%"
						order by Execution_ID desc
						Limit 1;
					Select Count(1) into @Customer_Count from Event_Execution_Current_Run where Is_Target ="Y";
					
					Select `End` into @CLM_Status from CLM_Process_Log order by Id Desc limit 1;
					
					Select @clmresult,@CLM_Status,@Event_ID;
					
					IF (@CLM_Status ='ENDED' or @CLM_Status ='FAILED') and (@Customer_Count > 0 or @Customer_Count <= 0)  and (@clmresult ="CLM Completed !!" or @clmresult Like"%No data - zero rows fetched, selected, or processed%"  or @clmresult Like"% does not exist%" or @clmresult Like"% doesn't exist%" or @clmresult like "%'field list'%" or @clmresult Like"%Incorrect number of arguments%" or @clmresult Like "%ERROR : Hold Out has less customers than the specified value%")
						
						THEN 
							Select 1;
							INSERT into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
                            values ('S_CLM_Run',concat("Completed the CLM From UI ") ,now(),now(),'Started',@Event_ID);
								   
					End  IF;
					
					
			        SELECT Execution_ID into @minExecution_ID 
					from  ETL_Execution_Details
					where job_name like "%Started" and Procedure_Name = "S_CLM_Run"
					order by Execution_ID desc
					Limit 1;
						
					SELECT Execution_ID into @maxExecution_ID 
					from  ETL_Execution_Details
					where job_name like "%Completed the CLM for Event_ID:%"
					order by Execution_ID desc
					Limit 1;
						
												
					If @maxExecution_ID is NULL  or (@maxExecution_ID <=@minExecution_ID)
						Then 
							SET @stmt=concat('SELECT max(Execution_ID)+1 into @maxExecution_ID FROM ETL_Execution_Details  ORDER BY Execution_ID DESC LIMIT 1;');
							select @stmt;
							PREPARE statement from @stmt;
							Execute statement;
							Deallocate PREPARE statement;
					End If;
					Select @minExecution_ID,@maxExecution_ID;
	
					SET @stmt=concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("s_no",S_NO,"proc_name",Procedure_Name,"Message",job_name,"start_time",Start_Time,"end_time",End_Time,"status",`Status`) ORDER BY Execution_ID ASC)),"]")into @temp_result
					From
						(	
						
							Select  "<b>Message</b>" as job_name,"0" as S_NO,"0" as Execution_ID,"Step" as Procedure_Name,"Start Time(DD-MM HR:MM)" as Start_Time,"End Time(DD-MM HR:MM)" as 		End_Time,   "Status" as Status
							Union 
						
							Select Distinct job_name,row_number() over(order by Execution_ID asc) as `S_NO`,Execution_ID,Procedure_Name,Start_Time,End_Time,Status
															From 
															
															(SELECT Execution_ID,
																Procedure_Name,
																CASE
																	WHEN Procedure_Name ="S_CLM_Run_Pre_Checks" then "Checking the avability of the SOLUS Engine..."
																	WHEN job_name like 	"Starting the CLM for Event_ID:%" Then Concat(Replace(Replace (job_name ,"Starting the CLM for Event_ID:","Campaign Started for the SOLUS_Id"),"From UI ","at "),date_format(Start_Time,"%d-%m %h:%i"))
																	WHEN Procedure_Name = "S_CLM_Event_Execution" and job_name like "RUN%" THEN "Selecting the Customers for the Campaign..."
																	
																	WHEN Procedure_Name = "S_CLM_Event_Execution" and (job_name  like "%Selecting the Customers%") THEN job_name
																	WHEN Procedure_Name = "S_CLM_Event_Execution" and ( job_name  Like  "%Reason%") THEN job_name
																	WHEN Procedure_Name = "S_CLM_Event_Execution" and ( job_name  Like  "%TotalRejected%") THEN SUBSTRING_INDEX(job_name,":",-22)
																	WHEN job_name = "" THEN Concat("Customer Selection Completed  Successfully !! Starting Construction of Messages at ",date_format(Start_Time,"%d-%m %h:%i"),"....")
																	WHEN Procedure_Name = "S_CLM_Communication_File_Gen_CustLoop" THEN concat("Constructing Message at  ",date_format(Start_Time,"%d-%m %h:%i"))
																	WHEN Procedure_Name = "S_CLM_Communication_File" THEN "Constructing Message"
																	WHEN job_name like  "%CLM IS COMPLETE%" THEN concat("Message Generated Successfully at ",date_format(Start_Time,"%d-%m %h:%i"))
																	WHEN job_name like  "%Completed the CLM for Event_ID%" THEN "Campaign Generated Successfully !!"
																	WHEN job_name Like "% does not exist%" THEN Concat("CLM Failed due to ",job_name," .Please connect with the Team ")
																	WHEN job_name Like "% does %" THEN Concat("CLM Failed due to ",job_name," .Please connect with the Team ")
																	WHEN job_name like "% field list %" THEN Concat("CLM Failed due to ",job_name," .Please connect with the Team ")
																	WHEN job_name Like "%Incorrect number of arguments%" THEN Concat("CLM Failed due to ",job_name," .Please connect with the Team ")
																	WHEN job_name Like "%Failed%" THEN Concat("CLM Failed due to ",job_name," .Please connect with the Team ")
																	WHEN job_name Like "%CLM is already running%" THEN "CLM is already running"
																	WHEN job_name Like "%error%" THEN Concat("CLM Failed due to ",job_name," .Please connect with the Team ")
	
																ELSE job_name
																END AS job_name,
																date_format(Start_Time,"%d-%m %h:%i") as Start_Time,
																Case when job_name like  "%No data - zero rows fetched, selected, or processed%" Then date_format(Start_Time,"%d-%m %h:%i")
																	Else date_format(End_Time,"%d-%m %h:%i")
																End as End_Time,
																Case when Status ="" then "Succeeded" Else Status end as Status
															FROM ETL_Execution_Details
															WHERE (Procedure_Name like "S_CLM%" or Procedure_Name like "S_Exclusion_Filter_Execution")
															 and Procedure_Name <> "S_CLM_Event_Attribution"
															and ((job_name is not null) and( job_name not like "%No data - zero rows fetched, selected, or processed%")
																	and (job_name !="Started"))
																	
															and
															Execution_ID between ',@minExecution_ID,' and ',@maxExecution_ID,'
															
															
															ORDER BY Execution_ID ASC)A
															Group by job_name 
															Order by Execution_ID ASC
														

						   )A 
							');
					
					select @stmt;
					PREPARE statement from @stmt;
					Execute statement;
					Deallocate PREPARE statement;
							
					Select @temp_result;
					
					Select Count(1) into @Running_Screen_CNT FROM  information_schema.tables where Table_Name='V_UI_Campaign_Running_Screen';		
					if @Running_Screen_CNT is null or @Running_Screen_CNT=0 or @Running_Screen_CNT =''
						then 
						
								SET @stmt=concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("s_no",S_NO,"proc_name",Procedure_Name,"Message",job_name,"start_time",Start_Time,"end_time",End_Time,"status",`Status`) ORDER BY Execution_ID ASC)),"]")into @temp_result
					From
						(	
						
							Select  "<b>Message</b>" as job_name,"0" as S_NO,"0" as Execution_ID,"Step" as Procedure_Name,"Start Time(DD-MM HR:MM)" as Start_Time,"End Time(DD-MM HR:MM)" as 		End_Time,   "Status" as Status
							Union 
						
							Select Distinct job_name,row_number() over(order by Execution_ID asc) as `S_NO`,Execution_ID,Procedure_Name,Start_Time,End_Time,Status
															From 
															
															(SELECT Execution_ID,
																Procedure_Name,
																CASE
																	WHEN Procedure_Name ="S_CLM_Run_Pre_Checks" then "Checking the avability of the SOLUS Engine..."
																	WHEN job_name like 	"Starting the CLM for Event_ID:%" Then Concat(Replace(Replace (job_name ,"Starting the CLM for Event_ID:","Campaign Started for the SOLUS_Id"),"From UI ","at "),date_format(Start_Time,"%d-%m %h:%i"))
																	WHEN Procedure_Name = "S_CLM_Event_Execution" and (job_name like "RUN%" or job_name  like "%TEST%" ) THEN "Selecting the Customers for the Campaign..."
																	
																	WHEN Procedure_Name = "S_CLM_Event_Execution" and (job_name  like "%Selecting the Customers%") THEN job_name
																	WHEN Procedure_Name = "S_CLM_Event_Execution" and ( job_name  Like  "%Reason%") THEN job_name
																	WHEN Procedure_Name = "S_CLM_Event_Execution" and ( job_name  Like  "%TotalRejected%") THEN SUBSTRING_INDEX(job_name,":",-22)
																	WHEN job_name = "" THEN Concat("Customer Selection Completed  Successfully !! Starting Construction of Messages at ",date_format(Start_Time,"%d-%m %h:%i"),"....")
																	WHEN Procedure_Name = "S_CLM_Communication_File_Gen_CustLoop" THEN concat("Constructing Message at  ",date_format(Start_Time,"%d-%m %h:%i"))
																	WHEN Procedure_Name = "S_CLM_Communication_File" THEN "Constructing Message"
																	WHEN job_name like  "%CLM IS COMPLETE%" THEN concat("Message Generated Successfully at ",date_format(Start_Time,"%d-%m %h:%i"))
																	WHEN job_name like  "%Completed the CLM for Event_ID%" THEN "Campaign Generated Successfully !!"
																	WHEN job_name Like "% does not exist%" THEN Concat("CLM Failed due to ",job_name," .Please connect with the Team ")
																	WHEN job_name Like "% does %" THEN Concat("CLM Failed due to ",job_name," .Please connect with the Team ")
																	WHEN job_name like "% field list %" THEN Concat("CLM Failed due to ",job_name," .Please connect with the Team ")
																	WHEN job_name Like "%Incorrect number of arguments%" THEN Concat("CLM Failed due to ",job_name," .Please connect with the Team ")
																	WHEN job_name Like "%Failed%" THEN Concat("CLM Failed due to ",job_name," .Please connect with the Team ")
																	WHEN job_name Like "%CLM is already running%" THEN "CLM is already running"
																	WHEN job_name Like "%error%" THEN Concat("CLM Failed due to ",job_name," .Please connect with the Team ")
	
	
																ELSE job_name
																END AS job_name,
																date_format(Start_Time,"%d-%m %h:%i") as Start_Time,
																Case when job_name like  "%No data - zero rows fetched, selected, or processed%" Then date_format(Start_Time,"%d-%m %h:%i")
																	Else date_format(End_Time,"%d-%m %h:%m")
																End as End_Time,
																Case when Status ="" then "Succeeded" Else Status end as Status
															FROM ETL_Execution_Details
															WHERE (Procedure_Name like "S_CLM%" or Procedure_Name like "S_Exclusion_Filter_Execution")
															 and Procedure_Name <> "S_CLM_Event_Attribution"
															and ((job_name is not null) and( job_name not like "%No data - zero rows fetched, selected, or processed%")
																	and (job_name !="Started"))
																	
															and
															Execution_ID between ',@minExecution_ID,' and ',@maxExecution_ID,'
															
															
															ORDER BY Execution_ID ASC)A
															Group by job_name 
															Order by Execution_ID ASC
															limit 50
														

						   )A 
							');
					
					select @stmt;
					PREPARE statement from @stmt;
					Execute statement;
					Deallocate PREPARE statement;
					
					End if;
										
					if @temp_result is null
						then set @temp_result="[]";
					End if;
										
					SET out_result_json = @temp_result;

	End If;
		


IF IN_SCREEN ="Sent"
 Then 
 	SET @Query_String = CONCAT('SELECT CONCAT(
    "[",
    GROUP_CONCAT(
        JSON_OBJECT(
            "Sent_ID", A.Event_ID,
            "Event_Name",D.Name,
            "Sent_Execution", A.Event_Execution_Date_Id,
            
            
            "Start_Time", A.Start_Time,
            "End_Time", A.End_Time,
            "Time",Duration,
            "UI_Status",UI_Status,
            "Progress",concat(Progress,"%"),
            "Sent_comms", No_Of_Comms,
            "Sent_rejected", NO_Of_Rejected_Comms,
            "Funnel_delivered", IFNULL(Target_Delivered, "-"),
            "Funnel_clicked", IFNULL(Target_CTA, "-"),
            "Funnel_opened", IFNULL(Target_Open, "-")
        ) ORDER BY CASE WHEN A.UI_Status = "Running" THEN 0 ELSE 1 END, A.Modified_Date DESC
	LIMIT 20
    ),
    "]"
) AS result
INTO @temp_result
FROM Event_Execution_History_Summary A
LEFT JOIN CLM_Event_Dashboard B ON A.Event_Id = B.Event_ID AND A.Event_Execution_Date_Id = B.Event_Execution_Date_ID
JOIN Event_Master_UI C ON A.Event_Id = C.Event_ID AND C.Type_Of_Event IN  ("',@Type_of_Event,'")
JOIN Event_Master D ON A.Event_Id = D.EventId;


');

                                              

											  
		SELECT @Query_String;
		PREPARE statement from @Query_String;
		Execute statement;
		Deallocate PREPARE statement;
        Select @temp_result;											
								
		IF IN_AGGREGATION_4 = "SEND_MESSAGE" 		
			THEN	Select IN_AGGREGATION_4;
					
					SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Successful") )),"]")
					    into @temp_result ;') ;
					      
					SELECT @Query_String;
					PREPARE statement from @Query_String;
					Execute statement;
					Deallocate PREPARE statement;
						
					Set @Event_ID=IN_AGGREGATION_3;
						 
					Set @UI_Campaign_Task=concat('Send_Campaign_RUN_',@Type_of_Channel);
								
					-- INSERT into UI_Campaign_Task_List (UI_Campaign_Task, Run_Mode, Event_Id,Status,CreatedDate) 
                    -- values (@UI_Campaign_Task,"FINAL",@Event_ID,"START", current_timestamp());	
								 
					If @temp_result is null
					 then set @temp_result="[]";
				    End if;

			    SET out_result_json = @temp_result;
		END IF;
		
		If @temp_result is null
					 then set @temp_result="[]";
				    End if;

			    SET out_result_json = @temp_result;


End if; 



 /*If IN_SCREEN ="Sent"
 Then 
 		SET @Query_String = CONCAT('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Sent_ID", A.Event_ID, "Sent_Execution", A.Event_Execution_Date_Id, "Time", Duration, "Sent_comms", `No_Of_Comms`, "Sent_rejected", `NO_Of_Rejected_Comms`, "Funnel_delivered", Target_Delivered, "Funnel_clicked", Target_CTA, "Funnel_opened", Target_Open))), "]")
        into @temp_result from CLM_Event_Dashboard A, Event_Execution_History_Summary B
        where A.Event_ID = B.Event_Id;');
                                              
                                              

											  
		SELECT @Query_String;
		PREPARE statement from @Query_String;
		Execute statement;
		
					      
	

	/* If IN_SCREEN ="Funnel"
	Then 
		
		If Filter_Date is null or Filter_Date =''
			Then	Set @Check_Date='-1';
		Else
			
				Set @Query_String= concat('Select Count(1) into @Check_Date from UI_Campaign_Funnel_Report_View where Event_Execution_Date_ID ="',Filter_Date,'" ;') ;
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 
		
		End If;	
			
		Select @Check_Date;
		If @Check_Date>=0
		Then
		
			Set @Query_String= concat('Create or replace View V_UI_Campaign_Funnel_Report_View as
										Select A.Event_ID,C.Name,C.Creative1,C.CLMSegmentName,Convert(format(Target_Base,","),CHAR) as 		Target_Base,Convert(format(Target_Delivered,","),CHAR) as Target_Delivered,Convert(format(Target_Open,","),CHAR) as Target_Open,Convert(format(Target_CTA,","),CHAR) as Target_CTA, date_format(A.Event_Execution_Date_ID,"%Y-%m-%d") as Event_Execution_Date_ID,rCore
										FROM UI_Campaign_Funnel_Report_View A,Event_Master_UI B,Event_Master C
										where A.Event_Id=B.Event_Id and A.Event_Id=C.Id and 
										B.Type_Of_Event="',@Type_of_Event,'" AND C.ChannelName="',@Type_of_Channel,'"
										and A.Event_Execution_Date_ID ="',Filter_Date,'" ;') ;

	   
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement; 
			
			Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Funnel_ID",Event_ID,"Funnel_name",Name,"Funnel_Msg",Creative1,"Funnel_segment",CLMSegmentName,"Funnel_sent",Target_Base,"Funnel_delivered",Target_Delivered,"Funnel_opened",Target_Open,"Funnel_clicked",Target_CTA,"Funnel_Date",Event_Execution_Date_ID,"Funnel_Rcore",rCore) )),"]") into @temp_result
                        FROM V_UI_Campaign_Funnel_Report_View
						  ;') ;
                          select * from V_UI_Campaign_Funnel_Report_View;

   
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement; 
		
			
									
		Else 
				Drop table if exists Event_Execution_Date_table;
				Create table Event_Execution_Date_table (Event_Execution_Date_ID varchar(128));
				insert into Event_Execution_Date_table
				Select Distinct Event_Execution_Date_ID from UI_Campaign_Funnel_Report_View order by Event_Execution_Date_ID desc limit 7;
			
			Set @Query_String= concat('Create or replace View V_UI_Campaign_Funnel_Report_View as
										Select A.Event_ID,C.Name,C.Creative1,C.CLMSegmentName,Convert(format(Target_Base,","),CHAR) as 		Target_Base,Convert(format(Target_Delivered,","),CHAR) as Target_Delivered,Convert(format(Target_Open,","),CHAR) as Target_Open,Convert(format(Target_CTA,","),CHAR) as Target_CTA, date_format(A.Event_Execution_Date_ID,"%Y-%m-%d") as Event_Execution_Date_ID,rCore
										FROM UI_Campaign_Funnel_Report_View A,Event_Master_UI B,Event_Master C
										where A.Event_Id=B.Event_Id and A.Event_Id=C.Id and 
										B.Type_Of_Event="',@Type_of_Event,'" AND C.ChannelName="',@Type_of_Channel,'"
										and A.Event_Execution_Date_ID in (Select Event_Execution_Date_ID from Event_Execution_Date_table )
										;') ;
                                       

	   
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement; 
			
			Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Funnel_ID",Event_ID,"Funnel_name",Name,"Funnel_Msg",Creative1,"Funnel_segment",CLMSegmentName,"Funnel_sent",Target_Base,"Funnel_delivered",Target_Delivered,"Funnel_opened",Target_Open,"Funnel_clicked",Target_CTA,"Funnel_Date",Event_Execution_Date_ID,"Funnel_Rcore",rCore) )),"]") into @temp_result
                        FROM V_UI_Campaign_Funnel_Report_View 
						  ;') ;

   
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement; 
		
			
			
		END IF;
		
				
		if @temp_result is null
			then set @temp_result="[]";
		End if;
							
		SET out_result_json = @temp_result;
	
	END IF; */
	
	
	

 	-- Send Clicked 

 	if (IN_SCREEN="TESTED" or IN_SCREEN="VERIFIED") and  IN_AGGREGATION_3 !=''  and IN_AGGREGATION_4 ='NUMBER_SENT'
		 	Then	
																 
								SET @Query_String0 =  concat('Update Event_Master_UI
															SET Status_RunMode = 0;
															;') ;
									      
								SELECT @Query_String0;
								PREPARE statement from @Query_String0;
								Execute statement;
								Deallocate PREPARE statement; 
								 
								 
									SET @Query_String1 =  concat('
															Update Event_Master_UI
															SET Status_RunMode=1
															where Event_ID in (',@Event_ID,');') ;
									      
								SELECT @Query_String1;
								PREPARE statement from @Query_String1;
								Execute statement;
								Deallocate PREPARE statement;  
				
				
								
							    SET @Query_String2 =  concat('Update Event_Master_UI
															SET Test_Moblie_Number_1="',IN_AGGREGATION_5,'",
																Test_Moblie_Number_2="',IN_AGGREGATION_6,'",
																Test_Moblie_Number_3="',IN_AGGREGATION_7,'",
																Test_Moblie_Number_4="',IN_AGGREGATION_8,'",
																Test_Moblie_Number_5="',IN_AGGREGATION_9,'"
																where Event_ID in (',@Event_ID,');') ;
									      
								SELECT @Query_String2;
								PREPARE statement from @Query_String2;
								Execute statement;
								Deallocate PREPARE statement; 
								
							Select type into @Medium_Of_Event_ID from Downstream_Adapter_Details A,Event_Master B where B.ChannelName=A.Channel and A.In_Use='Enable' and B.Id= @Event_ID; 	
							
							If @Medium_Of_Event_ID !=''
							Then
								SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Starting the Process") )),"]") into @temp_result ;') ;
					      
								SELECT @Query_String;
								PREPARE statement from @Query_String;
								Execute statement;
								Deallocate PREPARE statement;
						
						
								INSERT into UI_Campaign_Task_List (UI_Campaign_Task, Run_Mode, Event_Id,Status,CreatedDate) 
								  values ("Paste_File_On_SFTP"," ",@Event_ID,"START", current_timestamp());		
							
							ELSE 
								SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Please Enable the Medium of Sending the Communication") )),"]")into @temp_result ;') ;
					      
								SELECT @Query_String;
								PREPARE statement from @Query_String;
								Execute statement;
								Deallocate PREPARE statement;
						
							End If;		
								 if @temp_result is null
									 then set @temp_result="[]";
				     			  End if;
					
			    SET out_result_json = @temp_result;
	End if;		    
	
	if (IN_SCREEN="TESTED" or IN_SCREEN="VERIFIED") and  IN_AGGREGATION_3 !=''  and IN_AGGREGATION_4 ='Paste_File_On_SFTP_Status'
		 	Then	
						
							Select type into @Medium_Of_Event_ID from Downstream_Adapter_Details A,Event_Master B where B.ChannelName=A.Channel and A.In_Use='Enable' and B.Id= @Event_ID; 	
							
							If @Medium_Of_Event_ID !=''
							Then
								SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Processing ") )),"]")into @temp_result ;') ;
					      
								SELECT @Query_String;
								PREPARE statement from @Query_String;
								Execute statement;
								Deallocate PREPARE statement;
							ELSE	
								SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Please Enable the Medium of Sending the Communication") )),"]")into @temp_result ;') ;
					      
								SELECT @Query_String;
								PREPARE statement from @Query_String;
								Execute statement;
								Deallocate PREPARE statement;
						
							End If;		
					
								 
								 if @temp_result is null
									 then set @temp_result="[]";
				     			  End if;

			    SET out_result_json = @temp_result;
	End if;		    

	if (IN_SCREEN="TESTED" or IN_SCREEN="VERIFIED") and  IN_AGGREGATION_3 !=''  and IN_AGGREGATION_4 ='Download_Output_File'
		 	Then	
																 
				Select Communication_Template_Id into @V_ID from Event_Master where Id = @Event_ID;
				Select ChannelName into @V_Communication_Channel from Event_Master where Id = @Event_ID;
				Select `type` into @Medium from Downstream_Adapter_Details A,Event_Master B where B.ChannelName=A.Channel and A.In_Use='Enable' and B.Id=@Event_ID ;
				
				Select @V_ID,@V_Communication_Channel,@Medium ;
				CALL S_UI_Campaign_Paste_Filename("TEST",@V_ID,@V_Communication_Channel,"10000000",@Event_ID,@Out_FileName, @Out_FileDelimiter,@OUT_MarkerFile,@Out_File_Header);
				
				If @V_Communication_Channel="SMS" and @Medium !="SFTP"
				THEN
						SET @Query_String =  concat("Create or replace View Campaign_Test_Output_Files As
						Select * from (
						Select 'Mobile','Message' UNION ALL 
						SELECT trim(replace(Test_Moblie_Number_1,' ','')),concat('|\"',SUBSTRING_INDEX(trim(trailing '\n' from Message),'|',-1),'\"')
						FROM Event_Master_UI  WHERE Test_Moblie_Number_1 <>'' and Event_id=",@Event_ID,"
						UNION ALL  
						SELECT trim(replace(Test_Moblie_Number_2,' ','')),concat('|\"',SUBSTRING_INDEX(trim(trailing '\n' from Message),'|',-1),'\"')
						FROM Event_Master_UI  WHERE Test_Moblie_Number_2 <>'' and Event_id=",@Event_ID,"
						UNION ALL  
						SELECT trim(replace(Test_Moblie_Number_3,' ','')),concat('|\"',SUBSTRING_INDEX(trim(trailing '\n' from Message),'|',-1),'\"')
						FROM Event_Master_UI  WHERE Test_Moblie_Number_3<>'' and Event_id=",@Event_ID,"
						UNION ALL  
						SELECT trim(replace(Test_Moblie_Number_4,' ','')),concat('|\"',SUBSTRING_INDEX(trim(trailing '\n' from Message),'|',-1),'\"')
						FROM Event_Master_UI  WHERE Test_Moblie_Number_4 <>'' and Event_id=",@Event_ID,"
						UNION ALL  
						SELECT trim(replace(Test_Moblie_Number_5,' ','')),concat('|\"',SUBSTRING_INDEX(trim(trailing '\n' from Message),'|',-1),'\"')
						FROM Event_Master_UI  WHERE Test_Moblie_Number_5 <>'' and Event_id=",@Event_ID,"
						)A;	") ;
					      
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement;
																	
							
				End If;
					
					
					
			
							
			
	
				call S_dashboard_performance_download('Campaign_Test_Output_Files',@temp_result);
								
				
								 if @temp_result is null
									 then set @temp_result="[]";
				     			  End if;
				
	End if;	
	
If  IN_AGGREGATION_3 !=''  and IN_AGGREGATION_4 ='DISPLAY_CAMPAIGN_DETAIL'
THEN

Drop View if EXISTS V_UI_DISPLAY_CAMPAIGN_DETAIL;
 SET @view_stmt=concat('CREATE OR REPLACE VIEW V_UI_DISPLAY_CAMPAIGN_DETAIL AS
 
 (Select A.Name,ChannelName,Coverage,Template,A.Communication_Cooloff
 ,Waterfall,Event_Response_Days,CampaignThemeName,CampaignName,Type_Of_Event,Header,Message,
 Case when Replaced_Query is null or Replaced_Query="" then "NA" else Replaced_Query end as Replaced_Query,
 Case when Replaced_Query_Bill is null or Replaced_Query_Bill="" then "NA" else Replaced_Query_Bill end as Replaced_Query_Bill
 ,Event_Limit,
  Date(A.Start_Date_Id) as Scheduled_at
from    Event_Master A,Communication_Template B,Event_Master_UI C 
 where A.Id=C.Event_Id and  A.CreativeId1=B.id  and A.Id=',IN_AGGREGATION_3,'   
  )
 ');
 
 
 SELECT @view_stmt;
				PREPARE statement from @view_stmt;
				Execute statement;
				Deallocate PREPARE statement; 
				

 SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Channel",ChannelName,"Name",Name,"Target_Audience",Coverage,"Template",Template,"Cool_Off",Communication_Cooloff,"Priority",Waterfall,"Response_Window",Event_Response_Days,"Campaign_Theme",CampaignThemeName,"Campaign_Group",CampaignName,"Campaign_Type",Type_Of_Event,"File_Header",Header,"Message",Message,"Selection_Criteral",Replaced_Query,"Bill_Level_Selection_Criteral",Replaced_Query_Bill,"Throttling",Event_Limit ,"Schedule",Scheduled_at))),"]")
									    into @temp_result from  V_UI_DISPLAY_CAMPAIGN_DETAIL
									     ;') ;
			      
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 
				
				
End If;

	
If  IN_AGGREGATION_5 !=''  and IN_AGGREGATION_4 ='Download_Campaign_Data'
THEN


	
Drop View if EXISTS V_UI_Download_Campaign_Data;
SET @view_stmt=concat('CREATE OR REPLACE VIEW V_UI_Download_Campaign_DataL AS
 
 (Select A.Name,ChannelName,Coverage,Template,A.Communication_Cooloff
 ,Waterfall,Event_Response_Days,CampaignThemeName,CampaignName,Type_Of_Event,Header,Message,
 Case when Replaced_Query is null or Replaced_Query="" then "NA" else Replaced_Query end as Replaced_Query,
 Case when Replaced_Query_Bill is null or Replaced_Query_Bill="" then "NA" else Replaced_Query_Bill end as Replaced_Query_Bill
 ,Event_Limit,
  Date(A.Start_Date_Id) as Scheduled_at
from    Event_Master A,Communication_Template B,Event_Master_UI C 
 where A.Id=C.Event_Id and  A.CreativeId1=B.id  and A.State="',IN_AGGREGATION_5,'" and A.ChannelName="',IN_AGGREGATION_1,'" and C.Type_Of_Event="',IN_AGGREGATION_2,'" 
  )
 ');
 
 
SELECT @view_stmt;
PREPARE statement from @view_stmt;
Execute statement;
Deallocate PREPARE statement; 

call S_dashboard_performance_download('V_UI_Download_Campaign_DataL',@temp_result);
								
								
If @temp_result is null
 then set @temp_result="[]";
End if;
				
				
End If;




	If  IN_AGGREGATION_3 !=''  and IN_AGGREGATION_4 ='EDIT'
	
		THEN
			SET @EDIT_EVENT='';
			SET @EDIT_EVENT=IN_AGGREGATION_3;
			Select @EDIT_EVENT;
			
		   SET @Query_String =  concat('SELECT InputJson into @Json From Event_Master_UI where Event_ID ="',IN_AGGREGATION_3,'";') ;
					      
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement;
			
			Select @Json;
    		Set InputJson=@Json;
			Set InputJson = Replace(InputJson,'\n','\\n');
			Select InputJson;
			
		-- User Information
	   
		    SET @IN_USER_NAME = json_unquote(json_extract(InputJson,"$.USER"));
			
			IF @IN_USER_NAME IS NULL
				THEN
					Set  @IN_USER_NAME='';		
			END IF;
			
            SET @IN_SCREEN = json_unquote(json_extract(InputJson,"$.SCREEN"));
			
			IF @IN_SCREEN IS NULL
				THEN
					Set  @IN_SCREEN='';		
			END IF;
			
			
			SELECT Trigger_Type into @Trigger_Type FROM Event_Master_UI WHERE Event_Id =@EDIT_EVENT;
			
		
		-- ----------------------Target Audience------------------------------------------------------------------------------------------------------------------
		
			-- TRIGGER NAME
		    Set  @Trigger_Id='';
			Set  @Trigger_Name='';
			Select CampTriggerId into @Trigger_Id From  CLM_Campaign_Events where EventId =@EDIT_EVENT;
			Select CampTriggerName into @Trigger_Name  from CLM_Campaign_Trigger where CampTriggerId=@Trigger_Id;
			
		
		

			-- CAMPAIGN NAME
		    SET @Existing_Campaign_Id ='';
			 SET @Existing_Campaign_Name ='';
			 
			Select CampaignId into @Existing_Campaign_Id from CLM_Campaign_Events where  EventId=@EDIT_EVENT;		
			Select CampaignName into @Existing_Campaign_Name  from CLM_Campaign where CampaignId=@Existing_Campaign_Id;
			
			-- THEME NAME
		    SET @Existing_Theme_Id = '';
			SET @Existing_Theme_Name = '';
			Select CampaignThemeId into @Existing_Theme_Id from CLM_Campaign_Events where  EventId=@EDIT_EVENT;		
			Select CampaignThemeName into @Existing_Theme_Name  from CLM_CampaignTheme where CampaignThemeId=@Existing_Theme_Id;
			
			-- CAMPAIGN TYPE 
			Set  @Campaign_Type='';
		    Select Type_Of_Event into @Campaign_Type from Event_Master_UI where  Event_Id=@EDIT_EVENT;		
			
			-- REGION NAME
		    SET @Region_Id ='';
			Select RegionSegmentId into @Region_Id from CLM_Campaign_Events where  EventId=@EDIT_EVENT;		
			Select RegionSegmentName into @Region_Name  from CLM_RegionSegment where RegionSegmentId=@Region_Id;
			
			IF @Region_Id IS NULL or @Region_Id=''
				THEN
					set @Region_Id=''; set @Region_Name='';
					
			END IF;	
			
			-- CAMPAIGN SEGMENT
			
		    SET @Campaign_Segment_Id = '';
			Set @Campaign_Segment_Name='';
		

			Set @Query_String_Life= concat('Select Campaign_SegmentId into @Campaign_Segment_Id from CLM_Campaign_Events where  EventId=',@EDIT_EVENT,';') ;		
			SELECT @Query_String_Life;
			PREPARE statement from @Query_String_Life;
			Execute statement;
			Deallocate PREPARE statement; 

			IF  @Campaign_Segment_Id != ''
				THEN			
			Set @Query_String= concat('SELECT  GROUP_CONCAT(Segment_Name) INTO @Campaign_Segment_Name FROM  Campaign_Segment WHERE  Segment_Id in(',@Campaign_Segment_Id,');'					) ;
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement; 
			Set @Campaign_Segment_Id= Concat("[", @Campaign_Segment_Id,"]");
			Set @Campaign_Segment_Name= Concat("[", @Campaign_Segment_Name,"]");
			End If;
			
			IF @Campaign_Segment_Id IS NULL or  @Campaign_Segment_Id= ''
				THEN
					Set  @Campaign_Segment_Id='';	
					Set  @Campaign_Segment_Name='';						
			END IF;
			
			
			
			-- LIFECYCLE SEGMENT
			
			Set @Query_String_Life= concat('Select CLMSegmentId into @Lifecyle_Segment_Id from CLM_Campaign_Events where  EventId=',@EDIT_EVENT,';') ;		
			SELECT @Query_String_Life;
			PREPARE statement from @Query_String_Life;
			Execute statement;
			Deallocate PREPARE statement; 
		
			
			Set @Query_String= concat('SELECT  GROUP_CONCAT(CLMSegmentName) INTO @Lifecyle_Segment_Name FROM  CLM_Segment WHERE  CLMSegmentId in(',@Lifecyle_Segment_Id,');'					) ;
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement; 
			
			Set @Lifecyle_Segment_Id= Concat("[", @Lifecyle_Segment_Id,"]");
			Set @Lifecyle_Segment_Name= Concat("[", @Lifecyle_Segment_Name,"]");
			 
			 
			-- COMMUNICATION COOL OFF
			Set  @Cool_Off='';	
			Set  @Comm_Cool_Off='';					
			
			Select  Communication_Cooloff into @Cool_Off from CLM_Campaign_Events A,CLM_Creative B where A.CreativeId=B.CreativeId and EventId=@EDIT_EVENT;
			
			Select Comm_Cool_Off_Type into @Comm_Cool_Off from Event_Master_UI where  Event_Id=@EDIT_EVENT;
			
			
			IF @Cool_Off IS NULL
				THEN
					Set  @Cool_Off='';		
			END IF;
			
			IF @Comm_Cool_Off IS NULL
				THEN
					Set  @Comm_Cool_Off='';		
			END IF;
			
			
			-- COMMUNICATION PRIORITY
			Set @EventSequence='';
			Set @Comm_Priority='';
			SELECT EventSequence INTO @EventSequence
			FROM   (
					Select  row_number() over( order by Execution_Sequence asc,EventId desc) AS EventSequence,EventId,Execution_Sequence
						from CLM_Campaign_Events
				
					)a
					where   EventId=@EDIT_EVENT	;
				
				
			IF @EventSequence= 1
				THEN SET @Comm_Priority="HIGH_PRIORITY";
			ELSE 
					SET @Comm_Priority="MANUALLY_DEFINED";
			END IF;	
			
			Select Waterfall into @Event_TriggerUsesWaterfall from Event_Master where Id=@EDIT_EVENT;
			
			If 	@Event_TriggerUsesWaterfall ="CMABPerfBased" 
				THEN SET @Comm_Priority="PERFORMANCE_BASED";
					
			END IF;	
			
			If 	@Event_TriggerUsesWaterfall ="TopPriorityNoCoolOff" 
				THEN -- SET @Comm_Priority="HIGH_PRIORITY";
						SET @Comm_Cool_Off='IGNORE_COOL_OFF';
						SET  @Cool_Off='';	
			END IF;	
			
		Select max(Execution_Sequence) into @max_Event_Execution_Sequence from  Event_Master;
		Select (Execution_Sequence) into @Edit_Event_Execution_Sequence from  Event_Master where Id=@EDIT_EVENT;
		
		If @max_Event_Execution_Sequence=@Edit_Event_Execution_Sequence
			Then
					SET @Comm_Priority="LOW_PRIORITY";
							
			END IF;
		
        
        
        -- ---Propensity Based Trigger ------------------
		
		Set @Mcore_Module_Id='';
        SET @MCore_Module_Check = 'NO';
			Select PropensityModel_Id into @Mcore_Module_Id from Event_Master where Id =@EDIT_EVENT;
				
			IF @Mcore_Module_Id ='-1'
				THEN
					Set  @Mcore_Module_Id='';
                    SET @MCore_Module_Check = 'NO';		
			END IF;
					
			
			IF @Mcore_Module_Id IS NULL
				THEN
					Set  @Mcore_Module_Id='';
                    SET @MCore_Module_Check = 'NO';		
			END IF;
					
					
			
			Set @Mcore_Module_Name='';		
			IF @Mcore_Module_Id !=''
				THEN	
                SET @MCore_Module_Check = 'YES';	
                    Select Description into @Mcore_Module_Name from CLM_MCore_Model_Master where Id =@Mcore_Module_Id;
                    Set  @Comm_Priority="PREDICTIVE_SCORE";	
				
                Set @Predictive_Score='';	
                Select  
                
                Case when PropensityTopN_Percentile like "0.0%" then "0"
                when PropensityTopN_Percentile like "0.1%" then "10"
                when PropensityTopN_Percentile like "0.2%" then "20"
                when PropensityTopN_Percentile like "0.3%" then "30"
                when PropensityTopN_Percentile like "0.4%" then "40"
                when PropensityTopN_Percentile like "0.5%" then "50"
                when PropensityTopN_Percentile like "0.6%" then "60"
                when PropensityTopN_Percentile like "0.7%" then "70"
                when PropensityTopN_Percentile like "0.8%" then "80"
                when PropensityTopN_Percentile like "0.9%" then "90"
                when PropensityTopN_Percentile like "1%" then "100"
                else PropensityTopN_Percentile
                end
                
                into @Predictive_Score from Event_Master where Id =@EDIT_EVENT;
            
            END IF; 
			
			IF @Predictive_Score IS NULL
				THEN
					Set  @Predictive_Score='';		
			END IF;
            
            /* Segment Logic For Propensity */
            
            SET @ALL_Check = '';
            
            SELECT CLMSegmentName INTO @ALL_Check FROM Event_Master
            WHERE Id =@EDIT_EVENT;
            
            IF @ALL_Check = "ALL"
            THEN 
				
                SET @Propensity_Segment_Id = "All_segment";
                
                
                
                SET @Propensity_Segment_Name = "All";
                
			ELSE 
                     
                     
                     
               SET @Propensity_Segment_Id = REPLACE(REPLACE(@Lifecyle_Segment_Id,'[',''),']','');
               
               SET @Propensity_Segment_Id = REPLACE(@Propensity_Segment_Id,'"','');

               
               SET @Propensity_Segment_Name = CONCAT('"',REPLACE(REPLACE(@ALL_Check,'(',''),')',''),'"');
               
               SET @Propensity_Segment_Name = REPLACE(@Propensity_Segment_Name,'"','');
               
			END IF;
            
            
            
        -- --- Communication Priority ---------    
			
		IF @Comm_Priority IS NULL OR @Comm_Priority=''
				THEN
					SET @Comm_Priority="MANUALLY_DEFINED";
							
			END IF;
			
		
			
			Set  @Execution_After='';
			Set  @Execution_After_Name='';				
			IF @Comm_Priority="MANUALLY_DEFINED"
				THEN
						Select Higher_Trigger_id into @Execution_After from
							(Select EventId,Execution_Sequence,lastseq,dep,
								lag(EventId)over(order by dep) as Higher_Trigger_id
										from (select EventId,Execution_Sequence,
												lag(Execution_Sequence)over(order by Execution_Sequence) as lastseq,row_number() over() as dep
												from CLM_Campaign_Events 
												order by Execution_Sequence asc
											)d1
							)d2 where EventId=@EDIT_EVENT;
											
			END IF;
			
					
			Select Name into @Execution_After_Name from Event_Master where Id=@Execution_After;
			
			IF @Execution_After IS NULL
				THEN
					Set  @Execution_After='';		
			END IF;
			
			IF @Execution_After_Name IS NULL
				THEN
					Set  @Execution_After_Name='';		
			END IF;
			
			-- THROOTTLING LIMIT
			Set  @Throttling_Level='';	
			Select Event_Limit into @Throttling_Level from CLM_Campaign_Events A,CLM_Campaign_Trigger B 
			where A.CampTriggerId=B.CampTriggerId and EventId=@EDIT_EVENT;
			
			IF @Throttling_Level IS NULL
				THEN
					Set  @Throttling_Level='';		
			END IF;
			-- ATTRIBUTION
			Set  @Attribution='';	
		    Select Event_Response_Days into @Attribution from Event_Master where Id=@EDIT_EVENT;
			
			IF @Attribution IS NULL
				THEN
					Set  @Attribution='';		
			END IF;
			
			-- FAVOURITE DAY
			Set  @Favourite_Day='';	
		    Select Favourite_Day into @Favourite_Day from Event_Master_UI where Event_Id=@EDIT_EVENT;	
			
			IF @Favourite_Day IS NULL
				THEN
					Set  @Favourite_Day='';		
			END IF;
			
			-- OFFER BASED
			Set  @Offer_Based='';
			Select Offer_Based into @Offer_Based from Event_Master_UI where Event_Id=@EDIT_EVENT;
			
			IF @Offer_Based IS NULL
				THEN
					Set  @Offer_Based='';		
			END IF;
			-- RECO BASED
			Set  @Reco_Based='';
			Select Reco_Based into @Reco_Based from Event_Master_UI where Event_Id=@EDIT_EVENT;
			
			IF @Reco_Based IS NULL
				THEN
					Set  @Reco_Based='';		
			END IF;
			
-- -------------------------------------Advanced RFMP Criteria----------------------------------------------------------------------------------------------------------

			-- VALID MOBILE
			Set  @Valid_Mobile='';	
			Select Valid_Mobile into @Valid_Mobile from Event_Master_UI	where Event_Id=@EDIT_EVENT;
			
			IF @Valid_Mobile IS NULL
				THEN
					Set  @Valid_Mobile='';		
			END IF;
			
			IF @Valid_Mobile= '1'
				THEN
					Set  @Valid_Mobile='Yes';		
			END IF;
			
			-- VALID EMAIL
			Set  @Valid_Email='';	
			Select Valid_Email into @Valid_Email from Event_Master_UI	where Event_Id=@EDIT_EVENT;
			
			IF @Valid_Email IS NULL
				THEN
					Set  @Valid_Email='';		
			END IF;
			
			IF @Valid_Email= '1'
				THEN
					Set  @Valid_Email='Yes';		
			END IF;
			-- DND
			
			Set  @DND='';	
			Select DND into @DND from Event_Master_UI where Event_Id=@EDIT_EVENT;
			
			IF @DND IS NULL
				THEN
					Set  @DND='';		
			END IF;
			
			-- RECECNY START
			Set  @Recency_Start='';
		    Select Recency_Start into @Recency_Start from Event_Master_UI where Event_Id=@EDIT_EVENT;	
			
			IF @Recency_Start IS NULL
				THEN
					Set  @Recency_Start='';		
			END IF;
			
			-- RECECNY END
				Set  @Recency_End='';
		    Select Recency_End into @Recency_End from Event_Master_UI where Event_Id=@EDIT_EVENT;	
			
			IF @Recency_End IS NULL
				THEN
					Set  @Recency_End='';		
			END IF;
			
			-- VISIT
			Set  @Visit_Start='';
			Set  @Visit_End='';	
		    Select Visit_Start into @Visit_Start from Event_Master_UI where Event_Id=@EDIT_EVENT;	
			
			IF @Visit_Start IS NULL
				THEN
					Set  @Visit_Start='';		
			END IF;
			
			Select Visit_End into @Visit_End from Event_Master_UI where Event_Id=@EDIT_EVENT;	
			
			IF @Visit_End IS NULL
				THEN
					Set  @Visit_End='';		
			END IF;
			
			
			-- MONETARY VALUE
			Set  @Monetary_Value_Start='';	
			Set  @Monetary_Value_End='';
		    Select Monetary_Value_Start into @Monetary_Value_Start from Event_Master_UI where Event_Id=@EDIT_EVENT;	
			IF @Monetary_Value_Start IS NULL
				THEN
					Set  @Monetary_Value_Start='';		
			END IF;
			
			Select Monetary_Value_End into @Monetary_Value_End from Event_Master_UI where Event_Id=@EDIT_EVENT;	
			IF @Monetary_Value_End IS NULL
				THEN
					Set  @Monetary_Value_End='';		
			END IF;
			
			-- DISCOUNT
			Set  @Discount='';	
			Select Discount into @Discount from Event_Master_UI where Event_Id=@EDIT_EVENT;	
			
			IF @Discount IS NULL
				THEN
					Set  @Discount='';		
			END IF;
			
			-- LAST BOUGHT CATEGORY
			Set  @Last_Bought_Category='';	
		    Select Last_Bought_Category into @Last_Bought_Category from Event_Master_UI where Event_Id=@EDIT_EVENT;	
			IF @Last_Bought_Category IS NULL
				THEN
					Set  @Last_Bought_Category='';		
			END IF;
			
			-- FAVOURITE CATEGORY
			Set  @Favourite_Category='';
		    Select Favourite_Category into @Favourite_Category from Event_Master_UI where Event_Id=@EDIT_EVENT;	
			
			IF @Favourite_Category IS NULL
				THEN
					Set  @Favourite_Category='';		
			END IF;
			
			
			-- AP FAVOURITE CATEGORY
			Set  @Active_Period_Category='';
		    Select AP_Favourite_Category into @Active_Period_Category from Event_Master_UI where Event_Id=@EDIT_EVENT;	
			
			IF @Active_Period_Category IS NULL
				THEN
					Set  @Active_Period_Category='';		
			END IF;
			
			-- Purchase_Anniversary
			Set  @Purchase_Anniversary='';	
		    Select Purchase_Anniversary into @Purchase_Anniversary from Event_Master_UI where Event_Id=@EDIT_EVENT;	
			
			IF @Purchase_Anniversary IS NULL
				THEN
					Set  @Purchase_Anniversary='';		
			END IF;
			
			
			-- Multiple_of_5
			Set  @Multiple_of_5='';	
		    Select Multiple_of_5 into @Multiple_of_5 from Event_Master_UI where Event_Id=@EDIT_EVENT;	
			
			IF @Multiple_of_5 IS NULL
				THEN
					Set  @Multiple_of_5='';		
			END IF;
			

-- ----------------------------Construct Creatives -----------------------------------------------------------------------------------------------------------------
			
			-- CHANNEL
		    Select  upper(ChannelName) into @Channel from Event_Master where Id=@EDIT_EVENT;
			
			-- PERSONALISED RECO VIA SOLUS SHORTENER
		    
			SET @Personalised_Reco_via_Solus_Shortener = json_unquote(json_extract(InputJson,"$.Personalised_Reco_via_Solus_Shortener"));
			
			IF @Personalised_Reco_via_Solus_Shortener IS NULL
				THEN
					Set  @Personalised_Reco_via_Solus_Shortener='';		
			END IF;
			
			-- PERSONALISED
			
			Set  @Personalization='';
			
		    If @Channel ="SMS"
				THEN
					Select Replace(Replace(Creative1,"'","\'"),'"','\"') into  @Personalization from Event_Master where Id=@EDIT_EVENT;
						
					
			End If;	
			
			IF @Channel ="PN"
				THEN
					Select Replace(Replace(Creative1,"'","\'"),'"','\"') into  @Personalization  from Event_Master where Id=@EDIT_EVENT;
					
			End If;
			
			Set @Email_Adapter = Null;
            SET @sql_stmt000=concat('SELECT type into @Email_Adapter from Downstream_Adapter_Details where Channel = "Email" and In_Use = "Enable";');
			   PREPARE statement from @sql_stmt000;
				Execute statement;
				Deallocate PREPARE statement;
				select @Email_Adapter;

				if @Email_Adapter is NULL
				Then set @Email_Adapter = 'SFTP';
				 End if;
			
			IF @Channel ="EMAIL" and @Email_Adapter = "SFTP"
				THEN
					Select Replace(Replace(Creative1,"'","\'"),'"','\"') into  @Personalization  from Event_Master where Id=@EDIT_EVENT;
					
			End If;
			
			Set @WhatsApp_Adapter = Null;
            SET @sql_stmt000=concat('SELECT type into @WhatsApp_Adapter from Downstream_Adapter_Details where Channel = "WhatsApp" and In_Use = "Enable";');
			   PREPARE statement from @sql_stmt000;
				Execute statement;
				Deallocate PREPARE statement;
			
			select @WhatsAPP_Adapter;
			if @WhatsApp_Adapter is NULL
			  Then set @WhatsAPP_Adapter = 'SFTP';
			End if;
			
			IF @Channel ="WHATSAPP" and @WhatsApp_Adapter = "SFTP"
				THEN
					Select Replace(Replace(Creative1,"'","\'"),'"','\"') into  @Personalization  from Event_Master where Id=@EDIT_EVENT;
					
			End If;


			
			IF @Personalization IS NULL
				THEN
					Set  @Personalization='';		
			END IF;
			
			Set  @Email_Template_Personalization='';
			Set  @Email_Subject_Personalization='';	
			
			SET @Email_Template_Personalization = json_unquote(json_extract(InputJson,"$.Email_Template_Personalization"));
			
			IF @Email_Template_Personalization IS NULL
				THEN
					Set  @Email_Template_Personalization='';		
			END IF;
			
			
			
			
			If @Channel ="Email"  and @Email_Adapter != "SFTP"
				THEN
					Select SUBSTRING_INDEX(Replace(Replace(Creative1,"'","\'"),'"','\"'),"|",-1) into @Email_Subject_Personalization from Event_Master where ChannelName="Email" and Id=@EDIT_EVENT;
			End If;	
							
			IF @Email_Subject_Personalization IS NULL
				THEN
					Set  @Email_Subject_Personalization='';		
			END IF;
			
			Set  @WhatsApp_Template_Id='';			
			Set  @WhatsApp_Banner_Personalization='';
			Set  @WhatsApp_Variable1_Personalization='';	
			Set  @WhatsApp_Variable2_Personalization='';
			Set  @WhatsApp_Variable3_Personalization='';		
			Set  @WhatsApp_Variable4_Personalization='';
			Set  @WhatsApp_Variable5_Personalization='';	
			Set  @WhatsApp_Variable5_Personalization='';
			Set  @WhatsApp_Variable6_Personalization='';
			Set  @WhatsApp_Variable7_Personalization='';
			Set  @WhatsApp_Variable8_Personalization='';
			Set  @WhatsApp_Variable9_Personalization='';
			Set  @WhatsApp_Variable10_Personalization='';
			
		
			If @Channel ="WhatsApp" and @WhatsApp_Adapter != "SFTP"
				THEN
				
					Select  length(Creative1)-length(Replace(Creative1,'|','')) into @Number_Of_Filed_In_WhatsApp from Event_Master where ChannelName="WhatsApp" and Id=@EDIT_EVENT;	
					
					If @Number_Of_Filed_In_WhatsApp >=3
					Then
					Select SUBSTRING_INDEX(SUBSTRING_INDEX(Replace(Replace(Creative1,"'","\'"),'"','\"'),"|",4),"|",-1) into @WhatsApp_Template_Id from Event_Master where ChannelName="WhatsApp" and Id=@EDIT_EVENT;
					
					End If;
					
					If @Number_Of_Filed_In_WhatsApp >=4
					Then
					Select SUBSTRING_INDEX(SUBSTRING_INDEX(Replace(Replace(Creative1,"'","\'"),'"','\"'),"|",5),"|",-1) into @WhatsApp_Banner_Personalization from Event_Master where ChannelName="WhatsApp" and Id=@EDIT_EVENT;
					End If;
					
					If @Number_Of_Filed_In_WhatsApp >=5
					Then
					Select SUBSTRING_INDEX(SUBSTRING_INDEX(Replace(Replace(Creative1,"'","\'"),'"','\"'),"|",6),"|",-1) into @WhatsApp_Variable1_Personalization from Event_Master where ChannelName="WhatsApp" and Id=@EDIT_EVENT;
					End If;
					
					If @Number_Of_Filed_In_WhatsApp >=6
					Then
					Select SUBSTRING_INDEX(SUBSTRING_INDEX(Replace(Replace(Creative1,"'","\'"),'"','\"'),"|",7),"|",-1) into @WhatsApp_Variable2_Personalization from Event_Master where ChannelName="WhatsApp" and Id=@EDIT_EVENT;
					End If;
					
					If @Number_Of_Filed_In_WhatsApp >=7
					Then
					Select SUBSTRING_INDEX(SUBSTRING_INDEX(Replace(Replace(Creative1,"'","\'"),'"','\"'),"|",8),"|",-1) into @WhatsApp_Variable3_Personalization from Event_Master where ChannelName="WhatsApp" and Id=@EDIT_EVENT;
					End If;
					
					If @Number_Of_Filed_In_WhatsApp >=8
					Then
					Select SUBSTRING_INDEX(SUBSTRING_INDEX(Replace(Replace(Creative1,"'","\'"),'"','\"'),"|",9),"|",-1) into @WhatsApp_Variable4_Personalization from Event_Master where ChannelName="WhatsApp" and Id=@EDIT_EVENT;
					End If;
					
					If @Number_Of_Filed_In_WhatsApp >=9
					Then
					Select SUBSTRING_INDEX(SUBSTRING_INDEX(Replace(Replace(Creative1,"'","\'"),'"','\"'),"|",10),"|",-1) into @WhatsApp_Variable5_Personalization from Event_Master where ChannelName="WhatsApp" and Id=@EDIT_EVENT;
					End If;
					
					If @Number_Of_Filed_In_WhatsApp >=10
					Then
					Select SUBSTRING_INDEX(SUBSTRING_INDEX(Replace(Replace(Creative1,"'","\'"),'"','\"'),"|",11),"|",-1) into @WhatsApp_Variable6_Personalization from Event_Master where ChannelName="WhatsApp" and Id=@EDIT_EVENT;
					End If;
					
					If @Number_Of_Filed_In_WhatsApp >=11
					Then
					Select SUBSTRING_INDEX(SUBSTRING_INDEX(Replace(Replace(Creative1,"'","\'"),'"','\"'),"|",12),"|",-1) into @WhatsApp_Variable7_Personalization from Event_Master where ChannelName="WhatsApp" and Id=@EDIT_EVENT;
					End If;
					
					If @Number_Of_Filed_In_WhatsApp >=12
					Then
					Select SUBSTRING_INDEX(SUBSTRING_INDEX(Replace(Replace(Creative1,"'","\'"),'"','\"'),"|",13),"|",-1) into @WhatsApp_Variable8_Personalization from Event_Master where ChannelName="WhatsApp" and Id=@EDIT_EVENT;
					End If;
					
					If @Number_Of_Filed_In_WhatsApp >=13
					Then
					Select SUBSTRING_INDEX(SUBSTRING_INDEX(Replace(Replace(Creative1,"'","\'"),'"','\"'),"|",14),"|",-1) into @WhatsApp_Variable9_Personalization from Event_Master where ChannelName="WhatsApp" and Id=@EDIT_EVENT;
					End If;
					
					If @Number_Of_Filed_In_WhatsApp =14
					Then
					Select SUBSTRING_INDEX(SUBSTRING_INDEX(Replace(Replace(Creative1,"'","\'"),'"','\"'),"|",15),"|",-1) into @WhatsApp_Variable10_Personalization from Event_Master where ChannelName="WhatsApp" and Id=@EDIT_EVENT;
					End If;
			End If;
			
			IF @WhatsApp_Template_Id IS NULL
				THEN
					Set  @WhatsApp_Template_Id='';		
			END IF;
					
			IF @WhatsApp_Banner_Personalization IS NULL
				THEN
					Set  @WhatsApp_Banner_Personalization='';		
			END IF;
		
			IF @WhatsApp_Variable1_Personalization IS NULL
				THEN
					Set  @WhatsApp_Variable1_Personalization='';		
			END IF;
				
			IF @WhatsApp_Variable2_Personalization IS NULL
				THEN
					Set  @WhatsApp_Variable2_Personalization='';		
			END IF;

			IF @WhatsApp_Variable3_Personalization IS NULL
				THEN
					Set  @WhatsApp_Variable3_Personalization='';		
			END IF;
			
			IF @WhatsApp_Variable4_Personalization IS NULL
				THEN
					Set  @WhatsApp_Variable4_Personalization='';		
			END IF;
					
			IF @WhatsApp_Variable5_Personalization IS NULL
				THEN
					Set  @WhatsApp_Variable5_Personalization='';		
			END IF;
								
			IF @WhatsApp_Variable6_Personalization IS NULL
				THEN
					Set  @WhatsApp_Variable6_Personalization='';		
			END IF;
			
			IF @WhatsApp_Variable7_Personalization IS NULL
				THEN
					Set  @WhatsApp_Variable7_Personalization='';		
			END IF;
							
			IF @WhatsApp_Variable8_Personalization IS NULL
				THEN
					Set  @WhatsApp_Variable8_Personalization='';		
			END IF;
							
			IF @WhatsApp_Variable9_Personalization IS NULL
				THEN
					Set  @WhatsApp_Variable9_Personalization='';		
			END IF;
							
			IF @WhatsApp_Variable10_Personalization IS NULL
				THEN
					Set  @WhatsApp_Variable10_Personalization='';		
			END IF;
							
				
			
			
			-- MICROSITE URL
			SET  @Microsite_URL='';
			Select Website_URL into @Microsite_URL from Event_Master where Id=@EDIT_EVENT;		
			
			
			IF @Microsite_URL IS NULL
				THEN
					Set  @Microsite_URL='';		
			END IF;
			
			
			Set @So2_Website_Deep_Link_Page =json_unquote(json_extract(InputJson,"$.So2_Website_Deep_Link_Page"));
			
			IF @So2_Website_Deep_Link_Page IS NULL
				THEN
					Set  @So2_Website_Deep_Link_Page='';		
			END IF;
			
			
			Set @So2_Product_Display_Page =json_unquote(json_extract(InputJson,"$.So2_Product_Display_Page"));
			
			IF @So2_Product_Display_Page IS NULL
				THEN
					Set  @So2_Product_Display_Page='';		
			END IF;
			
			
			Set @So2_Personalised_Smart_Basket_Recommendation_Page =json_unquote(json_extract(InputJson,"$.So2_Personalised_Smart_Basket_Recommendation_Page"));
			
			IF @So2_Personalised_Smart_Basket_Recommendation_Page IS NULL
				THEN
					Set  @So2_Personalised_Smart_Basket_Recommendation_Page='';		
			END IF;
			
			
			Set @So2_Dynamic_Landing_Page =json_unquote(json_extract(InputJson,"$.So2_Dynamic_Landing_Page"));
			
			IF @So2_Dynamic_Landing_Page IS NULL
				THEN
					Set  @So2_Dynamic_Landing_Page='';		
			END IF;
			
			If  @So2_Website_Deep_Link_Page='' and @So2_Product_Display_Page='' and @So2_Personalised_Smart_Basket_Recommendation_Page='' and @So2_Dynamic_Landing_Page='' and @Microsite_URL !=''
			
				THEN
						Set  @So2_Product_Display_Page='YES';	
			End if;
-- ---------------------------------------------Advance Campaign----------------------------------------------------------------------------------------------------------
			
			-- TIME SLOT
			Set  @TimeSlot='';	
			Select  C.Timeslot_Name into @TimeSlot from CLM_Campaign_Events A,CLM_Creative B,CLM_Timeslot_Master C
			where A.CreativeId=B.CreativeId	and B.Timeslot_Id =C.Timeslot_Id and A.EventId=@EDIT_EVENT;
			
			IF @TimeSlot IS NULL
				THEN
					Set  @TimeSlot='';		
			END IF;
			
			-- OFFER TYPE
            
            IF @Offer_Based = "YES" 
            
            THEN 
		
				Set  @Offer_Type_Name='';
				Set @Offer_Type='';
				Select  D.OfferTypeId into @Offer_Type from CLM_Campaign_Events A,CLM_Creative B,CLM_Offer C,CLM_OfferType D
						where A.CreativeId=B.CreativeId	and B.OfferId=C.OfferId and C.OfferTypeId=D.OfferTypeId	and A.EventId=@EDIT_EVENT;
				
				Select  D.OfferTypeName into @Offer_Type_Name from CLM_Campaign_Events A,CLM_Creative B,CLM_Offer C,CLM_OfferType D
				where A.CreativeId=B.CreativeId	and B.OfferId=C.OfferId and C.OfferTypeId=D.OfferTypeId	and A.EventId=@EDIT_EVENT;
						
				IF @Offer_Type IS NULL or @Offer_Type=''
					THEN
						Set  @Offer_Type='';	Set   @Offer_Type_Name='';		
				END IF;
				
				-- OFFER CODE
				Set  @Offer_Code='';	
				
				Select  C.OfferName  into @Offer_Code from CLM_Campaign_Events A,CLM_Creative B,CLM_Offer C
						where A.CreativeId=B.CreativeId	and B.OfferId=C.OfferId and A.EventId=@EDIT_EVENT;
				
				IF @Offer_Code IS NULL
					THEN
						Set  @Offer_Code='';		
				END IF;
				
				
				
				-- OFFER START DATE
				Set  @Offer_Start_Date='';
				Select  C.OfferStartDate  into @Offer_Start_Date from CLM_Campaign_Events A,CLM_Creative B,CLM_Offer C
						where A.CreativeId=B.CreativeId	and B.OfferId=C.OfferId and A.EventId=@EDIT_EVENT;
				
				IF @Offer_Start_Date IS NULL
					THEN
						Set  @Offer_Start_Date='';		
				END IF;
				
				-- OFFER END DATE
				Set  @Offer_End_Date='';	
				Select  C.OfferEndDate  into @Offer_End_Date from CLM_Campaign_Events A,CLM_Creative B,CLM_Offer C
						where A.CreativeId=B.CreativeId	and B.OfferId=C.OfferId and A.EventId=@EDIT_EVENT;
				
				IF @Offer_End_Date IS NULL
					THEN
						Set  @Offer_End_Date='';		
				END IF;
                
                
			ELSE 
               
               Set  @Offer_Code='';
               Set  @Offer_Type='';	
               Set  @Offer_Type_Name='';	
               Set  @Offer_Start_Date='';
               Set  @Offer_End_Date='';
			
            END IF;
			
			
			-- EXT CAMPAIGN KEY
			Set  @Ext_Campaign_Key='';	
			Select  B.ExtCreativeKey  into @Ext_Campaign_Key from CLM_Campaign_Events A,CLM_Creative B where A.CreativeId=B.CreativeId and A.EventId=@EDIT_EVENT;
			
			IF @Ext_Campaign_Key IS NULL
				THEN
					Set  @Ext_Campaign_Key='';		
			END IF;
			
			-- DLT
			SET @DLT = json_unquote(json_extract(InputJson,"$.DLT"));
			
			IF @DLT IS NULL
				THEN
					Set  @DLT='';		
			END IF;
			
			
			-- SENDER KEY
			Set  @Sender_Key='';
			Select  B.SenderKey  into @Sender_Key from CLM_Campaign_Events A,CLM_Creative B where A.CreativeId=B.CreativeId and A.EventId=@EDIT_EVENT;
			
			
			IF @Sender_Key IS NULL
				THEN
					Set  @Sender_Key='';		
			END IF;
			
			
			-- COMMUNICATION HEADER
			SET  @Communication_Header='';
			

			Select Header into @Communication_Header from Event_Master A, Communication_Template B where A.Communication_Template_ID=B.Id and A.Id =@EDIT_EVENT;
			
			
			-- RECOMMENDATION STORY ID
			SET @Personalised_Reccomendation_Name='';	
			Select B.Name into @Personalised_Reccomendation_Name
			from Event_Master A, RankedPickList_Stories_Master B
			where A.RankedPickListStory_Id=B.Story_Id  and Id =@EDIT_EVENT;
			
			
			SET  @Personalised_Reccomendation_Id='';
			Select B.Story_Id into @Personalised_Reccomendation_Id
			from Event_Master A, RankedPickList_Stories_Master B
			where A.RankedPickListStory_Id=B.Story_Id  and Id =@EDIT_EVENT;
				
			
			IF @Personalised_Reccomendation_Id IS NULL or  @Personalised_Reccomendation_Id= ''
				THEN
					Set  @Personalised_Reccomendation_Id='';	
					Set  @Personalised_Reccomendation_Name='';						
			END IF;
					
			
			-- ADVANCE SVOC CONDITION
			Set  @Trigger_CTReplacedQuery='';	
			
			Select CTReplacedQuery into @Trigger_CTReplacedQuery From CLM_Campaign_Trigger where CampTriggerId = @Trigger_Id; 
			
			IF @Trigger_CTReplacedQuery IS NULL
				THEN
					Set  @Trigger_CTReplacedQuery='';		
			END IF;
					
					
			-- ADVANCE SVOT CONDITION
			Set  @Trigger_CTReplacedQueryBill='';
			Select CTReplacedQueryBill into @Trigger_CTReplacedQueryBill From CLM_Campaign_Trigger where CampTriggerId = @Trigger_Id; 
			
			
			IF @Trigger_CTReplacedQueryBill IS NULL
				THEN
					Set  @Trigger_CTReplacedQueryBill='';		
			END IF;
			
			
			-- Remainder_Parent_Trigger_Id 
			Set @Reminder_Parent_Trigger_Id='';
			Set @Reminder_Parent_Trigger_Name=''; 
			Set @Reminder_Parent_Trigger_OffSetDays='' ;
			
			-- Select Reminder_Parent_Event_Id into @Reminder_Parent_Trigger_Id from Event_Master where Id =@EDIT_EVENT;	
			
			-- Set @Reminder_Parent_Trigger_Id=@EDIT_EVENT;
			
			-- SELECT NAME  into @Reminder_Parent_Trigger_Name FROM Event_Master where Id =@EDIT_EVENT;

			Select Reminder_Parent_Event_Id into @Reminder_Parent_Event_Id from Event_Master where Id =@EDIT_EVENT;
			
			Select A.EventId into @Reminder_Parent_Trigger_Id 
			from CLM_Campaign_Events A,CLM_Campaign_Trigger B
			where A.CampTriggerId=B.CampTriggerId and A.CampTriggerId=@Reminder_Parent_Event_Id;
			
			SELECT NAME  into @Reminder_Parent_Trigger_Name FROM Event_Master where Id =@Reminder_Parent_Trigger_Id;

			
			Select Reminder_Days into @Reminder_Parent_Trigger_OffSetDays from Event_Master where Id =@EDIT_EVENT;	
			
			
			IF @Reminder_Parent_Trigger_Id is null 	THEN Set @Reminder_Parent_Trigger_Id=''; END IF;
			IF @Reminder_Parent_Trigger_Name is null THEN Set @Reminder_Parent_Trigger_Name=''; END IF;
			IF @Reminder_Parent_Trigger_OffSetDays is null THEN Set @Reminder_Parent_Trigger_OffSetDays='' ; END IF;
			
			Set @Event_Start_Date='';
			Set @Event_End_Date='';
			Select date_format(Start_Date_ID,"%Y-%m-%d") into @Event_Start_Date from Event_Master where Id=@EDIT_EVENT;
			Select date_format(End_Date_ID,"%Y-%m-%d") into @Event_End_Date from Event_Master where Id=@EDIT_EVENT;
			
			SET @ONBOARDING_TIME='';
			IF @Recency_Start=1 and @Recency_End=1
				THEN SET @ONBOARDING_TIME='1';
			END IF;	
			IF @Recency_Start=2 and @Recency_End=2
				THEN SET @ONBOARDING_TIME='2';
			END IF;	
			IF @Recency_Start=3 and @Recency_End=3
				THEN SET @ONBOARDING_TIME='3';
			END IF;	
			IF @Recency_Start=4 and @Recency_End=4
				THEN SET @ONBOARDING_TIME='4';
			END IF;	
			IF @Recency_Start=5 and @Recency_End=5
				THEN SET @ONBOARDING_TIME='5';
			END IF;	
			IF @Recency_Start=6 and @Recency_End=6
				THEN SET @ONBOARDING_TIME='6';
			END IF;	
			
			
			
			Set @In_Control_Group='';	
			Select Case when Needs_ControlGroup="Y" then "YES" else "NO"  end into @In_Control_Group from Event_Master where Id =@EDIT_EVENT;
            
            
            
            -- ALL_USERS LOGIC --- 
            
            SET @ALL_USERS_CHECK = '';
            SET @ALL_USERS = 'NO';
            
            SELECT CLMSegmentName  INTO @ALL_USERS_CHECK FROM Event_Master 
            WHERE Id =@EDIT_EVENT;
            
            IF @ALL_USERS_CHECK = "ALL"
            THEN 
            
				IF @MCore_Module_Check = "NO" AND @Campaign_Segment_Id = ''
                THEN 
                
					SET @ALL_USERS = 'YES';
                    
				END IF;
                    
                    
			 IF @MCore_Module_Check = "YES"
             THEN 
             
				SET @ALL_USERS = 'NO';
                
			 
			 END IF;
             
             IF @Campaign_Segment_Id != ''
             THEN 
				
                SET @ALL_Users = 'NO';
                
			END IF ;
            
		END IF;
        
        -- Life Cycle Check Box 
        
        SET @LC_Check_Box = 'YES';
        
        IF @ALL_USERS_CHECK = "ALL"
        THEN 
			
            SET @LC_Check_Box = 'NO';
            
		END IF;
        
        IF @MCore_Module_Check = "YES"
        THEN 
        
			SET @LC_Check_Box = 'NO';
		
        END IF;
			
			
            Select @IN_USER_NAME,@IN_SCREEN,@Trigger_Id,@Trigger_Name,@Existing_Campaign_Id,@Existing_Campaign_Name,@Existing_Theme_Id,@Existing_Theme_Name,
                   @Campaign_Type,@Region_Id,@Region_Name,@Campaign_Segment_Id,@Campaign_Segment_Name,@Lifecyle_Segment_Id,@Lifecyle_Segment_Name,@Governance,
                   @Execution_After,@Throttling_Level,@Comm_Cool_Off,@Cool_Off,@Comm_Priority,@Attribution,@Favourite_Day,@Recency_Start,@Recency_End,@Visit_Start,@Visit_End,@Monetary_Value_Start,@Monetary_Value_End,
                   @Discount,@Last_Bought_Category,@Favourite_Category,@Active_Period_Category,@Channel,@Personalised_Reccomendation_Name,@Personalised_Reccomendation_Id,
                   @Personalised_Reco_via_Solus_Shortener,@Personalization,@Valid_Mobile,@Valid_Email,@DND,@Reco_Based,@Offer_Based,@Microsite_URL,@Trigger_Type,@TimeSlot,@Offer_Type,@Offer_Code,@Offer_Start_Date,@Offer_End_Date,@Ext_Campaign_Key,@DLT,@Sender_Key,@Trigger_CTReplacedQuery,@Trigger_CTReplacedQueryBill,@Offer_Type_Name,@Reminder_Parent_Trigger_OffSetDays,@Reminder_Parent_Trigger_Id,@Purchase_Anniversary,@Multiple_of_5,@Email_Template_Personalization,@WhatsApp_Template_Id,@Email_Subject_Personalization,@WhatsApp_Banner_Personalization,@WhatsApp_Variable1_Personalization,@WhatsApp_Variable2_Personalization,@WhatsApp_Variable3_Personalization,@WhatsApp_Variable4_Personalization,@WhatsApp_Variable5_Personalization,@WhatsApp_Variable6_Personalization,@WhatsApp_Variable7_Personalization,@WhatsApp_Variable8_Personalization,@WhatsApp_Variable9_Personalization,@WhatsApp_Variable10_Personalization,@So2_Website_Deep_Link_Page,@So2_Product_Display_Page,@So2_Personalised_Smart_Basket_Recommendation_Page,@So2_Dynamic_Landing_Page,@Reminder_Parent_Trigger_Name,@ONBOARDING_TIME,@Event_Start_Date, @Event_End_Date,@Mcore_Module_Id,@Mcore_Module_Name,@Predictive_Score,@In_Control_Group;
				    SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("USER_NAME","","SCREEN","',@IN_SCREEN,'","Trigger_Id","',@Trigger_Id,'","Trigger_Name","',@Trigger_Name,'","Existing_Campaign_Id","',@Existing_Campaign_Id,'","Existing_Campaign_Name","',@Existing_Campaign_Name,'","Existing_Theme_Id","',@Existing_Theme_Id,'","Existing_Theme_Name","',@Existing_Theme_Name,'","Campaign_Type","',@Campaign_Type,'","Region_Id","',@Region_Id,'","Region_Name","',@Region_Name,'","Campaign_Segment_Id","',@Campaign_Segment_Id,'","Campaign_Segment_Name","',@Campaign_Segment_Name,'","Lifecyle_Segment_Id","',@Lifecyle_Segment_Id,'","Lifecyle_Segment_Name","',@Lifecyle_Segment_Name,'","Execution_After","',@Execution_After,'","Throttling_Level","',@Throttling_Level,'","Comm_Cool_Off","',@Comm_Cool_Off,'","Cool_Off","',@Cool_Off,'","Comm_Priority","',@Comm_Priority,'","Attribution","',@Attribution,'","Favourite_Day","',@Favourite_Day,'","Channel","',@Channel,'","Recency_Start","',@Recency_Start,'","Recency_End","',@Recency_End,'","Visit_Start","',@Visit_Start,'","Visit_End","',@Visit_End,'","Monetary_Value_Start","',@Monetary_Value_Start,'","Monetary_Value_End","',@Monetary_Value_End,'","Last_Bought_Category","',@Last_Bought_Category,'","Favourite_Category","',@Favourite_Category,'","Discount","',@Discount,'","Personalised_Reccomendation_Name","',@Personalised_Reccomendation_Name,'","Personalised_Reccomendation_Id","',@Personalised_Reccomendation_Id,'","Personalised_Reco_via_Solus_Shortener","',@Personalised_Reco_via_Solus_Shortener,'","Personalization","',@Personalization,'","Edit_Status","EDIT","Valid_Mobile","',@Valid_Mobile,'","Valid_Email","',@Valid_Email,'","DND","',@DND,'","Offer_Based","',@Offer_Based,'","Reco_Based","',@Reco_Based,'","Microsite_URL","',@Microsite_URL,'","Trigger_Type","',@Trigger_Type,'","TimeSlot","',@TimeSlot,'","Offer_Type_Id","',@Offer_Type,'","Offer_Code","',@Offer_Code,'","Offer_Start_Date","',@Offer_Start_Date,'","Offer_End_Date","',@Offer_End_Date,'","Ext_Campaign_Key","',@Ext_Campaign_Key,'","DLT","',@DLT,'","Sender_Key","',@Sender_Key,'","SVOC_Condition","',@Trigger_CTReplacedQuery,'","SVOT_Condition","',@Trigger_CTReplacedQueryBill,'","Offer_Type_Name","',@Offer_Type_Name,'","Reminder_Parent_Trigger_Id","',@Reminder_Parent_Trigger_Id,'","Reminder_Parent_Trigger_Name","',@Reminder_Parent_Trigger_Name,'","Reminder_Parent_Trigger_OffSetDays","',@Reminder_Parent_Trigger_OffSetDays,'","Purchase_Anniversary","',@Purchase_Anniversary,'","Multiple_of_5","',@Multiple_of_5,'","Email_Template_Personalization","',@Email_Template_Personalization,'","Email_Subject_Personalization","',@Email_Subject_Personalization,'","WhatsApp_Banner_Personalization","',@WhatsApp_Banner_Personalization,'","WhatsApp_Template_Id","',@WhatsApp_Template_Id,'","WhatsApp_Variable1_Personalization","',@WhatsApp_Variable1_Personalization,'","WhatsApp_Variable2_Personalization","',@WhatsApp_Variable2_Personalization,'","WhatsApp_Variable3_Personalization","',@WhatsApp_Variable3_Personalization,'","WhatsApp_Variable4_Personalization","',@WhatsApp_Variable4_Personalization,'","WhatsApp_Variable5_Personalization","',@WhatsApp_Variable5_Personalization,'","WhatsApp_Variable6_Personalization","',@WhatsApp_Variable6_Personalization,'","WhatsApp_Variable7_Personalization","',@WhatsApp_Variable7_Personalization,'","WhatsApp_Variable8_Personalization","',@WhatsApp_Variable8_Personalization,'","WhatsApp_Variable9_Personalization","',@WhatsApp_Variable9_Personalization,'","WhatsApp_Variable10_Personalization","',@WhatsApp_Variable10_Personalization,'","So2_Website_Deep_Link_Page","',@So2_Website_Deep_Link_Page,'","So2_Product_Display_Page","',@So2_Product_Display_Page,'","So2_Personalised_Smart_Basket_Recommendation_Page","',@So2_Personalised_Smart_Basket_Recommendation_Page,'","So2_Dynamic_Landing_Page","',@So2_Dynamic_Landing_Page,'","Id","',@ONBOARDING_TIME,'","Event_Start_Date","',@Event_Start_Date,'","Event_End_Date","',@Event_End_Date,'","Active_Period_Category","',@Active_Period_Category,'","Communication_Header","',@Communication_Header,'","Execution_After_Name","',@Execution_After_Name,'","Propensity","',@MCore_Module_Check,'","Predictive_Score","',@Predictive_Score,'","Predictive_Value","',(@Predictive_Score)/100,'","Mcore_Module_Id","',@Mcore_Module_Id,'","Mcore_Module_Name","',@Mcore_Module_Name,'","Propensity_Segment_Id","',@Propensity_Segment_Id,'","Propensity_Segment_Name","',@Propensity_Segment_Name,'","All_Users","',@ALL_USERS,'","LifeCycle_Check_Box","',@LC_Check_Box,'","In_Control_Group","',@In_Control_Group,'" ) )),"]") into @temp_result
								     	') ;


     
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement;
			
		
				
								 
		
		
	End If;	
	
	
	if @temp_result is null
		then
								
							Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Invalid Parameters") )),"]") into @temp_result                       
												       ;') ;
      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
						
	End if;
						
	SET out_result_json = @temp_result;
				
 END$$
DELIMITER ;
