
CREATE OR REPLACE PROCEDURE `S_LOOPING_Update_EEH_Columns`()
BEGIN
DECLARE vStart_Cnt BIGINT DEFAULT 0;
DECLARE vBatchSize BIGINT DEFAULT 100000;
DECLARE vEnd_Cnt BIGINT;
SELECT Customer_Id INTO @Rec_Cnt FROM Event_Execution_History ORDER BY Customer_Id DESC LIMIT 1;

PROCESS_LOOP: LOOP
SET vEnd_Cnt = vStart_Cnt + vBatchSize - 1;
INSERT IGNORE INTO `CART_SOLUS_PROD`.`Event_Execution_History`
(`Event_Execution_ID`,
`Event_ID`,
`Event_Execution_Date_ID`,
`Customer_Id`,
`Is_Target`,
`Exclusion_Filter_ID`,
`Communication_Template_ID`,
`Communication_Channel`,
`Message`,
`Email_Message`,
`SMS_Message`,
`PN_Message`,
`WhatsApp_Message`,
`In_Control_Group`,
`In_Event_Control_Group`,
`Offer_Code`,
`GV_Code`,
`E_Voucher_Code`,
`Booklet_Code`,
`Is_MAB_Event`,
`Campaign_Key`,
`Segment_Id`,
`Revenue_Center`,
`Recommendation_Genome_Lov_Id_1`,
`Recommendation_Genome_Lov_Id_2`,
`Recommendation_Genome_Lov_Id_3`,
`Recommendation_Genome_Lov_Id_4`,
`Recommendation_Genome_Lov_Id_5`,
`Recommendation_Genome_Lov_Id_6`,
`Recommendation_Product_Id_1`,
`Recommendation_Product_Id_2`,
`Recommendation_Product_Id_3`,
`Recommendation_Product_Id_4`,
`Recommendation_Product_Id_5`,
`Recommendation_Product_Id_6`,
`Run_Mode`,
`Microsite_URL`,
`Created_Date`)
SELECT 
`Event_Execution_ID`,
`Event_ID`,
`Event_Execution_Date_ID`,
`Customer_Id`,
`Is_Target`,
`Exclusion_Filter_ID`,
`Communication_Template_ID`,
`Communication_Channel`,
`Message`,
`Email_Message`,
`SMS_Message`,
`PN_Message`,
`WhatsApp_Message`,
`In_Control_Group`,
`In_Event_Control_Group`,
`Offer_Code`,
`GV_Code`,
`E_Voucher_Code`,
`Booklet_Code`,
`Is_MAB_Event`,
`Campaign_Key`,
`Segment_Id`,
`Revenue_Center`,
`Recommendation_Genome_Lov_Id_1`,
`Recommendation_Genome_Lov_Id_2`,
`Recommendation_Genome_Lov_Id_3`,
`Recommendation_Genome_Lov_Id_4`,
`Recommendation_Genome_Lov_Id_5`,
`Recommendation_Genome_Lov_Id_6`,
`Recommendation_Product_Id_1`,
`Recommendation_Product_Id_2`,
`Recommendation_Product_Id_3`,
`Recommendation_Product_Id_4`,
`Recommendation_Product_Id_5`,
`Recommendation_Product_Id_6`,
`Run_Mode`,
`Microsite_URL`,
`Created_Date`
From Event_Execution_History_23Sep
where Customer_Id between vStart_Cnt and vEnd_Cnt ;

SET vStart_Cnt = vEnd_Cnt;
IF vStart_Cnt  >= @Rec_Cnt THEN
	LEAVE PROCESS_LOOP;
END IF;        
END LOOP PROCESS_LOOP;  
END

