DELIMITER $$
CREATE or replace PROCEDURE `S_UI_Campaign_Custom_Vairable`(IN in_request_json json, OUT out_result_json json)
begin

	DECLARE  ID,vExists int;
	DECLARE done1,done2 INT DEFAULT FALSE;
	DECLARE vSql0,vSql1,vErrMsg text;
	DECLARE vSuccess INT DEFAULT 0;
	DECLARE AGGREGATION_1 varchar(400);
	DECLARE AGGREGATION_2 TEXT;
	DECLARE AGGREGATION_3 TEXT;
	DECLARE AGGREGATION_4 TEXT;
	DECLARE AGGREGATION_5 TEXT;
	DECLARE Custom_Variable_Condition TEXT;

	SET SQL_SAFE_UPDATES=0;
	SET foreign_key_checks=0;
		
	Set in_request_json = Replace(in_request_json,"\"",'"');
		
	SET AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET AGGREGATION_4 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_4"));
	SET AGGREGATION_5 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_5"));
		
		
	SET vErrMsg = 'Start';
	SET vSuccess = 0;
		
    If 	AGGREGATION_3 ="BIGINT"
		Then SET AGGREGATION_3= "BIGINT(128)";
	End IF;
	
	If 	AGGREGATION_3 ="DECIMAL"
		Then SET AGGREGATION_3= "DECIMAL(15,2)";
	End IF;
	
	If 	AGGREGATION_3 ="VARCHAR"
		Then SET AGGREGATION_3= "VARCHAR(128)";
	End IF;
	
	If 	AGGREGATION_3 ="TEXT"
		Then SET AGGREGATION_3= "Text";
	End IF;
	
	If 	AGGREGATION_5 ="BIGINT"
		Then SET AGGREGATION_5= "BIGINT(128)";
	End IF;
	
	If 	AGGREGATION_5 ="DECIMAL"
		Then SET AGGREGATION_5= "DECIMAL(15,2)";
	End IF;
	
	If 	AGGREGATION_5 ="VARCHAR"
		Then SET AGGREGATION_5= "VARCHAR(128)";
	End IF;
	
	If 	AGGREGATION_5 ="TEXT"
		Then SET AGGREGATION_5= "Text";
	End IF;
	
	
	SET @File_Path= concat((select value from M_Config where Name='Custom_Variable_Path'));
	If  @File_Path is null
		Then
			
			Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Please configure the Custom_Variable_Path in M_Config ") )),"]") into @temp_result ;') ;
		   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;
							
	ELSE		Select 1;				
			IF AGGREGATION_1="Create_Custom_Variable" and AGGREGATION_2 !='' and AGGREGATION_3 !='' and AGGREGATION_4 =''
				THEN
					Select substring_index(Value,"_",1) into @Brand_Name from M_Config where Name='Tenant_Name';
					Set AGGREGATION_2 = Concat(@Brand_Name,'_',AGGREGATION_2);
					Select AGGREGATION_2;
						
					SELECT count(1) INTO vExists FROM information_schema.columns WHERE  TABLE_NAME = 'Client_SVOC' AND COLUMN_NAME = AGGREGATION_2;
						
					If vExists <>1 
						Then 
							DROP TABLE IF EXISTS Client_SVOC_Temp;
							Create Table Client_SVOC_Temp
											(`Customer_Id_1` bigint(20) NOT NULL AUTO_INCREMENT,
											PRIMARY KEY (`Customer_Id_1`)
											) ENGINE=InnoDB DEFAULT CHARSET=latin1;
											
							Insert ignore into Client_SVOC_Temp (Customer_Id_1)
							Select Distinct Customer_ID from CDM_Customer_Master where Customer_ID<1001;
										
							SET vSql0 = CONCAT('ALTER TABLE Client_SVOC_Temp ADD COLUMN ',AGGREGATION_2, ' ', AGGREGATION_3);
							SELECT vSql0;
							PREPARE statement from vSql0;
							Execute statement;
							Deallocate PREPARE statement;
														
							SET vSql1 = CONCAT('ALTER TABLE Client_SVOC_Temp ADD INDEX  ',AGGREGATION_2,'(',AGGREGATION_2,')');
							SELECT vSql1;
							PREPARE statement from vSql1;
							Execute statement;
							Deallocate PREPARE statement;
										
								
							Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Variable Created") )),"]") into @temp_result ;') ;
		   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;
										
					End If;
									
					If vExists =1
						Then 			
							Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Variable Already Exists") )),"]") into @temp_result  ;') ;
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;
					End If;

			End If;
			
			If AGGREGATION_1 ="Variable_Defination" and AGGREGATION_2 !='' and AGGREGATION_3 !='' and AGGREGATION_4 ="TEST"
				Then 
					
					Select substring_index(Value,"_",1) into @Brand_Name from M_Config where Name='Tenant_Name';
					SET AGGREGATION_3 = Replace(AGGREGATION_3,"@START_COUNT",1);
					SET AGGREGATION_3 = Replace(AGGREGATION_3,"@END_COUNT",1000);
					SET AGGREGATION_3 = Replace(AGGREGATION_3,"Client_SVOC",'Client_SVOC_Temp');
					SET AGGREGATION_3 = Replace(AGGREGATION_3,AGGREGATION_2,Concat('',@Brand_Name,'_',AGGREGATION_2));
					Select AGGREGATION_3;
					
					Set AGGREGATION_2 = Concat(@Brand_Name,'_',AGGREGATION_2);
					Select AGGREGATION_2;
						
					SET @SQL1 =Concat('',AGGREGATION_3,'');
					SELECT @SQL1;
					PREPARE statement from @SQL1;
					Execute statement;
					Deallocate PREPARE statement; 
					Select vErrMsg;
						 
					Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Customer_Id",Customer_Id_1,"Value",',AGGREGATION_2,') )),"]") into @temp_result  
												From   Client_SVOC_Temp 
												Where ',AGGREGATION_2,' is Not Null or ',AGGREGATION_2,' >0
												order by Customer_Id_1 limit 100 ;') ;
					   
					SELECT @Query_String;
					PREPARE statement from @Query_String;
					Execute statement;
					Deallocate PREPARE statement; 
												
					If 	@temp_result is null
						THEN
							Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","NO Customer got Selected") )),"]") into @temp_result ;') ;
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
					End If;	
						

							
					
							
			End If;	
			
			If AGGREGATION_1 ="Variable_Defination" and AGGREGATION_2 !=''and AGGREGATION_3 !='' and AGGREGATION_4 ="SAVE" AND AGGREGATION_5 !=''
				Then
						
					Set @Variable_Name=AGGREGATION_2;
						
					Select substring_index(Value,"_",1) into @Brand_Name from M_Config where Name='Tenant_Name';
					Set AGGREGATION_2 = Concat(@Brand_Name,'_',AGGREGATION_2);
					Select AGGREGATION_2;
					
					SET AGGREGATION_3 = Replace(AGGREGATION_3,@Variable_Name,AGGREGATION_2);
					SET AGGREGATION_3 = Replace(AGGREGATION_3,"@START_COUNT","',@START_COUNT,'");
					SET AGGREGATION_3 = Replace(AGGREGATION_3,"@END_COUNT","',@END_COUNT,'");	
					Select @Variable_Name,AGGREGATION_2,AGGREGATION_3;
						
						
					SET @File_Path= concat((select value from M_Config where Name='Custom_Variable_Path'));
					Select Concat('', @File_Path,'/S_SVOC_Custom_',AGGREGATION_2,'.sql') into @Out_FileName_SQL;
					Select @Out_FileName_SQL;
						
					Select AGGREGATION_3;
					Set Custom_Variable_Condition = Replace(AGGREGATION_3,"Client_SVOC custom,","Client_SVOC custom,CDM_Recompute_Customer_Var_List Recompute, ");
					Select Custom_Variable_Condition;
							
					Set Custom_Variable_Condition = Replace(Custom_Variable_Condition,"custom.Customer_Id_1 = bills.Customer_Id","custom.Customer_Id_1 = bills.Customer_Id and custom.Customer_Id_1=Recompute.Customer_Id");
					Select Custom_Variable_Condition;
						
					SET @SQL1 =Concat("DELIMITER $$ 
									 CREATE or replace PROCEDURE S_SVOC_Custom_",AGGREGATION_2,"(IN vBatchSize bigint,IN vTreat_As_Incr TINYINT) 
									Begin
							
										If vTreat_As_Incr != 1
											Then	
												SELECT  Customer_Id INTO @Rec_Cnt FROM Customer_One_View ORDER BY Customer_Id DESC LIMIT 1;
												SET @START_COUNT = 0;
												PROCESS_LOOP1:
												LOOP SET @END_COUNT = @START_COUNT + vBatchSize; 
												set @sqlCust0 = concat('",AGGREGATION_3,"'); 
												SELECT @sqlCust0;
												prepare stmt1 from @sqlCust0;
												execute stmt1;
												deallocate prepare stmt1;
												SET @START_COUNT=@END_COUNT+1;
												IF @START_COUNT  >= @Rec_Cnt
												THEN	LEAVE PROCESS_LOOP1;
												END IF;	
												END LOOP PROCESS_LOOP1;
										End If;
							
										If vTreat_As_Incr = 1
											Then
								
												SELECT  Customer_Id INTO @Rec_Cnt FROM CDM_Recompute_Customer_Var_List ORDER BY Customer_Id DESC LIMIT 1;
												SET @START_COUNT = 0;
												PROCESS_LOOP1:
												LOOP SET @END_COUNT = @START_COUNT + vBatchSize; 
												set @sqlCust0 = concat('",Custom_Variable_Condition,"'); 
												SELECT @sqlCust0;
												prepare stmt1 from @sqlCust0;
												execute stmt1;
												deallocate prepare stmt1;
												SET @START_COUNT=@END_COUNT+1;
												IF @START_COUNT  >= @Rec_Cnt
												THEN	LEAVE PROCESS_LOOP1;
												END IF;	
												END LOOP PROCESS_LOOP1;
										End If;
							
									End$$
									DELIMITER ;");
			
					SELECT @SQL1;
					SET @sql4=concat(' Select "',@SQL1,'" INTO OUTFILE "',@Out_FileName_SQL,'" FIELDS TERMINATED BY "''" escaped by "\" LINES TERMINATED BY "\n"');
					SELECT @sql4;
					PREPARE statement from @sql4;
					Execute statement;
					Deallocate PREPARE statement;
						
						
					Select Concat('', @File_Path,'/S_SVOC_Custom_',AGGREGATION_2,'.sh') into @Out_FileName_Sh;
					Select @Out_FileName_Sh;
						
					Set @sql1=Concat('. $HOME/.solusconfig
mysql --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME --verbose << EOF 
source ',@Out_FileName_SQL,'
Call S_SVOC_Custom_',AGGREGATION_2,'(10000,0);
EOF');
							
		  
					SET @sql4=concat(' Select "',@sql1,'" INTO OUTFILE "',@Out_FileName_Sh,'" FIELDS TERMINATED BY "''" escaped by "\" LINES TERMINATED BY "\n"');
					SELECT @sql4;
					PREPARE statement from @sql4;
					Execute statement;
					Deallocate PREPARE statement;
						
					Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","The SQL file is created named as ', @Out_FileName_SQL,' and 
												Shell Script is also created to executed the .sql file Name as ',@Out_FileName_Sh,'.Please run it.") )),"]") 
												into @temp_result ;') ;
			   
					SELECT @Query_String;
					PREPARE statement from @Query_String;
					Execute statement;
					Deallocate PREPARE statement; 
										
					Insert Ignore into 	UI_Campaign_Custom_Variable(Custom_Variable_Name,Custom_Variable_Condition,Custom_Variable_Files,Status)
					VALUE(AGGREGATION_2,AGGREGATION_3,@Out_FileName_Sh,"Created");	
									
					SET vSql0 = CONCAT('ALTER TABLE Client_SVOC ADD COLUMN ',AGGREGATION_2, ' ', AGGREGATION_5);
					SELECT vSql0;
					PREPARE statement from vSql0;
					Execute statement;
					Deallocate PREPARE statement;
										
					SET vSql1 = CONCAT('ALTER TABLE Client_SVOC ADD INDEX  ',AGGREGATION_2,'(',AGGREGATION_2,')');
					SELECT vSql1;
					PREPARE statement from vSql1;
					Execute statement;
					Deallocate PREPARE statement;
										
					SET vSql1 = CONCAT('Insert Ignore into Customer_View_Config(Table_Name,Table_Alias,Column_Name,Column_Name_in_One_View,
															  Column_Name_in_Details_View,In_Use_Customer_One_View,In_Use_Customer_Details_View)
															 Value ("Client_SVOC","custom","',AGGREGATION_2,'","',AGGREGATION_2,'","',AGGREGATION_2,'","Y","Y")');
					SELECT vSql1;
					PREPARE statement from vSql1;
					Execute statement;
					Deallocate PREPARE statement;
										
					Call S_CLM_Create_View();	
					
					SET vSql1 = CONCAT('Insert Ignore into UI_Variables_Sample_Data(Variable_View_Name,Variable_Name,Vairable_Sample_Data)
										Value("Sample_Data", "',AGGREGATION_2, '", "',AGGREGATION_5,'")
										');
					SELECT vSql1;
					PREPARE statement from vSql1;
					Execute statement;
					Deallocate PREPARE statement;
				
			End If;	
			
	
	End If;
	If AGGREGATION_1 ="Variable_Display"
		THEN
				
				   
			SET @Query_String = concat('SELECT  			CONCAT("[",(GROUP_CONCAT(json_object("Custom_Variable_Id",`Custom_Variable_Id`,"Custom_Variable_Name",`Custom_Variable_Name`,"Custom_Variable_Condition",`Custom_Variable_Condition`,"Custom_Variable_Files",`Custom_Variable_Files`,"Status",`Status`,"Date",SUBSTRING(ModifiedDate,6,11))order by ModifiedDate desc )),"]")
			into @temp_result from UI_Campaign_Custom_Variable ;') ;
				      
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement; 
					
			If @temp_result is null
				then set @temp_result="[]";
			End if;
	
	End If;
	
	
	If @temp_result is null
		Then
			Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Invalid Parameters") )),"]") into @temp_result;') ;
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement; 
							
	End if;
								
	SET out_result_json = @temp_result;					
end$$
DELIMITER ;
