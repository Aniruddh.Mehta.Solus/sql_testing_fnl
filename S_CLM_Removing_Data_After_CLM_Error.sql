
CREATE OR REPLACE PROCEDURE `S_CLM_Removing_Data_After_CLM_Error`(IN OnDemandEventId BIGINT(20),IN Run_Mode Varchar(10))
BEGIN
	DECLARE Load_Exe_ID, Exe_ID int;
    
DECLARE vEnd_Cnt BIGINT;
DECLARE vStart_Cnt BIGINT;
DECLARE vBatchSize BIGINT;
DECLARE vRec_Cnt BIGINT;


    
    set foreign_key_checks=0;
    
    set Load_Exe_ID = F_Get_Load_Execution_Id();
    
	insert into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
    values ('S_CLM_Event_Execution_History', '',now(),null,'Started', Load_Exe_ID);
    
    set Exe_ID = F_Get_Execution_Id();

	    SET vStart_Cnt=0;
SET vBatchSize=50000;
SET vRec_Cnt= 0;
SET vEnd_Cnt=0;

SELECT 
    Customer_Id
INTO vRec_Cnt FROM
    CDM_Customer_Master
ORDER BY Customer_Id DESC
LIMIT 1;
        
        PROCESS_LOOP: LOOP
SET vEnd_Cnt = vStart_Cnt + vBatchSize;
insert into log_solus(module, rec_start, rec_end) values ('S_CLM_Removing_Data_After_CLM_Error',vStart_Cnt,vEnd_Cnt);

If  (Run_Mode='TEST' or Run_Mode='RUN')
THEN


Delete From Event_Execution_History where Customer_ID  between vStart_Cnt and vEnd_Cnt and 
              Event_Execution_ID in (Select Event_Execution_ID from Event_Execution)
             AND Event_ID =OnDemandEventId
             and date(Event_Execution_Date_Id)=Current_Date;
             
             
Delete From Recommendation_List_EEH where Customer_ID  between vStart_Cnt and vEnd_Cnt and 
              Event_Execution_ID in (Select Event_Execution_ID from Event_Execution)
             AND Event_ID =OnDemandEventId
             and date(Event_Execution_Date_Id)=Current_Date;             

			 

ELSE
		Delete From Event_Execution_History where Customer_ID  between vStart_Cnt and vEnd_Cnt and 
              Event_Execution_ID in (Select Event_Execution_ID from Event_Execution)
              and date(Event_Execution_Date_Id)=Current_Date
             ;
             
        Delete From Recommendation_List_EEH where Customer_ID  between vStart_Cnt and vEnd_Cnt and 
              Event_Execution_ID in (Select Event_Execution_ID from Event_Execution)
              and date(Event_Execution_Date_Id)=Current_Date
             ;     
             
             

END IF;	
IF vEnd_Cnt  >= vRec_Cnt THEN
		LEAVE PROCESS_LOOP;
END IF;
SET vStart_Cnt = vStart_Cnt + vBatchSize;
END LOOP PROCESS_LOOP;

UPDATE ETL_Execution_Details 
SET 
    Status = 'Succeeded',
    End_Time = NOW()
WHERE
    Procedure_Name = 'S_Event_Execution_History'
        AND Load_Execution_ID = Load_Exe_ID
        AND Execution_ID = Exe_ID;
	



END

