DELIMITER $$
CREATE or replace PROCEDURE `S_UI_Calendar`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
    declare IN_AGGREGATION_4 text;
    declare IN_AGGREGATION_5 text;
    declare IN_AGGREGATION_6 text;
    declare IN_AGGREGATION_7 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    declare in_interval int(20);
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
    SET IN_AGGREGATION_4 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_4"));
    SET IN_AGGREGATION_5 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_5"));
    SET IN_AGGREGATION_6 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_6"));
    SET IN_AGGREGATION_7 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_7"));    
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
	SET in_interval = json_unquote(json_extract(in_request_json,"$.interval"));
    
    select Value1 into @Incremental_Revenue_Method from UI_Configuration_Global where Config_Name = 'Incremental_Revenue_Method';
    SET @Incremental_Revenue_Method=ifnull(@Incremental_Revenue_Method,'Method2');
    select @Incremental_Revenue_Method;
    IF @Incremental_Revenue_Method = 'Method1'
    THEN
		SET @incr_rev_concat='_By_Day';
        SET @group_cond1='EE_Date';
        SET @mtd_concat='';
	ELSEIF @Incremental_Revenue_Method = 'Method2'
    THEN
		SET @group_cond1='YM';
		SET @incr_rev_concat='';
        SET @mtd_concat='MTD_';
	ELSE
		SET @incr_rev_concat='';
        SET @mtd_concat='';
		SET @incr_Rev = ' round((CASE WHEN SUM(Control_Base) < 0 or SUM(Target_Delivered) = 0 then 0 WHEN (((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*100 < 0 then 0 WHEN SUM(Control_Base)>0 and SUM(Control_Responder)=0 then SUM(Target_Revenue)
					ELSE ifnull((((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*(SUM(Target_Revenue)/SUM(Target_Responder))*(SUM(Target_Delivered)),0) END),0) ';
		SET @group_cond1='YM';
	END IF;
    SELECT @mtd_concat;
    
    IF IN_AGGREGATION_3 = 'ST'
    THEN
    SET @sql1=CONCAT('
		CREATE OR REPLACE VIEW CLM_Event_Dashboard_STLT
        as 
		SELECT `CLM_Event_Dashboard`.`Revenue Center`,
			`CLM_Event_Dashboard`.`Segment`,
			`CLM_Event_Dashboard`.`Campaign`,
			`CLM_Event_Dashboard`.`EE_Date`,
			`CLM_Event_Dashboard`.`Event_Execution_Date_ID`,
			`CLM_Event_Dashboard`.`Theme`,
			`CLM_Event_Dashboard`.`Offer`,
			`CLM_Event_Dashboard`.`Channel`,
			`CLM_Event_Dashboard`.`Has_Recommendations`,
			`CLM_Event_Dashboard`.`Event_ID`,
			`CLM_Event_Dashboard`.`Control_Base_Event` as `Control_Base`,
			`CLM_Event_Dashboard`.`Target_Base`,
			`CLM_Event_Dashboard`.`Target_Delivered`,
			`CLM_Event_Dashboard`.`Target_Delivered_NA`,
			`CLM_Event_Dashboard`.`Control_Responder_Event` as `Control_Responder`,
			`CLM_Event_Dashboard`.`Target_Responder`,
			`CLM_Event_Dashboard`.`Target_Revenue`,
			`CLM_Event_Dashboard`.`Target_Discount`,
			`CLM_Event_Dashboard`.`Target_Bills`,
			`CLM_Event_Dashboard`.`Target_Open`,
			`CLM_Event_Dashboard`.`Target_CTA`,
			`CLM_Event_Dashboard`.`Control_Revenue`,
			`CLM_Event_Dashboard`.`Control_Discount`,
			`CLM_Event_Dashboard`.`Control_Bills`,
			`CLM_Event_Dashboard`.`Created_Date`,
			`CLM_Event_Dashboard`.`Total_Customer_Base`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Day`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Month`,
			`CLM_Event_Dashboard`.`YM`,
			`CLM_Event_Dashboard`.`Incremental_Revenue_ST',@incr_rev_concat,'` AS `Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_ST',@incr_rev_concat,'` AS `MTD_Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Segment_ST',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Segment`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Revenue_Center_ST',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Revenue_Center`
		FROM `CLM_Event_Dashboard`;
        ');
        select @sql1;
		PREPARE statement from @sql1;
		Execute statement;
		Deallocate PREPARE statement;
    ELSE
		SET @sql1=CONCAT('
		CREATE OR REPLACE VIEW CLM_Event_Dashboard_STLT
        as 
		SELECT `CLM_Event_Dashboard`.`Revenue Center`,
			`CLM_Event_Dashboard`.`Segment`,
			`CLM_Event_Dashboard`.`Campaign`,
			`CLM_Event_Dashboard`.`EE_Date`,
			`CLM_Event_Dashboard`.`Event_Execution_Date_ID`,
			`CLM_Event_Dashboard`.`Theme`,
			`CLM_Event_Dashboard`.`Offer`,
			`CLM_Event_Dashboard`.`Channel`,
			`CLM_Event_Dashboard`.`Has_Recommendations`,
			`CLM_Event_Dashboard`.`Event_ID`,
			`CLM_Event_Dashboard`.`Control_Base` as `Control_Base`,
			`CLM_Event_Dashboard`.`Target_Base`,
			`CLM_Event_Dashboard`.`Target_Delivered`,
			`CLM_Event_Dashboard`.`Target_Delivered_NA`,
			`CLM_Event_Dashboard`.`Control_Responder` as `Control_Responder`,
			`CLM_Event_Dashboard`.`Target_Responder`,
			`CLM_Event_Dashboard`.`Target_Revenue`,
			`CLM_Event_Dashboard`.`Target_Discount`,
			`CLM_Event_Dashboard`.`Target_Bills`,
			`CLM_Event_Dashboard`.`Target_Open`,
			`CLM_Event_Dashboard`.`Target_CTA`,
			`CLM_Event_Dashboard`.`Control_Revenue`,
			`CLM_Event_Dashboard`.`Control_Discount`,
			`CLM_Event_Dashboard`.`Control_Bills`,
			`CLM_Event_Dashboard`.`Created_Date`,
			`CLM_Event_Dashboard`.`Total_Customer_Base`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Day`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Month`,
			`CLM_Event_Dashboard`.`YM`,
			`CLM_Event_Dashboard`.`Incremental_Revenue',@incr_rev_concat,'` AS `Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue',@incr_rev_concat,'` AS `MTD_Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Segment',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Segment`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Revenue_Center',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Revenue_Center`
		FROM `CLM_Event_Dashboard`;
        ');
        select @sql1;
		PREPARE statement from @sql1;
		Execute statement;
		Deallocate PREPARE statement;
    END IF;
     SET @wherecond1 = ' 1=1 ';
	SET @wherecond2 = ' 1=1 ';
    
    CREATE OR REPLACE VIEW V_Dashboard_CLM_Event_Month_Report AS
   select CASE WHEN datediff(End_Date_ID,Start_Date_ID) < 32 THEN 'GTM' else 'CLM' END AS `Category`,b.Name as `Trigger`, b.CampaignName as `CampaignName`,a.Segment AS `CLMSegmentName`, a.* from CLM_Event_Dashboard_STLT a join Event_Master b on a.Event_ID = b.ID;
   
   CREATE OR REPLACE VIEW V_Event_Master_UI
					AS
					SELECT Event_ID, Case when (Scheduled_at is null or Scheduled_at = "") then '09:00:00' else REPLACE(TRIM(SUBSTR(Scheduled_at, 28, 35)), 'NA', '09:00:00') end  AS Scheduled_at,Type_of_event FROM Event_Master_UI;
		
   SET @create_view=CONCAT('CREATE OR REPLACE VIEW V_Dashboard_Calendar_Temp
   as
   select Category,
   Theme,
   Channel,
   CampaignName,
   CLMSegmentName,
   `Trigger`,
   YM,
   Event_Id,
   EE_Date,
   SUM(`Target_Base`) AS Target_Base,
   SUM(`Target_Bills`) AS Target_Bills,
   SUM(`Target_Delivered`) AS Target_Delivered,
   SUM(`Target_Delivered_NA`) AS Target_Delivered_NA,
   SUM(`Target_CTA`) AS Target_CTA,
   SUM(`Control_Base`) AS Control_Base,
   SUM(`Target_Responder`) AS Target_Responder,
   SUM(`Control_Responder`) AS Control_Responder,
   SUM(Target_Revenue) AS Target_Revenue,
   MAX(MTD_Incremental_Revenue) AS Incremental_Revenue
   from V_Dashboard_CLM_Event_Month_Report
   where ',@wherecond1,' and ',@wherecond2,'
   group by Event_Id,',@group_cond1,'');
   
   select @create_view;
	PREPARE statement from @create_view;
	Execute statement;
	Deallocate PREPARE statement;
   
   IF IN_MEASURE = 'YEAR_VIEW' AND IN_AGGREGATION_2 IS NULL
   THEN
   
   SELECT CONCAT(YEAR(CURDATE()),'0',MONTH(CURDATE())) INTO @Current_Month;
   SELECT @Current_Month;

	set @year_start_date = concat(IN_AGGREGATION_1,"-01-01");
    set @year_end_date = concat(IN_AGGREGATION_1,"-12-31");
	select @year_start_date,@year_end_date;
    
    set @view_stmt1= concat('CREATE OR REPLACE VIEW V_UI_Calendar1
					AS
					WITH GROUP1 AS
                            (
							SELECT COUNT(Event_ID) AS CLM,
                            date_format(EE_DATE,"%b-%Y") AS `Date`
							from V_Dashboard_Calendar_Temp
                            where Category = "CLM"
                            AND YEAR(EE_DATE) = ',IN_AGGREGATION_1,'
                            GROUP BY YM
                            ORDER BY EE_Date
                            )
                             ,
                             GROUP2 AS
							(
                            SELECT COUNT(Event_ID) AS GTM,
                            date_format(EE_DATE,"%b-%Y") AS `Date`
							from V_Dashboard_Calendar_Temp
                            where Category = "GTM"
                            AND YEAR(EE_DATE) = ',IN_AGGREGATION_1,'
                            GROUP BY YM
                            ORDER BY EE_Date
                            )
                            ,
                             GROUP3 AS
							(
                            SELECT
                            date_format(EE_DATE,"%b-%Y") AS `Date`,
							monthname(EE_Date) AS `Month`,
							CAST(format(ifnull(SUM(`Target_Base`),0),",") AS CHAR) as `Coverage`,
							CAST(format(ifnull(round(SUM(Incremental_Revenue),0),0),",") AS CHAR) AS `Rev Lift`
							from V_Dashboard_Calendar_Temp
							WHERE YEAR(EE_DATE) = ',IN_AGGREGATION_1,'
							GROUP BY YM
							ORDER BY EE_Date
                            )
                             ,
                             GROUP4 AS
							(
                            SELECT 
                            DISTINCT date_format(EE_DATE,"%b-%Y") AS `Date`,
                            monthname(EE_Date) AS `Month`,
                            date_format(EE_DATE,"%Y%m") as YM
							from tablenew
                            WHERE YEAR(EE_DATE) = ',IN_AGGREGATION_1,'
                            GROUP BY 1
                            ORDER BY EE_Date
                            )
                            SELECT IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM, A.`Date`, A.`Month`, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`, CASE WHEN A.YM = ',@Current_Month,' THEN "1" ELSE "0" END AS `Current_Month_Flag`, A.YM
                            FROM GROUP4 A
							LEFT JOIN GROUP1 B
                            ON B.`Date`=A.`Date`
                            LEFT JOIN GROUP2 C
                            ON C.`Date`=A.`Date`
                            LEFT JOIN GROUP3 D
                            ON D.`Date`=A.`Date`
                            UNION
                            SELECT IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM, A.`Date`, A.`Month`, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`, CASE WHEN A.YM = ',@Current_Month,' THEN "1" ELSE "0" END AS `Current_Month_Flag`, A.YM
                            FROM GROUP4 A
							RIGHT JOIN GROUP1 B
                            ON B.`Date`=A.`Date`
                            RIGHT JOIN GROUP2 C
                            ON C.`Date`=A.`Date`
                            RIGHT JOIN GROUP3 D
                            ON D.`Date`=A.`Date`
                            WHERE A.`Date` IS NOT NULL');
                            
				SELECT @view_stmt1;
				PREPARE statement from @view_stmt1;
				Execute statement;
				Deallocate PREPARE statement;
                            
   set @view_stmt2= concat('CREATE OR REPLACE VIEW V_UI_Calendar2
					AS
					WITH GROUP1 AS
                            (
                            select date_format(tablenew.EE_date,"%b-%Y") AS `Date`, count(distinct(em.Id)) as "CLM"
							from tablenew
							inner join Event_Master em
							on tablenew.EE_date between em.Start_Date_ID and em.End_Date_ID
							where tablenew.EE_date between "',@year_start_date,'" and "',@year_end_date,'"
							and em.State in( "Enabled","Schedule")
							and em.Id in (select Event_ID from Event_Master_UI where Type_Of_Event = "CLM")
							group by 1
							order by 1
                            )
                             ,
                             GROUP2 AS
							(
                            select date_format(tablenew.EE_date,"%b-%Y") AS `Date`, count(distinct(em.Id)) as "GTM"
							from tablenew
							inner join Event_Master em
							on tablenew.EE_date between em.Start_Date_ID and em.End_Date_ID
							where tablenew.EE_date between "',@year_start_date,'" and "',@year_end_date,'"
							-- and em.State in( "Enabled","Schedule")
							and em.Id in (select Event_ID from Event_Master_UI where Type_Of_Event = "GTM")
							group by 1
							order by 1
                            )
                            ,
                             GROUP3 AS
							(
                            SELECT
                            date_format(EE_DATE,"%b-%Y") AS `Date`,
							monthname(EE_Date) AS `Month`,
							CAST(format(ifnull(SUM(`Target_Base`),0),",") AS CHAR) as `Coverage`,
							CAST(format(ifnull(round(SUM(Incremental_Revenue),0),0),",") AS CHAR) AS `Rev Lift`
							from V_Dashboard_Calendar_Temp
							WHERE YEAR(EE_DATE) = ',IN_AGGREGATION_1,'
							GROUP BY YM
							ORDER BY EE_Date
                            )
                             ,
                             GROUP4 AS
							(
                            SELECT 
                            DISTINCT date_format(EE_DATE,"%b-%Y") AS `Date`,
                            monthname(EE_Date) AS `Month`,
                            date_format(EE_DATE,"%Y%m") as YM
							from tablenew
                            WHERE YEAR(EE_DATE) = ',IN_AGGREGATION_1,'
                            GROUP BY 1
                            ORDER BY EE_Date
                            )
                            SELECT IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM, A.`Date`, A.`Month`, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`, CASE WHEN A.YM = ',@Current_Month,' THEN "1" ELSE "0" END AS `Current_Month_Flag`, A.YM
                            FROM GROUP4 A
							LEFT JOIN GROUP1 B
                            ON B.`Date`=A.`Date`
                            LEFT JOIN GROUP2 C
                            ON C.`Date`=A.`Date`
                            LEFT JOIN GROUP3 D
                            ON D.`Date`=A.`Date`
                            UNION
                            SELECT IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM, A.`Date`, A.`Month`, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`, CASE WHEN A.YM = ',@Current_Month,' THEN "1" ELSE "0" END AS `Current_Month_Flag`,A.YM
                            FROM GROUP4 A
							RIGHT JOIN GROUP1 B
                            ON B.`Date`=A.`Date`
                            RIGHT JOIN GROUP2 C
                            ON C.`Date`=A.`Date`
                            RIGHT JOIN GROUP3 D
                            ON D.`Date`=A.`Date`
                            WHERE A.`Date` IS NOT NULL');
                            
					SELECT @view_stmt2;
				PREPARE statement from @view_stmt2;
				Execute statement;
				Deallocate PREPARE statement;
                
		set @view_stmt = concat('CREATE OR REPLACE VIEW V_UI_Calendar AS
        select A.`Date`, A.`Month`, IFNULL(A.Coverage,0) AS Coverage, IFNULL(A.`Rev Lift`,0) AS `Rev Lift`, CASE WHEN A.`YM` <= date_format(CURDATE(),"%Y%m") THEN A.CLM ELSE B.CLM END AS CLM, CASE WHEN A.`YM` <= date_format(CURDATE(),"%Y%m") THEN A.GTM ELSE B.GTM END AS GTM, A.`Current_Month_Flag`
FROM V_UI_Calendar1 A, V_UI_Calendar2 B
WHERE A.`Date` = B.`Date`');
                     
SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("CLM",`CLM`,"GTM",`GTM`,"Date",`Date`,"Month",`Month`,"Coverage",ifnull(`Coverage`,0),"Incr_Rev",ifnull(`Rev Lift`,0),"Current_Month_Flag",`Current_Month_Flag`) )),"]")into @temp_result from V_UI_Calendar ',';') ;
	END IF;
    
    IF IN_MEASURE = 'YEAR_VIEW' AND IN_AGGREGATION_2 = 'POPUP'
   THEN

   set @view_stmt= concat('CREATE OR REPLACE VIEW V_UI_Calendar
					AS
					(SELECT (SELECT COUNT(Event_Id) FROM V_Dashboard_Calendar_Temp WHERE Category = "CLM" AND monthname(EE_Date) = "',IN_AGGREGATION_1,'" AND YEAR(EE_Date) = "',IN_AGGREGATION_3,'") AS CLM, (SELECT COUNT(Event_Id) FROM V_Dashboard_Calendar_Temp WHERE Category = "GTM" AND monthname(EE_Date) = "',IN_AGGREGATION_1,'" AND YEAR(EE_Date) = "',IN_AGGREGATION_3,'") AS GTM,
                    date_format(EE_DATE,"%b-%Y") AS `Date`,
                    monthname(EE_Date) AS `Month`,
                    CAST(format(ifnull(SUM(`Target_Base`),0),",") AS CHAR) as `Coverage`,
					CAST(format(ifnull(round(SUM(Incremental_Revenue),0),0),",") AS CHAR) AS `Rev Lift`
					from V_Dashboard_Calendar_Temp
                    WHERE monthname(EE_Date) = "',IN_AGGREGATION_1,'"
                    AND YEAR(EE_Date) = "',IN_AGGREGATION_3,'" )');
                     
SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("CLM",`CLM`,"GTM",`GTM`,"Date",`Date`,"Month",`Month`,"Coverage",ifnull(`Coverage`,0),"Incr_Rev",ifnull(`Rev Lift`,0)) )),"]")into @temp_result from V_UI_Calendar ',';') ;
	END IF;
    
   
   IF IN_MEASURE = 'YEAR_VIEW' AND IN_AGGREGATION_2 = 'TOTAL'
   THEN
	SELECT CONCAT(YEAR(CURDATE()),'0',MONTH(CURDATE())) INTO @Current_Month;
   SELECT @Current_Month;

	set @year_start_date = concat(IN_AGGREGATION_1,"-01-01");
    set @year_end_date = concat(IN_AGGREGATION_1,"-12-31");
	select @year_start_date,@year_end_date;
    
    set @view_stmt= concat('CREATE OR REPLACE VIEW V_UI_Calendar
					AS
					WITH GROUP1 AS
                            (
							SELECT
                            1 AS "NUM", COUNT(Event_ID) AS CLM,
                            date_format(EE_DATE,"%b-%Y") AS `Date`
							from V_Dashboard_Calendar_Temp
                            where Category = "CLM"
                            AND YEAR(EE_DATE) = ',IN_AGGREGATION_1,'
                            -- GROUP BY YM
                            ORDER BY EE_Date
                            )
                             ,
                             GROUP2 AS
							(
                            SELECT 
                            1 AS "NUM",COUNT(Event_ID) AS GTM,
                            date_format(EE_DATE,"%b-%Y") AS `Date`
							from V_Dashboard_Calendar_Temp
                            where Category = "GTM"
                            AND YEAR(EE_DATE) = ',IN_AGGREGATION_1,'
                            -- GROUP BY YM
                            ORDER BY EE_Date
                            )
                            ,
                             GROUP3 AS
							(
                            SELECT
                            1 AS "NUM",
                            date_format(EE_DATE,"%b-%Y") AS `Date`,
							monthname(EE_Date) AS `Month`,
							CAST(format(ifnull(SUM(`Target_Base`),0),",") AS CHAR) as `Coverage`,
							CAST(format(ifnull(round(SUM(Incremental_Revenue),0),0),",") AS CHAR) AS `Rev Lift`
							from V_Dashboard_Calendar_Temp
							WHERE YEAR(EE_DATE) = ',IN_AGGREGATION_1,'
							-- GROUP BY YM
							ORDER BY EE_Date
                            )
                             ,
                             GROUP4 AS
							(
                            SELECT 
                            1 AS "NUM"
                            )
                            SELECT IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`
                            FROM GROUP4 A
							LEFT JOIN GROUP1 B
                            ON B.`NUM`=A.`NUM`
                            LEFT JOIN GROUP2 C
                            ON C.`NUM`=A.`NUM`
                            LEFT JOIN GROUP3 D
                            ON D.`NUM`=A.`NUM`
                            UNION
                            SELECT IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`
                            FROM GROUP4 A
							RIGHT JOIN GROUP1 B
                            ON B.`NUM`=A.`NUM`
                            RIGHT JOIN GROUP2 C
                            ON C.`NUM`=A.`NUM`
                            RIGHT JOIN GROUP3 D
                            ON D.`NUM`=A.`NUM`');
                            
				
	/*
    set @view_stmt2= concat('CREATE OR REPLACE VIEW V_UI_Calendar2
					AS
					WITH GROUP1 AS
                            (
                            select date_format(tablenew.EE_date,"%b-%Y") AS `Date`, count(distinct(em.Id)) as "CLM"
							from tablenew
							inner join Event_Master em
							on tablenew.EE_date between em.Start_Date_ID and em.End_Date_ID
							where tablenew.EE_date between "',@year_start_date,'" and "',@year_end_date,'"
							and em.State in( "Enabled","Schedule")
							and em.Id in (select Event_ID from Event_Master_UI where Type_Of_Event = "CLM")
							-- group by 1
							order by 1
                            )
                             ,
                             GROUP2 AS
							(
                            select date_format(tablenew.EE_date,"%b-%Y") AS `Date`, count(distinct(em.Id)) as "GTM"
							from tablenew
							inner join Event_Master em
							on tablenew.EE_date between em.Start_Date_ID and em.End_Date_ID
							where tablenew.EE_date between "',@year_start_date,'" and "',@year_end_date,'"
							-- and em.State in( "Enabled","Schedule")
							and em.Id in (select Event_ID from Event_Master_UI where Type_Of_Event = "GTM")
							-- group by 1
							order by 1
                            )
                            ,
                             GROUP3 AS
							(
                            SELECT
                            date_format(EE_DATE,"%b-%Y") AS `Date`,
							monthname(EE_Date) AS `Month`,
							CAST(format(ifnull(SUM(`Target_Base`),0),",") AS CHAR) as `Coverage`,
							CAST(format(ifnull(round(SUM(Incremental_Revenue),0),0),",") AS CHAR) AS `Rev Lift`
							from V_Dashboard_Calendar_Temp
							WHERE YEAR(EE_DATE) = ',IN_AGGREGATION_1,'
							-- GROUP BY YM
							ORDER BY EE_Date
                            )
                             ,
                             GROUP4 AS
							(
                            SELECT 
                            DISTINCT date_format(EE_DATE,"%b-%Y") AS `Date`,
                            monthname(EE_Date) AS `Month`,
                            date_format(EE_DATE,"%Y%m") as YM
							from tablenew
                            WHERE YEAR(EE_DATE) = ',IN_AGGREGATION_1,'
                            -- GROUP BY 1
                            ORDER BY EE_Date
                            )
                            SELECT IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM, A.`Date`, A.`Month`, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`, CASE WHEN A.YM = ',@Current_Month,' THEN "1" ELSE "0" END AS `Current_Month_Flag`, A.YM
                            FROM GROUP4 A
							LEFT JOIN GROUP1 B
                            ON B.`Date`=A.`Date`
                            LEFT JOIN GROUP2 C
                            ON C.`Date`=A.`Date`
                            LEFT JOIN GROUP3 D
                            ON D.`Date`=A.`Date`
                            UNION
                            SELECT IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM, A.`Date`, A.`Month`, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`, CASE WHEN A.YM = ',@Current_Month,' THEN "1" ELSE "0" END AS `Current_Month_Flag`,A.YM
                            FROM GROUP4 A
							RIGHT JOIN GROUP1 B
                            ON B.`Date`=A.`Date`
                            RIGHT JOIN GROUP2 C
                            ON C.`Date`=A.`Date`
                            RIGHT JOIN GROUP3 D
                            ON D.`Date`=A.`Date`
                            WHERE A.`Date` IS NOT NULL');
                            
					SELECT @view_stmt2;
				PREPARE statement from @view_stmt2;
				Execute statement;
				Deallocate PREPARE statement;
             */   
		
                     
SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("CLM",`CLM`,"GTM",`GTM`,"Coverage",ifnull(`Coverage`,0),"Incr_Rev",ifnull(`Rev Lift`,0)) )),"]")into @temp_result from V_UI_Calendar ',';') ;
	
   END IF;
   /* 
   IF IN_MEASURE = 'MONTH_VIEW_DATES' 
   THEN
   
	SELECT DATE_FORMAT(`EE_Date`, '%Y-%m-01') INTO @First_day_of_month FROM CLM_Event_Dashboard WHERE YM = IN_FILTER_CURRENTMONTH GROUP BY 1;
	SELECT @First_day_of_month;
    
	SELECT DATE_SUB(@First_day_of_month, INTERVAL (DAYOFWEEK(@First_day_of_month) + 5) % 7 DAY) INTO @Last_Monday;
	SELECT @Last_Monday;
    
	SELECT DATE_ADD(@Last_Monday, INTERVAL 41 DAY) INTO @EOM_Sunday;
	SELECT @EOM_Sunday;
    
    SELECT CURDATE() INTO @Today_Date;

   set @view_stmt= concat('CREATE OR REPLACE VIEW V_UI_Calendar
					AS
					Select EE_Date, DAYNAME(EE_Date) AS Day_Name, DAY(EE_Date) AS `Day` from tablenew WHERE EE_Date BETWEEN "',@Last_Monday,'" AND "',@EOM_Sunday,'";
                    ');
                     
SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Date",`EE_Date`,"Day_Name",`Day_Name`,"Day",`Day`) )),"]")into @temp_result from V_UI_Calendar ',';') ;
	END IF;
    */
     IF IN_MEASURE = 'MONTH_VIEW' AND IN_AGGREGATION_2 IS NULL
   THEN
   
   SELECT DATE_FORMAT(`EE_Date`, '%Y-%m-01') INTO @First_day_of_month FROM tablenew WHERE DATE_FORMAT(`EE_Date`, '%Y%m') = IN_FILTER_CURRENTMONTH 	GROUP BY 1;
	SELECT @First_day_of_month;
    
    set @Last_day_of_month = date_sub(date_add(@First_day_of_month ,interval 1 month),interval 1 day);
    SELECT @First_day_of_month, @Last_day_of_month;
	SELECT DATE_SUB(@First_day_of_month, INTERVAL (DAYOFWEEK(@First_day_of_month) + 5) % 7 DAY) INTO @Last_Monday;
	SELECT @Last_Monday;
    
	SELECT DATE_ADD(@Last_Monday, INTERVAL 41 DAY) INTO @EOM_Sunday;
	SELECT @EOM_Sunday;
    
    SELECT CURDATE() INTO @Today_Date;
    SELECT @Today_Date;
    
    set @view_stmt1= concat('CREATE OR REPLACE VIEW V_UI_Calendar1
					AS
					WITH GROUP1 AS
                            (
							SELECT COUNT(Event_ID) AS CLM,
                            date_format(EE_DATE,"%d-%b") AS "Date"
							from V_Dashboard_Calendar_Temp
                            where Category = "CLM"
                            AND YM = ',IN_FILTER_CURRENTMONTH,'
                            GROUP BY 2
                            ORDER BY EE_Date
                            )
                             ,
                             GROUP2 AS
							(
                            SELECT COUNT(Event_ID) AS GTM,
                            date_format(EE_DATE,"%d-%b") AS "Date"
							from V_Dashboard_Calendar_Temp
                            where Category = "GTM"
                            AND YM = ',IN_FILTER_CURRENTMONTH,'
                            GROUP BY 2
                            ORDER BY EE_Date
                            )
                            ,
                             GROUP3 AS
							(
                            SELECT
                            date_format(EE_DATE,"%d-%b") AS "Date",
							CAST(format(ifnull(SUM(`Target_Base`),0),",") AS CHAR) as `Coverage`,
							CAST(format(ifnull(round(SUM(Incremental_Revenue),0),0),",") AS CHAR) AS `Rev Lift`,
                            YM
							from V_Dashboard_Calendar_Temp
							WHERE YM = ',IN_FILTER_CURRENTMONTH,'
                            GROUP BY 1
							ORDER BY EE_Date
                            )
                             ,
                             GROUP4 AS
							(
                            SELECT 
                            DISTINCT date_format(EE_DATE,"%d-%b") AS `Date`,
                            EE_Date
							from tablenew
                            WHERE EE_Date BETWEEN "',@Last_Monday,'" AND "',@EOM_Sunday,'"
                            GROUP BY 1
                            ORDER BY EE_Date
                            )
                            SELECT EE_Date, DAYNAME(EE_Date) AS Day_Name, DAY(EE_Date) AS `Day`, IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM, A.`Date`, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`, CASE WHEN A.EE_Date = "',@Today_Date,'" THEN "1" ELSE "0" END AS `Current_Date_Flag`, D.YM
                            FROM GROUP4 A
							LEFT JOIN GROUP1 B
                            ON B.`Date`=A.`Date`
                            LEFT JOIN GROUP2 C
                            ON C.`Date`=A.`Date`
                            LEFT JOIN GROUP3 D
                            ON D.`Date`=A.`Date`
                            UNION
                            SELECT EE_Date, DAYNAME(EE_Date) AS Day_Name, DAY(EE_Date) AS `Day`, IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM, A.`Date`, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`, CASE WHEN A.EE_Date = "',@Today_Date,'" THEN "1" ELSE "0" END AS `Current_Date_Flag`, D.YM
                            FROM GROUP4 A
							RIGHT JOIN GROUP1 B
                            ON B.`Date`=A.`Date`
                            RIGHT JOIN GROUP2 C
                            ON C.`Date`=A.`Date`
                            RIGHT JOIN GROUP3 D
                            ON D.`Date`=A.`Date`
                            WHERE A.`Date` IS NOT NULL');
                            
				SELECT @view_stmt1;
		PREPARE statement from @view_stmt1;
		Execute statement;
		Deallocate PREPARE statement;
        
                    
   set @view_stmt2= concat('CREATE OR REPLACE VIEW V_UI_Calendar2
					AS
					WITH GROUP1 AS
                            (
                            
                            select date_format(tablenew.EE_date,"%d-%b") AS `Date`, count(distinct(em.Id)) as "CLM"
							from tablenew
							inner join Event_Master em
							on tablenew.EE_date between em.Start_Date_ID and em.End_Date_ID
							where tablenew.EE_date between "',@First_day_of_month,'" and "',@Last_day_of_month,'"
							and em.State in( "Enabled","Schedule")
							and em.Id in (select Event_ID from Event_Master_UI where Type_Of_Event = "CLM")
							group by 1
							order by 1
                            )
                             ,
                             GROUP2 AS
							(
                            select date_format(tablenew.EE_date,"%d-%b") AS `Date`, count(distinct(em.Id)) as "GTM"
							from tablenew
							inner join Event_Master em
							on tablenew.EE_date between em.Start_Date_ID and em.End_Date_ID
							where tablenew.EE_date between "',@First_day_of_month,'" and "',@Last_day_of_month,'"
							-- and em.State in( "Enabled","Schedule")
							and em.Id in (select Event_ID from Event_Master_UI where Type_Of_Event = "GTM")
							group by 1
							order by 1
                            )
                            ,
                             GROUP3 AS
							(
                            SELECT
                            date_format(EE_DATE,"%d-%b") AS "Date",
							CAST(format(ifnull(SUM(`Target_Base`),0),",") AS CHAR) as `Coverage`,
							CAST(format(ifnull(round(SUM(Incremental_Revenue),0),0),",") AS CHAR) AS `Rev Lift`
							from V_Dashboard_Calendar_Temp
							WHERE YM = ',IN_FILTER_CURRENTMONTH,'
                            GROUP BY 1
							ORDER BY EE_Date
                            )
                             ,
                             GROUP4 AS
							(
                            SELECT 
                            DISTINCT date_format(EE_DATE,"%d-%b") AS `Date`,
                            EE_Date
							from tablenew
                            WHERE EE_Date BETWEEN "',@Last_Monday,'" AND "',@EOM_Sunday,'"
                            GROUP BY 1
                            ORDER BY EE_Date
                            )
                            SELECT EE_Date, DAYNAME(EE_Date) AS Day_Name, DAY(EE_Date) AS `Day`, IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM, A.`Date`, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`, CASE WHEN A.EE_Date = "',@Today_Date,'" THEN "1" ELSE "0" END AS `Current_Date_Flag`
                            FROM GROUP4 A
							LEFT JOIN GROUP1 B
                            ON B.`Date`=A.`Date`
                            LEFT JOIN GROUP2 C
                            ON C.`Date`=A.`Date`
                            LEFT JOIN GROUP3 D
                            ON D.`Date`=A.`Date`
                            UNION
                            SELECT EE_Date, DAYNAME(EE_Date) AS Day_Name, DAY(EE_Date) AS `Day`, IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM, A.`Date`, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`, CASE WHEN A.EE_Date = "',@Today_Date,'" THEN "1" ELSE "0" END AS `Current_Date_Flag`
                            FROM GROUP4 A
							RIGHT JOIN GROUP1 B
                            ON B.`Date`=A.`Date`
                            RIGHT JOIN GROUP2 C
                            ON C.`Date`=A.`Date`
                            RIGHT JOIN GROUP3 D
                            ON D.`Date`=A.`Date`
                            WHERE A.`Date` IS NOT NULL');
                            
				SELECT @view_stmt2;
		PREPARE statement from @view_stmt2;
		Execute statement;
		Deallocate PREPARE statement;
        
        set @view_stmt = concat('CREATE OR REPLACE VIEW V_UI_Calendar AS
        select A.`EE_Date`, A.`Day_Name`, A.`Day`, A.`Date`, IFNULL(A.Coverage,0) AS Coverage, IFNULL(A.`Rev Lift`,0) AS `Rev Lift`, CASE WHEN A.`EE_Date` <= CURDATE() THEN A.CLM ELSE B.CLM END AS CLM, CASE WHEN A.`EE_Date` <= CURDATE()  THEN A.GTM ELSE B.GTM END AS GTM, A.`Current_Date_Flag`
FROM V_UI_Calendar1 A, V_UI_Calendar2 B
WHERE A.`EE_Date` = B.`EE_Date`');
                     
SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Date",`EE_Date`,"Day_Name",`Day_Name`,"Day",`Day`,"CLM",`CLM`,"GTM",`GTM`,"Coverage",ifnull(`Coverage`,0),"Incr_Rev",ifnull(`Rev Lift`,0),"Current_Date_Flag",`Current_Date_Flag`) )),"]")into @temp_result from V_UI_Calendar ',';') ;
	END IF;
    
     IF IN_MEASURE = 'MONTH_VIEW' AND IN_AGGREGATION_2 = 'POPUP'
   THEN

   set @view_stmt= concat('CREATE OR REPLACE VIEW V_UI_Calendar
					AS
					(SELECT (SELECT COUNT(Event_Id) FROM V_Dashboard_Calendar_Temp WHERE Category = "CLM" AND DAY(EE_Date) = "',IN_AGGREGATION_1,'" AND YM = "',IN_AGGREGATION_3,'") AS CLM, (SELECT COUNT(Event_Id) FROM V_Dashboard_Calendar_Temp WHERE Category = "GTM" AND DAY(EE_Date) = "',IN_AGGREGATION_1,'" AND YM = "',IN_AGGREGATION_3,'") AS GTM,
                    date_format(EE_DATE,"%d-%b") AS `Date`,
                    DAY(EE_Date) AS `Day`,
                    CAST(format(ifnull(SUM(`Target_Base`),0),",") AS CHAR) as `Coverage`,
					CAST(format(ifnull(round(SUM(Incremental_Revenue),0),0),",") AS CHAR) AS `Rev Lift`
					from V_Dashboard_Calendar_Temp
                    WHERE DAY(EE_Date) = "',IN_AGGREGATION_1,'"
                    AND YM = "',IN_AGGREGATION_3,'" )');
                     
SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("CLM",`CLM`,"GTM",`GTM`,"Date",`Date`,"Day",`Day`,"Coverage",ifnull(`Coverage`,0),"Incr_Rev",ifnull(`Rev Lift`,0)) )),"]")into @temp_result from V_UI_Calendar ',';') ;
	END IF;
    
    IF IN_MEASURE = 'MONTH_VIEW' AND IN_AGGREGATION_2 = 'TOTAL'
   THEN
   
   SELECT DATE_FORMAT(`EE_Date`, '%Y-%m-01') INTO @First_day_of_month FROM tablenew WHERE DATE_FORMAT(`EE_Date`, '%Y%m') = IN_FILTER_CURRENTMONTH 	GROUP BY 1;
	SELECT @First_day_of_month;
    
    set @Last_day_of_month = date_sub(date_add(@First_day_of_month ,interval 1 month),interval 1 day);
    SELECT @First_day_of_month, @Last_day_of_month;
	SELECT DATE_SUB(@First_day_of_month, INTERVAL (DAYOFWEEK(@First_day_of_month) + 5) % 7 DAY) INTO @Last_Monday;
	SELECT @Last_Monday;
    
	SELECT DATE_ADD(@Last_Monday, INTERVAL 41 DAY) INTO @EOM_Sunday;
	SELECT @EOM_Sunday;
    
    SELECT CURDATE() INTO @Today_Date;
    SELECT @Today_Date;
    
    set @view_stmt= concat('CREATE OR REPLACE VIEW V_UI_Calendar
					AS
					WITH GROUP1 AS
                            (
							SELECT COUNT(Event_ID) AS CLM,
                            1 as "NUM",
                            date_format(EE_DATE,"%d-%b") AS "Date"
							from V_Dashboard_Calendar_Temp
                            where Category = "CLM"
                            AND YM = ',IN_FILTER_CURRENTMONTH,'
                            -- GROUP BY 2
                            ORDER BY EE_Date
                            )
                             ,
                             GROUP2 AS
							(
                            SELECT COUNT(Event_ID) AS GTM,
                             1 as "NUM",
                            date_format(EE_DATE,"%d-%b") AS "Date"
							from V_Dashboard_Calendar_Temp
                            where Category = "GTM"
                            AND YM = ',IN_FILTER_CURRENTMONTH,'
                            -- GROUP BY 2
                            ORDER BY EE_Date
                            )
                            ,
                             GROUP3 AS
							(
                            SELECT
                            date_format(EE_DATE,"%d-%b") AS "Date",
                             1 as "NUM",
							CAST(format(ifnull(SUM(`Target_Base`),0),",") AS CHAR) as `Coverage`,
							CAST(format(ifnull(round(SUM(Incremental_Revenue),0),0),",") AS CHAR) AS `Rev Lift`,
                            YM
							from V_Dashboard_Calendar_Temp
							WHERE YM = ',IN_FILTER_CURRENTMONTH,'
                            -- GROUP BY 1
							ORDER BY EE_Date
                            )
                             
                            SELECT  IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`, A.YM
                            FROM GROUP3 A
							LEFT JOIN GROUP1 B
                            ON B.`NUM`=A.`NUM`
                            LEFT JOIN GROUP2 C
                            ON C.`NUM`=A.`NUM`
                            
                            UNION
                            SELECT IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM,  IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift` ,A.YM
                            FROM GROUP3 A
							RIGHT JOIN GROUP1 B
                            ON B.`NUM`=A.`NUM`
                            RIGHT JOIN GROUP2 C
                            ON C.`NUM`=A.`NUM`
                            ');
                            
				/*
                SELECT @view_stmt1;
		PREPARE statement from @view_stmt1;
		Execute statement;
		Deallocate PREPARE statement;
        
                    
   set @view_stmt2= concat('CREATE OR REPLACE VIEW V_UI_Calendar2
					AS
					WITH GROUP1 AS
                            (
                            
                            select date_format(tablenew.EE_date,"%d-%b") AS `Date`, count(distinct(em.Id)) as "CLM"
							from tablenew
							inner join Event_Master em
							on tablenew.EE_date between em.Start_Date_ID and em.End_Date_ID
							where tablenew.EE_date between "',@First_day_of_month,'" and "',@Last_day_of_month,'"
							and em.State in( "Enabled","Schedule")
							and em.Id in (select Event_ID from Event_Master_UI where Type_Of_Event = "CLM")
							group by 1
							order by 1
                            )
                             ,
                             GROUP2 AS
							(
                            select date_format(tablenew.EE_date,"%d-%b") AS `Date`, count(distinct(em.Id)) as "GTM"
							from tablenew
							inner join Event_Master em
							on tablenew.EE_date between em.Start_Date_ID and em.End_Date_ID
							where tablenew.EE_date between "',@First_day_of_month,'" and "',@Last_day_of_month,'"
							-- and em.State in( "Enabled","Schedule")
							and em.Id in (select Event_ID from Event_Master_UI where Type_Of_Event = "GTM")
							group by 1
							order by 1
                            )
                            ,
                             GROUP3 AS
							(
                            SELECT
                            date_format(EE_DATE,"%d-%b") AS "Date",
							CAST(format(ifnull(SUM(`Target_Base`),0),",") AS CHAR) as `Coverage`,
							CAST(format(ifnull(round(SUM(Incremental_Revenue),0),0),",") AS CHAR) AS `Rev Lift`
							from V_Dashboard_Calendar_Temp
							WHERE YM = ',IN_FILTER_CURRENTMONTH,'
                            GROUP BY 1
							ORDER BY EE_Date
                            )
                             ,
                             GROUP4 AS
							(
                            SELECT 
                            DISTINCT date_format(EE_DATE,"%d-%b") AS `Date`,
                            EE_Date
							from tablenew
                            WHERE EE_Date BETWEEN "',@Last_Monday,'" AND "',@EOM_Sunday,'"
                            GROUP BY 1
                            ORDER BY EE_Date
                            )
                            SELECT EE_Date, DAYNAME(EE_Date) AS Day_Name, DAY(EE_Date) AS `Day`, IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM, A.`Date`, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`, CASE WHEN A.EE_Date = "',@Today_Date,'" THEN "1" ELSE "0" END AS `Current_Date_Flag`
                            FROM GROUP4 A
							LEFT JOIN GROUP1 B
                            ON B.`Date`=A.`Date`
                            LEFT JOIN GROUP2 C
                            ON C.`Date`=A.`Date`
                            LEFT JOIN GROUP3 D
                            ON D.`Date`=A.`Date`
                            UNION
                            SELECT EE_Date, DAYNAME(EE_Date) AS Day_Name, DAY(EE_Date) AS `Day`, IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM, A.`Date`, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`, CASE WHEN A.EE_Date = "',@Today_Date,'" THEN "1" ELSE "0" END AS `Current_Date_Flag`
                            FROM GROUP4 A
							RIGHT JOIN GROUP1 B
                            ON B.`Date`=A.`Date`
                            RIGHT JOIN GROUP2 C
                            ON C.`Date`=A.`Date`
                            RIGHT JOIN GROUP3 D
                            ON D.`Date`=A.`Date`
                            WHERE A.`Date` IS NOT NULL');
                            
				SELECT @view_stmt2;
		PREPARE statement from @view_stmt2;
		Execute statement;
		Deallocate PREPARE statement;
        
        set @view_stmt = concat('CREATE OR REPLACE VIEW V_UI_Calendar AS
        select A.`EE_Date`, A.`Day_Name`, A.`Day`, A.`Date`, IFNULL(A.Coverage,0) AS Coverage, IFNULL(A.`Rev Lift`,0) AS `Rev Lift`, CASE WHEN A.`EE_Date` <= CURDATE() THEN A.CLM ELSE B.CLM END AS CLM, CASE WHEN A.`EE_Date` <= CURDATE()  THEN A.GTM ELSE B.GTM END AS GTM, A.`Current_Date_Flag`
FROM V_UI_Calendar1 A, V_UI_Calendar2 B
WHERE A.`EE_Date` = B.`EE_Date`');*/
                     
SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("CLM",`CLM`,"GTM",`GTM`,"Outreaches",ifnull(`Coverage`,0),"Incr_Rev",ifnull(`Rev Lift`,0)) )),"]")into @temp_result from V_UI_Calendar ',';') ;
	END IF;
    
     
    
    IF IN_MEASURE = 'COMPARE_WEEK_DATES' 
   THEN
	set @interval_week = in_interval;
   set @view_stmt= concat('CREATE OR REPLACE VIEW V_UI_Calendar
					AS
                    select group_concat(EE_date) AS EE_date from tablenew where EE_date BETWEEN
DATE_FORMAT((NOW() - INTERVAL WEEKDAY(NOW()) DAY - INTERVAL ',@interval_week,'  week), "%Y-%m-%d") AND
			DATE_FORMAT((NOW() + INTERVAL (6 - WEEKDAY(NOW())) DAY - INTERVAL ',@interval_week,'  week), "%Y-%m-%d") ;
					');
                     
SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("EE_date",`EE_date`) )),"]")into @temp_result from V_UI_Calendar ',';') ;
	END IF;
    
     IF IN_MEASURE = 'COMPARE_WEEK_VIEW' 
   THEN
   
   set @view_stmt1= concat('CREATE OR REPLACE VIEW V_UI_Calendar1
					AS
					WITH GROUP1 AS
                            (
							SELECT COUNT(Event_ID) AS CLM,
                            date_format(EE_DATE,"%d-%b") AS "Date"
							from V_Dashboard_Calendar_Temp
                            where Category = "CLM"
                            AND EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                            GROUP BY 2
                            ORDER BY EE_Date
                            )
                             ,
                             GROUP2 AS
							(
                            SELECT COUNT(Event_ID) AS GTM,
                            date_format(EE_DATE,"%d-%b") AS "Date"
							from V_Dashboard_Calendar_Temp
                            where Category = "GTM"
                            AND EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                            GROUP BY 2
                            ORDER BY EE_Date
                            )
                            ,
                             GROUP3 AS
							(
                            SELECT
                            date_format(EE_DATE,"%d-%b") AS "Date",
							CAST(format(ifnull(SUM(`Target_Base`),0),",") AS CHAR) as `Coverage`,
							CAST(format(ifnull(round(SUM(Incremental_Revenue),0),0),",") AS CHAR) AS `Rev Lift`
							from V_Dashboard_Calendar_Temp
							WHERE EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                            GROUP BY 1
							ORDER BY EE_Date
                            )
                             ,
                             GROUP4 AS
							(
                            SELECT 
                            DISTINCT date_format(EE_DATE,"%d-%b") AS `Date`,
                            YEAR(EE_Date) AS `Year`,
                            EE_Date
							from tablenew
                            WHERE EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                            GROUP BY 1
                            ORDER BY EE_Date
                            )
                            SELECT IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM, A.`Date`, A.`Year`, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`, A.EE_Date
                            FROM GROUP4 A
							LEFT JOIN GROUP1 B
                            ON B.`Date`=A.`Date`
                            LEFT JOIN GROUP2 C
                            ON C.`Date`=A.`Date`
                            LEFT JOIN GROUP3 D
                            ON D.`Date`=A.`Date`
                            UNION
                            SELECT IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM, A.`Date`, A.`Year`, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`, A.EE_Date
                            FROM GROUP4 A
							RIGHT JOIN GROUP1 B
                            ON B.`Date`=A.`Date`
                            RIGHT JOIN GROUP2 C
                            ON C.`Date`=A.`Date`
                            RIGHT JOIN GROUP3 D
                            ON D.`Date`=A.`Date`
                            WHERE A.`Date` IS NOT NULL');
                            
				SELECT @view_stmt1;
		PREPARE statement from @view_stmt1;
		Execute statement;
		Deallocate PREPARE statement;

   set @view_stmt2= concat('CREATE OR REPLACE VIEW V_UI_Calendar2
					AS
					WITH GROUP1 AS
                            (
                            select date_format(tablenew.EE_date,"%d-%b") AS `Date`, count(distinct(em.Id)) as "CLM"
							from tablenew
							inner join Event_Master em
							on tablenew.EE_date between em.Start_Date_ID and em.End_Date_ID
							where tablenew.EE_date  BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
							and em.State in( "Enabled","Schedule")
							and em.Id in (select Event_ID from Event_Master_UI where Type_Of_Event = "CLM")
							group by 1
							order by 1
                            )
                             ,
                             GROUP2 AS
							(
                            select date_format(tablenew.EE_date,"%d-%b") AS `Date`, count(distinct(em.Id)) as "GTM"
							from tablenew
							inner join Event_Master em
							on tablenew.EE_date between em.Start_Date_ID and em.End_Date_ID
							where tablenew.EE_date  BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
							-- and em.State in( "Enabled","Schedule")
							and em.Id in (select Event_ID from Event_Master_UI where Type_Of_Event = "GTM")
							group by 1
							order by 1
                            )
                            ,
                             GROUP3 AS
							(
                            SELECT
                            date_format(EE_DATE,"%d-%b") AS "Date",
							CAST(format(ifnull(SUM(`Target_Base`),0),",") AS CHAR) as `Coverage`,
							CAST(format(ifnull(round(SUM(Incremental_Revenue),0),0),",") AS CHAR) AS `Rev Lift`
							from V_Dashboard_Calendar_Temp
							WHERE EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                            GROUP BY 1
							ORDER BY EE_Date
                            )
                             ,
                             GROUP4 AS
							(
                            SELECT 
                            DISTINCT date_format(EE_DATE,"%d-%b") AS `Date`,
                            YEAR(EE_Date) AS `Year`,
                            EE_Date
							from tablenew
                            WHERE EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                            GROUP BY 1
                            ORDER BY EE_Date
                            )
                            SELECT IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM, A.`Date`, A.`Year`, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`, A.EE_Date
                            FROM GROUP4 A
							LEFT JOIN GROUP1 B
                            ON B.`Date`=A.`Date`
                            LEFT JOIN GROUP2 C
                            ON C.`Date`=A.`Date`
                            LEFT JOIN GROUP3 D
                            ON D.`Date`=A.`Date`
                            UNION
                            SELECT IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM, A.`Date`, A.`Year`, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`, A.EE_Date
                            FROM GROUP4 A
							RIGHT JOIN GROUP1 B
                            ON B.`Date`=A.`Date`
                            RIGHT JOIN GROUP2 C
                            ON C.`Date`=A.`Date`
                            RIGHT JOIN GROUP3 D
                            ON D.`Date`=A.`Date`
                            WHERE A.`Date` IS NOT NULL');
                            
					SELECT @view_stmt2;
		PREPARE statement from @view_stmt2;
		Execute statement;
		Deallocate PREPARE statement;
        
        set @view_stmt = concat('CREATE OR REPLACE VIEW V_UI_Calendar AS
        select A.`Date`, A.`Year`, IFNULL(A.Coverage,0) AS Coverage, IFNULL(A.`Rev Lift`,0) AS `Rev Lift`, CASE WHEN A.`EE_Date` <= CURDATE() THEN A.CLM ELSE B.CLM END AS CLM, CASE WHEN A.`EE_Date` <= CURDATE()  THEN A.GTM ELSE B.GTM END AS GTM
FROM V_UI_Calendar1 A, V_UI_Calendar2 B
WHERE A.`Date` = B.`Date`');
                     
SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("CLM",`CLM`,"GTM",`GTM`,"Date",`Date`,"Year",`Year`,"Coverage",ifnull(`Coverage`,0),"Incr_Rev",ifnull(`Rev Lift`,0)) )),"]")into @temp_result from V_UI_Calendar ',';') ;
	END IF;
    
    IF IN_MEASURE = 'COMPARE_WEEK_VIEW' AND IN_AGGREGATION_2 = 'TOTAL' 
   THEN

   set @view_stmt= concat('CREATE OR REPLACE VIEW V_UI_Calendar
					AS
					WITH GROUP1 AS
                            (
                            SELECT IFNULL(COUNT(Event_ID),0) AS CLM,
                            1 AS "NUM"
							from V_Dashboard_Calendar_Temp
                            where Category = "CLM"
                            AND EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                            ORDER BY EE_Date
                            
                            /*
                            select date_format(tablenew.EE_date,"%d-%b") AS `Date`, count(distinct(em.Id)) as "CLM", 1 AS "NUM"
							from tablenew
							inner join Event_Master em
							on tablenew.EE_date between em.Start_Date_ID and em.End_Date_ID
							where 
                            tablenew.EE_date  BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
							and em.State in( "Enabled","Schedule")
							and em.Id in (select Event_ID from V_Event_Master_UI where Type_Of_Event = "CLM")
							
                            
                            
                            SELECT ifnull(COUNT(emui.Event_ID),0) AS CLM,
							1 AS "NUM",
                            date_format(em.End_Date_ID,"%Y-%m-%d") AS `EE_D`
                            from Event_Master_UI emui, Event_Master em
							where emui.Event_ID = em.ID 
							and emui.Type_Of_Event = "CLM"
                            AND date_format(em.End_Date_ID,"%Y-%m-%d") BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                            ORDER BY 3
                            
                            
							
                            */
                            )
                             ,
                             GROUP2 AS
							(
                            SELECT IFNULL(COUNT(Event_ID),0) AS GTM,
                            1 AS "NUM"
							from V_Dashboard_Calendar_Temp
                            where Category = "GTM"
                            AND EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                            ORDER BY EE_Date
                            )
                            ,
                             GROUP3 AS
							(
                            SELECT
                             1 AS "NUM",
							CAST(format(ifnull(SUM(`Target_Base`),0),",") AS CHAR) as `Coverage`,
							CAST(format(ifnull(round(SUM(Incremental_Revenue),0),0),",") AS CHAR) AS `Rev Lift`
							from V_Dashboard_Calendar_Temp
							WHERE EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
							ORDER BY EE_Date
                            )

                            SELECT IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`
                            FROM GROUP3 A
							LEFT JOIN GROUP1 B
                            ON B.`NUM`=A.`NUM`
                            LEFT JOIN GROUP2 C
                            ON C.`NUM`=A.`NUM`
                            
                            UNION
                            SELECT IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM,  IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`
                            FROM GROUP3 A
							RIGHT JOIN GROUP1 B
                            ON B.`NUM`=A.`NUM`
                            RIGHT JOIN GROUP2 C
                            ON C.`NUM`=A.`NUM`
                            
                            WHERE A.`NUM` IS NOT NULL');
                     
SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("CLM",`CLM`,"GTM",`GTM`,"Coverage",ifnull(`Coverage`,0),"Incr_Rev",ifnull(`Rev Lift`,0)) )),"]")into @temp_result from V_UI_Calendar ',';') ;
	END IF;
    
    IF IN_MEASURE = 'COMPARE_WEEK_VIEW' AND IN_AGGREGATION_2 = 'POPUP'
   THEN

   set @view_stmt= concat('CREATE OR REPLACE VIEW V_UI_Calendar
					AS
					WITH GROUP1 AS
                            (
							SELECT DISTINCT(`Trigger`) AS CLM,
                            date_format(EE_DATE,"%d-%b") AS "Date",
                            DAY(EE_Date) AS `Day`
							from V_Dashboard_Calendar_Temp
                            where Category = "CLM"
                            AND DAY(EE_Date) = "',IN_AGGREGATION_1,'"
                            AND YM = "',IN_AGGREGATION_3,'"
                            )
                             ,
                             GROUP2 AS
							(
                            SELECT DISTINCT(`Trigger`) AS GTM,
                            date_format(EE_DATE,"%d-%b") AS "Date",
                            DAY(EE_Date) AS `Day`
							from V_Dashboard_Calendar_Temp
                            where Category = "GTM"
                            AND DAY(EE_Date) = "',IN_AGGREGATION_1,'"
                            AND YM = "',IN_AGGREGATION_3,'"
                            )
                             ,
                             GROUP3 AS
							(
                            SELECT
                            date_format(EE_DATE,"%d-%b") AS "Date",
							CAST(format(ifnull(SUM(`Target_Base`),0),",") AS CHAR) as `Coverage`,
							CAST(format(ifnull(round(SUM(Incremental_Revenue),0),0),",") AS CHAR) AS `Rev Lift`,
                            DAY(EE_Date) AS `Day`
							from V_Dashboard_Calendar_Temp
							WHERE DAY(EE_Date) = "',IN_AGGREGATION_1,'"
                            AND YM = "',IN_AGGREGATION_3,'"
                            ),
                            GROUP4 AS
							(
                            SELECT 
                            DISTINCT date_format(EE_DATE,"%d-%b") AS `Date`,
                            YEAR(EE_Date) AS `Year`,
                            EE_Date
							from tablenew
                            WHERE EE_Date ="',IN_FILTER_CURRENTMONTH,'" 
                            GROUP BY 1
                            ORDER BY EE_Date
                            )
                            SELECT B.CLM, C.GTM, B.`Day`, A.`Date`, Coverage, `Rev Lift`
                            FROM GROUP4 A
							LEFT JOIN GROUP1 B
                            ON B.`Date`=A.`Date`
                            LEFT JOIN GROUP2 C
                            ON C.`Date`=A.`Date`
                            LEFT JOIN GROUP3 D
                            ON D.`Date`=A.`Date`
                            UNION
                            SELECT B.CLM, C.GTM, B.`Day`, A.`Date`, Coverage, `Rev Lift`
                            FROM GROUP4 A
							RIGHT JOIN GROUP1 B
                            ON B.`Date`=A.`Date`
                            RIGHT JOIN GROUP2 C
                            ON C.`Date`=A.`Date`
                            RIGHT JOIN GROUP3 D
                            ON D.`Date`=A.`Date`
                            WHERE A.`DATE` IS NOT NULL
                            ');
                     
SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("CLM",IFNULL(`CLM`,""),"GTM",IFNULL(`GTM`,""),"Date",`Date`,"Day",`Day`,"Coverage",ifnull(`Coverage`,0),"Incr_Rev",ifnull(`Rev Lift`,0)) )),"]")into @temp_result from V_UI_Calendar ',';') ;
	END IF;
    
    IF IN_MEASURE = 'WEEK_VIEW_DATES' 
   THEN

   set @interval_week = in_interval;
   set @view_stmt= concat('CREATE OR REPLACE VIEW V_UI_Calendar
					AS
                    select group_concat(EE_date) AS EE_date from tablenew where EE_date BETWEEN
DATE_FORMAT((NOW() - INTERVAL WEEKDAY(NOW()) DAY - INTERVAL ',@interval_week,'  week), "%Y-%m-%d") AND
			DATE_FORMAT((NOW() + INTERVAL (6 - WEEKDAY(NOW())) DAY - INTERVAL ',@interval_week,'  week), "%Y-%m-%d") ;
					');
                     
SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("EE_date",`EE_date`) )),"]")into @temp_result from V_UI_Calendar ',';') ;
	END IF;
    
	IF IN_MEASURE = 'WEEK_VIEW' AND IN_AGGREGATION_1 <> ''
   THEN
   SELECT CURDATE() INTO @Current_Month;
   SELECT @Current_Month;
	IF IN_AGGREGATION_1 = 'TIME_SLOT1'
    THEN 
    SET @TIME_SLOT_COND = CONCAT('Scheduled_at BETWEEN "09:00:00" AND "11:59:59"');
    END IF;
    
    IF IN_AGGREGATION_1 = 'TIME_SLOT2'
    THEN 
    SET @TIME_SLOT_COND = CONCAT('Scheduled_at BETWEEN "12:00:00" AND "17:00:00"');
    END IF;
    
    IF IN_AGGREGATION_1 = 'TIME_SLOT3'
    THEN 
    SET @TIME_SLOT_COND = CONCAT('Scheduled_at BETWEEN "17:00:00" AND "22:00:00"');
    END IF;
    
    SELECT @TIME_SLOT_COND;
   
   /*
   set @view_stmt= concat('CREATE OR REPLACE VIEW V_UI_Calendar
					AS with 
                    GROUP1 as
					(SELECT Category, COUNT(B.Event_Id) AS CNT,
                    date_format(EE_DATE,"%d-%b") AS "Date",
                    CAST(format(ifnull(SUM(`Target_Base`),0),",") AS CHAR) as `Coverage`,
					CAST(format(ifnull(round(SUM(Incremental_Revenue),0),0),",") AS CHAR) AS `Rev Lift`
					from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
                    WHERE A.Event_Id = B.Event_ID
                    AND EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                    AND Scheduled_at BETWEEN "09:00:00" AND "11:59:59"
					GROUP BY 1,3
					ORDER BY EE_Date),
                    GROUP2 as
							(
                            SELECT 
                            DISTINCT date_format(EE_DATE,"%d-%b") AS `Date`,
                            YEAR(EE_Date) AS `Year`
							from tablenew
                            WHERE EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                            GROUP BY 1
                            ORDER BY EE_Date
                            )
					SELECT IFNULL(B.CLM,0) AS CLM, IFNULL(B.GTM,0) AS GTM, A.`Date`, A.`Year`, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`
                            FROM GROUP2 A
							LEFT JOIN GROUP1 B
                            ON B.`Date`=A.`Date`
                            UNION
                            SELECT IFNULL(B.CLM,0) AS CLM, IFNULL(B.GTM,0) AS GTM, A.`Date`, A.`Year`, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`
                            FROM GROUP2 A
							RIGHT JOIN GROUP1 B
                            ON B.`Date`=A.`Date`
                            WHERE A.`Date` IS NOT NULL ');
                    
	SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Category",`Category`,"Count",`CNT`,"Date",`Date`,"Coverage",CAST(format(ifnull(`Coverage`,0),",") AS CHAR),"Incr_Rev",format(ifnull(`Rev Lift`,0),",")) )),"]")into @temp_result from V_UI_Calendar ',';') ;  */
    
    
    set @view_stmt1= concat('CREATE OR REPLACE VIEW V_UI_Calendar1
					AS
					WITH GROUP1 AS
                            (
							SELECT COUNT(B.Event_ID) AS CLM,
                            date_format(EE_DATE,"%d-%b") AS "Date"
							from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
							WHERE A.Event_Id = B.Event_ID
                            AND Category = "CLM"
                            AND EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                            AND  ',@TIME_SLOT_COND,'
                            GROUP BY 2
                            ORDER BY EE_Date
                            )
                             ,
                             GROUP2 AS
							(
                            SELECT COUNT(B.Event_ID) AS GTM,
                            date_format(EE_DATE,"%d-%b") AS "Date"
							from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
							WHERE A.Event_Id = B.Event_ID
                            AND Category = "GTM"
                            AND EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                            AND  ',@TIME_SLOT_COND,'
                            GROUP BY 2
                            ORDER BY EE_Date
                            )
                            ,
                             GROUP3 AS
							(
                            SELECT
                            date_format(EE_DATE,"%d-%b") AS "Date",
							CAST(format(ifnull(SUM(`Target_Base`),0),",") AS CHAR) as `Coverage`,
							CAST(format(ifnull(round(SUM(Incremental_Revenue),0),0),",") AS CHAR) AS `Rev Lift`
							from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
                    WHERE A.Event_Id = B.Event_ID
							AND EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                            AND ',@TIME_SLOT_COND,'
                            GROUP BY 1
							ORDER BY EE_Date
                            )
                             ,
                             GROUP4 AS
							(
                            SELECT 
                            DISTINCT date_format(EE_DATE,"%d-%b") AS `Date`,
                            YEAR(EE_Date) AS `Year`,
                            EE_Date
							from tablenew
                            WHERE EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                            GROUP BY 1
                            ORDER BY EE_Date
                            )
                            SELECT IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM, A.`Date`, A.`Year`, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`, A.EE_Date
                            FROM GROUP4 A
							LEFT JOIN GROUP1 B
                            ON B.`Date`=A.`Date`
                            LEFT JOIN GROUP2 C
                            ON C.`Date`=A.`Date`
                            LEFT JOIN GROUP3 D
                            ON D.`Date`=A.`Date`
                            UNION
                            SELECT IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM, A.`Date`, A.`Year`, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`, A.EE_Date
                            FROM GROUP4 A
							RIGHT JOIN GROUP1 B
                            ON B.`Date`=A.`Date`
                            RIGHT JOIN GROUP2 C
                            ON C.`Date`=A.`Date`
                            RIGHT JOIN GROUP3 D
                            ON D.`Date`=A.`Date`
                            WHERE A.`Date` IS NOT NULL');
                            
				SELECT @view_stmt1;
		PREPARE statement from @view_stmt1;
		Execute statement;
		Deallocate PREPARE statement;

   set @view_stmt2= concat('CREATE OR REPLACE VIEW V_UI_Calendar2
					AS
					WITH GROUP1 AS
                            (
                            select date_format(tablenew.EE_date,"%d-%b") AS `Date`, count(distinct(em.Id)) as "CLM"
							from tablenew
							inner join Event_Master em
							on tablenew.EE_date between em.Start_Date_ID and em.End_Date_ID
							where tablenew.EE_date  BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
							and em.State in( "Enabled","Schedule")
							and em.Id in (select Event_ID from V_Event_Master_UI where Type_Of_Event = "CLM" AND ',@TIME_SLOT_COND,' )
							group by 1
							order by 1
                            )
                             ,
                             GROUP2 AS
							(
                            select date_format(tablenew.EE_date,"%d-%b") AS `Date`, count(distinct(em.Id)) as "GTM"
							from tablenew
							inner join Event_Master em
							on tablenew.EE_date between em.Start_Date_ID and em.End_Date_ID
							where tablenew.EE_date  BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
							-- and em.State in( "Enabled","Schedule")
							and em.Id in (select Event_ID from V_Event_Master_UI where Type_Of_Event = "GTM" AND ',@TIME_SLOT_COND,' )
							group by 1
							order by 1
                            )
                            ,
                             GROUP3 AS
							(
                            SELECT
                            date_format(EE_DATE,"%d-%b") AS "Date",
							CAST(format(ifnull(SUM(`Target_Base`),0),",") AS CHAR) as `Coverage`,
							CAST(format(ifnull(round(SUM(Incremental_Revenue),0),0),",") AS CHAR) AS `Rev Lift`
							from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
							WHERE A.Event_Id = B.Event_ID
							and EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'" AND ',@TIME_SLOT_COND,' 
                            GROUP BY 1
							ORDER BY EE_Date
                            )
                             ,
                             GROUP4 AS
							(
                            SELECT 
                            DISTINCT date_format(EE_DATE,"%d-%b") AS `Date`,
                            YEAR(EE_Date) AS `Year`,
                            EE_Date
							from tablenew
                            WHERE EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'" 
                            GROUP BY 1
                            ORDER BY EE_Date
                            )
                            SELECT IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM, A.`Date`, A.`Year`, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`, A.EE_Date
                            FROM GROUP4 A
							LEFT JOIN GROUP1 B
                            ON B.`Date`=A.`Date`
                            LEFT JOIN GROUP2 C
                            ON C.`Date`=A.`Date`
                            LEFT JOIN GROUP3 D
                            ON D.`Date`=A.`Date`
                            UNION
                            SELECT IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM, A.`Date`, A.`Year`, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`, A.EE_Date
                            FROM GROUP4 A
							RIGHT JOIN GROUP1 B
                            ON B.`Date`=A.`Date`
                            RIGHT JOIN GROUP2 C
                            ON C.`Date`=A.`Date`
                            RIGHT JOIN GROUP3 D
                            ON D.`Date`=A.`Date`
                            WHERE A.`Date` IS NOT NULL');
                            
					SELECT @view_stmt2;
		PREPARE statement from @view_stmt2;
		Execute statement;
		Deallocate PREPARE statement;
        
        set @view_stmt = concat('CREATE OR REPLACE VIEW V_UI_Calendar AS
        select A.`Date`, A.`Year`, IFNULL(A.Coverage,0) AS Coverage, IFNULL(A.`Rev Lift`,0) AS `Rev Lift`, CASE WHEN A.`EE_Date` <= CURDATE() THEN A.CLM ELSE B.CLM END AS CLM, CASE WHEN A.`EE_Date` <= CURDATE()  THEN A.GTM ELSE B.GTM END AS GTM,CASE WHEN  A.EE_Date = "',@Current_Month,'" THEN "1" ELSE "0" END AS `Current_Month_Flag`
FROM V_UI_Calendar1 A, V_UI_Calendar2 B
WHERE A.`Date` = B.`Date`');
                     
SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("CLM",`CLM`,"GTM",`GTM`,"Date",`Date`,"Year",`Year`,"Coverage",ifnull(`Coverage`,0),"Incr_Rev",ifnull(`Rev Lift`,0),"Current_Date_Flag",`Current_Month_Flag`) )),"]")into @temp_result from V_UI_Calendar ',';') ;
	
    /*
    
    SELECT CURDATE() INTO @Current_Month;
   SELECT @Current_Month;
    set @view_stmt= concat('CREATE OR REPLACE VIEW V_UI_Calendar
					AS
					WITH GROUP1 AS
                            (
                            SELECT COUNT(B.Event_ID) AS CLM,
                            date_format(EE_DATE,"%d-%b") AS "Date"
							from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
							WHERE A.Event_Id = B.Event_ID
                            AND Category = "CLM"
                            AND EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                            AND Scheduled_at BETWEEN "09:00:00" AND "11:59:59" 
                            GROUP BY 2
                            ORDER BY EE_Date
                            
                            /*
                            select date_format(tablenew.EE_date,"%d-%b") AS `Date`, count(distinct(em.Id)) as "CLM"
							from tablenew
							inner join Event_Master em
							on tablenew.EE_date between em.Start_Date_ID and em.End_Date_ID
							where tablenew.EE_date  BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
							and em.State in( "Enabled","Schedule")
							and em.Id in (select Event_ID from V_Event_Master_UI where Type_Of_Event = "CLM" AND Scheduled_at BETWEEN "09:00:00" AND "11:59:59" )
							group by 1
							order by 1
                            
                            
                            SELECT COUNT(emui.Event_ID) AS CLM,
                            date_format(em.End_Date_ID,"%d-%b") AS `Date`,
                            date_format(em.End_Date_ID,"%Y-%m-%d") AS `EE_D`
                            from Event_Master_UI emui, Event_Master em
							where emui.Event_ID = em.ID 
							and emui.Type_Of_Event = "CLM"
                            AND Scheduled_at BETWEEN "09:00:00" AND "11:59:59" 
                            AND date_format(em.End_Date_ID,"%Y-%m-%d") BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                            GROUP BY 2
                            ORDER BY 3
                            
                           
							SELECT COUNT(B.Event_ID) AS CLM,
                            date_format(EE_DATE,"%d-%b") AS "Date"
							from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
							WHERE A.Event_Id = B.Event_ID
                            AND Category = "CLM"
                            AND EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                            AND Scheduled_at BETWEEN "09:00:00" AND "11:59:59" 
                            GROUP BY 2
                            ORDER BY EE_Date
                            */
                            /*)
                             ,
                             GROUP2 AS
							(
                            SELECT COUNT(B.Event_ID) AS GTM,
                            date_format(EE_DATE,"%d-%b") AS "Date"
							from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
							WHERE A.Event_Id = B.Event_ID
                            AND Category = "GTM"
                            AND EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                            AND Scheduled_at BETWEEN "09:00:00" AND "11:59:59" 
                            GROUP BY 2
                            ORDER BY EE_Date
                            )
                            ,
                             GROUP3 AS
							(
                            SELECT
                            date_format(EE_DATE,"%d-%b") AS "Date",
							CAST(format(ifnull(SUM(`Target_Base`),0),",") AS CHAR) as `Coverage`,
							CAST(format(ifnull(round(SUM(Incremental_Revenue),0),0),",") AS CHAR) AS `Rev Lift`
							from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
                    WHERE A.Event_Id = B.Event_ID
							AND EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                            AND Scheduled_at BETWEEN "09:00:00" AND "11:59:59"
                            GROUP BY 1
							ORDER BY EE_Date
                            )
                             ,
                             GROUP4 AS
							(
                            SELECT 
                            DISTINCT date_format(EE_DATE,"%d-%b") AS `Date`,
                            YEAR(EE_Date) AS `Year`,
                            EE_Date
							from tablenew
                            WHERE EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                            GROUP BY 1
                            ORDER BY EE_Date
                            )
                            SELECT IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM, A.`Date`, A.`Year`, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`,CASE WHEN  A.EE_Date = "',@Current_Month,'" THEN "1" ELSE "0" END AS `Current_Month_Flag`
                            FROM GROUP4 A
							LEFT JOIN GROUP1 B
                            ON B.`Date`=A.`Date`
                            LEFT JOIN GROUP2 C
                            ON C.`Date`=A.`Date`
                            LEFT JOIN GROUP3 D
                            ON D.`Date`=A.`Date`
                            UNION
                            SELECT IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM, A.`Date`, A.`Year`, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`,CASE WHEN  A.EE_Date = "',@Current_Month,'" THEN "1" ELSE "0" END AS `Current_Month_Flag`
                            FROM GROUP4 A
							RIGHT JOIN GROUP1 B
                            ON B.`Date`=A.`Date`
                            RIGHT JOIN GROUP2 C
                            ON C.`Date`=A.`Date`
                            RIGHT JOIN GROUP3 D
                            ON D.`Date`=A.`Date`
                            WHERE A.`Date` IS NOT NULL');
                     
SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("CLM",`CLM`,"GTM",`GTM`,"Date",`Date`,"Year",`Year`,"Coverage",ifnull(`Coverage`,0),"Current_Date_Flag",`Current_Month_Flag`,"Incr_Rev",ifnull(`Rev Lift`,0)) )),"]")into @temp_result from V_UI_Calendar ',';') ;
    */
	END IF;
    /*
    IF IN_MEASURE = 'WEEK_VIEW' AND IN_AGGREGATION_1 = 'TIME_SLOT2'
   THEN

    /*
    set @view_stmt= concat('CREATE OR REPLACE VIEW V_UI_Calendar
					AS
					(SELECT Category, COUNT(B.Event_Id) AS CNT,
                    date_format(EE_DATE,"%d-%b") AS "Date",
                    CAST(format(ifnull(SUM(`Target_Base`),0),",") AS CHAR) as `Coverage`,
					CAST(format(ifnull(round(SUM(Incremental_Revenue),0),0),",") AS CHAR) AS `Rev Lift`
					from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
                    WHERE A.Event_Id = B.Event_ID
                    AND EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                    AND Scheduled_at BETWEEN "12:00:00" AND "05:00:00"
					GROUP BY 1,3
					ORDER BY EE_Date)');*/
                    
     /*SELECT CURDATE() INTO @Current_Month;
   SELECT @Current_Month;                
	 set @view_stmt= concat('CREATE OR REPLACE VIEW V_UI_Calendar
					AS
					WITH GROUP1 AS
                            (
                            
                            SELECT COUNT(B.Event_ID) AS CLM,
                            date_format(EE_DATE,"%d-%b") AS "Date"
							from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
                    WHERE A.Event_Id = B.Event_ID
                            AND Category = "CLM"
                            AND EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                            AND Scheduled_at BETWEEN "12:00:00" AND "17:00:00" 
                            GROUP BY 2
                            ORDER BY EE_Date
                            /*
                            select date_format(tablenew.EE_date,"%d-%b") AS `Date`, count(distinct(em.Id)) as "CLM"
							from tablenew
							inner join Event_Master em
							on tablenew.EE_date between em.Start_Date_ID and em.End_Date_ID
							where tablenew.EE_date  BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
							and em.State in( "Enabled","Schedule")
							and em.Id in (select Event_ID from V_Event_Master_UI where Type_Of_Event = "CLM" AND Scheduled_at BETWEEN "12:00:00" AND "17:00:00"  )
							group by 1
							order by 1
                            
                            SELECT COUNT(emui.Event_ID) AS CLM,
                            date_format(em.End_Date_ID,"%d-%b") AS `Date`,
                            date_format(em.End_Date_ID,"%Y-%m-%d") AS `EE_D`
                            from Event_Master_UI emui, Event_Master em
							where emui.Event_ID = em.ID 
							and emui.Type_Of_Event = "CLM"
                            AND Scheduled_at BETWEEN "12:00:00" AND "17:00:00" 
                            AND date_format(em.End_Date_ID,"%Y-%m-%d") BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                            GROUP BY 2
                            ORDER BY 3
                            
                            
							SELECT COUNT(B.Event_ID) AS CLM,
                            date_format(EE_DATE,"%d-%b") AS "Date"
							from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
                    WHERE A.Event_Id = B.Event_ID
                            AND Category = "CLM"
                            AND EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                            AND Scheduled_at BETWEEN "12:00:00" AND "17:00:00" 
                            GROUP BY 2
                            ORDER BY EE_Date*/
                           /* )
                             ,
                             GROUP2 AS
							(
                            
                              SELECT COUNT(B.Event_ID) AS GTM,
                            date_format(EE_DATE,"%d-%b") AS "Date"
							from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
                    WHERE A.Event_Id = B.Event_ID
                            AND Category = "GTM"
                            AND EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                            AND Scheduled_at BETWEEN "12:00:00" AND "17:00:00" 
                            GROUP BY 2
                            ORDER BY EE_Date
                            
                            /*
                            SELECT COUNT(B.Event_ID) AS GTM,
                            date_format(EE_DATE,"%d-%b") AS "Date"
							from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
                    WHERE A.Event_Id = B.Event_ID
                            AND Category = "GTM"
                            AND EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                            AND Scheduled_at BETWEEN "12:00:00" AND "17:00:00"
                            GROUP BY 2
                            ORDER BY EE_Date*/
                            /*)
                            ,
                             GROUP3 AS
							(
                            SELECT
                            date_format(EE_DATE,"%d-%b") AS "Date",
							CAST(format(ifnull(SUM(`Target_Base`),0),",") AS CHAR) as `Coverage`,
							CAST(format(ifnull(round(SUM(Incremental_Revenue),0),0),",") AS CHAR) AS `Rev Lift`
							from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
                    WHERE A.Event_Id = B.Event_ID
							AND EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                            AND Scheduled_at BETWEEN "12:00:00" AND "17:00:00"
                            GROUP BY 1
							ORDER BY EE_Date
                            )
                             ,
                             GROUP4 AS
							(
                            SELECT 
                            DISTINCT date_format(EE_DATE,"%d-%b") AS `Date`,
                            YEAR(EE_Date) AS `Year`,
                            EE_Date
							from tablenew
                            WHERE EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                            GROUP BY 1
                            ORDER BY EE_Date
                            )
                            SELECT IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM, A.`Date`, A.`Year`, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`,CASE WHEN  A.EE_Date = "',@Current_Month,'" THEN "1" ELSE "0" END AS `Current_Month_Flag`
                            FROM GROUP4 A
							LEFT JOIN GROUP1 B
                            ON B.`Date`=A.`Date`
                            LEFT JOIN GROUP2 C
                            ON C.`Date`=A.`Date`
                            LEFT JOIN GROUP3 D
                            ON D.`Date`=A.`Date`
                            UNION
                            SELECT IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM, A.`Date`, A.`Year`, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`,CASE WHEN  A.EE_Date = "',@Current_Month,'" THEN "1" ELSE "0" END AS `Current_Month_Flag`
                            FROM GROUP4 A
							RIGHT JOIN GROUP1 B
                            ON B.`Date`=A.`Date`
                            RIGHT JOIN GROUP2 C
                            ON C.`Date`=A.`Date`
                            RIGHT JOIN GROUP3 D
                            ON D.`Date`=A.`Date`
                            WHERE A.`Date` IS NOT NULL');
                     
SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("CLM",`CLM`,"GTM",`GTM`,"Date",`Date`,"Year",`Year`,"Coverage",ifnull(`Coverage`,0),"Current_Date_Flag",`Current_Month_Flag`,"Incr_Rev",ifnull(`Rev Lift`,0)) )),"]")into @temp_result from V_UI_Calendar ',';') ;  
    
	END IF;
    
	IF IN_MEASURE = 'WEEK_VIEW' AND IN_AGGREGATION_1 = 'TIME_SLOT3'
   THEN
    
    /*set @view_stmt= concat('CREATE OR REPLACE VIEW V_UI_Calendar
					AS
					(SELECT Category, COUNT(B.Event_Id) AS CNT,
                    date_format(EE_DATE,"%d-%b") AS "Date",
                    CAST(format(ifnull(SUM(`Target_Base`),0),",") AS CHAR) as `Coverage`,
					CAST(format(ifnull(round(SUM(Incremental_Revenue),0),0),",") AS CHAR) AS `Rev Lift`
					from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
                    WHERE A.Event_Id = B.Event_ID
                    AND EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                    AND Scheduled_at BETWEEN "05:00:00" AND "10:00:00"
					GROUP BY 1,3
					ORDER BY EE_Date)');*/
	/*set @view_stmt= concat('CREATE OR REPLACE VIEW V_UI_Calendar
					AS
					WITH GROUP1 AS
                            (SELECT COUNT(B.Event_ID) AS CLM,
                            date_format(EE_DATE,"%d-%b") AS "Date"
							from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
                    WHERE A.Event_Id = B.Event_ID
                            AND Category = "CLM"
                            AND EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                             AND Scheduled_at BETWEEN "17:00:00" AND "22:00:00"
                            GROUP BY 2
                            ORDER BY EE_Date
                            
                            /*
                            select date_format(tablenew.EE_date,"%d-%b") AS `Date`, count(distinct(em.Id)) as "CLM"
							from tablenew
							inner join Event_Master em
							on tablenew.EE_date between em.Start_Date_ID and em.End_Date_ID
							where tablenew.EE_date  BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
							and em.State in( "Enabled","Schedule")
							and em.Id in (select Event_ID from V_Event_Master_UI where Type_Of_Event = "CLM" AND Scheduled_at  BETWEEN "17:00:00" AND "22:00:00" )
							group by 1
							order by 1
                           
							SELECT COUNT(B.Event_ID) AS CLM,
                            date_format(EE_DATE,"%d-%b") AS "Date"
							from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
                    WHERE A.Event_Id = B.Event_ID
                            AND Category = "CLM"
                            AND EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                             AND Scheduled_at BETWEEN "17:00:00" AND "22:00:00"
                            GROUP BY 2
                            ORDER BY EE_Date*/
                           /* )
                             ,
                             GROUP2 AS
							(
                            SELECT COUNT(B.Event_ID) AS GTM,
                            date_format(EE_DATE,"%d-%b") AS "Date"
							from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
                    WHERE A.Event_Id = B.Event_ID
                            AND Category = "GTM"
                            AND EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                             AND Scheduled_at BETWEEN "17:00:00" AND "22:00:00"
                            GROUP BY 2
                            ORDER BY EE_Date
                            /*
                            SELECT COUNT(B.Event_ID) AS GTM,
                            date_format(EE_DATE,"%d-%b") AS "Date"
							from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
                    WHERE A.Event_Id = B.Event_ID
                            AND Category = "GTM"
                            AND EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                             AND Scheduled_at BETWEEN "17:00:00" AND "22:00:00" 
                            GROUP BY 2
                            ORDER BY EE_Date*/
                            /*)
                            ,
                             GROUP3 AS
							(
                            SELECT
                            date_format(EE_DATE,"%d-%b") AS "Date",
							CAST(format(ifnull(SUM(`Target_Base`),0),",") AS CHAR) as `Coverage`,
							CAST(format(ifnull(round(SUM(Incremental_Revenue),0),0),",") AS CHAR) AS `Rev Lift`
							from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
                    WHERE A.Event_Id = B.Event_ID
							AND EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                             AND Scheduled_at BETWEEN "17:00:00" AND "22:00:00"
                            GROUP BY 1
							ORDER BY EE_Date
                            )
                             ,
                             GROUP4 AS
							(
                            SELECT 
                            DISTINCT date_format(EE_DATE,"%d-%b") AS `Date`,
                            YEAR(EE_Date) AS `Year`,EE_Date
							from tablenew
                            WHERE EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                            GROUP BY 1
                            ORDER BY EE_Date
                            )
                            SELECT IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM, A.`Date`, A.`Year`, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`,CASE WHEN  A.EE_Date = "',@Current_Month,'" THEN "1" ELSE "0" END AS `Current_Month_Flag`
                            FROM GROUP4 A
							LEFT JOIN GROUP1 B
                            ON B.`Date`=A.`Date`
                            LEFT JOIN GROUP2 C
                            ON C.`Date`=A.`Date`
                            LEFT JOIN GROUP3 D
                            ON D.`Date`=A.`Date`
                            UNION
                            SELECT IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM, A.`Date`, A.`Year`, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`,CASE WHEN  A.EE_Date = "',@Current_Month,'" THEN "1" ELSE "0" END AS `Current_Month_Flag`
                            FROM GROUP4 A
							RIGHT JOIN GROUP1 B
                            ON B.`Date`=A.`Date`
                            RIGHT JOIN GROUP2 C
                            ON C.`Date`=A.`Date`
                            RIGHT JOIN GROUP3 D
                            ON D.`Date`=A.`Date`
                            WHERE A.`Date` IS NOT NULL');
                     
SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("CLM",`CLM`,"GTM",`GTM`,"Date",`Date`,"Year",`Year`,"Coverage",ifnull(`Coverage`,0),"Current_Date_Flag",`Current_Month_Flag`,"Incr_Rev",ifnull(`Rev Lift`,0)) )),"]")into @temp_result from V_UI_Calendar ',';') ;  
                     
  
    
	END IF;
    
    */
    IF IN_MEASURE = 'WEEK_VIEW' AND IN_AGGREGATION_2 = 'TOTAL'
   THEN
	
    
    
    SELECT CURDATE() INTO @Current_Month;
   SELECT @Current_Month;                
	 set @view_stmt= concat('CREATE OR REPLACE VIEW V_UI_Calendar
					AS
					WITH GROUP1 AS
                            (
                            SELECT COUNT(B.Event_ID) AS CLM,
                            date_format(EE_DATE,"%d-%b") AS "Date",
                            1 AS "NUM"
							from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
							WHERE A.Event_Id = B.Event_ID
                            AND Category = "CLM"
                            AND EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                            ORDER BY EE_Date
                            
                            /*
                            select date_format(tablenew.EE_date,"%d-%b") AS `Date`, count(distinct(em.Id)) as "CLM"
							from tablenew
							inner join Event_Master em
							on tablenew.EE_date between em.Start_Date_ID and em.End_Date_ID
							where tablenew.EE_date  BETWEEN "2023-05-01" AND "2023-05-07"
							and em.State in( "Enabled","Schedule")
							and em.Id in (select Event_ID from V_Event_Master_UI where Type_Of_Event = "CLM" AND Scheduled_at BETWEEN "09:00:00" AND "11:59:59" )
							group by 1
							order by 1
                            
                            
                            SELECT COUNT(emui.Event_ID) AS CLM,
                            date_format(em.End_Date_ID,"%d-%b") AS `Date`,
                            date_format(em.End_Date_ID,"%Y-%m-%d") AS `EE_D`
                            from Event_Master_UI emui, Event_Master em
							where emui.Event_ID = em.ID 
							and emui.Type_Of_Event = "CLM"
                            AND Scheduled_at BETWEEN "09:00:00" AND "11:59:59" 
                            AND date_format(em.End_Date_ID,"%Y-%m-%d") BETWEEN "2023-05-01" AND "2023-05-07"
                            GROUP BY 2
                            ORDER BY 3
                            
                           
							SELECT COUNT(B.Event_ID) AS CLM,
                            date_format(EE_DATE,"%d-%b") AS "Date"
							from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
							WHERE A.Event_Id = B.Event_ID
                            AND Category = "CLM"
                            AND EE_Date BETWEEN "2023-05-01" AND "2023-05-07"
                            AND Scheduled_at BETWEEN "09:00:00" AND "11:59:59" 
                            GROUP BY 2
                            ORDER BY EE_Date
                            */
                            )
                             ,
                             GROUP2 AS
							(
                            SELECT COUNT(B.Event_ID) AS GTM,
                            1 AS "NUM",
                            date_format(EE_DATE,"%d-%b") AS "Date"
							from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
							WHERE A.Event_Id = B.Event_ID
                            AND Category = "GTM"
                            AND EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                            ORDER BY EE_Date
                            )
                            ,
                             GROUP3 AS
							(
                            SELECT
                            date_format(EE_DATE,"%d-%b") AS "Date",
                            1 AS "NUM",
							CAST(format(ifnull(SUM(`Target_Base`),0),",") AS CHAR) as `Coverage`,
							CAST(format(ifnull(round(SUM(Incremental_Revenue),0),0),",") AS CHAR) AS `Rev Lift`
							from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
                    WHERE A.Event_Id = B.Event_ID
							AND EE_Date BETWEEN "',IN_FILTER_CURRENTMONTH,'" AND "',IN_FILTER_MONTHDURATION,'"
                            ORDER BY EE_Date
                            )
                            SELECT IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`
                            FROM GROUP3 A
							LEFT JOIN GROUP1 B
                            ON B.`NUM`=A.`NUM`
                            LEFT JOIN GROUP2 C
                            ON C.`NUM`=A.`NUM`
                            
                            UNION
                            SELECT IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`
                            FROM GROUP3 A
							RIGHT JOIN GROUP1 B
                            ON B.`NUM`=A.`NUM`
                            RIGHT JOIN GROUP2 C
                            ON C.`NUM`=A.`NUM`
                           ;');
                     
SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("CLM",`CLM`,"GTM",`GTM`,"Coverage",ifnull(`Coverage`,0),"Incr_Rev",ifnull(`Rev Lift`,0)) )),"]")into @temp_result from V_UI_Calendar ',';') ;  
    end if;
    
    
    IF IN_MEASURE = 'WEEK_VIEW' AND IN_AGGREGATION_2 = 'POPUP'
   THEN
	IF IN_AGGREGATION_1 = 'TIME_SLOT1'
    THEN 
    SET @TIME_SLOT_COND = CONCAT('Scheduled_at BETWEEN "09:00:00" AND "11:59:59"');
    END IF;
    
    IF IN_AGGREGATION_1 = 'TIME_SLOT2'
    THEN 
    SET @TIME_SLOT_COND = CONCAT('Scheduled_at BETWEEN "12:00:00" AND "17:00:00"');
    END IF;
    
    IF IN_AGGREGATION_1 = 'TIME_SLOT3'
    THEN 
    SET @TIME_SLOT_COND = CONCAT('Scheduled_at BETWEEN "17:00:00" AND "22:00:00"');
    END IF;
    
    SELECT @TIME_SLOT_COND;
   set @view_stmt= concat('CREATE OR REPLACE VIEW V_UI_Calendar
					AS
					WITH GROUP1 AS
                            (
							SELECT DISTINCT(`Trigger`) AS CLM,
                            date_format(EE_DATE,"%d-%b") AS "Date",
                            DAY(EE_Date) AS `Day`
							from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
                    WHERE A.Event_Id = B.Event_ID
                            AND Category = "CLM"
                            AND DAY(EE_Date) = "',IN_AGGREGATION_4,'"
                            AND YM = "',IN_AGGREGATION_3,'"
                            AND ',@TIME_SLOT_COND,'
                            )
                             ,
                             GROUP2 AS
							(
                            SELECT DISTINCT(`Trigger`) AS GTM,
                            date_format(EE_DATE,"%d-%b") AS "Date",
                            DAY(EE_Date) AS `Day`
							from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
                    WHERE A.Event_Id = B.Event_ID
                            AND Category = "GTM"
                            AND DAY(EE_Date) = "',IN_AGGREGATION_4,'"
                            AND YM = "',IN_AGGREGATION_3,'"
                            AND ',@TIME_SLOT_COND,'
                            )
                             ,
                             GROUP3 AS
							(
                            SELECT
                            date_format(EE_DATE,"%d-%b") AS "Date",
							CAST(format(ifnull(SUM(`Target_Base`),0),",") AS CHAR) as `Coverage`,
							CAST(format(ifnull(round(SUM(Incremental_Revenue),0),0),",") AS CHAR) AS `Rev Lift`,
                            DAY(EE_Date) AS `Day`
							from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
                    WHERE A.Event_Id = B.Event_ID
							AND DAY(EE_Date) = "',IN_AGGREGATION_4,'"
                            AND YM = "',IN_AGGREGATION_3,'"
                            AND ',@TIME_SLOT_COND,'
                            )
                            SELECT A.CLM, B.GTM, A.`Day`, A.`Date`, Coverage, `Rev Lift`
                            FROM GROUP1 A
							JOIN GROUP2 B
                            ON B.`Date`=A.`Date`
                            JOIN GROUP3 C
                            ON C.`Date`=B.`Date`');
                     
SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("CLM",IFNULL(`CLM`,""),"GTM",IFNULL(`GTM`,""),"Date",`Date`,"Day",`Day`,"Coverage",ifnull(`Coverage`,0),"Incr_Rev",ifnull(`Rev Lift`,0)) )),"]")into @temp_result from V_UI_Calendar ',';') ;
	END IF;
    
    IF IN_MEASURE = 'DAY_VIEW_DATES' 
   THEN
	set @interval_week = in_interval;
   set @view_stmt= concat('CREATE OR REPLACE VIEW V_UI_Calendar
					AS
					SELECT CURDATE() - INTERVAL ',@interval_week,' DAY AS Today,
                    DAYNAME(CURDATE() - INTERVAL ',@interval_week,' DAY) AS WKDAY;
                    ');
                     
SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Today",`Today`,"WEEKDAY",`WKDAY`) )),"]")into @temp_result from V_UI_Calendar ',';') ;
	END IF;
    
    IF IN_MEASURE = 'DAY_VIEW_DATE' 
   THEN
   set @view_stmt= concat('CREATE OR REPLACE VIEW V_UI_Calendar
					AS
					SELECT DAYNAME("',IN_AGGREGATION_1,'") AS WEEKDAY;');
                     
SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("WEEKDAY",`WEEKDAY`) )),"]")into @temp_result from V_UI_Calendar ',';') ;
	END IF;
    
	IF IN_MEASURE = 'DAY_VIEW' AND IN_AGGREGATION_1 <> ''
    then
    IF IN_AGGREGATION_1 = 'TIME_SLOT1'
    THEN 
    SET @TIME_SLOT_COND = CONCAT('Scheduled_at BETWEEN "09:00:00" AND "11:59:59"');
    END IF;
    
    IF IN_AGGREGATION_1 = 'TIME_SLOT2'
    THEN 
    SET @TIME_SLOT_COND = CONCAT('Scheduled_at BETWEEN "12:00:00" AND "17:00:00"');
    END IF;
    
    IF IN_AGGREGATION_1 = 'TIME_SLOT3'
    THEN 
    SET @TIME_SLOT_COND = CONCAT('Scheduled_at BETWEEN "17:00:00" AND "22:00:00"');
    END IF;
    
    SELECT @TIME_SLOT_COND;
    
  

   set @view_stmt1= concat('CREATE OR REPLACE VIEW V_UI_Calendar1
					AS
					WITH GROUP1 AS
                            (
							SELECT SUM(Target_Base) AS SMS,
                            date_format(EE_DATE,"%d-%b") AS "Date"
							from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
                    WHERE A.Event_Id = B.Event_ID
                            AND Channel = "SMS"
                            AND EE_Date = "',IN_FILTER_MONTHDURATION,'"
                            AND ',@TIME_SLOT_COND,'
                            GROUP BY 2
                            ORDER BY EE_Date
                            )
                             ,
                             GROUP2 AS
							(
                            SELECT SUM(Target_Base) AS "EMAIL",
                            date_format(EE_DATE,"%d-%b") AS "Date"
							from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
                    WHERE A.Event_Id = B.Event_ID
                            AND Channel = "EMAIL"
                            AND EE_Date = "',IN_FILTER_MONTHDURATION,'"
                            AND ',@TIME_SLOT_COND,'
                            GROUP BY 2
                            ORDER BY EE_Date
                            )
                            ,
                            GROUP3 AS
							(
                            SELECT SUM(Target_Base) AS "PN",
                            date_format(EE_DATE,"%d-%b") AS "Date"
							from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
                    WHERE A.Event_Id = B.Event_ID
                            AND Channel = "PN"
                            AND EE_Date = "',IN_FILTER_MONTHDURATION,'"
                            AND ',@TIME_SLOT_COND,'
                            GROUP BY 2
                            ORDER BY EE_Date
                            )
                            ,
                            GROUP5 AS
							(
                            SELECT SUM(Target_Base) AS "WHATSAPP",
                            date_format(EE_DATE,"%d-%b") AS "Date"
							from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
                    WHERE A.Event_Id = B.Event_ID
                            AND Channel = "WHATSAPP"
                            AND EE_Date = "',IN_FILTER_MONTHDURATION,'"
                            AND ',@TIME_SLOT_COND,'
                            GROUP BY 2
                            ORDER BY EE_Date
                            )
                            ,
                             GROUP4 AS
							(
                            SELECT 
                            DISTINCT date_format(EE_DATE,"%d-%b") AS `Date`,
                            YEAR(EE_Date) AS `Year`,
                            EE_Date
							from tablenew
                            WHERE EE_Date = "',IN_FILTER_MONTHDURATION,'"
                            GROUP BY 1
                            ORDER BY EE_Date
                            )
                            SELECT IFNULL(B.SMS,0) AS SMS, IFNULL(C.EMAIL,0) AS EMAIL,IFNULL(D.PN,0) AS PN,IFNULL(E.WHATSAPP,0) AS WHATSAPP, A.`Date`, A.`Year`,A.EE_Date
                            FROM GROUP4 A
							LEFT JOIN GROUP1 B
                            ON B.`Date`=A.`Date`
                            LEFT JOIN GROUP2 C
                            ON C.`Date`=A.`Date`
                            LEFT JOIN GROUP3 D
                            ON D.`Date`=A.`Date`
                            LEFT JOIN GROUP5 E
                            ON E.`Date`=A.`Date`
                            UNION
                            SELECT IFNULL(B.SMS,0) AS SMS, IFNULL(C.EMAIL,0) AS EMAIL,IFNULL(D.PN,0) AS PN,IFNULL(E.WHATSAPP,0) AS WHATSAPP, A.`Date`, A.`Year`,A.EE_Date
                            FROM GROUP4 A
							RIGHT JOIN GROUP1 B
                            ON B.`Date`=A.`Date`
                            RIGHT JOIN GROUP2 C
                            ON C.`Date`=A.`Date`
                            RIGHT JOIN GROUP3 D
                            ON D.`Date`=A.`Date`
                            RIGHT JOIN GROUP5 E
                            ON E.`Date`=A.`Date`
                            WHERE A.`Date` IS NOT NULL
					/*(SELECT Channel,EE_Date,date_format(EE_DATE,"%d-%b") AS "Date",
                    SUM(Target_Base) AS Base
					from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
                    WHERE A.Event_Id = B.Event_ID
                    AND EE_Date = ',IN_FILTER_MONTHDURATION,'
                    AND ',@TIME_SLOT_COND,'
					GROUP BY 1,2)*/');
                    
	select @view_stmt1;
	PREPARE statement from @view_stmt1;
	Execute statement;
	Deallocate PREPARE statement;
    
    
   set @view_stmt2= concat('CREATE OR REPLACE VIEW V_UI_Calendar2
					AS
					WITH GROUP1 AS
                            (
                            SELECT COUNT(B.Event_ID) AS CLM,
                            date_format(EE_DATE,"%d-%b") AS "Date"
							from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
							WHERE A.Event_Id = B.Event_ID
                            AND Category = "CLM"
                            AND EE_Date = "',IN_FILTER_MONTHDURATION,'"
                            AND  ',@TIME_SLOT_COND,'
                            GROUP BY 2
                            ORDER BY EE_Date
                            )
                             ,
                             GROUP2 AS
							(
                            SELECT COUNT(B.Event_ID) AS GTM,
                            date_format(EE_DATE,"%d-%b") AS "Date"
							from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
							WHERE A.Event_Id = B.Event_ID
                            AND Category = "GTM"
                            AND EE_Date = "',IN_FILTER_MONTHDURATION,'"
                            AND  ',@TIME_SLOT_COND,'
                            GROUP BY 2
                            ORDER BY EE_Date
                            )
                            ,
                             GROUP3 AS
							(
                            SELECT
                            date_format(EE_DATE,"%d-%b") AS "Date",
							CAST(format(ifnull(SUM(`Target_Base`),0),",") AS CHAR) as `Coverage`,
							CAST(format(ifnull(round(SUM(Incremental_Revenue),0),0),",") AS CHAR) AS `Rev Lift`
							from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
							WHERE A.Event_Id = B.Event_ID
							AND EE_Date = "',IN_FILTER_MONTHDURATION,'"
                            AND  ',@TIME_SLOT_COND,'
                            GROUP BY 1
							ORDER BY EE_Date
                            )
                             ,
                             GROUP4 AS
							(
                            SELECT 
                            DISTINCT date_format(EE_DATE,"%d-%b") AS `Date`,
                            YEAR(EE_Date) AS `Year`,EE_Date
							from tablenew
                            WHERE  EE_Date = "',IN_FILTER_MONTHDURATION,'"
                            GROUP BY 1
                            ORDER BY EE_Date
                            )
                            SELECT IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM, A.`Date`, A.`Year`, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`
                            FROM GROUP4 A
							LEFT JOIN GROUP1 B
                            ON B.`Date`=A.`Date`
                            LEFT JOIN GROUP2 C
                            ON C.`Date`=A.`Date`
                            LEFT JOIN GROUP3 D
                            ON D.`Date`=A.`Date`
                            UNION
                            SELECT IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM, A.`Date`, A.`Year`, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`
                            FROM GROUP4 A
							RIGHT JOIN GROUP1 B
                            ON B.`Date`=A.`Date`
                            RIGHT JOIN GROUP2 C
                            ON C.`Date`=A.`Date`
                            RIGHT JOIN GROUP3 D
                            ON D.`Date`=A.`Date`
                            WHERE A.`Date` IS NOT NULL');
                                     
	
    select @view_stmt2;
	PREPARE statement from @view_stmt2;
	Execute statement;
	Deallocate PREPARE statement;
    /*
   set @view_stmt2= concat('CREATE OR REPLACE VIEW V_UI_Calendar2
					AS
					WITH GROUP1 AS
                            (
                            SELECT COUNT(B.Event_ID) AS CLM,
                            date_format(EE_DATE,"%d-%b") AS "Date"
							from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
							WHERE A.Event_Id = B.Event_ID
                            AND Category = "CLM"
                            AND EE_Date = "',IN_FILTER_MONTHDURATION,'"
                            AND  ',@TIME_SLOT_COND,'
                            GROUP BY 2
                            ORDER BY EE_Date
                            
                            
                            )
                             ,
                             GROUP2 AS
							(
                             SELECT COUNT(B.Event_ID) AS GTM,
                            date_format(EE_DATE,"%d-%b") AS "Date"
							from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
                    WHERE A.Event_Id = B.Event_ID
                            AND Category = "GTM"
                            AND EE_Date = "',IN_FILTER_MONTHDURATION,'"
                            AND  ',@TIME_SLOT_COND,'
                            GROUP BY 2
                            ORDER BY EE_Date
                            
                            
                            SELECT COUNT(B.Event_ID) AS GTM,
                            date_format(EE_DATE,"%d-%b") AS "Date"
							from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
                    WHERE A.Event_Id = B.Event_ID
                            AND Category = "GTM"
                            AND EE_Date = "',IN_FILTER_MONTHDURATION,'"
                            AND Scheduled_at BETWEEN "09:00:00" AND "11:59:59"
                            GROUP BY 2
                            ORDER BY EE_Date
                            )
                            ,
                             GROUP3 AS
							(
                            SELECT
                            date_format(EE_DATE,"%d-%b") AS "Date",
							CAST(format(ifnull(SUM(`Target_Base`),0),",") AS CHAR) as `Coverage`,
							CAST(format(ifnull(round(SUM(Incremental_Revenue),0),0),",") AS CHAR) AS `Rev Lift`
							from V_Dashboard_Calendar_Temp A, V_Event_Master_UI B
                    WHERE A.Event_Id = B.Event_ID
							AND EE_Date = "',IN_FILTER_MONTHDURATION,'"
                            AND  ',@TIME_SLOT_COND,'
                            GROUP BY 1
							ORDER BY EE_Date
                            )
                             ,
                             GROUP4 AS
							(
                            SELECT 
                            DISTINCT date_format(EE_DATE,"%d-%b") AS `Date`,
                            YEAR(EE_Date) AS `Year`,
                            EE_Date
							from tablenew
                            WHERE EE_Date = "',IN_FILTER_MONTHDURATION,'"
                            GROUP BY 1
                            ORDER BY EE_Date
                            )
                            SELECT IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM, A.`Date`, A.`Year`, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`,CASE WHEN  A.EE_Date = "',@Current_Month,'" THEN "1" ELSE "0" END AS `Current_Month_Flag`
                            FROM GROUP4 A
							LEFT JOIN GROUP1 B
                            ON B.`Date`=A.`Date`
                            LEFT JOIN GROUP2 C
                            ON C.`Date`=A.`Date`
                            LEFT JOIN GROUP3 D
                            ON D.`Date`=A.`Date`
                            UNION
                            SELECT IFNULL(B.CLM,0) AS CLM, IFNULL(C.GTM,0) AS GTM, A.`Date`, A.`Year`, IFNULL(Coverage,0) AS Coverage, IFNULL(`Rev Lift`,0) AS `Rev Lift`,CASE WHEN  A.EE_Date = "',@Current_Month,'" THEN "1" ELSE "0" END AS `Current_Month_Flag`
                            FROM GROUP4 A
							RIGHT JOIN GROUP1 B
                            ON B.`Date`=A.`Date`
                            RIGHT JOIN GROUP2 C
                            ON C.`Date`=A.`Date`
                            RIGHT JOIN GROUP3 D
                            ON D.`Date`=A.`Date`
                            WHERE A.`Date` IS NOT NULL');
                    
	select @view_stmt2;
	PREPARE statement from @view_stmt2;
	Execute statement;
	Deallocate PREPARE statement;*/
    
    set @view_stmt = concat('CREATE OR REPLACE VIEW V_UI_Calendar AS
        select `SMS`,`PN`,`WHATSAPP`,`EMAIL`, `CLM`,`GTM`, A.`Date`,`Coverage`, `Rev Lift`,A.EE_Date
FROM V_UI_Calendar1 A, V_UI_Calendar2 B
WHERE A.`Date` = B.`Date`
AND A.EE_Date = "',IN_FILTER_MONTHDURATION,'"');
                    
	SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("SMS",`SMS`,"PN",`PN`,"WHATSAPP",`WHATSAPP`,"EMAIL",`EMAIL`,"CLM",`CLM`,"GTM",`GTM`,"Date",`Date`,"Coverage",`Coverage`,"Incr_Rev",`Rev Lift`) )),"]")into @temp_result from V_UI_Calendar ',';') ;  
    
	
   
    
    
    
            
    END IF;
    
    
    IF IN_MEASURE = 'DAY_VIEW' AND IN_AGGREGATION_2 = 'TABLE' AND IN_AGGREGATION_3 IS NULL
   THEN
    
    set @view_stmt= concat('CREATE OR REPLACE VIEW V_UI_Calendar
					AS(
					SELECT
                    `Category` as "Type",
                    `Trigger` AS "Name",
                    CAST(format(ifnull(SUM(`Target_Base`),0),",") AS CHAR) as `Coverage`,
                    Target_Delivered AS Delivered,
                    IFNULL(Target_CTA,0) AS Clicked,
                    Target_Bills AS Conversion,
                    CAST(format(ifnull(round(SUM(Incremental_Revenue),0),0),",") AS CHAR) AS `Revenue`
					from V_Dashboard_Calendar_Temp
                    where EE_Date = "',IN_FILTER_MONTHDURATION,'"
                    group by Event_id)
                   
                    ');
             
                      
SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Type",`Type`,"Name",ifnull(`Name`,0),"Outreaches",`Coverage`,"Delivered",format(ifnull(`Delivered`,0),","),"Clicked",format(`Clicked`,","),"Conversion",CAST(format(ifnull(`Conversion`,0),",") AS CHAR),"Revenue",`Revenue`) )),"]")into @temp_result from V_UI_Calendar ',';') ;  
    
	END IF;
    
    IF IN_MEASURE = 'DAY_VIEW' AND IN_AGGREGATION_2 = 'TABLE' AND IN_AGGREGATION_3 = 'TOTAL'
   THEN
   set @view_stmt= concat('CREATE OR REPLACE VIEW V_UI_Calendar
					AS
					 
                    (SELECT 
					"TOTAL" as "Type",
                    COUNT(DISTINCT `Trigger`) AS "Name",
                    CAST(format(ifnull(SUM(`Target_Base`),0),",") AS CHAR) as `Coverage`,
                    SUM(Target_Delivered) AS Delivered,
                    IFNULL(SUM(Target_CTA),0) AS Clicked,
                    SUM(Target_Bills) AS Conversion,
                    CAST(format(ifnull(round(SUM(Incremental_Revenue),0),0),",") AS CHAR) AS `Revenue`
					from V_Dashboard_Calendar_Temp
                    where EE_Date = "',IN_FILTER_MONTHDURATION,'")');
                                     
	
    
    
   
                      
					SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Type",`Type`,"Name",ifnull(`Name`,0),"Outreaches",`Coverage`,"Delivered",format(ifnull(`Delivered`,0),","),"Clicked",format(`Clicked`,","),"Conversion",CAST(format(ifnull(`Conversion`,0),",") AS CHAR),"Revenue",`Revenue`) )),"]")into @temp_result from V_UI_Calendar ',';') ;    
    
	END IF;
    
    IF IN_MEASURE = 'TODAY_VIEW_DATES' 
   THEN
   set @view_stmt= concat('CREATE OR REPLACE VIEW V_UI_Calendar
					AS
					SELECT CURDATE() - INTERVAL 0 DAY AS Today,
                    DAYNAME(CURDATE() - INTERVAL 0 DAY) AS WKDAY;
                    ');
                     
SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Today",`Today`,"WEEKDAY",`WKDAY`) )),"]")into @temp_result from V_UI_Calendar ',';') ;
	END IF;
 IF IN_SCREEN <> "DOWNLOAD"
    THEN
    select @view_stmt;
	PREPARE statement from @view_stmt;
	Execute statement;
	Deallocate PREPARE statement;
	select @Query_String;
	PREPARE statement from @Query_String;
	Execute statement;
	Deallocate PREPARE statement; 
    if @temp_result is null
    then 
		SET out_result_json = "NULL RESPONSE";
	else
    SET out_result_json = @temp_result;
    end if;
    ELSE
		call S_dashboard_performance_download('V_UI_Calendar',@out_result_json1);
		select @out_result_json1 into out_result_json;
    END IF;
    
END$$
DELIMITER ;
