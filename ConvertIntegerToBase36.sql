
CREATE or replace FUNCTION `ConvertIntegerToBase36`(vInputValue BIGINT
) RETURNS varchar(20) CHARSET latin1
BEGIN
    
    DECLARE vResult VARCHAR(20);
	DECLARE vVal BIGINT;
	DECLARE vShortCharacterSet VARCHAR(36);
    DECLARE vLenShortCharacterSet TINYINT;
	
    SET @vResult = '';
	SET @vShortCharacterSet = '0123456789abcdefghijklmnopqrstuvwxyz';
	SET @vLenShortCharacterSet = LENGTH(@vShortCharacterSet);
    SET @vInputValue=vInputValue;
	IF @vInputValue > 0 THEN
		SET @vVal = @vInputValue;
		WHILE @vVal >= 1 DO
			
			SET @vResult = CONCAT(@vResult,SUBSTRING(@vShortCharacterSet, FLOOR(@vVal % @vLenShortCharacterSet + 1), 1));
			SET @vVal = @vVal / @vLenShortCharacterSet;
            
		END WHILE;
	ELSE 
		SET @vResult = '';
	END IF;
    set vResult=@vResult;
    RETURN vResult;
END


