DELIMITER $$
CREATE or replace PROCEDURE `S_dashboard_performance_download`(IN view_name VARCHAR(100), OUT out_result_json json)
BEGIN
    declare IN_VIEW_NAME VARCHAR(100); 	
   
	SET IN_VIEW_NAME = view_name;
  	
  
               Select concat('[{',GROUP_CONCAT('\"', column_name, '\":\"',
            CASE 
               when data_type='bigint' or data_type='int' then 'String'
             ELSE
                'String'            
            END
            ,'\"'),'}]') into @temp_result1
            FROM   `information_schema`.`columns` 
		    WHERE  `table_schema`=DATABASE() 
		    AND `table_name`=  IN_VIEW_NAME;   
            select @temp_result1;
            
            Select concat('{',GROUP_CONCAT('\"', column_name, '\":\"',column_name ,'\"'),'}') into @temp_result2
            FROM   `information_schema`.`columns` 
		    WHERE  `table_schema`=DATABASE() 
		    AND `table_name`=  IN_VIEW_NAME; 
			select @temp_result2;
			select group_concat(column_name) into @distinct_column  FROM   `information_schema`.`columns` WHERE  `table_schema`=DATABASE() 
		    AND `table_name`=IN_VIEW_NAME ;
            select @distinct_column;
			select (length(@distinct_column)-length(replace(@distinct_column,',','')))+1 into @num_of_columns;
		    set @loop_var=1;
		    set @json_select='';
            set @json_select1='';
		    select @loop_var,@num_of_columns,@distinct_column;
		    WHILE @loop_var<=@num_of_columns
		    do
			SET @selected_columns=substring_index(substring_index(concat(@distinct_column,',-1'),',',@loop_var),',',-1);
            if (@loop_var=1) then 
            
			SET  @json_select=concat('"',@selected_columns,'",`',@selected_columns,'`');
            else 
			 SET @json_select=concat(@json_select,',"',@selected_columns,'",`',@selected_columns,'`');
			end if;
			set @loop_var=@loop_var+1;
		    end while;    
               select @json_select;
			  set session group_concat_max_len=5000000;
              SET @Query_String3 =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object(',@json_select,') )),"]")into @temp_result3 from ',IN_VIEW_NAME,';') ;
            
             
            select @Query_String3;
		      PREPARE statement3 from @Query_String3;
              Execute statement3;
             select @temp_result3 into out_result_json;
              
              
		END$$
DELIMITER ;
