
CREATE OR REPLACE PROCEDURE `PROC_CLM_CU_CLM_Channel`(
	IN vUserId BIGINT, 
	IN vChannelName VARCHAR(128), 
	IN vChannelDesc VARCHAR(1024), 
	IN vFile_Delimiter VARCHAR(16),
    IN vFile_Extension VARCHAR(16),
    IN vMarker_File_Required VARCHAR(16),
    IN vMarker_File_Extension VARCHAR(16),
	INOUT vChannelId BIGINT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN

	DECLARE vRowCnt BIGINT DEFAULT 0;
	DECLARE vSqlstmt TEXT;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN 
		SET vSuccess = 0; 
		GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
		SET vErrMsg = CONCAT(IFNULL(vErrMsg,''), "ERROR ", @errno, " (", @sqlstate, "): ", @text);
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 0, vErrMsg, 0);
	END;
	SET vErrMsg = 'Start';
	SET vSuccess = 0;
	SET @CURR_PROC_NAME = 'PROC_CLM_CU_CLM_Channel';
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 1, vErrMsg, vSuccess);
	IF vChannelName IS NULL OR vChannelName = '' THEN
		SET vErrMsg = 'Channel Name is a Mandatory Field and cannot be empty or NULL';
		SET vSuccess = 0;
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 2, vErrMsg, vSuccess);
	ELSE
		IF vChannelId IS NULL OR vChannelId <= 0 THEN
			SET vSqlstmt = CONCAT(
				'INSERT IGNORE INTO CLM_Channel (
					ChannelName, 
					ChannelDesc, 
                         File_Delimiter ,
     File_Extension ,
     Marker_File_Required, 
     Marker_File_Extension,
					CreatedBy, 
					ModifiedBy
				) VALUES ('
				,'"', vChannelName,'"'
				,',"', vChannelDesc,'"'
                ,',"', vFile_Delimiter,'"'
                ,',"', vFile_Extension,'"'
                ,',"', vMarker_File_Required,'"'
                ,',"', vMarker_File_Extension,'"'
				,',', vUserId
				,',', vUserId
				, ')'
			);

			SET vErrMsg = vSqlstmt;
			SET @sql_stmt = vSqlstmt;
			PREPARE EXEC_STMT FROM @sql_stmt;
			EXECUTE EXEC_STMT;
			DEALLOCATE PREPARE EXEC_STMT;
			CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 3, vErrMsg, vSuccess);
		ELSE
				
			SELECT EXISTS(
				SELECT 1 
				FROM CLM_Campaign_Events 
				WHERE 
					ChannelId = vChannelId
					AND Has_Records_In_Execution_Hist = 1
			) INTO vRowCnt;
			
			IF 1=1 THEN
				SET vSqlstmt = CONCAT(
					'INSERT IGNORE INTO CLM_Channel (
						ChannelId,
						ChannelName, 
						ChannelDesc,
						File_Delimiter,
						File_Extension,
                        Marker_File_Required, 
                        Marker_File_Extension,
						CreatedBy, 
						ModifiedBy
					) VALUES ('
					, vChannelId
					,',"', vChannelName,'"'
					,',"', vChannelDesc,'"'
                    ,',"', vFile_Delimiter,'"'
                    ,',"', vFile_Extension,'"'
                    ,',"', vMarker_File_Required,'"'
                    ,',"', vMarker_File_Extension,'"'
					,',', vUserId
					,',', vUserId
					,') ON DUPLICATE KEY UPDATE '
					,' ChannelDesc = "', vChannelDesc, '"'
                    ,' ,File_Delimiter = "', vFile_Delimiter, '"'
                    ,' ,File_Extension = "', vFile_Extension, '"'
                    ,' ,Marker_File_Extension = "', vMarker_File_Extension, '"'
					,', CreatedBy = ', vUserId
					,', ModifiedBy = ', vUserId
				);

				SET vErrMsg = vSqlstmt;
				SET @sql_stmt = vSqlstmt;
				PREPARE EXEC_STMT FROM @sql_stmt;
				EXECUTE EXEC_STMT;
				DEALLOCATE PREPARE EXEC_STMT;
				CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 4, vErrMsg, vSuccess);
			ELSE
				SET vErrMsg = 'The Channel already exists and has been used in Campaign execution and hence cannot be modified.';
				SET vSuccess = 0;
				CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 5, vErrMsg, vSuccess);
			END IF;
		END IF;
	END IF;
	
	SELECT ChannelId INTO vChannelId 
	FROM CLM_Channel
	WHERE ChannelName = vChannelName
	LIMIT 1;
 
	SET vSuccess = 1;
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 6, vErrMsg, vSuccess);

END

