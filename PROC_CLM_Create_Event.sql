DELIMITER $$
CREATE or replace PROCEDURE `PROC_CLM_Create_Event`(

	IN vUserId BIGINT,    
	IN vCampaignThemeId BIGINT,  
	IN vCLMSegmentId  VARCHAR(128),-- Changed  
	IN vCampaignId BIGINT,  
	IN vMicroSegmentId BIGINT,  
	IN vChannelId BIGINT,  
	IN vOfferTypeId BIGINT,   

	IN vOfferName VARCHAR(128), 
	IN vOfferDesc VARCHAR(1024), 
	IN vOfferDef VARCHAR(2048), 
	IN vOfferStartDate DATE, 
	IN vOfferEndDate DATE, 
	IN vOfferRedemptionDays INTEGER, 
	IN vOfferIsAtCustomerLevel TINYINT, 

	IN vCampTriggerName VARCHAR(128),  
	IN vCampTriggerDesc VARCHAR(1024), 
	IN vPrecedingTriggerName VARCHAR(128), 
	IN vPrecedingTriggerId BIGINT, 
	IN vDaysOffsetFromPrecedingTrigger SMALLINT, 
	IN vCTState TINYINT, 
	IN vCTIsMandatory TINYINT, 
	IN vIsAReminder TINYINT, 
	IN vCTReplacedQueryAsInUI TEXT, 
	IN vCTReplacedQuery TEXT, 
	IN vCTIsValidSVOCCondition TINYINT, 
	IN vCTReplacedQueryBillAsInUI TEXT, 
	IN vCTReplacedQueryBill TEXT, 
	IN vCTIsValidBillCondition TINYINT, 
	IN vCTUsesGoodTimeModel	TINYINT, 

	IN vCreativeName VARCHAR(128), 
	IN vExtCreativeKey VARCHAR(128),
    IN vSenderKey VARCHAR(255),
    IN vTimeslot_Id BIGINT(20),
	IN vCreativeDesc VARCHAR(1024), 
	IN vCreative TEXT, 
	IN vHeader TEXT, 
    IN vMicrosite_Creative TEXT,
    IN vMicrosite_Header TEXT,

	IN vExclude_Recommended_SameAP_NotLT_FavCat TINYINT, 
	IN vExclude_Recommended_SameLTD_FavCat TINYINT, 
	IN vExclude_Recommended_SameLT_FavCat TINYINT, 
	IN vRecommendation_Column VARCHAR(2048),
	IN vRecommendation_Type VARCHAR(2048), 
	IN vExclude_Stock_out TINYINT, 
	IN vExclude_Transaction_X_Days TINYINT, 
	IN vExclude_Recommended_X_Days TINYINT, 
	IN vExclude_Never_Brought TINYINT, 
	IN vExclude_No_Image TINYINT, 
	IN vNumber_Of_Recommendation INTEGER, 
	IN vRecommendation_Filter_Logic VARCHAR(2048), 
	IN vRecommendation_Column_Header VARCHAR(2048), 
	IN vIs_Recommended_SameLT_FavCat TINYINT, 
	IN vIs_Recommended_SameLTD_Cat TINYINT, 
	IN vIs_Recommended_SameLTD_Dep TINYINT, 
	IN vIs_Recommended_NewLaunch_Product TINYINT, 
	IN vEvent_Limit BIGINT,
    IN vCommunication_Cooloff BIGINT,
    
    IN vCustom_Attr1 VARCHAR(255),
    IN vCustom_Attr2 VARCHAR(255),
    IN vCustom_Attr3 VARCHAR(255),
    IN vCustom_Attr4 VARCHAR(255),
    IN vCustom_Attr5 VARCHAR(255),

	INOUT vCampTriggerId BIGINT,
    INOUT vOfferId BIGINT,
	INOUT vCreativeId BIGINT,
	INOUT vEventId BIGINT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN
	DECLARE vCondition TEXT DEFAULT '';
	DECLARE vEventName VARCHAR(1024);
	DECLARE vExtEventName VARCHAR(1024);
	DECLARE vCTExecutionSequence BIGINT DEFAULT 1;
    DECLARE vTemplatePersonalizationScore SMALLINT DEFAULT 1;
    DECLARE vExecution_Sequence BIGINT DEFAULT 1;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN 
		SET vSuccess = 0; 
		GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
		SET vErrMsg = CONCAT(IFNULL(vErrMsg,''), "ERROR ", @errno, " (", @sqlstate, "): ", @text);
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 0, vErrMsg, 0);
	END;

	SET vErrMsg = 'Start';
	SET vSuccess = 0;
	SET @CURR_PROC_NAME = 'PROC_CLM_Create_Event';
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 1, vErrMsg, vSuccess);

	IF vUserId > 0 THEN
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 2, vErrMsg, vSuccess);

		IF (vCampaignThemeId > 0 AND vCLMSegmentId > 0  AND vCampaignId > 0) THEN
        	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 3, vErrMsg, vSuccess);
			SET vCTIsValidSVOCCondition = 0;
			IF vCTReplacedQuery <> '' THEN
				SET vErrMsg  = CONCAT('Invalid CLM Campaign Trigger SVOC condition: - ',vCTReplacedQuery);
				CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 4, vErrMsg, vSuccess);
				CALL PROC_CLM_IS_VALID_SVOC_CONDITION(vCTReplacedQuery,vCTReplacedQueryBill,vCLMSegmentId,1,@Final_Condition_Applied,vCTIsValidSVOCCondition);
			ELSE
				SET vCTIsValidSVOCCondition = 1;
				CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 5, vErrMsg, vSuccess);
			END IF;
			IF vCTIsValidSVOCCondition = 1 THEN
            	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 6, vErrMsg, vSuccess);
				SET vCTIsValidBillCondition = 0;
				IF vCTReplacedQueryBill <> '' THEN
					SET vErrMsg  = CONCAT('Invalid CLM Campaign Trigger Bill condition: - ',vCTReplacedQueryBill);
					CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 7, vErrMsg, vSuccess);
					CALL PROC_CLM_IS_VALID_SVOBILL_CONDITION(vCTReplacedQueryBill,vCTReplacedQuery,vCLMSegmentId,1,vCTIsValidBillCondition);
                    SET vCTIsValidBillCondition = 1;
				ELSE
					SET vCTIsValidBillCondition = 1;
					CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 8, vErrMsg, vSuccess);
				END IF;
				IF vCTIsValidBillCondition = 1 THEN
					SET vErrMsg  = 'IN PROC_CLM_CU_CLM_Campaign_Trigger';
					CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 9, vErrMsg, vSuccess);
					CALL PROC_CLM_CU_CLM_Campaign_Trigger(
						vUserId, 
						vCampTriggerName, 
						vCampTriggerDesc,
						vPrecedingTriggerId,
						vDaysOffsetFromPrecedingTrigger,
						vCTState,
						vCTUsesGoodTimeModel,
						vCTIsMandatory,
						vIsAReminder,
						vCampaignThemeId,
						vCampaignId,
						vCTReplacedQueryAsInUI,
						vCTReplacedQuery,
						vCTIsValidSVOCCondition,
						vCTReplacedQueryBillAsInUI,
						vCTReplacedQueryBill,
						vCTIsValidBillCondition,
						vCTExecutionSequence,
						vCampTriggerId,
						vEvent_Limit,
                        vErrMsg,
                        vSuccess
					);
					
					IF vCampTriggerId > 0 THEN
						CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 10, vErrMsg, vSuccess);
						IF vOfferTypeId > 0 THEN
							SET vErrMsg  = 'IN PROC_CLM_CU_CLM_Offer';
							CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 11, vErrMsg, vSuccess);
							IF (vOfferName <> '' AND vOfferName IS NOT NULL )THEN
								CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 12, vErrMsg, vSuccess);
								IF vOfferName = 'CUSTOMER_LEVEL_OFFER' THEN
									SET vOfferIsAtCustomerLevel = 1;
								ELSE
									SET vOfferIsAtCustomerLevel = 0;
								END IF;
								CALL PROC_CLM_CU_CLM_Offer(
										vUserId, 
										vOfferName, 
										vOfferDesc, 
										vOfferTypeId, 
										vOfferDef, 
										vOfferStartDate, 
										vOfferEndDate, 
										vOfferRedemptionDays, 
										vOfferIsAtCustomerLevel, 
										vOfferId,
										vErrMsg,
                                        vSuccess
									);
							END IF;
						END IF;
						CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 13, vErrMsg, vSuccess);
						IF vSuccess = 1 THEN
							CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 14, vErrMsg, vSuccess);
							IF vChannelId > 0 THEN
								SET vErrMsg  = 'IN PROC_CLM_CU_CLM_Creative';
								CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 15, vErrMsg, vSuccess);
								CALL PROC_CLM_CU_CLM_Creative(
										vUserId, 
										vCreativeName, 
										vCreativeDesc, 
										vExtCreativeKey,
                                        vSenderKey,
                                        vTimeslot_Id,
                                        vChannelId, 
										vCreative, 
										vHeader,
                                        vMicrosite_Creative,
                                        vMicrosite_Header,
                                        vTemplatePersonalizationScore, 
										vOfferId,
                                        vCommunication_Cooloff,
                                        vCustom_Attr1,
                                        vCustom_Attr2,
                                        vCustom_Attr3,
                                        vCustom_Attr4,
                                        vCustom_Attr5,
                                        vRecommendation_Type,
										vCreativeId,
										vErrMsg,
                                        vSuccess
									);
								IF vCreativeId > 0 THEN
									SET vErrMsg  = 'IN PROC_CLM_CU_CLM_Reco_In_Creative';
									CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 16, vErrMsg, vSuccess);
									CALL PROC_CLM_CU_CLM_Reco_In_Creative(
											vUserId, 
											vCreativeId, 
											vExclude_Recommended_SameAP_NotLT_FavCat, 
											vExclude_Recommended_SameLTD_FavCat, 
											vExclude_Recommended_SameLT_FavCat, 
											vRecommendation_Column, 
											vRecommendation_Type, 
											vExclude_Stock_out, 
											vExclude_Transaction_X_Days, 
											vExclude_Recommended_X_Days, 
											vExclude_Never_Brought, 
											vExclude_No_Image, 
											vNumber_Of_Recommendation, 
											vRecommendation_Filter_Logic, 
											vRecommendation_Column_Header, 
											vIs_Recommended_SameLT_FavCat, 
											vIs_Recommended_SameLTD_Cat, 
											vIs_Recommended_SameLTD_Dep, 
											vIs_Recommended_NewLaunch_Product,
											vErrMsg,
                                            vSuccess
										); 

									IF vSuccess = 1 THEN
										CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 17, vErrMsg, vSuccess);
										IF (vCampaignThemeId > 0 AND vCampaignId > 0 AND vCLMSegmentId > 0 AND vCampTriggerId > 0 AND vChannelId > 0 AND vMicroSegmentId > 0 AND vCreativeId > 0) THEN
											SET vErrMsg  = 'IN PROC_CLM_CU_CLM_Campaign_Events';
                                            CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 18, vErrMsg, vSuccess);
											
											SELECT CONCAT(
														(SELECT CampaignName FROM CLM_Campaign WHERE CampaignId = vCampaignId)
														, '_'
														, (SELECT CampTriggerName FROM CLM_Campaign_Trigger WHERE CampTriggerId = vCampTriggerId)
														, '_'
														, (SELECT MicroSegmentName FROM CLM_MicroSegment WHERE MicroSegmentId = vMicroSegmentId)
														,'_'
														, (SELECT ChannelName FROM CLM_Channel WHERE ChannelId = vChannelId)
														,'_'
														, (SELECT CreativeName FROM CLM_Creative WHERE CreativeId = vCreativeId)
													) INTO vEventName;
											SET vExtEventName = vEventName;
											Select vUserId, 
													vEventName,
													vExtEventName,
													vCampaignThemeId, 
													vCampaignId, 
													vCLMSegmentId, 
													vCampTriggerId, 
													vChannelId, 
													vMicroSegmentId, 
													vCreativeId, 
													vExecution_Sequence, 
													vEventId,
													vErrMsg,
                                                    vSuccess;
											CALL PROC_CLM_CU_CLM_Campaign_Events(
													vUserId, 
													vEventName,
													vExtEventName,
													vCampaignThemeId, 
													vCampaignId, 
													vCLMSegmentId, 
													vCampTriggerId, 
													vChannelId, 
													vMicroSegmentId, 
													vCreativeId, 
													vExecution_Sequence, 
													vEventId,
													vErrMsg,
                                                    vSuccess
												); 
										ELSE
											SET vErrMsg  = 'MicroSegmentId value cannot be less than or equal to 0';
											SET vSuccess = 0;
                                            CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 19, vErrMsg, vSuccess);
										END IF;
									END IF;
								END IF;
							END IF;
						END IF;
					END IF;
				ELSE
					SET vErrMsg  = CONCAT('Invalid CLM Campaign Trigger Bill condition: - ',vCTReplacedQueryBill);
					SET vSuccess = 0;	
					CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 20, vErrMsg, vSuccess);
				END IF;
			ELSE
				SET vErrMsg  = CONCAT('Invalid CLM Campaign Trigger SVOC condition: - ',vCTReplacedQuery);
				SET vSuccess = 0;	
				CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 21, vErrMsg, vSuccess);
			END IF;
		END IF;
	ELSE
		SET vErrMsg  = 'INVALID USER ID PASSED';	
		SET vSuccess = 0;	
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 22, vErrMsg, vSuccess);
	END IF;
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 23, vErrMsg, vSuccess);
END$$
DELIMITER ;
