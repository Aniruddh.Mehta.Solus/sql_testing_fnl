
CREATE OR REPLACE PROCEDURE `CDM_ETL_CLM_Vouchers`()
BEGIN 
  CALL CDM_Update_Process_Log('CDM_ETL_CLM_Vouchers', 1, 'CDM_ETL_CLM_Vouchers', 1); 
	SET SQL_SAFE_UPDATES=0;
    
    UPDATE CDM_Stg_CLM_Vouchers a, CDM_Customer_Key_Lookup b
	SET a.Customer_Id = b.Customer_Id
    where a.Customer_Key=b.Customer_Key;
    
	INSERT IGNORE INTO `CDM_CLM_Vouchers`
		(
		`Customer_Id`,
		`Offer_Key`,
		`Voucher_Code`,
		`Voucher_Start_Date`,
		`Voucher_Expiry_Date`,
		`Custom_Attr1`,
		`Custom_Attr2`,
		`Custom_Attr3`,
		`Custom_Attr4`,
		`Custom_Attr5`)
		(SELECT
		`Customer_Id`,
		`Offer_Key`,
		`Voucher_Code`,
		`Voucher_Start_Date`,
		`Voucher_Expiry_Date`,
		`Custom_Attr1`,
		`Custom_Attr2`,
		`Custom_Attr3`,
		`Custom_Attr4`,
		`Custom_Attr5`
        from CDM_Stg_CLM_Vouchers where Customer_Id is not NULL);

  
  CALL CDM_Update_Process_Log('CDM_ETL_CLM_Vouchers', 4, 'CDM_ETL_CLM_Vouchers', 1); 
END

