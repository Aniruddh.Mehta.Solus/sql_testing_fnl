DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_QSR_Repeat_metrics`()
BEGIN

DELETE from log_solus where module = 'S_dashboard_QSR_Repeat_metrics';
SET SQL_SAFE_UPDATES=0;

SELECT `Value1` into @TYSM from UI_Configuration_Global where `Config_Name` = 'FISCAL';
SELECT @TYSM;

SELECT `Value2` into @TYEM from UI_Configuration_Global where `Config_Name` = 'FISCAL';
SELECT @TYEM;

SET @LYSM = F_Month_Diff(@TYSM,12);
SET @LYEM = F_Month_Diff(@TYEM,12);

SELECT @LYSM,@LYEM;

SET @LLYSM = F_Month_Diff(@LYSM,12);
SET @LLYEM = F_Month_Diff(@LYEM,12);

SELECT @LLYSM,@LLYEM;

SET @Start_Date = date_format(date_sub(concat(date_format(current_date(),"%Y%m"),'01'), INTERVAL 14 MONTH),"%Y%m");
SET @End_Date = date_format(current_date(),"%Y%m");

SET @TM = (SELECT date_format(current_date(),"%Y%m"));
SELECT @TM;
	

DROP TABLE IF EXISTS Dashboard_QSR_Repeat ;
  CREATE TABLE IF NOT EXISTS Dashboard_QSR_Repeat ( 
  Platform varchar(50) ,
  Bill_Year_Month bigint(20),
  New_count int(20),
  Existing_count int(20),
  new_per decimal(20,5),
  existing_per decimal(20,5)
  ) ENGINE = INNODB DEFAULT CHARACTER SET=LATIN1; 
	
    ALTER TABLE Dashboard_QSR_Repeat    
		ADD INDEX (Platform);
    
	set @a =1;
    
FOR_LAST_12_MONTHS: LOOP
		set @a=@a+ 1;
			IF @a = 12 THEN
				LEAVE FOR_LAST_12_MONTHS;
			END IF;
		SELECT @TM, CURRENT_TIMESTAMP();
		
        INSERT IGNORE INTO Dashboard_QSR_Repeat(Platform, Bill_Year_Month, New_Count)
		select
		Platform, Bill_Year_Month, count(distinct(Customer_ID)) 
		from Dashboard_Insights_QSR_Customers_Temp
		WHERE Is_New_Customer = 1
		group by 1,2;

		update Dashboard_QSR_Repeat U,
        (select
		Platform, Bill_Year_Month, count(distinct(Customer_ID)) as cnt 
		from Dashboard_Insights_QSR_Customers_Temp 
		WHERE Bill_Year_Month = @TM 
		and Is_Existing_Customer = 1
		group by Platform) S
        set U.Existing_count = S.Cnt
        where U.Bill_Year_Month = S.Bill_Year_Month 
		and U.Platform = S.Platform;

		SET @TM = F_Month_Diff(@TM,1);
		
end LOOP FOR_LAST_12_MONTHS;


UPDATE Dashboard_QSR_Repeat
SET new_per= (New_Count / (New_Count + Existing_count)) * 100;

UPDATE Dashboard_QSR_Repeat
SET existing_per= (Existing_count / (New_Count + Existing_count)) * 100;


DROP TABLE IF EXISTS Dashboard_QSR_Base ;
  CREATE TABLE IF NOT EXISTS Dashboard_QSR_Base ( 
  Platform varchar(50) ,
  Base_count int(20),
  Repeat_count int(20),
  Repeat_per decimal(20,5)
  ) ENGINE = INNODB DEFAULT CHARACTER SET=LATIN1; 
	
    ALTER TABLE Dashboard_QSR_Base    
		ADD INDEX (Platform);
	
		
		
        INSERT IGNORE INTO Dashboard_QSR_Base(Platform, Base_count)
		select Platform, count(distinct(Customer_Id)) as Base
		from Dashboard_Insights_QSR_Customers_Temp
        group by Platform;

		update Dashboard_QSR_Base U,
        (select 
			Platform, count(distinct(Customer_ID)) as cnt 
			from Dashboard_Insights_QSR_Customers_Temp
			WHERE Is_Existing_Customer = 1
			group by Platform) S
        set U.Repeat_count = S.Cnt
        where  U.Platform = S.Platform;

		

UPDATE Dashboard_QSR_Base
SET Repeat_per= (Repeat_count / (Base_count + Repeat_count)) * 100;


END$$
DELIMITER ;
