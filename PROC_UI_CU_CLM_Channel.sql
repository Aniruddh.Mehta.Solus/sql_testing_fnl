
CREATE OR REPLACE PROCEDURE `PROC_UI_CU_CLM_Channel`(
	IN vUserId BIGINT,
	IN vChannelName VARCHAR(128),
	IN vChannelDesc VARCHAR(1024),
    IN vFile_Delimiter VARCHAR(16),
    IN vFile_Extension VARCHAR(16),
    IN vMarker_File_Required VARCHAR(16),
    IN vMarker_File_Extension VARCHAR(16),
	INOUT vChannelId BIGINT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN
	DECLARE vResult TINYINT;
	DECLARE vInputValues TEXT;
	   select 'works';
	SET vSuccess = 1;
	SET vErrMsg = '';

	SET vInputValues  = CONCAT(
			'UserId: ', IFNULL(vUserId,'NULL VALUE')
			,' | ChannelName: ',IFNULL(vChannelName,'NULL VALUE')
			,' | ChannelDesc: ',IFNULL(vChannelDesc,'NULL VALUE')
		);

	IF vUserId IS NULL OR vUserId <= 0 THEN
		SET vErrMsg = CONCAT('User Id cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vChannelName IS NULL OR vChannelName = '' THEN
		SET vErrMsg = CONCAT('Channel Name cannot be empty.');
		SET vSuccess = 0;
	END IF;
	IF vSuccess = 1 THEN 
	CALL PROC_CLM_CU_CLM_Channel(
			vUserId,
			vChannelName,
		IFNULL(vChannelDesc, ''),
			vFile_Delimiter ,
     vFile_Extension ,
     vMarker_File_Required ,
     vMarker_File_Extension,
		vChannelId,
		vErrMsg,
		vSuccess
	); 
	END IF;
	IF vSuccess = 0 THEN 
		SET vErrMsg = CONCAT(vErrMsg , '\r\n', vInputValues);	
	END IF;

END

