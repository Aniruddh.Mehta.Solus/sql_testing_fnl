DELIMITER $$
CREATE OR REPLACE FUNCTION `F_Month_Diff`(curYYYYMM int(6),vNoOfMonthInput int(2)) RETURNS char(6) CHARSET latin1
BEGIN
	DECLARE prevYYYYMM char(6);
    DECLARE curYYYY,prevYYYY,vNoOfYear int(4);
	DECLARE curMM,prevMM,vNoOfMonth int(2);
    
    set curYYYY=substr(curYYYYMM,1,4);
	set curMM=substr(curYYYYMM,5,2);
    
    set vNoOfMonth = vNoOfMonthInput%12;
    set vNoOfYear = floor(vNoOfMonthInput/12);

    IF  curMM <= vNoOfMonth THEN
		SET prevMM 	= 12 + curMM - vNoOfMonth ;
		SET prevYYYY	= curYYYY - 1 - vNoOfYear; 
	ELSE
		SET prevMM 	= curMM - vNoOfMonth;
        SET prevYYYY	= curYYYY - vNoOfYear; 
	END IF;
    
    IF prevMM < 10 THEN
		SET prevYYYYMM=CONCAT(prevYYYY,'0',prevMM);
    ELSE
		SET prevYYYYMM=CONCAT(prevYYYY,prevMM);
    END IF;
    
    return prevYYYYMM;
END$$
DELIMITER ;
