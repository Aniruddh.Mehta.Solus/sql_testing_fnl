
CREATE or replace PROCEDURE `S_CLM_Migrate_Old_Header`()
BEGIN
SET SQL_SAFE_UPDATES = 0;
update CLM_Creative
SET Header =
	replace(replace(replace(replace(replace(JSON_EXTRACT(Header,'$.SMS'),'[',''),']',''),'"',''),',','|'),' ','')  
    where ChannelId=1;
    update CLM_Creative
SET Header =
	replace(replace(replace(replace(replace(JSON_EXTRACT(Header,'$.Email'),'[',''),']',''),'"',''),',','|'),' ','') 
    where ChannelId=2;
    update CLM_Creative
SET Header =
	replace(replace(replace(replace(replace(JSON_EXTRACT(Header,'$.PN'),'[',''),']',''),'"',''),',','|'),' ','') 
    where ChannelId=3;
select * from CLM_Creative;
END

