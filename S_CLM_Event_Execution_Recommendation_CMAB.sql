
CREATE or replace PROCEDURE `S_CLM_Event_Execution_Recommendation_CMAB`(IN OnDemandEventId BIGINT(20),IN RUN_MODE Varchar(10))
begin

DECLARE vEvent_Limit_1 varchar(16);
DECLARE vRecommendation_Filter_Logic varchar(2048);
Declare Event_Condition_SVOC_1, Event_Condition_Bill_1,Communication_CoolOff_1 text;
DECLARE vCommunication_CoolOff int;
DECLARE V_Offer_Code_1 varchar(20);
DECLARE  Exe_ID, Event_ID_cur1_1, Communication_Template_ID_cur1_1, Reminder_Parent_TriggerId_cur1_1, Reminder_Days_cur1_1,vGoodTime_1,vEvent_CoolOff_Days_1 int;
DECLARE vCommunication_Channel_1 VARCHAR(256);
DECLARE done1,done2 INT DEFAULT FALSE;
DECLARE cur1 cursor for 
	SELECT 	em.ID,
			em.Replaced_Query, 
            em.Replaced_Query_Bill,
            em.Reminder_Parent_Event_Id, 
			em.Reminder_Days , 
            em.Communication_Template_ID,
            ct.Offer_Code,Event_Limit,
            ct.Communication_Cooloff,
            em.Is_Good_Time,
            ct.Communication_Channel,
            ct.Recommendation_Filter_Logic
           
	FROM Event_Master em

    JOIN Communication_Template ct on ct.ID=em.Communication_Template_ID
    WHERE  
     em.ID in (select distinct Event_ID from Event_Execution_Stg_CMAB)		
ORDER BY em.Execution_Sequence;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done1 = TRUE;

SET done1 = FALSE;
SET SQL_SAFE_UPDATES=0;
SET foreign_key_checks=0;
set @vStart_Cnt = 0;set @vStart_CntCust=0; set @vEnd_Cnt=0; set @vEnd_CntCust=0; set @vBatchSizeBills=100000; 
set @vBatchSizeCust=100000;

SELECT Customer_id INTO @Rec_Cnt FROM V_CLM_Customer_One_View ORDER BY Customer_id DESC LIMIT 1;
SET @Rec_Cnt=ifnull(@Rec_Cnt,0);
truncate Event_Execution_Stg;



SET done1 = FALSE;
OPEN cur1;
LOOP_ALL_EVENTS: LOOP

	FETCH cur1 into 
	  Event_ID_cur1_1, 
	  Event_Condition_SVOC_1,  
	  Event_Condition_Bill_1, 
	  Reminder_Parent_TriggerId_cur1_1,
	  Reminder_Days_cur1_1,
	  Communication_Template_ID_cur1_1, 
	  V_Offer_Code_1,
	  vEvent_Limit_1,
      vCommunication_CoolOff,
	  vGoodTime_1,
      vCommunication_Channel_1,
	  vRecommendation_Filter_Logic;
	 
	 SET @Event_ID_cur1=Event_ID_cur1_1;
	 SET @Event_Condition_SVOC=Event_Condition_SVOC_1;
	 SET @Event_Condition_Bill=Event_Condition_Bill_1;
	 SET @Reminder_Parent_TriggerId_cur1=Reminder_Parent_TriggerId_cur1_1;
	 SET @Reminder_Days_cur1=Reminder_Days_cur1_1;
	 SET @Communication_Template_ID_cur1=Communication_Template_ID_cur1_1;
	 SET @V_Offer_Code=V_Offer_Code_1;
	 SET @vEvent_Limit=vEvent_Limit_1;
     SET @vCommunication_CoolOff=vCommunication_CoolOff;
	 SET @vGoodTime=vGoodTime_1;
     SET @vCommunication_Channel=vCommunication_Channel_1;
	 IF done1 THEN
	   LEAVE LOOP_ALL_EVENTS;
	 END IF; 
     truncate Event_Execution_Stg;
	 set @vStart_Cnt = 0;set @vStart_CntCust=0; set @vEnd_Cnt=0; set @vEnd_CntCust=0;  


     
PROCESS_EVENT_EXECUTION_QUERY_CMAB: LOOP
SET @vEnd_CntCust = @vStart_CntCust + @vBatchSizeCust;

 
            
            INSERT IGNORE INTO Event_Execution_Stg SELECT * FROM Event_Execution_CMAB_Op eecm where Customer_Id between @vStart_CntCust and @vEnd_CntCust and Event_Id=@Event_ID_cur1 ;
            -- and not exists (select 1 from Event_Execution ee ,Event_Master em where ee.Event_ID=em.ID and ee.Customer_ID=eecm.Customer_ID and eecm.Communication_Channel=ee.Communication_Channel and em.Follows_Waterfall<>99);
            
select count(1) as Cnt from Event_Execution_Stg;
select @vStart_CntCust,@vEnd_CntCust;


			
         -- ## Limiting the number of Customers to 10 in case of test mode
            select count(1) into @eecnt from Event_Execution_Stg;
            SET @Overall_Limit_Test = @eecnt-10;
            IF RUN_MODE='TEST' and @eecnt>=15 
                        THEN
						   set @sql2=concat("UPDATE Event_Execution_Stg SET Is_Target = 'X' where Is_Target = 'Y' order by   RAND() LIMIT ",@Overall_Limit_Test,"");
	                       -- select @sql2;
		                   prepare stmt2 from @sql2;
		                   execute stmt2;
		                   deallocate prepare stmt2;
              DELETE from Event_Execution_Stg where Is_Target = 'X'; 
			  LEAVE PROCESS_EVENT_EXECUTION_QUERY_CMAB;
			END IF;


SELECT 
    Number_Of_Recommendation,
    Recommendation_Type,
    Exclude_Stock_out,
    Exclude_No_Image,
    Exclude_Recommended_X_Days,
    Exclude_Transaction_X_Days,
    Exclude_Never_Brought,
    Exclude_Recommended_SameLTD_FavCat,
    Recommendation_Filter_Logic
INTO @no_of_reco,@algo,@Exclude_Stock_out,@Exclude_No_Image,@Exclude_Recommended_X_Days , 
@Exclude_Transaction_X_Days , @Exclude_Never_Brought , @Exclude_Recommended_SameLTD_FavCat ,@Recommendation_Filter_Logic
FROM
    Communication_Template
WHERE
    ID = @Communication_Template_ID_cur1;

IF ifnull(@no_of_reco,0) > 0
THEN
    select count(1) into @EE_Cnt1 from Event_Execution_Stg;
    -- select @Event_ID_cur1,@Communication_Template_ID_cur1,@vStart_CntCust,@vEnd_CntCust,@algo,@Recommendation_Filter_Logic,
-- @Exclude_No_Image,@Exclude_Stock_out;
  CALL S_CLM_Event_Execution_Recommendation(@Event_ID_cur1,@Communication_Template_ID_cur1,@vStart_CntCust,@vEnd_CntCust,@algo,@Recommendation_Filter_Logic,@Exclude_No_Image,@Exclude_Stock_out);
  -- SELECT 'Reco',@vStart_CntCust,@vEnd_CntCust,@EE_Cnt1;
end if;

			IF @vEnd_CntCust >= @Rec_Cnt
            THEN
			  LEAVE PROCESS_EVENT_EXECUTION_QUERY_CMAB;
			END IF;

			SET @vStart_CntCust = @vEnd_CntCust;
END LOOP PROCESS_EVENT_EXECUTION_QUERY_CMAB;

CALL S_CLM_Event_Execution_PostProcess(@Event_ID_cur1,@Communication_Template_ID_cur1,@Event_Execution_Date_ID);
-- select 'Post_Process',@Event_ID_cur1;
    
END LOOP LOOP_ALL_EVENTS;	
close cur1;




END

