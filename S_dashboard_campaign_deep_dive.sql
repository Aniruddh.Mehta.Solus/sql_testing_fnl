DELIMITER $$
CREATE or replace PROCEDURE `S_dashboard_campaign_deep_dive`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 

	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare FILTER_DURATION text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    declare IN_AGGREGATION_4 text;
    declare IN_AGGREGATION_5 text;
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET FILTER_DURATION = json_unquote(json_extract(in_request_json,"$.FILTER_DURATION"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
	SET IN_AGGREGATION_4 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_4"));
    SET IN_AGGREGATION_5 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_5"));
	
			
			
			
	Select min(EE_Date) into @MinEE_Date from CLM_Event_Dashboard;
	
	Select max(EE_Date) into @MaxEE_Date from CLM_Event_Dashboard;

	CALL s_dashboard_filldate(@MinEE_Date,@MaxEE_Date);
	
	
	
	
	 
    SET @FY_SATRT_DATE_LIST = date_format(IN_AGGREGATION_4,"%Y-%m-%d");
	SET @FY_END_DATE_LIST = date_format(IN_AGGREGATION_5,"%Y-%m-%d");
	
	
	Create table if not EXISTS Event_Master_UI
	(Event_Id Varchar(128) Default '-1',
		Type_Of_Event Varchar(128) Default '-1',
		Primary Key Event_Id(Event_Id)
	);
	
	Insert Ignore into Event_Master_UI(Event_Id,Type_Of_Event)
	Select ID as Event_Id , CASE WHEN datediff(End_Date_ID,Start_Date_ID) < 32 THEN 'GTM' else 'CLM' END AS `Type_Of_Event`
	from  Event_Master;
	
	
	
	select Value1 into @Incremental_Revenue_Method from UI_Configuration_Global where Config_Name = 'Incremental_Revenue_Method';
    SET @Incremental_Revenue_Method=ifnull(@Incremental_Revenue_Method,'Method2');
    select @Incremental_Revenue_Method;
    IF @Incremental_Revenue_Method = 'Method1'
    THEN
		SET @incr_rev_concat='_By_Day';
        SET @group_cond='EE_Date';
        SET @mtd_concat='';
	ELSEIF @Incremental_Revenue_Method != 'Method1'
    THEN
		SET @group_cond='YM';
		SET @incr_rev_concat='';
        SET @mtd_concat='MTD_';
	ELSE
		SET @incr_rev_concat='';
        SET @mtd_concat='';
		SET @incr_Rev = ' round((CASE WHEN SUM(Control_Base) < 0 or SUM(Target_Delivered) = 0 then 0 WHEN (((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*100 < 0 then 0 WHEN SUM(Control_Base)>0 and SUM(Control_Responder)=0 then SUM(Target_Revenue)
					ELSE ifnull((((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*(SUM(Target_Revenue)/SUM(Target_Responder))*(SUM(Target_Delivered)),0) END),0) ';
		SET @group_cond='YM';
	END IF;
	
	IF IN_AGGREGATION_2 = 'ST'
    THEN
    SET @sql1=CONCAT('
		CREATE OR REPLACE VIEW CLM_Event_Dashboard_STLT
        as 
		SELECT `CLM_Event_Dashboard`.`Revenue Center`,
			`CLM_Event_Dashboard`.`Segment`,
			`CLM_Event_Dashboard`.`Campaign`,
			`CLM_Event_Dashboard`.`EE_Date`,
			`CLM_Event_Dashboard`.`Event_Execution_Date_ID`,
			`CLM_Event_Dashboard`.`Theme`,
			`CLM_Event_Dashboard`.`Offer`,
			`CLM_Event_Dashboard`.`Channel`,
			`CLM_Event_Dashboard`.`Has_Recommendations`,
			`CLM_Event_Dashboard`.`Event_ID`,
			`CLM_Event_Dashboard`.`Control_Base_Event` as `Control_Base`,
			`CLM_Event_Dashboard`.`Target_Base`,
			`CLM_Event_Dashboard`.`Target_Delivered`,
			`CLM_Event_Dashboard`.`Control_Responder_Event` as `Control_Responder`,
			`CLM_Event_Dashboard`.`Target_Responder`,
			`CLM_Event_Dashboard`.`Target_Revenue`,
			`CLM_Event_Dashboard`.`Target_Discount`,
			`CLM_Event_Dashboard`.`Target_Bills`,
			`CLM_Event_Dashboard`.`Target_Open`,
			`CLM_Event_Dashboard`.`Target_CTA`,
			`CLM_Event_Dashboard`.`Control_Revenue`,
			`CLM_Event_Dashboard`.`Control_Discount`,
			`CLM_Event_Dashboard`.`Control_Bills`,
			`CLM_Event_Dashboard`.`Created_Date`,
			`CLM_Event_Dashboard`.`Total_Customer_Base`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Day`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Month`,
			`CLM_Event_Dashboard`.`YM`,
			`CLM_Event_Dashboard`.`Incremental_Revenue_ST',@incr_rev_concat,'` AS `Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_ST',@incr_rev_concat,'` AS `MTD_Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Segment_ST',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Segment`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Revenue_Center_ST',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Revenue_Center`
		FROM `CLM_Event_Dashboard`;
        ');
        select @sql1;
		PREPARE statement from @sql1;
		Execute statement;
		Deallocate PREPARE statement;
    ELSE
		SET @sql1=CONCAT('
		CREATE OR REPLACE VIEW CLM_Event_Dashboard_STLT
        as 
		SELECT `CLM_Event_Dashboard`.`Revenue Center`,
			`CLM_Event_Dashboard`.`Segment`,
			`CLM_Event_Dashboard`.`Campaign`,
			`CLM_Event_Dashboard`.`EE_Date`,
			`CLM_Event_Dashboard`.`Event_Execution_Date_ID`,
			`CLM_Event_Dashboard`.`Theme`,
			`CLM_Event_Dashboard`.`Offer`,
			`CLM_Event_Dashboard`.`Channel`,
			`CLM_Event_Dashboard`.`Has_Recommendations`,
			`CLM_Event_Dashboard`.`Event_ID`,
			`CLM_Event_Dashboard`.`Control_Base` as `Control_Base`,
			`CLM_Event_Dashboard`.`Target_Base`,
			`CLM_Event_Dashboard`.`Target_Delivered`,
			`CLM_Event_Dashboard`.`Control_Responder` as `Control_Responder`,
			`CLM_Event_Dashboard`.`Target_Responder`,
			`CLM_Event_Dashboard`.`Target_Revenue`,
			`CLM_Event_Dashboard`.`Target_Discount`,
			`CLM_Event_Dashboard`.`Target_Bills`,
			`CLM_Event_Dashboard`.`Target_Open`,
			`CLM_Event_Dashboard`.`Target_CTA`,
			`CLM_Event_Dashboard`.`Control_Revenue`,
			`CLM_Event_Dashboard`.`Control_Discount`,
			`CLM_Event_Dashboard`.`Control_Bills`,
			`CLM_Event_Dashboard`.`Created_Date`,
			`CLM_Event_Dashboard`.`Total_Customer_Base`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Day`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Month`,
			`CLM_Event_Dashboard`.`YM`,
			`CLM_Event_Dashboard`.`Incremental_Revenue',@incr_rev_concat,'` AS `Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue',@incr_rev_concat,'` AS `MTD_Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Segment',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Segment`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Revenue_Center',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Revenue_Center`
		FROM `CLM_Event_Dashboard`;
        ');
        select @sql1;
		PREPARE statement from @sql1;
		Execute statement;
		Deallocate PREPARE statement;
    END IF;
	



     
Select IN_MEASURE,FILTER_DURATION;
IF FILTER_DURATION = 'Y' 
   THEN 
    
	    SET @filter_cond=concat(" year(EE_Date) = year(now())  ");
		SET @filter_conds=concat(" year(a1.EE_Date) = year(now())  ");
		SET @filter_cond_CC=concat(" year(Event_Execution_Date_Id) = year(now())  ");
		SET @date_select = 'str_to_date( ifnull(date_format(a1.EE_DATE,"%b-%Y"),0),"%b-%Y") AS "Year" ';
		SET @date_sel = 'str_to_date( ifnull(date_format(EE_DATE,"%b-%Y"),0),"%b-%Y") AS "Year" ';
		 SET @date_selects = ' date_format(Year,"%b-%Y") as year ';
		 SET @create_view=CONCAT('CREATE OR REPLACE VIEW V_Dashboard_MTD_Incr_Rev
			as
			select MAX(EE_Date) as EE_Date,MAX(MTD_Incremental_Revenue) as Incremental_Revenue,SUM(Target_Revenue) as Target_Revenue,MAX(Total_Revenue_This_Month) as Total_Revenue_This_Month,SUM(Target_Delivered) as Target_Delivered,SUM(Target_Bills) as Target_Bills,SUM(Control_Bills) as Control_Bills,Event_ID,Campaign,Channel,YM from CLM_Event_Dashboard_STLT group by Campaign, Event_Id,',@group_cond,'');
				select @create_view;
	PREPARE statement from @create_view;
	Execute statement;
	Deallocate PREPARE statement;
		
   END IF;
IF FILTER_DURATION = 'Q' 
   THEN 
	    SET @filter_cond=concat(" quarter(EE_Date) = quarter(now())  AND   year(EE_Date) = year(now()) ");
		SET @filter_conds=concat("  quarter(a1.EE_Date) = quarter(now())  AND  year(a1.EE_Date) = year(now()) ");
		SET @filter_cond_CC=concat(" quarter(Event_Execution_Date_Id) = quarter(now())  AND  year(Event_Execution_Date_Id) = year(now())  ");
        SET @date_select = ' str_to_date( ifnull(date_format(a1.EE_DATE,"%b-%Y"),0),"%b-%Y") AS "Year" ';
		SET @date_sel = ' str_to_date( ifnull(date_format(EE_DATE,"%b-%Y"),0),"%b-%Y") AS "Year" ';
		SET @date_selects = ' date_format(Year,"%b-%Y") as year ';
		SET @create_view=CONCAT('CREATE OR REPLACE VIEW V_Dashboard_MTD_Incr_Rev
			as
			select MAX(EE_Date) as EE_Date,MAX(MTD_Incremental_Revenue) as Incremental_Revenue,SUM(Target_Revenue) as Target_Revenue,MAX(Total_Revenue_This_Month) as Total_Revenue_This_Month,SUM(Target_Delivered) as Target_Delivered,SUM(Target_Bills) as Target_Bills,SUM(Control_Bills) as Control_Bills,Event_ID,Campaign,Channel,YM from CLM_Event_Dashboard_STLT group by Campaign, Event_Id,',@group_cond,'');
		  select @filter_cond;
		  	select @create_view;
	PREPARE statement from @create_view;
	Execute statement;
	Deallocate PREPARE statement;
		
   END IF;
IF FILTER_DURATION = '30' 
   THEN 
	    SET @filter_cond=concat("  EE_Date between DATE_SUB( now(), INTERVAL 30 day) and now()  ");
		SET @filter_conds=concat("  a1.EE_Date between DATE_SUB( now(), INTERVAL 30 day) and now()  ");
		 SET @filter_cond_CC=concat("  Event_Execution_Date_Id between DATE_SUB( now(), INTERVAL 30 day) and now()  ");
	    SET @date_select = ' str_to_date( date_format(a1.EE_DATE,"%d-%b"),"%d-%b")AS "Year" ';
		SET @date_sel = ' str_to_date( date_format(EE_DATE,"%d-%b"),"%d-%b")AS "Year" ';
		SET @date_selects = ' date_format(Year,"%d-%b") as year ';
		SET @create_view=CONCAT('CREATE OR REPLACE VIEW V_Dashboard_MTD_Incr_Rev
			as
			select MAX(EE_Date) as EE_Date,MAX(MTD_Incremental_Revenue) as Incremental_Revenue,SUM(Target_Revenue) as Target_Revenue,MAX(Total_Revenue_This_Month) as Total_Revenue_This_Month,SUM(Target_Delivered) as Target_Delivered,SUM(Target_Bills) as Target_Bills,SUM(Control_Bills) as Control_Bills,Event_ID,Campaign,Channel,YM from CLM_Event_Dashboard_STLT group by Campaign, Event_Id,',@group_cond,'');
				select @create_view;
	PREPARE statement from @create_view;
	Execute statement;
	Deallocate PREPARE statement;
		
		
   END IF;

IF FILTER_DURATION = '7' 
   THEN 
	    SET @filter_cond=concat("  EE_Date between DATE_SUB( now(), INTERVAL 7 day) and now() ");
		SET @filter_conds=concat(" a1.EE_Date between DATE_SUB( now(), INTERVAL 7 day) and now() ");
		SET @filter_cond_CC=concat(" Event_Execution_Date_Id between DATE_SUB( now(), INTERVAL 7 day) and now() ");
		SET @date_select = ' str_to_date( date_format(a1.EE_DATE,"%d-%b"),"%d-%b")AS "Year" ';
		SET @date_sel = ' str_to_date( date_format(EE_DATE,"%d-%b"),"%d-%b")AS "Year" ';
		SET @date_selects = ' date_format(Year,"%d-%b") as year ';
		SET @create_view=CONCAT('CREATE OR REPLACE VIEW V_Dashboard_MTD_Incr_Rev
			as
			select MAX(EE_Date) as EE_Date,MAX(MTD_Incremental_Revenue) as Incremental_Revenue,SUM(Target_Revenue) as Target_Revenue,MAX(Total_Revenue_This_Month) as Total_Revenue_This_Month,MAX(Total_Revenue_This_Day) as Total_Revenue_This_Day,SUM(Target_Delivered) as Target_Delivered,SUM(Target_Bills) as Target_Bills,SUM(Control_Bills) as Control_Bills,Event_ID,Campaign,Channel,YM  from CLM_Event_Dashboard_STLT group by Campaign, Event_Id,',@group_cond,'');
				select @create_view;
	PREPARE statement from @create_view;
	Execute statement;
	Deallocate PREPARE statement;
		
   END IF;
 IF FILTER_DURATION = 'BYMONTH' 
   THEN  
	    SET @filter_cond=concat(" EE_Date BETWEEN '",@FY_SATRT_DATE_LIST,"' AND '",@FY_END_DATE_LIST,"'  ");
		SET @filter_conds=concat(" a1.EE_Date BETWEEN '",@FY_SATRT_DATE_LIST,"' AND '",@FY_END_DATE_LIST,"'  ");
		SET @filter_cond_CC=concat(" Event_Execution_Date_Id BETWEEN DATE_FORMAT('",@FY_SATRT_DATE_LIST,"','%Y%m%d') AND DATE_FORMAT('",@FY_END_DATE_LIST,"','%Y%m%d')");
	    SET @date_select = ' str_to_date( date_format(a1.EE_DATE,"%d-%b"),"%d-%b")AS "Year" ';
		SET @date_sel = ' str_to_date( date_format(EE_DATE,"%d-%b"),"%d-%b")AS "Year" ';
		SET @date_selects = ' date_format(Year,"%d-%b") as year ';
		SET @create_view=CONCAT('CREATE OR REPLACE VIEW V_Dashboard_MTD_Incr_Rev
			as
			select MAX(EE_Date) as EE_Date,MAX(MTD_Incremental_Revenue) as Incremental_Revenue,SUM(Target_Revenue) as Target_Revenue,MAX(Total_Revenue_This_Month) as Total_Revenue_This_Month,MAX(Total_Revenue_This_Day) as Total_Revenue_This_Day,SUM(Target_Delivered) as Target_Delivered,SUM(Target_Bills) as Target_Bills,SUM(Control_Bills) as Control_Bills,Event_ID,Campaign,Channel,YM from CLM_Event_Dashboard_STLT group by Campaign, Event_Id,',@group_cond,'');
			
	select @create_view;
	PREPARE statement from @create_view;
	Execute statement;
	Deallocate PREPARE statement;
		
   END IF;
     
	 
	 
	 
	
   
   
   
      SET @incr_Rev =' round(SUM(Incremental_Revenue),0) ';
   
   IF IN_MEASURE = "Delivery_Rate" OR IN_MEASURE = "CARDS" 
	THEN
		SET  @Query_Measure=' round(ifnull(ifnull(SUM(Target_Delivered),0)/ifnull(SUM(Target_Base),0)*100,0),2)';
		SET @delivery_rate = ' concat(round(ifnull(ifnull(SUM(Target_Delivered),0)/ifnull(SUM(Target_Base),0)*100,0),2),"%")';
		SET @table_used = 'CLM_Event_Dashboard';
		select @delivery_rate;
	END IF;
IF IN_MEASURE = "Incr_Rev" OR IN_MEASURE = "CARDS"
	THEN
		SET @Query_Measure = ' round(ifnull(sum(Incremental_Revenue),0),0) ';
		SET @incr_rev = ' cast(format(round(ifnull(sum(Incremental_Revenue),0),2),",")as char)  ';
		SET @table_used = 'V_Dashboard_MTD_Incr_Rev';
		select @incr_rev;			
		
	END IF;
	
IF IN_MEASURE = "Customers" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Customers =' cast(format(round(ifnull(SUM(Target_Base),0),2),",")as char)';
		set @Query_Measure = ' round(ifnull(SUM(a2.Target_Base),0),2) ';
		SET @table_used = 'CLM_Event_Dashboard';
	END IF;
IF IN_MEASURE = "CG_Base" OR IN_MEASURE = "CARDS" 
	THEN
		SET @cg_base =' cast(format(round(ifnull(SUM(Control_Base),0),2),",")as char) ';
		set @Query_Measure = ' round(ifnull(SUM(a2.Control_Base),0),2) ';
		SET @table_used = 'CLM_Event_Dashboard';
		
	END IF;
IF IN_MEASURE = "Avg_Spend" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' round(ifnull(ifnull(SUM(Target_Revenue),0)/ifnull(SUM(a2.Target_Responder),0),0),2) ';
		SET @avg_spend = ' cast(format(round(ifnull(ifnull(SUM(Target_Revenue),0)/ifnull(SUM(Target_Responder),0),0),2),",")as char) ';
		SET @table_used = 'CLM_Event_Dashboard';
	END IF;
/*IF IN_MEASURE = "Incr. Bills" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' round(ifnull(SUM(a2.Target_Bills),0)-ifnull(SUM(a2.Control_Bills),0),2) ';
        SET @incr_bill = ' cast(format(round(ifnull(SUM(Target_Bills),0)-ifnull(SUM(Control_Bills),0),2),",")as char) ';
		SET @table_used = 'CLM_Event_Dashboard';
	END IF;*/
IF IN_MEASURE = "Incr_as_per_of_TG_Rev" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' round(ifnull(ifnull(sum(Incremental_Revenue),0) / ifnull(SUM(Target_Revenue),0)*100,0),2) ';
	    SET @incr_as_of_tg_rev = ' concat(round(ifnull(ifnull(sum(Incremental_Revenue),0) / ifnull(SUM(Target_Revenue),0)*100,0),2),"%") ';
		SET @table_used = 'V_Dashboard_MTD_Incr_Rev';
	END IF;
IF IN_MEASURE = "Clicks" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure =' round(ifnull(SUM(Target_CTA),0),2) ';
		SET @clicks = 'cast(format(round(ifnull(SUM(Target_CTA),0),2),",")as char) ';
		SET @table_used = 'CLM_Event_Dashboard';
	END IF;
IF IN_MEASURE = "Click_Rate" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' round(ifnull(ifnull(SUM(Target_CTA),0) / ifnull(SUM(Target_Delivered),0)*100,0),2) ';
		SET @click_rate = ' concat(round(ifnull(ifnull(SUM(Target_CTA),0) / ifnull(SUM(Target_Delivered),0)*100,0),2),"%") ';
		SET @table_used = 'CLM_Event_Dashboard';
	END IF;
IF IN_MEASURE = "Click_Conversions" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' round(ifnull(SUM(CTA_Responder),0),2) ';
		SET @click_conversion = ' cast(format(round(ifnull(SUM(CTA_Responder),0),2),",")as char) ';
		SET @table_used = 'CLM_Event_Dashboard';
	END IF;
IF IN_MEASURE = "Hard_Conversion" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' round(ifnull(SUM(Hard_Responder_Target),0),2) ';
	    SET @hard_conversion = ' cast(format(round(ifnull(SUM(Hard_Responder_Target),0),2),",")as char) ';
		SET @table_used = 'CLM_Event_Dashboard';
	END IF;
IF IN_MEASURE = "Hard_Response" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' round(ifnull(ifnull(SUM(Hard_Responder_Target),0) / ifnull(SUM(Target_Delivered),0)*100,0),2) ';
        SET @hard_response = 'concat( round(ifnull(ifnull(SUM(Hard_Responder_Target),0) / ifnull(SUM(Target_Delivered),0)*100,0),2),"%") ';
		SET @table_used = 'CLM_Event_Dashboard';
	END IF;
IF IN_MEASURE = "Customer_Coverage" OR IN_MEASURE = "CARDS" 
	THEN	
		IF IN_AGGREGATION_1 IS NULL or  IN_AGGREGATION_1 =''
			THEN SET @Query_Measure=" '' ";
				SET @customer_coverage = "'' ";
				SET @table_used = "''";
			
			Else
				SET @Camapign = CONCAT(' Event_Id in (Select Event_ID from Campaign_Deep_Dive_Display where Campaign = "',IN_AGGREGATION_1,'")');
		
		
			Select @filter_cond_CC, @Camapign;
			
			Set @SQL1=concat('Select  Ifnull(count(distinct Customer_Id),0) into @Customer_Coverage_Count
            from Event_Execution_History 
			where ',@filter_cond_CC,' and  ',@Camapign,';');
			Select @SQL1;
			PREPARE statement from @SQL1;
			Execute statement;
			Deallocate PREPARE statement;
	
			
			Select @Customer_Coverage_Count;
		
		IF FILTER_DURATION ="Y" or FILTER_DURATION ="Q"
			Then 	SET @Query_Measure=" ifnull(max(Total_Distinct_Customers_Targeted_This_Month_This_Campaign)/MAX(Total_Customer_Base)*100,0) ";
			
			ELSE
				SET @Query_Measure=" ifnull(max(Total_Distinct_Customers_Targeted_This_Day_This_Campaign)/MAX(Total_Customer_Base)*100,0) ";
			
		End If;	
		
		Select Concat(round((ifnull((@Customer_Coverage_Count)/MAX(Total_Customer_Base),0))*100,2),"%") into @customer_coverage From CLM_Event_Dashboard limit 1 ;
		
		Select @customer_coverage;
		SET @table_used = 'CLM_Event_Dashboard';
		
			End If;
			
	END IF;
IF IN_MEASURE = "per_Click_Conversion" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' round(ifnull(ifnull(SUM(CTA_Responder),0) / ifnull(SUM(Target_CTA),0)*100,0),2)';
	     SET @percnt_click_conversion = ' concat(round(ifnull(ifnull(SUM(CTA_Responder),0) / ifnull(SUM(Target_CTA),0)*100,0),2),"%") ';
		 SET @table_used = 'CLM_Event_Dashboard';
	END IF;
IF IN_MEASURE = "Yeild" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure='  round(ifnull(ifnull(sum(Incremental_Revenue),0)/ifnull(SUM(Target_Delivered),0),0),2) ';
	    SET @yield = ' cast(round(ifnull(ifnull(sum(Incremental_Revenue),0)/ifnull(SUM(Target_Delivered),0),0),2)as char) ';
		SET @table_used = 'V_Dashboard_MTD_Incr_Rev';
	END IF;
IF IN_MEASURE = "TG_Conversions" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure='  round(ifnull(SUM(Target_Responder),0),2) ';
        SET @tg_conversions = '  cast(format(round(ifnull(SUM(Target_Responder),0),2),",")as char) ';
		SET @table_used = 'CLM_Event_Dashboard';
	END IF;
IF IN_MEASURE = "TG_Rev" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure='  round(ifnull(SUM(Target_Revenue),0),2) ';
		SET @tg_rev ='   cast(format(round(ifnull(SUM(Target_Revenue),0),2),",")as char) ';
		SET @table_used = 'CLM_Event_Dashboard';
	END IF;
IF IN_MEASURE = "CG_Response" OR IN_MEASURE = "CARDS" 
	THEN
	   SET @Query_Measure='  round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2)';
	   SET @cg_respons = '  concat(round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2),"%") ';
	   SET @table_used = 'CLM_Event_Dashboard';
	END IF;
IF IN_MEASURE = "TG_Delivered" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure='  round(ifnull(SUM(Target_Delivered),0),2) ';
		SET @tg_del = '  cast(format(round(ifnull(SUM(Target_Delivered),0),2),",")as char) ';
		SET @table_used = 'CLM_Event_Dashboard';
	END IF;
IF IN_MEASURE = "TG_Response" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure='  round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2)';
		SET @tg_resp = '  concat(round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%")';
		SET @table_used = 'CLM_Event_Dashboard';
	END IF;
	
	
	
IF IN_MEASURE = "Incr_Bills" OR IN_MEASURE = "CARDS" 
    THEN
		SET @Query_Measure='round(round(sum(Target_Base)*(CASE WHEN (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))<=0 THEN 0 ELSE (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))END),2)/100,2)';
        SET @incr_bill = ' cast(format(round(sum(Target_Base)*(CASE WHEN (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))<=0 THEN 0 ELSE (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))END),2)/100,",")as char) ';
		SET @table_used = 'CLM_Event_Dashboard';
	END IF;
		
        



	
	Select 	IN_AGGREGATION_2;
IF IN_AGGREGATION_2 = "GRAPH"  and IN_SCREEN ="DEEPDIVE"
THEN
	IF (IN_AGGREGATION_1 IS NULL or IN_AGGREGATION_1="")
	THEN SET @camp= "";
	ELSE SET @camp= CONCAT(' where  Campaign = "',IN_AGGREGATION_1,'"');
	End if;
	IF (FILTER_DURATION IS NULL or FILTER_DURATION="")
	THEN SET @filter_conds=concat(" quarter(t2.EE_Date) = quarter(now())  AND  year(t2.EE_Date) = year(now())    ");
        SET @date_select = ' str_to_date( ifnull(date_format(a1.EE_DATE,"%b-%Y"),0),"%b-%Y") AS "Year" ';
		SET @date_selects = ' date_format(Year,"%b-%Y") as year ';
	End if;
	/*IF  IN_MEASURE="TG Delivered"
	THEN SET @Query_Measure='  round(ifnull(SUM(a2.Target_Delivered),0),2) ';
	End if;*/
	
	

    select @date_select,@Query_Measure,@filter_cond;
		SET @view_stmt=concat( 'CREATE OR REPLACE VIEW V_Dashboard_Lifts_Graph AS(SELECT  ',@date_selects,' , Value   FROM (select * FROM (                                         select   ',@date_select,' , ',@Query_Measure,'  as "value" from (select * from ',@table_used,'  ',@camp,')  a2 right join tablenew  a1 on a1.EE_Date =a2.EE_Date WHERE ',@filter_conds, '     group by 1  
				 )as d1 ) AS D2) ');  
       
		select @view_stmt;
		PREPARE statement from @view_stmt;
	Execute statement;
	Deallocate PREPARE statement; 
	
	Select Case when count(1)< 15 Then "1"
	            when count(1)< 45 Then "2"
				when count(1)< 90 Then "3"
				when count(1)< 182 Then "4"
		else "5"
		end into @flag
		From V_Dashboard_Lifts_Graph;
		
	SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`Year`,"Value",`Value`,"Flag",@flag) )),"]")into @temp_result from V_Dashboard_Lifts_Graph  ' ;
	Select @Query_String;
		PREPARE statement from @Query_String;
	Execute statement;
	Deallocate PREPARE statement; 

End if;	
	
     SELECT  @FY_END_DATE_LIST ;
IF IN_AGGREGATION_2='Campaign_List'  and  IN_AGGREGATION_4 !="Search"  
    THEN
	IF IN_AGGREGATION_3 IS NULL  OR IN_AGGREGATION_3=""
	THEN SET @Typeofevent = "1=1";
	
	ELSE SET @Typeofevent = CONCAT('a2.Type_Of_Event="',IN_AGGREGATION_3,'"');
	End if;
	
	Select @Typeofevent;
	
	IF IN_AGGREGATION_1 IS NULL or IN_AGGREGATION_1=""
	THEN SET @Typeofchannel = "1=1";
	
	ELSE SET @Typeofchannel = CONCAT('a1.ChannelName = "',IN_AGGREGATION_1,'"');
	End if;
	
	Select @Typeofchannel;
	
		
	IF @FY_SATRT_DATE_LIST IS NOT  NULL and @FY_END_DATE_LIST IS NOT NULL
	   THEN  
	     SET @Typeofdate = CONCAT('date_format(a4.EE_Date,"%Y-%m-%d") >= "',@FY_SATRT_DATE_LIST,'" AND date_format(a4.EE_Date,"%Y-%m-%d") <= "',@FY_END_DATE_LIST,'"');
	
		Else 
		SET @Typeofdate  = "1=1";
	End if;
	
	Select @Typeofdate;
	
	Set @Condition =concat(@Typeofevent," and ",@Typeofchannel ," and ",@Typeofdate);
	SELECT @Condition;
	
set @view_stmt=concat('
					create or replace view Campaign_Deep_Dive_Display as
					(SELECT DISTINCT a4.Campaign AS Campaign,a2.Event_ID FROM Event_Master a1 
					 inner join Event_Master_UI a2 on a1.ID = a2.Event_ID
					 inner join Communication_Template a3 on a1.CreativeId1 = a3.ID 
					 inner join CLM_Event_Dashboard a4 on a4.Event_ID = a2.Event_ID
 WHERE ',@Condition,' );');
 select @view_stmt;

	PREPARE statement from @view_stmt;
	Execute statement;
	Deallocate PREPARE statement;
 select count(1) from Campaign_Deep_Dive_Display; 
    
SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("camp",`Campaign`) )),"]")into @temp_result from Campaign_Deep_Dive_Display;' ;
select @Query_String;
	PREPARE statement from @Query_String;
	Execute statement;
	Deallocate PREPARE statement; 
    
 
 End If;


IF IN_AGGREGATION_2='Campaign_List' and  IN_AGGREGATION_4 ="Search" 
	Then
	SET @Condition = CONCAT('Campaign LIKE  "',IN_AGGREGATION_1,'%"');
	
	Select @Condition;
	
	
	set @view_stmt=concat('
		create or replace view Camp_Display_Search as
		(SELECT  Campaign FROM Campaign_Deep_Dive_Display
		 WHERE ',@Condition,' );');
		 select @view_stmt;
			PREPARE statement from @view_stmt;
			Execute statement;
			Deallocate PREPARE statement;

			
	SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("camp",`Campaign`) )),"]")into @temp_result from Camp_Display_Search;' ;
    select @Query_String;
	PREPARE statement from @Query_String;
	Execute statement;
	Deallocate PREPARE statement; 
    		
End If;
	
 Select IN_AGGREGATION_2;
IF IN_AGGREGATION_2='Event_Details'
    THEN
IF IN_AGGREGATION_1 is NUll or  IN_AGGREGATION_1 =''
 Then set @temp_result='[]';
else
 set @view_stmt=concat('create or replace view Campaign_Details as
							(SELECt a2.Type_Of_Event AS Type_Of_Event  , a1.ChannelName AS ChannelName , Case when ChannelName ="Email" Then Concat( "Subject line is :",Substring_Index(a3.Template,"|",-1)) else replace(replace(replace(a3.Template,"</SOLUS_PFIELD>","#"),"<SOLUS_PFIELD>","#"),"|"," ") end AS Template, a4.Campaign FROM Event_Master a1 
					 inner join Event_Master_UI a2 on a1.ID = a2.Event_ID
					 inner join Communication_Template a3 on a3.ID = a1.CreativeId1 
					 inner join CLM_Event_Dashboard a4 on a4.Event_ID = a2.Event_ID where a4.Campaign = "',IN_AGGREGATION_1,'" LIMIT 1 );');
        Select @view_stmt;
 -- SET @t1 =CONCAT("SELECT ",@Query_Measure," FROM ",tab_name);
 PREPARE stmt3 FROM @view_stmt;
 EXECUTE stmt3;
 DEALLOCATE PREPARE stmt3;
 
    SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("TYPE",`Type_Of_Event`,"ChannelName",`ChannelName`,"Template",`Template`,"Campaign",Campaign) )),"]")into @temp_result from Campaign_Details;' ;
select @Query_String;
	PREPARE statement from @Query_String;
	Execute statement;
	Deallocate PREPARE statement; 
 End IF;
 End If;
   
	
IF IN_AGGREGATION_2='CARDS'  
THEN

IF IN_AGGREGATION_1 IS NULL or  IN_AGGREGATION_1 =''
	THEN SET @Camapign = "1=1";
	     
	
	 ELSE SET @Camapign = CONCAT(' Campaign = "',IN_AGGREGATION_1,'"');
	 Select @Camapign;
	End if;
	
    set @view_stmt=concat('create or replace view campaign_deep_dive_Lifts_Card as
	(SELECT "TG Delivered" as Card_Name,',@tg_del,' as `Value1`,"" as Value2,"" as Value3,"TG_Delivered" as `Measure` FROM CLM_Event_Dashboard where ',@filter_cond,' and ',@Camapign,' )
			Union 
      (SELECT "Delivery Rate" as Card_Name,',@delivery_rate,' as `Value1`,"" as Value2,"" as Value3,"Delivery_Rate" as `Measure` FROM CLM_Event_Dashboard where ',@filter_cond,' and ',@Camapign,')
 UNION
       (SELECT "Customers" as Card_Name, ',@Customers,'as `Value1`,"" as Value2,"" as Value3,"Customers" as `Measure` FROM CLM_Event_Dashboard where ',@filter_cond,' and ',@Camapign,' )
			Union
			(SELECT "CG Base" as Card_Name, ',@cg_base,'as `Value1`,"" as Value2,"" as Value3,"CG_Base" as `Measure` FROM CLM_Event_Dashboard where ',@filter_cond,' and  ',@Camapign,')
			UNION
			(SELECT "TG Response" as Card_Name ,',@tg_resp,'as `Value1`,"" as Value2,"" as Value3,"TG_Response" as `Measure` FROM CLM_Event_Dashboard where ',@filter_cond,' and  ',@Camapign,')
			Union
		(SELECT "TG Conversions" as Card_Name, ',@tg_conversions,'as `Value1`,"" as Value2,"" as Value3,"TG_Conversions" as `Measure` FROM CLM_Event_Dashboard where ',@filter_cond,' and  ',@Camapign,')
			Union
			(SELECT "Avg. Spend" as Card_Name,',@avg_spend,'as `Value1`,"" as Value2,"" as Value3,"Avg_Spend" as `Measure` FROM CLM_Event_Dashboard where ',@filter_cond,' and ',@Camapign,')
			Union
			(SELECT "TG Rev." as Card_Name,',@tg_rev,' as `Value1`,"" as Value2,"" as Value3,"TG_Rev" as `Measure` FROM CLM_Event_Dashboard where ',@filter_cond,' and  ',@Camapign,')
			Union
			(SELECT "CG Response" as Card_Name,',@cg_respons,' as `Value1`,"" as Value2,"" as Value3,"CG_Response" as `Measure` FROM CLM_Event_Dashboard where ',@filter_cond,' and  ',@Camapign,')
			Union
	(SELECT "Incr. Bills" as Card_Name, ',@incr_bill,' as `Value1`,"" as Value2,"" as Value3,"Incr_Bills" as `Measure` FROM CLM_Event_Dashboard where ',@filter_cond,' and  ',@Camapign,')
			Union
		(SELECT "Incr. Rev." as Card_Name,',@incr_rev,' as `Value1`,"" as Value2,"" as Value3,"Incr_Rev" as `Measure`FROM V_Dashboard_MTD_Incr_Rev where ',@filter_cond,' and  ',@Camapign,')
			Union
	(SELECT "Incr. as % of TG Rev." as Card_Name,',@incr_as_of_tg_rev,'as `Value1`,"" as Value2,"" as Value3,"Incr_as_per_of_TG_Rev" as `Measure` FROM V_Dashboard_MTD_Incr_Rev where ',@filter_cond,' and  ',@Camapign,')
			Union
	(SELECT "Clicks" as Card_Name,',@clicks,' as `Value1`,"" as Value2,"" as Value3,"Clicks" as `Measure`FROM CLM_Event_Dashboard where ',@filter_cond,' and  ',@Camapign,')
			Union
			(SELECT "Click Rate" as Card_Name, ',@click_rate,' as `Value1`,"" as Value2,"" as Value3,"Click_Rate" as `Measure` FROM CLM_Event_Dashboard where ',@filter_cond,' and  ',@Camapign,')
			Union
			(SELECT "Click Conversions" as Card_Name, ',@click_conversion,'as `Value1`,"" as Value2,"" as Value3,"Click_Conversions" as `Measure` FROM CLM_Event_Dashboard where ',@filter_cond,' and  ',@Camapign,')
			Union
	(SELECT "% Click Conversion" as Card_Name,',@percnt_click_conversion,' as `Value1`,"" as Value2,"" as Value3,"per_Click_Conversion" as `Measure`FROM CLM_Event_Dashboard where ',@filter_cond,' and  ',@Camapign,')
			Union
	(SELECT "Hard Conversion" as Card_Name,',@hard_conversion,' as `Value1`,"" as Value2,"" as Value3,"Hard_Conversion" as `Measure`FROM CLM_Event_Dashboard where ',@filter_cond,' and ',@Camapign,')
			Union
			(SELECT "Hard Response" as Card_Name,',@hard_response,' as `Value1`,"" as Value2,"" as Value3,"Hard_Response" as `Measure`FROM CLM_Event_Dashboard where ',@filter_cond,'and  ',@Camapign,')
			Union
	(SELECT "Customer Coverage" as Card_Name,"',@customer_coverage,'" as `Value1`,"" as Value2,"" as Value3,"Customer_Coverage" as `Measure`)
			Union
	(SELECT "Yeild" as Card_Name,',@yield,' as `Value1`,"" as Value2,"" as Value3,"yeild" as `Measure` FROM V_Dashboard_MTD_Incr_Rev where ',@filter_cond,' and  ',@Camapign,')');
			
			Select @view_stmt;	 
	 PREPARE stmt3 FROM @view_stmt;
	 EXECUTE stmt3;
	 DEALLOCATE PREPARE stmt3;
	  
	  SELECT CONCAT("[",(GROUP_CONCAT(json_object("Card_Name",`Card_Name`,"Value1",`Value1`,"Value2",`Value2`,"Value3",`Value3`,"Measure",`Measure`) )),"]")into @temp_result from campaign_deep_dive_Lifts_Card where  Value1!="''"  ;
	
	 
 End If;
 
IF IN_AGGREGATION_2='ALL' 

THEN
IF IN_AGGREGATION_1 is NUll or  IN_AGGREGATION_1 =''
 Then set @temp_result='[]';
else
    set @view_stmt=concat('create or replace view campaign_deep_dive_Lifts_Card as

 (select * from
(
  with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Campaign, concat(round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") as target FROM  CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI)
AND ',@filter_cond,' group by 1)
select Campaign,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Campaign = "',IN_AGGREGATION_1,'") as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display)  ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI) and  ',@filter_cond,'  ),0) as bt from cte2 ) as d2)	
UNION all
(select * from
(
  with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Campaign, concat(CASE WHEN (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))<=0 THEN 0 ELSE (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))END ,"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI)
AND ',@filter_cond,' group by 1)
select Campaign,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Campaign = "',IN_AGGREGATION_1,'") as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display)   ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(CASE WHEN (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))<=0 THEN 0 ELSE (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))END ,"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI) AND  ',@filter_cond,'  ),0) as bt from cte2 ) as d2)

UNION all
(
select * from

 ( with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Campaign, concat(round(ifnull(ifnull(sum(Incremental_Revenue),0) / ifnull(SUM(Target_Revenue),0)*100,0),2),"%") as target  FROM V_Dashboard_MTD_Incr_Rev where  Campaign in (select Campaign from Campaign_Deep_Dive_Display)  and Event_ID in (select event_id from Event_Master_UI ) AND  ',@filter_cond,'
 group by 1)
select Campaign,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Campaign = "',IN_AGGREGATION_1,'") as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display)  ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((SELECt  concat(round(ifnull(ifnull(sum(Incremental_Revenue),0) / ifnull(SUM(Target_Revenue),0)*100,0),2),"%") as target  FROM V_Dashboard_MTD_Incr_Rev where  Campaign in (select Campaign from Campaign_Deep_Dive_Display)  and Event_ID in (select event_id from Event_Master_UI )  AND  ',@filter_cond,'  ),0)as bt from cte2 ) as d3)


UNION all

(select * from
  (with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Campaign, concat(round(ifnull(ifnull(SUM(Target_CTA),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI)
AND ',@filter_cond,' group by 1)
select Campaign,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Campaign = "',IN_AGGREGATION_1,'") as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(Target_CTA),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI) AND  ',@filter_cond,'  ),0) as bt from cte2  ) as d4)

UNION all

(select * from 

 ( with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Campaign, concat(round(ifnull(ifnull(SUM(CTA_Responder),0) / ifnull(SUM(Target_CTA),0)*100,0),2),"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI)
AND ',@filter_cond,' group by 1)
select Campaign,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Campaign = "',IN_AGGREGATION_1,'") as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(CTA_Responder),0) / ifnull(SUM(Target_CTA),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI)  AND  ',@filter_cond,'  ),0) as bt from cte2 ) as d5)');


			Select @view_stmt;	 
	 PREPARE stmt3 FROM @view_stmt;
	 EXECUTE stmt3;
	 DEALLOCATE PREPARE stmt3;
	  
	  SELECT CONCAT("[",(GROUP_CONCAT(json_object("Card_Name",`camp`,"Value1",`ofs`,"Value2",`total`,"Value3",`bt`) )),"]")into @temp_result from campaign_deep_dive_Lifts_Card ;
	
	end if;
	 
 End If;
 
 IF IN_AGGREGATION_2='GTM' 
THEN    
IF IN_AGGREGATION_1  is NUll or  IN_AGGREGATION_1 ='' 
Then set @temp_result='[]';
else
set @view_stmt=concat('create or replace view campaign_deep_dive_Lifts_Card as

(select * from
(
  with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Campaign, concat(round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") as target FROM 
CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM")
AND ',@filter_cond,' group by 1)
select Campaign,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Campaign = "',IN_AGGREGATION_1,'") as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM") ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM") AND  ',@filter_cond,'  ),0) as bt from cte2 ) as d2)
UNION all
(select * from
(
  with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Campaign, concat(CASE WHEN (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))<=0 THEN 0 ELSE (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))END ,"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM")
AND ',@filter_cond,' group by 1)
select Campaign,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Campaign = "',IN_AGGREGATION_1,'") as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM") ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(CASE WHEN (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))<=0 THEN 0 ELSE (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))END ,"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM") AND  ',@filter_cond,'  ),0) as bt from cte2 ) as d2)

UNION all
(
select * from

 ( with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Campaign, concat(round(ifnull(ifnull(sum(Incremental_Revenue),0) / ifnull(SUM(Target_Revenue),0)*100,0),2),"%") as target  FROM V_Dashboard_MTD_Incr_Rev where  Campaign in (select Campaign from Campaign_Deep_Dive_Display)  and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM" ) AND  ',@filter_cond,'
 group by 1)
select Campaign,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Campaign = "',IN_AGGREGATION_1,'") as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM")  ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((SELECt  concat(round(ifnull(ifnull(sum(Incremental_Revenue),0) / ifnull(SUM(Target_Revenue),0)*100,0),2),"%") as target  FROM V_Dashboard_MTD_Incr_Rev where  Campaign in (select Campaign from Campaign_Deep_Dive_Display)  and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM" )  AND  ',@filter_cond,'  ),0)as bt from cte2 ) as d3)


UNION all

(select * from
  (with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Campaign, concat(round(ifnull(ifnull(SUM(Target_CTA),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM")
AND ',@filter_cond,' group by 1)
select Campaign,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Campaign = "',IN_AGGREGATION_1,'") as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM") ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(Target_CTA),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM") AND  ',@filter_cond,'  ),0) as bt from cte2  ) as d4)

UNION all

(select * from 

 ( with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Campaign, concat(round(ifnull(ifnull(SUM(CTA_Responder),0) / ifnull(SUM(Target_CTA),0)*100,0),2),"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM")
AND ',@filter_cond,' group by 1)
select Campaign,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Campaign = "',IN_AGGREGATION_1,'") as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM")) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(CTA_Responder),0) / ifnull(SUM(Target_CTA),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM")  AND  ',@filter_cond,'  ),0) as bt from cte2 ) as d5)');


			Select @view_stmt;	 
	 PREPARE stmt3 FROM @view_stmt;
	 EXECUTE stmt3;
	 DEALLOCATE PREPARE stmt3;
	  
	  SELECT CONCAT("[",(GROUP_CONCAT(json_object("Card_Name",`camp`,"Value1",`ofs`,"Value2",`total`,"Value3",`bt`) )),"]")into @temp_result from campaign_deep_dive_Lifts_Card ;
	
end if;
	 
 End If;
 
 IF IN_AGGREGATION_2='CLM' 
THEN 
IF IN_AGGREGATION_1 is NUll or  IN_AGGREGATION_1 =''
Then set @temp_result='[]';
else
   set @view_stmt=concat('create or replace view campaign_deep_dive_Lifts_Card as

 (select * from
(
  with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Campaign, concat(round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") as target FROM 
CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM")
AND ',@filter_cond,' group by 1)
select Campaign,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Campaign = "',IN_AGGREGATION_1,'") as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM") ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM") AND  ',@filter_cond,'  ),0) as bt from cte2 ) as d2)
UNION all
(select * from
(
  with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Campaign, concat(CASE WHEN (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))<=0 THEN 0 ELSE (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))END ,"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM")
AND ',@filter_cond,' group by 1)
select Campaign,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Campaign = "',IN_AGGREGATION_1,'") as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM") ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(CASE WHEN (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))<=0 THEN 0 ELSE (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))END ,"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM") AND  ',@filter_cond,'  ),0) as bt from cte2 ) as d2)

UNION all
(
select * from

 ( with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Campaign, concat(round(ifnull(ifnull(sum(Incremental_Revenue),0) / ifnull(SUM(Target_Revenue),0)*100,0),2),"%") as target  FROM V_Dashboard_MTD_Incr_Rev where  Campaign in (select Campaign from Campaign_Deep_Dive_Display)  and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM" ) AND  ',@filter_cond,'
 group by 1)
select Campaign,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Campaign = "',IN_AGGREGATION_1,'") as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM") ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((SELECt  concat(round(ifnull(ifnull(sum(Incremental_Revenue),0) / ifnull(SUM(Target_Revenue),0)*100,0),2),"%") as target  FROM V_Dashboard_MTD_Incr_Rev where  Campaign in (select Campaign from Campaign_Deep_Dive_Display)  and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM" )  AND  ',@filter_cond,'  ),0)as bt from cte2 ) as d3)

UNION all

(select * from
  (with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Campaign, concat(round(ifnull(ifnull(SUM(Target_CTA),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM")
AND ',@filter_cond,' group by 1)
select Campaign,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Campaign = "',IN_AGGREGATION_1,'") as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM") ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(Target_CTA),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM") AND  ',@filter_cond,'  ),0) as bt from cte2  ) as d4)

UNION all

(select * from 

 ( with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Campaign, concat(round(ifnull(ifnull(SUM(CTA_Responder),0) / ifnull(SUM(Target_CTA),0)*100,0),2),"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM")
AND ',@filter_cond,' group by 1)
select Campaign,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Campaign = "',IN_AGGREGATION_1,'") as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM")) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(CTA_Responder),0) / ifnull(SUM(Target_CTA),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM")  AND  ',@filter_cond,'  ),0) as bt from cte2 ) as d5)');



			Select @view_stmt;	 
	 PREPARE stmt3 FROM @view_stmt;
	 EXECUTE stmt3;
	 DEALLOCATE PREPARE stmt3;
	  
	  SELECT CONCAT("[",(GROUP_CONCAT(json_object("Card_Name",`camp`,"Value1",`ofs`,"Value2",`total`,"Value3",`bt`) )),"]")into @temp_result from campaign_deep_dive_Lifts_Card ;
	
end if;	 
 End If;
 
  IF IN_AGGREGATION_2='THIS' 
THEN   
IF IN_AGGREGATION_1 is NUll or  IN_AGGREGATION_1 =''
Then set @temp_result='[]';
else
 set @view_stmt=concat('create or replace view campaign_deep_dive_Lifts_Card as

 (SELECT "TG Response" as Card_Name ,concat(round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") as `Value1`,"" as Value2,"" as Value3,"TG Response" as `Measure` FROM CLM_Event_Dashboard where ',@filter_cond,' and Campaign = "',IN_AGGREGATION_1,'")

UNION all
(SELECT "TG - CG" as Card_Name, concat(CASE WHEN (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))<=0 THEN 0 ELSE (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))END ,"%") as `Value1`,"" as Value2,"" as Value3,"TG - CG" as `Measure` FROM CLM_Event_Dashboard where ',@filter_cond,' and Campaign = "',IN_AGGREGATION_1,'")
UNION all
(SELECT "Incr. as % of TG Rev." as Card_Name, concat(round(ifnull(ifnull(sum(Incremental_Revenue),0) / ifnull(SUM(Target_Revenue),0)*100,0),2),"%") as `Value1`,"" as Value2,"" as Value3,"Incr. as % of TG Rev." as `Measure` FROM V_Dashboard_MTD_Incr_Rev where ',@filter_cond,' and Campaign = "',IN_AGGREGATION_1,'")


UNION all

(SELECT "CTR" as Card_Name,concat(round(ifnull(ifnull(SUM(Target_CTA),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") as `Value1`,"" as Value2,"" as Value3,"CTR" as `Measure`FROM CLM_Event_Dashboard where ',@filter_cond,'
and Campaign = "',IN_AGGREGATION_1,'")

UNION all

(SELECT "Click Conversions" as Card_Name, concat(round(ifnull(ifnull(SUM(CTA_Responder),0) / ifnull(SUM(Target_CTA),0)*100,0),2),"%") as `Value1`,"" as Value2,"" as Value3,"Click Conversions" as `Measure` FROM CLM_Event_Dashboard where ',@filter_cond,' and Campaign = "',IN_AGGREGATION_1,'")');


			Select @view_stmt;	 
	 PREPARE stmt3 FROM @view_stmt;
	 EXECUTE stmt3;
	 DEALLOCATE PREPARE stmt3;
	  
	  SELECT CONCAT("[",(GROUP_CONCAT(json_object("Card_Name",`Card_Name`,"Value1",`Value1`,"Value2",`Value2`,"Value3",`Value3`) )),"]")into @temp_result from campaign_deep_dive_Lifts_Card ;
	
end if;	 
 End If;
 
  
 
  

 IF @temp_result is null
THEN SET @temp_result ="[]";
End if;

 SET out_result_json = @temp_result;  

END$$
DELIMITER ;
