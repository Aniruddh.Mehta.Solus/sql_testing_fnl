DELIMITER $$
CREATE or replace PROCEDURE `S_UI_Campaign_Paste_Filename`(
IN RUN_MODE Varchar(20),
IN V_ID  BIGINT(20),
IN V_Communication_Channel  VARCHAR(255), 
IN vEnd_CntCust  BIGINT(20),
IN vEvent_Id  BIGINT(20), 
OUT Out_FileName TEXT, 
OUT Out_FileDelimiter VARCHAR(10),
OUT Out_FileMarker TEXT,
OUT Out_File_Header TEXT
)
BEGIN
DECLARE vTempelate TEXT DEFAULT NULL;
DECLARE vTemplate_Original TEXT DEFAULT NULL;
DECLARE V_File_Header TEXT DEFAULT NULL;


SET SQL_SAFE_UPDATES=0;
set foreign_key_checks=0;


SELECT Template into vTemplate_Original FROM Communication_Template where ID=V_ID;
SELECT Header into V_File_Header FROM Communication_Template where ID=V_ID;

SELECT ifnull(Value,'FIRST') into @conf from M_Config where Name = 'Event_Execution_Id_Position';
SET @conf=ifnull(@conf,'FIRST');
IF @conf <> 'LAST' 
    THEN
		SET vTempelate=CONCAT('<SOLUS_PFIELD>Event_Execution_Id</SOLUS_PFIELD>|',vTemplate_Original);
		SET @Out_File_Header = CONCAT('"Event_Execution_ID,',replace(V_File_Header,'|',','),'"');
    ELSE
        SET vTempelate=CONCAT(vTemplate_Original,'|<SOLUS_PFIELD>Event_Execution_Id</SOLUS_PFIELD>');
        SET @Out_File_Header = concat('"',replace(V_File_Header,'|',','),',Event_Executon_ID"');
END IF;

Select  @Out_File_Header into Out_File_Header ;


select Header INTO @vHeader from Communication_Template where  ID = V_ID;
select Timeslot_Id into @vTimeslot_Id from Communication_Template where ID=V_ID LIMIT 1;
set @vTimeslot_Id = IFNULL(@vTimeslot_Id,-1);

Select @Out_File_Header,vTempelate;

-- Select @Out_File_Header,@vHeader ,@vTimeslot_Id ;

if V_Communication_Channel='SMS' then
	SELECT VALUE INTO Out_FileDelimiter FROM M_Config WHERE NAME = 'SMS_File_Delimiter' ;
	elseif (V_Communication_Channel='EMAIL') then
	SELECT VALUE INTO Out_FileDelimiter FROM M_Config WHERE NAME = 'Email_File_Delimiter' ;
	elseif (V_Communication_Channel='PN') then
	SELECT VALUE INTO Out_FileDelimiter FROM M_Config WHERE NAME = 'PN_File_Delimiter' ;
	elseif (V_Communication_Channel='WHATSAPP') then
	SELECT VALUE INTO Out_FileDelimiter FROM M_Config WHERE NAME = 'WhatsApp_File_Delimiter' ;
end if;

-- Select  Out_FileDelimiter;

if @vTimeslot_Id >0 THEN
	select Timeslot_Name into @Timeslot from CLM_Timeslot_Master where Timeslot_Id = @vTimeslot_Id;
    
ELSE
	SET @Timeslot = '_';
    
END IF;


SET @Timeslot = IFNULL(@Timeslot,"_");

-- Select @Timeslot;

SET @is_reminder='N';
SET @is_compliance='N';
SET @ran_last_week='N';
SET @Gaurd_Rail=0;
SET @Reason = '';
SET @Gaurd_Rail_Check = 'N';

SET @File_Path= concat((select value from M_Config where Name='File_Path'),'/CLM_Test_Files');
select ifnull(`Value`,'STANDARD') into @ExecAgency from M_Config where `Name`='Execution_Agency';
Select  @ExecAgency;
IF @ExecAgency='STANDARD' THEN
Select 1;
SELECT concat(@File_Path,'/',
(select value from M_Config where Name='File_Name_Prefix'),'_',
V_Id,'_',
V_Communication_Channel,'_',
(select ExtCreativeKey from Communication_Template where ID=V_ID limit 1),'_',
(select replace(SenderKey,' ','') from Communication_Template where ID=V_ID LIMIT 1),'_',
@Timeslot,'_',
(select Event_Execution_Date_ID from Event_Master_UI A, Event_Master B where A.Event_Id =B.ID and B.Communication_Template_ID = V_ID) ,'_',
vEnd_CntCust) into @FileNameWithoutPrefix;

ELSEIF @ExecAgency='ICS' THEN

SELECT concat(@File_Path,'/',
(select value from M_Config where Name='File_Name_Prefix'),'_',
(select value from M_Config where Name='Tenant_Name'),'_',
vEnd_CntCust,'_',
V_Communication_Channel,'_',
@Timeslot,'_',
(select replace(SenderKey,' ','') from Communication_Template where ID=V_ID LIMIT 1),'_',
V_Id,'_',
(select ExtCreativeKey from Communication_Template where ID=V_ID limit 1),'_',
(select Event_Execution_Date_ID from Event_Master_UI A, Event_Master B where A.Event_Id =B.ID and B.Communication_Template_ID = V_ID)) into @FileNameWithoutPrefix;

ELSEIF @ExecAgency='RESULTICKS' THEN

SELECT concat(@File_Path,'/',
(select value from M_Config where Name='File_Name_Prefix'),'_',
(select value from M_Config where Name='Tenant_Name'),'_',
vEnd_CntCust,'_',
V_Communication_Channel,'_',
(select replace(SenderKey,' ','') from Communication_Template where ID=V_ID LIMIT 1),'_',
@Timeslot,'_',
V_Id,'_',
(select ExtCreativeKey from Communication_Template where ID=V_ID limit 1),'_',
1,'_',
(now(0)+1)) into @FileNameWithoutPrefix;

ELSE

SELECT concat(@File_Path,'/',
(select value from M_Config where Name='File_Name_Prefix'),'_',
V_Id,'_',
V_Communication_Channel,'_',
(select ExtCreativeKey from Communication_Template where ID=V_ID limit 1),'_',
(select replace(SenderKey,' ','') from Communication_Template where ID=V_ID LIMIT 1),'_',
@Timeslot,'_',
(select Event_Execution_Date_ID from Event_Master_UI A, Event_Master B where A.Event_Id =B.ID and B.Communication_Template_ID = V_ID) ,'_',
vEnd_CntCust) into @FileNameWithoutPrefix;

END IF;

-- Select @FileNameWithoutPrefix;


if V_Communication_Channel='SMS' then
	SELECT concat(@FileNameWithoutPrefix,'_',current_time()+0,'.',(select value from M_Config where Name='SMS_File_Extension') ) INTO Out_FileName;
	elseif (V_Communication_Channel='EMAIL') then
	SELECT concat(@FileNameWithoutPrefix,'_',current_time()+0,'.',(select value from M_Config where Name='Email_File_Extension') ) INTO Out_FileName;
	elseif (V_Communication_Channel='PN') then
	SELECT concat(@FileNameWithoutPrefix,'_',current_time()+0,'.',(select value from M_Config where Name='PN_File_Extension') ) INTO Out_FileName;
	elseif (V_Communication_Channel='WHATSAPP') then
	SELECT concat(@FileNameWithoutPrefix,'_',current_time()+0,'.',(select value from M_Config where Name='WHATSAPP_File_Extension') ) INTO Out_FileName;
end if;

-- Select Out_FileName;


	if V_Communication_Channel='SMS' THEN
	SELECT concat(@FileNameWithoutPrefix,'_',current_time()+0,'.',(select value from M_Config where Name='SMS_Marker_File_Extension') ) INTO Out_FileMarker;
		
	END IF;
	
	if V_Communication_Channel='Email' THEN
		SELECT concat(@FileNameWithoutPrefix,'_',current_time()+0,'.',(select value from M_Config where Name='Email_Marker_File_Extension') ) INTO Out_FileMarker;
		
	END IF;
	
	if V_Communication_Channel='PN' THEN
	SELECT concat(@FileNameWithoutPrefix,'_',current_time()+0,'.',(select value from M_Config where Name='PN_Marker_File_Extension') ) INTO Out_FileMarker;
			
	END IF;  
	
	
    if V_Communication_Channel='WHATSAPP' THEN
		SELECT concat(@FileNameWithoutPrefix,'_',current_time()+0,'.',(select value from M_Config where Name='Whatsapp_Marker_File_Extension') ) INTO Out_FileMarker;
		
	END IF;  
    
    -- Select Out_FileMarker;    
	SET @Out_FileName =Out_FileName; 
	Set Out_FileName =CONCAT(@Out_FileName,"~|",Out_FileMarker);
	Select Out_FileName;
	
	SET @Out_File_Header = REPLACE(@Out_File_Header,',',Out_FileDelimiter);
   

	IF V_Communication_Channel='Email'
		THEN
			
			SET @Query_String1 = Concat('Select length(Replace(Substring("',vTempelate,'",1,Locate("<SOLUS_PFIELD>Email_Id</SOLUS_PFIELD>","',vTempelate,'")),"',Out_FileDelimiter,'","")) into @Length1;');
			
			Select @Query_String1;
			PREPARE statement from @Query_String1;
			Execute statement;
			Deallocate PREPARE statement; 
			
			SET @Query_String2 = Concat('Select Replace(Locate("<SOLUS_PFIELD>Email_Id</SOLUS_PFIELD>","',vTempelate,'"),"',Out_FileDelimiter,'","") into @Length2');
			Select @Query_String2;
			PREPARE statement from @Query_String2;
			Execute statement;
			Deallocate PREPARE statement; 
		
	
	ELSE
			SET @Query_String1 = Concat('Select length(Replace(Substring("',vTempelate,'",1,Locate("<SOLUS_PFIELD>Mobile_No</SOLUS_PFIELD>","',vTempelate,'")),"',Out_FileDelimiter,'","")) into @Length1;');
			
			Select @Query_String1;
			PREPARE statement from @Query_String1;
			Execute statement;
			Deallocate PREPARE statement; 
			
			SET @Query_String2 = Concat('Select Replace(Locate("<SOLUS_PFIELD>Mobile_No</SOLUS_PFIELD>","',vTempelate,'"),"',Out_FileDelimiter,'","") into @Length2');
		Select @Query_String2;
			PREPARE statement from @Query_String2;
			Execute statement;
			Deallocate PREPARE statement; 
			
	END IF;

	Select (@Length2-@Length1) into @Place_of_Mobile;
	SET @Query_String3 = Concat('Select length("',vTempelate,'")-length(Replace("',vTempelate,'","',Out_FileDelimiter,'","")) INTO @Number_of_Pipe');
	
	 Select  @Query_String3;	
	PREPARE statement from @Query_String3;
	Execute statement;
	Deallocate PREPARE statement; 
	
	if  @Place_of_Mobile !=0
	Then
	SET @Query_String4 = Concat('Select SUBSTRING_INDEX(SUBSTRING_INDEX(Message,"',Out_FileDelimiter,'",@Place_of_Mobile+1),"',Out_FileDelimiter,'",-1) INTO @Replace_Data from Event_Master_UI where Event_id=',vEvent_Id,'');
		
	SELECT @Query_String4;
	PREPARE statement from @Query_String4;
	Execute statement;
	Deallocate PREPARE statement;
	
	ENd If;
	
	if  @Place_of_Mobile =0
	Then
	SET @Query_String4 = Concat('Select SUBSTRING_INDEX(Message,"',Out_FileDelimiter,'",1) INTO @Replace_Data from Event_Master_UI where Event_id=',vEvent_Id,'');
		
	SELECT @Query_String4;
	PREPARE statement from @Query_String4;
	Execute statement;
	Deallocate PREPARE statement;
	
	ENd If;
	
	
Select @Length1,@Length2,@Place_of_Mobile,Out_FileDelimiter;
	/*
	IF V_Communication_Channel='Email'
		THEN
			IF  @Place_of_Mobile=0 
				THEN
				
				
						SET @Query_String02 = Concat('Create or Replace View Campaign_Test_Output_Files  As
													(
														SELECT "Email_Id","',Out_FileDelimiter,'",',@Out_File_Header,'
														UNION ALL  
														( SELECT Contact_Details,"',Out_FileDelimiter,'",Message 
																FROM Event_Master_UI A,Campaign_UI_Sample_Contact_Details B
																WHERE B.Group_Id=A.Test_Moblie_Number_1 and Event_id=',vEvent_Id,'
															
														)
													);
													
						');
						SET @Query_String2 = Concat('SELECT * INTO OUTFILE "',@Out_FileName,'"  LINES TERMINATED BY "\n" 
													FROM
													(
														SELECT Concat("Email_Id","',Out_FileDelimiter,'"),',@Out_File_Header,'
														UNION ALL  
														( SELECT Contact_Details,"',Out_FileDelimiter,'",Message 
																FROM Event_Master_UI A,Campaign_UI_Sample_Contact_Details B
																WHERE B.Group_Id=A.Test_Moblie_Number_1 and Event_id=',vEvent_Id,'
														)
													)A;
													
						');
			End If;	


	ELSE		
			IF  @Place_of_Mobile=0 
				THEN
				
				
						SET @Query_String02 = Concat('Create or Replace View Campaign_Test_Output_Files  As
													(
														SELECT "Mobile_Number","',Out_FileDelimiter,'",',@Out_File_Header,'
														UNION ALL  
														(	SELECT Contact_Details,"',Out_FileDelimiter,'",Message 
																FROM Event_Master_UI A,Campaign_UI_Sample_Contact_Details B
																WHERE B.Group_Id=A.Test_Moblie_Number_1 and Event_id=',vEvent_Id,'
														)
													);
													
						');
						SET @Query_String2 = Concat('SELECT * INTO OUTFILE "',@Out_FileName,'"  LINES TERMINATED BY "\n" 
													FROM
													(
														SELECT Concat("Mobile_No","',Out_FileDelimiter,'"),',@Out_File_Header,'
														UNION ALL  
														(	SELECT Concat(Contact_Details,"',Out_FileDelimiter,'"),Message 
																FROM Event_Master_UI A,Campaign_UI_Sample_Contact_Details B
																WHERE B.Group_Id=A.Test_Moblie_Number_1 and  Event_id=',vEvent_Id,'
														)
													)A;
													
						');
			End If;	

	END IF;
	
	
	if  @Place_of_Mobile !=0
		THEN
		*/
		
				SET @Query_String02 = Concat('Create or Replace View Campaign_Test_Output_Files  As
											
											(
												SELECT ',@Out_File_Header,'
												UNION ALL  
												( SELECT Replace(Message,"',@Replace_Data,'",Contact_Details)												
														FROM Event_Master_UI A,Campaign_UI_Sample_Contact_Details B
																WHERE B.Group_Id=A.Test_Moblie_Number_1 and  Event_id=',vEvent_Id,'
												)
											);
											
				');
				SET @Query_String2 = Concat('SELECT * INTO OUTFILE "',@Out_FileName,'"  LINES TERMINATED BY "\n" 
											FROM
											(
												SELECT ',@Out_File_Header,'
												UNION ALL  
												( SELECT Replace(Message,"',@Replace_Data,'",Contact_Details)												
														FROM Event_Master_UI A,Campaign_UI_Sample_Contact_Details B
																WHERE B.Group_Id=A.Test_Moblie_Number_1 and Event_id=',vEvent_Id,'
												)
											)A;
											
				');
	-- End If;	
	SELECT @Query_String02;
	PREPARE statement from @Query_String02;
	Execute statement;
	Deallocate PREPARE statement; 
	
	SELECT @Query_String2;
	PREPARE statement from @Query_String2;
	Execute statement;
	Deallocate PREPARE statement; 
	
	-- Select Out_FileMarker;
	SET @Query_String2 =Concat_ws('','SELECT "" INTO OUTFILE "',Out_FileMarker,'" FIELDS TERMINATED BY "|" LINES TERMINATED BY "\n"');
	-- SELECT @Query_String2;
	PREPARE statement from @Query_String2;
	Execute statement;
	Deallocate PREPARE statement; 
	
	
END$$
DELIMITER ;
