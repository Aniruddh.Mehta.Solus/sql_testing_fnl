
CREATE OR REPLACE PROCEDURE `S_CLM_Is_New_Cust_Flag_Reset`()
BEGIN
-- Variable Declaration
DECLARE Load_Exe_ID,Exe_ID bigint(20);
-- Initialization
SET SQL_SAFE_UPDATES=0;
SET Load_Exe_ID = F_Get_Load_Execution_Id();
-- <LOG>
insert into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) values ('S_CLM_Is_New_Cust_Flag_Reset','',now(),null,'Started', Load_Exe_ID);
SELECT concat('S_CLM_Is_New_Cust_Flag_Reset : STARTED ', now(), ' LOAD ID :', Load_Exe_ID) as '';
-- </LOG>

/********************
* MAIN PROGRAM STARTS 
**********************/
-- UPDATE NEW FLAG
UPDATE CDM_Customer_Master set Is_New_Cust_Flag="N" WHERE Is_New_Cust_Flag <> "N";
 
 
-- <LOG>
UPDATE ETL_Execution_Details SET Status = 'Succeeded', End_Time = NOW() WHERE Procedure_Name = 'S_CLM_Is_New_Cust_Flag_Reset' AND Load_Execution_ID = Load_Exe_ID;
SELECT concat('S_CLM_Is_New_Cust_Flag_Reset : COMPLETED ', now(), ' LOAD ID :', Load_Exe_ID) as '';
-- </LOG>
END

