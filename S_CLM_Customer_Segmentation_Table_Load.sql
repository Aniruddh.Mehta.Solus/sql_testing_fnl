
CREATE OR REPLACE PROCEDURE `S_CLM_Customer_Segmentation_Table_Load`()
BEGIN

DECLARE vStart_Cnt BIGINT DEFAULT 0;
DECLARE vEnd_Cnt BIGINT;
DECLARE vBatchSize BIGINT DEFAULT 100000;
DECLARE vStart_Cnt_loop BIGINT DEFAULT 0;
DECLARE vEnd_Cnt_loop BIGINT;


SELECT Customer_Id INTO vStart_Cnt_loop FROM CDM_Customer_Master  ORDER BY Customer_Id ASC LIMIT 1;
SELECT Customer_Id INTO @Rec_Cnt_loop FROM CDM_Customer_Master  ORDER BY Customer_Id DESC LIMIT 1;

PROCESS_LOOP: LOOP
SET vEnd_Cnt_loop = vStart_Cnt_loop + 10000;

insert ignore into CLM_Customer_Segmentation(Customer_id1) select Customer_Id from CDM_Customer_Master
where Customer_Id between vStart_Cnt_loop and vEnd_Cnt_loop;
SET vStart_Cnt_loop = vEnd_Cnt_loop;

IF vStart_Cnt_loop  >= @Rec_Cnt_loop THEN
	LEAVE PROCESS_LOOP;
END IF;        
END LOOP PROCESS_LOOP;  


 
 
END

