DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_performance_homepage`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_MEASURE text; 
    declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
    declare IN_AGGREGATION_4 text;
    declare IN_AGGREGATION_5 text;
    declare IN_AGGREGATION_6 text;
	
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_AGGREGATION_4 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_4"));
    SET IN_AGGREGATION_5 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_5"));
    SET IN_AGGREGATION_6 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_6"));
    
    set @lift_measure_mtd1=0;
    set @lift_revenue_mtd1=0;
    set @lift_measure_ytd1=0;
    set @live_campaigns1=0;
    set @outreaches1=0;
    set @customer_coverage1=0;
    set @engagement_Channel1=0;
    set @personalized1=0;
    set @ctr1=0;
    
    select Value1 into @Incremental_Revenue_Method from UI_Configuration_Global where Config_Name = 'Incremental_Revenue_Method';
    SET @Incremental_Revenue_Method=ifnull(@Incremental_Revenue_Method,'Method2');
    select @Incremental_Revenue_Method;
    IF @Incremental_Revenue_Method = 'METHOD1'
    THEN
		SET @incr_rev_concat='_By_Day';
        SET @group_cond1='Event_Id,EE_Date';
        SET @mtd_concat='';
        
	ELSEIF @Incremental_Revenue_Method = 'Method2'
    THEN
		SET @group_cond1='Event_Id,YM';
		SET @incr_rev_concat='';
        SET @mtd_concat='MTD_';
	ELSE
		SET @incr_rev_concat='';
        SET @mtd_concat='';
		SET @incr_Rev = ' round((CASE WHEN SUM(Control_Base) < 0 or SUM(Target_Delivered) = 0 then 0 WHEN (((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*100 < 0 then 0 WHEN SUM(Control_Base)>0 and SUM(Control_Responder)=0 then SUM(Target_Revenue)
					ELSE ifnull((((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*(SUM(Target_Revenue)/SUM(Target_Responder))*(SUM(Target_Delivered)),0) END),0) ';
		SET @group_cond1='YM';
	END IF;
    SELECT @mtd_concat;
    
   
   SET @sql1=CONCAT('
		CREATE OR REPLACE VIEW CLM_Event_Dashboard_STLT_TEMP
        as 
		SELECT `CLM_Event_Dashboard`.`Revenue Center`,
			`CLM_Event_Dashboard`.`Segment`,
			`CLM_Event_Dashboard`.`Campaign`,
			`CLM_Event_Dashboard`.`EE_Date`,
			`CLM_Event_Dashboard`.`Event_Execution_Date_ID`,
			`CLM_Event_Dashboard`.`Theme`,
			`CLM_Event_Dashboard`.`Offer`,
			`CLM_Event_Dashboard`.`Channel`,
			`CLM_Event_Dashboard`.`Has_Recommendations`,
			`CLM_Event_Dashboard`.`Event_ID`,
			`CLM_Event_Dashboard`.`Control_Base` as `Control_Base`,
			`CLM_Event_Dashboard`.`Target_Base`,
			`CLM_Event_Dashboard`.`Target_Delivered`,
			`CLM_Event_Dashboard`.`Control_Responder` as `Control_Responder`,
			`CLM_Event_Dashboard`.`Target_Responder`,
			`CLM_Event_Dashboard`.`Target_Revenue`,
			`CLM_Event_Dashboard`.`Target_Discount`,
			`CLM_Event_Dashboard`.`Target_Bills`,
			`CLM_Event_Dashboard`.`Target_Open`,
			`CLM_Event_Dashboard`.`Target_CTA`,
			`CLM_Event_Dashboard`.`Control_Revenue`,
			`CLM_Event_Dashboard`.`Control_Discount`,
			`CLM_Event_Dashboard`.`Control_Bills`,
			`CLM_Event_Dashboard`.`Created_Date`,
			`CLM_Event_Dashboard`.`Total_Customer_Base`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Day`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Month`,
			`CLM_Event_Dashboard`.`YM`,
			`CLM_Event_Dashboard`.`Incremental_Revenue',@incr_rev_concat,'` AS `Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue',@incr_rev_concat,'` AS `MTD_Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Segment',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Segment`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Revenue_Center',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Revenue_Center`
		FROM `CLM_Event_Dashboard`;
        ');
        select @sql1;
		PREPARE statement from @sql1;
		Execute statement;
		Deallocate PREPARE statement;
 
	CREATE OR REPLACE VIEW CLM_Event_Dashboard_STLT AS
   select Type_Of_Event AS `Category`,b.Name as `Trigger`, b.CampaignName as `CampaignName`,a.Segment AS `CLMSegmentName`, a.* from CLM_Event_Dashboard_STLT_TEMP a, Event_Master b, Event_Master_UI c where a.Event_ID = b.ID AND b.ID = c.Event_Id;
    
   
   IF IN_AGGREGATION_1 = 'YIELD' AND IN_MEASURE = 'GRAPHBYSEG'
   THEN 
   
    SET @create_view=CONCAT('CREATE OR REPLACE VIEW V_Dashboard_Home_Page_Temp
   as
   select
   `Category`,
   `Trigger`,
   Theme,
   `Channel`,
   `CLMSegmentName`,
   YM,
   EE_Date,
   Event_Id,
   SUM(`Target_Base`) AS Target_Base,
   SUM(`Target_Bills`) AS Target_Bills,
   SUM(`Target_Delivered`) AS Target_Delivered,
   SUM(`Control_Base`) AS Control_Base,
   SUM(`Target_Responder`) AS Target_Responder,
   SUM(`Control_Responder`) AS Control_Responder,
   SUM(Target_Revenue) AS Target_Revenue,
   MAX(MTD_Incremental_Revenue_By_Segment) AS Incremental_Revenue,
   MAX(Total_Revenue_This_Month) AS Total_Revenue_This_Month
   from CLM_Event_Dashboard_STLT
   group by CLMSegmentName, ',@group_cond1,'');
    
    ELSE
    
    SET @create_view=CONCAT('CREATE OR REPLACE VIEW V_Dashboard_Home_Page_Temp
   as
   select
   `Category`,
   `Trigger`,
   Theme,
   `Channel`,
   `CLMSegmentName`,
   YM,
   EE_Date,
   Event_Id,
   SUM(`Target_Base`) AS Target_Base,
   SUM(`Target_Bills`) AS Target_Bills,
   SUM(`Target_Delivered`) AS Target_Delivered,
   SUM(`Control_Base`) AS Control_Base,
   SUM(`Target_Responder`) AS Target_Responder,
   SUM(`Control_Responder`) AS Control_Responder,
   SUM(Target_Revenue) AS Target_Revenue,
   MAX(MTD_Incremental_Revenue) AS Incremental_Revenue,
   MAX(Total_Revenue_This_Month) AS Total_Revenue_This_Month
   from CLM_Event_Dashboard_STLT
   group by ',@group_cond1,'');

	END IF;
    
    select @create_view;
	PREPARE statement from @create_view;
	Execute statement;
	Deallocate PREPARE statement;

	IF IN_AGGREGATION_1 = 'CARDS'
	THEN 
    
        set @view_stmt= CONCAT(' create or replace view V_Dashboard_homepage_graph as 
							select "Outreaches" as "Card_Name",format(ifnull(SUM(Target_Base),0),",") as "Value",date_format(EE_Date,"%b %Y") AS "Date"
                            FROM V_Dashboard_Home_Page_Temp
                            WHERE YM = "',IN_FILTER_CURRENTMONTH,'"
                            UNION
                            select "Conversions" as "Card_Name",format(ifnull(SUM(Target_Bills),0),",") as "Value",date_format(EE_Date,"%b %Y") AS "Date"
                            FROM V_Dashboard_Home_Page_Temp
                            WHERE YM = "',IN_FILTER_CURRENTMONTH,'"
                            UNION
                            select "Lift" as "Card_Name",format(round(ifnull(SUM(Incremental_Revenue),0),0),",") as "Value",date_format(EE_Date,"%b %Y") AS "Date"
                            FROM V_Dashboard_Home_Page_Temp
                            WHERE YM = "',IN_FILTER_CURRENTMONTH,'"
                            UNION
                            select "Yield" as "Card_Name",
                            ROUND(SUM(Incremental_Revenue)/SUM(Target_Delivered),1) as "Value",date_format(EE_Date,"%b %Y") AS "Date"
                            FROM V_Dashboard_Home_Page_Temp
                            WHERE YM = "',IN_FILTER_CURRENTMONTH,'"
                            UNION
                            select "Triggers" as "Card_Name",COUNT(DISTINCT `Trigger`) as "Value",date_format(EE_Date,"%b %Y") AS "Date"
                            FROM V_Dashboard_Home_Page_Temp
                            WHERE YM = "',IN_FILTER_CURRENTMONTH,'"
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Card_Name",`Card_Name`,"Value",`Value`,"Date",`Date`) )),"]")into @temp_result from V_Dashboard_homepage_graph;' ;

END IF;
    
	IF IN_AGGREGATION_1 = 'OUTREACHES' AND IN_MEASURE = 'COVERAGECARD'
	THEN 
    
        set @view_stmt= CONCAT(' create or replace view V_Dashboard_homepage_graph as 
                            select "Coverage" as "Card_Name",ifnull(round((`Total_Distinct_Customers_Targeted_This_Month`/`Total_Customer_Base`)*100,1),0) as "Value1", format(ifnull(MAX(Total_Distinct_Customers_Targeted_This_Month),0),",") AS "Value2"
                            FROM CLM_Event_Dashboard
                            WHERE YM = "',IN_FILTER_CURRENTMONTH,'"
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Card_Name",`Card_Name`,"Value1",`Value1`,"Value2",`Value2`) )),"]")into @temp_result from V_Dashboard_homepage_graph;' ;

END IF;

	IF IN_AGGREGATION_1 = 'OUTREACHES' AND IN_MEASURE = 'GRAPHBYCHANNEL'
	THEN 
    
        SET @view_stmt=concat('CREATE OR REPLACE VIEW Dashboard_Outreaches_Graph AS
						select 
							A.Channel AS "Channel",
							round((A.c1*100)/B.c2,1) as "Value" 
							from 
							(
							(
							SELECT Channel,
							SUM(Target_Base) as c1
							from V_Dashboard_Home_Page_Temp
							WHERE YM = "',IN_FILTER_CURRENTMONTH,'"
							group by 1
							)A
							,
							(
							SELECT SUM(Target_Base) as c2
							from V_Dashboard_Home_Page_Temp
							WHERE YM = "',IN_FILTER_CURRENTMONTH,'"
							)B
							)
							group by A.Channel;
                            
						
                        
                        ');
	SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`Channel`,"Value",`Value`) )),"]")into @temp_result from Dashboard_Outreaches_Graph;' ;
    
	END IF;

	IF IN_AGGREGATION_1 = 'OUTREACHES' AND IN_MEASURE = 'GRAPHYEAR'
	THEN 
    
		SET @filter_cond = concat(' YM =',IN_FILTER_CURRENTMONTH);
		SET @date_select = ' date_format(EE_DATE,"%d-%b") AS "Year" ';
        
        select @filter_cond, @date_select;
    
        SET @view_stmt=concat('CREATE OR REPLACE VIEW Dashboard_Outreaches_Graph AS
						(SELECT DAY(EE_Date) AS `Year`,
                         SUM(TARGET_BASE) as "Value"
                         from CLM_Event_Dashboard
                         WHERE ',@filter_cond,'
                         GROUP BY 1
                         ORDER BY 1 ASC
                        )');
	SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`Year`,"Value",`Value`) )),"]")into @temp_result from Dashboard_Outreaches_Graph;' ;
    
	END IF;
	
	IF IN_AGGREGATION_1 = 'OUTREACHES' AND IN_MEASURE = 'GRAPHBYCAT'
	THEN 
    
        SET @view_stmt=concat('CREATE OR REPLACE VIEW Dashboard_Outreaches_Graph AS
						(SELECT `Category`,
                         SUM(Target_Base) as "Value"
                         from V_Dashboard_Home_Page_Temp
                         WHERE YM = "',IN_FILTER_CURRENTMONTH,'"
                         GROUP BY 1
                        )');
	SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`Category`,"Value",`Value`) )),"]")into @temp_result from Dashboard_Outreaches_Graph;' ;
    
	END IF;
    
	IF IN_AGGREGATION_1 = 'CONVERSIONS' AND IN_MEASURE = 'CARD'
	THEN 
    
        set @view_stmt= CONCAT(' create or replace view V_Dashboard_homepage_graph as 
							select "Conversions" as "Card_Name",SUM(Target_Bills) as "Value"
                            FROM V_Dashboard_Home_Page_Temp
                            WHERE YM = "',IN_FILTER_CURRENTMONTH,'";
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Card_Name",`Card_Name`,"Value",`Value`) )),"]")into @temp_result from V_Dashboard_homepage_graph;' ;

END IF;

	IF IN_AGGREGATION_1 = 'CARDPERCENT' 
	THEN 
    
		SET @Interval_month = CASE WHEN IN_AGGREGATION_2='M' THEN 0
							WHEN IN_AGGREGATION_2='L3M' THEN 2
                            WHEN IN_AGGREGATION_2='L12M' THEN 11 END;
                            
    
    
	SET @FY_SATRT_DATE = date_format(date_sub(concat(IN_FILTER_CURRENTMONTH,'01'), INTERVAL @Interval_month MONTH),"%Y%m");
	SET @FY_END_DATE = IN_FILTER_CURRENTMONTH;
    
    SELECT @FY_SATRT_DATE,@FY_END_DATE;
    SET @filter_cond=concat(" YM between '",@FY_SATRT_DATE,"' and '",@FY_END_DATE,"'");
    
    SET @Start_Date = date_format(date_sub(concat(date_format(current_date(),"%Y%m"),'01'), INTERVAL 15 MONTH),"%Y%m");
	SET @End_Date = date_format(current_date(),"%Y%m");
    
        set @view_stmt= CONCAT(' create or replace view V_Dashboard_homepage_graph as 
							select "Conversions" as "Card_Name",
                            ROUND(SUM(Target_Bills)*100/SUM(Target_Base),1) as "Value"
                            FROM V_Dashboard_Home_Page_Temp
                            WHERE ',@filter_cond,'
                            UNION
                            select "Yield" as "Card_Name",ROUND(SUM(Incremental_Revenue)/SUM(Target_Delivered),1) as "Value"
                            FROM V_Dashboard_Home_Page_Temp
                            WHERE ',@filter_cond,';
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Card_Name",`Card_Name`,"Value",`Value`) )),"]")into @temp_result from V_Dashboard_homepage_graph;' ;

END IF;

	IF IN_AGGREGATION_1 = 'CONVERSIONS' AND IN_MEASURE = 'GRAPHBYCAT'
	THEN 
    
        SET @view_stmt=concat('CREATE OR REPLACE VIEW Dashboard_Outreaches_Graph AS
						(SELECT `Category`,
                         ROUND(SUM(Target_bills)*100/SUM(Target_Base),1) as "Value"
                         from V_Dashboard_Home_Page_Temp
                         WHERE YM = "',IN_FILTER_CURRENTMONTH,'"
                         GROUP BY 1
                        )');
	SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`Category`,"Value",`Value`) )),"]")into @temp_result from Dashboard_Outreaches_Graph;' ;
    
	END IF;
    
	IF IN_AGGREGATION_1 = 'CONVERSIONS' AND IN_MEASURE = 'GRAPH'
	THEN 
    
        SET @view_stmt=concat('CREATE OR REPLACE VIEW Dashboard_Outreaches_Graph AS
						(SELECT SUM(Target_Responder) AS Customers, SUM(Target_Revenue)/SUM(Target_Responder) AS ATV, SUM(Target_Revenue) AS Amt
                         from CLM_Event_Dashboard
                         WHERE YM = "',IN_FILTER_CURRENTMONTH,'"
                        )');
                        
	SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Customers",FORMAT(`Customers`,","),"ATV",FORMAT(ROUND(`ATV`,0),","),"Amt",FORMAT(`Amt`,",")) )),"]")into @temp_result from Dashboard_Outreaches_Graph;' ;
    
	END IF;
    
	IF IN_AGGREGATION_1 = 'LIFT' AND IN_MEASURE = 'CARD'
	THEN 
    
        set @view_stmt= CONCAT(' create or replace view V_Dashboard_homepage_graph as 
							select "Lift" as "Card_Name",SUM(Incremental_Revenue) as "Value"
                            FROM V_Dashboard_Home_Page_Temp
                            WHERE YM = "',IN_FILTER_CURRENTMONTH,'";
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Card_Name",`Card_Name`,"Value",`Value`) )),"]")into @temp_result from V_Dashboard_homepage_graph;' ;

END IF;

	IF IN_AGGREGATION_1 = 'LIFT' AND IN_MEASURE = 'PIE_CHART'
	THEN 

        SET @view_stmt=concat('CREATE OR REPLACE VIEW Dashboard_Outreaches_Graph AS
						
							SELECT 
                            A.Category,
							round((A.c1*100)/B.c2,1) as "Value" 
							from 
							(
							(
							SELECT Category,
							SUM(Incremental_Revenue) as c1
							from V_Dashboard_Home_Page_Temp
							WHERE YM = "',IN_FILTER_CURRENTMONTH,'"
							group by 1
							)A
							,
							(
							SELECT SUM(Incremental_Revenue) as c2
							from V_Dashboard_Home_Page_Temp
							WHERE YM = "',IN_FILTER_CURRENTMONTH,'"
							)B
							)
							group by 1;
                        ');
                        
					
	SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("name",`Category`,"value",`Value`) )),"]")into @temp_result from Dashboard_Outreaches_Graph;' ;
    
	END IF;
    
    IF IN_AGGREGATION_1 = 'LIFT' AND IN_MEASURE = 'CARDPERCENT'
	THEN 
                            
    SET @FY_SATRT_DATE = date_format(date_sub(concat(IN_FILTER_CURRENTMONTH,'01'), INTERVAL 3 MONTH),"%Y%m");
	 SET @FY_END_DATE = IN_FILTER_CURRENTMONTH;
    
    SELECT @FY_SATRT_DATE,@FY_END_DATE;
    SET @filter_cond=concat(" YM between '",@FY_SATRT_DATE,"' and '",@FY_END_DATE,"'");
    SET @date_select = ' date_format(EE_DATE,"%d-%b") AS "Year" ';
        
        select @filter_cond, @date_select;
    
        set @view_stmt= CONCAT(' create or replace view V_Dashboard_homepage_graph as 
							select ',@date_select,', SUM(Incremental_Revenue) as "Value"
                            FROM V_Dashboard_Home_Page_Temp
                            WHERE ',@filter_cond,';
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`Year`,"Value",`Value`) )),"]")into @temp_result from V_Dashboard_homepage_graph;' ;

	END IF;
    
    IF IN_AGGREGATION_1 = 'LIFT' AND IN_MEASURE = 'LIFTCARDS'
	THEN 
    
        set @view_stmt= CONCAT(' create or replace view V_Dashboard_homepage_graph as 
							select "TG Rev" as "Card_Name", CASE WHEN SUM(Target_Revenue) = 0 THEN 0 ELSE round((SUM(Incremental_Revenue)/MAX(Total_Revenue_This_Month))*100,2) END as "Value"
                            FROM V_Dashboard_Home_Page_Temp
                            WHERE YM = "',IN_FILTER_CURRENTMONTH,'"
                            UNION
                            select "Total Sales" as "Card_Name", CASE WHEN SUM(Target_Revenue) = 0 THEN 0 ELSE round((SUM(Incremental_Revenue)/SUM(Target_Revenue))*100,2) END as "Value"
                            FROM V_Dashboard_Home_Page_Temp
                            WHERE YM = "',IN_FILTER_CURRENTMONTH,'";
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Card_Name",`Card_Name`,"Value",`Value`) )),"]")into @temp_result from V_Dashboard_homepage_graph;' ;

END IF;

	IF IN_AGGREGATION_1 = 'LIFT' AND IN_MEASURE = 'LINEGRAPH'
	THEN 
    
		SELECT DATE_FORMAT(DATE_SUB(REPLACE(STR_TO_DATE(IN_FILTER_CURRENTMONTH, '%Y%m'), '00', '01'), INTERVAL 3 MONTH), '%Y%m') INTO @FY_SATRT_DATE;
		SET @FY_END_DATE=IN_FILTER_CURRENTMONTH;
        
        SET @filter_cond=concat(" YM between '",@FY_SATRT_DATE,"' and '",@FY_END_DATE,"'");
		SET @date_select=' date_format(EE_DATE,"%b-%Y") AS "Year" ';

        SET @view_stmt=concat('CREATE OR REPLACE VIEW Dashboard_Outreaches_Graph AS
								SELECT ',@date_select,', CASE WHEN SUM(Target_Revenue) = 0 THEN 0 ELSE round((SUM(Incremental_Revenue)/MAX(Total_Revenue_This_Month))*100,2) END as "Value"
								FROM V_Dashboard_Home_Page_Temp
								WHERE ',@filter_cond,'
								GROUP BY 1
								ORDER BY EE_Date;
                        ');
                        
					
	SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`Year`,"Value",`Value`) )),"]")into @temp_result from Dashboard_Outreaches_Graph;' ;
    
	END IF;
    
    IF IN_AGGREGATION_1 = 'LIFT' AND IN_MEASURE = 'TABLE'
	THEN 
    
		SELECT DATE_FORMAT(DATE_SUB(REPLACE(STR_TO_DATE(IN_FILTER_CURRENTMONTH, '%Y%m'), '00', '01'), INTERVAL 3 MONTH), '%Y%m') INTO @FY_SATRT_DATE;
		SET @FY_END_DATE=IN_FILTER_CURRENTMONTH;
        
        SET @filter_cond=concat(" YM between '",@FY_SATRT_DATE,"' and '",@FY_END_DATE,"'");
		SET @date_select=' date_format(EE_DATE,"%b-%Y") AS "Year" ';

        SET @view_stmt=concat('CREATE OR REPLACE VIEW Dashboard_Outreaches_Graph AS
							SELECT ',@date_select,',
                         FORMAT(SUM(Incremental_Revenue),",") as "Value",
                         MAX(YM) as YM
                         from V_Dashboard_Home_Page_Temp
                         WHERE ',@filter_cond,'
                         GROUP BY 1
                         ORDER BY EE_Date;');
                        
					
	SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Year",`Year`,"Value",`Value`) )),"]")into @temp_result from Dashboard_Outreaches_Graph;' ;
    
	END IF;
    
	IF IN_AGGREGATION_1 = 'YIELD' AND IN_MEASURE = 'CARD'
	THEN 
    
        set @view_stmt= CONCAT(' create or replace view V_Dashboard_homepage_graph as 
							select "YEILD" as "Card_Name",
                            ROUND(SUM(Incremental_Revenue)/SUM(Target_Delivered),1) as "Value"
                            FROM V_Dashboard_Home_Page_Temp
                            WHERE YM = "',IN_FILTER_CURRENTMONTH,'";
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Card_Name",`Card_Name`,"Value",`Value`) )),"]")into @temp_result from V_Dashboard_homepage_graph;' ;

END IF;
    
    IF IN_AGGREGATION_1 = 'YIELD' AND IN_MEASURE = 'GRAPHBYCAT'
	THEN 
    
        set @view_stmt= CONCAT(' create or replace view V_Dashboard_homepage_graph as 
							select `Category`,
                            ROUND(SUM(Incremental_Revenue)/SUM(Target_Delivered),1) as "Value"
                            FROM V_Dashboard_Home_Page_Temp
                            WHERE YM = "',IN_FILTER_CURRENTMONTH,'"
                            GROUP BY `Category`;
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`Category`,"Value",`Value`) )),"]")into @temp_result from V_Dashboard_homepage_graph;' ;

END IF;
    
    IF IN_AGGREGATION_1 = 'YIELD' AND IN_MEASURE = 'YIELDCARDS'
	THEN 
		SET @Interval_month = CASE WHEN IN_AGGREGATION_2='M' THEN 0
							WHEN IN_AGGREGATION_2='L3M' THEN 2
                            WHEN IN_AGGREGATION_2='L12M' THEN 11 END;
    
	SET @FY_SATRT_DATE = date_format(date_sub(concat(IN_FILTER_CURRENTMONTH,'01'), INTERVAL @Interval_month MONTH),"%Y%m");
	SET @FY_END_DATE = IN_FILTER_CURRENTMONTH;
    
    SELECT @FY_SATRT_DATE,@FY_END_DATE;
    SET @filter_cond=concat(" YM between '",@FY_SATRT_DATE,"' and '",@FY_END_DATE,"'");
    
        set @view_stmt= CONCAT(' create or replace view V_Dashboard_homepage_graph as 
							select ROUND(SUM(Incremental_Revenue)/SUM(Target_Delivered),1) as "Value"
                            FROM V_Dashboard_Home_Page_Temp
                            WHERE ',@filter_cond,';
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Value",`Value`) )),"]")into @temp_result from V_Dashboard_homepage_graph;' ;

END IF;
    
	IF IN_AGGREGATION_1 = 'YIELD' AND IN_MEASURE = 'GRAPHBYCHANNEL'
	THEN 
    
        set @view_stmt= CONCAT(' create or replace view V_Dashboard_homepage_graph as 
							select `Channel`,
                            ROUND(SUM(Incremental_Revenue)/SUM(Target_Delivered),1) as "Value"
                            FROM V_Dashboard_Home_Page_Temp
                            WHERE YM = "',IN_FILTER_CURRENTMONTH,'"
                            GROUP BY `Channel`;
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`Channel`,"Value",`Value`) )),"]")into @temp_result from V_Dashboard_homepage_graph;' ;

END IF;

	IF IN_AGGREGATION_1 = 'YIELD' AND IN_MEASURE = 'GRAPHBYSEG'
	THEN 
    
        set @view_stmt= CONCAT(' create or replace view V_Dashboard_homepage_graph as 
							select  `CLMSegmentName`,
                            ROUND(SUM(Incremental_Revenue)/SUM(Target_Delivered),1) as "Value"
                            FROM V_Dashboard_Home_Page_Temp
                            WHERE YM = "',IN_FILTER_CURRENTMONTH,'"
                            GROUP BY `CLMSegmentName`;
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`CLMSegmentName`,"Value",`Value`) )),"]")into @temp_result from V_Dashboard_homepage_graph;' ;

END IF;
    
    IF IN_AGGREGATION_1 = 'TRIGGER' AND IN_MEASURE = 'CARD'
	THEN 
    
        set @view_stmt= CONCAT(' create or replace view V_Dashboard_homepage_graph as 
							select  "Card_Name",
                            count(DISTINCT(`Trigger`)) as "Value"
                            FROM V_Dashboard_Home_Page_Temp
                            WHERE YM = "',IN_FILTER_CURRENTMONTH,'";
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Card_Name",`Card_Name`,"Value",`Value`) )),"]")into @temp_result from V_Dashboard_homepage_graph;' ;

END IF;
    
    IF IN_AGGREGATION_1 = 'TRIGGER' AND IN_MEASURE = 'PIE_CHART'
	THEN 
    
        set @view_stmt= CONCAT(' create or replace view V_Dashboard_homepage_graph as 
							select  `Category` as "Card_Name",
                            count(DISTINCT(`Trigger`)) as "Value"
                            FROM V_Dashboard_Home_Page_Temp
                            WHERE YM = "',IN_FILTER_CURRENTMONTH,'"
                            GROUP BY  `Category`;
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("name",`Card_Name`,"value",`Value`) )),"]")into @temp_result from V_Dashboard_homepage_graph;' ;

END IF;
     
	IF IN_AGGREGATION_1 = 'TRIGGER' AND IN_MEASURE = 'TRIGGERANDLIFTTABLE'
	THEN 
    
        set @view_stmt= CONCAT(' create or replace view V_Dashboard_homepage_graph as 
							select DISTINCT(`Trigger`) as "Trigger",
                            ROUND(SUM(Incremental_Revenue),0) as "Lift"
                            FROM V_Dashboard_Home_Page_Temp
                            WHERE YM = "',IN_FILTER_CURRENTMONTH,'"
                            GROUP BY 1
                            ORDER BY 2 DESC
                            LIMIT 5;
							');
     
		 SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Trigger",`Trigger`,"Lift",CAST(format(ifnull(`Lift`,0),",") AS CHAR)) )),"]")into @temp_result from V_Dashboard_homepage_graph;' ;

END IF;
    
    IF IN_AGGREGATION_1 = 'TRIGGER' AND IN_MEASURE = 'TRIGGERANDYIELDTABLE'
	THEN 
    
        set @view_stmt= CONCAT(' create or replace view V_Dashboard_homepage_graph as 
							select DISTINCT(`Trigger`) as "Trigger",
							ROUND(SUM(Incremental_Revenue)/SUM(Target_Delivered),1) as "Yield"
                            FROM V_Dashboard_Home_Page_Temp
                            WHERE YM = "',IN_FILTER_CURRENTMONTH,'"
                            GROUP BY  1
                            ORDER BY 2 DESC
                            LIMIT 5;
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Trigger",`Trigger`,"Yield",`Yield`) )),"]")into @temp_result from V_Dashboard_homepage_graph;' ;

END IF;
    
    if (@customer_coverage1 is null OR @customer_coverage1='') then
      SET @customer_coverage1='0';  
      end if; 
      
IF IN_SCREEN <> "DOWNLOAD"
    THEN
    select @view_stmt;
	PREPARE statement from @view_stmt;
	Execute statement;
	Deallocate PREPARE statement;
	SELECT @Query_String;
	PREPARE statement from @Query_String;
	Execute statement;
	Deallocate PREPARE statement; 
    SET out_result_json = @temp_result;
    ELSE
		call S_dashboard_performance_download('V_Dashboard_Insights_Conversions',@out_result_json1);
		SELECT @out_result_json1 INTO out_result_json;
    END IF;
    
END$$
DELIMITER ;
