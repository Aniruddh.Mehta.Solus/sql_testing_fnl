DELIMITER $$
CREATE or replace PROCEDURE `PROC_UI_CU_CLM_RegionSegment`(
	IN vUserId BIGINT,
	IN vRegionSegmentName VARCHAR(128),
	IN vRegionSegmentDesc VARCHAR(1024),
	IN vRegionReplacedQueryAsInUI TEXT,
	IN vRegionReplacedQuery TEXT,
	IN vIsValidSVOCCondition TINYINT,
	IN vRegionReplacedQueryBillAsInUI TEXT,
	IN vRegionReplacedQueryBill TEXT,
	IN vIsValidBillCondition TINYINT,
	INOUT vRegionSegmentId BIGINT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN
	DECLARE vResult TINYINT;
	DECLARE vInputValues TEXT;
	
	SET vSuccess = 1;
	SET vErrMsg = '';

	SET vInputValues  = CONCAT(
			'UserId: ', IFNULL(vUserId,'NULL VALUE')
			,' | RegionSegmentName: ',IFNULL(vRegionSegmentName,'NULL VALUE')
			,' | RegionSegmentDesc: ',IFNULL(vRegionSegmentDesc,'NULL VALUE')
			,' | RSReplacedQueryAsInUI: ',IFNULL(vRegionReplacedQueryAsInUI,'NULL VALUE')
			,' | RSReplacedQuery: ',IFNULL(vRegionReplacedQuery,'NULL VALUE')
			,' | IsValidSVOCCondition: ',IFNULL(vIsValidSVOCCondition,'NULL VALUE')
			,' | RSReplacedQueryBillAsInUI: ',IFNULL(vRegionReplacedQueryBillAsInUI,'NULL VALUE')
			,' | RSReplacedQueryBill: ',IFNULL(vRegionReplacedQueryBill,'NULL VALUE')
			,' | IsValidBillCondition: ',IFNULL(vIsValidBillCondition,'NULL VALUE')
		);

	IF vUserId IS NULL OR vUserId <= 0 THEN
		SET vErrMsg = CONCAT('User Id cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vRegionSegmentName IS NULL OR vRegionSegmentName = '' THEN
		SET vErrMsg = CONCAT('Regionsegment Name cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vSuccess = 1 THEN 
	CALL PROC_CLM_CU_CLM_RegionSegment(
			vUserId,
			vRegionSegmentName,
		IFNULL(vRegionSegmentDesc, ''),
		IFNULL(vRegionReplacedQueryAsInUI, ''),
		IFNULL(vRegionReplacedQuery, ''),
		IFNULL(vIsValidSVOCCondition, 0),
		IFNULL(vRegionReplacedQueryBillAsInUI, ''),
		IFNULL(vRegionReplacedQueryBill, ''),
		IFNULL(vIsValidBillCondition, 0),
		vRegionSegmentId,
		vErrMsg,
		vSuccess
	);
	END IF;
	IF vSuccess = 0 THEN 
		SET vErrMsg = CONCAT(vErrMsg , '\r\n', vInputValues);	
	END IF;

END$$
DELIMITER ;


DELIMITER $$
CREATE or replace PROCEDURE `PROC_CLM_CU_CLM_RegionSegment`(
	IN vUserId BIGINT, 
	IN vRegionSegmentName VARCHAR(128), 
	IN vRegionSegmentDesc VARCHAR(1024), 
	IN vRegionReplacedQueryAsInUI TEXT,
	IN vRegionReplacedQuery TEXT,
	IN vIsValidSVOCCondition TINYINT,
	IN vRegionReplacedQueryBillAsInUI TEXT,
	IN vRegionReplacedQueryBill TEXT,
	IN vIsValidBillCondition TINYINT,
	INOUT vRegionSegmentId BIGINT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN

	DECLARE vRowCnt BIGINT DEFAULT 0;
	DECLARE vSqlstmt TEXT;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN 
		SET vSuccess = 0; 
		GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
		SET vErrMsg = CONCAT(IFNULL(vErrMsg,''), "ERROR ", @errno, " (", @sqlstate, "): ", @text);
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 0, vErrMsg, 0);
	END;

	SET vErrMsg = 'Start';
	SET vSuccess = 0;
	SET @CURR_PROC_NAME = 'PROC_CLM_CU_CLM_RegionSegment';
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 1, vErrMsg, vSuccess);

	IF vRegionSegmentName IS NULL OR vRegionSegmentName = '' THEN
		SET vErrMsg = 'RegionSegment Name is a Mandatory Field and cannot be empty or NULL';
		SET vSuccess = 0;
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 2, vErrMsg, vSuccess);
	ELSE
		IF vRegionSegmentId IS NULL OR vRegionSegmentId <= 0 THEN
			SET vSqlstmt = CONCAT(
				'INSERT IGNORE INTO CLM_RegionSegment (
					RegionSegmentName, 
					RegionSegmentDesc,
					RegionReplacedQueryAsInUI,
					RegionReplacedQuery,
					IsValidSVOCCondition,
					RegionReplacedQueryBillAsInUI,
					RegionReplacedQueryBill,
					IsValidBillCondition,
					CreatedBy, 
					ModifiedBy
				) VALUES ('
				,'"', vRegionSegmentName,'"'
				,', "', vRegionSegmentDesc,'"'
				,', "', vRegionReplacedQueryAsInUI,'"'
				,', "', vRegionReplacedQuery,'"'
				,', ', vIsValidSVOCCondition
				,', "', vRegionReplacedQueryBillAsInUI,'"'
				,', "', vRegionReplacedQueryBill,'"'
				,', ', vIsValidBillCondition
				,', ', vUserId
				,', ', vUserId
				, ')'
			);

			SET vErrMsg = vSqlstmt;
			SET @sql_stmt = vSqlstmt;
			PREPARE EXEC_STMT FROM @sql_stmt;
			EXECUTE EXEC_STMT;
			DEALLOCATE PREPARE EXEC_STMT;
			
			CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 3, vErrMsg, vSuccess);
		ELSE
				
			
			
				SET vSqlstmt = CONCAT(
					'INSERT IGNORE INTO CLM_RegionSegment (
						RegionSegmentId,
						RegionSegmentName, 
						RegionSegmentDesc,
						RegionReplacedQueryAsInUI,
						RegionReplacedQuery,
						IsValidSVOCCondition,
						RegionReplacedQueryBillAsInUI,
						RegionReplacedQueryBill,
						IsValidBillCondition,
						CreatedBy, 
						ModifiedBy
					) VALUES ('
					, vRegionSegmentId
					,', "', vRegionSegmentName,'"'
					,', "', vRegionSegmentDesc,'"'
					,', "', vRegionReplacedQueryAsInUI,'"'
					,', "', vRegionReplacedQuery,'"'
					,', ', vIsValidSVOCCondition
					,', "', vRegionReplacedQueryBillAsInUI,'"'
					,', "', vRegionReplacedQueryBill,'"'
					,', ', vIsValidBillCondition
					,', ', vUserId
					,', ', vUserId
					,') ON DUPLICATE KEY UPDATE '
					, '	RegionSegmentDesc = "',vRegionSegmentDesc, '"'
					,',	RegionReplacedQueryAsInUI = "',vRegionReplacedQueryAsInUI, '"'
					,',	RegionReplacedQuery = "',vRegionReplacedQuery, '"'
					,',	IsValidSVOCCondition = ', vIsValidSVOCCondition
					,',	RegionReplacedQueryBillAsInUI = "',vRegionReplacedQueryBillAsInUI, '"'
					,',	RegionReplacedQueryBill = "',vRegionReplacedQueryBill, '"'
					,',	IsValidBillCondition = ', vIsValidBillCondition
					,',	CreatedBy = ', vUserId
					,',	ModifiedBy = ', vUserId
					,';'
				);
				
				SET vErrMsg = vSqlstmt;

				SET @sql_stmt = vSqlstmt;
				PREPARE EXEC_STMT FROM @sql_stmt;
				EXECUTE EXEC_STMT;
				DEALLOCATE PREPARE EXEC_STMT;
				CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 4, vErrMsg, vSuccess);
		
		END IF;
	END IF;
	
	SELECT RegionSegmentId INTO vRegionSegmentId 
	FROM CLM_RegionSegment
	WHERE RegionSegmentName = vRegionSegmentName
	LIMIT 1;
 
	SET vSuccess = 1;
	
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 6, vErrMsg, vSuccess);

END$$
DELIMITER ;
