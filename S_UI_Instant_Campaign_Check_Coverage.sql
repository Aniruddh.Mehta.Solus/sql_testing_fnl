
CREATE or replace PROCEDURE `S_UI_Instant_Campaign_Check_Coverage`(IN in_request_json json, OUT out_result_json json)
BEGIN
    
    

    declare IN_USER_NAME varchar(240); 
    declare IN_SCREEN varchar(400); 
    declare vSuccess  varchar(400); 
    declare vErrMsg  varchar(400);
    declare vSQLCondition varchar(400);
    declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text; 
	declare IN_AGGREGATION_3 text;
    declare Comm_Channel varchar(200);
    declare Coverage_Level varchar(200);
  
    declare Campaign_Segment varchar(128);
    declare Lifecycle_Segment varchar(400);
  
  
    Set @Err =0;
   
   
   
    SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    SET Lifecycle_Segment = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1")); 
    SET Campaign_Segment = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2")); 
    SET Comm_Channel = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3")); 
    SET Coverage_Level = json_unquote(json_extract(in_request_json,"$.AGGREGATION_4"));
	set Lifecycle_Segment = concat('LC_',Lifecycle_Segment);
    set Campaign_Segment = concat('CM_',Campaign_Segment);
    SET @temp_result = Null;
   
   
   
 if Coverage_Level = 'Total'
 then
	 if Comm_Channel = 'SMS' or Comm_Channel = 'WhatsApp' or Comm_Channel = 'Email'
     then 
     
select Convert(format(count(1),","),CHAR)  into @c1 from CLM_Customer_Segmentation;


	                    Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Count",@c1) )),"]") into @temp_result;') ;
	                                   
	                    SELECT @Query_String;
	                    PREPARE statement from @Query_String;
	                    Execute statement;
	                    Deallocate PREPARE statement; 


	             End If;
       
          
elseif Coverage_Level = 'Lifecycle'
	then 
                 
		if Comm_Channel = 'SMS' or Comm_Channel = 'WhatsApp' or Comm_Channel = 'Email'
		then 
     
set @query1=concat('select Convert(format(count(1),","),CHAR)  into @c2 from CLM_Customer_Segmentation where ',Lifecycle_Segment,' = 1;');

	                    
	                    PREPARE stmt FROM @query1;
	                    EXECUTE stmt; 
	                    DEALLOCATE PREPARE stmt;
			
	                    Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Count",@c2) )),"]") into @temp_result;') ;
	                                   
	                    SELECT @Query_String;
	                    PREPARE statement from @Query_String;
	                    Execute statement;
	                    Deallocate PREPARE statement; 

end if;
                 
                 
elseif Coverage_Level = 'Campaign'
   then
                 
   if Comm_Channel = 'SMS' or Comm_Channel = 'WhatsApp' or Comm_Channel = 'Email'
     then 
     


set @query2=concat('select Convert(format(count(1),","),CHAR)  into @c3 from CLM_Customer_Segmentation where ',Lifecycle_Segment,' = 1 and ',Campaign_Segment,' = 1;');

	                 
                        PREPARE stmt FROM @query2;
	                    EXECUTE stmt; 
	                    DEALLOCATE PREPARE stmt;

	                    Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Count",@c3) )),"]") into @temp_result;') ;
	                                   
	                    SELECT @Query_String;
	                    PREPARE statement from @Query_String;
	                    Execute statement;
	                    Deallocate PREPARE statement;  


	             End If;
                 
                 
elseif Coverage_Level = 'Consent'
	then
                 
 if Comm_Channel = 'SMS'
     then 
     

set @query3=concat('select Convert(format(count(1),","),CHAR)  into @c4 from CLM_Customer_Segmentation where ',Lifecycle_Segment,' = 1 and ',Campaign_Segment,' = 1 and SMS_Consent_Flag = 0;');
	                    
	                    
	                   
                        PREPARE stmt FROM @query3;
	                    EXECUTE stmt; 
	                    DEALLOCATE PREPARE stmt;

	                    Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Count",@c4) )),"]") into @temp_result;') ;
	                                   
	                    SELECT @Query_String;
	                    PREPARE statement from @Query_String;
	                    Execute statement;
	                    Deallocate PREPARE statement; 

elseif  Comm_Channel = 'WhatsApp'
then
 
set @query3=concat('select Convert(format(count(1),","),CHAR)  into @c4 from CLM_Customer_Segmentation where ',Lifecycle_Segment,' = 1 and ',Campaign_Segment,' = 1 and WhatsApp_Consent_Flag = 0;');
	                    
	                    
	                   
                        PREPARE stmt FROM @query3;
	                    EXECUTE stmt; 
	                    DEALLOCATE PREPARE stmt;

	                    Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Count",@c4) )),"]") into @temp_result;') ;
	                                   
	                    SELECT @Query_String;
	                    PREPARE statement from @Query_String;
	                    Execute statement;
	                    Deallocate PREPARE statement; 


ELSEIF Comm_Channel = 'Email'
then

set @query3=concat('select Convert(format(count(1),","),CHAR)  into @c4 from CLM_Customer_Segmentation where ',Lifecycle_Segment,' = 1 and ',Campaign_Segment,' = 1 and Email_Consent_Flag = 0;');
	                    
	                    
	                   
                        PREPARE stmt FROM @query3;
	                    EXECUTE stmt; 
	                    DEALLOCATE PREPARE stmt;


	                    Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Count",@c4) )),"]") into @temp_result;') ;
	                                   
	                    SELECT @Query_String;
	                    PREPARE statement from @Query_String;
	                    Execute statement;
	                    Deallocate PREPARE statement; 
                        

	              ELSE
	                          Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Invalid Parameters:-Please Select Channel") )),"]") into @temp_result                       
	                                                                                  ;') ;
	                                   
	                           SELECT @Query_String;
	                           PREPARE statement from @Query_String;
	                           Execute statement;
	                           Deallocate PREPARE statement; 
	                                                


 End If;
                 
                 elseif Coverage_Level = 'Valid_Details'
                 then
                 
                 
                 	 if Comm_Channel = 'SMS'
     then 
    

set @query4=concat('select Convert(format(count(1),","),CHAR)   INTO @c5  from ( select CUstomer_Id1 from CLM_Customer_Segmentation a, CDM_Customer_PII_Master b, CDM_Customer_Key_Lookup c 
 where a.Customer_Id1 = b.Customer_Id and a.Customer_Id1 = c.Customer_id and a.',Lifecycle_Segment,' = 1 and a.',Campaign_Segment,' = 1 and a.SMS_Consent_Flag = 0 and length(b.Mobile) >= 10 ) as t;');

	                    
                       PREPARE stmt FROM @query4;
					EXECUTE stmt; 
						DEALLOCATE PREPARE stmt;


	                    Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Count",@c5) )),"]") into @temp_result;') ;
	                                   
	                    SELECT @Query_String;
	                    PREPARE statement from @Query_String;
	                    Execute statement;
	                    Deallocate PREPARE statement; 

elseif  Comm_Channel = 'WhatsApp'
then
 
     set @cnt=concat('select Convert(format(count(1),","),CHAR)   INTO @c5  from ( select CUstomer_Id1 from CLM_Customer_Segmentation a, CDM_Customer_PII_Master b, CDM_Customer_Key_Lookup c 
 where a.Customer_Id1 = b.Customer_Id and a.Customer_Id1 = c.Customer_id and a.',Lifecycle_Segment,' = 1 and a.',Campaign_Segment,' = 1 and a.WhatsApp_Consent_Flag = 0 and length(b.Mobile) >= 10 ) as t;');

     
	                    
	                    Select @cnt;
	                    PREPARE stmt FROM @cnt;
	                    EXECUTE stmt; 
	                    DEALLOCATE PREPARE stmt;


	                    Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Count",@c5) )),"]") into @temp_result;') ;
	                                   
	                    SELECT @Query_String;
	                    PREPARE statement from @Query_String;
	                    Execute statement;
	                    Deallocate PREPARE statement; 


ELSEIF Comm_Channel = 'Email'
then

set @cnt=concat('select Convert(format(count(1),","),CHAR)  INTO @c5 from (select Customer_id1 from CLM_Customer_Segmentation a, CDM_Customer_PII_Master b, CDM_Customer_Key_Lookup c 
 where a.Customer_Id1 = b.Customer_Id and a.Customer_Id1 = c.Customer_id and a.',Lifecycle_Segment,' = 1 and a.',Campaign_Segment,' = 1 and a.Email_Consent_Flag = 0 and b.Email != "") as t;');
     
	                    
	                    Select @cnt;
                        PREPARE stmt FROM @cnt;
	                    EXECUTE stmt; 
	                    DEALLOCATE PREPARE stmt;


	                    Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Count",@c5) )),"]") into @temp_result;') ;
	                                   
	                    SELECT @Query_String;
	                    PREPARE statement from @Query_String;
	                    Execute statement;
	                    Deallocate PREPARE statement; 
                        

	              ELSE
	                          Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Invalid Parameters:-Please Select Coverage Level") )),"]") into @temp_result                       
	                                                                                  ;') ;
	                                   
	                           SELECT @Query_String;
	                           PREPARE statement from @Query_String;
	                           Execute statement;
	                           Deallocate PREPARE statement; 
	                                                


	             End If;
                 
                 
                 else 
                 
                  Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Invalid Parameters:-Please Select Channel") )),"]") into @temp_result                       
	                                                                                  ;') ;
	                                   
	                           SELECT @Query_String;
	                           PREPARE statement from @Query_String;
	                           Execute statement;
	                           Deallocate PREPARE statement; 
	                                                
                                                    
                 end if;
	
    SET out_result_json = @temp_result;


END

