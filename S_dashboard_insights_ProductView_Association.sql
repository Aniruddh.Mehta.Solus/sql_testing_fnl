DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_insights_ProductView_Association`(IN in_request_json json, OUT out_result_json json)
BEGIN
   
   declare IN_USER_NAME varchar(240); 
   declare IN_SCREEN varchar(400);
   declare IN_MEASURE text; 
   declare IN_FILTER_CURRENTMONTH text; 
   declare IN_FILTER_CURRENTYEAR text;
   declare IN_FILTER_MONTHDURATION text; 
   declare IN_NUMBER_FORMAT text;
   declare IN_AGGREGATION_1 text; 
   declare IN_AGGREGATION_2 text;
   declare IN_AGGREGATION_3 text;
   declare Query_String text;
   declare Query_Measure_Name varchar(100);
   declare vProductName varchar(100);
   declare vAssociated_ProductName varchar(100);
   declare vCustomerCount varchar(64);
   declare vAssociation varchar(64);
   declare checkForCursorEnd INT DEFAULT 0;
   
   DECLARE Cur_Prod_View_Summ CURSOR FOR
   select ProductName, Associated_ProductName,
   CASE 
		WHEN ProductName=Associated_ProductName THEN '-'
		ELSE  Customer_Count 
   END 
   AS Customer_Count1,
   CASE 
		WHEN ProductName=Associated_ProductName THEN '-'
		ELSE  T3MthBase 
   END 
   AS T3MthBase1
from 
   (select ProductName,
   Associated_ProductName,Customer_Count,
   round(ifnull((Customer_Count/T3MthBase),0)*100,0) as `T3MthBase`
   from
   (select *, sum(Customer_Count) over(partition by ProductName) as `T3MthBase`,
    row_number() over(partition by ProductName order by Customer_Count desc) as `Ranking` 
    from Dashboard_Insights_Product_View_Association order by ProductName,Associated_ProductName) as temp
    where Ranking<21 limit 400 ) as x; 
   

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET checkForCursorEnd=1;
   
	SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    
   SET @iFirstTime=0;
   SET @jsonStr = ' ';
   SET @vPrevProductName= ' ';
   
OPEN Cur_Prod_View_Summ;
Loop_Cur : Loop
     FETCH Cur_Prod_View_Summ INTO vProductName, vAssociated_ProductName, vCustomerCount, vAssociation;
     
     	IF checkForCursorEnd THEN    
		LEAVE LOOP_cur;
	    END IF;
        
        IF IN_NUMBER_FORMAT = 'NUMBER'
        THEN
           SET @vMetrics = CONCAT('"',vCustomerCount,'"');
		ELSE 
           IF vAssociation != '-' THEN
              SET @vMetrics=CONCAT('"',vAssociation,'%"');
		   ELSE
              SET @vMetrics=CONCAT(' "',vAssociation,'" ');
		   END IF;
        END IF;
        
        
		IF vProductName != @vPrevProductName THEN
		IF @iFirstTime = 0 THEN
			SET @iFirstTime=1;
        ELSE 
			SET @jsonStr = CONCAT(@jsonStr,'},');
		END IF;
		SET @jsonStr=CONCAT( @jsonStr , '{" ": "',vProductName,'"' );
        SET @jsonStr=CONCAT( @jsonStr , ',"Base": "100% "' );

        SET @vPrevProductName = vProductName;
		END IF;
        SET @jsonStr=CONCAT(@jsonStr,',"',vAssociated_ProductName,'" : ',@vMetrics);
END LOOP Loop_Cur;
		SET @jsonStr=CONCAT('[ ',@jsonStr,'} ]') ;
CLOSE Cur_Prod_View_Summ;
   
select @jsonStr;
SET out_result_json=@jsonStr;
SELECT out_result_json;
 
END$$
DELIMITER ;
