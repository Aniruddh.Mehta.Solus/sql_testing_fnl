DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_performance_outreaches`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    
    SET @FY_SATRT_DATE=IN_FILTER_CURRENTMONTH-100;
    SET @FY_END_DATE=IN_FILTER_CURRENTMONTH;
    select Value1 into @primary_channel from UI_Configuration_Global where Config_Name = 'PRIMARY_CHANNEL';
    SET @primary_channel=ifnull(@primary_channel,'SMS');

	select Value1 into @START_OF_WEEK from UI_Configuration_Global where Config_Name = 'FIRSTDAYOFWEEK';
    if @START_OF_WEEK = 'Sunday'
    then 
    SET @START_OF_WEEK= 0;
    else
    SET @START_OF_WEEK= 5;
	END IF;
    
	IF IN_AGGREGATION_1 = 'BY_DAY' 
	THEN 
		SET @filter_cond = concat(' date_format(EE_DATE,"%Y%m")=',IN_FILTER_CURRENTMONTH);
		SET @date_select = ' date_format(EE_DATE,"%d-%b") AS "Year" ';
	
    ELSEIF IN_AGGREGATION_1 = 'BY_WEEK' 
	THEN 
		
		SET @filter_cond = CONCAT('  EE_Date between DATE_SUB( now(), INTERVAL 7 day) and now() ');
        SET @filter_condc=concat('  EE_Date between DATE_SUB( now(), INTERVAL 7 day) and now() ');
		SET @filter_condc=CONCAT(' date_format(`EE_Date`,"%Y%m") =',IN_FILTER_CURRENTMONTH);
        SET @date_select = ' date_format(EE_DATE,"%d-%b") AS "Year" ';
        SET @date_select_graph = CONCAT('YEARWEEK(EE_Date,',@START_OF_WEEK,') AS "dt"');
        
        set @Y = concat( substring(IN_FILTER_CURRENTMONTH,1,4) );
        set @M = concat( substring(IN_FILTER_CURRENTMONTH,5,6) );
        SET @graph_in_filter_curr_month = concat(@Y,'-',@M,"-01");
        SET @graph_in_filter_curr_month = DATE_ADD(@graph_in_filter_curr_month,INTERVAL 4 WEEK);
        SET @filter_cond_graph = CONCAT('EE_DATE BETWEEN DATE_SUB("',@graph_in_filter_curr_month,'", INTERVAL 7 WEEK) AND "',@graph_in_filter_curr_month,'"');
        SET @temp_view = concat('
        create or replace View temp_Outreaches_Week as
        
        SELECT 
		YEARWEEK(EE_Date,',@START_OF_WEEK,') AS "dt"
		FROM
		CLM_Event_Dashboard dash
		WHERE
		',@filter_cond_graph,'
        GROUP BY 1
		ORDER BY EE_Date');
	
		select @temp_view;
		PREPARE statement from @temp_view;
		Execute statement;
		Deallocate PREPARE statement;
        
        create or replace view week_name as
        select date_Format(Date_Sub(max(tm.EE_date), interval 6 day), "%b%e") as week_name, temp.dt
        from temp_Outreaches_Week temp, table_date_yearweek tm 
        where temp.dt = tm.EE_Yearweek
        group by temp.dt
        order by tm.EE_date;
		
        select * from week_name;
        select group_concat(week_name) FROM week_name INTO @distinct_channel_week;
		SELECT group_concat(dt) FROM temp_Outreaches_Week INTO @distinct_channel;
		select @distinct_channel;
        select (length(@distinct_channel)-length(replace(@distinct_channel,',','')))+1 into @num_of_channel;
		set @loop_var=1;
		set @Q_M=concat("CASE ");
        select @loop_var,@num_of_channel,@distinct_channel;
		WHILE @loop_var<=@num_of_channel
		do
			SET @selected_channel=substring_index(substring_index(concat(@distinct_channel,',-1'),',',@loop_var),',',-1);
            SET @selected_channel_week = 
            substring_index(substring_index(concat(@distinct_channel_week,',-1'),',',@loop_var),',',-1);
			set @Q_M=concat(@Q_M," WHEN YEARWEEK(EE_Date,",@START_OF_WEEK,") = ",@selected_channel," Then 'Week of ",@selected_channel_week,"' 
			");
			set @json_select=concat(@json_select,',"Week of ',@selected_channel_week,'",IFNULL(`Week of ',@selected_channel_week,'`,0)');
			set @loop_var=@loop_var+1;
		end while;
		set @Q_M=concat(@Q_M," else EE_Date END AS 'w'");
		select @Q_M;
        SET @temp_view_stmt = concat('
        SELECT 
		YEARWEEK(EE_Date,',@START_OF_WEEK,') AS dt,',@Q_M,'
		FROM
		CLM_Event_Dashboard dash
		WHERE
		',@filter_cond_graph,'
        GROUP BY YEARWEEK(EE_Date,',@START_OF_WEEK,')
		ORDER BY EE_Date');
        select @temp_view_stmt;
    ELSEIF IN_AGGREGATION_1 = 'BY_MONTH' 
	THEN 
		SET @filter_cond=concat(" YM between '",@FY_SATRT_DATE,"' and '",@FY_END_DATE,"'");
        SET @filter_condc=concat(' date_format(`EE_Date`,"%Y%m") =',IN_FILTER_CURRENTMONTH);
		SET @date_select=' date_format(EE_DATE,"%b-%Y") AS "Year" ';
	
    ELSEIF IN_AGGREGATION_1 = 'BY_QUARTER' 
	THEN 
		SET @FY_SATRT_DATE = F_Month_Diff(IN_FILTER_CURRENTMONTH,2);
		SET @filter_cond=concat(" YM between '",@FY_SATRT_DATE,"' and '",@FY_END_DATE,"'");
        SET @filter_condc=concat(" YM between '",@FY_SATRT_DATE,"' and '",@FY_END_DATE,"'");
		SET @date_select=' date_format(EE_DATE,"%b-%Y") AS "Year" ';
	END IF;

	

	SET IN_MEASURE = CONCAT(IN_MEASURE,IN_AGGREGATION_2);
	
	IF IN_MEASURE LIKE 'OUTREACHCHANNEL%'
	THEN 
		IF IN_AGGREGATION_1='BY_MONTH'
		THEN
			select group_concat(DISTINCT `Channel`) into @distinct_channel from CLM_Event_Dashboard where YM Between @FY_SATRT_DATE and @FY_END_DATE and `Channel` <> '' and Channel <> 'NA';
			SET @inside_query=' date_format(a.EE_DATE,"%b-%Y")=date_format(b.EE_DATE,"%b-%Y") ';
		ELSEIF IN_AGGREGATION_1='BY_QUARTER'
		THEN
			select group_concat(DISTINCT `Channel`) into @distinct_channel from CLM_Event_Dashboard where YM Between @FY_SATRT_DATE and @FY_END_DATE and `Channel` <> '' and Channel <> 'NA';
			SET @inside_query=' date_format(a.EE_DATE,"%b-%Y")=date_format(b.EE_DATE,"%b-%Y") ';
		ELSE 
			select group_concat(DISTINCT `Channel`) into @distinct_channel from CLM_Event_Dashboard where date_format(EE_DATE,"%Y%m")=IN_FILTER_CURRENTMONTH and `Channel` <> '' and Channel <> 'NA';
			SET @inside_query = ' date_format(a.EE_DATE,"%d-%b")=date_format(b.EE_DATE,"%d-%b") ';
            SET @date_select = ' date_format(EE_DATE,"%d-%b-%Y") AS "Year" ';
		END IF;
		select (length(@distinct_channel)-length(replace(@distinct_channel,',','')))+1 into @num_of_channel;
		set @loop_var=1;
		set @Query_Measure='';
		set @json_select='';
		select @loop_var,@num_of_channel,@distinct_channel;
		WHILE @loop_var<=@num_of_channel
		do
			SET @selected_channel=substring_index(substring_index(concat(@distinct_channel,',-1'),',',@loop_var),',',-1);
			set @Query_Measure=concat(@Query_Measure,",MAX(CASE WHEN `Channel` = '",@selected_channel,"' Then pct else 0 END) AS '",@selected_channel,"'");
			set @json_select=concat(@json_select,',"',@selected_channel,'",`',@selected_channel,'`');
			set @loop_var=@loop_var+1;
		end while;
		SET @view_stmt=concat('create or replace view Dashboard_Outreaches_Graph as
		select MAX(YM) AS YM,`Year`',@Query_Measure,' from 
		(select ',@date_select,',MAX(Event_Execution_Date_ID) AS YM,`Channel`,round((SUM(Target_Base)/(CASE WHEN "',IN_NUMBER_FORMAT,'"="PERCENT" THEN (select SUM(Target_Base) from CLM_Event_Dashboard b where ',@inside_query,') ELSE 100 END) )*100,1) pct from CLM_Event_Dashboard a
		where ',@filter_cond,'
		group by 1,`Channel`)C
		group by `Year` ORDER BY YM');
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`Year`',@json_select,') )),"]")into @temp_result from Dashboard_Outreaches_Graph ORDER BY YM;') ;
        
        END IF;

	IF IN_MEASURE LIKE 'DELIVEREDBY_CHANNEL%'
	THEN
    select "Delivered by channel";
		IF IN_AGGREGATION_1='BY_MONTH'
		THEN
			select group_concat(DISTINCT `Channel`) into @distinct_channel from CLM_Event_Dashboard where YM Between @FY_SATRT_DATE and @FY_END_DATE and `Channel` <> '' and Channel <> 'NA';
			SET @inside_query=' date_format(a.EE_DATE,"%b-%Y")=date_format(b.EE_DATE,"%b-%Y") ';
		ELSEIF IN_AGGREGATION_1='BY_QUARTER'
		THEN
			select group_concat(DISTINCT `Channel`) into @distinct_channel from CLM_Event_Dashboard where YM Between @FY_SATRT_DATE and @FY_END_DATE and `Channel` <> '' and Channel <> 'NA';
			SET @inside_query=' date_format(a.EE_DATE,"%b-%Y")=date_format(b.EE_DATE,"%b-%Y") ';
		ELSE 
			select group_concat(DISTINCT `Channel`) into @distinct_channel from CLM_Event_Dashboard where date_format(EE_DATE,"%Y%m")=IN_FILTER_CURRENTMONTH and `Channel` <> '' and Channel <> 'NA';
			SET @inside_query = ' date_format(a.EE_DATE,"%d-%b")=date_format(b.EE_DATE,"%d-%b") ';
            SET @date_select = ' date_format(EE_DATE,"%d-%b-%Y") AS "Year" ';
		END IF;
        select @distinct_channel,@inside_query;
		select (length(@distinct_channel)-length(replace(@distinct_channel,',','')))+1 into @num_of_channel;
		set @loop_var=1;
		set @Query_Measure='';
		set @json_select='';
		select @loop_var,@num_of_channel,@distinct_channel;
		WHILE @loop_var<=@num_of_channel
		do
			SET @selected_channel=substring_index(substring_index(concat(@distinct_channel,',-1'),',',@loop_var),',',-1);
			set @Query_Measure=concat(@Query_Measure,",MAX(CASE WHEN `Channel` = '",@selected_channel,"' Then pct else 0 END) AS '",@selected_channel,"'");
			set @json_select=concat(@json_select,',"',@selected_channel,'",`',@selected_channel,'`');
			set @loop_var=@loop_var+1;
		end while;
        select @Query_Measure;
		
        SET @view_stmt=concat('create or replace view Dashboard_Outreaches_Graph as
		select MAX(YM) AS YM,`Year`',@Query_Measure,' from 
		(
        select ',@date_select,',
        MAX(Event_Execution_Date_ID) AS YM,
        `Channel`,
        case when SUM(TARGET_DELIVERED) = SUM(TARGET_DELIVERED_NA) then null else 
        ROUND((
        SUM(TARGET_DELIVERED)
        /
        (
        CASE WHEN "',IN_NUMBER_FORMAT,'"="PERCENT" THEN 
        (select SUM(Target_Base) from CLM_Event_Dashboard b where ',@inside_query,') 
        ELSE 100 END
        ) 
        )*100,1) END  pct from CLM_Event_Dashboard a
		where ',@filter_cond,'
		group by 1,`Channel`)C
		group by `Year` ORDER BY YM');
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`Year`',@json_select,') )),"]")into @temp_result from Dashboard_Outreaches_Graph ORDER BY YM;') ;
        
        END IF;
    
	IF IN_MEASURE= 'COVERAGEBY_SEGMENT'
	THEN 
		IF IN_AGGREGATION_1='BY_MONTH'
		THEN
			select group_concat(DISTINCT `Segment`) into @distinct_channel from CLM_Event_Dashboard where YM Between @FY_SATRT_DATE and @FY_END_DATE and `Segment` <> '' and Segment <> 'NA';
			SET @inside_query=' date_format(a.EE_DATE,"%b-%Y")=date_format(b.EE_DATE,"%b-%Y") ';
            SET @filter_cond=concat(" YM between ",(IN_FILTER_CURRENTMONTH-2)," and ",IN_FILTER_CURRENTMONTH,"");
		ELSE 
			select group_concat(DISTINCT `Segment`) into @distinct_channel from CLM_Event_Dashboard where date_format(EE_DATE,"%Y%m")=IN_FILTER_CURRENTMONTH and `Segment` <> '' and Segment <> 'NA';
            SET @date_select = ' date_format(EE_DATE,"%d-%b-%Y") AS "Year" ';
			SET @inside_query = ' date_format(a.EE_DATE,"%d-%b")=date_format(b.EE_DATE,"%d-%b-%Y") ';
		END IF;
		select (length(@distinct_channel)-length(replace(@distinct_channel,',','')))+1 into @num_of_channel;
		set @loop_var=1;
		set @Query_Measure='';
		set @json_select='';
		select @loop_var,@num_of_channel,@distinct_channel;
		WHILE @loop_var<=@num_of_channel
		do
			SET @selected_channel=substring_index(substring_index(concat(@distinct_channel,',-1'),',',@loop_var),',',-1);
			set @Query_Measure=concat(@Query_Measure,",MAX(CASE WHEN `Segment` = '",@selected_channel,"' Then pct else 0 END) AS `",@selected_channel,"`");
			set @json_select=concat(@json_select,',"',@selected_channel,'",`',@selected_channel,'`');
			set @loop_var=@loop_var+1;
		end while;
        SET @inside_query=(case when IN_NUMBER_FORMAT = 'PERCENT' THEN (case when IN_AGGREGATION_1 ='BY_DAY' then ' round((Total_Distinct_Customers_Targeted_This_Day/Total_Customer_Base)*100,1) ' when IN_AGGREGATION_1 = 'BY_MONTH' then ' round((Total_Distinct_Customers_Targeted_This_Month_This_Segment/Total_Customer_Base)*100,1) ' end)
							when IN_NUMBER_FORMAT = 'NUMBER' THEN (case when IN_AGGREGATION_1 ='BY_DAY' then ' Total_Distinct_Customers_Targeted_This_Day ' when IN_AGGREGATION_1 = 'BY_MONTH' then ' Total_Distinct_Customers_Targeted_This_Month ' end) end);
		SET @view_stmt=concat('create or replace view Dashboard_Outreaches_Graph as
		select MAX(YM),`Year`',@Query_Measure,' from 
		(select Event_Execution_Date_ID as YM,',@date_select,',`Segment`, (',@inside_query,') pct from CLM_Event_Dashboard a
		where ',@filter_cond,'
		group by 1,`Segment`)C
		group by `Year`
        order by `YM`');
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`Year`',@json_select,') )),"]")into @temp_result from Dashboard_Outreaches_Graph;') ;
	END IF;

	IF IN_MEASURE = "OUTREACH" OR IN_MEASURE = "CARDS"
	THEN 
		SET @Query_Measure=' SUM(TARGET_BASE) ';
		SET @outreach_measure=@Query_Measure;
	END IF;
	IF IN_MEASURE = "OUTREACHCUST" OR IN_MEASURE = "CARDS"
	THEN
		SET @Query_Measure=(case when IN_AGGREGATION_1 IN ('BY_DAY') then ' ifnull(round((SUM(Target_Base)/Total_Distinct_Customers_Targeted_This_Day),1),0) ' when IN_AGGREGATION_1 IN ('BY_MONTH','BY_QUARTER','BY_WEEK') then ' ifnull(round((SUM(Target_Base)/Total_Distinct_Customers_Targeted_This_Month),1),0) ' end);
		SET @outreachcust_measure=@Query_Measure;
	END IF;
	IF IN_MEASURE = "DELIVERED" OR IN_MEASURE = "CARDS"
	THEN
		SET @Query_Measure=(case when IN_NUMBER_FORMAT = 'NUMBER' THEN  'case when SUM(TARGET_DELIVERED) = SUM(TARGET_DELIVERED_NA) then "" else  SUM(TARGET_DELIVERED) end'  WHEN IN_NUMBER_FORMAT='PERCENT' THEN 'case when SUM(TARGET_DELIVERED) = SUM(TARGET_DELIVERED_NA) then "" else  ifnull(round((SUM(TARGET_DELIVERED)/SUM(TARGET_BASE))*100,1),0)  END' END);
		SET @delivered_measure=' case when SUM(TARGET_DELIVERED) = SUM(TARGET_DELIVERED_NA) then " - " else ifnull(round((SUM(TARGET_DELIVERED)/SUM(TARGET_BASE))*100,1),0) END ';
	END IF;
	IF IN_MEASURE = "COVERAGE" OR IN_MEASURE = "CARDS"
	THEN
		SET @Query_Measure=(case when IN_NUMBER_FORMAT = 'PERCENT' THEN (case when IN_AGGREGATION_1 IN ('BY_DAY','BY_WEEK') then ' ifnull(round((Total_Distinct_Customers_Targeted_This_Day/Total_Customer_Base)*100,1),0) ' when IN_AGGREGATION_1 IN ('BY_MONTH' ,'BY_QUARTER') then ' ifnull(round((Total_Distinct_Customers_Targeted_This_Month/Total_Customer_Base)*100,1),0) ' end)
							when IN_NUMBER_FORMAT = 'NUMBER' THEN (case when IN_AGGREGATION_1 IN ('BY_DAY','BY_WEEK') then ' Total_Distinct_Customers_Targeted_This_Day ' when IN_AGGREGATION_1 IN ('BY_MONTH' ,'BY_QUARTER') then ' Total_Distinct_Customers_Targeted_This_Month ' end) end);
		SET @coverage_measure=' ifnull(round((MAX(Total_Distinct_Customers_Targeted_This_Month)/MAX(Total_Customer_Base))*100,1),0) ';
	END IF;
	IF IN_MEASURE = "NOOFCAMPAIGN" OR IN_MEASURE = "CARDS"
	THEN
		
        SET @Query_Measure=' (CASE WHEN MIN(Event_Id) <=0 then (count(distinct Event_ID)-1) ELSE (count(distinct Event_ID)) END) ';
		SET @campaign_measure=@Query_Measure;
        SET @camaign__measure_desc=concat(@Query_Measure,',"Distinct Number of Triggers"');
	END IF;
	IF IN_MEASURE = "OPENRATE" OR IN_MEASURE = "CARDS"
	THEN
		SET @Query_Measure=' ifnull(round((SUM(TARGET_OPEN)/SUM(Target_Delivered))*100,1),0) '; 
		SET @Query_Filter_Cond=' CHANNEL="EMAIL" ';
		SET @openrate_measure=@Query_Measure;
	END IF;
	IF IN_MEASURE = "OUTREACHOFFER" OR IN_MEASURE = "CARDS"
	THEN
		SET @Query_Measure=(case when IN_NUMBER_FORMAT = 'PERCENT' THEN ' ifnull(round((SUM(CASE WHEN OFFER NOT IN ("NA","NO_OFFER","NO OFFER") THEN Target_Base ELSE 0 END)/SUM(Target_Base))*100,1),0) ' WHEN IN_NUMBER_FORMAT = 'NUMBER' THEN ' SUM(CASE WHEN OFFER NOT IN ("NA","NO_OFFER","NO OFFER") THEN Target_Base ELSE 0 END) ' END);
		SET @offer_measure=' ifnull(round((SUM(CASE WHEN OFFER NOT IN ("NA","NO_OFFER","NO OFFER") THEN Target_Base ELSE 0 END)/SUM(Target_Base))*100,1),0) ';
	END IF;
	IF IN_MEASURE = "CHANNEL" OR IN_MEASURE = "CARDS"
	THEN 
		SET @Query_Measure=' ';
		SET @channel_measure=concat(' ifnull(round(((select SUM(Target_Base) from CLM_Event_Dashboard where `Channel` = "',@primary_channel,'" and date_format(EE_DATE,"%Y%m")=',IN_FILTER_CURRENTMONTH,')/SUM(Target_Base))*100,1),0) ');
	END IF;
    
    
    IF IN_MEASURE='CARDS'
    THEN
		select @filter_condc;
		SELECT @outreach_measure,@delivered_measure,@coverage_measure,@campaign_measure,@channel_measure,@outreachcust_measure,@openrate_measure,@offer_measure;
		set @view_stmt=concat('create or replace view V_CLIENT_UI_Measures_Outreaches_Card as
		select "Outreaches" as Card_Name,convert(format(ifnull(',@outreach_measure,',0),0),CHAR) as `Value1`,"" as `Value2`, "" as `Value3`,"OUTREACH" as `Measure`  from CLM_Event_Dashboard WHERE ',@filter_condc,'
		Union
		select "Delivered %" as Card_Name,cast(',@delivered_measure,' AS CHAR) as `Value1`,"" as `Value2`, "" as `Value3`,"DELIVERED" as `Measure` from CLM_Event_Dashboard WHERE ',@filter_condc,'
		Union
		select "Customer Coverage" as Card_Name,
		concat(',@coverage_measure,',"%") as `Value1`,"" as `Value2`, "" as `Value3`,"COVERAGE" as `Measure` from CLM_Event_Dashboard WHERE ',@filter_condc,'
		Union
		select "Triggers" as Card_Name, 
		',@campaign_measure,' as `Value1`,"" as `Value2`, "" as `Value3`,"NOOFTRIGGER" as `Measure` from CLM_Event_Dashboard A, Event_Master B where A.Event_Id = B.ID AND ',@filter_condc,'
		Union
		select "Channel Mix" as Card_Name,
		concat(',@channel_measure,',"%") as `Value1`,"',@primary_channel,'" as `Value2`, "" as `Value3`,"OUTREACHCHANNEL" as `Measure` from CLM_Event_Dashboard where ',@filter_condc,'
		Union
		select "Outreaches/Cust" as Card_Name, ',@outreachcust_measure,' as `Value1`,"" as `Value2`,"" as `Value3`,"OUTREACHCUST" as `Measure` from CLM_Event_Dashboard where ',@filter_condc,'
		Union
		select "Open %" as Card_Name,concat(',@openrate_measure,',"%") as `Value1`,"" as `Value2`,"" as `Value3`,"OPENRATE" as `Measure` from CLM_Event_Dashboard where ',@filter_condc,'
		Union
		select "Offer Based" as Card_Name,
		concat(',@offer_measure,',"%") AS `Value1`,
		"" as `Value2`, "" as `Value3`,"OUTREACHOFFER" as `Measure` from CLM_Event_Dashboard where ',@filter_condc,'
		');
	SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Card_Name",`Card_Name`,"Value1",`Value1`,"Value2",`Value2`,"Value3",`Value3`,"Measure",`Measure`) )),"]")into @temp_result from V_CLIENT_UI_Measures_Outreaches_Card;' ;
    ELSEIF IN_MEASURE='DOWNLOAD'
    THEN
    SET @view_stmt1=CONCAT('CREATE OR REPLACE VIEW V_Dashboard_Outreaches_Download as       
    (  SELECT 
    EE_Date,
    Segment,
    Campaign,
   Theme,
    Offer,
    Channel,
    Event_ID,
    Control_Base,
    Target_Base,
    Target_Delivered,
	Target_Delivered_NA,
    Control_Responder,
    Target_Responder,
    Target_Revenue,
    Target_Discount,
    Target_Bills,
    Target_Open,
    Target_CTA,
    Control_Revenue,
    Control_Discount,
    Control_Bills,
    Total_Customer_Base,
    Total_Distinct_Customers_Targeted_This_Month,
    Total_Distinct_Customers_Targeted_This_Day   
FROM CLM_Event_Dashboard where YM = "',  IN_FILTER_CURRENTMONTH,'");') ;

    select @view_stmt1;
		PREPARE statement1 from @view_stmt1;
		Execute statement1;
		Deallocate PREPARE statement1; 
		
        call S_dashboard_performance_download('V_Dashboard_Outreaches_Download',@out_result_json1);
		select @out_result_json1 into out_result_json;
    
    ELSEIF IN_AGGREGATION_2 = "" AND IN_MEASURE <> 'OUTREACHCHANNEL' AND IN_AGGREGATION_1 <> "BY_WEEK"
    THEN
    SET @view_stmt=concat('CREATE OR REPLACE VIEW Dashboard_Outreaches_Graph AS
						(SELECT ',@date_select,',
                         ',@Query_Measure,' as "Value",
                         MAX(YM) as YM
                         from CLM_Event_Dashboard
                         WHERE ',@filter_cond,'
                         GROUP BY 1
                         ORDER BY EE_Date
                        )');
	SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`Year`,"Value",`Value`) )),"]")into @temp_result from Dashboard_Outreaches_Graph ORDER BY YM;' ;
    
    
    ELSEIF IN_AGGREGATION_2 = "" AND IN_MEASURE <> 'OUTREACHCHANNEL' AND IN_AGGREGATION_1 = "BY_WEEK"
    THEN
    set @view_stmt = CONCAT(' CREATE OR REPLACE VIEW Dashboard_Outreaches_Graph AS
        with 
        
        GROUP1 AS
        (
			SELECT ',@date_select_graph,',
                         ',@Query_Measure,' as "Value",
                         MAX(YM) as YM
                         from CLM_Event_Dashboard
                         WHERE ',@filter_cond_graph,'
                         GROUP BY 1
                         ORDER BY EE_Date
        ),
        GROUP2 AS
        (
			',@temp_view_stmt,'	
        )
        SELECT C.dt AS dt,C.w,Value,YM
        FROM GROUP2 C
		LEFT OUTER JOIN GROUP1 B ON B.dt=C.dt
        UNION
        SELECT C.dt,C.w,Value,YM
        FROM GROUP2 C
		RIGHT OUTER JOIN GROUP1 A ON A.dt=C.dt
        ');
    -- SET @view_stmt=concat('CREATE OR REPLACE VIEW Dashboard_Outreaches_Graph AS (                        )');
	SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`w`,"Value",`Value`) )),"]")into @temp_result from Dashboard_Outreaches_Graph ORDER BY YM;' ;
	END IF;
    
    
    IF IN_MEASURE<>'DOWNLOAD'
    THEN
		select @view_stmt;
		PREPARE statement from @view_stmt;
		Execute statement;
		Deallocate PREPARE statement; 
		
		select @Query_String;
		PREPARE statement from @Query_String;
		Execute statement;
		Deallocate PREPARE statement; 
		SET out_result_json = @temp_result;
	END IF;
    
END$$
DELIMITER ;
