
CREATE OR REPLACE PROCEDURE `S_UI_Instant_Campaign_Segment`(IN in_request_json json, OUT out_result_json json)
BEGIN
    

    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
    declare EDIT Bigint; 
     
    

    declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare IN_AGGREGATION_4 text;
	declare IN_AGGREGATION_5 text;
    declare IN_AGGREGATION_6 text;
	
	SET SQL_SAFE_UPDATES=0;
	SET foreign_key_checks=0;

	Set in_request_json = Replace(in_request_json,'\n','\\n');
  
   
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1")); 
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_AGGREGATION_4 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_4"));
    SET IN_AGGREGATION_5 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_5"));
    SET IN_AGGREGATION_6 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_6"));
    
	Set @temp_result =Null;
    Set @vSuccess ='';
	Set @vErrMsg='';
	Set @vSuccess='';
	Set @Err =0;
	set @a1 = '[{"Val":"Values"}]';
    
if IN_AGGREGATION_1 = 'Segment_Attribute_List'
    then 
    call S_UI_Instant_Campaign_Refresh_SVOC();
    
    if IN_AGGREGATION_2 = 'All'
		then
    
		SET @Query_String = concat('
SELECT CONCAT("[",(GROUP_CONCAT(json_object("UI_Variable_Name",b.UI_Variable_Name,"Col1",b.Col1,"Col2",b.Col2,"SOLUS_Attribute",SOLUS_Attribute,"Variable_Category",Variable_Category))),"]")
				into @temp_result	from ((
select UI_Variable_Name,"min_max" as Col1,concat("[",UI_Value_List,"]") as Col2,SOLUS_Attribute,Variable_Category from Campaign_Segment_Dictionary where UI_Data_Type = "Continous"
union all
select UI_Variable_Name,"DATENULL" as Col1, "DATENULL" as Col2,SOLUS_Attribute,Variable_Category from Campaign_Segment_Dictionary where UI_Data_Type = "DateRange"
union all
select UI_Variable_Name,"NULL" as Col1, concat("[",UI_Value_List,"]") as Col2,SOLUS_Attribute,Variable_Category from Campaign_Segment_Dictionary where UI_Data_Type = "Categorical" and UI_Value_List is not NULL
union all
select UI_Variable_Name,"Search" as Col1, concat("[",UI_Value_List,"]") as Col2,SOLUS_Attribute,Variable_Category from Campaign_Segment_Dictionary where Variable_Category = "Custom Variable" 
union all
select UI_Variable_Name,"Search" as Col1, concat("[",UI_Value_List,"]") as Col2,SOLUS_Attribute,Variable_Category from Campaign_Segment_Dictionary where UI_Data_Type = "Search" and UI_Value_List is not NULL
union all
select "`Category Purchased`" as UI_Variable_Name,"Search" as Col1,(select concat("[",group_concat(json_object(val,res)),"]") from (
select "Val" as val,res from (
select distinct(LOV_Value) as res from CDM_Product_Master_Original a, CDM_LOV_Master b
where  Lov_Id = Cat_LOV_Id and LOV_Value != '') tab1) tab2 ) as Col2,"Cat_Name" as SOLUS_Attribute,"Categorical" as Variable_Category
union all
select "`Favourite Category`" as UI_Variable_Name,"Search" as Col1,(select concat("[",group_concat(json_object(val,res)),"]") from (
select "Val" as val,res from (
select distinct(LOV_Value) as res from CDM_Customer_TP_Var a, CDM_LOV_Master b
where  Lov_Id = LT_Fav_Cat_LOV_Id and LOV_Value != '') tab1) tab2) as Col2,"Fav_Cat" as SOLUS_Attribute,"Categorical" as Variable_Category
)  b);
') ;
				      
	elseif 	IN_AGGREGATION_2 = 'RFM'
        then
        
        	SET @Query_String = concat('
SELECT CONCAT("[",(GROUP_CONCAT(json_object("UI_Variable_Name",b.UI_Variable_Name,"Col1",b.Col1,"Col2",b.Col2,"SOLUS_Attribute",SOLUS_Attribute,"Variable_Category",Variable_Category))),"]")
				into @temp_result	from ((
select UI_Variable_Name,"min_max" as Col1,concat("[",UI_Value_List,"]") as Col2,SOLUS_Attribute,Variable_Category from Campaign_Segment_Dictionary where UI_Data_Type = "Continous" and Variable_Category = "RFM"
union all
select UI_Variable_Name,"DATENULL" as Col1, "DATENULL" as Col2,SOLUS_Attribute,Variable_Category from Campaign_Segment_Dictionary where UI_Data_Type = "DateRange" and Variable_Category = "RFM"
)  b);
') ;
        
        
	elseif 	IN_AGGREGATION_2 = 'Loyalty'
        then 
        
                	SET @Query_String = concat('
SELECT CONCAT("[",(GROUP_CONCAT(json_object("UI_Variable_Name",b.UI_Variable_Name,"Col1",b.Col1,"Col2",b.Col2,"SOLUS_Attribute",SOLUS_Attribute,"Variable_Category",Variable_Category))),"]")
				into @temp_result	from ((
select UI_Variable_Name,"min_max" as Col1,concat("[",UI_Value_List,"]") as Col2,SOLUS_Attribute,Variable_Category from Campaign_Segment_Dictionary where UI_Data_Type = "Continous" and Variable_Category = "Loyalty"
union all
select UI_Variable_Name,"DATENULL" as Col1, "DATENULL" as Col2,SOLUS_Attribute,Variable_Category from Campaign_Segment_Dictionary where UI_Data_Type = "DateRange" and Variable_Category = "Loyalty"
)  b);
') ;
        
	elseif IN_AGGREGATION_2 = 'Demography'
        then
        
                       	SET @Query_String = concat('
SELECT CONCAT("[",(GROUP_CONCAT(json_object("UI_Variable_Name",b.UI_Variable_Name,"Col1",b.Col1,"Col2",b.Col2,"SOLUS_Attribute",SOLUS_Attribute,"Variable_Category",Variable_Category))),"]")
				into @temp_result	from ((
select UI_Variable_Name,"min_max" as Col1,concat("[",UI_Value_List,"]") as Col2,SOLUS_Attribute,Variable_Category from Campaign_Segment_Dictionary where  Variable_Category = "Demography"

 )  b);
') ;


elseif IN_AGGREGATION_2 = 'Product_Purchase'
        then
        
                       	SET @Query_String = concat('
SELECT CONCAT("[",(GROUP_CONCAT(json_object("UI_Variable_Name",b.UI_Variable_Name,"Col1",b.Col1,"Col2",b.Col2,"SOLUS_Attribute",SOLUS_Attribute,"Variable_Category",Variable_Category))),"]")
				into @temp_result	from ((
select UI_Variable_Name,"min_max" as Col1,concat("[",UI_Value_List,"]") as Col2,SOLUS_Attribute,Variable_Category from Campaign_Segment_Dictionary where UI_Data_Type = "Continous"  and Variable_Category = "Product Purchase"
union all
select UI_Variable_Name,"NULL" as Col1, concat("[",UI_Value_List,"]") as Col2,SOLUS_Attribute,Variable_Category from Campaign_Segment_Dictionary where UI_Data_Type = "Categorical" and Variable_Category = "Product Purchase"

)  b);
') ;


elseif IN_AGGREGATION_2 = 'Custom_Variable'
        then
        
        
	
                       	SET @Query_String = concat('
SELECT CONCAT("[",(GROUP_CONCAT(json_object("UI_Variable_Name",b.UI_Variable_Name,"Col1",b.Col1,"Col2",b.Col2,"SOLUS_Attribute",SOLUS_Attribute,"Variable_Category",Variable_Category))),"]")
				into @temp_result	from ((
select UI_Variable_Name,"Search" as Col1, concat("[",UI_Value_List,"]") as Col2,SOLUS_Attribute,Variable_Category from Campaign_Segment_Dictionary where Variable_Category = "Custom Variable"  
)  b);
') ;
          
	
          
	else
      
        Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Invalid Type") )),"]") 
														into @temp_result                       
												      ;') ; 
	end if;


        
	SELECT @Query_String;
	PREPARE statement from @Query_String;
	Execute statement;
	Deallocate PREPARE statement; 
	
        set @temp_result = replace(@temp_result,'}]"','}]');
         set @temp_result = replace(@temp_result,'"[{','[{');
         set @temp_result = replace(@temp_result,'"}"','"}]');
         set @temp_result = replace(@temp_result,'"{"Val','[{"Val');
        set @temp_result = replace(@temp_result,"\\","");
        
        
        
	ELSEIF IN_AGGREGATION_1 = 'Segment_Scheme_List'
   THEN
    IF IN_AGGREGATION_2 = 'CAMPAIGN'
		THEN
    
		SET @Query_String = concat('
SELECT CONCAT("[",(GROUP_CONCAT(json_object("Segment_Scheme",b.Segment_Scheme,"Scheme_Count",b.Scheme_Count))),"]")
				into @temp_result	from ((
select "CLM Segment" as Segment_Scheme,count(1) as Scheme_Count from Campaign_Segment
where Segment_Scheme = "CLM Segment"

UNION ALL

select "VF Segment" as Segment_Scheme,count(1) as Scheme_Count from Campaign_Segment
where Segment_Scheme = "VF Segment"

UNION ALL

select "Business Segment" as Segment_Scheme,count(1) as Scheme_Count from Campaign_Segment
where Segment_Scheme = "Business Segment"

UNION ALL

select "Tiers" as Segment_Scheme,count(1) as Scheme_Count from Campaign_Segment
where Segment_Scheme = "Tiers"

UNION ALL

select "Channel" as Segment_Scheme,count(1) as Scheme_Count from Campaign_Segment
where Segment_Scheme = "Channel"

UNION ALL


select "Revenue Center" as Segment_Scheme,count(1) as Scheme_Count from Campaign_Segment
where Segment_Scheme = "Revenue Center"

UNION ALL


select "Product Centric" as Segment_Scheme,count(1) as Scheme_Count from Campaign_Segment
where Segment_Scheme = "Product Centric"

UNION ALL


select "Demographic Based" as Segment_Scheme,count(1) as Scheme_Count from Campaign_Segment
where Segment_Scheme = "Demographic Based"
)  b);
') ;
				      
		ELSEIF IN_AGGREGATION_2 = 'DASHBOARD'
        THEN
        SET @Query_String = concat('
SELECT CONCAT("[",(GROUP_CONCAT(json_object("Segment_Scheme",b.Segment_Scheme,"Scheme_Count",b.Scheme_Count))),"]")
				into @temp_result	from ((
select "CLM Segment" as Segment_Scheme,count(1) as Scheme_Count from Dashboard_Segment
where Segment_Scheme = "CLM Segment"

UNION ALL

select "VF Segment" as Segment_Scheme,count(1) as Scheme_Count from Dashboard_Segment
where Segment_Scheme = "VF Segment"

UNION ALL

select "Business Segment" as Segment_Scheme,count(1) as Scheme_Count from Dashboard_Segment
where Segment_Scheme = "Business Segment"

UNION ALL

select "Tiers" as Segment_Scheme,count(1) as Scheme_Count from Dashboard_Segment
where Segment_Scheme = "Tiers"

UNION ALL

select "Channel" as Segment_Scheme,count(1) as Scheme_Count from Dashboard_Segment
where Segment_Scheme = "Channel"

UNION ALL


select "Revenue Center" as Segment_Scheme,count(1) as Scheme_Count from Dashboard_Segment
where Segment_Scheme = "Revenue Center"

UNION ALL


select "Product Centric" as Segment_Scheme,count(1) as Scheme_Count from Dashboard_Segment
where Segment_Scheme = "Product Centric"

UNION ALL


select "Demographic Based" as Segment_Scheme,count(1) as Scheme_Count from Dashboard_Segment
where Segment_Scheme = "Demographic Based"
)  b);
') ;
	END IF;
    
			SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement; 

        
        elseif IN_AGGREGATION_1 = 'Segment_Type_List'
        then
        
        Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Variable_Category",a.Var_Type) )),"]") 
														into @temp_result  from (
                                                        select distinct(Variable_Category) as Var_Type from Campaign_Segment_Dictionary  union select "All" as Var_Type) a where a.Var_Type is not NULL
												      ;') ;
                                                      
        
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
        
        elseif IN_AGGREGATION_1 = 'Check_Existing_Segment'
        then
   	SELECT Segment_Name into @CampaignSegmentName FROM Campaign_Segment where Segment_Name =IN_AGGREGATION_2;

     				If @CampaignSegmentName  = IN_AGGREGATION_2
     				   Then
			 			
		                    SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Segment Name Already Present") )),"]")
													    into @temp_result 
													   ;') ;
													      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
                            
                            Else 
							SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Segment Name Not Present") )),"]")
													    into @temp_result 
													   ;') ;
						      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
        end if;
        
        
elseif IN_AGGREGATION_1 = 'Refresh_Segment_Status'
        then 

        select 	`Status` into @p1 from Campaign_Segment where Segment_Name = IN_AGGREGATION_2;
SELECT 
    DATE_FORMAT(LastRefreshDate, '%d/%m %H:%i')
INTO @p2 FROM
    Campaign_Segment
WHERE
    Segment_Name = IN_AGGREGATION_2;
SELECT 
    progress
INTO @p3 FROM
    Campaign_Segment
WHERE
    Segment_Name = IN_AGGREGATION_2;
        if @p1 = 1
        then 
           Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Refresh Completed","Segment_Id",Segment_Id,"Segment_Name",Segment_Name,"Segment_Desc",Segment_Desc,"Last Refresh Date",date_format(LastRefreshDate, "%d/%m %H:%i"),"Coverage",Coverage, ) )),"]") 
														into @temp_result  from Campaign_Segment where Segment_Name = ','"',IN_AGGREGATION_2,'"',';                 
												      ;') ;
                                                      
        
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
        
        else 
         Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Refresh_Not_Yet_Complete","Progress",@p3) )),"]") 
														into @temp_result                       
												      ;') ;
        
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
end if;
        
        
	
                            
elseif  IN_AGGREGATION_1 = 'Check_Segment_Coverage'                   
		then
        set @s2 = IN_AGGREGATION_2;
        set @s2 = replace(@s2,",AND"," AND ");
        set @s2 = replace(@s2,",OR"," OR ");
          -- set @s2 = replace(@s2,","," AND ");
          set @s2 = replace(@s2,"Not equal"," not IN");
           set @s2 = replace(@s2,"equal"," IN ");
           set @s2 = replace(@s2,"NOT >"," < ");
         --  set @s2 = replace(@s2,"Avg"," ");
        --   set @s2 = replace(@s2,"Max"," ");
           
        set @temp_result = @s2;
        
        
   if @s2 not RLIKE '`Transacated On`|Product_Name|`Category Purchased`'
			then
       Set @Query_String= concat('select count( `Customer Id` ) into @temp_result from (
SELECT 
    Point_Balance as `Point Balance`,
    Customer_Id as `Customer Id`,
    Lt_ABV as ABV,
    AP_Num_of_Visits as `Visits in Active Period`,
    Lt_Num_of_Visits as `Total Visits`,
    Lt_Rev as `Total Revenue`,
    Lt_Fav_Day_Of_Week as `Fav Day Of Week`,
            x.*,
            Lt_UPT as `Units per Transaction`,
Lt_GMV as `Gross Margin value`,
Lt_ASP as `Average Selling Price`,
  
    LT_Distinct_Prod_Cnt as `Distinct Products Bought`,
    LT_Distinct_Cat_Cnt as `Distinct Category Bought`,
    (select Lov_Value from CDM_LOV_Master a where
    a.Lov_Id = LT_Fav_Cat_LOV_Id) as `Favourite Category`,
    (select Lov_Value from CDM_LOV_Master a where
    a.Lov_Id = LT_Fav_Prod_LOV_Id) as `Favourite Product`
FROM
    Customer_One_View x, Client_SVOC y
    where x.Customer_Id = y.Customer_Id_1) t1
    where ',@s2,';');
    
SELECT @Query_String;
    
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;
        
        elseif 
        @s2 not RLIKE 'Recency|Vintage|City|Point_Balance|AP_Num_of_Visits|`Category Purchased`|`Total Revenue`|Lt_Num_of_Visits|Lt_Rev|Age|Num_Of_Cart_Items|Fav_Cat|Fav_Prod|`Distinct Products Bought`|`Distinct Category Bought`'
        then
        
       Set @Query_String= concat('select count(distinct(`Customer Id`)) into @temp_result from (
select y.*,Customer_id as `Customer Id`, Bill_Date as `Transacated On`,(select Product_Comm_Name from CDM_Product_Master_Original a where
    a.Product_Id = b.Product_Id) as `Product Name`,
    (select Lov_Value from CDM_LOV_Master c,CDM_Product_Master_Original d where
    c.Lov_Id = d.Cat_Lov_Id and
     d.Product_Id = b.Product_Id) as `Category Purchased`
from CDM_Bill_Details b, Client_SVOC y
    where b.Customer_Id = y.Customer_Id_1) t1
where ',@s2,';');

SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;

else

Set @Query_String= concat('select count(distinct `Customer Id`) into @temp_result from (
 select 

 Bill_Date  as `Transacated On` ,
(select Product_Comm_Name from CDM_Product_Master_Original a where a.Product_Id = t1.Product_Id) as Product_Name,
(select Lov_Value from CDM_LOV_Master c,CDM_Product_Master_Original d where c.Lov_Id = d.Cat_Lov_Id and d.Product_Id = t1.Product_Id) as `Category Purchased`,

    Point_Balance as `Point Balance`,
    Lt_ABV as ABV,
        
Lt_UPT as `Units per Transaction`,
Lt_GMV as `Gross Margin value`,
Lt_ASP as `Average Selling Price`,
     LT_Distinct_Prod_Cnt as `Distinct Products Bought`,
    LT_Distinct_Cat_Cnt as `Distinct Category Bought`,
        Lt_Fav_Day_Of_Week as `Fav Day Of Week`,
    t1.Customer_Id as `Customer Id`,
    AP_Num_of_Visits as `Visits in Active Period`,
    Lt_Num_of_Visits as `Total Visits`,
    Lt_Rev as `Total Revenue`,
    
    t2.*,
   (select Lov_Value from CDM_LOV_Master a where
    a.Lov_Id = LT_Fav_Cat_LOV_Id) as `Favourite Category`,
    (select Lov_Value from CDM_LOV_Master a where
    a.Lov_Id = LT_Fav_Prod_LOV_Id) as `Favourite Product`
from CDM_Bill_Details t1, Customer_One_View t2, Client_SVOC y
where t1.Customer_Id = t2.Customer_Id
and t2.Customer_Id = y.Customer_Id_1
) as t3
where ',@s2,';');

SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;


        end if;
        
        
           Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message",','"',@temp_result,'"',') )),"]") 
														into @temp_result                       
												      ;') ;
        
SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
	
      elseif IN_AGGREGATION_1 = 'Save_Segment'
        then
                set @s2 = IN_AGGREGATION_2;
        set @s2 = replace(@s2,",AND"," AND ");
        set @s2 = replace(@s2,",OR"," OR ");
           set @s2 = replace(@s2,","," AND ");
   set @s2 = replace(@s2,"equal"," IN ");
           set @s2 = replace(@s2,"NOT >"," < ");
        
         if IN_AGGREGATION_3 != ''
     				then
                    insert into Solus_Segment( Segment_Name,Segment_Type,Segment_Scheme,Segment_Desc,Segment_Query_Condition,Segment_Human_Readable_Condition,Coverage,`Status`)
select IN_AGGREGATION_3,IN_AGGREGATION_5,IN_AGGREGATION_6,IN_AGGREGATION_4,@s2,replace(replace(replace(IN_AGGREGATION_2,"1=1",''),')',' '),'(',' '),0,"Not_Refreshed_Once";
                    
                    
set sql_safe_updates=0;
UPDATE Solus_Segment P,
    (SELECT DISTINCT
        (new_Col), old_col
    FROM
        (SELECT 
        Segment_Id,
            b.Segment_Query_Condition AS old_col,
            REPLACE(b.Segment_Query_Condition, a.UI_Variable_Name, a.SOLUS_Attribute) AS new_Col
    FROM
        Solus_Segment b
    LEFT JOIN Campaign_Segment_Dictionary a ON b.Segment_Query_Condition LIKE '%'
        || a.UI_Variable_Name
        || '%') AS t1
    WHERE
        new_Col NOT IN (SELECT 
                Segment_Query_Condition
            FROM
                Solus_Segment)) Q 
SET 
    Segment_Condition_Solus = Q.new_Col
WHERE
    P.Segment_Query_Condition = Q.old_col;

UPDATE Solus_Segment 
SET 
    Segment_Condition_Solus = Segment_Query_Condition
WHERE
    Segment_Condition_Solus IS NULL;
			 			
		                    SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Success") )),"]")
													    into @temp_result 
													   ;') ;
													      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
                            
                            Else 
							SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Segment Name Not Present") )),"]")
													    into @temp_result 
													   ;') ;
						  
                              
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
        end if;
        

 elseif IN_AGGREGATION_1 = 'Save_Segment_Custom_SQL'
        then
                set @s2 = IN_AGGREGATION_2;
        
         if IN_AGGREGATION_3 != ''
     				then
                    insert into Solus_Segment( Segment_Name,Segment_Type,Segment_Scheme,Segment_Desc,Segment_Query_Condition,Segment_Human_Readable_Condition,Coverage,`Status`)
select IN_AGGREGATION_3,IN_AGGREGATION_5,IN_AGGREGATION_6,IN_AGGREGATION_4,@s2,replace(replace(replace(IN_AGGREGATION_2,"1=1",''),')',' '),'(',' '),0,"Not_Refreshed_Once";
       
       set sql_safe_updates=0;
UPDATE Solus_Segment P,
    (SELECT DISTINCT
        (new_Col), old_col
    FROM
        (SELECT 
        Segment_Id,
            b.Segment_Query_Condition AS old_col,
            REPLACE(b.Segment_Query_Condition, a.UI_Variable_Name, a.SOLUS_Attribute) AS new_Col
    FROM
        Campaign_Segment b
    LEFT JOIN Campaign_Segment_Dictionary a ON b.Segment_Query_Condition LIKE '%'
        || a.UI_Variable_Name
        || '%') AS t1
    WHERE
        new_Col NOT IN (SELECT 
                Segment_Query_Condition
            FROM
                Solus_Segment)) Q 
SET 
    Segment_Condition_Solus = Q.new_Col
WHERE
    P.Segment_Query_Condition = Q.old_col;

UPDATE Solus_Segment 
SET 
    Segment_Condition_Solus = Segment_Query_Condition
WHERE
    Segment_Condition_Solus IS NULL;
			 			
                    
			 			
		                    SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Success") )),"]")
													    into @temp_result 
													   ;') ;
													      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
                            
                            Else 
							SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Segment Name Not Present") )),"]")
													    into @temp_result 
													   ;') ;
						  
                              
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
        end if;

            
elseif IN_AGGREGATION_1 = 'Campaign_Segment_List' 
        then
        
        SET @Segment_Scheme = IN_AGGREGATION_2;
        
        select count(1) into @cnt1 from Campaign_Segment where `status` = 'Refreshing';
        if @cnt1 > 0
        then
       set  @ref = "Not_Completed";
        else 
        set @ref = "Completed";
        end if;
UPDATE Solus_Segment 
SET 
    LastRefreshDate = ''
WHERE
    `status` = 'Not_Refreshed_Once';
         Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Segment_Id",Segment_Id,"Segment_Name",Segment_Name,"Segment_Desc",Segment_Desc,"Last Refresh Date",date_format(LastRefreshDate, "%d/%m %H:%i"),"Coverage",Coverage,"Segment_Human_Readable_Condition",right(Segment_Human_Readable_Condition,length(Segment_Human_Readable_Condition)-6),"ModifiedDate",date_format(ModifiedDate, "%d/%m %H:%i"),"Status",`Status`,"Refresh",@ref ,"progress",progress)order by ModifiedDate desc )),"]") 
														into @temp_result  from Campaign_Segment
                                                        where Segment_Scheme = "',@Segment_Scheme,'";             
												      ;') ;
                                                      
                                                      
 
                                                      
        
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
                            
		IF @temp_result is null 
        THEN
			SET @temp_result = 'No Segment Is Configured For This Scheme';
		END IF;
        
       
       
       elseif IN_AGGREGATION_1 = 'Dashboard_Segment_List' 
        then
        
			SET @Segment_Scheme = IN_AGGREGATION_2;
        
SELECT 
    COUNT(1)
INTO @cnt1 FROM
    Dashboard_Segment
WHERE
    `status` = 'Refreshing';
        if @cnt1 > 0
        then
       set  @ref = "Not_Completed";
        else 
        set @ref = "Completed";
        end if;
UPDATE Solus_Segment 
SET 
    LastRefreshDate = ''
WHERE
    `status` = 'Not_Refreshed_Once';
        
                                                      
                                                      
		Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Segment_Id",Segment_Id,"Segment_Name",Segment_Name,"Segment_Desc",Segment_Desc,"Last Refresh Date",date_format(LastRefreshDate, "%d/%m %H:%i"),"Coverage",Coverage,"Segment_Human_Readable_Condition",right(Segment_Human_Readable_Condition,length(Segment_Human_Readable_Condition)-6),"ModifiedDate",date_format(ModifiedDate, "%d/%m %H:%i"),"Status",`Status`,"Refresh",@ref ,"progress",progress)order by ModifiedDate desc )),"]") 
															into @temp_result
															from Dashboard_Segment
                                                            where Segment_Scheme = "',@Segment_Scheme,'";
														  ;') ;
                                                      
        
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
                            
                            
       IF @temp_result is null 
        THEN
			SET @temp_result = 'No Segment Is Configured For This Scheme';
		END IF;
       
       
	end if;
    
    



    
              
    if @temp_result is null
		Then
								
							Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Invalid Parameters") )),"]") 
														into @temp_result                       
												      ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
						
	End if;
							
    SET out_result_json = @temp_result;                                             
  


END

