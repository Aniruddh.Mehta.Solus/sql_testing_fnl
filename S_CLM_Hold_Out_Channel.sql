
CREATE OR REPLACE PROCEDURE `S_CLM_Hold_Out_Channel`(
IN vStartCnt bigint(10),
IN vEndCnt bigint(10)
 )
BEGIN
DECLARE v,v1,weekly_v,weekly_v1 int;
DECLARE Exe_ID int;
DECLARE Loop_Check_Var Int Default 0;
DECLARE cur_Channel_Name VARCHAR(128);


DECLARE curALL_Channel_Names CURSOR For SELECT DISTINCT ChannelName from Event_Master;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET Loop_Check_Var=1;


SET sql_safe_updates=0;
SET foreign_key_checks=0;


INSERT into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
							values ('S_Hold_Out_Channel', '',now(),null,'Started', 0909213);
SET Exe_ID = F_Get_Execution_Id();        


SET Loop_Check_Var = FALSE;
OPEN curALL_Channel_Names;
LOOP_ALL_Channel_Names: LOOP
	FETCH curALL_Channel_Names into cur_Channel_Name;
	IF Loop_Check_Var THEN
	   LEAVE LOOP_ALL_Channel_Names;
	END IF;
	
		SELECT count(1) into @vExistingHoldOut from Hold_Out_Channel
        where Channel_Name = Cur_Channel_Name;


		IF @vExistingHoldOut = 0 
		THEN
			SELECT `Value` into @percentage from M_Config where `Name`='Channel_Level_Hold_Out_Percentage';
			SELECT count(Customer_Id) into v from V_CLM_Customer_One_View;
			SELECT round(v*(@percentage/100)) into v1;
			SET @sql1=concat('insert ignore into Hold_Out_Channel (Customer_Id,Channel_Name,Inserted_On)','(
			select Customer_Id,"',cur_Channel_Name,'",now() 
			FROM V_CLM_Customer_One_View
			WHERE Customer_Id Not IN (select Customer_Id from Hold_Out)
			ORDER BY RAND() limit ',v1,
			')');
		ELSE
			SELECT count(Customer_Id) into v from V_CLM_Customer_One_View where Customer_ID between vStartCnt and vEndCnt and Is_New_Cust_Flag="Y" ;
			SELECT round(v*(@percentage/100)) into v1;
			SET @sql1=concat('INSERT ignore into Hold_Out_Channel (Customer_Id,Channel_Name,Inserted_On)','(
			SELECT Customer_Id,"',cur_Channel_Name,'",now() 
			FROM V_CLM_Customer_One_View 
			WHERE Customer_Id between ',vStartCnt,' and ',vEndCnt,'  
			AND Is_New_Cust_Flag="Y"
            AND Customer_Id Not IN (select Customer_Id from Hold_Out)
			ORDER BY RAND() limit ',v1,
			')');
		END IF;

		prepare stmt from @sql1;
		execute stmt;
		deallocate prepare stmt;
			

END LOOP LOOP_ALL_Channel_Names;


UPDATE CDM_Customer_Master 
SET 
    Is_New_Cust_Flag = 'N'
WHERE Customer_Id between vStartCnt and vEndCnt
AND Is_New_Cust_Flag <> 'N';

END

