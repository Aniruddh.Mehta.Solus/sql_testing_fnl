DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_loyalty_metrics`(IN vBatchSize BIGINT)
BEGIN
DECLARE vStart_Cnt BIGINT DEFAULT 0;
DECLARE vEnd_Cnt BIGINT DEFAULT 0;
DECLARE vRec_Cnt BIGINT DEFAULT 0;

DELETE from log_solus where module = 'S_dashboard_loyalty_metrics';
SET SQL_SAFE_UPDATES=0;

SET @Start_Date = date_format(date_sub(concat(date_format(current_date(),"%Y%m"),'01'), INTERVAL 14 MONTH),"%Y%m");
SET @End_Date = date_format(current_date(),"%Y%m");

DROP TABLE IF EXISTS Dashboard_Loyalty_Metrics_Temp;
  CREATE TABLE IF NOT EXISTS Dashboard_Loyalty_Metrics_Temp ( 
	`Customer_Id` bigint(20) NOT NULL,
	`Bill_Year_Month` int(11) NOT NULL,
	`Tier` varchar(50),
	`Membership_Status` varchar(3),
	`Segment` varchar(20),
	`Region` varchar(128),
    `Store_id` int(10),
    `Store_Name` VARCHAR(255),
    `Store_Region` VARCHAR(255),
    `Store_City` VARCHAR(255),
	`Visits` int(11),
	`Revenue` int(128),
	`Repeat_Rev` int(128),
	`ABV` int(11),
	`Qty` int(11),
	`BS` int(11),
    `Store_Rev` int(200),
    `Region_rank` int(10),
	`National_rank` int(10),
    `Email_Capture` Varchar(255),
    `Mobile_Capture` Varchar(255),
    `Name_Capture` Varchar(255),
    `Category` Varchar(255),
    `Address_Capture` Varchar(255),
    `DOB_Capture` Varchar(255),
    `Gender_Capture` Varchar(255),
    `Anniversary_Capture` Varchar(255)
  ) ENGINE = INNODB DEFAULT CHARACTER SET=LATIN1; 
   
 
  ALTER TABLE Dashboard_Loyalty_Metrics_Temp 
    ADD INDEX (Customer_Id),     
    ADD INDEX (Bill_Year_Month), 
    ADD INDEX (Tier),
	ADD INDEX (Membership_Status),     
    ADD INDEX (Segment), 
    ADD INDEX (Region),
	ADD INDEX (Visits),     
    ADD INDEX (Revenue), 
    ADD INDEX (Repeat_Rev),
	ADD INDEX (ABV),     
    ADD INDEX (Qty), 
    ADD INDEX (BS); 
    
DROP TABLE IF EXISTS Dashboard_Loyalty_Metrics_Cat_Temp;
CREATE TABLE IF NOT EXISTS Dashboard_Loyalty_Metrics_Cat_Temp ( 
	`Customer_Id` bigint(20) NOT NULL,
	`Bill_Year_Month` int(11) NOT NULL,
	`Tier` varchar(50),
	`Membership_Status` varchar(3),
	`Segment` varchar(20),
	`Region` varchar(128),
    `Category` varchar(255)
  ) ENGINE = INNODB DEFAULT CHARACTER SET=LATIN1;

ALTER TABLE Dashboard_Loyalty_Metrics_Cat_Temp 
    ADD INDEX (Customer_Id),     
    ADD INDEX (Bill_Year_Month), 
    ADD INDEX (Tier),
	ADD INDEX (Membership_Status),     
    ADD INDEX (Segment), 
    ADD INDEX (Region);

SELECT 
    Customer_Id
INTO vRec_Cnt FROM
    CDM_Customer_Master
ORDER BY Customer_Id DESC
LIMIT 1;
	SET vStart_Cnt=0;
	SET vEnd_Cnt=0; 
LOOP_ALL_CUSTOMERS: LOOP
	SET vEnd_Cnt = vStart_Cnt + vBatchSize;
	SELECT vStart_Cnt, vEnd_Cnt, CURRENT_TIMESTAMP();
    IF vStart_Cnt  >= vRec_Cnt THEN
		LEAVE LOOP_ALL_CUSTOMERS;
	END IF;  
INSERT INTO log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_loyalty_metrics',vStart_Cnt,vEnd_Cnt,'Loading data with valid LOV_Id');

INSERT INTO Dashboard_Loyalty_Metrics_Temp
(
Customer_Id,
Bill_Year_Month,
Tier,
Membership_Status,
Region,
Visits,
Revenue,
Qty
)
SELECT 
B.Customer_Id, 
B.Bill_Year_Month, 
L.LOV_Key AS Tier, 
CASE WHEN B.Customer_Id IN (SELECT p.Customer_Id FROM CDM_Customer_PII_Master p WHERE Mobile <> "") THEN "Y" ELSE "N" 
END AS Membership_Status,
S.Store_Region AS Region,
COUNT(distinct(B.Bill_Header_Id)) AS Visits, 
SUM(Sale_Net_Val) AS Revenue,
SUM(Sale_Qty) AS Qty 
FROM CDM_Customer_Master C ,CDM_LOV_Master L, CDM_Bill_Details B, CDM_Store_Master S 
WHERE C.Tier_Name_LOV_Id = L.LOV_Id 
AND C.Customer_ID = B.Customer_Id 
AND S.Store_Id= B.Store_Id  
AND C.Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt
GROUP BY B.Customer_Id, B.Bill_Year_Month;

INSERT into Dashboard_Loyalty_Metrics_Cat_Temp
  (Customer_Id,
  Bill_Year_Month,
  Membership_Status,
  Region,
  Category)
  select 
  B.Customer_Id, 
  B.Bill_Year_Month,
  CASE WHEN B.Customer_Id IN (SELECT p.Customer_Id FROM CDM_Customer_PII_Master p WHERE Mobile <> "") THEN "Y" ELSE "N" 
END AS Membership_Status,
  S.Store_Region AS Region,
  L.LOV_Key as Category
  from CDM_Bill_Details B, CDM_LOV_Master L, CDM_Store_Master S, CDM_Product_Master P
  where B.Store_ID = S.Store_ID 
  and P.Product_Id = B.Product_Id
  and P.Cat_LOV_Id = L.LOV_id
AND B.Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt
  group by B.Bill_Details_Id;

INSERT INTO log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_loyalty_metrics',vStart_Cnt,vEnd_Cnt,'Loading rest of the Customers');

INSERT INTO Dashboard_Loyalty_Metrics_Temp
(
Customer_Id,
Bill_Year_Month,
Membership_Status,
Region,
Visits,
Revenue,
Qty
)
SELECT 
B.Customer_Id, 
B.Bill_Year_Month, 
CASE WHEN B.Customer_Id IN (SELECT p.Customer_Id FROM CDM_Customer_PII_Master p WHERE Mobile <> "") THEN "Y" ELSE "N" 
END AS Membership_Status,
S.Store_Region AS Region,
COUNT(distinct(B.Bill_Header_Id)) AS Visits, 
SUM(Sale_Net_Val) AS Revenue, 
SUM(Sale_Qty) AS Qty 
FROM CDM_Customer_Master C , CDM_Bill_Details B, CDM_Store_Master S 
WHERE C.Customer_ID = B.Customer_Id 
AND S.Store_Id= B.Store_Id 
AND C.Customer_Id IN (SELECT customer_id FROM CDM_Bill_Details WHERE Customer_Id NOT IN (SELECT customer_Id FROM Dashboard_Loyalty_Metrics_Temp))
AND C.Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt
GROUP BY B.Customer_Id, B.Bill_Year_Month;

INSERT INTO log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_loyalty_metrics',vStart_Cnt,vEnd_Cnt,'Loading LEADS');

INSERT INTO Dashboard_Loyalty_Metrics_Temp
(
Customer_Id,
Tier,
Membership_Status
)
SELECT 
C.Customer_Id, 
L.LOV_Key AS Tier,
CASE WHEN C.Customer_Id IN 
(
SELECT p.Customer_Id FROM CDM_Customer_PII_Master p WHERE Mobile <> ""
) THEN "Y" ELSE "N" 
END AS Membership_Status
FROM CDM_Customer_Master C, CDM_LOV_Master L 
WHERE C.Tier_Name_LOV_Id = L.LOV_Id
AND C.Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt
AND C.Customer_Id IN 
(
SELECT DISTINCT(C.customer_id) 
FROM CDM_Customer_Master C, CDM_LOV_Master L 
WHERE C.Tier_Name_LOV_Id = L.LOV_Id
AND C.Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt
AND C.Customer_Id 
NOT IN (SELECT customer_Id FROM Dashboard_Loyalty_Metrics_Temp)
)
GROUP BY C.Customer_Id;

UPDATE Dashboard_Loyalty_Metrics_Temp A, CDM_Customer_Segment B
SET A.Segment = B.CLMSegmentName
WHERE A.Customer_Id = B.Customer_Id
AND A.Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt;

UPDATE Dashboard_Loyalty_Metrics_Cat_Temp A, CDM_Customer_Segment B
SET A.Segment = B.CLMSegmentName
WHERE A.Customer_Id = B.Customer_Id
AND A.Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt;

SET vStart_Cnt = vEnd_Cnt;
END LOOP LOOP_ALL_CUSTOMERS; 

INSERT INTO log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_loyalty_metrics',vStart_Cnt,vEnd_Cnt,'UPDATING STORE VALUES');

UPDATE Dashboard_Loyalty_Metrics_Temp D, CDM_Store_Master S, CDM_Bill_Details B
SET 
D.Store_id = S.Store_ID,
D.Store_name =S.Store_Name,
D.Store_Region =S.Store_Region,
D.Store_City= S.Store_City
WHERE 
D.Customer_id = B.Customer_id and B.Store_id = S.Store_Id;

INSERT INTO log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_loyalty_metrics',vStart_Cnt,vEnd_Cnt,'Store rev and store ranks');

update Dashboard_Loyalty_Metrics_Temp D,(Select customer_ID,Store_ID, Sum(Revenue) as rev from Dashboard_Loyalty_Metrics_Temp  where Store_ID is not NULL
group by Store_Id) A
set 
D.Store_Rev = A.rev
where D.Store_ID = A.Store_ID;

update Dashboard_Loyalty_Metrics_Temp D,
(select Store_ID, Store_region, Store_Name, 
dense_rank() over (partition by Store_Region order by Store_Rev desc) Store_rank 
from Dashboard_Loyalty_Metrics_Temp 
group by 1
order by Store_rank) A
set 
D.Region_rank = A.Store_Rank
where D.Store_ID = A.Store_ID;


update Dashboard_Loyalty_Metrics_Temp D,
(select Store_ID, Store_region, Store_Name, 
dense_rank() over (order by Store_Rev desc) National_rank 
from Dashboard_Loyalty_Metrics_Temp 
group by 1
order by National_rank) A
set 
D.National_rank = A.National_rank
where D.Store_ID = A.Store_ID;


INSERT INTO log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_loyalty_metrics',vStart_Cnt,vEnd_Cnt,'all data capture');


UPDATE Dashboard_Loyalty_Metrics_Temp D,
    (SELECT 
        customer_id,
            CASE
                WHEN Email_domain <> '' THEN 'Y'
                ELSE 'N'
            END AS 'Email_Capture'
    FROM
        CDM_Customer_Master) A 
SET 
    D.Email_Capture = A.Email_Capture
WHERE
    D.Customer_ID = A.customer_ID;



UPDATE Dashboard_Loyalty_Metrics_Temp D,
    (SELECT 
        customer_id,
            CASE
                WHEN Address_Line1 <> '' THEN 'Y'
                ELSE 'N'
            END AS 'Address_Capture'
    FROM
        CDM_Customer_PII_Master) A 
SET 
    D.Address_Capture = A.Address_Capture
WHERE
    D.Customer_ID = A.customer_ID;


UPDATE Dashboard_Loyalty_Metrics_Temp D,
    (SELECT 
        customer_id,
            CASE
                WHEN Gender_LOV_Id NOT IN (0 , - 1) THEN 'Y'
                ELSE 'N'
            END AS 'Gender_Capture'
    FROM
        CDM_Customer_Master) A 
SET 
    D.Gender_Capture = A.Gender_Capture
WHERE
    D.Customer_ID = A.customer_ID;


UPDATE Dashboard_Loyalty_Metrics_Temp D,
    (SELECT 
        customer_id,
            CASE
                WHEN DoA IS NOT NULL THEN 'Y'
                ELSE 'N'
            END AS 'Anniversary_Capture'
    FROM
        CDM_Customer_Master) A 
SET 
    D.Anniversary_Capture = A.Anniversary_Capture
WHERE
    D.Customer_ID = A.customer_ID;


UPDATE Dashboard_Loyalty_Metrics_Temp D,
    (SELECT 
        customer_id,
            CASE
                WHEN Mobile <> '' THEN 'Y'
                ELSE 'N'
            END AS 'Mobile_Capture'
    FROM
        CDM_Customer_PII_Master) A 
SET 
    D.Mobile_Capture = A.Mobile_Capture
WHERE
    D.Customer_ID = A.customer_ID;


UPDATE Dashboard_Loyalty_Metrics_Temp D,
    (SELECT 
        customer_id,
            CASE
                WHEN Full_Name <> '' THEN 'Y'
                ELSE 'N'
            END AS 'Name_Capture'
    FROM
        CDM_Customer_PII_Master) A 
SET 
    D.Name_Capture = A.Name_Capture
WHERE
    D.Customer_ID = A.customer_ID;


UPDATE Dashboard_Loyalty_Metrics_Temp D,
    (SELECT 
        customer_id,
            CASE
                WHEN Dob IS NOT NULL THEN 'Y'
                ELSE 'N'
            END AS 'DOB_Capture'
    FROM
        CDM_Customer_Master) A 
SET 
    D.DOB_Capture = A.DOB_Capture
WHERE
    D.Customer_ID = A.customer_ID;


UPDATE Dashboard_Loyalty_Metrics_Cat_Temp A,
    CDM_LOV_Master L,
    CDM_Customer_Master C 
SET 
    A.Tier = L.LOV_Key
WHERE
    L.LOV_Id = C.Tier_Name_LOV_Id
        AND A.Customer_Id = C.Customer_Id;


UPDATE Dashboard_Loyalty_Metrics_Cat_Temp 
SET 
    Category = REPLACE(Category, CHAR(13), '');

UPDATE Dashboard_Loyalty_Metrics_Temp loyal,
    (SELECT 
			bill.Customer_id,
            bill.Bill_Year_Month AS Bill_Year_Month,
            SUM(bill.Sale_Net_Val) AS repeat_rev
    FROM
        Dashboard_Insights_Repeat_Cohorts_Customers_Temp rep, CDM_Bill_Details bill
    WHERE
        bill.Customer_Id = rep.Customer_id
        and bill.Bill_Date = rep.Bill_Date
            AND rep.Is_Existing_Customer = 1
            
    GROUP BY 1 , 2) existing 
SET 
    loyal.Repeat_Rev = existing.repeat_rev
WHERE
    loyal.Customer_id = existing.Customer_id
AND	loyal.Bill_Year_Month = existing.Bill_Year_Month;

END$$
DELIMITER ;