
CREATE OR REPLACE PROCEDURE `S_CLM_Unique_Offer_Code_Assignment`(IN OnDemandEventId bigint(20),IN Communication_Template_ID_cur1 BIGINT(20),IN vStart_CntCust BIGINT(20),IN vEnd_CntCust BIGINT(20), OUT vSuccess int (4), OUT vErrMsg Text )
BEGIN
	
    /* Version : Solus 2.1 20221124 */
	DECLARE Exe_ID ,V_Communication_Template_ID, vRec_Cnt,v_cnt_req_offercodes int;
	DECLARE COMMUNICATION_OFFER_CODE VARCHAR(128) DEFAULT NULL;	
	
	
	set foreign_key_checks=0;
    set sql_safe_updates=0;
	
	
	SELECT Offer_Code into COMMUNICATION_OFFER_CODE FROM Communication_Template where Id= Communication_Template_ID_cur1;
	
	
	IF  COMMUNICATION_OFFER_CODE ="CUSTOMER_LEVEL_OFFER"	
		THEN
				
			INSERT INTO ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
			VALUE ('S_Unique_Offer_Code_Assignment', '',now(),null,'Started', 090921);
			
			SELECT * FROM ETL_Execution_Details ORDER BY Execution_Id DESC LIMIT 1;
			SET Exe_ID = F_Get_Execution_Id();
			
            
            SET @t_initialize=now(); 
            
			INSERT IGNORE INTO Archived_CLM_Customer_Level_Offer_Code SELECT * FROM CLM_Customer_Level_Offer_Code WHERE CURRENT_DATE >  End_Date;
			
			DELETE FROM CLM_Customer_Level_Offer_Code WHERE CURRENT_DATE > End_Date;
            
            SET @t_enable=now();
			
			UPDATE CLM_Customer_Level_Offer_Code cclocc, Communication_Template ct 
			SET  cclocc.Communication_Template_ID = ct.ID
			WHERE   ct.`Name` = cclocc.Communication_Template
					AND ct.State = 'Enabled' 
					AND ct.Offer_Code = 'CUSTOMER_LEVEL_OFFER'; 
			
			SET vRec_Cnt = 0;
			
            SET @t_count=now();
            
			SELECT  COUNT(1) INTO vRec_Cnt
			FROM Event_Execution EE
			WHERE   EE.Offer_Code = 'CUSTOMER_LEVEL_OFFER'
					AND EE.Is_Target = 'Y'
					AND Communication_Template_ID = Communication_Template_ID_cur1
					AND EE.In_Control_Group IS NULL
					AND EE.In_Event_Control_Group IS NULL;
			
						SELECT COUNT(Offer_Code) INTO v_cnt_req_offercodes 
			FROM  CLM_Customer_Level_Offer_Code
			WHERE  Communication_Template_ID = Communication_Template_ID_cur1
					AND State != 'assigned'
					AND CURRENT_DATE BETWEEN Start_Date AND End_Date;
			
			IF vRec_Cnt IS NULL 
				THEN 
					SET vRec_Cnt = 0;
			END IF; 
			
			IF v_cnt_req_offercodes IS NULL 
				THEN 
					SET v_cnt_req_offercodes = 0;
  			END IF;
			
			IF (v_cnt_req_offercodes < vRec_Cnt)
				THEN
					SIGNAL SQLSTATE '45000'
					SET MESSAGE_TEXT = 'Please note code <<Customer_Level_Offer>> is case sensitive:In sufficient Offer code in CLM_Customer_Level_Offer_Code!';
					SET @vSuccess=0;
					SET @vErrMsg='Please note code <<Customer_Level_Offer>> is case sensitive:In sufficient Offer code in CLM_Customer_Level_Offer_Code! or you have less offer as compared tot eh customer selected';
			ELSE
				Set @vSuccess=1;
			END IF;
			
            SET @t_update_EE = now();
            
			SET @EE_ROW_NUMBER=0;
			SET @OFF_ROW_NUMBER=0;	
			
			UPDATE IGNORE Event_Execution E,
				(SELECT (@EE_ROW_NUMBER:=@EE_ROW_NUMBER + 1) AS id,Event_Execution_ID
					FROM Event_Execution
					WHERE Offer_Code = 'Customer_Level_Offer'
							AND Is_Target = 'Y'
							AND Communication_Template_ID = Communication_Template_ID_cur1
							AND In_Control_Group IS NULL
							AND Customer_Id between vStart_CntCust AND vEnd_CntCust
							AND In_Event_Control_Group IS NULL
				) AS EE,
				(SELECT (@OFF_ROW_NUMBER:=@OFF_ROW_NUMBER + 1) AS id, Offer_Code
					FROM CLM_Customer_Level_Offer_Code
					WHERE  Communication_Template_ID = Communication_Template_ID_cur1
							AND State != 'assigned'
							AND CURRENT_DATE BETWEEN Start_Date AND End_Date
					LIMIT VREC_CNT
				) AS CLOC 
			
			SET E.Offer_Code = CLOC.Offer_Code
			WHERE 	EE.id = CLOC.id
					AND E.Event_Execution_ID = EE.Event_Execution_ID
					AND E.Communication_Template_ID = Communication_Template_ID_cur1
					AND E.Offer_Code = 'Customer_Level_Offer'
					AND E.Is_Target = 'Y'
					AND E.In_Control_Group IS NULL
					AND Customer_Id between vStart_CntCust AND vEnd_CntCust
					AND E.In_Event_Control_Group IS NULL;
                    
                    
			SET @t_update_CLOC = now();

			UPDATE CLM_Customer_Level_Offer_Code CLOC, Event_Execution EE 
			SET  CLOC.State = 'assigned',
				CLOC.Updated_On = NOW()
			WHERE CLOC.Offer_Code = EE.Offer_Code AND CLOC.Communication_Template_ID = Communication_Template_ID_cur1;
			
			SET @t_end  = now();
            
            
            /* Set Offer Code To NULL For Left Out Customers */
              UPDATE Event_Execution
			  SET Offer_Code = NULL
			  WHERE Customer_Id between vStart_CntCust AND vEnd_CntCust
			  AND Offer_Code = "Customer_Level_Offer";
            
			UPDATE ETL_Execution_Details 
				SET 
					Status = 'Succeeded',
					End_Time = NOW()
				WHERE
					Procedure_Name = 'S_Unique_Offer_Code_Assignment'
					AND Execution_ID = Exe_ID;
				  SELECT concat('S_Unique_Offer_Code_Assignment : COMPLETED ', now(), ' LOAD ID :', Exe_ID) as '';

	END IF;	

	IF  COMMUNICATION_OFFER_CODE ="Reminder_Offer"	
		THEN
		
		INSERT INTO ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
			VALUE ('S_Unique_Offer_Code_Assignment', '',now(),null,'Started', 090921);
			
			SELECT * FROM ETL_Execution_Details ORDER BY Execution_Id DESC LIMIT 1;
			SET Exe_ID = F_Get_Execution_Id();
            
            SELECT Reminder_Parent_Event_Id INTO @Reminder_CampTrigger_Id FROM Event_Master WHERE ID = OnDemandEventId;
            
            SELECT Reminder_Days INTO @Reminder_Days FROM Event_Master WHERE ID = OnDemandEventId;
            
         /* Updating the Reminder Offer Code From EEH*/   
        UPDATE Event_Execution EE,Event_Execution_History EEH
		SET EE.Offer_Code =EEH.Offer_Code
		WHERE EE.Customer_Id between vStart_CntCust AND vEnd_CntCust
			AND EEH.Customer_Id between vStart_CntCust AND vEnd_CntCust
            AND    EE.Customer_Id=EEH.Customer_Id
			AND EEH.Event_Id=(Select Id from Event_Master TM where TM.CampTriggerId=@Reminder_CampTrigger_Id)
			AND EE.Event_Id=OnDemandEventId
			AND EEH.Event_Execution_Date_ID =DATE_FORMAT(DATE_SUB(EE.Event_Execution_Date_ID,INTERVAL @Reminder_Days DAY),'%Y%m%d');
            
          /*Set Offer Code to NULL For Left Out Customers */
          UPDATE Event_Execution
          SET Offer_Code = NULL
          WHERE Customer_Id between vStart_CntCust AND vEnd_CntCust
          AND Offer_Code = "Reminder_Offer";
	
			
			UPDATE ETL_Execution_Details 
				SET 
					Status = 'Succeeded',
					End_Time = NOW()
				WHERE
					Procedure_Name = 'S_Unique_Offer_Code_Assignment'
					AND Execution_ID = Exe_ID;
				  SELECT concat('S_Unique_Offer_Code_Assignment : COMPLETED ', now(), ' LOAD ID :', Exe_ID) as '';
				  
				  
  
	END IF;	


 set @sql_msg_log=
                            CONCAT_WS( '', ' Time(Initialize): ',timediff(@t_enable,@t_initialize),
                                    ' Time(Enable): ',timediff(@t_count,@t_enable),
                                    ' Time(Count): ',timediff(@t_update_EE,@t_count),
                                    ' Time(Update_EE): ',timediff(@t_update_CLOC,@t_update_EE),
                                    ' Time(Update_CLOC): ',timediff(@t_end,@t_update_CLOC)
                                    ) ;
        SELECT @sql_msg_log;


END

