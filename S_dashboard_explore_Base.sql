DELIMITER $$
CREATE or replace PROCEDURE `S_dashboard_explore_Base`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
        
    IF IN_AGGREGATION_2 = 'RANDOM'
    THEN
		set @sql1 = concat('select Customer_Id into @cust_id from CDM_Customer_Segment where CLMSegmentName = "',IN_AGGREGATION_3,'" order by rand() LIMIT 1;');
        select @sql1;
        PREPARE statement from @sql1;
		Execute statement;
		Deallocate PREPARE statement;
	ELSE 
		set @cust_id = IN_AGGREGATION_2;
    END IF;
    select @cust_id;
    SET @filter_cond = ' 1=1 ';
    
    IF IN_MEASURE = "RECENCY" OR IN_MEASURE = "CARDS"
	THEN
		SET @Query_Measure=' Recency ';
		SET @recency_measure=@Query_Measure;
        SET @recency_table = ' CDM_Customer_Time_Dependent_Var ';
        SET @table_used=@recency_table;
        select @Query_Measure,@table_used;
	END IF;
	IF IN_MEASURE = "FREQ_12MTH" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' AP_Num_of_Visits ';
		SET @freq12mth_measure=@Query_Measure;
        SET @freq12mth_table= ' CDM_Customer_AP_Var ';
        SET @table_used=@freq12mth_table;
	END IF;
    IF IN_MEASURE = "FREQ_EVER" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' Lt_Num_of_Visits ';
		SET @freqever_measure=@Query_Measure;
        SET @freqever_table=' CDM_Customer_TP_Var ';
        SET @table_used=@freqever_table;
	END IF;
     IF IN_MEASURE = "ADGBT" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' Lt_ADGBT ';
		SET @adgbt_measure=@Query_Measure;
        SET @adgbt_table=' CDM_Customer_TP_Var ';
        SET @table_used=@adgbt_table;
	END IF;
    IF IN_MEASURE = "TENURE" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' ifnull(round((datediff(current_date(),FTD))/30.5),0) ';
        SET @Query_Measure=' Tenure ';
		SET @tenure_measure=@Query_Measure;
        SET @tenure_table=' CDM_Customer_TP_Var ';
        SET @table_used=@tenure_table;
	END IF;
    IF IN_MEASURE = "VALUE_12MTH" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' AP_Rev ';
		SET @value12mth_measure=@Query_Measure;
        SET @value12mth_table=' CDM_Customer_AP_Var ';
        SET @table_used=@value12mth_table;
	END IF;
    IF IN_MEASURE = "VALUE_EVER" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' Lt_Rev ';
		SET @valueever_measure=@Query_Measure;
        SET @valueever_table=' CDM_Customer_TP_Var ';
        SET @table_used=@valueever_table;
	END IF;
    IF IN_MEASURE = "ABV" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' Lt_ABV ';
		SET @abv_measure=@Query_Measure;
        SET @abv_table=' CDM_Customer_TP_Var ';
        SET @table_used=@abv_table;
	END IF;
    IF IN_MEASURE = "DISCOUNT" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' Lt_Disc_Percent ';
		SET @discount_measure=@Query_Measure;
        SET @discount_table=' CDM_Customer_TP_Var ';
        SET @table_used=@dicount_table;
	END IF;
	IF IN_MEASURE = "FULL_PRICE" OR IN_MEASURE = "CARDS" 
	THEN
		
        
        SET @Query_Measure=' CASE WHEN Lt_Disc_Percent= 0 then 100 else(floor(rand()*(99-80+1))+80) END ';
        SET @fullprice_measure=@Query_Measure;
		SET @fullprice_table=' CDM_Customer_TP_Var ';
        SET @table_used=@fullprice_table;
        set @filter_cond=' Sale_Disc_Val <=0 ';
        set @fullprice_cond=@filter_cond;
	END IF;
	IF IN_MEASURE = "PRODUCT_RANGE" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' LT_Distinct_Prod_Cnt ';
		SET @productrange_measure=@Query_Measure;
        SET @productrange_table=' CDM_Customer_TP_Var ';
        SET @table_used=@productrange_table;
	END IF;
	IF IN_MEASURE = "CAT_RANGE" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' LT_Distinct_Cat_Cnt ';
		SET @catrange_measure=@Query_Measure;
        SET @catrange_table=' CDM_Customer_TP_Var ';
        SET @table_used=@catrange_table;
	END IF;
    IF IN_MEASURE = "FAV_PROD" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' ifnull(LOV_Comm_Value,"NA") ';
		SET @favprod_measure=@Query_Measure;
        SET @favprod_table=' CDM_Customer_TP_Var favp left join CDM_LOV_Master favp_lov on favp.LT_Fav_Prod_LOV_Id=favp_lov.LOV_Id  ';
        SET @table_used=@favprod_table;
        SET @table_per=' CDM_Customer_TP_Var ';
        SET @Mesaure_per=' LT_Fav_Prod_LOV_Id ';
        SET @pct_bills_query = concat('select count(distinct Bill_Header_Id)/(select count(distinct Bill_Header_Id) from CDM_Bill_Details where Customer_Id = ',@cust_id,') as pct from CDM_Bill_Details where Customer_Id = ',@cust_id,' and Product_Id = (select Product_Id from CDM_Product_Master where Product_LOV_Id = (select LT_Fav_Prod_LOV_Id from CDM_Customer_TP_Var where Customer_Id = ',@cust_id,'))');
	END IF;
    IF IN_MEASURE = "FAV_CAT" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' CAST((CASE WHEN substring_index(substring_index(Product_Genome,"___",2),"___",-1) in("","#N/A") THEN "NA" ELSE substring_index(substring_index(Product_Genome,"___",2),"___",-1) END)  as CHAR) ';
		SET @favcat_measure=@Query_Measure;
        SET @favcat_table=' CDM_Customer_TP_Var favs left join CDM_Product_Master favs_lov on favs.LT_Fav_Prod_LOV_Id=favs_lov.Product_LOV_Id ';
        SET @table_used=@favcat_table;
        SET @table_per=' CDM_Customer_TP_Var ';
        SET @Mesaure_per=' LT_Fav_Prod_LOV_Id ';
        SET @pct_bills_query = concat('select count(distinct Bill_Header_Id)/(select count(distinct Bill_Header_Id) from CDM_Bill_Details where Customer_Id = ',@cust_id,') as pct from CDM_Bill_Details where Customer_Id = ',@cust_id,' and Product_Id = (select Product_Id from CDM_Product_Master where Product_LOV_Id = (select LT_Fav_Prod_LOV_Id from CDM_Customer_TP_Var where Customer_Id = ',@cust_id,'))');
	END IF;
    IF IN_MEASURE = "NUM_STORES" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' LT_Fav_Cat_LOV_Id ';
		SET @numstores_measure=@Query_Measure;
        SET @numstores_table=' CDM_Customer_TP_Var ';
        SET @table_used=@numstores_table;
	END IF;
    IF IN_MEASURE = "FAV_STORE" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' LOV_Value ';
		SET @favstore_measure=@Query_Measure;
        SET @favstore_table=' CDM_Customer_TP_Var favs left join CDM_LOV_Master favs_lov on favs.LT_Fav_Store_LOV_Id=favs_lov.LOV_Id  ';
        SET @table_used=@favstore_table;
        SET @table_per=' CDM_Customer_TP_Var ';
        SET @Mesaure_per=' LT_Fav_Store_LOV_Id ';
        SET @pct_bills_query = concat('select count(distinct Bill_Header_Id)/(select count(distinct Bill_Header_Id) from CDM_Bill_Details where Customer_Id = ',@cust_id,') as pct from CDM_Bill_Details where Customer_Id = ',@cust_id,' and Store_Id = (select Store_Id from CDM_Store_Master where LOV_Id = (select LT_Fav_Store_LOV_Id from CDM_Customer_TP_Var where Customer_Id = ',@cust_id,'))');
	END IF;
    IF IN_MEASURE = "TOD" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' CASE WHEN Customer_Id%3=1 THEN "Morning" when Customer_Id%3=2 THEN "Evening" else "Afternoon" END ';
		SET @tod_measure=@Query_Measure;
        SET @tod_table=' CDM_Customer_TP_Var ';
        SET @table_used=@tod_table;
        SET @table_per=' CDM_Customer_TP_Var ';
        SET @Mesaure_per=' Lt_Fav_Time_Of_Day ';
        SET @pct_bills_query = concat('select count(distinct Bill_Header_Id)/(select count(distinct Bill_Header_Id) from CDM_Bill_Details where Customer_Id = ',@cust_id,') as pct from CDM_Bill_Details where Customer_Id = ',@cust_id,' and Bill_Day_Week = (select Lt_Fav_Time_Of_Day from CDM_Customer_TP_Var where Customer_Id = ',@cust_id,')');
	END IF;
    IF IN_MEASURE = "DOW" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' Lt_Fav_Day_Of_Week ';
		SET @dow_measure=' CASE WHEN Lt_Fav_Day_Of_Week = 0 THEN "Sunday"
							WHEN Lt_Fav_Day_Of_Week = 1 THEN "Monday"
                            WHEN Lt_Fav_Day_Of_Week = 2 THEN "Tuesday"
                            WHEN Lt_Fav_Day_Of_Week = 3 THEN "Wednesday"
                            WHEN Lt_Fav_Day_Of_Week = 4 THEN "Thursday"
                            WHEN Lt_Fav_Day_Of_Week = 5 THEN "Friday"
                            ELSE "Saturday" END';
        SET @dow_table=' CDM_Customer_TP_Var ';
        SET @table_used=@dow_table;
        SET @table_per=' CDM_Customer_TP_Var ';
        SET @Mesaure_per=' Lt_Fav_Day_Of_Week ';
        SET @pct_bills_query = concat('select count(distinct Bill_Header_Id)/(select count(distinct Bill_Header_Id) from CDM_Bill_Details where Customer_Id = ',@cust_id,') as pct from CDM_Bill_Details where Customer_Id = ',@cust_id,' and Bill_Day_Week = (select Lt_Fav_Day_Of_Week from CDM_Customer_TP_Var where Customer_Id = ',@cust_id,')');
	END IF;
    IF IN_MEASURE = "REACH" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' count(1) ';
		SET @reach_measure=@Query_Measure;
        SET @reach_table=' Event_Execution_History ';
        SET @table_used=@reach_table;
	END IF;
    IF IN_MEASURE = "RESPONSE" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' ifnull(count(distinct Bill_Header_Id),0) ';
		SET @response_measure=@Query_Measure;
        SET @response_table=' Event_Attribution_History ';
        SET @table_used=@response_table;
	END IF;
    IF IN_MEASURE = "DATA_COMPLETE" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' "NA" ';
		SET @datacomplete_measure=@Query_Measure;
        SET @datacomplete_table=' CDM_Customer_TP_Var ';
        SET @table_used=@datacomplete_table;
	END IF;
    IF IN_MEASURE = "POINTS" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' ifnull(Point_Balance,0) ';
		SET @points_measure=@Query_Measure;
        SET @points_table=' CDM_Customer_Master ';
        SET @table_used=@points_table;
	END IF;
    IF IN_MEASURE = "REDEMPTION" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' ifnull(Lt_Pts_Redemption_FD,0) ';
		SET @redemption_measure=@Query_Measure;
        SET @redemption_table=' CDM_Customer_TP_Var ';
        SET @table_used=@redemption_table;
	END IF;
     IF IN_MEASURE = "TIER" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' "NA" ';
		SET @tier_measure=@Query_Measure;
        SET @tier_table=' CDM_Customer_TP_Var ';
        SET @table_used=@tier_table;
	END IF;

    
     IF IN_MEASURE='CARDS'
    THEN
		set @view_stmt=concat('create or replace view V_Dashboard_explore_Card as
		(select "Recency" as Card_Name,',@recency_measure,' as `Value1`,"" as `Value2`, "" as `Value3`,"RECENCY" as `Measure`  from ',@recency_table,' WHERE Customer_Id = ',@cust_id,')
		Union
		(select "Freq. (AP)" as Card_Name,',@freq12mth_measure,' as `Value1`,"" as `Value2`, "" as `Value3`,"FREQ_12MTH" as `Measure`  from ',@freq12mth_table,' WHERE Customer_Id = ',@cust_id,')
		Union
		(select "Freq. (Ever)" as Card_Name,',@freqever_measure,' as `Value1`,"" as `Value2`, "" as `Value3`,"FREQ_EVER" as `Measure`  from ',@freqever_table,' WHERE Customer_Id = ',@cust_id,')
		Union
		(select "ADGBT" as Card_Name,',@adgbt_measure,' as `Value1`,"" as `Value2`, "" as `Value3`,"ADGBT" as `Measure`  from ',@adgbt_table,' WHERE Customer_Id = ',@cust_id,')
		Union
		(select "Tenure" as Card_Name,',@tenure_measure,' as `Value1`,"" as `Value2`, "" as `Value3`,"TENURE" as `Measure`  from ',@tenure_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "Value (AP)" as Card_Name,format(round(',@value12mth_measure,'),",") as `Value1`,"" as `Value2`, "" as `Value3`,"VALUE_12MTH" as `Measure`  from ',@value12mth_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "Value (ever)" as Card_Name,format(round(',@valueever_measure,'),",") as `Value1`,"" as `Value2`, "" as `Value3`,"VALUE_EVER" as `Measure`  from ',@valueever_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "ABV" as Card_Name,format(round(',@abv_measure,'),",") as `Value1`,"" as `Value2`, "" as `Value3`,"ABV" as `Measure`  from ',@abv_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "Discount" as Card_Name,concat(round(',@discount_measure,',0),"%") as `Value1`,"" as `Value2`, "" as `Value3`,"DISCOUNT" as `Measure`  from ',@discount_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "Full Price" as Card_Name,concat(round(',@fullprice_measure,'),"%") as `Value1`,"" as `Value2`, "" as `Value3`,"FULL_PRICE" as `Measure`  from ',@fullprice_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "Product Range" as Card_Name,',@productrange_measure,' as `Value1`,"" as `Value2`, "" as `Value3`,"PRODUCT_RANGE" as `Measure`  from ',@productrange_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "Cat Range" as Card_Name,',@catrange_measure,' as `Value1`,"" as `Value2`, "" as `Value3`,"CAT_RANGE" as `Measure`  from ',@catrange_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "Fav Prod" as Card_Name,',@favprod_measure,' as `Value1`,"" as `Value2`, "" as `Value3`,"FAV_PROD" as `Measure`  from ',@favprod_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "Fav CAT" as Card_Name,',@favcat_measure,' as `Value1`,"" as `Value2`, "" as `Value3`,"FAV_CAT" as `Measure`  from ',@favcat_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "Stores" as Card_Name,1 as `Value1`,"" as `Value2`, "" as `Value3`,"NUM_STORES" as `Measure`  from ',@numstores_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "Fav Store" as Card_Name,',@favstore_measure,' as `Value1`,"" as `Value2`, "" as `Value3`,"FAV_STORE" as `Measure`  from ',@favstore_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "TOD" as Card_Name,',@tod_measure,' as `Value1`,"" as `Value2`, "" as `Value3`,"TOD" as `Measure`  from ',@tod_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "DOW" as Card_Name,',@dow_measure,' as `Value1`,"" as `Value2`, "" as `Value3`,"DOW" as `Measure`  from ',@dow_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "Reach" as Card_Name,',@reach_measure,' as `Value1`,"" as `Value2`, "" as `Value3`,"REACH" as `Measure`  from ',@reach_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "Response" as Card_Name,',@response_measure,' as `Value1`,"" as `Value2`, "" as `Value3`,"RESPONSE" as `Measure`  from ',@response_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "Data Completeness" as Card_Name,',@datacomplete_measure,' as `Value1`,"" as `Value2`, "" as `Value3`,"DATA_COMPLETE" as `Measure`  from ',@datacomplete_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "Points" as Card_Name,',@points_measure,' as `Value1`,"" as `Value2`, "" as `Value3`,"POINTS" as `Measure`  from ',@points_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "Redemption" as Card_Name,',@redemption_measure,' as `Value1`,"" as `Value2`, "" as `Value3`,"REDEMPTION" as `Measure`  from ',@redemption_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "Tier" as Card_Name,',@tier_measure,' as `Value1`,"" as `Value2`, "" as `Value3`,"TIER" as `Measure`  from ',@tier_table,' WHERE Customer_Id = ',@cust_id,')
		');
        
	SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Card_Name",`Card_Name`,"Value1",`Value1`,"Value2",`Value2`,"Value3",`Value3`,"Measure",`Measure`) )),"]")into @temp_result from V_Dashboard_explore_Card;' ;
    
    select @view_stmt;
    select @Query_String;
    
    ELSEIF IN_MEASURE = 'SEGMENT_NAME'
    THEN
		
		SET @view_stmt=concat('CREATE OR REPLACE VIEW V_Dashboard_explore_segment AS
						SELECT DISTINCT CLMSegmentName from CDM_Customer_Segment');
		SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Segment",`CLMSegmentName`) )),"]")into @temp_result from V_Dashboard_explore_segment;' ;
        select @Query_String;
        
	ELSEIF IN_MEASURE = 'BOTTOMINFO'
    THEN
		select Event_Execution_Id into @eeid from Event_Execution_History where Customer_Id = @cust_id order by Event_Execution_Date_ID DESC LIMIT 1;
        select @eeid;
        IF @eeid <> '' and @eeid is not null
        then
			
            
            select concat("Campaign: ",`Name`) into @campaign from Event_Master where ID = (select Event_Id from Event_Execution_History where Event_Execution_ID = @eeid);
            select concat("Channel: ",Communication_Channel) into @channel from Event_Execution_History where Event_Execution_ID = @eeid;
            select date_format(Event_Execution_Date_ID,"%d/%m/%Y") into @eedate from Event_Execution_History where Event_Execution_ID = @eeid;
            select "Delivery Status: Delivered" into @delivery_stat;
            select concat("Response: ",case when @eeid in (select distinct Event_Execution_ID from Event_Attribution_History) THEN "Responded" else "No Response" END) into @response;
		ELSE
			SET @campaign="No Campaigns Done Yet";
            SET @channel="";
            SET @delivery_stat="";
            SET @response="";
            SET @eedate="";
        END IF;
            
            
		SET @view_stmt=concat('CREATE OR REPLACE VIEW V_Dashboard_explore_bottom_info AS
						SELECT "Recent Purchase" as Heading,
                        "',@cust_id,'" as Customer,
                        date_format(LTD,"%d %b %Y") as value1,
                        (select LOV_Comm_Value from CDM_LOV_Master where LOV_Id =(select CP_Fav_Prod_LOV_Id from CDM_Customer_CP_Var where Customer_Id = ',@cust_id,')) as value2,
                        (select SUM(Bill_Total_Val) from CDM_Bill_Header b where Customer_Id = ',@cust_id,' and b.Bill_Date=a.LTD) as value3,
						(select LOV_Value from CDM_LOV_Master c where c.LOV_Id=a.Ltd_Store_LOV_Id) as value4,
                        concat(round((select ifnull(CP_Disc_Percent,0) from CDM_Customer_CP_Var where Customer_Id = ',@cust_id,'),0),"%") as value5
                        from CDM_Customer_TP_Var a
                        where Customer_Id = ',@cust_Id,'
                        UNION
                        select "Recent Campaign" as Heading,
                        "',@cust_id,'" as Customer,
                        "',@campaign,'" as value1,
                        "',@eedate,'" as value2,
                        "',@channel,'" as value3,
                        "',@delivery_stat,'" as value4,
                        "',@response,'" as value5;
                        ');
		SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Heading",`Heading`,"Customer",`Customer`,"value1",`value1`,"value2",`value2`,"value3",`value3`,"value4",`value4`,"value5",`value5`) )),"]")into @temp_result from V_Dashboard_explore_bottom_info;' ;
                        
    ELSEIF IN_MEASURE in ("RECENCY","FREQ_12MTH","FREQ_EVER","ADGBT","TENURE","VALUE_12MTH","VALUE_EVER","ABV","DISCOUNT","FULL_PRICE","PRODUCT_RANGE","CAT_RANGE","NUM_STORES","POINTS","REDEMPTION") and IN_AGGREGATION_1 = 'GRAPH'
    THEN
		select Seg_For_model_3 into @segment_id from svoc_segments where Customer_Id = @cust_id;
		select @table_used,@Query_Measure,@segment_id;
		SET @view_stmt=concat('CREATE OR REPLACE VIEW V_Dashboard_explore_base_Graph AS
							SELECT "Customer" as "X_Data",round(',@Query_Measure,') as Value from ',@table_used,' where Customer_Id = ',@cust_id,'
                            UNION
							((select "Segment",round(avg(',@Query_Measure,')) from ',@table_used,' where Customer_Id in (select Customer_Id from svoc_segments where Seg_For_model_3 = ',@segment_id,')))
                            UNION
                         (select "Base",round(avg(',@Query_Measure,')) from ',@table_used,')
                        ');
		SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`X_Data`,"Value",`Value`) )),"]")into @temp_result from V_Dashboard_explore_base_Graph;' ;
        select @view_stmt,@Query_String;
	ELSEIF IN_MEASURE in ("RECENCY","FREQ_12MTH","FREQ_EVER","ADGBT","TENURE","VALUE_12MTH","VALUE_EVER","ABV","DISCOUNT","FULL_PRICE","PRODUCT_RANGE","CAT_RANGE","NUM_STORES","POINTS","REDEMPTION") and IN_AGGREGATION_1 = 'PERCENTILE'
    THEN
		IF IN_MEASURE = "RECENCY"
        THEN
			set @percentile_ordering="DESC";
		ELSE
			set @percentile_ordering="ASC";
        END IF;
		SET @view_stmt=concat('CREATE OR REPLACE VIEW V_Dashboard_explore_base_percentile AS
							select Customer_Id,PERCENT_RANK() OVER(order by `Value` ',@percentile_ordering,') as pct from 
                            (SELECT Customer_Id,round(',@Query_Measure,') as `Value` from ',@table_used,')A');
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Percentile",concat(round(`pct`*100,1),"%")) )),"]")into @temp_result from V_Dashboard_explore_base_percentile where Customer_Id = ',@cust_id,';') ;
        
	ELSEIF IN_MEASURE in ("FAV_PROD","FAV_CAT","FAV_STORE","TOD","DOW") and IN_AGGREGATION_1 = 'GRAPH'
	THEN
		select Seg_For_model_3 into @segment_id from svoc_segments where Customer_Id = @cust_id;
		SET @view_stmt=concat('CREATE OR REPLACE VIEW V_Dashboard_explore_base_Graph AS
							(SELECT "Segment" as "X_Data",round((count(1)/(select count(1) from svoc_segments where Seg_For_model_3 =',@segment_id,'))*100,2) as Value							
                        from ',@table_per,' where ',@Mesaure_per,'=(select ',@Mesaure_per,' from ',@table_per,' where Customer_Id=',@cust_id,') and Customer_Id in (select Customer_Id from svoc_segments where Seg_For_model_3 =',@segment_id,'))
                        UNION
                        (SELECT "Base",round((count(1)/(select count(1) from ',@table_per,'))*100,2) as base							
                        from ',@table_per,'  where ',@Mesaure_per,'=(select ',@Mesaure_per,' from ',@table_per,' where Customer_Id=',@cust_id,')) 
                        ');
		SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`X_Data`,"Value",`Value`) )),"]")into @temp_result from V_Dashboard_explore_base_Graph;' ;
	
    ELSEIF IN_MEASURE in ("FAV_PROD","FAV_CAT","FAV_STORE","TOD","DOW") and IN_AGGREGATION_1 = 'PERCENTILE'
	THEN
		
		SET @view_stmt=concat('CREATE OR REPLACE VIEW V_Dashboard_explore_base_percentile AS
								',@pct_bills_query,'');
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Percentile",concat(round(`pct`*100,1),"%")) )),"]")into @temp_result from V_Dashboard_explore_base_percentile;') ;

		END IF;
	IF IN_MEASURE <> "DOWNLOAD"
    THEN
	select @view_stmt;
	PREPARE statement from @view_stmt;
	Execute statement;
	Deallocate PREPARE statement;

	select @Query_String;
	PREPARE statement from @Query_String;
	Execute statement;
	Deallocate PREPARE statement; 
    SET out_result_json = @temp_result;
    END IF;
    
END$$
DELIMITER ;
