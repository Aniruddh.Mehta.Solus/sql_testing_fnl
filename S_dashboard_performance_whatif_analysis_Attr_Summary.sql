DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_performance_whatif_analysis_Attr_Summary`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text;
	declare IN_FILTER_CHANNEL text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
    declare IN_AGGREGATION_4 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
    SET IN_FILTER_CHANNEL = json_unquote(json_extract(in_request_json,"$.FILTER_CHANNEL"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
    SET IN_AGGREGATION_4 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_4"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    
    SET @Interval_month = CASE WHEN IN_AGGREGATION_2='M' THEN 0
							WHEN IN_AGGREGATION_2='L3M' THEN 2
                            WHEN IN_AGGREGATION_2='L12M' THEN 11 END;
                            
    
    
	SET @FY_SATRT_DATE = date_format(date_sub(concat(IN_FILTER_CURRENTMONTH,'01'), INTERVAL @Interval_month MONTH),"%Y%m");
	SET @FY_END_DATE = IN_FILTER_CURRENTMONTH;
    
    SELECT @FY_SATRT_DATE,@FY_END_DATE;
    SET @filter_cond=concat(" YM between '",@FY_SATRT_DATE,"' and '",@FY_END_DATE,"'");
	
    CREATE OR REPLACE TABLE Campaign_Insights_Attr_Definations
	(
	`Attribute` varchar(250),
	`Defination` varchar(1000)
	);
    INSERT IGNORE INTO Campaign_Insights_Attr_Definations (Attribute,Defination)
    select CLMSegmentName,CLMSegmentReplacedQuery from CLM_Segment;
    INSERT IGNORE INTO Campaign_Insights_Attr_Definations (Attribute,Defination)
	VALUES
	("CLM","Type of Campaign"),
	("GTM","Type of Campaign- GO TO MARKET"),
	("No Offer","Campaigns without any Offer"),
	("Offer","Campaigns with Offer like '% OFF', 'BOGO' etc."),
	("XL","Size greater than or equal to 250K"),
	("L","Size between 100K and 250K"),
	("M","Size between 50K and 100K"),
	("S","Size between 10K and 50K"),
	("XS","Size less than or equal to 10K"),
	("Reco","Campaigns with SOLUS.AI custom recommendations"),
	("No Reco","Campaigns without SOLUS.AI custom recommendations"),
	("Friday", "Transaction Day of the Week"),
	("Monday", "Transaction Day of the Week"),
	("Saturday", "Transaction Day of the Week"),
	("Sunday", "Transaction Day of the Week"),
	("Thursday","Transaction Day of the Week"),
	("Tuesday", "Transaction Day of the Week"),
	("Wednesday", "Transaction Day of the Week")
    /*
    ,
	("LEADS","Customer Segment- Customer contactable in the database but no transaction to date"),
	("FTR","Customer Segment- Recent Single Transactors"),
	("AHF","Customer Segment- Active High Frequency Customers"),
	("ALF","Customer Segment- Active Low Frequency Customers"),
	("OTB","Customer Segment- At risk customers on the verge of inactive period"),
	("Inactive","Customer Segment- Inactive customers ie. Customer with recency more than 180"),
	("Lapsed","Customer Segment- Deep Lapsed Caustomers with recency more than 365")*/
	;
    
    IF IN_FILTER_CHANNEL = "ALL"
    THEN 
		SET IN_FILTER_CHANNEL = "";
	END IF;
    
    IF IN_FILTER_CHANNEL IS  NULL OR IN_FILTER_CHANNEL = ""
    THEN
		SET @filter_cond = @filter_cond;
    ELSE
		SET @filter_cond=concat(@filter_cond," AND chan = '",IN_FILTER_CHANNEL,"'");
    END IF;
    SELECT @filter_cond;
    
    SET @incr_Rev =' round(SUM(Incremental_Revenue),0) ';
    
    
      select Value1 into @Incremental_Revenue_Method from UI_Configuration_Global where Config_Name = 'Incremental_Revenue_Method';
    SET @Incremental_Revenue_Method=ifnull(@Incremental_Revenue_Method,'Method2');
    select @Incremental_Revenue_Method;
    IF @Incremental_Revenue_Method = 'Method1'
    THEN
		SET @incr_rev_concat='_By_Day';
        SET @group_cond1='Event_Id,EE_Date';
        SET @mtd_concat='';
	ELSEIF @Incremental_Revenue_Method = 'Method2'
    THEN
		SET @group_cond1='Event_Id,YM';
		SET @incr_rev_concat='';
        SET @mtd_concat='MTD_';
	ELSE
		SET @incr_rev_concat='';
        SET @mtd_concat='MTD_';
		SET @incr_Rev = ' round((CASE WHEN SUM(Control_Base) < 0 or SUM(Target_Delivered) = 0 then 0 WHEN (((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*100 < 0 then 0 WHEN SUM(Control_Base)>0 and SUM(Control_Responder)=0 then SUM(Target_Revenue)
					ELSE ifnull((((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*(SUM(Target_Revenue)/SUM(Target_Responder))*(SUM(Target_Delivered)),0) END),0) ';
		SET @group_cond1='YM';
	END IF;
    SELECT @mtd_concat;
    
    SET @sql1=CONCAT('
		CREATE OR REPLACE VIEW Dashboard_Performance_Campaign_Insights_STLT
        as 
		SELECT 
			`Trigger`,
			`EE_Date`,
			`Chan`,
			`Type`,
			`Size`,
			`Recommendation`,
			`Pers`,
			`Offer`,
			`DOW_num`,
			`DOW`,
			`WEEKDAY_WEEKEND`,
			`Segment`,
			`Event_ID`,
			`Event_Execution_Date_ID`,
			`YM`,
			`Target_Base`,
			`Target_Delivered`,
			`Target_Bills`,
			`Conversion_Per`,
			`Incremental_Revenue',@incr_rev_concat,'` AS `Incremental_Revenue`,
			`',@mtd_concat,'Incremental_Revenue',@incr_rev_concat,'` AS `MTD_Incremental_Revenue`
		FROM `Dashboard_Performance_Campaign_Insights`;
        ');
        select @sql1;
		PREPARE statement from @sql1;
		Execute statement;
		Deallocate PREPARE statement;
    
	IF IN_AGGREGATION_1 = 'YIELD'  
    THEN
    
		SET @create_view=CONCAT('CREATE OR REPLACE VIEW V_Dashboard_What_If_Incr_Rev
			as
			select `Trigger`,
			`Type`,
			`Size`,
			`Recommendation`,
			`Pers`,
			`Offer`,
			`DOW_num`,
			`DOW`,
			`WEEKDAY_WEEKEND`,
			`Segment`,
			`Conversion_Per`,
            MAX(EE_Date) as EE_Date,MAX(MTD_Incremental_Revenue) as Incremental_Revenue,SUM(Target_Delivered) as Target_Delivered,SUM(Target_Bills) as Target_Bills,SUM(Target_Base) as Target_Base,
            Event_ID,Chan,YM from Dashboard_Performance_Campaign_Insights_STLT
            group by ',@group_cond1,'');
    
     select @create_view;
	PREPARE statement from @create_view;
	Execute statement;
	Deallocate PREPARE statement;
        
        SET @view_stmt_temp=CONCAT("CREATE OR REPLACE VIEW V_Dashboard_Campaign_Insights_Attributes_Summary
			as(
            select `Type` AS Columns,
            round((",@incr_Rev,"/SUM(Target_Delivered)),1) AS Yield 
            from V_Dashboard_What_If_Incr_Rev
			where ",@filter_cond,"
            group by 1
            )
            union
			(	
            select `Offer` AS Columns,
            round((",@incr_Rev,"/SUM(Target_Delivered)),1) AS Yield 
            from V_Dashboard_What_If_Incr_Rev
			where ",@filter_cond,"
            group by 1
							)
			union
			(	
            select `Size` AS Columns,
            round((",@incr_Rev,"/SUM(Target_Delivered)),1) AS Yield 
            from V_Dashboard_What_If_Incr_Rev
			where ",@filter_cond,"
            group by 1
							)
			union
			(	
            select `Recommendation` AS Columns,
            round((",@incr_Rev,"/SUM(Target_Delivered)),1) AS Yield 
            from V_Dashboard_What_If_Incr_Rev
			where ",@filter_cond,"
            group by 1
            )
            union
			(	
            select `DOW` AS Columns,
            round((",@incr_Rev,"/SUM(Target_Delivered)),1) AS Yield 
            from V_Dashboard_What_If_Incr_Rev
			where ",@filter_cond,"
            group by 1
            )
            union
			(	
            select `Segment` AS Columns,
            round((",@incr_Rev,"/SUM(Target_Delivered)),1) AS Yield 
            from V_Dashboard_What_If_Incr_Rev
			where ",@filter_cond,"
            group by 1
            );");
		
        IF IN_MEASURE = 'GRAPH'
        THEN
        SET @view_stmt=CONCAT("CREATE OR REPLACE VIEW V_Dashboard_Campaign_Insights_Attributes_Summary_Final
			as 
            SELECT * 
            FROM 
            V_Dashboard_Campaign_Insights_Attributes_Summary
            ORDER BY Yield desc
            limit 15;");
            
		SET @Query_String =  CONCAT('SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`Columns`,"Value",`Yield`) )),"]")into @temp_result from V_Dashboard_Campaign_Insights_Attributes_Summary_Final',';') ;
		END IF;
        
        IF IN_MEASURE = 'TABLE'
        THEN 
        SET @view_stmt=CONCAT("CREATE OR REPLACE VIEW V_Dashboard_Campaign_Insights_Attributes_Summary_Final
			as 
            SELECT * 
            FROM 
            V_Dashboard_Campaign_Insights_Attributes_Summary V_Dash, Campaign_Insights_Attr_Definations def
            WHERE V_Dash.Columns = def.Attribute
            ORDER BY Yield desc
            limit 15;");
            
		SET @Query_String =  CONCAT('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Columns",`Columns`,"Description",`Defination`,"Value",`Yield`) )),"]")into @temp_result from V_Dashboard_Campaign_Insights_Attributes_Summary_Final',';') ;
		
        END IF;
	END IF;
        
        
    IF  IN_AGGREGATION_1 = 'CONV'  
    THEN
        
        SET @view_stmt_temp=CONCAT("CREATE OR REPLACE VIEW V_Dashboard_Campaign_Insights_Attributes_Summary
			as
			(
			select `Type` AS Columns,
            SUM(Target_Bills)/SUM(Target_Base) AS `% Conv`
            from Dashboard_Performance_Campaign_Insights
			where ",@filter_cond,"
            group by 1
            ORDER BY `% Conv` desc
            )
            union
            (
			select `Offer` AS Columns,
            SUM(Target_Bills)/SUM(Target_Base) AS `% Conv`
            from Dashboard_Performance_Campaign_Insights
			where ",@filter_cond,"
            group by 1
            ORDER BY `% Conv` desc
            )
            union
            (
			select `Size` AS Columns,
            SUM(Target_Bills)/SUM(Target_Base) AS `% Conv`
            from Dashboard_Performance_Campaign_Insights
			where ",@filter_cond,"
            group by 1
            ORDER BY `% Conv` desc
            )
            union
            (
			select `Recommendation` AS Columns,
            SUM(Target_Bills)/SUM(Target_Base) AS `% Conv`
            from Dashboard_Performance_Campaign_Insights
			where ",@filter_cond,"
            group by 1
            ORDER BY `% Conv` desc
            )
            union
            (
			select `DOW` AS Columns,
            SUM(Target_Bills)/SUM(Target_Base) AS `% Conv`
            from Dashboard_Performance_Campaign_Insights
			where ",@filter_cond,"
            group by DOW_num
            ORDER BY `% Conv` desc
            )
            union
            (
			select `Segment` AS Columns,
            SUM(Target_Bills)/SUM(Target_Base) AS `% Conv`
            from Dashboard_Performance_Campaign_Insights
			where ",@filter_cond,"
            group by 1
            ORDER BY `% Conv` desc
            )
            
                            ;");
		IF IN_MEASURE = 'GRAPH'
        THEN
		SET @view_stmt=CONCAT("CREATE OR REPLACE VIEW V_Dashboard_Campaign_Insights_Attributes_Summary_Final
			as 
            SELECT * 
            FROM 
            V_Dashboard_Campaign_Insights_Attributes_Summary
            ORDER BY `% Conv` desc
            limit 15;");
        
        SET @Query_String =  CONCAT('SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`Columns`,"Value",round(ifnull(`% Conv` * 100,0),2)) )),"]")into @temp_result from V_Dashboard_Campaign_Insights_Attributes_Summary_Final order by `% Conv` desc',';') ;
		END IF;
        IF IN_MEASURE = 'TABLE'
        THEN
		SET @view_stmt=CONCAT("CREATE OR REPLACE VIEW V_Dashboard_Campaign_Insights_Attributes_Summary_Final
			as 
            SELECT * 
            FROM 
            V_Dashboard_Campaign_Insights_Attributes_Summary V_Dash, Campaign_Insights_Attr_Definations def
            WHERE V_Dash.Columns = def.Attribute
            ORDER BY `% Conv` desc
            limit 15;");
        
        SET @Query_String =  CONCAT('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Columns",`Columns`,"Description",`Defination`,"Value",round(ifnull(`% Conv` * 100,0),2)) )),"]")into @temp_result from V_Dashboard_Campaign_Insights_Attributes_Summary_Final order by `% Conv` desc',';') ;
		END IF;
	END IF;
        
	IF IN_AGGREGATION_1 = 'LIFT'  
    THEN
    
		SET @create_view=CONCAT('CREATE OR REPLACE VIEW V_Dashboard_What_If_Incr_Rev
			as
			select `Trigger`,
			`Type`,
			`Size`,
			`Recommendation`,
			`Pers`,
			`Offer`,
			`DOW_num`,
			`DOW`,
			`WEEKDAY_WEEKEND`,
			`Segment`,
			`Conversion_Per`,
            MAX(EE_Date) as EE_Date,MAX(MTD_Incremental_Revenue) as Incremental_Revenue,SUM(Target_Delivered) as Target_Delivered,SUM(Target_Bills) as Target_Bills,SUM(Target_Base) as Target_Base,
            Event_ID,Chan,YM from Dashboard_Performance_Campaign_Insights_STLT
            group by ',@group_cond1,'');
    
     select @create_view;
	PREPARE statement from @create_view;
	Execute statement;
	Deallocate PREPARE statement;
        
        SET @view_stmt_temp=CONCAT("CREATE OR REPLACE VIEW V_Dashboard_Campaign_Insights_Attributes_Summary
			as
			(
			select `Type` AS Columns,
            ifnull(",@incr_Rev,",0) AS `Rev_Lift`
            from V_Dashboard_What_If_Incr_Rev
			where ",@filter_cond,"
            group by 1
            ORDER BY `Rev_Lift` desc
            )
            union
            (
			select `Size` AS Columns,
            ifnull(",@incr_Rev,",0) AS `Rev_Lift`
            from V_Dashboard_What_If_Incr_Rev
			where ",@filter_cond,"
            group by 1
            ORDER BY `Rev_Lift` desc
            )
            union
            (
			select `Recommendation` AS Columns,
            ifnull(",@incr_Rev,",0) AS `Rev_Lift`
            from V_Dashboard_What_If_Incr_Rev
			where ",@filter_cond,"
            group by 1
            ORDER BY `Rev_Lift` desc
            )
            union
            (
			select `Offer` AS Columns,
            ifnull(",@incr_Rev,",0) AS `Rev_Lift`
            from V_Dashboard_What_If_Incr_Rev
			where ",@filter_cond,"
            group by 1
            ORDER BY `Rev_Lift` desc
            )
            union
            (
			select `DOW` AS Columns,
            ifnull(",@incr_Rev,",0) AS `Rev_Lift`
            from V_Dashboard_What_If_Incr_Rev
			where ",@filter_cond,"
            group by 1
            ORDER BY `Rev_Lift` desc
            )
            union
            (
			select `Segment` AS Columns,
            ifnull(",@incr_Rev,",0) AS `Rev_Lift`
            from V_Dashboard_What_If_Incr_Rev
			where ",@filter_cond,"
            group by 1
            ORDER BY `Rev_Lift` desc
            )
            ;");
            
		IF IN_MEASURE = 'GRAPH'
        THEN
        SET @view_stmt=CONCAT("CREATE OR REPLACE VIEW V_Dashboard_Campaign_Insights_Attributes_Summary_Final
			as 
            SELECT * 
            FROM 
            V_Dashboard_Campaign_Insights_Attributes_Summary
            ORDER BY `Rev_Lift` desc
            limit 15;");
        SET @Query_String =  CONCAT('SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`Columns`,"Value",`Rev_Lift`) )),"]")into @temp_result from V_Dashboard_Campaign_Insights_Attributes_Summary_Final',';') ;
		END IF;
        IF IN_MEASURE = 'TABLE'
        THEN 
        SET @view_stmt=CONCAT("CREATE OR REPLACE VIEW V_Dashboard_Campaign_Insights_Attributes_Summary_Final
			as 
            SELECT * 
            FROM 
            V_Dashboard_Campaign_Insights_Attributes_Summary V_Dash, Campaign_Insights_Attr_Definations def
            WHERE V_Dash.Columns = def.Attribute
            ORDER BY `Rev_Lift` desc
            limit 15;");
        SET @Query_String =  CONCAT('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Columns",`Columns`,"Description",`Defination`,"Value",format(`Rev_Lift`,",")) )),"]")into @temp_result from V_Dashboard_Campaign_Insights_Attributes_Summary_Final',';') ;
        END IF;
	END IF;
        
	
	IF IN_MEASURE <> "DOWNLOAD"
    THEN
    
    select @view_stmt_temp;
	PREPARE statement from @view_stmt_temp;
	Execute statement;
	Deallocate PREPARE statement;
    
    select @view_stmt;
	PREPARE statement from @view_stmt;
	Execute statement;
	Deallocate PREPARE statement;
    
	select @Query_String;
	PREPARE statement from @Query_String;
	Execute statement;
	Deallocate PREPARE statement; 
    
    
    
    SET out_result_json = @temp_result;
    END IF;
    
END$$
DELIMITER ;
