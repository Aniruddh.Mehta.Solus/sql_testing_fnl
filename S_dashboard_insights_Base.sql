DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_insights_Base`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    
    
    
    SET @FY_SATRT_DATE = IN_FILTER_CURRENTMONTH-100;
	SET @FY_END_DATE = IN_FILTER_CURRENTMONTH;
    
    
	
    
    IF IN_AGGREGATION_1 = 'BY_DAY' OR IN_FILTER_MONTHDURATION = 1
		THEN 
			SET @filter_cond = concat(' YYYYMM =',IN_FILTER_CURRENTMONTH);
			SET @date_select = ' date_format(YYYYMMDD,"%d-%b") AS "Year" ';
	ELSEIF IN_AGGREGATION_1 = 'BY_MONTH' or IN_FILTER_MONTHDURATION = 12
		THEN 
			SET @filter_cond=concat(" YYYYMM between '",@FY_SATRT_DATE,"' and '",@FY_END_DATE,"'");
			SET @date_select=' date_format(concat(YYYYMM,"01"),"%b-%Y") AS "Year" ';
	END IF;
    
    
    IF IN_MEASURE = "BASE" OR IN_MEASURE = "CARDS"
	THEN
		SET @Query_Measure=' SUM(Base) ';
		SET @base_measure=@Query_Measure;
	END IF;
	IF IN_MEASURE = "ACTIVE_BASE" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' SUM(ActiveBase) ';
		SET @activebase_measure=@Query_Measure;
	END IF;
    IF IN_MEASURE = "MONTHLY_ACTIVE" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' SUM(MonthlyActive) ';
		SET @monthlyactive_measure=@Query_Measure;
	END IF;
     IF IN_MEASURE = "QTR_ACTIVE" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' SUM(QuarterlyActive) ';
		SET @qtractive_measure=@Query_Measure;
	END IF;
    IF IN_MEASURE = "TM_ACTIVE" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' SUM(`T12MthActive`) ';
		SET @tmactive_measure=@Query_Measure;
	END IF;
    IF IN_MEASURE = "INACTIVE" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' SUM(InactiveBase) ';
		SET @inactive_measure=@Query_Measure;
	END IF;
    IF IN_MEASURE = "LAPSED" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' SUM(LapsedBase) ';
		SET @lapsed_measure=@Query_Measure;
	END IF;
    IF IN_MEASURE = "FY_BASE" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' SUM(FYBase) ';
		SET @fybase_measure=@Query_Measure;
	END IF;

    
     IF IN_MEASURE='CARDS'
    THEN
		set @view_stmt=concat('create or replace view V_Dashboard_Base_Card as
		(select "Base" as Card_Name,format(',@base_measure,',",") as `Value1`,"" as `Value2`, "" as `Value3`,"BASE" as `Measure`  from Dashboard_Insights_Base WHERE YYYYMM = ',IN_FILTER_CURRENTMONTH,' and AGGREGATION = "BY_MONTH")
		Union
		(select "Active Base" as Card_Name,format(',@activebase_measure,',",") as `Value1`,"" as `Value2`, "" as `Value3`,"ACTIVE_BASE" as `Measure`  from Dashboard_Insights_Base WHERE YYYYMM = ',IN_FILTER_CURRENTMONTH,' and AGGREGATION = "BY_MONTH")
		Union
		(select "Monthly Active Base" as Card_Name,format(',@monthlyactive_measure,',",") as `Value1`,"" as `Value2`, "" as `Value3`,"MONTHLY_ACTIVE" as `Measure`  from Dashboard_Insights_Base WHERE YYYYMM = ',IN_FILTER_CURRENTMONTH,' and AGGREGATION = "BY_MONTH")
		Union
		(select "QTR Active Base" as Card_Name,format(',@qtractive_measure,',",") as `Value1`,"" as `Value2`, "" as `Value3`,"QTR_ACTIVE" as `Measure`  from Dashboard_Insights_Base WHERE YYYYMM = ',IN_FILTER_CURRENTMONTH,' and AGGREGATION = "BY_MONTH")
		Union
		(select "12 Months Active Base" as Card_Name,format(',@tmactive_measure,',",") as `Value1`,"" as `Value2`, "" as `Value3`,"TM_ACTIVE" as `Measure`  from Dashboard_Insights_Base WHERE YYYYMM = ',IN_FILTER_CURRENTMONTH,' and AGGREGATION = "BY_MONTH")
        Union
		(select "Inactive Base" as Card_Name,format(',@inactive_measure,',",") as `Value1`,"" as `Value2`, "" as `Value3`,"INACTIVE" as `Measure`  from Dashboard_Insights_Base WHERE YYYYMM = ',IN_FILTER_CURRENTMONTH,' and AGGREGATION = "BY_MONTH")
        Union
		(select "Lapsed Base" as Card_Name,format(',@lapsed_measure,',",") as `Value1`,"" as `Value2`, "" as `Value3`,"LAPSED" as `Measure`  from Dashboard_Insights_Base WHERE YYYYMM = ',IN_FILTER_CURRENTMONTH,' and AGGREGATION = "BY_MONTH")
        Union
		(select "FY Base" as Card_Name,format(',@fybase_measure,',",") as `Value1`,"" as `Value2`, "" as `Value3`,"FY_BASE" as `Measure`  from Dashboard_Insights_Base WHERE YYYYMM = ',IN_FILTER_CURRENTMONTH,' and AGGREGATION = "BY_MONTH")
		');
        
	SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Card_Name",`Card_Name`,"Value1",`Value1`,"Value2",`Value2`,"Value3",`Value3`,"Measure",`Measure`) )),"]")into @temp_result from V_Dashboard_Base_Card;' ;
    
    ELSEIF IN_AGGREGATION_2="DRILL"
    THEN
		IF IN_AGGREGATION_1 = 'BY_DAY' 
		THEN 
			SET @filter_cond = concat(' YYYYMM =',IN_FILTER_CURRENTMONTH);
            SET @filter_cond2= ' AGGREGATION = "BY_DAY" ';
			SET @date_select = ' date_format(YYYYMMDD,"%d-%b-%Y") AS "Year" ';
            SET @ordering = ' YYYYMMDD ';
		ELSEIF IN_AGGREGATION_1 = 'BY_MONTH' 
		THEN 
			SET @filter_cond=concat(" YYYYMM between '",@FY_SATRT_DATE,"' and '",@FY_END_DATE,"'");
			SET @date_select=' date_format(concat(YYYYMM,"01"),"%d-%b-%Y") AS "Year" ';
            SET @filter_cond2= ' AGGREGATION = "BY_MONTH" ';
            SET @ordering = ' YYYYMM ';
		ELSEIF IN_AGGREGATION_1 = 'BY_YEAR'
		THEN
			SET @filter_cond = ' 1=1 ';
            SET @date_select = ' date_format(concat(YYYY,"0101"),"%d-%b-%Y") as "Year"';
            SET @filter_cond2 = ' AGGREGATION = "BY_YEAR" ';
            SET @ordering = ' YYYYMM ';
		END IF;
		SET IN_MEASURE= concat(IN_MEASURE,IN_AGGREGATION_2);
        SET @top_query = '';
        select group_concat(DISTINCT `Revenue_Center`) into @distinct_rc from Dashboard_Insights_Base where YYYYMM=IN_FILTER_CURRENTMONTH and AGGREGATION = IN_AGGREGATION_1 and Revenue_Center <> '';
        select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
		set @loop_var=1;
		set @json_select='';
		select @loop_var,@num_of_rc,@distinct_rc;
		WHILE @loop_var<=@num_of_rc
		do
        SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
		set @top_query=concat(@top_query,",MAX(CASE WHEN `Revenue_Center` = '",@selected_rc,"' Then pct else 0 END) AS `",@selected_rc,"`");
			set @json_select=concat(@json_select,',"',@selected_rc,'",`',@selected_rc,'`');
			set @loop_var=@loop_var+1;
		end while;
        
		SET @view_stmt=concat('create or replace view V_Dashboard_base_drill as
		select `Year`',@top_query,' from 
		(select max(',@ordering,') as YM,',@date_select,',`Revenue_Center`, (',@Query_Measure,') pct from Dashboard_Insights_Base a
		where ',@filter_cond,'
        and ',@filter_cond2,'
		group by 2,`Revenue_Center`
        having pct > 0)C
		group by `Year`
        order by YM');
        
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`Year`',@json_select,') )),"]")into @temp_result from V_Dashboard_base_drill;') ;
	
    ELSE
		select @date_select,@Query_Measure,@filter_cond;
		SET @view_stmt=concat('CREATE OR REPLACE VIEW V_Dashboard_Base_Graph AS
						SELECT ',@date_select,',
                         ',@Query_Measure,' as "Value"
                         from Dashboard_Insights_Base
                         WHERE ',@filter_cond,'
                         AND AGGREGATION="',IN_AGGREGATION_1,'"
                         GROUP BY 1
                         ORDER BY YYYYMM
                        ');
		SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`Year`,"Value",`Value`) )),"]")into @temp_result from V_Dashboard_Base_Graph;' ;
		END IF;
	
	IF IN_SCREEN <> "DOWNLOAD"
    THEN
		select @view_stmt;
		PREPARE statement from @view_stmt;
		Execute statement;
		Deallocate PREPARE statement;
		select @Query_String;
		PREPARE statement from @Query_String;
		Execute statement;
		Deallocate PREPARE statement; 
		SET out_result_json = @temp_result;
	ELSE
		call S_dashboard_performance_download('V_Dashboard_Base_Graph',@out_result_json1);
		select @out_result_json1 into out_result_json;
    END IF;
END$$
DELIMITER ;
