DELIMITER $$
CREATE or replace PROCEDURE `S_dashboard_explore_variable`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
     
     
    IF IN_MEASURE = 'TABLE'
    THEN
		set @view_stmt = 'create or replace view V_Dashboard_explore_Variable_table as
							select Attribute, MAX(`Mean`) as `Mean` from Dashboard_Explore_Variable group by Attribute';
		SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Variable",`Attribute`,"Value",`Mean`) )),"]")into @temp_result from V_Dashboard_explore_Variable_table;' ;
        select @Query_String;
    END IF;
    IF IN_MEASURE = "Recency" OR IN_MEASURE = "CARDS"
	THEN
        set @dist_attr='Recency';
	END IF;
	IF IN_MEASURE = "Frequency(12mth)" OR IN_MEASURE = "CARDS" 
	THEN
        set @dist_attr='Lt_Num_of_Visits';
	END IF;
    IF IN_MEASURE = "Frequency(ever)" OR IN_MEASURE = "CARDS" 
	THEN
        set @dist_attr='Lt_Num_of_Visits';
	END IF;
     IF IN_MEASURE = "ADGBT" OR IN_MEASURE = "CARDS" 
	THEN
        set @dist_attr='Lt_ADGBT';
	END IF;
    IF IN_MEASURE = "Tenure" OR IN_MEASURE = "CARDS" 
	THEN
        set @dist_attr='Tenure';
	END IF;
    IF IN_MEASURE = "Value(12mth)" OR IN_MEASURE = "CARDS" 
	THEN
        SET @dist_attr='Lt_Rev';
	END IF;
    IF IN_MEASURE = "Value(ever)" OR IN_MEASURE = "CARDS" 
	THEN
        SET @dist_attr='Lt_Rev';
	END IF;
    IF IN_MEASURE = "ABV" OR IN_MEASURE = "CARDS" 
	THEN
        SET @dist_attr='Lt_ABV';
	END IF;
    IF IN_MEASURE = "Discount%" OR IN_MEASURE = "CARDS" 
	THEN
        SET @dist_attr='Lt_Disc_Percent';
	END IF;
	IF IN_MEASURE = "Product Range" OR IN_MEASURE = "CARDS" 
	THEN
        SET @dist_attr='LT_Distinct_Prod_Cnt';
	END IF;
	IF IN_MEASURE = "Cat Range" OR IN_MEASURE = "CARDS" 
	THEN
        SET @dist_attr='LT_Distinct_Cat_Cnt';
	END IF;
    IF IN_MEASURE = "FAV_PROD" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' ifnull(LOV_Comm_Value,"NA") ';
		SET @favprod_measure=@Query_Measure;
        SET @favprod_table=' CDM_Customer_TP_Var favp left join CDM_LOV_Master favp_lov on favp.LT_Fav_Prod_LOV_Id=favp_lov.LOV_Id  ';
        SET @table_used=@favprod_table;
        SET @table_per=' CDM_Customer_TP_Var ';
        SET @Mesaure_per=' LT_Fav_Prod_LOV_Id ';
	END IF;
    IF IN_MEASURE = "FAV_CAT" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' ifnull(LOV_Value,"NA") ';
		SET @favcat_measure=@Query_Measure;
        SET @favcat_table=' CDM_Customer_TP_Var favs left join CDM_LOV_Master favs_lov on favs.LT_Fav_Cat_LOV_Id=favs_lov.LOV_Id ';
        SET @table_used=@favcat_table;
        SET @table_per=' CDM_Customer_TP_Var ';
        SET @Mesaure_per=' LT_Fav_Cat_LOV_Id ';
	END IF;
    IF IN_MEASURE = "NUM_STORES" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' LT_Fav_Cat_LOV_Id ';
		SET @numstores_measure=@Query_Measure;
        SET @numstores_table=' CDM_Customer_TP_Var ';
        SET @table_used=@numstores_table;
	END IF;
    IF IN_MEASURE = "FAV_STORE" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' LOV_Value ';
		SET @favstore_measure=@Query_Measure;
        SET @favstore_table=' CDM_Customer_TP_Var favs left join CDM_LOV_Master favs_lov on favs.LT_Fav_Store_LOV_Id=favs_lov.LOV_Id  ';
        SET @table_used=@favstore_table;
        SET @table_per=' CDM_Customer_TP_Var ';
        SET @Mesaure_per=' LT_Fav_Store_LOV_Id ';
	END IF;
    IF IN_MEASURE = "TOD" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' Lt_Fav_Time_Of_Day ';
		SET @tod_measure=@Query_Measure;
        SET @tod_table=' CDM_Customer_TP_Var ';
        SET @table_used=@tod_table;
        SET @table_per=' CDM_Customer_TP_Var ';
        SET @Mesaure_per=' Lt_Fav_Time_Of_Day ';
	END IF;
    IF IN_MEASURE = "DOW" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' Lt_Fav_Day_Of_Week ';
		SET @dow_measure=@Query_Measure;
        SET @dow_table=' CDM_Customer_TP_Var ';
        SET @table_used=@dow_table;
        SET @table_per=' CDM_Customer_TP_Var ';
        SET @Mesaure_per=' Lt_Fav_Day_Of_Week ';
	END IF;
    IF IN_MEASURE = "REACH" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' "NA" ';
		SET @reach_measure=@Query_Measure;
        SET @reach_table=' CDM_Customer_TP_Var ';
        SET @table_used=@reach_table;
	END IF;
    IF IN_MEASURE = "RESPONSE" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' "NA" ';
		SET @response_measure=@Query_Measure;
        SET @response_table=' CDM_Customer_TP_Var ';
        SET @table_used=@response_table;
	END IF;
    IF IN_MEASURE = "DATA_COMPLETE" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' "NA" ';
		SET @datacomplete_measure=@Query_Measure;
        SET @datacomplete_table=' CDM_Customer_TP_Var ';
        SET @table_used=@datacomplete_table;
	END IF;
    IF IN_MEASURE = "Points" OR IN_MEASURE = "CARDS" 
	THEN
        SET @dist_attr='ifnull(Point_Balance,0)';
	END IF;
    IF IN_MEASURE = "Redemption" OR IN_MEASURE = "CARDS" 
	THEN
        SET @dist_attr='ifnull(Lt_Pts_Redemption_FD,0)';
	END IF;
    IF IN_AGGREGATION_1 = 'CARDS' and IN_MEASURE <> 'TABLE'
    THEN
		set @view_stmt=concat('create or replace view V_Dashboard_explore_Card as
		(select "Mean" as Card_Name,MAX(`Mean`) as `Value1`,"" as `Value2`, "" as `Value3`,"RECENCY" as `Measure`  from Dashboard_Explore_Variable where Attribute = "',IN_MEASURE,'")
		Union
        (select "Median" as Card_Name,MAX(`Median`) as `Value1`,"" as `Value2`, "" as `Value3`,"RECENCY" as `Measure`  from Dashboard_Explore_Variable where Attribute = "',IN_MEASURE,'")
		Union
        (select "Mode" as Card_Name,MAX(`Mode`) as `Value1`,"" as `Value2`, "" as `Value3`,"RECENCY" as `Measure`  from Dashboard_Explore_Variable where Attribute = "',IN_MEASURE,'")
		Union
        (select "SD" as Card_Name,MAX(`SD`) as `Value1`,"" as `Value2`, "" as `Value3`,"RECENCY" as `Measure`  from Dashboard_Explore_Variable where Attribute = "',IN_MEASURE,'")
       ');
        
        SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Card_Name",`Card_Name`,"Value1",`Value1`,"Value2",`Value2`,"Value3",`Value3`,"Measure",`Measure`) )),"]")into @temp_result from V_Dashboard_explore_Card;' ;
		select @view_stmt;
		select @Query_String;
	END IF;
    
    IF IN_AGGREGATION_1 = 'SEGMENT' and IN_MEASURE <> 'TABLE'
    THEN
       /* SET @view_stmt=concat('create or replace view Dashboard_explore_variable_segment as
		select c.Description as "Segment", round(avg(',@Query_Measure,'),0) Value from ',@table_used,' a join svoc_segments b on a.Customer_Id=b.Customer_Id
        join (select * from svoc_segments_master where SVOC_Segment_For_Model_Id=3) c on b.Seg_For_model_3 = c.Name
		group by 1
        having Value > 0
        order by c.Sequence
       ');
         */
        select @view_stmt as 'Dashboard_explore_variable_segment';
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`Segment`,"Value",`Value`) )),"]")into @temp_result from Dashboard_Explore_Variable where Attribute = "',IN_MEASURE,'";') ;
	END IF;
	

    IF IN_AGGREGATION_1 = 'BANDS' and IN_MEASURE <> 'TABLE'
    THEN
		IF IN_MEASURE IN ('Recency','ABV','ADGBT','Frequency(ever)','Value(ever)','Discount%','Tenure','Cat Range','Product Range','Value(12mth)','Frequency(12mth)')
        THEN
			SET @view_stmt=concat('CREATE OR REPLACE VIEW V_Dashboard_explore_variable_band AS
								select case when lower_limit=upper_limit then round(lower_limit) else concat(round(lower_limit)," to ",case when upper_limit = -1 then "Max" else round(upper_limit) END) END as Band,
                                round(Percent_Of_Customers*100,0) as Value
                                from Dashboard_Insights_Customer_Distribution
                                where attribute = "',@dist_attr,'"
                                ');
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`Band`,"Value",`Value`) )),"]")into @temp_result from V_Dashboard_explore_variable_band ;' ;
        select @Query_String,@view_stmt;
        
        ELSE
			
			SET @sql1 = concat('select group_concat(bm) into @bands from 
			(select max(qm) as bm from 
			(select Customer_Id,',@Query_Measure,' as qm,ntile(5) over(order by ',@Query_Measure,') as bin from ',@table_used,') A
			group by bin)B');
			
			select @sql1;
			PREPARE statement from @sql1;
			Execute statement;
			Deallocate PREPARE statement;
			
			select @bands;
			select round(substring_index(substring_index(@bands,",",1),",",-1)) into @b1;
			select round(substring_index(substring_index(@bands,",",2),",",-1)) into @b2;
			select round(substring_index(substring_index(@bands,",",3),",",-1)) into @b3;
			select round(substring_index(substring_index(@bands,",",4),",",-1)) into @b4;
			select round(substring_index(substring_index(@bands,",",5),",",-1)) into @b5;
			
			select @b1,@b2,@b3,@b4,@b5;
			
			select count(1) into @total from CDM_Customer_TP_Var;
		
			SET @view_stmt=concat('CREATE OR REPLACE VIEW V_Dashboard_explore_variable_band AS
			select concat("0 to ",',@b1,') as Band,round((count(Customer_Id)/',@total,')*100,0) as Value from ',@table_used,' where ',@Query_Measure,' between 0 and ',@b1,'
			UNION
			select concat("",',@b1+1,' ," to ",',@b2,'),round((count(Customer_Id)/',@total,')*100,0) from ',@table_used,' where ',@Query_Measure,' between ',@b1+1,' and ',@b2,'
			UNION
			select concat("",',@b2+1,' ," to ",',@b3,'),round((count(Customer_Id)/',@total,')*100,0) from ',@table_used,' where ',@Query_Measure,' between ',@b2+1,' and ',@b3,'
			UNION
			select concat("",',@b3+1,' ," to ",',@b4,'),round((count(Customer_Id)/',@total,')*100,0) from ',@table_used,' where ',@Query_Measure,' between ',@b3+1,' and ',@b4,'
			UNION
			select concat("",',@b4+1,' ," to ",',@b5,'),round((count(Customer_Id)/',@total,')*100,0) from ',@table_used,' where ',@Query_Measure,' between ',@b4+1,' and ',@b5,';');
			
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`Band`,"Value",`Value`) )),"]")into @temp_result from V_Dashboard_explore_variable_band ;' ;
			select @Query_String,@view_stmt;
		END IF;
			
	END IF;
	IF IN_MEASURE <> "DOWNLOAD"
    THEN
	select @view_stmt;
	PREPARE statement from @view_stmt;
	Execute statement;
	Deallocate PREPARE statement;

	select @Query_String;
	PREPARE statement from @Query_String;
	Execute statement;
	Deallocate PREPARE statement; 
    SET out_result_json = @temp_result;
    END IF;
    
END$$
DELIMITER ;
