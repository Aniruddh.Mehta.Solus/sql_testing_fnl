DELIMITER $$
CREATE or replace PROCEDURE `S_dashboard_insights_ProductView_Repeat`(IN in_request_json json, OUT out_result_json json)
BEGIN

   declare IN_USER_NAME varchar(240); 
   declare IN_SCREEN varchar(400);
   declare IN_MEASURE text; 
   declare IN_FILTER_CURRENTMONTH text; 
   declare IN_FILTER_CURRENTYEAR text;
   declare IN_FILTER_MONTHDURATION text; 
   declare IN_NUMBER_FORMAT text;
   declare IN_AGGREGATION_1 text; 
   declare IN_AGGREGATION_2 text;
   declare IN_AGGREGATION_3 text;
   declare Query_String text;
   declare Query_Measure_Name varchar(100);
   
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    
   IF IN_SCREEN = 'Any Repeat' THEN
   IF IN_NUMBER_FORMAT = 'NUMBER' THEN
      IF IN_MEASURE = 'L1M' THEN
         set @view_stmt = concat('create or replace view V_Dashboard_Insights_Product_AnyRepeat as
                                  select Product_Name,
									sum(T1MthActive) as `RepeatCust` 
									from Dashboard_Insights_Base_ByProduct
									where Product_Type = "AnyRepeat"
									group by Product_Name limit 20;;');      
      ELSEIF IN_MEASURE = 'L6M' THEN
          set @view_stmt = concat('create or replace view V_Dashboard_Insights_Product_AnyRepeat as
                                  select Product_Name,
									sum(T6MthActive) as `RepeatCust` 
									from Dashboard_Insights_Base_ByProduct
									where Product_Type = "AnyRepeat"
									group by Product_Name limit 20;');
	  ELSE
          set @view_stmt = concat('create or replace view V_Dashboard_Insights_Product_AnyRepeat as
                                  select Product_Name,
									sum(T12MthActive) as `RepeatCust` 
									from Dashboard_Insights_Base_ByProduct
									where Product_Type = "AnyRepeat"
									group by Product_Name limit 20;');
	  END IF;
   END IF;
   IF IN_NUMBER_FORMAT = 'PERCENT' THEN
      IF IN_MEASURE = 'L1M' THEN
         set @view_stmt = concat('create or replace view V_Dashboard_Insights_Product_AnyRepeat as
                                  select Product_Name, 
									round(((ifnull(sum(T1MthActive),0)/(select ifnull(sum(T1MthActive),0) from Dashboard_Insights_Base_ByProduct where Product_Type = "Bought"))*100),2) as `RepeatCust`
									from Dashboard_Insights_Base_ByProduct 
									where Product_Type = "AnyRepeat"
									group by Product_Name limit 20;');      
      ELSEIF IN_MEASURE = 'L6M' THEN
          set @view_stmt = concat('create or replace view V_Dashboard_Insights_Product_AnyRepeat as
                                  select Product_Name, 
									round(((ifnull(sum(T6MthActive),0)/(select ifnull(sum(T6MthActive),0) from Dashboard_Insights_Base_ByProduct where Product_Type = "Bought"))*100),2) as `RepeatCust`
									from Dashboard_Insights_Base_ByProduct 
									where Product_Type = "AnyRepeat"
									group by Product_Name limit 20;');
	  ELSE
          set @view_stmt = concat('create or replace view V_Dashboard_Insights_Product_AnyRepeat as
                                  select Product_Name, 
									round(((ifnull(sum(T12MthActive),0)/(select ifnull(sum(T12MthActive),0) from Dashboard_Insights_Base_ByProduct where Product_Type = "Bought"))*100),2) as `RepeatCust`
									from Dashboard_Insights_Base_ByProduct 
									where Product_Type = "AnyRepeat"
									group by Product_Name limit 20;');
	  END IF;
   END IF;
       set @Query_String = 'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`product_name`,"Value", `RepeatCust`))),"]")into @temp_result from V_Dashboard_Insights_Product_AnyRepeat;';
   END IF;
   
   
   IF IN_SCREEN = 'Same Repeat' THEN
   IF IN_NUMBER_FORMAT = 'NUMBER' THEN
      IF IN_MEASURE = 'L1M' THEN
         set @view_stmt = concat('create or replace view V_Dashboard_Insights_Product_SameRepeat as
                                  select Product_Name,
									sum(T1MthActive) as `RepeatCust` 
									from Dashboard_Insights_Base_ByProduct
									where Product_Type = "SameRepeat"
									group by Product_Name limit 20;');      
      ELSEIF IN_MEASURE = 'L6M' THEN
          set @view_stmt = concat('create or replace view V_Dashboard_Insights_Product_SameRepeat as
                                  select Product_Name,
									sum(T6MthActive) as `RepeatCust` 
									from Dashboard_Insights_Base_ByProduct
									where Product_Type = "SameRepeat"
									group by Product_Name limit 20;');
	  ELSE
          set @view_stmt = concat('create or replace view V_Dashboard_Insights_Product_SameRepeat as
                                  select Product_Name,
								sum(T12MthActive) as `RepeatCust` 
								from Dashboard_Insights_Base_ByProduct
								where Product_Type = "SameRepeat"
								group by Product_Name limit 20;');
	  END IF;
   END IF;
   IF IN_NUMBER_FORMAT = 'PERCENT' THEN
      IF IN_MEASURE = 'L1M' THEN
         set @view_stmt = concat('create or replace view V_Dashboard_Insights_Product_SameRepeat as
                                  select Product_Name, 
									round(((ifnull(sum(T1MthActive),0)/(select ifnull(sum(T1MthActive),0) from Dashboard_Insights_Base_ByProduct where Product_Type = "Bought"))*100),2) as `RepeatCust`
									from Dashboard_Insights_Base_ByProduct 
									where Product_Type = "SameRepeat"
									group by Product_Name limit 20;');      
      ELSEIF IN_MEASURE = 'L6M' THEN
          set @view_stmt = concat('create or replace view V_Dashboard_Insights_Product_SameRepeat as
                                  select Product_Name, 
									round(((ifnull(sum(T6MthActive),0)/(select ifnull(sum(T6MthActive),0) from Dashboard_Insights_Base_ByProduct where Product_Type = "Bought"))*100),2) as `RepeatCust`
									from Dashboard_Insights_Base_ByProduct 
									where Product_Type = "SameRepeat"
									group by Product_Name limit 20;');
	  ELSE
          set @view_stmt = concat('create or replace view V_Dashboard_Insights_Product_SameRepeat as
                                  select Product_Name, 
									round(((ifnull(sum(T12MthActive),0)/(select ifnull(sum(T12MthActive),0) from Dashboard_Insights_Base_ByProduct where Product_Type = "Bought"))*100),2) as `RepeatCust`
									from Dashboard_Insights_Base_ByProduct 
									where Product_Type = "SameRepeat"
									group by Product_Name limit 20;');
	  END IF;
   END IF;
       set @Query_String = 'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`product_name`,"Value", `RepeatCust`))),"]")into @temp_result from V_Dashboard_Insights_Product_SameRepeat;';
   END IF;
    
   IF IN_SCREEN <> "DOWNLOAD"
    THEN
        select @view_stmt;
	    PREPARE statement from @view_stmt;
	    Execute statement;
	    Deallocate PREPARE statement;
    
		select @Query_String;
		PREPARE statement from @Query_String;
		Execute statement;
		Deallocate PREPARE statement; 
		SET out_result_json = @temp_result;
    END IF;
    
END$$
DELIMITER ;
