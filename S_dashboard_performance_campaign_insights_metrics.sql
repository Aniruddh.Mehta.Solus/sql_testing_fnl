DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_performance_campaign_insights_metrics`()
BEGIN
DECLARE vStart_Cnt BIGINT DEFAULT 0;
DECLARE vEnd_Cnt BIGINT DEFAULT 0;
DECLARE vRec_Cnt BIGINT DEFAULT 0;

DELETE from log_solus where module = 'S_dashboard_performance_campaign_insights_metrics';
SET SQL_SAFE_UPDATES=0;

INSERT INTO Dashboard_Metrics_Log
(
`Screen_Name`,
`Status`
)
VALUES
('INTELLIGENCE - DASHBOARD',
'Started');


INSERT INTO log_solus(module, msg) values ('S_dashboard_performance_campaign_insights_metrics','Loading Data for all the Customers');

select replace(Value,"_SOLUS_PROD","") into @client_name from M_Config where Name = "Tenant_Name";

DROP TABLE IF EXISTS Dashboard_Performance_Campaign_Insights;
CREATE TABLE `Dashboard_Performance_Campaign_Insights` (
    `Client_Name` varchar(64) DEFAULT NULL,
    `Trigger` VARCHAR(100) DEFAULT NULL,
    `Theme` VARCHAR(100) DEFAULT NULL,
    `Trigger_Type` CHAR(50) DEFAULT NULL,
    `EE_Date` date NOT NULL,
    `Chan` CHAR(30) DEFAULT NULL,
    `Type` CHAR(3) DEFAULT NULL,
    `Size` CHAR(3) DEFAULT NULL,
    `Recommendation` VARCHAR(10) DEFAULT NULL,
    `Personalization_Score` INT(3) DEFAULT NULL,
    `Pers` CHAR(20) DEFAULT NULL,
    `Offer` CHAR(20) DEFAULT NULL,
    `Offer_Type` CHAR(20) DEFAULT NULL,
    `Has_Shortened_URL` CHAR(10) DEFAULT NULL,
    `DOW_num` CHAR(20) DEFAULT NULL,
    `DOW` CHAR(20) DEFAULT NULL,
    `WEEKDAY_WEEKEND`CHAR(20) DEFAULT NULL,
    `Segment` CHAR(20) DEFAULT NULL,
    `Campaign_Segment` varchar(64) DEFAULT NULL,
	`Rcore_Story` varchar(512) DEFAULT NULL,
    `Event_ID` INT(11) DEFAULT NULL,
    `Event_Execution_Date_ID` INT(11) DEFAULT NULL,
    `YM` INT(6) DEFAULT NULL,
    `Target_Base` BIGINT(20) DEFAULT 0,
    `Target_Delivered` BIGINT(20) DEFAULT 0,
    `Target_Bills` BIGINT(20) DEFAULT 0,
    `Incremental_Revenue` BIGINT(20) DEFAULT 0.00,
    `Incremental_Revenue_By_Day` decimal(20,2) DEFAULT 0.00,
    `MTD_Incremental_Revenue` bigint(20) DEFAULT 0,
    `Incremental_Revenue_By_Segment_By_Day` decimal(20,2) DEFAULT 0.00,
    `MTD_Incremental_Revenue_By_Segment` bigint(20) DEFAULT 0,
    `Conversion_Per` decimal (20,2) DEFAULT 0,
	`Client_Industry` varchar(64) DEFAULT NULL,
	`Client_Platform` varchar(64) DEFAULT NULL,
    `Created_Date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP (),
    KEY `Event_ID` (`Event_ID`),
    KEY `YM` (`YM`),
    KEY `Event_Execution_Date_ID` (`Event_Execution_Date_ID`),
    KEY `Trigger` (`Trigger`),
    KEY `EE_Date` (`EE_Date`)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8;


INSERT INTO Dashboard_Performance_Campaign_Insights
(
Event_Id,
`Theme`,
`Segment`,
`Type`,
`EE_Date`,
`Trigger`,
Trigger_Type,
`Recommendation`,
`Personalization_Score`,
`Offer`,
Has_Shortened_URL,
Campaign_Segment,
Rcore_Story,
Event_Execution_Date_ID,
Target_Base,
Target_Bills,
Target_Delivered,
Incremental_Revenue,
Incremental_Revenue_By_Day,
MTD_Incremental_Revenue,
Incremental_Revenue_By_Segment_By_Day,
MTD_Incremental_Revenue_By_Segment,
YM,
Client_Name
)
SELECT 
a.Event_Id,
b.CampaignThemeName,
a.Segment,
CASE WHEN datediff(End_Date_ID,Start_Date_ID) < 32 THEN 'GTM' else 'CLM' END AS `Type`,
a.EE_Date,
b.Name as `Trigger`,
b.Waterfall as Trigger_Type,
-- case when Creative1 RLIKE "Reco" then "Reco" else "No Reco" end as `Recommendation`,
CASE WHEN c.Number_Of_Recommendation <=0 THEN "No Reco" ELSE "Reco" END AS `Recommendation`,
(length(Creative1)-length(replace(Creative1,"|","")))+1 AS Personalization_Score,
case when Creative1 RLIKE "Voucher|Offer|Flat|BOGO|Percent" then "Offer" else "No Offer" end as Offer,
case when Creative1 RLIKE "s00.ai" then "Yes" else "No" end as Has_Shortened_URL,
MicroSegmentName as Campaign_Segment,
case when c.Name is NULL then "NA" else c.Name end as Rcore_Story,
Event_Execution_Date_ID,
Target_Base,
Target_Bills,
Target_Delivered,
Incremental_Revenue,
Incremental_Revenue_By_Day,
MTD_Incremental_Revenue,
Incremental_Revenue_By_Segment_By_Day,
MTD_Incremental_Revenue_By_Segment,
YM,
@client_name
FROM CLM_Event_Dashboard a 
JOIN Event_Master b 
ON a.Event_ID = b.ID
LEFT JOIN Communication_Template c 
ON b.Communication_Template_ID=c.ID
LEFT JOIN RankedPickList_Stories_Master d
ON b.RankedPickListStory_Id = d.Story_Id;

UPDATE Dashboard_Performance_Campaign_Insights A, 
(select ID from Event_Master where Creative1 LIKE '%PERCENT%' ) AS B
SET A.Offer_Type = 'PERCENT'
WHERE A.Event_Id = B.ID;

UPDATE Dashboard_Performance_Campaign_Insights A, 
(select ID from Event_Master where Creative1 LIKE '%FLAT%' ) AS B
SET A.Offer_Type = 'FLAT'
WHERE A.Event_Id = B.ID;

UPDATE Dashboard_Performance_Campaign_Insights
SET Offer_Type = 'OTHERS'
WHERE Offer_Type is null and Offer = "Offer";

UPDATE Dashboard_Performance_Campaign_Insights A,
(select Event_ID from Dashboard_Performance_Campaign_Insights where `Offer` is null) B
set A.`Offer` = "No Offer"
where A.Event_ID = B.Event_ID;

UPDATE Dashboard_Performance_Campaign_Insights
SET Pers = CASE WHEN Personalization_Score = 0 THEN 'No Personalization'
				WHEN Personalization_Score = 1 THEN '1 Pers. field'
                WHEN Personalization_Score = 2 THEN '2 Pers. field'
                WHEN Personalization_Score >= 3 THEN '3 or more Pers.' END;
                
UPDATE Dashboard_Performance_Campaign_Insights A,
(
select A.Event_Id, B.`Name`, CASE WHEN COUNT(DISTINCT Customer_Id) > (SELECT `Value1` FROM UI_Configuration_Global WHERE `Config_Name` = 'Trigger_Size_XL') THEN 'XL'
						WHEN COUNT(DISTINCT Customer_Id) BETWEEN (SELECT `Value1` FROM UI_Configuration_Global WHERE `Config_Name` = 'Trigger_Size_L') AND (SELECT `Value2` FROM UI_Configuration_Global WHERE `Config_Name` = 'Trigger_Size_L') THEN 'L' 
                        WHEN COUNT(DISTINCT Customer_Id) BETWEEN (SELECT `Value1` FROM UI_Configuration_Global WHERE `Config_Name` = 'Trigger_Size_M') AND (SELECT `Value2` FROM UI_Configuration_Global WHERE `Config_Name` = 'Trigger_Size_M')THEN 'M'
                        WHEN COUNT(DISTINCT Customer_Id) BETWEEN (SELECT `Value1` FROM UI_Configuration_Global WHERE `Config_Name` = 'Trigger_Size_S') AND (SELECT `Value2` FROM UI_Configuration_Global WHERE `Config_Name` = 'Trigger_Size_S') THEN 'S'
                        WHEN COUNT(DISTINCT Customer_Id) < (SELECT `Value1` FROM UI_Configuration_Global WHERE `Config_Name` = 'Trigger_Size_XS') THEN 'XS' END AS Size FROM Event_Execution_History A, Event_Master B WHERE A.Event_Id = B.ID GROUP BY 1,2
) AS B
SET A.`Size` = B.`Size`
WHERE A.Event_Id = B.Event_Id;


UPDATE Dashboard_Performance_Campaign_Insights A,
    (SELECT 
        dash.Event_ID, dash.EE_Date, WEEKDAY(EE_Date) AS DOW
    FROM
        CLM_Event_Dashboard dash, Event_Master em
    WHERE
        dash.Event_ID = em.ID) AS B 
SET 
    A.DOW_num = B.DOW
WHERE
    A.Event_ID = B.Event_ID and 
    A.EE_Date = B.EE_Date;

UPDATE Dashboard_Performance_Campaign_Insights
SET DOW = Case when `DOW_num` = 0 then "Monday"
when `DOW_num` = 1 then "Tuesday"
when `DOW_num` = 2 then "Wednesday"
when `DOW_num` = 3 then "Thursday"
when `DOW_num` = 4 then "Friday"
when `DOW_num` = 5 then "Saturday"
else "Sunday" end;

UPDATE Dashboard_Performance_Campaign_Insights
SET WEEKDAY_WEEKEND = Case when `DOW_num` BETWEEN 0 AND 4 THEN "WEEKDAY" 
ELSE "WEEKEND" end;

UPDATE Dashboard_Performance_Campaign_Insights A, 
Event_Master B
SET 
A.Chan = B.ChannelName
WHERE A.Event_ID = B.ID;

UPDATE Dashboard_Metrics_Log
SET `Status` = 'Ended' WHERE Screen_Name = 'INTELLIGENCE - DASHBOARD';


END$$
DELIMITER ;
