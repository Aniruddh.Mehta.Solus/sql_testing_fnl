
CREATE or replace PROCEDURE `S_UI_Instant_Campaign_Creation`(IN in_request_json json, OUT out_result_json json)
BEGIN
    
    

    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
    declare EDIT Bigint; 
    

	declare Trigger_Name text; 
	declare Campaign_Segment text;
	declare Lifecyle_Segment text;
	declare Throttling_Level bigint;
    declare `Channel` text;
	declare Creative text;
	declare WA_template_name text;
    declare Created_By text;
    declare File_Type text;
    declare Solus_Attr text;
    declare Header_text text;
	SET SQL_SAFE_UPDATES=0;
	SET foreign_key_checks=0;

	Set in_request_json = Replace(in_request_json,'\n','\\n');
  
   
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));


    SET Trigger_Name = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
    SET Campaign_Segment = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
    SET Lifecyle_Segment = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
    SET Throttling_Level = json_unquote(json_extract(in_request_json,"$.AGGREGATION_4"));
    SET `Channel` = json_unquote(json_extract(in_request_json,"$.AGGREGATION_5"));
    SET Creative = json_unquote(json_extract(in_request_json,"$.AGGREGATION_6"));
    SET WA_template_name = json_unquote(json_extract(in_request_json,"$.AGGREGATION_7"));
 SET Created_By = json_unquote(json_extract(in_request_json,"$.AGGREGATION_8"));
  SET File_Type = json_unquote(json_extract(in_request_json,"$.Type"));
   SET Solus_Attr = json_unquote(json_extract(in_request_json,"$.Solus_Attr"));
    SET Header_text = json_unquote(json_extract(in_request_json,"$.Header_text"));
	Set @temp_result =Null;
    Set @vSuccess ='';
	
	Set @UserId=IN_USER_NAME;   
	Set @vCLMSegmentName=Lifecyle_Segment; 
	Set @vMicroSegmentName=Campaign_Segment; 
	Set @vChannelName=`Channel`; 
	Set @Throttling = Throttling_Level;
	Set @vTriggerName=Trigger_Name;
	Set @vCreative=Creative;
    	Set @Created_By=Created_By;
	Set @vErrMsg='';
	Set @vSuccess='';
	Set @Err =0;
	set @File_Type = File_Type;
    set @Solus_Attr = Solus_Attr ;
    set @Header_text= Header_text ;
    
    set @Solus_Attr = replace(@Solus_Attr,'"','');
set @Solus_Attr = replace(@Solus_Attr,']','');
set @Solus_Attr = replace(@Solus_Attr,'[','');
  set @Header_text = replace(@Header_text,'"','');
set @Header_text = replace(@Header_text,']','');
set @Header_text = replace(@Header_text,'[','');

    
    
	if @vTriggerName != '' 
    then 
	insert into Event_Master_Instant_Campaign( Trigger_Name, Theme_Name, `Channel`, Creative, LifeCycle_Segment, Campaign_Segment, Throttling, TimeSlot, Status,Created_By,Downstream_Type,Header_Text,Header_Column)
    values (@vTriggerName,'Instant_Campaign',@vChannelName,@vCreative,@vCLMSegmentName,@vMicroSegmentName,
    @Throttling,'1','Event_Created',@Created_By,@File_Type,@Header_text,@Solus_Attr);
    
    insert into log_solus_instant_campaign(Trigger_Name,Process_Name,Status,Log_Time)
    values (@vTriggerName,'Event_Created','Sucess',current_timestamp());
		
        	Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Success") )),"]") 
														into @temp_result                       
												      ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
                            
	end if;
    
    if @temp_result is null
		Then
								
							Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Invalid Parameters") )),"]") 
														into @temp_result                       
												      ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
						
	End if;
							
    SET out_result_json = @temp_result;


END


