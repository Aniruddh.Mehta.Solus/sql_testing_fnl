DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_metrics_automation_appendjson`(IN IN_SCREEN varchar(128), IN IN_MEASURE varchar(128), IN IN_COMPONENT varchar(128), OUT out_result_json JSON)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_FILTER_CURRENTMONTH text; 
    declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
    declare IN_AGGREGATION_4 text;
    declare IN_AGGREGATION_5 text;
    declare IN_AGGREGATION_6 text;
   
	
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
    

			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`Xaxis`,"Value1",`Yaxis1`) )),"]")into @temp_result from Dashboard_Response_Temp;' ;
            
	IF IN_MEASURE <> "DOWNLOAD"
    THEN
    
	select @Query_String;
	PREPARE statement from @Query_String;
	Execute statement;
	Deallocate PREPARE statement; 
    SET out_result_json = @temp_result;
    END IF;

    
END$$
DELIMITER ;
