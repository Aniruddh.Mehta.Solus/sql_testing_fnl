DELIMITER $$
CREATE or replace PROCEDURE `S_dashboard_insights_ProductView_Association_AttrSelection`(IN in_request_json json,OUT out_result_json JSON)
BEGIN

   declare IN_ATTR_FILTER varchar(25);
   declare IN_ATTR_FILTER_1 varchar(25);
   -- declare attrName varchar(50);
   declare IN_AGGREGATION_1 text; 
   declare IN_AGGREGATION_2 text;
   declare IN_USER_NAME varchar(30); 
   declare IN_MEASURE varchar(30); 
   declare IN_METRICS_NAME varchar(128);
   declare IN_SUB_METRICS_NAME varchar(128);
   declare IN_STORE_PROC_OR_SHELL varchar(128);
   declare IN_STATUS_WORD varchar(128);
   
   set IN_ATTR_FILTER = json_unquote(json_extract(in_request_json,"$.ATTR_FILTER"));
   set IN_ATTR_FILTER_1 = json_unquote(json_extract(in_request_json,"$.ATTR_FILTER_1"));
   set IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
   set IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
   set IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
   set IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
   set IN_METRICS_NAME = json_unquote(json_extract(in_request_json,"$.METRICS_NAME"));
   set IN_SUB_METRICS_NAME = json_unquote(json_extract(in_request_json,"$.SUB_METRICS_NAME"));
   set IN_STORE_PROC_OR_SHELL = json_unquote(json_extract(in_request_json,"$.STORE_PROC_OR_SHELL"));
   set IN_STATUS_WORD = json_unquote(json_extract(in_request_json,"$.STATUS_WORD"));
   
   CREATE TABLE IF NOT EXISTS UI_Product_Configuration_Histroy(
       id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
       prev_config varchar(50),
       update_config varchar(50),
       comments text,
       User_Name varchar(100),
       Last_Modified datetime DEFAULT current_timestamp()
   );
   
   IF IN_AGGREGATION_1 = 'Show_Col' THEN
      set @Query_String = concat('select concat("[",(group_concat(distinct json_object("Attribute",`LOV_Attribute`))),"]") into @temp_res from CDM_LOV_Master where LOV_Attribute="BRAND" 
									or LOV_Attribute="BU" or LOV_Attribute="CAT" or LOV_Attribute="DEP" or LOV_Attribute="PRODUCT"
									or LOV_Attribute="SEASON" or LOV_Attribute="SECTION" or LOV_Attribute="MANUF" or LOV_Attribute="PRODUCT_GENOME" ;');
	
   END IF;
   
   select Value1 into @prevval from UI_Configuration_Global where Config_Name="Product_View_Attribute";
   select "before:",@prevval;
   
   IF IN_AGGREGATION_1 = 'CONFIG_HISTROY' THEN
     select IN_ATTR_FILTER;
     set @attrName = concat(IN_ATTR_FILTER , "_LOV_ID" );
     select @attrName;
	   IF @attrName!=@prevval THEN
		INSERT INTO UI_Product_Configuration_Histroy(prev_config,update_config,comments,User_Name)
		VALUES(@prevval,@attrName,'Config Changed. Run Metrics Again',IN_USER_NAME);
	   ELSE
		INSERT INTO UI_Product_Configuration_Histroy(prev_config,update_config,comments,User_Name)
		VALUES(@prevval,@attrName,'Same Product Config.',IN_USER_NAME);
	   END IF;
       
       delete from UI_Configuration_Global where Config_Name='Product_View_Attribute';
       insert into UI_Configuration_Global(Config_Name, Value1) values ('Product_View_Attribute', @attrName);

	   set @view_stmt = concat('create or replace view CDM_Product_Master_ProductView as
								   select CDM_Product_Master.Product_Id as Product_Id,  CDM_Product_Master.',@attrName,' as ProductView_Product_Id,
                                   CDM_LOV_Master.LOV_Key  AS  Product_Name 
								   from (CDM_Product_Master JOIN CDM_LOV_Master)
                                   WHERE
                                   CDM_Product_Master . ',@attrName,'  =  CDM_LOV_Master . LOV_Id;');
                                   
			select @view_stmt;
			PREPARE statement from @view_stmt;
			Execute statement;
			Deallocate PREPARE statement;
	
			   IF IN_MEASURE = 'SHOW_MESSAGE' THEN
			  select IN_ATTR_FILTER;
			  set @attrName1 = concat(IN_ATTR_FILTER , "_LOV_ID" );
			  
			  IF @attrName1!=@prevval THEN
				 SET @Query_String = concat('select json_object("message","Previous Configuration : ',@prevval,'. Selected Configuration : ',@attrName1,'. Configuration Changed.Run Metrics Again.") into @temp_res');
			  ELSE
				 SET @Query_String = concat('select json_object("message","Previous Configuration : ',@prevval,'. Selected Configuration : ',@attrName1,'.Same Product Configuration.") into @temp_res');
			  END IF;
			 END IF;
    
   END IF;
   
   IF IN_AGGREGATION_1 = 'DISPLAY' THEN
     SET @Query_String = concat('select concat("[",(group_concat(distinct json_object("Last_Changed",`Last_Modified`,"Update_value",`update_config`,"User_Name",`User_Name`,"Comments",`comments`))),"]") into @temp_res from UI_Product_Configuration_Histroy WHERE id > (SELECT MAX(id) - 3 from UI_Product_Configuration_Histroy);');
   END IF;
   
   IF IN_AGGREGATION_1 = 'CONFIG_TABLE_VALUES' THEN
   
      SET @attrtype = concat(IN_ATTR_FILTER,'_LOV_ID');
      SET @view_stmt = concat('create or replace view V_Config_Value_Table as 
							   select CDM_LOV_Master.LOV_Key  AS  Product_Name,
								count(CDM_LOV_Master.LOV_Key)  AS count 
								from CDM_Product_Master JOIN CDM_LOV_Master
								ON
								CDM_Product_Master.',@attrtype,'  =  CDM_LOV_Master.LOV_Id
								group by CDM_LOV_Master.LOV_Key;');
	
      select @view_stmt;
	  PREPARE statement from @view_stmt;
	  Execute statement;
	  Deallocate PREPARE statement;
      
      SET @Query_String = concat('select concat("[",(group_concat(distinct json_object("Product",`Product_Name`,"Product Count",`Count`))),"]") into @temp_res from V_Config_Value_Table;');
   END IF;
   
   -- IF IN_MEASURE = 'RUN_METRICS' THEN
--       INSERT INTO Dashboard_InsightProduct_Metrics_Monitoring(ID,USER_NAME,METRICS_NAME,PRODUCT_ATTRIBUTE,SUB_METRICS_NAME,STORE_PROC_OR_SHELL,STATUS_WORD,STATUS_PERCENT,START_TIME,END_TIME,MODIFITED_DATE)
-- 	  VALUES(IN_USER_NAME,IN_METRICS_NAME,IN_ATTR_FILTER,IN_SUB_METRICS_NAME,IN_STORE_PROC_OR_SHELL,IN_STATUS_WORD,0,current_timestamp(),"");
--    END IF;	

	IF IN_MEASURE = 'RUN_METRICS' THEN
		IF IN_AGGREGATION_2 = 'COMPUTE' THEN
			CALL `PLUM_SOLUS_REPORTING`.s_solus_scheduler_createOnDemandJob('ProductViewMetrics','mysolus.dashboard.insights.ProductView.sh',1,@result);
			if @result != null THEN
				SET @Query_String = concat('select concat("[",json_object("message",',@result,'),"]") into @temp_res');
			else
				SET @Query_String = concat('select concat("[",json_object("message","Computation will start in 1 minute."),"]") into @temp_res');
			END IF;
		END IF;
       
		IF IN_AGGREGATION_2 = 'UPDATE' THEN
			CALL `PLUM_SOLUS_REPORTING`.s_solus_scheduler_getJobStatus('mysolus.dashboard.insights.ProductView.sh','ONDEMAND',@res1,@res2,@res3,@res4,@res5);
				SET @Query_String = concat('select concat("[",json_object("message","Progress of Computation: ',@res2,'."),"]") into @temp_res');
		END IF;
    END IF;
    
    IF IN_MEASURE = 'Attr_Analysis' THEN
       IF IN_AGGREGATION_1 = 'Show_column' THEN
          SET @Query_String = concat('select concat("[",(group_concat(distinct json_object("Attribute",`Product_Attr`))),"]") into @temp_res from Dashboard_Insights_Product_Metrics_Configuration_History');
       END IF;
    END IF;
    
    IF IN_MEASURE = 'Show_Attr_Table' THEN
       IF IN_AGGREGATION_1 = 'Load_Table' THEN
       select 'a';
          SET @attrtype1 = IN_ATTR_FILTER_1;
          select @attrtype1;
         SET @view_stmt = concat('create or replace view V_Config_Value_Table as 
							   select CDM_LOV_Master.LOV_Key  AS  Product_Name,
								count(CDM_LOV_Master.LOV_Key)  AS count 
								from CDM_Product_Master JOIN CDM_LOV_Master
								ON
								CDM_Product_Master.',@attrtype1,'  =  CDM_LOV_Master.LOV_Id
								group by CDM_LOV_Master.LOV_Key;');
                                
		select @view_stmt;
	  PREPARE statement from @view_stmt;
	  Execute statement;
	  Deallocate PREPARE statement;
      
      SET @Query_String = concat('select concat("[",(group_concat(distinct json_object("Product",`Product_Name`,"Count",`Count`))),"]") into @temp_res from V_Config_Value_Table;');
       END IF;
    END IF;
    
    select @Query_String;
	  PREPARE statement from @Query_String;
	  Execute statement;
	  Deallocate PREPARE statement; 		
      set out_result_json = @temp_res;
END$$
DELIMITER ;
