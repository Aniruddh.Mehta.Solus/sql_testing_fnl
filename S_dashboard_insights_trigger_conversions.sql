DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_insights_trigger_conversions`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    
    
	SET @FY_SATRT_DATE = date_format(date_sub(concat(IN_FILTER_CURRENTMONTH,'01'), INTERVAL 11 MONTH),"%Y%m");
	SET @FY_END_DATE = IN_FILTER_CURRENTMONTH;
     
	select @FY_SATRT_DATE, @FY_END_DATE;
     
	SET @filter_cond=concat(" Bill_Year_Month = '",@FY_END_DATE,"'");
	SET @date_select=' date_format(concat(Bill_Year_Month,"01"),"%b-%Y") AS "Year" ';
    
	IF IN_MEASURE = 'TABLE' AND IN_NUMBER_FORMAT = 'NUMBER'
		THEN
			set @view_stmt= CONCAT(' create or replace view V_Dashboard_Trigger_Conversions_Table as 
							WITH GROUP1 AS
                            (
							SELECT `Trigger` AS "Trigger_Name" ,Bills AS "Value1"
							from Dashboard_Insights_Trigger_Conversion
                            where Event_Execution_Month = "',IN_FILTER_CURRENTMONTH,'"
							and Transaction_Mode = "Online"
                            and Bills IS NOT NULL
							GROUP BY 1
                            )
                             ,
                             GROUP2 AS
							(
                            SELECT `Trigger` AS "Trigger_Name" ,Bills AS "Value2"
							from Dashboard_Insights_Trigger_Conversion
                            where Event_Execution_Month = "',IN_FILTER_CURRENTMONTH,'"
							and Transaction_Mode = "Offline"
                            and Bills IS NOT NULL
							GROUP BY 1
                            )
                            SELECT A.Trigger_Name, Value1, Value2 
                            FROM GROUP1 A
                            JOIN GROUP2 B
                            ON A.Trigger_Name=B.Trigger_Name
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Trigger",`Trigger_Name`,"Online",CAST(format(ifnull(`Value1`,0),",") AS CHAR),"Offline",CAST(format(ifnull(`Value2`,0),",") AS CHAR)) )),"]")into @temp_result from V_Dashboard_Trigger_Conversions_Table;' ;
	END IF;
		
	IF IN_MEASURE = 'TABLE' AND IN_NUMBER_FORMAT = 'COLPERCENT'
		THEN
			set @view_stmt= CONCAT(' create or replace view V_Dashboard_Trigger_Conversions_Table as 
							WITH GROUP1 AS
                            (
							SELECT `Trigger` AS "Trigger_Name" ,Bills_Col_Pct AS "Value1"
							from Dashboard_Insights_Trigger_Conversion
                            where Event_Execution_Month = "',IN_FILTER_CURRENTMONTH,'"
							and Transaction_Mode = "Online"
                            and Bills_Col_Pct IS NOT NULL
							GROUP BY 1
                            )
                             ,
                             GROUP2 AS
							(
                            SELECT `Trigger` AS "Trigger_Name" ,Bills_Col_Pct AS "Value2"
							from Dashboard_Insights_Trigger_Conversion
                            where Event_Execution_Month = "',IN_FILTER_CURRENTMONTH,'"
							and Transaction_Mode = "Offline"
                            and Bills_Col_Pct IS NOT NULL
							GROUP BY 1
                            )
                            SELECT A.Trigger_Name, Value1, Value2 
                            FROM GROUP1 A
                            JOIN GROUP2 B
                            ON A.Trigger_Name=B.Trigger_Name
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Trigger",`Trigger_Name`,"Online",concat(ifnull(`Value1`,0),"%"),"Offline",concat(ifnull(`Value2`,0),"%")) )),"]")into @temp_result from V_Dashboard_Trigger_Conversions_Table;' ;
	END IF;
		
	IF IN_MEASURE = 'TABLE' AND IN_NUMBER_FORMAT = 'ROWPERCENT'
		THEN
			set @view_stmt= CONCAT(' create or replace view V_Dashboard_Trigger_Conversions_Table as 
							WITH GROUP1 AS
                            (
							SELECT `Trigger` AS "Trigger_Name" ,Bills_Row_Pct AS "Value1"
							from Dashboard_Insights_Trigger_Conversion
                            where Event_Execution_Month = "',IN_FILTER_CURRENTMONTH,'"
							and Transaction_Mode = "Online"
                            and Bills_Row_Pct IS NOT NULL
							GROUP BY 1
                            )
                             ,
                             GROUP2 AS
							(
                            SELECT `Trigger` AS "Trigger_Name" ,Bills_Row_Pct AS "Value2"
							from Dashboard_Insights_Trigger_Conversion
                            where Event_Execution_Month = "',IN_FILTER_CURRENTMONTH,'"
							and Transaction_Mode = "Offline"
                            and Bills_Row_Pct IS NOT NULL
							GROUP BY 1
                            )
                            SELECT A.Trigger_Name, Value1, Value2 
                            FROM GROUP1 A
                            JOIN GROUP2 B
                            ON A.Trigger_Name=B.Trigger_Name
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Trigger",`Trigger_Name`,"Online",concat(ifnull(`Value1`,0),"%"),"Offline",concat(ifnull(`Value2`,0),"%")) )),"]")into @temp_result from V_Dashboard_Trigger_Conversions_Table;' ;
	END IF;
	
	
	IF IN_SCREEN <> "DOWNLOAD"
    THEN
		select @view_stmt;
		PREPARE statement from @view_stmt;
		Execute statement;
		Deallocate PREPARE statement;
		select @Query_String;
		PREPARE statement from @Query_String;
		Execute statement;
		Deallocate PREPARE statement; 
		SET out_result_json = @temp_result;
	ELSE
		call S_dashboard_performance_download('V_Dashboard_Trigger_Conversions_Table',@out_result_json1);
		select @out_result_json1 into out_result_json;
    END IF;
	
END$$
DELIMITER ;
