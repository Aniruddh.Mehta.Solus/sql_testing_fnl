DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_insights_metrics_QC`()
BEGIN

select a.bill_year_month, a.base, b.base, a.base-b.base, a.bills, b.bills, a.bills-b.bills,a.rev,b.rev, a.rev-b.rev as x
from 
(
SELECT 
    Bill_Year_Month,
    COUNT(DISTINCT bill_header_id) AS bills,
    SUM(sale_net_val) AS rev,
    COUNT(DISTINCT Customer_Id) AS base
FROM
    CDM_Bill_Details
WHERE Bill_Year_Month=202201
GROUP BY Bill_Year_Month
 ) a,
(SELECT 
    YYYYMM,
    SUM(MonthlyActiveBills) AS bills,
    SUM(MonthlyActiveRevenue) AS rev,
    SUM(MonthlyActive) AS base
FROM
    Dashboard_Insights_Base
WHERE
    AGGREGATION = 'BY_MONTH'
AND YYYYMM=202201
GROUP BY YYYYMM)
 b
where a.Bill_year_month=b.YYYYMM
order by a.bills-b.bills desc;


select a.bill_year_month, a.base, b.base, a.base-b.base, a.bills, b.bills, a.bills-b.bills,a.rev,b.rev, a.rev-b.rev as x
from 
(
SELECT 
    Bill_Year_Month,
    COUNT(DISTINCT bill_header_id) AS bills,
    SUM(sale_net_val) AS rev,
    COUNT(DISTINCT Customer_Id) AS base
FROM
    CDM_Bill_Details
GROUP BY Bill_Year_Month
 ) a,
(SELECT 
    YYYYMM,
    SUM(MonthlyActiveBills) AS bills,
    SUM(MonthlyActiveRevenue) AS rev,
    SUM(MonthlyActive) AS base
FROM
    Dashboard_Insights_Base
WHERE
    AGGREGATION = 'BY_MONTH'
GROUP BY YYYYMM)
 b
where a.Bill_year_month=b.YYYYMM
order by a.bills-b.bills desc;



select a.bill_year_month, a.base, b.base, a.base-b.base, a.bills, b.bills, a.bills-b.bills,a.rev,b.rev, a.rev-b.rev as x
from 
(
SELECT 
    YYYYMM,
    SUM(MonthlyActiveBills) AS bills,
    SUM(MonthlyActiveRevenue) AS rev,
    SUM(MonthlyActive) AS base
FROM
    Dashboard_Insights_Base
WHERE
    AGGREGATION = 'BY_DAY'
GROUP BY YYYYMM
 ) a,
(
SELECT 
    YYYYMM,
    SUM(MonthlyActiveBills) AS bills,
    SUM(MonthlyActiveRevenue) AS rev,
    SUM(MonthlyActive) AS base
FROM
    Dashboard_Insights_Base
WHERE
    AGGREGATION = 'BY_MONTH'
GROUP BY YYYYMM
)
 b
where a.YYYYMM=b.YYYYMM
order by a.bills-b.bills desc;

END$$
DELIMITER ;
