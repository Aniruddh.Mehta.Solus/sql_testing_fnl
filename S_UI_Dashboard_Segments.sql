DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_UI_Dashboard_Segments`(IN in_request_json json, OUT out_result_json json)
BEGIN


    -- User Information

    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	 DECLARE IN_ACTION  TEXT;

    -- Element Details	

	declare Element_Creation text; 
	declare Element_Name text;
	declare Element_Type text;
	declare SVOC_Replaced_Query text;
	declare SVOT_Replaced_Query text;	
	DECLARE Element_ThemeId BIGINT;
	DECLARE Element_SegmentId BIGINT;
	DECLARE Element_CGPERCENTAGE BIGINT;
	DECLARE Element_RESPONSEDAYS BIGINT;
	DECLARE Element_STARTDATE TEXT;
    DECLARE Element_ENDDATE  TEXT;
	DECLARE Condition_Type  TEXT;
	DECLARE Condition_Type_Div  TEXT;
	DECLARE Conditions  TEXT;
	declare SegmentId text;
	declare InUse text;
	declare Channel_ID text;
	   
	-- Calcucaltion Vairables
	
	DECLARE vStart_Cnt BIGINT DEFAULT 0;
    DECLARE vEnd_Cnt BIGINT DEFAULT 0;
	
    DECLARE done1,done2,done3,done4 INT DEFAULT FALSE;
	
	-- LifeCycle Segment Master Details
    DECLARE LifeCycleId_cur1,Exe_ID int;
    DECLARE LifeCycleReplacedQuery_cur1 TEXT;
	
	-- Campaign Segment Master Details
	DECLARE CampaignId_cur1,Exe_ID1 int;
    DECLARE CampaignReplacedQuery_cur1 TEXT;
	DECLARE CampaignReplacedBillQuery_cur1 TEXT;
	
	-- Region Master Details
	DECLARE RegionId_cur1,Exe_ID2 int;
    DECLARE RegionReplacedQuery_cur1 TEXT;
	DECLARE RegionReplacedBillQuery_cur1 TEXT;
	
    DECLARE vSuccess int (4);                      
	DECLARE vErrMsg Text ; 

				
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER_NAME"));
	SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
	SET IN_ACTION = json_unquote(json_extract(in_request_json,"$.ACTION"));
	SET Element_Creation = json_unquote(json_extract(in_request_json,"$.Element_Creation"));
	SET Element_Name = json_unquote(json_extract(in_request_json,"$.Element_Name"));
	SET Element_Type = json_unquote(json_extract(in_request_json,"$.Element_Type"));
	SET SVOC_Replaced_Query = json_unquote(json_extract(in_request_json,"$.SVOC_Replaced_Query"));
	SET SVOT_Replaced_Query = json_unquote(json_extract(in_request_json,"$.SVOT_Replaced_Query"));
	SET Element_ThemeId = json_unquote(json_extract(in_request_json,"$.Element_ThemeId"));
	SET Element_SegmentId = json_unquote(json_extract(in_request_json,"$.Element_SegmentId"));
	SET Element_CGPERCENTAGE = json_unquote(json_extract(in_request_json,"$.Element_CGPERCENTAGE"));
	SET Element_RESPONSEDAYS = json_unquote(json_extract(in_request_json,"$.Element_RESPONSEDAYS"));
	SET Element_STARTDATE = json_unquote(json_extract(in_request_json,"$.Element_STARTDATE"));
	SET Element_ENDDATE = json_unquote(json_extract(in_request_json,"$.Element_ENDDATE"));
	SET Condition_Type = json_unquote(json_extract(in_request_json,"$.Condition_Type"));
	SET Condition_Type_Div = json_unquote(json_extract(in_request_json,"$.Condition_Type_Div"));
	SET Conditions = json_unquote(json_extract(in_request_json,"$.conditions"));
	SET SegmentId = json_unquote(json_extract(in_request_json,"$.SegmentId"));
	SET InUse = json_unquote(json_extract(in_request_json,"$.InUse"));
	SET Channel_ID = json_unquote(json_extract(in_request_json,"$.Channel_ID"));
	SET @temp_result = Null;
	
	Select Element_Creation;

	
	If Element_Creation ="DASHBOARDSEGMENT"  AND (IN_ACTION IS NULL or IN_ACTION ='')
	  THEN
		    Set @vUserId='1';
			Set	@vCLMSegmentName='';
			Set	@vCLMSegmentDesc='';
			Set	@vCLMSegmentReplacedQueryAsInUI='';
			Set @vCLMSegmentReplacedQuery='';
			Set @vIsValidCondition='';
			Set	@vCLMSegmentId='';
			Set	@vErrMsg='';
			Set	@vSuccess='';
		
			Set	@vCLMSegmentName=Element_Name;
			Set	@vCLMSegmentDesc=Element_Type;
			Set	@vCLMSegmentReplacedQueryAsInUI=SVOC_Replaced_Query;
			Set @vCLMSegmentReplacedQuery=SVOC_Replaced_Query;
			Set @vIsValidCondition='1';
			Select CLMSegmentId into @vCLMSegmentId From CLM_Segment where CLMSegmentName=Element_Name ;
			Set	@vErrMsg='';
			Set	@vSuccess='';
			Set	@LIFECYCLESEGMENT_Count='';
			
			
			Set @SVOC_UI_CONDITION_MAKER= concat(' Delete from SVOC_UI_CONDITION_MAKER 
			where CONDITION_PAGE="LIFECYCLESEGMENT" and CONDITION_NAME="',@vCLMSegmentName,'"                      
													  ;') ;
			SELECT @SVOC_UI_CONDITION_MAKER;
			PREPARE statement from @SVOC_UI_CONDITION_MAKER;
			Execute statement;
			Deallocate PREPARE statement;			
			
			If @vCLMSegmentReplacedQuery='' then Set @vCLMSegmentReplacedQuery='1=1'; End If;
			Set @Sql1=CONCAT('Select Count(1) into @LIFECYCLESEGMENT_Count from Customer_One_View where ',@vCLMSegmentReplacedQuery,';');
			SELECT @Sql1;
			PREPARE statement from @Sql1;
			Execute statement;
			Deallocate PREPARE statement; 						
							
			If @LIFECYCLESEGMENT_Count !=''
				THEN
					
					
					Select @vUserId, @vCLMSegmentName, @vCLMSegmentDesc,@vCLMSegmentReplacedQueryAsInUI, @vCLMSegmentReplacedQuery,@vIsValidCondition,@vCLMSegmentId,@vErrMsg,@vSucces;
					CALL PROC_UI_CU_CLM_Segment(@vUserId, @vCLMSegmentName, @vCLMSegmentDesc,@vCLMSegmentReplacedQueryAsInUI, @vCLMSegmentReplacedQuery,@vIsValidCondition,@vCLMSegmentId,@vErrMsg,@vSuccess);

					IF @vSuccess!=''
					   THEN
											
							Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message"," Life Cycle Segment Is Createrd") )),"]") into @temp_result                       
													  ;') ;
			   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
							
							Select CLMSegmentId into @vCLMSegmentId From CLM_Segment where CLMSegmentName=Element_Name;
							
							
							if @vCLMSegmentReplacedQuery='' then Set @vCLMSegmentReplacedQuery='1=1'; End If;
							Set @Sql1=CONCAT('Update CLM_Segment 
												Set Coverage = ',@LIFECYCLESEGMENT_Count,'
													,In_Use ="Yes"
											where CLMSegmentId=',@vCLMSegmentId,';');
													
							SELECT @Sql1;
							prepare stmt1 from @Sql1;
							execute stmt1;
							deallocate prepare stmt1;
							
									
					End if; 
					
					IF @vSuccess =''
					   THEN
											
							Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message",@vErrMsg) )),"]") into @temp_result                       
													  ;') ;
			   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
									
					End if; 
			End If;
			Delete From SVOC_UI_CONDITION_MAKER
			Where  CONDITION_PAGE=Element_Creation and CONDITION_NAME= Element_Name;
	
	END IF;
	
	
	If Element_Creation ="DASHBOARDSEGMENT" and (SegmentId is not Null or SegmentId !='') 
		THEN
				IF IN_ACTION="Yes"
					THEN
						Update Dashboard_Segments_Master
						Set IsActive="1"
						Where SegmentSequence=SegmentId;
												   
						Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Dashboard Segment Is Enabled, Please run Insights Historical metrics for Segment numbers updation") )),"]") into @temp_result                       
														  ;') ;
		   
						SELECT @Query_String;
						PREPARE statement from @Query_String;
						Execute statement;
						Deallocate PREPARE statement; 											   
				End If;	

				IF IN_ACTION="No"
					THEN
						Update Dashboard_Segments_Master
						Set IsActive="0"
						Where SegmentSequence=SegmentId;
						
												   
						Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Dashboard Segment Is Disabled, Please run Insights Historical metrics for Segment numbers updation") )),"]") into @temp_result                       
														  ;') ;
		   
						SELECT @Query_String;
						PREPARE statement from @Query_String;
						Execute statement;
						Deallocate PREPARE statement; 
						
				End If;							   				
	
    End If;	
	
	
	If Element_Creation ="DASHBOARDSEGMENT_DETAILS" and (SegmentId is not Null or SegmentId !='') 
		THEN
			
			Delete from SVOC_UI_CONDITION_MAKER 
			where CONDITION_PAGE="LIFECYCLESEGMENT" and CONDITION_NAME in (Select CLMSegmentName  FROM CLM_Segment where CLMSegmentId =SegmentId );
				
			Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Element_ID",CLMSegmentId,"Element_Name",CLMSegmentName,"SVOC_Replaced_Query",CLMSegmentReplacedQuery) )),"]") into @temp_result  
                             FROM CLM_Segment where CLMSegmentId =',SegmentId,'						
													  ;') ;
	   
								SELECT @Query_String;
								PREPARE statement from @Query_String;
								Execute statement;
								Deallocate PREPARE statement;
										   				
	
    End If;	
			
	IF Element_Creation ="DASHBOARDSEGMENT_DISPLAY"
		THEN 
			Set @View_STM= Concat('Create or replace View DASHBOARD_SEGMENT_DISPLAY AS(Select SegmentSequence,SegmentName,SegmentReplacedQuery,IsActive, 
SUBSTRING(ModifiedDate,6,11) as ModifiedDate From Dashboard_Segments_Master where IsActive = "',InUse,'");');
			
			SELECT @View_STM;
			PREPARE statement from @View_STM;
			Execute statement;
			Deallocate PREPARE statement; 
				
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object(  "Id",`SegmentSequence`,"Name",`SegmentName`,"Condition",`SegmentReplacedQuery`,"Date",ModifiedDate,"In_Use",IsActive)order by SegmentSequence asc)),"]")  into @temp_result 
											FROM DASHBOARD_SEGMENT_DISPLAY
													  ;') ;
													  
				SELECT @Query_String;
								PREPARE statement from @Query_String;
								Execute statement;
								Deallocate PREPARE statement; 
													  
	END IF;

	
	If @temp_result is null
		Then
								
							Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Invalid Parameters") )),"]") into @temp_result                       
												  ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
						
	End if;
							

    SET out_result_json = @temp_result;
	
END$$
DELIMITER ;
