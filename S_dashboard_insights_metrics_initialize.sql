DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_insights_metrics_initialize`(in vYYYYMM_StartMonth int(6))
BEGIN

DECLARE vCheckEndOfCursor int Default 0;
DECLARE vCurYYYYMM int(6) ;
DECLARE curALL_MONTHS CURSOR FOR 
SELECT DISTINCT Bill_Year_Month
FROM CDM_Bill_Details where Bill_Year_Month >= vYYYYMM_StartMonth ORDER BY Bill_Year_Month DESC;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET vCheckEndOfCursor=1;

SET SQL_SAFE_UPDATES=0;


SET vCheckEndOfCursor=0;
OPEN curALL_MONTHS;
LOOP_ALL_MONTHS : LOOP
	FETCH curALL_MONTHS into vCurYYYYMM;
	IF vCheckEndOfCursor THEN    
		LEAVE LOOP_ALL_MONTHS;
	END IF;
  	
	insert into log_solus(module, rec_start,rec_end) values ('S_dashboard_insights_metrics_initialize',-1,vCurYYYYMM);
        
		UPDATE Dashboard_Insights_Base_CURRENTRUN dashboard,
		(SELECT 
				Bill_Year_Month AS YYYYMM,
				count(DISTINCT Bill_Header_Id) as MonthlyActiveBillsNonTagged
		FROM  CDM_NM_Bill_Details WHERE Bill_Year_Month=vCurYYYYMM  group by 1 ) temp 
		SET 
			dashboard.MonthlyActiveBillsNonTagged =  ifnull(temp.MonthlyActiveBillsNonTagged,0)
		WHERE dashboard.YYYYMM = temp.YYYYMM AND AGGREGATION = 'BY_MONTH' and dashboard.YYYYMM=vCurYYYYMM;
        
END LOOP LOOP_ALL_MONTHS;
COMMIT;
END$$
DELIMITER ;
