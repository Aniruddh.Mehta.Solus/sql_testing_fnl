DELIMITER $$
CREATE or REPLACE PROCEDURE `S_Reporting_Compute_Last_Month_Metrics`()
BEGIN


DECLARE Load_Exe_Id, Exe_ID INT;


SET Load_Exe_Id = F_Get_Load_Execution_Id();


SET SQL_SAFE_UPDATES = 0;
SET FOREIGN_KEY_CHECKS = 0;


INSERT INTO ETL_Execution_Details
(Procedure_Name, Job_Name, Start_Time, End_Time, `Status`, Load_Execution_ID)
VALUES ('S_Reporting_Compute_Last_Month_Metrics','',NOW(),NULL,'Started',Load_Exe_ID);


SET Exe_ID = F_Get_Execution_Id();


SET @Startdate =  (SELECT DATE_ADD(LAST_DAY(DATE_SUB(CURDATE(), INTERVAL 2 MONTH)),INTERVAL 1 DAY));
SET @Enddate   =  (SELECT LAST_DAY(@Startdate));


CALL `S_Reporting_Top_Campaigns`(@Startdate,@Enddate);



update ETL_Execution_Details
	set Status ='Succeeded',
	End_Time = now()
	where Procedure_Name='S_Reporting_Compute_Last_Month_Metrics' and 
	Load_Execution_ID = Load_Exe_ID
	and Execution_ID = Exe_ID;


END$$
DELIMITER ;
