DELIMITER $$
CREATE or replace PROCEDURE `S_dashboard_insights_customer_distribution_metrics`()
BEGIN
DECLARE vCheckEndOfCursor int Default 0;
DECLARE vcur_Banding int default 0;
DECLARE vAttribute varchar(256);
DECLARE vlower_limit int default 0;
DECLARE vupper_limit int default 0;

DECLARE cur_Banding CURSOR FOR SELECT DISTINCT
    Attribute, lower_limit, upper_limit
FROM
    Dashboard_Insights_Customer_Distribution
ORDER BY Attribute , lower_limit ASC;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET vCheckEndOfCursor=1;
SET SQL_SAFE_UPDATES=0;
DELETE from log_solus where module='S_dashboard_insights_customer_distribution_metrics';

INSERT INTO Dashboard_Metrics_Log
(
`Screen_Name`,
`Status`
)
VALUES
('INSIGHTS - CUSTOMER DISTRIBUTION',
'Started');

OPEN cur_Banding;
LOOP_cur_Banding : LOOP

	FETCH cur_Banding INTO vAttribute, vlower_limit, vupper_limit ;
	IF vCheckEndOfCursor THEN    
		LEAVE LOOP_cur_Banding;
	END IF;
    IF vupper_limit <> '-1' THEN
		SET @sqlstring=concat('select count(1) into @base from Customer_One_View_Genome_Original where ',vAttribute,' between ',vlower_limit,'  and ',vupper_limit,'');
	ELSE
    	SET @sqlstring=concat('select count(1) into @base from Customer_One_View_Genome_Original where ',vAttribute,' >= ',vlower_limit,'');
    END IF;
    
    PREPARE statement from @sqlstring;
	EXECUTE statement;
	DEALLOCATE PREPARE statement;
    
  --  select @sqlstring;
  --  select vAttribute, vlower_limit, vupper_limit, @base;
	UPDATE Dashboard_Insights_Customer_Distribution
	SET base=@base
	WHERE Attribute=vAttribute
    and lower_limit=vlower_limit;
    
END LOOP LOOP_cur_Banding;
UPDATE Dashboard_Insights_Customer_Distribution dashboard,
    (SELECT 
        Attribute, SUM(base) AS totalbase
    FROM
        Dashboard_Insights_Customer_Distribution
    GROUP BY Attribute) t 
SET 
    dashboard.Percent_Of_Customers = dashboard.Base / t.totalbase
WHERE
    dashboard.Attribute = t.Attribute;

	insert into log_solus(module,rec_start, rec_end,msg) values ('S_dashboard_insights_customer_distribution_metrics',ifnull(@x,0), ifnull(@y,0),'NTN1 Done');
    
    UPDATE Dashboard_Metrics_Log
SET `Status` = 'Ended' WHERE Screen_Name = 'INSIGHTS - CUSTOMER DISTRIBUTION';

END$$
DELIMITER ;
