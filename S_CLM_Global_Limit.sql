
CREATE OR REPLACE PROCEDURE `S_CLM_Global_Limit`()
begin

    DECLARE event_limit int;
    DECLARE Overall_Limit int;
    DECLARE vStart_Cnt BIGINT DEFAULT 0;
    DECLARE vEnd_Cnt BIGINT;
    DECLARE Load_Exe_ID, Exe_ID int;
    DECLARE E_ID,E_limit bigint;
    DECLARE done INT DEFAULT False; 

set sql_safe_updates=0;
set Load_Exe_ID = F_Get_Load_Execution_Id();
insert into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
values ('S_Global_Limit', '',now(),null,'Started', Load_Exe_ID);
select concat ('S_CLM_Global_Limit : STARTED', now(), Load_Exe_ID ) as '';
Drop table if exists Event_Execution_bkp;
rename table Event_Execution to Event_Execution_bkp;
drop table if exists Event_Execution_limit;
CREATE TABLE Event_Execution_limit LIKE Event_Execution_bkp;
SELECT 
    `Value`
INTO @Overall_Limit FROM
    M_Config
WHERE
    `Name` = 'CLM_Event_Limit';
if ifnull(@Overall_Limit,-1)>0
then
 set @sql2=concat("insert ignore into Event_Execution_limit (select * from Event_Execution_bkp where Is_Target='Y'  ORDER BY RAND() limit ",@Overall_Limit,")");
select @sql2;
		prepare stmt2 from @sql2;
		execute stmt2;
		deallocate prepare stmt2;
rename table Event_Execution_limit to Event_Execution;
else
rename table Event_Execution_bkp to Event_Execution;
end if;

-- <LOG>
	UPDATE ETL_Execution_Details 
SET 
    Status = 'Succeeded',
    End_Time = NOW()
WHERE
    Procedure_Name = 'S_Global_Limit'
        AND Execution_ID = (SELECT 
            MAX(Execution_ID)
        FROM
            ETL_Execution_Details);
select concat ('S_CLM_Global_Limit : COMPLETED', now(), Load_Exe_ID ) as '';

END

