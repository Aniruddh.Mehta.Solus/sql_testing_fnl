DELIMITER $$
CREATE or REPLACE PROCEDURE `S_Reporting_Compute_Historic_Metrics`()
BEGIN


DECLARE Load_Exe_Id, Exe_ID INT;


SET Load_Exe_Id = F_Get_Load_Execution_Id();
SET @Activestorelovid = (SELECT LOV_Id FROM CDM_LOV_Master WHERE LOV_Attribute = 'SOLUS_SEED' AND LOV_Key = 'ACTIVESTORE' LIMIT 1);

SET SQL_SAFE_UPDATES = 0;
SET FOREIGN_KEY_CHECKS = 0;


INSERT INTO ETL_Execution_Details
(Procedure_Name, Job_Name, Start_Time, End_Time, `Status`, Load_Execution_ID)
VALUES ('S_Reporting_Compute_Historic_Metrics','',NOW(),NULL,'Started',Load_Exe_ID);


SET Exe_ID = F_Get_Execution_Id();


SET @Startdate =  (SELECT DATE_ADD(LAST_DAY(DATE_SUB(CURDATE(), INTERVAL 7 MONTH)),INTERVAL 1 DAY));
SET @End_Date  =  (SELECT LAST_DAY(DATE_ADD(@Startdate, INTERVAL 5 MONTH)));
SET @Enddate   =  (SELECT LAST_DAY(@Startdate));


WHILE @Enddate <= @End_Date DO


CALL `S_Reporting_Top_Campaigns`(@Startdate,@Enddate);


SET @Startdate =  (SELECT DATE_ADD(@Startdate, INTERVAL 1 MONTH));
SET @Enddate   =  Last_Day(@Startdate);


END WHILE;



CALL `CDM_Calc_Hist_Monthly_CRM_KPIs`(1,@Activestorelovid);



update ETL_Execution_Details
	set Status ='Succeeded',
	End_Time = now()
	where Procedure_Name='S_Reporting_Compute_Historic_Metrics' and 
	Load_Execution_ID = Load_Exe_ID
	and Execution_ID = Exe_ID;


END$$
DELIMITER ;
