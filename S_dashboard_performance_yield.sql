DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_performance_yield`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    
    
    
    SET @Interval_month = CASE WHEN IN_AGGREGATION_2='M' THEN 0
							WHEN IN_AGGREGATION_2='3M' THEN 2
                            ELSE 11 END;
    
    
	SET @FY_SATRT_DATE = date_format(date_sub(concat(IN_FILTER_CURRENTMONTH,'01'), INTERVAL @Interval_month MONTH),"%Y%m");
	SET @FY_END_DATE = IN_FILTER_CURRENTMONTH;
    
    SELECT @FY_SATRT_DATE,@FY_END_DATE;
    SET @incr_Rev =' round(SUM(Incremental_Revenue),0) ';
    SET @filter_cond=concat(" YM between '",@FY_SATRT_DATE,"' and '",@FY_END_DATE,"'");
    
      select Value1 into @Incremental_Revenue_Method from UI_Configuration_Global where Config_Name = 'Incremental_Revenue_Method';
    SET @Incremental_Revenue_Method=ifnull(@Incremental_Revenue_Method,'Method2');
    select @Incremental_Revenue_Method;
    IF @Incremental_Revenue_Method = 'Method1'
    THEN
		SET @incr_rev_concat='_By_Day';
        SET @group_cond1='Event_Id,EE_Date';
        SET @mtd_concat='';
	ELSEIF @Incremental_Revenue_Method = 'Method2'
    THEN
		SET @group_cond1='Event_Id,YM';
		SET @incr_rev_concat='';
        SET @mtd_concat='MTD_';
	ELSE
		SET @incr_rev_concat='';
        SET @mtd_concat='MTD_';
		SET @incr_Rev = ' round((CASE WHEN SUM(Control_Base) < 0 or SUM(Target_Delivered) = 0 then 0 WHEN (((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*100 < 0 then 0 WHEN SUM(Control_Base)>0 and SUM(Control_Responder)=0 then SUM(Target_Revenue)
					ELSE ifnull((((SUM(Target_Responder)/SUM(Target_Delivered))-(SUM(Control_Responder)/SUM(Control_Base))))*(SUM(Target_Revenue)/SUM(Target_Responder))*(SUM(Target_Delivered)),0) END),0) ';
		SET @group_cond1='YM';
	END IF;
    SELECT @mtd_concat;
   
   SET @sql1=CONCAT('
		CREATE OR REPLACE VIEW CLM_Event_Dashboard_STLT
        as 
		SELECT `CLM_Event_Dashboard`.`Revenue Center`,
			`CLM_Event_Dashboard`.`Segment`,
			`CLM_Event_Dashboard`.`Campaign`,
			`CLM_Event_Dashboard`.`EE_Date`,
			`CLM_Event_Dashboard`.`Event_Execution_Date_ID`,
			`CLM_Event_Dashboard`.`Theme`,
			`CLM_Event_Dashboard`.`Offer`,
			`CLM_Event_Dashboard`.`Channel`,
			`CLM_Event_Dashboard`.`Has_Recommendations`,
			`CLM_Event_Dashboard`.`Event_ID`,
			`CLM_Event_Dashboard`.`Control_Base` as `Control_Base`,
			`CLM_Event_Dashboard`.`Target_Base`,
			`CLM_Event_Dashboard`.`Target_Delivered`,
			`CLM_Event_Dashboard`.`Control_Responder` as `Control_Responder`,
			`CLM_Event_Dashboard`.`Target_Responder`,
			`CLM_Event_Dashboard`.`Target_Revenue`,
			`CLM_Event_Dashboard`.`Target_Discount`,
			`CLM_Event_Dashboard`.`Target_Bills`,
			`CLM_Event_Dashboard`.`Target_Open`,
			`CLM_Event_Dashboard`.`Target_CTA`,
			`CLM_Event_Dashboard`.`Control_Revenue`,
			`CLM_Event_Dashboard`.`Control_Discount`,
			`CLM_Event_Dashboard`.`Control_Bills`,
			`CLM_Event_Dashboard`.`Created_Date`,
			`CLM_Event_Dashboard`.`Total_Customer_Base`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Month_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Campaign`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Theme`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Segment`,
			`CLM_Event_Dashboard`.`Total_Distinct_Customers_Targeted_This_Day_This_Trigger`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Day`,
			`CLM_Event_Dashboard`.`Total_Revenue_This_Month`,
			`CLM_Event_Dashboard`.`YM`,
			`CLM_Event_Dashboard`.`Incremental_Revenue',@incr_rev_concat,'` AS `Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue',@incr_rev_concat,'` AS `MTD_Incremental_Revenue`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Segment',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Segment`,
			`CLM_Event_Dashboard`.`',@mtd_concat,'Incremental_Revenue_By_Revenue_Center',@incr_rev_concat,'` AS `MTD_Incremental_Revenue_By_Revenue_Center`
		FROM `CLM_Event_Dashboard`;
        ');
        select @sql1;
		PREPARE statement from @sql1;
		Execute statement;
		Deallocate PREPARE statement;
 
 
  
   IF IN_MEASURE = 'Segment'
    THEN
		
        SET @create_view=CONCAT('CREATE OR REPLACE VIEW V_Dashboard_MTD_Incr_Rev
			as
			select CASE WHEN datediff(End_Date_ID,Start_Date_ID) < 32 THEN "GTM" else "CLM" END AS `Category`,
            MAX(EE_Date) as EE_Date,MAX(MTD_Incremental_Revenue_By_Segment) as Incremental_Revenue,SUM(Target_Revenue) as Target_Revenue,MAX(Total_Revenue_This_Month) as Total_Revenue_This_Month,SUM(Target_Delivered) as Target_Delivered,SUM(Target_Bills) as Target_Bills,SUM(Control_Bills) as Control_Bills,MAX(Total_Customer_Base) AS Total_Customer_Base,
            SUM(Control_Base) as Control_Base,
            SUM(Target_Responder) AS Target_Responder,
            SUM(Control_Responder) AS Control_Responder,
            MAX(Total_Distinct_Customers_Targeted_This_Month) as Total_Distinct_Customers_Targeted_This_Month,
            MAX(Total_Distinct_Customers_Targeted_This_Month_This_Theme) AS Total_Distinct_Customers_Targeted_This_Month_This_Theme,
            MAX(Total_Distinct_Customers_Targeted_This_Month_This_Segment) AS Total_Distinct_Customers_Targeted_This_Month_This_Segment,
            c.Number_Of_Recommendation as Number_Of_Recommendation,
            (length(Creative1)-length(replace(Creative1,"|","")))+1 AS Pesonalization_Score,
            Event_ID,Channel,Theme,Segment,YM from CLM_Event_Dashboard_STLT a left join Event_Master b on a.Event_ID = b.ID
            LEFT JOIN Communication_Template c on b.Communication_Template_ID=c.ID
            group by ',@group_cond1,',Segment');
	ELSE
		SET @create_view=CONCAT('CREATE OR REPLACE VIEW V_Dashboard_MTD_Incr_Rev
			as
			select CASE WHEN datediff(End_Date_ID,Start_Date_ID) < 32 THEN "GTM" else "CLM" END AS `Category`,
            MAX(EE_Date) as EE_Date,MAX(MTD_Incremental_Revenue) as Incremental_Revenue,SUM(Target_Revenue) as Target_Revenue,MAX(Total_Revenue_This_Month) as Total_Revenue_This_Month,SUM(Target_Delivered) as Target_Delivered,SUM(Target_Bills) as Target_Bills,SUM(Control_Bills) as Control_Bills,MAX(Total_Customer_Base) AS Total_Customer_Base,
            SUM(Control_Base) as Control_Base,
            SUM(Target_Responder) AS Target_Responder,
            SUM(Control_Responder) AS Control_Responder,
            MAX(Total_Distinct_Customers_Targeted_This_Month) as Total_Distinct_Customers_Targeted_This_Month,
            MAX(Total_Distinct_Customers_Targeted_This_Month_This_Theme) AS Total_Distinct_Customers_Targeted_This_Month_This_Theme,
            MAX(Total_Distinct_Customers_Targeted_This_Month_This_Segment) AS Total_Distinct_Customers_Targeted_This_Month_This_Segment,
            c.Number_Of_Recommendation as Number_Of_Recommendation,
            (length(Creative1)-length(replace(Creative1,"|","")))+1 AS Pesonalization_Score,
            Event_ID,Channel,Theme,YM from CLM_Event_Dashboard_STLT a left join Event_Master b on a.Event_ID = b.ID
            LEFT JOIN Communication_Template c on b.Communication_Template_ID=c.ID
            group by ',@group_cond1,'');
    END IF;
    
     select @create_view;
	PREPARE statement from @create_view;
	Execute statement;
	Deallocate PREPARE statement;
    
            
    
    IF IN_AGGREGATION_1 = 'YIELD' 
		THEN 
			SET @Query_Measure=concat(' (CASE WHEN SUM(Target_Delivered) < 1 THEN 0 ELSE round((',@incr_Rev,'/SUM(Target_Delivered)),1) END) ');
	ELSEIF IN_AGGREGATION_1 = 'COVERAGE' 
		THEN 
			
            SET @Query_Measure=concat('ifnull(round((SUM(Target_Delivered)/(select SUM(Target_Delivered) from V_Dashboard_MTD_Incr_Rev where YM between ',@FY_SATRT_DATE,' and ',@FY_END_DATE,' ))*100,1),0)');
	END IF;

    
    IF IN_MEASURE = "TYPE"
	THEN
		SET @group_measure='Category';
        SET @display_mesure=@group_measure;
	END IF;
    IF IN_MEASURE = "THEME"
	THEN
		SET @group_measure='Theme';
        SET @display_mesure='replace(Theme,"Customer_","")';
	END IF;
	IF IN_MEASURE = "CHANNEL" OR IN_MEASURE = "CARDS" 
	THEN
		SET @group_measure='Channel';
        SET @display_mesure=@group_measure;
	END IF;
    IF IN_MEASURE = "SEGMENT"
	THEN
		SET @group_measure='Segment';
        SET @display_mesure=@group_measure;
	END IF;
    IF IN_MEASURE = "PERSONALIZATION" 
	THEN
		SET @group_measure='CASE WHEN Pesonalization_Score > 3 then ">3" ELSE Pesonalization_Score END';
        SET @display_mesure=@group_measure;
	END IF;
    IF IN_MEASURE = "RECOUSAGE"
	THEN
		SET @group_measure='CASE WHEN Number_Of_Recommendation<=0 THEN "No Reco" ELSE "Use Reco" END';
        SET @display_mesure=@group_measure;
	END IF;
        

		select @group_measure,@Query_Measure,@filter_cond;
		SET @view_stmt=concat('CREATE OR REPLACE VIEW V_Dashboard_Yield_Graph AS
						SELECT ',@display_mesure,' AS X_Data,
                         ',@Query_Measure,' as "Value"
                         from V_Dashboard_MTD_Incr_Rev ced
                         WHERE ',@filter_cond,'
                         GROUP BY ',@group_measure,'
                         Having `Value`>0
                        ');
		SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`X_Data`,"Value",`Value`) )),"]")into @temp_result from V_Dashboard_Yield_Graph;' ;
	IF IN_MEASURE <> "DOWNLOAD"
    THEN
	select @view_stmt;
	PREPARE statement from @view_stmt;
	Execute statement;
	Deallocate PREPARE statement;
    
	select @Query_String;
	PREPARE statement from @Query_String;
	Execute statement;
	Deallocate PREPARE statement; 
    SET out_result_json = @temp_result;
    END IF;
    
END$$
DELIMITER ;
