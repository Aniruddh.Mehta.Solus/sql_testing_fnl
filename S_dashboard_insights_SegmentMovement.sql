DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_insights_SegmentMovement`(IN in_request_json text, OUT out_result_json text)
BEGIN


DECLARE vThisPeriodSegment varchar(64);
DECLARE vLastPeriodSegment varchar(64);
DECLARE vBase int(10);
DECLARE vPercent varchar(16);
DECLARE vRevChange int(10);
DECLARE vCheckEndOfCursor int Default 0;

DECLARE cur_SM CURSOR FOR 
SELECT  
	 a.LastPeriodSegment,a.ThisPeriodSegment, ifnull(Base,0), ifnull(Percent,0), ifnull(RevChange,0)
FROM
    Dashboard_Insights_SegmentMovement_Summary a LEFT join CDM_Customer_Segment_Sequence b on a.LastPeriodSegment=b.CLMSegmentName 
    LEFT JOIN CDM_Customer_Segment_Sequence c on a.ThisPeriodSegment=c.CLMSegmentName 
WHERE AGGREGATION=@AGGREGATION
ORDER BY AGGREGATION,b.sequence,c.sequence ASC; 



DECLARE CONTINUE HANDLER FOR NOT FOUND SET vCheckEndOfCursor=1;

SET @IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
SET @IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
SELECT @IN_MEASURE, @IN_NUMBER_FORMAT;

IF @IN_MEASURE = 'TQ_LQ' THEN
	SET @AGGREGATION='TQLQ';
ELSEIF @IN_MEASURE = 'TY_LY' THEN
	SET @AGGREGATION='TYLY';
ELSEIF @IN_MEASURE = 'TM_LM' THEN
	SET @AGGREGATION='TMLM';
END IF;

SET @iFirstTime=0;
SET @jsonStr = ' ';
SET @vPrevThisPeriodSegment= ' ';

set sql_safe_updates = 0;
delete from CDM_Customer_Segment_Sequence where CLMSegmentName is null or CLMSegmentName = "";
select group_concat(distinct(summ.LastPeriodSegment)) from Dashboard_Insights_SegmentMovement_Summary summ where summ.LastPeriodSegment not in (select CLMSegmentName from CDM_Customer_Segment_Sequence) into @missing_segments;
select @missing_segments;
select (length(@missing_segments)-length(replace(@missing_segments,',','')))+1 into @num_of_channel;
select @num_of_channel;
select count(CLMSegmentName) from CDM_Customer_Segment_Sequence into @seq;
select @seq;
set @seq=@seq+2;
set @loop_var = 1;
while @loop_var <= @num_of_channel
do
SET @selected_channel=substring_index(substring_index(concat(@missing_segments,',-1'),',',@loop_var),',',-1);
select @selected_channel;
set @exe_stmt = concat('insert into CDM_Customer_Segment_Sequence(CLMSegmentName,sequence) values ("',@selected_channel,'", ',@seq,');');
select @exe_stmt;
PREPARE statement from @exe_stmt;
Execute statement;
Deallocate PREPARE statement; 
set @seq=@seq+1;
set @loop_var=@loop_var+1;
end while;

OPEN cur_SM;
LOOP_cur : LOOP
	FETCH cur_SM INTO vThisPeriodSegment, vLastPeriodSegment, vBase, vPercent, vRevChange ;
	
    
	IF vCheckEndOfCursor THEN    
		LEAVE LOOP_cur;
	END IF;
    
	IF @IN_NUMBER_FORMAT='NUMBER' THEN
		SET @vMetrics=vBase;
	ELSE
		SET  @vMetrics=CONCAT('"',vPercent,'%"');
	END IF;
	IF @IN_NUMBER_FORMAT='REV' THEN
		SET @vMetrics = vRevChange;
	END IF;
    
    IF vThisPeriodSegment != @vPrevThisPeriodSegment THEN
		IF @iFirstTime = 0 THEN
			SET @iFirstTime=1;
        ELSE 
			SET @jsonStr = CONCAT(@jsonStr,'},');
		END IF;
		SET @jsonStr=CONCAT( @jsonStr , '{" ": "',vThisPeriodSegment,'"' );
        SET @vPrevThisPeriodSegment = vThisPeriodSegment;
    END IF;
	SET @jsonStr=CONCAT(@jsonStr,',"',vLastPeriodSegment,'" : ',@vMetrics);
END LOOP LOOP_cur;

SET @jsonStr=CONCAT('[ ',@jsonStr,'} ]') ;
CLOSE cur_SM;

SET out_result_json=@jsonStr;
SELECT out_result_json;

END$$
DELIMITER ;
