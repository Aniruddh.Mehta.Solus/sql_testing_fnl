DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_campaign_deep_dive_metrics`()
BEGIN
DECLARE Cursor_Check_Var Int Default 0;
DECLARE cur_Execution_Date_ID INT(11);
DECLARE cur_Event_Id INT(11);


DECLARE curALL_EVENTS CURSOR For SELECT DISTINCT
    Event_Id
FROM
    CLM_Event_Dashboard A
    WHERE Event_Id <> 0
	ORDER BY Event_Id ASC;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET Cursor_Check_Var=1;
SET sql_safe_updates=0;

SET Cursor_Check_Var = FALSE;
OPEN curALL_EVENTS;
LOOP_ALL_DATES: LOOP

	FETCH curALL_EVENTS into cur_Event_Id;
	IF Cursor_Check_Var THEN
	   LEAVE LOOP_ALL_DATES;
	END IF;

		Select cur_Event_Id,now();
		
CREATE OR REPLACE VIEW V_Dashboard_Deep_Dive_Incr_Rev
as
select MAX(EE_Date) as EE_Date,MAX(MTD_Incremental_Revenue) as Incremental_Revenue,SUM(Target_Revenue) as Target_Revenue,MAX(Total_Revenue_This_Month) as Total_Revenue_This_Month,SUM(Target_Delivered) as Target_Delivered,SUM(Target_Bills) as Target_Bills,SUM(Control_Bills) as Control_Bills,Event_ID,Campaign,Channel,YM from CLM_Event_Dashboard_STLT group by Campaign, Event_Id,EE_Date;
		
-- Loading ALL data

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'ALL' AS Measure,
'TG_Res' AS Aggregation,
'Y' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
(
  with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") as target FROM  CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI)
AND  year(EE_Date) = year(now()) group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display)  ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI) and   year(EE_Date) = year(now())  ),0) as bt from cte2 ) as d2;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'ALL' AS Measure,
'TG_CG' AS Aggregation,
'Y' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
(
  with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(CASE WHEN (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))<=0 THEN 0 ELSE (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))END ,"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI)
AND  year(EE_Date) = year(now()) group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display)   ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(CASE WHEN (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))<=0 THEN 0 ELSE (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))END ,"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI) AND   year(EE_Date) = year(now())  ),0) as bt from cte2 ) as d2;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'ALL' AS Measure,
'INCR_TG' AS Aggregation,
'Y' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
( with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(sum(Incremental_Revenue),0) / ifnull(SUM(Target_Revenue),0)*100,0),2),"%") as target  FROM V_Dashboard_Deep_Dive_Incr_Rev where  Campaign in (select Campaign from Campaign_Deep_Dive_Display)  and Event_ID in (select event_id from Event_Master_UI ) AND   year(EE_Date) = year(now())
 group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display)  ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((SELECt  concat(round(ifnull(ifnull(sum(Incremental_Revenue),0) / ifnull(SUM(Target_Revenue),0)*100,0),2),"%") as target  FROM V_Dashboard_Deep_Dive_Incr_Rev where  Campaign in (select Campaign from Campaign_Deep_Dive_Display)  and Event_ID in (select event_id from Event_Master_UI )  AND   year(EE_Date) = year(now()) ),0)as bt from cte2 ) as d3;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'ALL' AS Measure,
'CTR' AS Aggregation,
'Y' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
(with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(SUM(Target_CTA),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI)
AND  year(EE_Date) = year(now()) group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 WHERE Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(Target_CTA),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI) AND   year(EE_Date) = year(now())  ),0) as bt from cte2  ) as d4;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'ALL' AS Measure,
'Click_Conv' AS Aggregation,
'Y' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
( with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(SUM(CTA_Responder),0) / ifnull(SUM(Target_CTA),0)*100,0),2),"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI)
AND  year(EE_Date) = year(now()) group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 WHERE Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(CTA_Responder),0) / ifnull(SUM(Target_CTA),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI)  AND   year(EE_Date) = year(now())  ),0) as bt from cte2 ) as d5;

-- Loading Q data


INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'ALL' AS Measure,
'TG_Res' AS Aggregation,
'Q' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
(
  with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") as target FROM  CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI)
AND  quarter(EE_Date) = quarter(now())  AND   year(EE_Date) = year(now()) group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display)  ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI) and   quarter(EE_Date) = quarter(now())  AND   year(EE_Date) = year(now())  ),0) as bt from cte2 ) as d2;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'ALL' AS Measure,
'TG_CG' AS Aggregation,
'Q' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
(
  with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(CASE WHEN (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))<=0 THEN 0 ELSE (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))END ,"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI)
AND  quarter(EE_Date) = quarter(now())  AND   year(EE_Date) = year(now()) group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display)   ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(CASE WHEN (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))<=0 THEN 0 ELSE (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))END ,"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI) AND   quarter(EE_Date) = quarter(now())  AND   year(EE_Date) = year(now())  ),0) as bt from cte2 ) as d2;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'ALL' AS Measure,
'INCR_TG' AS Aggregation,
'Q' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
( with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(sum(Incremental_Revenue),0) / ifnull(SUM(Target_Revenue),0)*100,0),2),"%") as target  FROM V_Dashboard_Deep_Dive_Incr_Rev where  Campaign in (select Campaign from Campaign_Deep_Dive_Display)  and Event_ID in (select event_id from Event_Master_UI ) AND  quarter(EE_Date) = quarter(now())  AND   year(EE_Date) = year(now())
 group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display)  ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((SELECt  concat(round(ifnull(ifnull(sum(Incremental_Revenue),0) / ifnull(SUM(Target_Revenue),0)*100,0),2),"%") as target  FROM V_Dashboard_Deep_Dive_Incr_Rev where  Campaign in (select Campaign from Campaign_Deep_Dive_Display)  and Event_ID in (select event_id from Event_Master_UI )  AND  quarter(EE_Date) = quarter(now())  AND   year(EE_Date) = year(now()) ),0)as bt from cte2 ) as d3;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'ALL' AS Measure,
'CTR' AS Aggregation,
'Q' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
(with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(SUM(Target_CTA),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI)
AND  quarter(EE_Date) = quarter(now())  AND   year(EE_Date) = year(now()) group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 WHERE Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(Target_CTA),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI) AND   quarter(EE_Date) = quarter(now())  AND   year(EE_Date) = year(now())  ),0) as bt from cte2  ) as d4;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'ALL' AS Measure,
'Click_Conv' AS Aggregation,
'Q' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
( with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(SUM(CTA_Responder),0) / ifnull(SUM(Target_CTA),0)*100,0),2),"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI)
AND  quarter(EE_Date) = quarter(now())  AND   year(EE_Date) = year(now()) group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 WHERE Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(CTA_Responder),0) / ifnull(SUM(Target_CTA),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI)  AND   quarter(EE_Date) = quarter(now())  AND   year(EE_Date) = year(now())  ),0) as bt from cte2 ) as d5;

-- Loading M Data

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'ALL' AS Measure,
'TG_Res' AS Aggregation,
'M' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
(
  with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") as target FROM  CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI)
AND  EE_Date between DATE_SUB( now(), INTERVAL 30 day) and now() group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display)  ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI) and   EE_Date between DATE_SUB( now(), INTERVAL 30 day) and now()  ),0) as bt from cte2 ) as d2;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'ALL' AS Measure,
'TG_CG' AS Aggregation,
'M' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
(
  with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(CASE WHEN (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))<=0 THEN 0 ELSE (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))END ,"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI)
AND  EE_Date between DATE_SUB( now(), INTERVAL 30 day) and now() group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display)   ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(CASE WHEN (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))<=0 THEN 0 ELSE (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))END ,"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI) AND   EE_Date between DATE_SUB( now(), INTERVAL 30 day) and now()  ),0) as bt from cte2 ) as d2;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'ALL' AS Measure,
'INCR_TG' AS Aggregation,
'M' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
( with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(sum(Incremental_Revenue),0) / ifnull(SUM(Target_Revenue),0)*100,0),2),"%") as target  FROM V_Dashboard_Deep_Dive_Incr_Rev where  Campaign in (select Campaign from Campaign_Deep_Dive_Display)  and Event_ID in (select event_id from Event_Master_UI ) AND   EE_Date between DATE_SUB( now(), INTERVAL 30 day) and now()
 group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display)  ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((SELECt  concat(round(ifnull(ifnull(sum(Incremental_Revenue),0) / ifnull(SUM(Target_Revenue),0)*100,0),2),"%") as target  FROM V_Dashboard_Deep_Dive_Incr_Rev where  Campaign in (select Campaign from Campaign_Deep_Dive_Display)  and Event_ID in (select event_id from Event_Master_UI )  AND   EE_Date between DATE_SUB( now(), INTERVAL 30 day) and now() ),0)as bt from cte2 ) as d3;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'ALL' AS Measure,
'CTR' AS Aggregation,
'M' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
(with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(SUM(Target_CTA),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI)
AND  EE_Date between DATE_SUB( now(), INTERVAL 30 day) and now() group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 WHERE Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(Target_CTA),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI) AND  EE_Date between DATE_SUB( now(), INTERVAL 30 day) and now()  ),0) as bt from cte2  ) as d4;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'ALL' AS Measure,
'Click_Conv' AS Aggregation,
'M' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
( with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(SUM(CTA_Responder),0) / ifnull(SUM(Target_CTA),0)*100,0),2),"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI)
AND  EE_Date between DATE_SUB( now(), INTERVAL 30 day) and now() group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 WHERE Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(CTA_Responder),0) / ifnull(SUM(Target_CTA),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI)  AND   EE_Date between DATE_SUB( now(), INTERVAL 30 day) and now()  ),0) as bt from cte2 ) as d5;

-- Loading W Data

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'ALL' AS Measure,
'TG_Res' AS Aggregation,
'W' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
(
  with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") as target FROM  CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI)
AND  EE_Date between DATE_SUB( now(), INTERVAL 7 day) and now() group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display)  ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI) and   EE_Date between DATE_SUB( now(), INTERVAL 7 day) and now()  ),0) as bt from cte2 ) as d2;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'ALL' AS Measure,
'TG_CG' AS Aggregation,
'W' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
(
  with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(CASE WHEN (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))<=0 THEN 0 ELSE (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))END ,"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI)
AND  EE_Date between DATE_SUB( now(), INTERVAL 7 day) and now() group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display)   ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(CASE WHEN (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))<=0 THEN 0 ELSE (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))END ,"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI) AND   EE_Date between DATE_SUB( now(), INTERVAL 7 day) and now()  ),0) as bt from cte2 ) as d2;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'ALL' AS Measure,
'INCR_TG' AS Aggregation,
'W' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
( with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(sum(Incremental_Revenue),0) / ifnull(SUM(Target_Revenue),0)*100,0),2),"%") as target  FROM V_Dashboard_Deep_Dive_Incr_Rev where  Campaign in (select Campaign from Campaign_Deep_Dive_Display)  and Event_ID in (select event_id from Event_Master_UI ) AND   EE_Date between DATE_SUB( now(), INTERVAL 7 day) and now()
 group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display)  ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((SELECt  concat(round(ifnull(ifnull(sum(Incremental_Revenue),0) / ifnull(SUM(Target_Revenue),0)*100,0),2),"%") as target  FROM V_Dashboard_Deep_Dive_Incr_Rev where  Campaign in (select Campaign from Campaign_Deep_Dive_Display)  and Event_ID in (select event_id from Event_Master_UI )  AND   EE_Date between DATE_SUB( now(), INTERVAL 7 day) and now() ),0)as bt from cte2 ) as d3;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'ALL' AS Measure,
'CTR' AS Aggregation,
'W' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
(with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(SUM(Target_CTA),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI)
AND  EE_Date between DATE_SUB( now(), INTERVAL 7 day) and now() group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 WHERE Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(Target_CTA),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI) AND  EE_Date between DATE_SUB( now(), INTERVAL 7 day) and now()  ),0) as bt from cte2  ) as d4;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'ALL' AS Measure,
'Click_Conv' AS Aggregation,
'W' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
( with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(SUM(CTA_Responder),0) / ifnull(SUM(Target_CTA),0)*100,0),2),"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI)
AND  EE_Date between DATE_SUB( now(), INTERVAL 7 day) and now() group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 WHERE Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(CTA_Responder),0) / ifnull(SUM(Target_CTA),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI)  AND   EE_Date between DATE_SUB( now(), INTERVAL 7 day) and now()  ),0) as bt from cte2 ) as d5;

-- Loading GTM data

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'GTM' AS Measure,
'TG_Res' AS Aggregation,
'Y' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
(
  with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") as target FROM 
CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM")
AND year(EE_Date) = year(now()) group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM") ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM") AND  year(EE_Date) = year(now())  ),0) as bt from cte2 ) as d2;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'GTM' AS Measure,
'TG_CG' AS Aggregation,
'Y' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
(
  with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(CASE WHEN (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))<=0 THEN 0 ELSE (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))END ,"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM")
AND year(EE_Date) = year(now()) group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM") ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(CASE WHEN (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))<=0 THEN 0 ELSE (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))END ,"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM") AND  year(EE_Date) = year(now())  ),0) as bt from cte2 ) as d2;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'GTM' AS Measure,
'INCR_TG' AS Aggregation,
'Y' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
( with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(sum(Incremental_Revenue),0) / ifnull(SUM(Target_Revenue),0)*100,0),2),"%") as target  FROM V_Dashboard_Deep_Dive_Incr_Rev where  Campaign in (select Campaign from Campaign_Deep_Dive_Display)  and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM" ) AND  year(EE_Date) = year(now())
 group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM")  ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((SELECt  concat(round(ifnull(ifnull(sum(Incremental_Revenue),0) / ifnull(SUM(Target_Revenue),0)*100,0),2),"%") as target  FROM V_Dashboard_Deep_Dive_Incr_Rev where  Campaign in (select Campaign from Campaign_Deep_Dive_Display)  and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM" )  AND  year(EE_Date) = year(now())  ),0)as bt from cte2 ) as d3;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'GTM' AS Measure,
'CTR' AS Aggregation,
'Y' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
(with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(SUM(Target_CTA),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM")
AND year(EE_Date) = year(now()) group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM") ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(Target_CTA),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM") AND  year(EE_Date) = year(now())  ),0) as bt from cte2  ) as d4;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'GTM' AS Measure,
'Click_Conv' AS Aggregation,
'Y' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
( with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(SUM(CTA_Responder),0) / ifnull(SUM(Target_CTA),0)*100,0),2),"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM")
AND year(EE_Date) = year(now()) group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM")) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(CTA_Responder),0) / ifnull(SUM(Target_CTA),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM")  AND  year(EE_Date) = year(now())  ),0) as bt from cte2 ) as d5;

-- Loading Q data

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'GTM' AS Measure,
'TG_Res' AS Aggregation,
'Q' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
(
  with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") as target FROM 
CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM")
AND quarter(EE_Date) = quarter(now())  AND   year(EE_Date) = year(now()) group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM") ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM") AND  quarter(EE_Date) = quarter(now())  AND   year(EE_Date) = year(now())  ),0) as bt from cte2 ) as d2;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'GTM' AS Measure,
'TG_CG' AS Aggregation,
'Q' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
(
  with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(CASE WHEN (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))<=0 THEN 0 ELSE (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))END ,"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM")
AND quarter(EE_Date) = quarter(now())  AND   year(EE_Date) = year(now()) group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM") ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(CASE WHEN (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))<=0 THEN 0 ELSE (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))END ,"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM") AND  quarter(EE_Date) = quarter(now())  AND   year(EE_Date) = year(now())  ),0) as bt from cte2 ) as d2;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'GTM' AS Measure,
'INCR_TG' AS Aggregation,
'Q' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
( with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(sum(Incremental_Revenue),0) / ifnull(SUM(Target_Revenue),0)*100,0),2),"%") as target  FROM V_Dashboard_Deep_Dive_Incr_Rev where  Campaign in (select Campaign from Campaign_Deep_Dive_Display)  and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM" ) AND  quarter(EE_Date) = quarter(now())  AND   year(EE_Date) = year(now())
 group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM")  ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((SELECt  concat(round(ifnull(ifnull(sum(Incremental_Revenue),0) / ifnull(SUM(Target_Revenue),0)*100,0),2),"%") as target  FROM V_Dashboard_Deep_Dive_Incr_Rev where  Campaign in (select Campaign from Campaign_Deep_Dive_Display)  and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM" )  AND  quarter(EE_Date) = quarter(now())  AND   year(EE_Date) = year(now())  ),0)as bt from cte2 ) as d3;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'GTM' AS Measure,
'CTR' AS Aggregation,
'Q' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
(with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(SUM(Target_CTA),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM")
AND quarter(EE_Date) = quarter(now())  AND   year(EE_Date) = year(now()) group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM") ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(Target_CTA),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM") AND  quarter(EE_Date) = quarter(now())  AND   year(EE_Date) = year(now())  ),0) as bt from cte2  ) as d4;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'GTM' AS Measure,
'Click_Conv' AS Aggregation,
'Q' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
( with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(SUM(CTA_Responder),0) / ifnull(SUM(Target_CTA),0)*100,0),2),"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM")
AND quarter(EE_Date) = quarter(now())  AND   year(EE_Date) = year(now()) group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM")) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(CTA_Responder),0) / ifnull(SUM(Target_CTA),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM")  AND  quarter(EE_Date) = quarter(now())  AND   year(EE_Date) = year(now())  ),0) as bt from cte2 ) as d5;

-- Loading M Data

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'GTM' AS Measure,
'TG_Res' AS Aggregation,
'M' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
(
  with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") as target FROM 
CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM")
AND EE_Date between DATE_SUB( now(), INTERVAL 30 day) and now() group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM") ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM") AND  EE_Date between DATE_SUB( now(), INTERVAL 30 day) and now()  ),0) as bt from cte2 ) as d2;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'GTM' AS Measure,
'TG_CG' AS Aggregation,
'M' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
(
  with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(CASE WHEN (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))<=0 THEN 0 ELSE (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))END ,"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM")
AND EE_Date between DATE_SUB( now(), INTERVAL 30 day) and now() group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM") ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(CASE WHEN (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))<=0 THEN 0 ELSE (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))END ,"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM") AND  EE_Date between DATE_SUB( now(), INTERVAL 30 day) and now()  ),0) as bt from cte2 ) as d2;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'GTM' AS Measure,
'INCR_TG' AS Aggregation,
'M' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
( with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(sum(Incremental_Revenue),0) / ifnull(SUM(Target_Revenue),0)*100,0),2),"%") as target  FROM V_Dashboard_Deep_Dive_Incr_Rev where  Campaign in (select Campaign from Campaign_Deep_Dive_Display)  and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM" ) AND  EE_Date between DATE_SUB( now(), INTERVAL 30 day) and now()
 group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM")  ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((SELECt  concat(round(ifnull(ifnull(sum(Incremental_Revenue),0) / ifnull(SUM(Target_Revenue),0)*100,0),2),"%") as target  FROM V_Dashboard_Deep_Dive_Incr_Rev where  Campaign in (select Campaign from Campaign_Deep_Dive_Display)  and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM" )  AND  EE_Date between DATE_SUB( now(), INTERVAL 30 day) and now()  ),0)as bt from cte2 ) as d3;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'GTM' AS Measure,
'CTR' AS Aggregation,
'M' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
(with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(SUM(Target_CTA),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM")
AND EE_Date between DATE_SUB( now(), INTERVAL 30 day) and now() group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM") ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(Target_CTA),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM") AND  EE_Date between DATE_SUB( now(), INTERVAL 30 day) and now()  ),0) as bt from cte2  ) as d4;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'GTM' AS Measure,
'Click_Conv' AS Aggregation,
'M' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
( with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(SUM(CTA_Responder),0) / ifnull(SUM(Target_CTA),0)*100,0),2),"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM")
AND EE_Date between DATE_SUB( now(), INTERVAL 30 day) and now() group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM")) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(CTA_Responder),0) / ifnull(SUM(Target_CTA),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM")  AND  EE_Date between DATE_SUB( now(), INTERVAL 30 day) and now()  ),0) as bt from cte2 ) as d5;

-- Loading W Data

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'GTM' AS Measure,
'TG_Res' AS Aggregation,
'W' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
(
  with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") as target FROM 
CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM")
AND EE_Date between DATE_SUB( now(), INTERVAL 7 day) and now() group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM") ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM") AND  EE_Date between DATE_SUB( now(), INTERVAL 7 day) and now()  ),0) as bt from cte2 ) as d2;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'GTM' AS Measure,
'TG_CG' AS Aggregation,
'W' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
(
  with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(CASE WHEN (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))<=0 THEN 0 ELSE (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))END ,"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM")
AND EE_Date between DATE_SUB( now(), INTERVAL 7 day) and now() group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM") ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(CASE WHEN (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))<=0 THEN 0 ELSE (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))END ,"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM") AND  EE_Date between DATE_SUB( now(), INTERVAL 7 day) and now()  ),0) as bt from cte2 ) as d2;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'GTM' AS Measure,
'INCR_TG' AS Aggregation,
'W' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
( with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(sum(Incremental_Revenue),0) / ifnull(SUM(Target_Revenue),0)*100,0),2),"%") as target  FROM V_Dashboard_Deep_Dive_Incr_Rev where  Campaign in (select Campaign from Campaign_Deep_Dive_Display)  and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM" ) AND  EE_Date between DATE_SUB( now(), INTERVAL 7 day) and now()
 group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM")  ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((SELECt  concat(round(ifnull(ifnull(sum(Incremental_Revenue),0) / ifnull(SUM(Target_Revenue),0)*100,0),2),"%") as target  FROM V_Dashboard_Deep_Dive_Incr_Rev where  Campaign in (select Campaign from Campaign_Deep_Dive_Display)  and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM" )  AND  EE_Date between DATE_SUB( now(), INTERVAL 7 day) and now()  ),0)as bt from cte2 ) as d3;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'GTM' AS Measure,
'CTR' AS Aggregation,
'W' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
(with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(SUM(Target_CTA),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM")
AND EE_Date between DATE_SUB( now(), INTERVAL 7 day) and now() group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM") ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(Target_CTA),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM") AND  EE_Date between DATE_SUB( now(), INTERVAL 7 day) and now()  ),0) as bt from cte2  ) as d4;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'GTM' AS Measure,
'Click_Conv' AS Aggregation,
'W' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
( with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(SUM(CTA_Responder),0) / ifnull(SUM(Target_CTA),0)*100,0),2),"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM")
AND EE_Date between DATE_SUB( now(), INTERVAL 7 day) and now() group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM")) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(CTA_Responder),0) / ifnull(SUM(Target_CTA),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "GTM")  AND  EE_Date between DATE_SUB( now(), INTERVAL 7 day) and now()  ),0) as bt from cte2 ) as d5;

-- Loading CLM data

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'CLM' AS Measure,
'TG_Res' AS Aggregation,
'Y' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
(
  with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") as target FROM 
CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM")
AND year(EE_Date) = year(now()) group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM") ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM") AND  year(EE_Date) = year(now())  ),0) as bt from cte2 ) as d2;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'CLM' AS Measure,
'TG_CG' AS Aggregation,
'Y' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
(
  with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(CASE WHEN (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))<=0 THEN 0 ELSE (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))END ,"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM")
AND year(EE_Date) = year(now()) group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM") ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(CASE WHEN (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))<=0 THEN 0 ELSE (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))END ,"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM") AND  year(EE_Date) = year(now())  ),0) as bt from cte2 ) as d2;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'CLM' AS Measure,
'INCR_TG' AS Aggregation,
'Y' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
( with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(sum(Incremental_Revenue),0) / ifnull(SUM(Target_Revenue),0)*100,0),2),"%") as target  FROM V_Dashboard_Deep_Dive_Incr_Rev where  Campaign in (select Campaign from Campaign_Deep_Dive_Display)  and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM" ) AND  year(EE_Date) = year(now())
 group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM")  ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((SELECt  concat(round(ifnull(ifnull(sum(Incremental_Revenue),0) / ifnull(SUM(Target_Revenue),0)*100,0),2),"%") as target  FROM V_Dashboard_Deep_Dive_Incr_Rev where  Campaign in (select Campaign from Campaign_Deep_Dive_Display)  and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM" )  AND  year(EE_Date) = year(now())  ),0)as bt from cte2 ) as d3;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'CLM' AS Measure,
'CTR' AS Aggregation,
'Y' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
(with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(SUM(Target_CTA),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM")
AND year(EE_Date) = year(now()) group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM") ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(Target_CTA),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM") AND  year(EE_Date) = year(now())  ),0) as bt from cte2  ) as d4;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'CLM' AS Measure,
'Click_Conv' AS Aggregation,
'Y' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
( with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(SUM(CTA_Responder),0) / ifnull(SUM(Target_CTA),0)*100,0),2),"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM")
AND year(EE_Date) = year(now()) group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM")) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(CTA_Responder),0) / ifnull(SUM(Target_CTA),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM")  AND  year(EE_Date) = year(now())  ),0) as bt from cte2 ) as d5;

-- Loading Q data

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'CLM' AS Measure,
'TG_Res' AS Aggregation,
'Q' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
(
  with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") as target FROM 
CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM")
AND quarter(EE_Date) = quarter(now())  AND   year(EE_Date) = year(now()) group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM") ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM") AND  quarter(EE_Date) = quarter(now())  AND   year(EE_Date) = year(now())  ),0) as bt from cte2 ) as d2;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'CLM' AS Measure,
'TG_CG' AS Aggregation,
'Q' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
(
  with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(CASE WHEN (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))<=0 THEN 0 ELSE (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))END ,"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM")
AND quarter(EE_Date) = quarter(now())  AND   year(EE_Date) = year(now()) group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM") ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(CASE WHEN (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))<=0 THEN 0 ELSE (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))END ,"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM") AND  quarter(EE_Date) = quarter(now())  AND   year(EE_Date) = year(now())  ),0) as bt from cte2 ) as d2;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'CLM' AS Measure,
'INCR_TG' AS Aggregation,
'Q' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
( with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(sum(Incremental_Revenue),0) / ifnull(SUM(Target_Revenue),0)*100,0),2),"%") as target  FROM V_Dashboard_Deep_Dive_Incr_Rev where  Campaign in (select Campaign from Campaign_Deep_Dive_Display)  and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM" ) AND  quarter(EE_Date) = quarter(now())  AND   year(EE_Date) = year(now())
 group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM")  ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((SELECt  concat(round(ifnull(ifnull(sum(Incremental_Revenue),0) / ifnull(SUM(Target_Revenue),0)*100,0),2),"%") as target  FROM V_Dashboard_Deep_Dive_Incr_Rev where  Campaign in (select Campaign from Campaign_Deep_Dive_Display)  and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM" )  AND  quarter(EE_Date) = quarter(now())  AND   year(EE_Date) = year(now())  ),0)as bt from cte2 ) as d3;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'CLM' AS Measure,
'CTR' AS Aggregation,
'Q' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
(with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(SUM(Target_CTA),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM")
AND quarter(EE_Date) = quarter(now())  AND   year(EE_Date) = year(now()) group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM") ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(Target_CTA),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM") AND  quarter(EE_Date) = quarter(now())  AND   year(EE_Date) = year(now())  ),0) as bt from cte2  ) as d4;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'CLM' AS Measure,
'Click_Conv' AS Aggregation,
'Q' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
( with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(SUM(CTA_Responder),0) / ifnull(SUM(Target_CTA),0)*100,0),2),"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM")
AND quarter(EE_Date) = quarter(now())  AND   year(EE_Date) = year(now()) group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM")) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(CTA_Responder),0) / ifnull(SUM(Target_CTA),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM")  AND  quarter(EE_Date) = quarter(now())  AND   year(EE_Date) = year(now())  ),0) as bt from cte2 ) as d5;

-- Loading M Data

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'CLM' AS Measure,
'TG_Res' AS Aggregation,
'M' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
(
  with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") as target FROM 
CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM")
AND EE_Date between DATE_SUB( now(), INTERVAL 30 day) and now() group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM") ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM") AND  EE_Date between DATE_SUB( now(), INTERVAL 30 day) and now()  ),0) as bt from cte2 ) as d2;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'CLM' AS Measure,
'TG_CG' AS Aggregation,
'M' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
(
  with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(CASE WHEN (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))<=0 THEN 0 ELSE (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))END ,"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM")
AND EE_Date between DATE_SUB( now(), INTERVAL 30 day) and now() group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM") ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(CASE WHEN (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))<=0 THEN 0 ELSE (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))END ,"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM") AND  EE_Date between DATE_SUB( now(), INTERVAL 30 day) and now()  ),0) as bt from cte2 ) as d2;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'CLM' AS Measure,
'INCR_TG' AS Aggregation,
'M' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
( with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(sum(Incremental_Revenue),0) / ifnull(SUM(Target_Revenue),0)*100,0),2),"%") as target  FROM V_Dashboard_Deep_Dive_Incr_Rev where  Campaign in (select Campaign from Campaign_Deep_Dive_Display)  and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM" ) AND  EE_Date between DATE_SUB( now(), INTERVAL 30 day) and now()
 group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM")  ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((SELECt  concat(round(ifnull(ifnull(sum(Incremental_Revenue),0) / ifnull(SUM(Target_Revenue),0)*100,0),2),"%") as target  FROM V_Dashboard_Deep_Dive_Incr_Rev where  Campaign in (select Campaign from Campaign_Deep_Dive_Display)  and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM" )  AND  EE_Date between DATE_SUB( now(), INTERVAL 30 day) and now()  ),0)as bt from cte2 ) as d3;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'CLM' AS Measure,
'CTR' AS Aggregation,
'M' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
(with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(SUM(Target_CTA),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM")
AND EE_Date between DATE_SUB( now(), INTERVAL 30 day) and now() group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM") ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(Target_CTA),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM") AND  EE_Date between DATE_SUB( now(), INTERVAL 30 day) and now()  ),0) as bt from cte2  ) as d4;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'CLM' AS Measure,
'Click_Conv' AS Aggregation,
'M' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
( with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(SUM(CTA_Responder),0) / ifnull(SUM(Target_CTA),0)*100,0),2),"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM")
AND EE_Date between DATE_SUB( now(), INTERVAL 30 day) and now() group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM")) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(CTA_Responder),0) / ifnull(SUM(Target_CTA),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM")  AND  EE_Date between DATE_SUB( now(), INTERVAL 30 day) and now()  ),0) as bt from cte2 ) as d5;

-- Loading W Data

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'CLM' AS Measure,
'TG_Res' AS Aggregation,
'W' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
(
  with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") as target FROM 
CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM")
AND EE_Date between DATE_SUB( now(), INTERVAL 7 day) and now() group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM") ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM") AND  EE_Date between DATE_SUB( now(), INTERVAL 7 day) and now()  ),0) as bt from cte2 ) as d2;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'CLM' AS Measure,
'TG_CG' AS Aggregation,
'W' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
(
  with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(CASE WHEN (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))<=0 THEN 0 ELSE (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))END ,"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM")
AND EE_Date between DATE_SUB( now(), INTERVAL 7 day) and now() group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM") ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(CASE WHEN (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))<=0 THEN 0 ELSE (round(ifnull(ifnull(SUM(Target_Responder),0)/ifnull(SUM(Target_Delivered),0)*100,0),2) - round(ifnull(ifnull(SUM(Control_Responder),0)/ifnull(SUM(Control_Base),0)*100,0),2))END ,"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM") AND  EE_Date between DATE_SUB( now(), INTERVAL 7 day) and now()  ),0) as bt from cte2 ) as d2;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'CLM' AS Measure,
'INCR_TG' AS Aggregation,
'W' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
( with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(sum(Incremental_Revenue),0) / ifnull(SUM(Target_Revenue),0)*100,0),2),"%") as target  FROM V_Dashboard_Deep_Dive_Incr_Rev where  Campaign in (select Campaign from Campaign_Deep_Dive_Display)  and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM" ) AND  EE_Date between DATE_SUB( now(), INTERVAL 7 day) and now()
 group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM")  ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((SELECt  concat(round(ifnull(ifnull(sum(Incremental_Revenue),0) / ifnull(SUM(Target_Revenue),0)*100,0),2),"%") as target  FROM V_Dashboard_Deep_Dive_Incr_Rev where  Campaign in (select Campaign from Campaign_Deep_Dive_Display)  and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM" )  AND  EE_Date between DATE_SUB( now(), INTERVAL 7 day) and now()  ),0)as bt from cte2 ) as d3;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'CLM' AS Measure,
'CTR' AS Aggregation,
'W' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
(with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(SUM(Target_CTA),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM")
AND EE_Date between DATE_SUB( now(), INTERVAL 7 day) and now() group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM") ) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(Target_CTA),0)/ifnull(SUM(Target_Delivered),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM") AND  EE_Date between DATE_SUB( now(), INTERVAL 7 day) and now()  ),0) as bt from cte2  ) as d4;

INSERT INTO Dashboard_Campaign_Deep_Dive
(
`Measure`,
`Aggregation`,
`Date_Aggregation`,
`Event_Id`,
`Camp`,
`Ofs`,
`Total`,
`BT`
)
SELECT
'CLM' AS Measure,
'Click_Conv' AS Aggregation,
'W' AS Date_Aggregation,
cur_Event_Id AS Event_Id,
Camp,
Ofs,
total,
bt
FROM
( with cte2 as  (  
 (select     
COALESCE((select Rank from( 
with cte5 as (with cte as  (SELECt Event_Id, concat(round(ifnull(ifnull(SUM(CTA_Responder),0) / ifnull(SUM(Target_CTA),0)*100,0),2),"%") as target FROM CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM")
AND EE_Date between DATE_SUB( now(), INTERVAL 7 day) and now() group by 1)
select Event_Id,ROW_NUMBER() OVER ( ORDER BY target desc) Rank from cte )
select * from cte5 where Event_Id = cur_Event_Id) as d1),"__") as camp
, "ofs" ,(select count(distinct Campaign) from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM")) as total from CLM_Event_Dashboard limit 1)  )
select  *, IFNULL((select concat(round(ifnull(ifnull(SUM(CTA_Responder),0) / ifnull(SUM(Target_CTA),0)*100,0),2),"%") from CLM_Event_Dashboard  where   Campaign in (select Campaign from Campaign_Deep_Dive_Display) and Event_ID in (select event_id from Event_Master_UI where Type_Of_Event = "CLM")  AND  EE_Date between DATE_SUB( now(), INTERVAL 7 day) and now()  ),0) as bt from cte2 ) as d5;



END LOOP LOOP_ALL_DATES;
END$$
DELIMITER ;
