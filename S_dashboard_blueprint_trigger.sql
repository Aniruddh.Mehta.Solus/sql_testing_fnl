DELIMITER $$
CREATE or replace PROCEDURE `S_dashboard_blueprint_trigger`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
    declare IN_FILTER_CURRENTMONTH text;
    declare IN_MEASURE text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
     SET IN_MEASURE=json_unquote(json_extract(in_request_json,"$.MEASURE"));	
    SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
    SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
   
	CASE 
    WHEN IN_SCREEN="BLUEPRINT_TRIGGER_LIST" AND IN_MEASURE="LIST" THEN
    BEGIN
    
   
    SET @Query_String1= 'create or replace view V_Dashboard_Blueprint_Trigger_List as    SELECT 
      `CLM_Campaign_Trigger`.`CampTriggerName` AS `Trigger Name`,
       CASE
            WHEN `CLM_Campaign_Trigger`.`CTState` = 1 THEN "Enabled"
            WHEN `CLM_Campaign_Trigger`.`CTState` = 2 THEN "Draft"
            ELSE "Disabled"
        END AS `Status`,
      `CLM_Campaign`.`CampaignName` As `Campaign`,  
      `CLM_CampaignTheme`.`CampaignThemeName` As `Campaign Theme`,
	  `CLM_Channel`.`ChannelName` As `Channel`,
      `CLM_Segment`.`CLMSegmentName` As `Lifecycle Segment`
    FROM
        (((((`CLM_CampaignTheme`
        JOIN `CLM_Segment`)
        JOIN `CLM_Campaign`)
        JOIN `CLM_Campaign_Trigger`)
       JOIN `CLM_Channel`)
        JOIN `CLM_Campaign_Events`)
    WHERE
        `CLM_Campaign_Events`.`CampaignThemeId` = `CLM_CampaignTheme`.`CampaignThemeId`
            AND `CLM_Campaign_Events`.`CLMSegmentId` = `CLM_Segment`.`CLMSegmentId`
            AND `CLM_Campaign_Events`.`CampaignId` = `CLM_Campaign`.`CampaignId`
            AND `CLM_Campaign_Events`.`CampTriggerId` = `CLM_Campaign_Trigger`.`CampTriggerId`
            AND `CLM_Campaign_Events`.`ChannelId` = `CLM_Channel`.`ChannelId` 
            AND `CLM_Campaign_Trigger`.`CTState` = 1;';
               select @Query_String1;
                PREPARE statement1 from @Query_String1;
                Execute statement1;
                Deallocate PREPARE statement1; 
				
			    SET @Query_String = 'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Trigger Name",`Trigger Name`,"Status",`Status`,"Campaign",`Campaign`,"Channel",`Channel`,"Campaign Theme",`Campaign Theme`,"Lifecycle Segment",`Lifecycle Segment`) )),"]")into @temp_result from V_Dashboard_Blueprint_Trigger_List;' ;
			
               select @Query_String;
                PREPARE statement from @Query_String;
                Execute statement;
                Deallocate PREPARE statement; 
                select @temp_result into out_result_json;
                                      
    END;
    WHEN IN_SCREEN="BLUEPRINT_TRIGGER_LIST" AND IN_MEASURE="EDIT" THEN
    BEGIN
        SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("EventId", EventId, 
					"EventName", EventName, 
					"CampaignThemeId", CampaignThemeId, 
					"CampaignThemeName", CampaignThemeName, 
					"CLMSegmentId", CLMSegmentId, 
					"CLMSegmentName", CLMSegmentName, 
					"CampaignId", CampaignId, 
					"CampaignName", CampaignName, 
					"MicroSegmentId", MicroSegmentId, 
					"MicroSegmentName", MicroSegmentName, 
					"ChannelId", ChannelId, 
					"ChannelName", ChannelName, 
					"OfferTypeId", OfferTypeId, 
					"OfferTypeName", OfferTypeName, 
					"OfferId", OfferId, 
					"OfferName", OfferName, 
					"OfferDesc", OfferDesc, 
					"OfferDef", OfferDef, 
					"OfferStartDate", OfferStartDate, 
					"OfferEndDate", OfferEndDate, 
					"OfferRedemptionDays", OfferRedemptionDays, 
					"OfferIsAtCustomerLevel", OfferIsAtCustomerLevel, 
					"CampTriggerId", CampTriggerId, 
					"CampTriggerName", CampTriggerName, 
					"CampTriggerDesc", CampTriggerDesc, 
					"PrecedingTriggerId", PrecedingTriggerId, 
					"PrecedingTriggerName", PrecedingTriggerName, 
					"DaysOffsetFromPrecedingTrigger", DaysOffsetFromPrecedingTrigger, 
					"CTState", CTState, 
					"Execution_Sequence", Execution_Sequence, 
					"CTIsMandatory", CTIsMandatory, 
					"IsAReminder", IsAReminder, 
					"CTReplacedQueryAsInUI", CTReplacedQueryAsInUI, 
					"CTReplacedQuery", CTReplacedQuery, 
					"CTIsValidSVOCCondition", CTIsValidSVOCCondition, 
					"CTReplacedQueryBillAsInUI", CTReplacedQueryBillAsInUI, 
					"CTReplacedQueryBill", CTReplacedQueryBill, 
					"CTIsValidBillCondition", CTIsValidBillCondition, 
					"CTUsesGoodTimeModel", CTUsesGoodTimeModel, 
					"CreativeId", CreativeId, 
					"CreativeName", CreativeName, 
					"ExtCreativeKey", ExtCreativeKey, 
					"SenderKey", SenderKey, 
					"Timeslot_Id", Timeslot_Id, 
					"CreativeDesc", CreativeDesc, 
					"Creative", Creative, 
					"Header", Header, 
					"Microsite_Creative", Microsite_Creative, 
					"Microsite_Header", Microsite_Header, 
					"Exclude_Recommended_SameAP_NotLT_FavCat", Exclude_Recommended_SameAP_NotLT_FavCat, 
					"Exclude_Recommended_SameLTD_FavCat", Exclude_Recommended_SameLTD_FavCat, 
					"Exclude_Recommended_SameLT_FavCat", Exclude_Recommended_SameLT_FavCat, 
					"Recommendation_Column", Recommendation_Column, 
					"Recommendation_Type", Recommendation_Type, 
					"Exclude_Stock_out", Exclude_Stock_out, 
					"Exclude_Transaction_X_Days", Exclude_Transaction_X_Days, 
					"Exclude_Recommended_X_Days", Exclude_Recommended_X_Days, 
					"Exclude_Never_Brought", Exclude_Never_Brought, 
					"Exclude_No_Image", Exclude_No_Image, 
					"Number_Of_Recommendation", Number_Of_Recommendation, 
					"Recommendation_Filter_Logic", Recommendation_Filter_Logic, 
					"Recommendation_Column_Header", Recommendation_Column_Header, 
					"Is_Recommended_SameLT_FavCat", Is_Recommended_SameLT_FavCat, 
					"Is_Recommended_SameLTD_Cat", Is_Recommended_SameLTD_Cat, 
					"Is_Recommended_SameLTD_Dep", Is_Recommended_SameLTD_Dep, 
					"Is_Recommended_NewLaunch_Product", Is_Recommended_NewLaunch_Product, 
					"throttling", throttling, 
					"Updated_On", Updated_On))),"]")into @temp_result from CLM_UI_EVENT_DETAIL_PAGE_VIEW
                    where CampTriggerName="',IN_AGGREGATION_1,'" and CampaignName="',IN_AGGREGATION_2,'";');
                     select @Query_String;
                    PREPARE statement from @Query_String;
                    Execute statement;
                    Deallocate PREPARE statement; 
                    select @temp_result into out_result_json;
    END;
     WHEN IN_SCREEN="BLUEPRINT_TRIGGER" AND IN_MEASURE="LOV" THEN
    BEGIN
     Select concat('[{',GROUP_CONCAT('\"', column_name, '\":\"',
            CASE 
               when data_type in ('bigint','int','smallint','decimal','tinyint') then 'Number'
				
               when data_type='Date' then 'Date'
             ELSE
                'String'            
            END
            ,'\"'),'}]') into @temp_result1
            FROM   `information_schema`.`columns` 
		    WHERE  `table_schema`=DATABASE() 
		    AND `table_name`= IN_AGGREGATION_1;   
            select @temp_result1 into out_result_json;
    
    END;
     WHEN IN_SCREEN="BLUEPRINT_TRIGGER" AND IN_MEASURE="VALIDATE_CREATIVE" THEN
    BEGIN
       declare IN_Comm_Template text;
                declare IN_Header text;
				declare OUT_Output text;
                declare OUT_json text;
                declare temp_result json;
                SET IN_Comm_Template = IN_AGGREGATION_1;
                SET IN_Header = IN_AGGREGATION_2;
				call PROC_CLM_IS_VALID_Template(IN_Comm_Template,IN_Header,OUT_Output);
                select OUT_Output into OUT_json;
                Set temp_result = json_object("Output",OUT_json);
                SELECT temp_result into out_result_json;
    END;
       WHEN IN_SCREEN="BLUEPRINT_TRIGGER" AND IN_MEASURE="VALIDATE_SVOC" THEN
    BEGIN
    
    
        declare IN_SegmentName text;
                declare IN_SQLCondition text;
				declare  IN_BillCondition text;
                declare IN_SQLCondition_SegmentCriteria text;
				declare IN_Segmentid TINYINT;
                declare IN_CheckValidityOnly TINYINT;
                declare Query_String text;
                declare OUT_SQLCondition_Segment text;
                declare OUT_Success Varchar(128);
                declare OUT_Count text;
                declare temp_result json;
                SET IN_SegmentName = IN_AGGREGATION_1;
                SET IN_SQLCondition = IN_AGGREGATION_2;
                SET IN_BillCondition=IN_AGGREGATION_3;
				SET IN_CheckValidityOnly = 1;
                Select CLMSegmentId into IN_Segmentid from CLM_Segment where CLMSegmentName = IN_SegmentName;
                Select CLMSegmentReplacedQueryAsInUI into IN_SQLCondition_SegmentCriteria from CLM_Segment where CLMSegmentName = IN_SegmentName;
                -- SET IN_SQLCondition = CONCAT(IN_SQLCondition, " and ", IN_SQLCondition_SegmentCriteria);
                call PROC_CLM_IS_VALID_SVOC_CONDITION(IN_SQLCondition,IN_BillCondition, IN_Segmentid, IN_CheckValidityOnly, OUT_SQLCondition_Segment, OUT_Success);
                SET Query_String = concat("Select Count(1) into @temp_count from Customer_One_View where ", OUT_SQLCondition_Segment, ";");
				PREPARE statement from Query_String;
                Execute statement;
                Deallocate PREPARE statement;
                Select @temp_count into OUT_Count;
                Set temp_result = json_object("Count",OUT_Count,"Success",OUT_Success);
                SELECT temp_result into out_result_json;

    END;
    
    ELSE
			SET out_result_json=json_object("Status","Not Implemented Yet");
	    END CASE;	
END$$
DELIMITER ;
