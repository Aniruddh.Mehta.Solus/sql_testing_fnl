DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_insights_glue_cohorts_metrics`()
BEGIN

DELETE from log_solus where module = 'S_dashboard_insights_glue_cohorts_metrics';
SET SQL_SAFE_UPDATES=0;

SET @Start_Date = date_format(date_sub(concat(date_format(current_date(),"%Y%m"),'01'), INTERVAL 15 MONTH),"%Y%m");
SET @End_Date = date_format(current_date(),"%Y%m");

INSERT INTO Dashboard_Metrics_Log
(
`Screen_Name`,
`Status`
)
VALUES
('INSIGHTS - GLUE COHORTS',
'Started');


INSERT INTO log_solus(module,msg) values ('S_dashboard_insights_glue_cohorts_metrics','Loading Glue Cohorts data');

INSERT INTO Dashboard_Insights_Glue_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Count`
)
SELECT 
'Glue Count' AS Measure, 
'New Base' AS AGGREGATION, 
DATE_FORMAT(FTD,'%Y%m') AS Bill_Year_Month,
COUNT(Customer_Id) AS Customer_Count
FROM CDM_Customer_TP_Var 
WHERE DATE_FORMAT(FTD,'%Y%m') BETWEEN @Start_Date AND @End_Date
AND FTD IS NOT NULL
GROUP BY Bill_Year_Month;

INSERT INTO Dashboard_Insights_Glue_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Count`
)
SELECT 
'Glue Count' AS Measure, 
'Single Transactors' AS AGGREGATION, 
DATE_FORMAT(FTD,'%Y%m') AS Bill_Year_Month,
COUNT(Customer_Id) AS Customer_Count
FROM CDM_Customer_TP_Var 
WHERE FTD IS NOT NULL
AND Lt_Num_of_Visits = 1 
AND FTD = LTD
AND DATE_FORMAT(FTD,'%Y%m') BETWEEN @Start_Date AND @End_Date
GROUP BY Bill_Year_Month;

INSERT INTO Dashboard_Insights_Glue_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Count`
)
SELECT 
'Glue Count' AS Measure, 
'2 Transactions' AS AGGREGATION, 
DATE_FORMAT(FTD,'%Y%m') AS Bill_Year_Month,
COUNT(Customer_Id) AS Customer_Count
FROM CDM_Customer_TP_Var 
WHERE FTD IS NOT NULL
AND Lt_Num_of_Visits = 2
AND DATE_FORMAT(FTD,'%Y%m') BETWEEN @Start_Date AND @End_Date
GROUP BY Bill_Year_Month;

INSERT INTO Dashboard_Insights_Glue_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Count`
)
SELECT 
'Glue Count' AS Measure, 
'3 Transactions' AS AGGREGATION, 
DATE_FORMAT(FTD,'%Y%m') AS Bill_Year_Month,
COUNT(Customer_Id) AS Customer_Count
FROM CDM_Customer_TP_Var 
WHERE FTD IS NOT NULL
AND Lt_Num_of_Visits = 3
AND DATE_FORMAT(FTD,'%Y%m') BETWEEN @Start_Date AND @End_Date
GROUP BY Bill_Year_Month;

INSERT INTO Dashboard_Insights_Glue_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Count`
)
SELECT 
'Glue Count' AS Measure, 
'4 Transactions' AS AGGREGATION, 
DATE_FORMAT(FTD,'%Y%m') AS Bill_Year_Month,
COUNT(Customer_Id) AS Customer_Count
FROM CDM_Customer_TP_Var 
WHERE FTD IS NOT NULL
AND Lt_Num_of_Visits = 4
AND DATE_FORMAT(FTD,'%Y%m') BETWEEN @Start_Date AND @End_Date
GROUP BY Bill_Year_Month;

INSERT INTO Dashboard_Insights_Glue_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Count`
)
SELECT 
'Glue Count' AS Measure, 
'5 Transactions' AS AGGREGATION, 
DATE_FORMAT(FTD,'%Y%m') AS Bill_Year_Month,
COUNT(Customer_Id) AS Customer_Count
FROM CDM_Customer_TP_Var 
WHERE FTD IS NOT NULL
AND Lt_Num_of_Visits = 5
AND DATE_FORMAT(FTD,'%Y%m') BETWEEN @Start_Date AND @End_Date
GROUP BY Bill_Year_Month;

INSERT INTO Dashboard_Insights_Glue_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Count`
)
SELECT 
'Glue Count' AS Measure, 
'>5 Transactions' AS AGGREGATION, 
DATE_FORMAT(FTD,'%Y%m') AS Bill_Year_Month,
COUNT(Customer_Id) AS Customer_Count
FROM CDM_Customer_TP_Var 
WHERE FTD IS NOT NULL
AND Lt_Num_of_Visits > 5
AND DATE_FORMAT(FTD,'%Y%m') BETWEEN @Start_Date AND @End_Date
GROUP BY Bill_Year_Month;

INSERT INTO Dashboard_Insights_Glue_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Percent`
)
SELECT 
'Glue Percent' AS Measure, 
'Single Transactors' AS AGGREGATION, 
Bill_Year_Month, 
Bills AS Customer_Percent
FROM
(
SELECT C.Bill_Year_Month , Bills FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills',B.Bill_Year_Month FROM
(
SELECT COUNT(DISTINCT Customer_Id) AS c1, DATE_FORMAT(FTD,'%Y%m') AS Bill_Year_Month 
FROM CDM_Customer_TP_Var
WHERE Lt_Num_of_Visits = 1
AND FTD = LTD
AND DATE_FORMAT(FTD,'%Y%m') BETWEEN @Start_Date AND @End_Date
GROUP BY Bill_Year_Month
)A
,
(
SELECT DATE_FORMAT(FTD,'%Y%m') AS Bill_Year_Month,COUNT(DISTINCT Customer_Id) AS COUNT2 
FROM CDM_Customer_TP_Var 
WHERE FTD IS NOT NULL 
AND DATE_FORMAT(FTD,'%Y%m') BETWEEN @Start_Date AND @End_Date
GROUP BY Bill_Year_Month 
)B
WHERE A.Bill_Year_Month = B.Bill_Year_Month
) C
GROUP BY C.Bill_Year_Month
) T;

INSERT INTO Dashboard_Insights_Glue_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Percent`
)
SELECT 
'Glue Percent' AS Measure, 
'2 Transactions' AS AGGREGATION, 
Bill_Year_Month, 
Bills AS Customer_Percent
FROM
(
SELECT C.Bill_Year_Month , Bills FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills',B.Bill_Year_Month FROM
(
SELECT COUNT(DISTINCT Customer_Id) AS c1, DATE_FORMAT(FTD,'%Y%m') AS Bill_Year_Month 
FROM CDM_Customer_TP_Var
WHERE Lt_Num_of_Visits = 2
AND DATE_FORMAT(FTD,'%Y%m') BETWEEN @Start_Date AND @End_Date
GROUP BY Bill_Year_Month
)A
,
(
SELECT DATE_FORMAT(FTD,'%Y%m') AS Bill_Year_Month,COUNT(DISTINCT Customer_Id) AS COUNT2 
FROM CDM_Customer_TP_Var 
WHERE FTD IS NOT NULL 
AND DATE_FORMAT(FTD,'%Y%m') BETWEEN @Start_Date AND @End_Date
GROUP BY Bill_Year_Month 
)B
WHERE A.Bill_Year_Month = B.Bill_Year_Month
) C
GROUP BY C.Bill_Year_Month
) T;

INSERT INTO Dashboard_Insights_Glue_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Percent`
)
SELECT 
'Glue Percent' AS Measure, 
'3 Transactions' AS AGGREGATION, 
Bill_Year_Month, 
Bills AS Customer_Percent
FROM
(
SELECT C.Bill_Year_Month , Bills FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills',B.Bill_Year_Month FROM
(
SELECT COUNT(DISTINCT Customer_Id) AS c1, DATE_FORMAT(FTD,'%Y%m') AS Bill_Year_Month 
FROM CDM_Customer_TP_Var
WHERE Lt_Num_of_Visits = 3
AND DATE_FORMAT(FTD,'%Y%m') BETWEEN @Start_Date AND @End_Date
GROUP BY Bill_Year_Month
)A
,
(
SELECT DATE_FORMAT(FTD,'%Y%m') AS Bill_Year_Month,COUNT(DISTINCT Customer_Id) AS COUNT2 
FROM CDM_Customer_TP_Var 
WHERE FTD IS NOT NULL 
AND DATE_FORMAT(FTD,'%Y%m') BETWEEN @Start_Date AND @End_Date
GROUP BY Bill_Year_Month 
)B
WHERE A.Bill_Year_Month = B.Bill_Year_Month
) C
GROUP BY C.Bill_Year_Month
) T;

INSERT INTO Dashboard_Insights_Glue_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Percent`
)
SELECT 
'Glue Percent' AS Measure, 
'4 Transactions' AS AGGREGATION, 
Bill_Year_Month, 
Bills AS Customer_Percent
FROM
(
SELECT C.Bill_Year_Month , Bills FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills',B.Bill_Year_Month FROM
(
SELECT COUNT(DISTINCT Customer_Id) AS c1, DATE_FORMAT(FTD,'%Y%m') AS Bill_Year_Month 
FROM CDM_Customer_TP_Var
WHERE Lt_Num_of_Visits = 4
AND DATE_FORMAT(FTD,'%Y%m') BETWEEN @Start_Date AND @End_Date
GROUP BY Bill_Year_Month
)A
,
(
SELECT DATE_FORMAT(FTD,'%Y%m') AS Bill_Year_Month,COUNT(DISTINCT Customer_Id) AS COUNT2 
FROM CDM_Customer_TP_Var 
WHERE FTD IS NOT NULL 
AND DATE_FORMAT(FTD,'%Y%m') BETWEEN @Start_Date AND @End_Date
GROUP BY Bill_Year_Month 
)B
WHERE A.Bill_Year_Month = B.Bill_Year_Month
) C
GROUP BY C.Bill_Year_Month
) T;

INSERT INTO Dashboard_Insights_Glue_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Percent`
)
SELECT 
'Glue Percent' AS Measure, 
'5 Transactions' AS AGGREGATION, 
Bill_Year_Month, 
Bills AS Customer_Percent
FROM
(
SELECT C.Bill_Year_Month , Bills FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills',B.Bill_Year_Month FROM
(
SELECT COUNT(DISTINCT Customer_Id) AS c1, DATE_FORMAT(FTD,'%Y%m') AS Bill_Year_Month 
FROM CDM_Customer_TP_Var
WHERE Lt_Num_of_Visits = 5
AND DATE_FORMAT(FTD,'%Y%m') BETWEEN @Start_Date AND @End_Date
GROUP BY Bill_Year_Month
)A
,
(
SELECT DATE_FORMAT(FTD,'%Y%m') AS Bill_Year_Month,COUNT(DISTINCT Customer_Id) AS COUNT2 
FROM CDM_Customer_TP_Var 
WHERE FTD IS NOT NULL 
AND DATE_FORMAT(FTD,'%Y%m') BETWEEN @Start_Date AND @End_Date
GROUP BY Bill_Year_Month 
)B
WHERE A.Bill_Year_Month = B.Bill_Year_Month
) C
GROUP BY C.Bill_Year_Month
) T;

INSERT INTO Dashboard_Insights_Glue_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Percent`
)
SELECT 
'Glue Percent' AS Measure, 
'>5 Transactions' AS AGGREGATION, 
Bill_Year_Month, 
Bills AS Customer_Percent
FROM
(
SELECT C.Bill_Year_Month , Bills FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills',B.Bill_Year_Month FROM
(
SELECT COUNT(DISTINCT Customer_Id) AS c1, DATE_FORMAT(FTD,'%Y%m') AS Bill_Year_Month 
FROM CDM_Customer_TP_Var
WHERE Lt_Num_of_Visits > 5
AND DATE_FORMAT(FTD,'%Y%m') BETWEEN @Start_Date AND @End_Date
GROUP BY Bill_Year_Month
)A
,
(
SELECT DATE_FORMAT(FTD,'%Y%m') AS Bill_Year_Month,COUNT(DISTINCT Customer_Id) AS COUNT2 
FROM CDM_Customer_TP_Var 
WHERE FTD IS NOT NULL 
AND DATE_FORMAT(FTD,'%Y%m') BETWEEN @Start_Date AND @End_Date
GROUP BY Bill_Year_Month 
)B
WHERE A.Bill_Year_Month = B.Bill_Year_Month
) C
GROUP BY C.Bill_Year_Month
) T;

INSERT INTO Dashboard_Insights_Glue_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Count`
)
SELECT 
'Glue Avg' AS Measure, 
'2 Transactions' AS AGGREGATION, 
DATE_FORMAT(FTD,'%Y%m') AS Bill_Year_Month,
ifnull(round(AVG(datediff(LTD, FTD))),0) AS Customer_Count
FROM CDM_Customer_TP_Var 
WHERE DATE_FORMAT(FTD,'%Y%m') BETWEEN @Start_Date AND @End_Date
AND Lt_Num_of_Visits = 2
GROUP BY Bill_Year_Month;

INSERT INTO Dashboard_Insights_Glue_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Count`
)
SELECT 
'Glue Avg' AS Measure, 
'3 Transactions' AS AGGREGATION, 
DATE_FORMAT(FTD,'%Y%m') AS Bill_Year_Month,
ifnull(round(AVG(datediff(LTD, FTD))),0) AS Customer_Count
FROM CDM_Customer_TP_Var 
WHERE DATE_FORMAT(FTD,'%Y%m') BETWEEN @Start_Date AND @End_Date
AND Lt_Num_of_Visits = 3
GROUP BY Bill_Year_Month;

INSERT INTO Dashboard_Insights_Glue_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Count`
)
SELECT 
'Glue Avg' AS Measure, 
'4 Transactions' AS AGGREGATION, 
DATE_FORMAT(FTD,'%Y%m') AS Bill_Year_Month,
ifnull(round(AVG(datediff(LTD, FTD))),0) AS Customer_Count
FROM CDM_Customer_TP_Var 
WHERE DATE_FORMAT(FTD,'%Y%m') BETWEEN @Start_Date AND @End_Date
AND Lt_Num_of_Visits = 4
GROUP BY Bill_Year_Month;

INSERT INTO Dashboard_Insights_Glue_Cohorts
(
`Measure`,
`AGGREGATION`,
`Bill_Year_Month`,
`Customer_Count`
)
SELECT 
'Glue Avg' AS Measure, 
'5 Transactions' AS AGGREGATION, 
DATE_FORMAT(FTD,'%Y%m') AS Bill_Year_Month,
ifnull(round(AVG(datediff(LTD, FTD))),0) AS Customer_Count
FROM CDM_Customer_TP_Var 
WHERE DATE_FORMAT(FTD,'%Y%m') BETWEEN @Start_Date AND @End_Date
AND Lt_Num_of_Visits = 5
GROUP BY Bill_Year_Month;

UPDATE Dashboard_Metrics_Log
SET `Status` = 'Ended' WHERE Screen_Name = 'INSIGHTS - GLUE COHORTS';

END$$
DELIMITER ;
