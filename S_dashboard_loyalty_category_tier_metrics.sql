DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_loyalty_category_tier_metrics`()
BEGIN
DECLARE Cursor_Check_Var Int Default 0;
DECLARE cur_Execution_Date_ID INT(11);
DECLARE cur_Segment CHAR(20);
DECLARE cur_Category CHAR(40);

DECLARE curALL_Categories
    CURSOR For SELECT DISTINCT
    Category
FROM
    Dashboard_Loyalty_Metrics_Cat_Temp
	WHERE Category IS NOT NULL
	ORDER BY Category ASC;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET Cursor_Check_Var=1;

DELETE from log_solus where module = 'S_dashboard_loyalty_category_tier_metrics';
SET SQL_SAFE_UPDATES=0;

SELECT `Value1` into @TYSM from UI_Configuration_Global where `Config_Name` = 'FISCAL';
SELECT @TYSM;

SELECT `Value2` into @TYEM from UI_Configuration_Global where `Config_Name` = 'FISCAL';
SELECT @TYEM;

SET @LYSM = F_Month_Diff(@TYSM,12);
SET @LYEM = F_Month_Diff(@TYEM,12);

SELECT @LYSM,@LYEM;

SET @LLYSM = F_Month_Diff(@LYSM,12);
SET @LLYEM = F_Month_Diff(@LYEM,12);

SELECT @LLYSM,@LLYEM;

SET @Start_Date = date_format(date_sub(concat(date_format(current_date(),"%Y%m"),'01'), INTERVAL 14 MONTH),"%Y%m");
SET @End_Date = date_format(current_date(),"%Y%m");

SET @TM = (SELECT date_format(current_date(),"%Y%m"));
SET @LM = F_Month_Diff(@TM,1);
SET @SMLY = F_Month_Diff(@TM,12);
SET @TY=substr(@TM,1,4);
SET @LY=substr(@SMLY,1,4);
SET @LLY = @LY - 1;

SELECT @TM,@LM,@SMLY,@TY,@LY,@LLY;
	
DROP TABLE IF EXISTS Dashboard_Loyalty_Category_Temp;
  CREATE TABLE IF NOT EXISTS Dashboard_Loyalty_Category_Temp ( 
	`Tier` varchar(50),
	`Region` varchar(20),
	`Segment` varchar(128),
	`Category` varchar(128),
	`Bill_Year_Month` int(11),
    `Membership_Status` varchar(3),
	`Member_desc` varchar(20),
	`Measure` varchar(50),
	`CategoryPercent` decimal(10,2)
  ) ENGINE = INNODB DEFAULT CHARACTER SET=LATIN1; 
 
  ALTER TABLE Dashboard_Loyalty_Category_Temp    
    ADD INDEX (Segment), 
    ADD INDEX (Tier),
    ADD INDEX (Measure),
	ADD INDEX (Category),
	ADD INDEX (Bill_Year_Month),
    ADD INDEX (Membership_Status),
	ADD INDEX (Region);

INSERT INTO log_solus(module, msg) values ('S_dashboard_loyalty_category_tier_metrics', 'Loading segment wise data');

SET Cursor_Check_Var = FALSE;
OPEN curALL_Categories;
LOOP_ALL_DATES: LOOP

	FETCH curALL_Categories into cur_Category;
	IF Cursor_Check_Var THEN
	   LEAVE LOOP_ALL_DATES;
	END IF;

		Select cur_Category,now();

INSERT INTO Dashboard_Loyalty_Category_Temp
(
Tier,
Category,
Measure,
Bill_Year_Month,
CategoryPercent
)		
SELECT
Tier,
Category,
"BYMONTH" AS Measure,
Bill_Year_Month,
CategoryPercent
FROM
(
SELECT C.Tier, C.Category, C.Bill_Year_Month, Bills AS CategoryPercent FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills', A.Tier, A.Category, A.Bill_Year_Month FROM
(
select Tier, Category, Bill_Year_Month, count(distinct Customer_Id) AS c1
from Dashboard_Loyalty_Metrics_Cat_Temp
where Tier is not NULL and Tier <> "NOT-CAPTUR"
and Category = cur_Category
and Bill_Year_Month BETWEEN @Start_Date AND @End_Date
group by 1,2,3
)A
,
(
select Tier, Bill_Year_Month, count(distinct Customer_Id) AS COUNT2
from Dashboard_Loyalty_Metrics_Cat_Temp
where Bill_Year_Month BETWEEN @Start_Date AND @End_Date
group by 1,2
)B
WHERE A.Tier = B.Tier
AND A.Bill_Year_Month = B.Bill_Year_Month
) C
GROUP BY C.Tier, C.Bill_Year_Month
) AS T;

INSERT INTO Dashboard_Loyalty_Category_Temp
(
Tier,
Category,
Measure,
CategoryPercent
)
SELECT
Tier,
Category,
"TY" AS Measure,
CategoryPercent
FROM
(
SELECT C.Tier, C.Category, Bills AS CategoryPercent FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills', A.Tier, A.Category FROM
(
select Tier, Category, count(distinct Customer_Id) AS c1
from Dashboard_Loyalty_Metrics_Cat_Temp
where Tier is not NULL and Tier <> "NOT-CAPTUR"
and Category = cur_Category
and Bill_Year_Month BETWEEN @TYSM AND @TYEM
group by 1,2
)A
,
(
select Tier, count(distinct Customer_Id) AS COUNT2
from Dashboard_Loyalty_Metrics_Cat_Temp
where Bill_Year_Month BETWEEN @TYSM AND @TYEM
group by Tier
)B
WHERE A.Tier = B.Tier
) C
GROUP BY C.Tier
) AS T;

INSERT INTO Dashboard_Loyalty_Category_Temp
(
Tier,
Category,
Measure,
CategoryPercent
)
SELECT
Tier,
Category,
"LY" AS Measure,
CategoryPercent
FROM
(
SELECT C.Tier, C.Category, Bills AS CategoryPercent FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills', A.Tier, A.Category FROM
(
select Tier, Category, count(distinct Customer_Id) AS c1
from Dashboard_Loyalty_Metrics_Cat_Temp
where Tier is not NULL and Tier <> "NOT-CAPTUR"
and Category = cur_Category
and Bill_Year_Month BETWEEN @LYSM AND @LYEM
group by 1,2
)A
,
(
select Tier, count(distinct Customer_Id) AS COUNT2
from Dashboard_Loyalty_Metrics_Cat_Temp
where Bill_Year_Month BETWEEN @LYSM AND @LYEM
group by Tier
)B
WHERE A.Tier = B.Tier
) C
GROUP BY C.Tier
) AS T;

INSERT INTO Dashboard_Loyalty_Category_Temp
(
Tier,
Category,
Measure,
CategoryPercent
)
WITH GROUP1 AS
                            (
							SELECT Tier, Category,
                             CategoryPercent as "Value1"
							from Dashboard_Loyalty_Category_Temp
                            where Measure = "TY"
                            and Category = cur_Category
							group by 1
                            order by 1 ASC
                            )
                             ,
                             GROUP2 AS
							(
                             SELECT Tier, Category,
                             CategoryPercent as "Value2"
							from Dashboard_Loyalty_Category_Temp
                            where Measure = "LY"
                            and Category = cur_Category
							group by 1
                            order by 1 ASC
                            )
                            SELECT A.Tier, A.Category, "TYLYCHANGE" AS Measure, Value1-Value2 AS CategoryPercent
                            FROM GROUP1 A
                            JOIN GROUP2 B
                            ON A.Tier=B.Tier
							AND A.Category=B.Category;

INSERT INTO Dashboard_Loyalty_Category_Temp
(
Tier,
Category,
Measure,
CategoryPercent
)
SELECT
Tier,
Category,
"L12M" AS Measure,
CategoryPercent
FROM
(
SELECT C.Tier, C.Category, Bills AS CategoryPercent FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills', A.Tier, A.Category FROM
(
select Tier, Category, count(distinct Customer_Id) AS c1
from Dashboard_Loyalty_Metrics_Cat_Temp
where Tier is not NULL and Tier <> "NOT-CAPTUR"
and Category = cur_Category
and Bill_Year_Month BETWEEN @SMLY AND @TM
group by 1,2
)A
,
(
select Tier, count(distinct Customer_Id) AS COUNT2
from Dashboard_Loyalty_Metrics_Cat_Temp
where Bill_Year_Month BETWEEN @SMLY AND @TM
group by Tier
)B
WHERE A.Tier = B.Tier
) C
GROUP BY C.Tier
) AS T;

INSERT INTO Dashboard_Loyalty_Category_Temp
(
Tier,
Category,
Measure,
CategoryPercent
)
SELECT
Tier,
Category,
"L1224M" AS Measure,
CategoryPercent
FROM
(
SELECT C.Tier, C.Category, Bills AS CategoryPercent FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills', A.Tier, A.Category FROM
(
select Tier, Category, count(distinct Customer_Id) AS c1
from Dashboard_Loyalty_Metrics_Cat_Temp
where Tier is not NULL and Tier <> "NOT-CAPTUR"
and Category = cur_Category
and substr(Bill_Year_Month,1,4) BETWEEN @LLY AND @LY
group by 1,2
)A
,
(
select Tier, count(distinct Customer_Id) AS COUNT2
from Dashboard_Loyalty_Metrics_Cat_Temp
where substr(Bill_Year_Month,1,4) BETWEEN @LLY AND @LY
group by Tier
)B
WHERE A.Tier = B.Tier
) C
GROUP BY C.Tier
) AS T;

INSERT INTO Dashboard_Loyalty_Category_Temp
(
Tier,
Category,
Measure,
CategoryPercent
)
WITH GROUP1 AS
                            (
							SELECT Tier, Category,
                             CategoryPercent as "Value1"
							from Dashboard_Loyalty_Category_Temp
                            where Measure = "L12M"
                            and Category = cur_Category
							group by 1
                            order by 1 ASC
                            )
                             ,
                             GROUP2 AS
							(
                             SELECT Tier, Category,
                             CategoryPercent as "Value2"
							from Dashboard_Loyalty_Category_Temp
                            where Measure = "L1224M"
                            and Category = cur_Category
							group by 1
                            order by 1 ASC
                            )
                            SELECT A.Tier, A.Category, "L1224MCHANGE" AS Measure, Value1-Value2 AS CategoryPercent
                            FROM GROUP1 A
                            JOIN GROUP2 B
                            ON A.Tier=B.Tier
							AND A.Category=B.Category;
							
INSERT INTO Dashboard_Loyalty_Category_Temp
(
Segment,
Category,
Measure,
Bill_Year_Month,
CategoryPercent
)		
SELECT
Segment,
Category,
"BYMONTH" AS Measure,
Bill_Year_Month,
CategoryPercent
FROM
(
SELECT C.Segment, C.Category, C.Bill_Year_Month, Bills AS CategoryPercent FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills', A.Segment, A.Category, A.Bill_Year_Month FROM
(
select Segment, Category, Bill_Year_Month, count(distinct Customer_Id) AS c1
from Dashboard_Loyalty_Metrics_Cat_Temp
where Category = cur_Category
and Bill_Year_Month BETWEEN @Start_Date AND @End_Date
group by 1,2,3
)A
,
(
select Segment, Bill_Year_Month, count(distinct Customer_Id) AS COUNT2
from Dashboard_Loyalty_Metrics_Cat_Temp
where Bill_Year_Month BETWEEN @Start_Date AND @End_Date
group by 1,2
)B
WHERE A.Segment = B.Segment
AND A.Bill_Year_Month = B.Bill_Year_Month
) C
GROUP BY C.Segment, C.Bill_Year_Month
) AS T;

INSERT INTO Dashboard_Loyalty_Category_Temp
(
Segment,
Category,
Measure,
CategoryPercent
)
SELECT
Segment,
Category,
"TY" AS Measure,
CategoryPercent
FROM
(
SELECT C.Segment, C.Category, Bills AS CategoryPercent FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills', A.Segment, A.Category FROM
(
select Segment, Category, count(distinct Customer_Id) AS c1
from Dashboard_Loyalty_Metrics_Cat_Temp
where Category = cur_Category
and Bill_Year_Month BETWEEN @TYSM AND @TYEM
group by 1,2
)A
,
(
select Segment, count(distinct Customer_Id) AS COUNT2
from Dashboard_Loyalty_Metrics_Cat_Temp
where Bill_Year_Month BETWEEN @TYSM AND @TYEM
group by Segment
)B
WHERE A.Segment = B.Segment
) C
GROUP BY C.Segment
) AS T;

INSERT INTO Dashboard_Loyalty_Category_Temp
(
Segment,
Category,
Measure,
CategoryPercent
)
SELECT
Segment,
Category,
"LY" AS Measure,
CategoryPercent
FROM
(
SELECT C.Segment, C.Category, Bills AS CategoryPercent FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills', A.Segment, A.Category FROM
(
select Segment, Category, count(distinct Customer_Id) AS c1
from Dashboard_Loyalty_Metrics_Cat_Temp
where Category = cur_Category
and Bill_Year_Month BETWEEN @LYSM AND @LYEM
group by 1,2
)A
,
(
select Segment, count(distinct Customer_Id) AS COUNT2
from Dashboard_Loyalty_Metrics_Cat_Temp
where Bill_Year_Month BETWEEN @LYSM AND @LYEM
group by Segment
)B
WHERE A.Segment = B.Segment
) C
GROUP BY C.Segment
) AS T;

INSERT INTO Dashboard_Loyalty_Category_Temp
(
Segment,
Category,
Measure,
CategoryPercent
)
WITH GROUP1 AS
                            (
							SELECT Segment, Category,
                             CategoryPercent as "Value1"
							from Dashboard_Loyalty_Category_Temp
                            where Measure = "TY"
                            and Category = cur_Category
							group by 1
                            order by 1 ASC
                            )
                             ,
                             GROUP2 AS
							(
                             SELECT Segment, Category,
                             CategoryPercent as "Value2"
							from Dashboard_Loyalty_Category_Temp
                            where Measure = "LY"
                            and Category = cur_Category
							group by 1
                            order by 1 ASC
                            )
                            SELECT A.Segment, A.Category, "TYLYCHANGE" AS Measure, Value1-Value2 AS CategoryPercent
                            FROM GROUP1 A
                            JOIN GROUP2 B
                            ON A.Segment=B.Segment
							AND A.Category=B.Category;

INSERT INTO Dashboard_Loyalty_Category_Temp
(
Segment,
Category,
Measure,
CategoryPercent
)
SELECT
Segment,
Category,
"L12M" AS Measure,
CategoryPercent
FROM
(
SELECT C.Segment, C.Category, Bills AS CategoryPercent FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills', A.Segment, A.Category FROM
(
select Segment, Category, count(distinct Customer_Id) AS c1
from Dashboard_Loyalty_Metrics_Cat_Temp
where Category = cur_Category
and Bill_Year_Month BETWEEN @SMLY AND @TM
group by 1,2
)A
,
(
select Segment, count(distinct Customer_Id) AS COUNT2
from Dashboard_Loyalty_Metrics_Cat_Temp
where Bill_Year_Month BETWEEN @SMLY AND @TM
group by Segment
)B
WHERE A.Segment = B.Segment
) C
GROUP BY C.Segment
) AS T;

INSERT INTO Dashboard_Loyalty_Category_Temp
(
Segment,
Category,
Measure,
CategoryPercent
)
SELECT
Segment,
Category,
"L1224M" AS Measure,
CategoryPercent
FROM
(
SELECT C.Segment, C.Category, Bills AS CategoryPercent FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills', A.Segment, A.Category FROM
(
select Segment, Category, count(distinct Customer_Id) AS c1
from Dashboard_Loyalty_Metrics_Cat_Temp
where Category = cur_Category
and substr(Bill_Year_Month,1,4) BETWEEN @LLY AND @LY
group by 1,2
)A
,
(
select Segment, count(distinct Customer_Id) AS COUNT2
from Dashboard_Loyalty_Metrics_Cat_Temp
where substr(Bill_Year_Month,1,4) BETWEEN @LLY AND @LY
group by Segment
)B
WHERE A.Segment = B.Segment
) C
GROUP BY C.Segment
) AS T;

INSERT INTO Dashboard_Loyalty_Category_Temp
(
Segment,
Category,
Measure,
CategoryPercent
)
WITH GROUP1 AS
                            (
							SELECT Segment, Category,
                             CategoryPercent as "Value1"
							from Dashboard_Loyalty_Category_Temp
                            where Measure = "L12M"
                            and Category = cur_Category
							group by 1
                            order by 1 ASC
                            )
                             ,
                             GROUP2 AS
							(
                             SELECT Segment, Category,
                             CategoryPercent as "Value2"
							from Dashboard_Loyalty_Category_Temp
                            where Measure = "L1224M"
                            and Category = cur_Category
							group by 1
                            order by 1 ASC
                            )
                            SELECT A.Segment, A.Category, "L1224MCHANGE" AS Measure, Value1-Value2 AS CategoryPercent
                            FROM GROUP1 A
                            JOIN GROUP2 B
                            ON A.Segment=B.Segment
							AND A.Category=B.Category;
							
INSERT INTO Dashboard_Loyalty_Category_Temp
(
Membership_Status,
Category,
Measure,
Bill_Year_Month,
CategoryPercent
)		
SELECT
Membership_Status,
Category,
"BYMONTH" AS Measure,
Bill_Year_Month,
CategoryPercent
FROM
(
SELECT C.Membership_Status, C.Category, C.Bill_Year_Month, Bills AS CategoryPercent FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills', A.Membership_Status, A.Category, A.Bill_Year_Month FROM
(
select Membership_Status, Category, Bill_Year_Month, count(distinct Customer_Id) AS c1
from Dashboard_Loyalty_Metrics_Cat_Temp
where Category = cur_Category
and Bill_Year_Month BETWEEN @Start_Date AND @End_Date
group by 1,2,3
)A
,
(
select Membership_Status, Bill_Year_Month, count(distinct Customer_Id) AS COUNT2
from Dashboard_Loyalty_Metrics_Cat_Temp
where Bill_Year_Month BETWEEN @Start_Date AND @End_Date
group by 1,2
)B
WHERE A.Membership_Status = B.Membership_Status
AND A.Bill_Year_Month = B.Bill_Year_Month
) C
GROUP BY C.Membership_Status, C.Bill_Year_Month
) AS T;

INSERT INTO Dashboard_Loyalty_Category_Temp
(
Membership_Status,
Category,
Measure,
CategoryPercent
)
SELECT
Membership_Status,
Category,
"TY" AS Measure,
CategoryPercent
FROM
(
SELECT C.Membership_Status, C.Category, Bills AS CategoryPercent FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills', A.Membership_Status, A.Category FROM
(
select Membership_Status, Category, count(distinct Customer_Id) AS c1
from Dashboard_Loyalty_Metrics_Cat_Temp
where Category = cur_Category
and Bill_Year_Month BETWEEN @TYSM AND @TYEM
group by 1,2
)A
,
(
select Membership_Status, count(distinct Customer_Id) AS COUNT2
from Dashboard_Loyalty_Metrics_Cat_Temp
where Bill_Year_Month BETWEEN @TYSM AND @TYEM
group by Membership_Status
)B
WHERE A.Membership_Status = B.Membership_Status
) C
GROUP BY C.Membership_Status
) AS T;

INSERT INTO Dashboard_Loyalty_Category_Temp
(
Membership_Status,
Category,
Measure,
CategoryPercent
)
SELECT
Membership_Status,
Category,
"LY" AS Measure,
CategoryPercent
FROM
(
SELECT C.Membership_Status, C.Category, Bills AS CategoryPercent FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills', A.Membership_Status, A.Category FROM
(
select Membership_Status, Category, count(distinct Customer_Id) AS c1
from Dashboard_Loyalty_Metrics_Cat_Temp
where Category = cur_Category
and Bill_Year_Month BETWEEN @LYSM AND @LYEM
group by 1,2
)A
,
(
select Membership_Status, count(distinct Customer_Id) AS COUNT2
from Dashboard_Loyalty_Metrics_Cat_Temp
where Bill_Year_Month BETWEEN @LYSM AND @LYEM
group by Membership_Status
)B
WHERE A.Membership_Status = B.Membership_Status
) C
GROUP BY C.Membership_Status
) AS T;

INSERT INTO Dashboard_Loyalty_Category_Temp
(
Membership_Status,
Category,
Measure,
CategoryPercent
)
WITH GROUP1 AS
                            (
							SELECT Membership_Status, Category,
                             CategoryPercent as "Value1"
							from Dashboard_Loyalty_Category_Temp
                            where Measure = "TY"
                            and Category = cur_Category
							group by 1
                            order by 1 ASC
                            )
                             ,
                             GROUP2 AS
							(
                             SELECT Membership_Status, Category,
                             CategoryPercent as "Value2"
							from Dashboard_Loyalty_Category_Temp
                            where Measure = "LY"
                            and Category = cur_Category
							group by 1
                            order by 1 ASC
                            )
                            SELECT A.Membership_Status, A.Category, "TYLYCHANGE" AS Measure, Value1-Value2 AS CategoryPercent
                            FROM GROUP1 A
                            JOIN GROUP2 B
                            ON A.Membership_Status=B.Membership_Status
							AND A.Category=B.Category;

INSERT INTO Dashboard_Loyalty_Category_Temp
(
Membership_Status,
Category,
Measure,
CategoryPercent
)
SELECT
Membership_Status,
Category,
"L12M" AS Measure,
CategoryPercent
FROM
(
SELECT C.Membership_Status, C.Category, Bills AS CategoryPercent FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills', A.Membership_Status, A.Category FROM
(
select Membership_Status, Category, count(distinct Customer_Id) AS c1
from Dashboard_Loyalty_Metrics_Cat_Temp
where Category = cur_Category
and Bill_Year_Month BETWEEN @SMLY AND @TM
group by 1,2
)A
,
(
select Membership_Status, count(distinct Customer_Id) AS COUNT2
from Dashboard_Loyalty_Metrics_Cat_Temp
where Bill_Year_Month BETWEEN @SMLY AND @TM
group by Membership_Status
)B
WHERE A.Membership_Status = B.Membership_Status
) C
GROUP BY C.Membership_Status
) AS T;

INSERT INTO Dashboard_Loyalty_Category_Temp
(
Membership_Status,
Category,
Measure,
CategoryPercent
)
SELECT
Membership_Status,
Category,
"L1224M" AS Measure,
CategoryPercent
FROM
(
SELECT C.Membership_Status, C.Category, Bills AS CategoryPercent FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills', A.Membership_Status, A.Category FROM
(
select Membership_Status, Category, count(distinct Customer_Id) AS c1
from Dashboard_Loyalty_Metrics_Cat_Temp
where Category = cur_Category
and substr(Bill_Year_Month,1,4) BETWEEN @LLY AND @LY
group by 1,2
)A
,
(
select Membership_Status, count(distinct Customer_Id) AS COUNT2
from Dashboard_Loyalty_Metrics_Cat_Temp
where substr(Bill_Year_Month,1,4) BETWEEN @LLY AND @LY
group by Membership_Status
)B
WHERE A.Membership_Status = B.Membership_Status
) C
GROUP BY C.Membership_Status
) AS T;

INSERT INTO Dashboard_Loyalty_Category_Temp
(
Membership_Status,
Category,
Measure,
CategoryPercent
)
WITH GROUP1 AS
                            (
							SELECT Membership_Status, Category,
                             CategoryPercent as "Value1"
							from Dashboard_Loyalty_Category_Temp
                            where Measure = "L12M"
                            and Category = cur_Category
							group by 1
                            order by 1 ASC
                            )
                             ,
                             GROUP2 AS
							(
                             SELECT Membership_Status, Category,
                             CategoryPercent as "Value2"
							from Dashboard_Loyalty_Category_Temp
                            where Measure = "L1224M"
                            and Category = cur_Category
							group by 1
                            order by 1 ASC
                            )
                            SELECT A.Membership_Status, A.Category, "L1224MCHANGE" AS Measure, Value1-Value2 AS CategoryPercent
                            FROM GROUP1 A
                            JOIN GROUP2 B
                            ON A.Membership_Status=B.Membership_Status
							AND A.Category=B.Category;

END LOOP LOOP_ALL_DATES;

UPDATE Dashboard_Loyalty_Category_Temp
SET Member_Desc = 'MEM' WHERE Membership_Status = 'Y';

UPDATE Dashboard_Loyalty_Category_Temp
SET Member_Desc = 'NON MEM' WHERE Membership_Status = 'N';

END$$
DELIMITER ;
