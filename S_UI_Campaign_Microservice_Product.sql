DELIMITER $$
CREATE or replace PROCEDURE `S_UI_Campaign_Microservice_Product`(IN in_request_json json, OUT out_result_json json)
BEGIN

    -- User Information

    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 

    -- Target Audience	
    declare AGGREGATION_1 varchar(400);
    declare AGGREGATION_2 varchar(400);
	declare AGGREGATION_3 varchar(400);
    declare AGGREGATION_4 varchar(400);
	declare AGGREGATION_5 varchar(400);
	declare AGGREGATION_6 varchar(400);
	 
   -- User Information
	Set in_request_json = Replace(in_request_json,'\n','\\n');
	Set in_request_json = Replace(in_request_json,'\\n','');
    Set in_request_json = Replace(in_request_json,'\\','');
   
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));

    -- Target Audience

	SET AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET AGGREGATION_2= json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET AGGREGATION_3= json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET AGGREGATION_4= json_unquote(json_extract(in_request_json,"$.AGGREGATION_4"));
	SET AGGREGATION_5= json_unquote(json_extract(in_request_json,"$.AGGREGATION_5"));
	SET AGGREGATION_6= json_unquote(json_extract(in_request_json,"$.AGGREGATION_6"));

    SET @temp_result = Null;
   

   
   IF AGGREGATION_1 ="DISPLAY_DATA"
	THEN	
			SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Product_Name",Product_Name,"Product_Key",Product_Key,"Product_Group",Product_Group,"Validity_Start_Date",Validity_Start_Date,"Validity_End_Date",Validity_End_Date,"IsActive",IsActive,"Modified_Date",Modified_Date) ORDER BY Validity_Start_Date DESC )),"]")  into @temp_result
											FROM CLM_Microservice_Product_Config
														;
									   
									   ;') ;
					      
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement; 
						
			If @temp_result is null
			   then set @temp_result="[]";
			End if;
	
	END IF;
   
   
   
   IF AGGREGATION_1 ="SEARCH_DATA"
	THEN	
	
			IF AGGREGATION_2 ='' OR AGGREGATION_2 IS NULL
				THEN
					
					SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Product_Key",Product_Key,"Product_Name",Product_Name)limit 30 )),"]")  into @temp_result
											FROM CDM_Product_Master where Product_Key is not null and Product_Key not in (Select Product_Key from CLM_Microservice_Product_Config  );
										
									   ') ;
					      
					SELECT @Query_String;
					PREPARE statement from @Query_String;
					Execute statement;
					Deallocate PREPARE statement; 

			END IF;	
			IF AGGREGATION_2 !='' OR AGGREGATION_2 IS NOT NULL
				THEN
					
					SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Product_Key",Product_Key,"Product_Name",Product_Name) )),"]")  into @temp_result
											FROM CDM_Product_Master where Product_Key is not null  and Product_Key not in (Select Product_Key from CLM_Microservice_Product_Config  )
											AND Product_Name LIKE "%',AGGREGATION_2,'%" ;
									   
									   ;') ;
					      
					SELECT @Query_String;
					PREPARE statement from @Query_String;
					Execute statement;
					Deallocate PREPARE statement; 

			END IF;
					      
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement; 
						
			If @temp_result is null
			   then set @temp_result="[]";
			End if;
	
	END IF;
   
   
   IF AGGREGATION_1 ="DELETE_DATA" AND (AGGREGATION_2 IS NOT NULL OR AGGREGATION_2 !='')
	THEN	
			SET @Query_String = concat('DELETE FROM CLM_Microservice_Product_Config
			WHERE Product_Key="',AGGREGATION_2,' ";') ;
					      
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement; 
			
			Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","SUCCESSFUL") )),"]") into @temp_result; ') ;
   
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement; 


							
			If @temp_result is null
			   then set @temp_result="[]";
			End if;
	
	END IF;
	
	   
   IF AGGREGATION_1 ="ADD_DATA" AND (AGGREGATION_2 !='') and ( AGGREGATION_6 !='')
	THEN	
			Select AGGREGATION_2,AGGREGATION_6;
			SET @Query_String = concat('INSERT IGNORE INTO CLM_Microservice_Product_Config(Product_Key,Product_Group,Validity_Start_Date,Validity_End_Date,IsActive,Product_Name)
			VALUE("',AGGREGATION_2,'","',AGGREGATION_3,'","',AGGREGATION_4,'","',AGGREGATION_5,'","Y","',AGGREGATION_6,'") ;') ;
					      
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement; 
			
			SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","SAVED") )),"]")  into @temp_result
											
														;
									   
									   ;') ;
					      
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement; 

							
			If @temp_result is null
			   then set @temp_result="[]";
			End if;
	
	END IF;
   
   
				
	if @temp_result is null
		Then
								
							Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Invalid Parameters") )),"]") into @temp_result; ') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
						
	End if;
							
    SET out_result_json = @temp_result;

 END$$
DELIMITER ;
   