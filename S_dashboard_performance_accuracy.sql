DELIMITER $$
CREATE or replace PROCEDURE `S_dashboard_performance_accuracy`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    
	SET @FY_SATRT_DATE = date_format(date_sub(concat(IN_FILTER_CURRENTMONTH,'01'), INTERVAL @Interval_month MONTH),"%Y%m");
	SET @FY_END_DATE = IN_FILTER_CURRENTMONTH;
    
    SELECT @FY_SATRT_DATE,@FY_END_DATE;
    SET @incr_Rev =' round(SUM(Incremental_Revenue),0) ';
    SET @filter_cond=concat(" YM between '",@FY_SATRT_DATE,"' and '",@FY_END_DATE,"'");
    
    IF IN_MEASURE = 'PRECISION'
    THEN
		SET @Query_Measure = 'Positive';
	ELSE
		SET @Query_Measure = 'Negative';
	END IF;
    
    IF IN_AGGREGATION_2 = 'ALGORITHM'
    THEN
		SET IN_AGGREGATION_2 = 'ALGO';
	END IF;
    
    IF IN_AGGREGATION_2 = 'GRANULARITY'
    THEN
    select replace(`Value`,'_LOV_Id','') into @attr1 from CDM_Product_Genome_Config where `Name` = "ProductGenome_Attr1";
	select replace(`Value`,'_LOV_Id','') into @attr2 from CDM_Product_Genome_Config where `Name` = "ProductGenome_Attr2";
	select replace(`Value`,'_LOV_Id','') into @attr3 from CDM_Product_Genome_Config where `Name` = "ProductGenome_Attr3";
	select replace(`Value`,'_LOV_Id','') into @attr4 from CDM_Product_Genome_Config where `Name` = "ProductGenome_Attr4";
    SET @view_stmt=concat('CREATE OR REPLACE VIEW V_Dahboard_Accuracy as
    (Select "',@attr1,'" as X_Data,ifnull(SUM(RecoAttr1_True_Positive)/(SUM(RecoAttr1_True_Positive)+SUM(RecoAttr1_False_',@Query_Measure,')),0)*100 as `Value` from CLM_Hard_Response where MonthYear = ',IN_FILTER_CURRENTMONTH,')
    UNION
    (Select "',@attr2,'" as X_Data,ifnull(SUM(RecoAttr2_True_Positive)/(SUM(RecoAttr2_True_Positive)+SUM(RecoAttr2_False_',@Query_Measure,')),0)*100 as `Value` from CLM_Hard_Response where MonthYear = ',IN_FILTER_CURRENTMONTH,')
    UNION
    (Select "',@attr3,'" as X_Data,ifnull(SUM(RecoAttr3_True_Positive)/(SUM(RecoAttr3_True_Positive)+SUM(RecoAttr3_False_',@Query_Measure,')),0)*100 as `Value` from CLM_Hard_Response where MonthYear = ',IN_FILTER_CURRENTMONTH,')
    UNION
    (Select "',@attr4,'" as X_Data,ifnull(SUM(RecoAttr4_True_Positive)/(SUM(RecoAttr4_True_Positive)+SUM(RecoAttr4_False_',@Query_Measure,')) ,0)*100 as `Value` from CLM_Hard_Response where MonthYear = ',IN_FILTER_CURRENTMONTH,')
    ');
    
    
    SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("num",round(`Value`,2),"over",`X_Data`) )),"]")into @temp_result from V_Dahboard_Accuracy;' ;
    END IF;
    IF IN_AGGREGATION_2 in ('SEGMENT','ALGO')
    THEN
		SET @view_stmt=concat('CREATE OR REPLACE VIEW V_Dahboard_Accuracy as
    Select ',IN_AGGREGATION_2,' as X_Data,ifnull(SUM(True_Positive)/(SUM(True_Positive)+SUM(False_',@Query_Measure,')),0)*100 as `Value` from CLM_Hard_Response where MonthYear = ',IN_FILTER_CURRENTMONTH,' group by 1
    ');
     SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Value",round(`Value`,2),"Year",`X_Data`) )),"]")into @temp_result from V_Dahboard_Accuracy;' ;
	END IF;
    
	IF IN_MEASURE <> "DOWNLOAD"
    THEN
	select @view_stmt;
	PREPARE statement from @view_stmt;
	Execute statement;
	Deallocate PREPARE statement;
    
	select @Query_String;
	PREPARE statement from @Query_String;
	Execute statement;
	Deallocate PREPARE statement; 
    SET out_result_json = @temp_result;
    END IF;
    
END$$
DELIMITER ;
