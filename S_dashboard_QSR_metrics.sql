DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_QSR_metrics`(IN vBatchSize BIGINT)
BEGIN
DECLARE vStart_Cnt BIGINT DEFAULT 0;
DECLARE vEnd_Cnt BIGINT DEFAULT 0;
DECLARE vRec_Cnt BIGINT DEFAULT 0;
DECLARE vStart_Date BIGINT DEFAULT 0;
DECLARE vEnd_Date BIGINT DEFAULT 0;
DECLARE vCurYYYYMM int(6);
DECLARE vCheckEndOfCursor int Default 0;

DECLARE curALL_MONTHS CURSOR FOR 
SELECT DISTINCT 
	Bill_Year_Month
FROM
	CDM_Bill_Details
WHERE Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt
AND Bill_Year_Month BETWEEN vStart_Date AND vEnd_Date
ORDER BY Bill_Year_Month ASC;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET vCheckEndOfCursor=1;
SET SQL_SAFE_UPDATES=0;

SET vStart_Date = date_format(date_sub(concat(date_format(current_date(),"%Y%m"),'01'), INTERVAL 23 MONTH),"%Y%m");
SET vEnd_Date = date_format(current_date(),"%Y%m");

SELECT vStart_Date, vEnd_Date;

DROP TABLE IF EXISTS Dashboard_Insights_QSR_Customers_Temp;
  CREATE TABLE IF NOT EXISTS Dashboard_Insights_QSR_Customers_Temp ( 
	Customer_Id bigint(20) NOT NULL,
	Bill_Year_Month int(11) NOT NULL,
	Bill_Date date NOT NULL,
    Bill_Day_Week tinyint(4),
    FTD date,
	`Bill_Header_Id` bigint(20) NOT NULL,
	`Sale_Net_Val` decimal(15,2) NOT NULL,
	`Sale_Qty` decimal(15,2) NOT NULL,
	`Region` varchar(128),
	`Location` varchar(128),
	`Chan` varchar(128),
	`Platform` varchar(128),
    `Segment` varchar(20),
	Is_Existing_Customer int(1) DEFAULT NULL,
	Is_New_Customer int(1) DEFAULT NULL
  ) ENGINE = INNODB DEFAULT CHARACTER SET=LATIN1; 
 
  ALTER TABLE Dashboard_Insights_QSR_Customers_Temp 
    ADD INDEX (Customer_Id),     
    ADD INDEX (Bill_Year_Month),
    ADD INDEX (Bill_Date),
    ADD INDEX (Bill_Day_Week),
    ADD INDEX (Is_Existing_Customer),
	ADD INDEX (Is_New_Customer),
    ADD INDEX (Segment),
    ADD INDEX (Platform),
    ADD INDEX (Chan),
    ADD INDEX (Region)
    ;

SELECT 
    Customer_Id
INTO vRec_Cnt FROM
    CDM_Customer_Master
ORDER BY Customer_Id DESC
LIMIT 1;
	SET vStart_Cnt=0;
	SET vEnd_Cnt=0; 
LOOP_ALL_CUSTOMERS: LOOP
	SET vEnd_Cnt = vStart_Cnt + vBatchSize;
	SELECT vStart_Cnt, vEnd_Cnt, CURRENT_TIMESTAMP();
    IF vStart_Cnt  >= vRec_Cnt THEN
		LEAVE LOOP_ALL_CUSTOMERS;
	END IF;  

INSERT INTO log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_QSR_metrics',vStart_Cnt,vEnd_Cnt,'Loading data');

SET vCheckEndOfCursor=0;
	OPEN curALL_MONTHS;
	LOOP_ALL_MONTHS : LOOP
		FETCH curALL_MONTHS into vCurYYYYMM;
		IF vCheckEndOfCursor THEN    
			LEAVE LOOP_ALL_MONTHS;
		END IF;
        
        Select vCurYYYYMM,now();

 IF (select Value1 from UI_Configuration_Global where Config_Name = "Override_Insights_Revenue_From_BillHeader")="Y"
	THEN
		TRUNCATE Dashboard_CDM_Bill_Header_QSR_ForOneBatch;
		
        INSERT INTO Dashboard_CDM_Bill_Header_QSR_ForOneBatch
        (select Customer_Id,bill_date,Bill_Total_Val from CDM_Bill_Header where Customer_Id between vStart_Cnt AND vEnd_Cnt
		and Bill_Date is not null);
        
        UPDATE CDM_Bill_Details_QSR_Temp a,Dashboard_CDM_Bill_Header_ForOneBatch b
        SET a.Sale_Net_Val=b.Revenue
        where a.Customer_Id=b.Customer_Id
        and a.bill_date=b.YYYYMMDD;

    END IF;

INSERT INTO Dashboard_Insights_QSR_Customers_Temp
(
	Customer_Id,
    Bill_Header_Id,
    Bill_Date,
    Bill_Day_Week,
	Bill_Year_Month,
	Sale_Net_Val,
    Sale_Qty,
	Chan,
    Platform,
    Region,
    Location,
    FTD
)
SELECT 
B.Customer_Id, 
Bill_Header_Id,
Bill_Date,
Bill_Day_Week,
B.Bill_Year_Month,
Sale_Net_Val,
Sale_Qty,
B.Trans_Sub_Type AS Chan,
B.Trans_Type AS Platform,
S.Store_Region AS Region,
L.LOV_Value as Location,
MIN(Bill_Date) OVER (PARTITION BY Customer_Id) AS FTD
FROM CDM_Bill_Details B, CDM_LOV_Master L,  CDM_Store_Master S 
WHERE B.Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt
AND L.Lov_Id = S.Store_Location_Type_LOV_Id
AND S.Store_Id= B.Store_Id 
AND B.Bill_Year_Month = vCurYYYYMM
GROUP BY 1,2,3,4,5,6,7,8,9,10,11;


UPDATE Dashboard_Insights_QSR_Customers_Temp A, CDM_Customer_Segment B
SET A.Segment = B.CLMSegmentName
WHERE A.Customer_Id = B.Customer_Id
AND A.Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt
AND A.Bill_Year_Month = vCurYYYYMM;

UPDATE Dashboard_Insights_QSR_Customers_Temp
SET Is_Existing_Customer = CASE WHEN FTD <> Bill_Date THEN 1 ELSE 0 END
WHERE Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt
AND Bill_Year_Month = vCurYYYYMM;

UPDATE Dashboard_Insights_QSR_Customers_Temp
SET Is_New_Customer = CASE WHEN FTD = Bill_Date THEN 1 ELSE 0 END
WHERE Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt
AND Bill_Year_Month = vCurYYYYMM;

END LOOP LOOP_ALL_MONTHS;
    COMMIT;
	CLOSE curALL_MONTHS;

SET vStart_Cnt = vEnd_Cnt;
END LOOP LOOP_ALL_CUSTOMERS; 


END$$
DELIMITER ;
