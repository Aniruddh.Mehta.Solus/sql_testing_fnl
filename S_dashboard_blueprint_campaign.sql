DELIMITER $$
CREATE or replace PROCEDURE `S_dashboard_blueprint_campaign`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
    declare IN_FILTER_CURRENTMONTH text;
    declare IN_MEASURE text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
	
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
     SET IN_MEASURE=json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
    SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
   
	CASE 
    WHEN IN_SCREEN="BLUEPRINT_CAMPAIGN_LIST" and IN_MEASURE="LIST" THEN
    BEGIN
        SET @Query_String1= 'create or replace view V_Dashboard_Blueprint_Campaign_List as   SELECT 
  
         `CLM_Campaign`.`CampaignName` As `Campaign Name`,
         CASE
            WHEN `CLM_Campaign`.`CampaignState` = 1 THEN "Enabled"
            WHEN `CLM_Campaign`.`CampaignState` = 2 THEN "Draft"
            ELSE "Disabled"
        END AS `Status`,
        `CLM_CampaignTheme`.`CampaignThemeName` As `Campaign Theme`,
        `CLM_Segment`.`CLMSegmentName` As `Lifecycle Segment`,
        count(`CLM_Campaign_Events`.`CampTriggerId`) As `# Triggers`,
        `CLM_Campaign`.`CampaignResponseDays` AS `Attribution Days`,
        date_format(`CLM_Campaign`.`CampaignStartDate`,"%d-%m-%Y") AS `From`,
         date_format(`CLM_Campaign`.`CampaignEndDate`,"%d-%m-%Y") AS `To`
         FROM
        ((((`CLM_CampaignTheme`
        JOIN `CLM_Segment`)
        JOIN `CLM_Campaign`)
        JOIN `CLM_Campaign_Trigger`)
        JOIN `CLM_Campaign_Events`)
    WHERE
        `CLM_Campaign_Events`.`CampaignThemeId` = `CLM_CampaignTheme`.`CampaignThemeId`
            AND `CLM_Campaign_Events`.`CLMSegmentId` = `CLM_Segment`.`CLMSegmentId`
            AND `CLM_Campaign_Events`.`CampaignId` = `CLM_Campaign`.`CampaignId`
            AND `CLM_Campaign_Events`.`CampTriggerId` = `CLM_Campaign_Trigger`.`CampTriggerId`
            AND `CLM_Campaign`.`CampaignState` = 1
            group by `CLM_Campaign_Events`.`CampaignId`;';
               select @Query_String1;
                PREPARE statement1 from @Query_String1;
                Execute statement1;
                Deallocate PREPARE statement1; 
				
			    SET @Query_String = 'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Campaign Name",`Campaign Name`,"Status",`Status`,"Campaign Theme",`Campaign Theme`,"Lifecycle Segment",`Lifecycle Segment`,"# Triggers",`# Triggers`,"Attribution Days",`Attribution Days`,"From",`From`,"To",`To`) )),"]")into @temp_result from V_Dashboard_Blueprint_Campaign_List;' ;
			
               select @Query_String;
                PREPARE statement from @Query_String;
                Execute statement;
                Deallocate PREPARE statement; 
                select @temp_result into out_result_json;                                      
    END;
     WHEN IN_SCREEN="BLUEPRINT_CAMPAIGN_LIST" and IN_MEASURE="CAMPAIGN" THEN
    BEGIN
     SET @Query_String1= concat('create or replace view V_Dashboard_Blueprint_Campaign_Details as  SELECT  
        `CLM_Campaign`.`CampaignId` AS `CampaignId`,
        `CLM_Campaign`.`CampaignName` AS `CampaignName`,
        `CLM_Campaign`.`CampaignDesc` AS `CampaignDesc`,
        `CLM_Campaign`.`CampaignResponseDays` AS `CampaignResponseDays`,
        `CLM_Campaign`.`CampaignIsPerpetual` AS `CampaignIsPerpetual`,
        `CLM_Campaign`.`CampaignStartDate` AS `CampaignStartDate`,
       	`CLM_Campaign`.`CampaignEndDate` AS `CampaignEndDate`,         
         `CLM_Segment`.`CLMSegmentName` AS `CLMSegmentName`,        
        `CLM_CampaignTheme`.`CampaignThemeName` AS `CampaignThemeName`,
        CASE
            WHEN `CLM_Campaign`.`CampaignUsesWaterfall` = 100 THEN "CMAB_Driven"
            WHEN `CLM_Campaign`.`CampaignUsesWaterfall` = 99 THEN "Outside_Waterfall"
            ELSE "Manual_Waterfall"
        END AS `WaterfallType`,
        "5%" As `ControlGroup`
        FROM
        ((`CLM_CampaignTheme`
        JOIN `CLM_Segment`)
        JOIN `CLM_Campaign`)       
    WHERE
        `CLM_Campaign`.`CampaignThemeId` = `CLM_CampaignTheme`.`CampaignThemeId`
            AND `CLM_Campaign`.`CLMSegmentId` = `CLM_Segment`.`CLMSegmentId` 
            AND `CLM_Campaign`.`CampaignName`= "',IN_AGGREGATION_1,'";');
               select @Query_String1;
                PREPARE statement1 from @Query_String1;
                Execute statement1;
                Deallocate PREPARE statement1; 
                
		SET @Query_String2= concat('create or replace view V_Dashboard_Blueprint_Campaign_Trigger_List as  
        select `Name` As `Trigger Name`,`State` As `Status`,
        `ChannelName`  As `Channel`,`Execution_Sequence` AS `Execution Sequence`,
        `CampaignName` from `Event_Master` where `State`="Enabled" and `CampaignName`="',IN_AGGREGATION_1,'";');
               select @Query_String2;
                PREPARE statement2 from @Query_String2;
                Execute statement2;
                Deallocate PREPARE statement2; 
                
	    SET @Query_String3=concat('create or replace view V_Dashboard_Blueprint_Campaign_Trigger_Group as SELECT (json_object("CampaignName",p.`CampaignName`,"CampaignDesc",p.`CampaignDesc`,"CampaignResponseDays",p.`CampaignResponseDays`,"CampaignStartDate",
    p.`CampaignStartDate`,"CampaignEndDate",p.`CampaignEndDate`,"CampaignIsPerpetual",p.`CampaignIsPerpetual`,
    "CLMSegmentName",p.`CLMSegmentName`,"CampaignThemeName",p.`CampaignThemeName`,
    "WaterfallType",p.`WaterfallType`,"ControlGroup",p.`ControlGroup`,
     "triggers",(SELECT CONCAT("[",(GROUP_CONCAT(json_object("Trigger Name",`Trigger Name`,"Status",
    `Status`,"Channel",`Channel`,"Execution Sequence",`Execution Sequence`) )),"]") from V_Dashboard_Blueprint_Campaign_Trigger_List)
       ) ) as `Campaign_Group` from V_Dashboard_Blueprint_Campaign_Details p ;');
               select @Query_String3;
                PREPARE statement3 from @Query_String3;
                Execute statement3;
                Deallocate PREPARE statement3; 
      
    SET @Query_String='select concat("[",group_concat(`Campaign_Group`),"]")into @temp_result from V_Dashboard_Blueprint_Campaign_Trigger_Group;';
			   select @Query_String;
               PREPARE statement from @Query_String;
                Execute statement;
                Deallocate PREPARE statement; 
                select @temp_result into out_result_json;
                end; 
	WHEN IN_SCREEN="BLUEPRINT_CAMPAIGN" and IN_MEASURE="THEME" THEN
     BEGIN 
           SET @Query_String='SELECT CONCAT("[",(GROUP_CONCAT(json_object("CampaignThemeName",`CampaignThemeName`) )),"]")into @temp_result from CLM_CampaignTheme;' ;
           select @Query_String;
               PREPARE statement from @Query_String;
                Execute statement;
                Deallocate PREPARE statement; 
                select @temp_result into out_result_json;
                end; 
    WHEN IN_SCREEN="BLUEPRINT_CAMPAIGN" and IN_MEASURE="SEGMENT" THEN
     BEGIN 
       SET @Query_String='SELECT CONCAT("[",(GROUP_CONCAT(json_object("SegmentName",`CLMSegmentName`) )),"]")into @temp_result from CLM_Segment;' ;
           select @Query_String;
               PREPARE statement from @Query_String;
                Execute statement;
                Deallocate PREPARE statement; 
                select @temp_result into out_result_json;
                end; 
   
	    ELSE
			SET out_result_json=json_object("Status","Not Implemented Yet");
	    END CASE;	
END$$
DELIMITER ;
