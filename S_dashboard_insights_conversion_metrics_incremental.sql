DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_insights_conversion_metrics_incremental`(IN vBatchSize BIGINT)
BEGIN
DECLARE vStart_Cnt BIGINT DEFAULT 0;
DECLARE vEnd_Cnt BIGINT DEFAULT 0;
DECLARE vRec_Cnt BIGINT DEFAULT 0;
DECLARE Loop_Check_Var Int Default 0;
DECLARE V_Event_ID, V_Event_Execution_Date_ID int ;
DECLARE V_Event_Execution_ID Varchar(20);
DECLARE Load_Exe_ID, Exe_ID,Load_Execution_ID int;

DECLARE V_Current_Execution_Date date default DATE_SUB(Current_Date(), INTERVAL 
(select ifnull(Value,1)  from CLM_Response_Config where Name='Response_LTD') DAY);
DECLARE V_Current_Execution_Date_Id int(11) default date_format(DATE_SUB(Current_Date(),INTERVAL 
(select ifnull(Value,11) from CLM_Response_Config where Name='Response_LTD') DAY),'%Y%m%d');


DECLARE V_Current_Execution_Date_Id_End int(11) default date_format(DATE_SUB(CURRENT_DATE(),
	INTERVAL (SELECT 
			ifnull(Value,10) 
		FROM
			CLM_Response_Config
		WHERE
			name = 'Response_FTD')  DAY) ,'%Y%m%d');
            
DECLARE curALL_EVENTS CURSOR For SELECT DISTINCT
    Event_ID,
    Event_Execution_Date_ID
FROM
    Event_Attribution_History eeh
WHERE Customer_Id between vStart_Cnt and vEnd_Cnt
AND Event_Execution_Date_ID=V_Current_Execution_Date_Id
ORDER BY Created_Date DESC;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET Loop_Check_Var=1;
DECLARE CONTINUE HANDLER FOR SQLWARNING
BEGIN
	GET DIAGNOSTICS CONDITION 1 @errsqlstate=RETURNED_SQLSTATE, @errno=MYSQL_ERRNO, @errtext = MESSAGE_TEXT;
	SET @logmsg=CONCAT(ifnull(@errsqlstate,'WARNING') , ifnull(@errno,'1'), ifnull(@errtext,'WARNING MESSAGE'));
    INSERT into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
		values ('S_dashboard_insights_conversion_metrics_incremental', @logmsg ,now(),null,@errno, ifnull(Load_Exe_ID,1));
    SELECT 'S_dashboard_insights_conversion_metrics_incremental : Warning Message :' as '', @logmsg as '';
END;
DECLARE EXIT HANDLER FOR SQLEXCEPTION
BEGIN
	GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
	SET @vErrMsg = CONCAT(@errno, " (", @sqlstate, "): ", @text);
	INSERT into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
			values ('S_dashboard_insights_conversion_metrics_incremental ', @vErrMsg ,now(),null,'ERROR', ifnull(Load_Exe_ID,1));
	select now() as '', @vErrMsg as '' ;
END;
SET SQL_SAFE_UPDATES=0;
SET FOREIGN_KEY_CHECKS=0;

INSERT INTO Dashboard_Metrics_Log
(
`Screen_Name`,
`Status`
)
VALUES
('PERFORMANCE - CONVERSION',
'Started');

DELETE from log_solus where module = 'S_dashboard_insights_conversion_metrics_incremental';

SELECT `Value` into @Online_Store from M_Config where `Name` = 'Online_Store_Id';
SELECT @Online_Store;


LOOP_ALL_DATES: LOOP
	IF V_Current_Execution_Date_Id<V_Current_Execution_Date_Id_End
	THEN
		LEAVE LOOP_ALL_DATES;
	END IF;
    
    SELECT 'S_dashboard_insights_conversion_metrics_incremental :Attribute ' as '', V_Current_Execution_Date_Id as '', V_Current_Execution_Date as '' ,NOW() as '' ; 

SELECT 
    Customer_Id
INTO vRec_Cnt FROM
    Event_Attribution_History
ORDER BY Customer_Id DESC
LIMIT 1;
	SET vStart_Cnt=0;
	SET vEnd_Cnt=0; 
LOOP_ALL_CUSTOMERS: LOOP
	SET vEnd_Cnt = vStart_Cnt + vBatchSize;
	SELECT vStart_Cnt, vEnd_Cnt, CURRENT_TIMESTAMP();
    IF vStart_Cnt  >= vRec_Cnt THEN
		LEAVE LOOP_ALL_CUSTOMERS;
	END IF; 
        
	SELECT 'S_dashboard_insights_conversion_metrics_incremental :' as '', concat(V_Current_Execution_Date_Id,'_',vEnd_Cnt) as '', NOW() as '' ;
    
DELETE FROM Dashboard_Insights_Conversions 
WHERE Event_Execution_Date_ID = V_Current_Execution_Date_ID
AND Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt;

SET Loop_Check_Var=0;
		OPEN curALL_EVENTS;
		LOOP_ALL_EVENTS : Loop
			FETCH curALL_EVENTS into V_Event_ID,V_Event_Execution_Date_ID;
			IF Loop_Check_Var THEN    
				LEAVE LOOP_ALL_EVENTS;
			END IF;
    
SET @sql2= concat('INSERT IGNORE INTO Dashboard_Insights_Conversions
(
`Customer_Id`,
`Bill_Details_ID`,
`Bill_Header_ID`,
`Event_Execution_ID`,
`Event_ID`,
`Event_Execution_Date_ID`,
`EE_Date`,
`Event_Execution_Month`,
`Bill_Date`,
`Product_Id`,
`Sale_Net_Val`,
`Sale_Disc_Val`,
`Bill_Total_Val`,
`Bill_Disc`,
`Sale_Qty`
)
SELECT
`Customer_Id`,
`Bill_Details_ID`,
`Bill_Header_ID`,
`Event_Execution_ID`,
`Event_ID`,
`Event_Execution_Date_ID`,
date_format(`Event_Execution_Date_ID`,"%Y-%m-%d") AS EE_Date,
date_format(`Event_Execution_Date_ID`,"%Y%m") AS Event_Execution_Month,
`Bill_Date`,
`Product_Id`,
`Sale_Net_Val`,
`Sale_Disc_Val`,
`Bill_Total_Val`,
`Bill_Disc`,
`Sale_Qty`
FROM Event_Attribution_History
WHERE In_Control_Group IS NULL
AND Customer_Id between ',vStart_Cnt,' and ',vEnd_Cnt,'
AND Event_Execution_Date_ID= ',V_Event_Execution_Date_ID,'
AND Event_Id= ',V_Event_ID,'');
 
            select @sql2;
            prepare stmt from @sql2;
			execute stmt;
			deallocate prepare stmt;

SET @vStart_Cnt=vStart_Cnt;
			SET @vEnd_Cnt=vEnd_Cnt;
			SET @V_Current_Execution_Date_Id=V_Current_Execution_Date_Id;
			SET @V_Event_ID=V_Event_ID;
            SET @V_Event_Execution_Date_ID=V_Event_Execution_Date_ID;
            
SELECT @Num_of_Recos,@vStart_Cnt,@vEnd_Cnt,@V_Current_Execution_Date_Id,@V_Event_Execution_Date_ID,@V_Event_ID;

END LOOP LOOP_ALL_EVENTS;
		CLOSE curALL_EVENTS;
        		SET vStart_Cnt = vEnd_Cnt;
END LOOP LOOP_ALL_CUSTOMERS;

UPDATE Dashboard_Insights_Conversions A, CDM_Bill_Details B
SET A.Store_Id = B.Store_Id
WHERE A.Customer_Id = B.Customer_Id
AND A.Bill_Details_Id = B.Bill_Details_Id;

UPDATE Dashboard_Insights_Conversions A, CLM_Segment B
SET A.Segment = B.CLMSegmentName
WHERE A.Segment_Id = B.CLMSegmentId;

UPDATE Dashboard_Insights_Conversions A, Event_Master B
SET A.Theme = B.CampaignThemeName
WHERE A.Event_ID = B.ID;

UPDATE Dashboard_Insights_Conversions A, Event_Master B
SET A.Campaign = B.CampaignName
WHERE A.Event_ID = B.ID;

UPDATE Dashboard_Insights_Conversions A, 
(
select EventId, B.ChannelId, ChannelName
FROM CLM_Campaign_Events A, CLM_Channel B
WHERE A.ChannelId = B.ChannelId
) AS B
SET A.`Channel` = B.ChannelName
WHERE A.Event_ID = B.EventId
AND A.Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt;

UPDATE Dashboard_Insights_Conversions A, Event_Master_UI B
SET A.Category = Type_Of_Event
WHERE A.Event_ID = B.Event_ID
AND A.Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt;

UPDATE Dashboard_Insights_Conversions A, 
(
select EventId, B.CampTriggerId, CampTriggerName
FROM CLM_Campaign_Events A, CLM_Campaign_Trigger B
WHERE A.CampTriggerId = B.CampTriggerId
) AS B
SET A.`Trigger` = B.CampTriggerName
WHERE A.Event_ID = B.EventId
AND A.Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt;

UPDATE Dashboard_Insights_Conversions
SET Transaction_Mode = CASE WHEN Store_Id = @Online_Store THEN 'Online' ELSE 'Offline' END;

UPDATE Dashboard_Insights_Conversions A,
(
select Product_Id, LOV_Key
FROM CDM_LOV_Master A, CDM_Product_Master B
WHERE B.Cat_LOV_Id = A.LOV_id
) B
SET A.Product_Category = B.LOV_Key
WHERE A.Product_Id=B.Product_Id;

UPDATE Dashboard_Insights_Conversions A, CDM_Product_Master B
SET Product = B.Product_Name
WHERE A.Product_Id=B.Product_Id;

UPDATE Dashboard_Insights_Conversions A, CDM_Store_Master B
SET Store = B.Store_Name
WHERE A.Store_Id=B.Store_Id;

set V_Current_Execution_Date=date_sub(V_Current_Execution_Date,interval 1 day);
	set V_Current_Execution_Date_Id=date_format(V_Current_Execution_Date,'%Y%m%d');

END LOOP LOOP_ALL_DATES;

UPDATE Dashboard_Metrics_Log
SET `Status` = 'Ended' WHERE Screen_Name = 'PERFORMANCE - CONVERSION';

END$$
DELIMITER ;
