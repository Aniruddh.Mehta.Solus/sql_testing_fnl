
CREATE or Replace PROCEDURE `S_CLM_Apply_CMAB`()
begin 
DECLARE v_eventid,v_arms VARCHAR(128);
DECLARE done1,done2 INT DEFAULT FALSE;
DECLARE vStart_CntCust,vEnd_CntCust BIGINT(20);
DECLARE cur1 cursor for SELECT distinct Arms from CMAB_Output_Arms_Mapping where Event_ID in (select distinct Event_ID from Event_Execution_Stg_CMAB); 
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done1 =TRUE;
set sql_safe_updates=0;
truncate Event_Execution_CMAB_Op;
truncate CMAB_Event_Score_Distribution;
select avg(Score) into @default_score from CMAB_Output_Cust_Scores where Score>0 and Arms<>'NA';
select min(Customer_Id) into vStart_CntCust from Event_Execution_Stg_CMAB;
select max(Customer_Id) into @vRec_CntCust from Event_Execution_Stg_CMAB;

set @vBatchSizeCust=1000;
LOOP_ALL_CUSTOMER: LOOP  
SET vEnd_CntCust = vStart_CntCust + @vBatchSizeCust;
insert ignore into CMAB_Output_Cust_Scores (Customer_Id,Arms,Score) select Customer_Id,'NA',@default_score from CDM_Customer_Master cm where not exists (select Customer_ID from CMAB_Output_Cust_Scores cos where cos.Customer_ID=cm.Customer_Id and cos.Arms='NA' and cos.Customer_Id between vStart_CntCust and vEnd_CntCust)
 and cm.Customer_Id between vStart_CntCust and vEnd_CntCust;
set done1=0;
OPEN cur1;  
LOOP_ALL_ARMS: LOOP 
FETCH cur1 into v_arms; 
IF done1 THEN 
LEAVE LOOP_ALL_ARMS;  
END IF;
select ifnull(round(round(avg(Score),2)*100),0) into @vreclimit from CMAB_Output_Cust_Scores
where Customer_ID between vStart_CntCust and vEnd_CntCust
and Arms=v_arms;
if done1 then set done1=FALSE; END IF;

set @loop_var=1;
while @loop_var<=@vreclimit
do
insert into CMAB_Event_Score_Distribution(Arms) select v_arms;

set @loop_var=@loop_var+1;
end while; 
end loop LOOP_ALL_ARMS;
CLOSE cur1;  
select Arms into @varms from CMAB_Event_Score_Distribution order by rand() limit 1;

truncate temp_Event_Execution_CMAB_Op;

set @cnt_check=0;
select 1 into @cnt_check from Event_Execution_Stg_CMAB ees,Event_Master em,CMAB_Output_Arms_Mapping crm
where ees.Event_ID=em.ID
and em.ID=crm.Event_ID
and crm.Arms=@varms
and Customer_ID between vStart_CntCust and vEnd_CntCust limit 1;

if @cnt_check>0 then 
insert into temp_Event_Execution_CMAB_Op(Event_Execution_ID,Event_ID,Event_Execution_Date_ID,Customer_Id,Is_Target,Communication_Template_ID,Communication_Channel,Offer_Code,Microsite_URL,Event_Rank)
select ees.Event_Execution_ID,ees.Event_ID,ees.Event_Execution_Date_ID,ees.Customer_Id,ees.Is_Target,ees.Communication_Template_ID,ees.Communication_Channel,ees.Offer_Code,Microsite_URL,rank() over(partition by Customer_Id,Communication_Channel order by rand()) as Event_Rnk from Event_Execution_Stg_CMAB ees,Event_Master em,CMAB_Output_Arms_Mapping crm
where ees.Event_ID=em.ID
and em.ID=crm.Event_ID
and crm.Arms=@varms
and Customer_ID between vStart_CntCust and vEnd_CntCust;
end if;


set @cnt_check=0;
select 1 into @cnt_check from temp_Event_Execution_CMAB_Op tcop where (Customer_ID between vStart_CntCust and vEnd_CntCust) and not exists (select 1 from Event_Execution_CMAB_Op cop where cop.Customer_Id=tcop.Customer_Id
and cop.Communication_Channel=tcop.Communication_Channel and cop.Customer_Id between vStart_CntCust and vEnd_CntCust)
and Event_Rank=1 limit 1;

if @cnt_check>0 then 
insert ignore into Event_Execution_CMAB_Op(Event_Execution_ID,Event_ID,Event_Execution_Date_ID,Customer_Id,Is_Target,Communication_Template_ID,Communication_Channel,Offer_Code,Microsite_URL)
select Event_Execution_ID,Event_ID,Event_Execution_Date_ID,Customer_Id,Is_Target,Communication_Template_ID,Communication_Channel,Offer_Code,Microsite_URL from temp_Event_Execution_CMAB_Op tcop where (Customer_ID between vStart_CntCust and vEnd_CntCust) and not exists (select 1 from Event_Execution_CMAB_Op cop where cop.Customer_Id=tcop.Customer_Id
and cop.Communication_Channel=tcop.Communication_Channel and cop.Customer_Id between vStart_CntCust and vEnd_CntCust)
and Event_Rank=1;
end if;



truncate temp_Event_Execution_CMAB_Op;

set @cnt_check=0;
select 1 into @cnt_check from Event_Execution_Stg_CMAB ees,Event_Master em,CMAB_Output_Arms_Mapping crm
where ees.Event_ID=em.ID
and em.ID=crm.Event_ID
and Customer_ID between vStart_CntCust and vEnd_CntCust limit 1;


if @cnt_check>0 then
insert into temp_Event_Execution_CMAB_Op(Event_Execution_ID,Event_ID,Event_Execution_Date_ID,Customer_Id,Is_Target,Communication_Template_ID,Communication_Channel,Offer_Code,Microsite_URL,Event_Rank)
select ees.Event_Execution_ID,ees.Event_ID,ees.Event_Execution_Date_ID,ees.Customer_Id,ees.Is_Target,ees.Communication_Template_ID,ees.Communication_Channel,ees.Offer_Code,Microsite_URL,rank() over(partition by Customer_Id,Communication_Channel order by rand()) as Event_Rnk from Event_Execution_Stg_CMAB ees,Event_Master em,CMAB_Output_Arms_Mapping crm
where ees.Event_ID=em.ID
and em.ID=crm.Event_ID
and Customer_ID between vStart_CntCust and vEnd_CntCust;
end if;


set @cnt_check=0;
select 1 into @cnt_check from temp_Event_Execution_CMAB_Op tcop where (Customer_ID between vStart_CntCust and vEnd_CntCust) and not exists (select 1 from Event_Execution_CMAB_Op cop where cop.Customer_Id=tcop.Customer_Id
and cop.Communication_Channel=tcop.Communication_Channel and cop.Customer_Id between vStart_CntCust and vEnd_CntCust)
and Event_Rank=1 
limit 1;


if @cnt_check>0 then
insert ignore into Event_Execution_CMAB_Op(Event_Execution_ID,Event_ID,Event_Execution_Date_ID,Customer_Id,Is_Target,Communication_Template_ID,Communication_Channel,Offer_Code,Microsite_URL)
select Event_Execution_ID,Event_ID,Event_Execution_Date_ID,Customer_Id,Is_Target,Communication_Template_ID,Communication_Channel,Offer_Code,Microsite_URL from temp_Event_Execution_CMAB_Op tcop where (Customer_ID between vStart_CntCust and vEnd_CntCust) and not exists (select 1 from Event_Execution_CMAB_Op cop where cop.Customer_Id=tcop.Customer_Id
and cop.Communication_Channel=tcop.Communication_Channel and cop.Customer_Id between vStart_CntCust and vEnd_CntCust)
and Event_Rank=1;
end if;




update Event_Execution_CMAB_Op eeco,(
select Customer_Id,group_concat(distinct Arms) as CMAB_Eligible_Arms from Event_Execution_Stg_CMAB eesb,CMAB_Output_Arms_Mapping mp
where eesb.Event_Id=mp.Event_Id
and Customer_Id between vStart_CntCust and vEnd_CntCust
group by Customer_Id)L
set eeco.CMAB_Eligible_Arms=L.CMAB_Eligible_Arms
where eeco.Customer_ID=L.Customer_ID
and eeco.Customer_ID between vStart_CntCust and vEnd_CntCust;


IF vEnd_CntCust >= @vRec_CntCust
THEN
LEAVE LOOP_ALL_CUSTOMER;
END IF;  
SET vStart_CntCust = vEnd_CntCust+1;
END LOOP LOOP_ALL_CUSTOMER;
END

