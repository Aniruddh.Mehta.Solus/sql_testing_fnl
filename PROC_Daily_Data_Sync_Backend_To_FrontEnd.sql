
CREATE OR REPLACE  PROCEDURE `PROC_Daily_Data_Sync_Backend_To_FrontEnd`(
	IN vUserId VARCHAR(128), 
	IN vSchemaName VARCHAR(128), 
	INOUT vSuccess TINYINT
)
BEGIN
	DECLARE vSqlstmt TEXT;
	
	CALL CDM_Update_Process_Log('PROC_Daily_Data_Sync_Backend_To_FrontEnd', 1, 'PROC_Daily_Data_Sync_Backend_To_FrontEnd', 1);

	
	DROP TABLE IF EXISTS log_solus;
	SET vSqlstmt = CONCAT(' CREATE TABLE log_solus LIKE ',vSchemaName,'.log_solus;');
	SET @SQL_STMT = vSqlstmt;	
	PREPARE stmt1 FROM @SQL_STMT;
	EXECUTE stmt1;

	
	DROP TABLE IF EXISTS Event_Execution;
	SET vSqlstmt = CONCAT(' CREATE TABLE Event_Execution LIKE ',vSchemaName,'.Event_Execution;');
	SET @SQL_STMT = vSqlstmt;	
	PREPARE stmt1 FROM @SQL_STMT;
	EXECUTE stmt1;

	
    DROP TABLE IF EXISTS ETL_Execution_Details;
	SET vSqlstmt = CONCAT(' CREATE TABLE ETL_Execution_Details LIKE ',vSchemaName,'.ETL_Execution_Details;');
	SET @SQL_STMT = vSqlstmt;	
	PREPARE stmt1 FROM @SQL_STMT ;
	EXECUTE stmt1;
	DEALLOCATE PREPARE stmt1;

	
	DROP TABLE IF EXISTS TEMP_Customer_Communication;
	SET vSqlstmt = CONCAT('CREATE TABLE TEMP_Customer_Communication LIKE ',vSchemaName,'.TEMP_Customer_Communication;');
	SET @SQL_STMT = vSqlstmt;	
	PREPARE stmt1 FROM @SQL_STMT ;
	EXECUTE stmt1;
	DEALLOCATE PREPARE stmt1;

	
	SET vSqlstmt = CONCAT(' CREATE OR REPLACE VIEW Customer_Details_View AS SELECT * FROM ',vSchemaName,'.Customer_Details_View;');
	SET @SQL_STMT = vSqlstmt;	
	PREPARE stmt1 FROM @SQL_STMT;
	EXECUTE stmt1;
    DEALLOCATE PREPARE stmt1;

	
	SET vSqlstmt = CONCAT(' CREATE OR REPLACE VIEW ETL_Execution_Master AS SELECT * FROM ',vSchemaName,'.ETL_Execution_Master;');
	SET @SQL_STMT = vSqlstmt;	
	PREPARE stmt1 FROM @SQL_STMT;
	EXECUTE stmt1;
    DEALLOCATE PREPARE stmt1;

	
	SET vSqlstmt = CONCAT(' CREATE OR REPLACE VIEW D_Date AS SELECT * FROM ',vSchemaName,'.D_Date;');
	SET @SQL_STMT = vSqlstmt;	
	PREPARE stmt1 FROM @SQL_STMT;
	EXECUTE stmt1;
    DEALLOCATE PREPARE stmt1;

	
	SET vSqlstmt = CONCAT(' CREATE OR REPLACE VIEW Hold_Out AS SELECT * FROM ',vSchemaName,'.Hold_Out;');
	SET @SQL_STMT = vSqlstmt;	
	PREPARE stmt1 FROM @SQL_STMT;
	EXECUTE stmt1;
    DEALLOCATE PREPARE stmt1;

	
	SET vSqlstmt = CONCAT(' CREATE OR REPLACE VIEW Archived_CLM_Customer_Level_Offer_Code AS SELECT * FROM ',vSchemaName,'.Archived_CLM_Customer_Level_Offer_Code;');
	SET @SQL_STMT = vSqlstmt;	
	PREPARE stmt1 FROM @SQL_STMT;
	EXECUTE stmt1;
    DEALLOCATE PREPARE stmt1;

	
	SET vSqlstmt = CONCAT(' CREATE OR REPLACE VIEW CLM_Customer_Level_Offer_Code AS SELECT * FROM ',vSchemaName,'.CLM_Customer_Level_Offer_Code;');
	SET @SQL_STMT = vSqlstmt;	
	PREPARE stmt1 FROM @SQL_STMT;
	EXECUTE stmt1;
    DEALLOCATE PREPARE stmt1;

	
	SET vSqlstmt = CONCAT(' CREATE OR REPLACE VIEW M_Config AS SELECT * FROM ',vSchemaName,'.M_Config;');
	SET @SQL_STMT = vSqlstmt;	
	PREPARE stmt1 FROM @SQL_STMT;
	EXECUTE stmt1;
    DEALLOCATE PREPARE stmt1;

	
	SET vSqlstmt = CONCAT('CREATE OR REPLACE VIEW V_CLM_Customer_One_View AS SELECT * FROM ',vSchemaName,'.V_CLM_Customer_One_View');
	SET @SQL_STMT = vSqlstmt;	
	PREPARE stmt1 FROM @SQL_STMT;
	EXECUTE stmt1;
    DEALLOCATE PREPARE stmt1;

	
	SET vSqlstmt = CONCAT('CREATE OR REPLACE VIEW Customer_Details_View AS SELECT * FROM ',vSchemaName,'.Customer_Details_View;');
	SET @SQL_STMT = vSqlstmt;	
	PREPARE stmt1 FROM @SQL_STMT;
	EXECUTE stmt1;
    DEALLOCATE PREPARE stmt1;

	
	SET vSqlstmt = CONCAT('CREATE OR REPLACE VIEW V_CLM_Bill_Detail_One_View AS SELECT * FROM ',vSchemaName,'.V_CLM_Bill_Detail_One_View');
	SET @SQL_STMT = vSqlstmt;	
	PREPARE stmt1 FROM @SQL_STMT;
	EXECUTE stmt1;
    DEALLOCATE PREPARE stmt1;

	
	SET vSqlstmt = CONCAT('CREATE OR REPLACE VIEW Event_Execution_History AS SELECT * FROM ',vSchemaName,'.Event_Execution_History');
	SET @SQL_STMT = vSqlstmt;	
	PREPARE stmt1 FROM @SQL_STMT;
	EXECUTE stmt1;
    DEALLOCATE PREPARE stmt1;

	
	SET vSqlstmt = CONCAT(' CREATE OR REPLACE VIEW rCore_Customer_Recommendation_Variables AS SELECT * FROM ',vSchemaName,'.rCore_Customer_Recommendation');
	SET @SQL_STMT = vSqlstmt;	
	PREPARE stmt1 FROM @SQL_STMT;
	EXECUTE stmt1;
    DEALLOCATE PREPARE stmt1;

	
	BEGIN
		DROP TABLE IF EXISTS CDM_LOV_Master;
		SET vSqlstmt = CONCAT(' CREATE TABLE CDM_LOV_Master LIKE ',vSchemaName,'.CDM_LOV_Master;');
		SET @SQL_STMT = vSqlstmt;	
		PREPARE stmt1 FROM @SQL_STMT;
		EXECUTE stmt1;
		DEALLOCATE PREPARE stmt1;    
        
		SET vSqlstmt = CONCAT(' INSERT INTO CDM_LOV_Master SELECT * FROM ',vSchemaName,'.CDM_LOV_Master;');
		SET @SQL_STMT = vSqlstmt;	
		PREPARE stmt1 FROM @SQL_STMT;
		EXECUTE stmt1;
		DEALLOCATE PREPARE stmt1;    
	END;

	
	BEGIN
		DROP TABLE IF EXISTS Client_Genome_Dictionary;
		SET vSqlstmt = CONCAT(' CREATE TABLE Client_Genome_Dictionary LIKE ',vSchemaName,'.Client_Genome_Dictionary;');
		SET @SQL_STMT = vSqlstmt;	
		PREPARE stmt1 FROM @SQL_STMT;
		EXECUTE stmt1;
		DEALLOCATE PREPARE stmt1;    

		SET vSqlstmt = CONCAT(' INSERT INTO Client_Genome_Dictionary SELECT * FROM ',vSchemaName,'.Client_Genome_Dictionary;');
		SET @SQL_STMT = vSqlstmt;	
		PREPARE stmt1 FROM @SQL_STMT;
		EXECUTE stmt1;
		DEALLOCATE PREPARE stmt1;    
	END;

	CALL CDM_Update_Process_Log('PROC_Daily_Data_Sync_Backend_To_FrontEnd', 2, 'PROC_Daily_Data_Sync_Backend_To_FrontEnd', 1);

	BEGIN
		DROP TABLE IF EXISTS Segment_Master;
		SET vSqlstmt = CONCAT(' CREATE TABLE Segment_Master AS SELECT * FROM ',vSchemaName,'.Segment_Master');
		SET @SQL_STMT = vSqlstmt;	
		PREPARE stmt1 FROM @SQL_STMT;
		EXECUTE stmt1;
		DEALLOCATE PREPARE stmt1;    
	END;
	
	CALL CDM_Update_Process_Log('PROC_Daily_Data_Sync_Backend_To_FrontEnd', 3, 'PROC_Daily_Data_Sync_Backend_To_FrontEnd', 1);
	
	BEGIN
		DROP TABLE IF EXISTS Weekly_Hold_Out;
		SET vSqlstmt = CONCAT('CREATE TABLE Weekly_Hold_Out AS SELECT * FROM ',vSchemaName,'.Weekly_Hold_Out');
		SET @SQL_STMT = vSqlstmt;	
		PREPARE stmt1 FROM @SQL_STMT;
		EXECUTE stmt1;
		DEALLOCATE PREPARE stmt1;    
	END;
	
	CALL CDM_Update_Process_Log('PROC_Daily_Data_Sync_Backend_To_FrontEnd', 4, 'PROC_Daily_Data_Sync_Backend_To_FrontEnd', 1);
END

