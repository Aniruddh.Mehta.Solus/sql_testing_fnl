DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_loyalty_category`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    
    
     SET @FY_SATRT_DATE = date_format(date_sub(concat(IN_FILTER_CURRENTMONTH,'01'), INTERVAL 11 MONTH),"%Y%m");
	 SET @FY_END_DATE = IN_FILTER_CURRENTMONTH;
	 SET @TM = IN_FILTER_CURRENTMONTH;
	 SET @LM = F_Month_Diff(IN_FILTER_CURRENTMONTH,1);
	 SET @SMLY = F_Month_Diff(IN_FILTER_CURRENTMONTH,12);
	 SET @TY=substr(@TM,1,4);
	 SET @LY=substr(@SMLY,1,4);
	 SET @LLY = @LY - 1;
     SET @SMLLY = F_Month_Diff(IN_FILTER_CURRENTMONTH,24);
     SELECT @TM,@LM,@SMLY,@TY,@LY,@LLY,@SMLLY;
	
	IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_1 = 'TM' AND IN_AGGREGATION_2 = 'TIER'  
		THEN

        SELECT group_concat(distinct Category order by Category) into @distinct_rc from Dashboard_Loyalty_Category_Temp WHERE Bill_Year_Month = @TM;
		    select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
			set @loop_var=1;
			set @json_select='';
            set @top_query='';
			select @loop_var,@num_of_rc,@distinct_rc;
			WHILE @loop_var<=@num_of_rc
			do
			SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
            SET @alias_Name = @selected_rc;
            
			set @top_query=concat(@top_query,",SUM(CASE WHEN Category = '",@selected_rc,"' Then CategoryPercent else concat(0.00,'%') END) AS `",@alias_Name,"`");
				set @json_select=concat(@json_select,',"',@alias_Name,'",concat(round(`',@alias_Name,'`,2), "%")');
				set @loop_var=@loop_var+1;
			end while;
			
			SELECT @json_select,@top_query;
			
			set @view_stmt = concat("create or replace View V_Dashboard_Loyalty_Category_Table as
			(
			select Tier
			",@top_query,"
			from 
			(SELECT Tier, Category, concat(ifnull(CategoryPercent,0),'%') AS CategoryPercent
			FROM Dashboard_Loyalty_Category_Temp 
			WHERE Bill_Year_Month = ",@TM,"
            AND Tier IS NOT NULL
			GROUP BY 1,2
			)A
			GROUP BY 1)
			;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Tier",`Tier`',@json_select,') )),"]")into @temp_result from V_Dashboard_Loyalty_Category_Table;') ; 
							
END IF;

		IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_1 = 'LM' AND IN_AGGREGATION_2 = 'TIER'  
		THEN

        SELECT group_concat(distinct Category order by Category) into @distinct_rc from Dashboard_Loyalty_Category_Temp;
		    select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
			set @loop_var=1;
			set @json_select='';
            set @top_query='';
			select @loop_var,@num_of_rc,@distinct_rc;
			WHILE @loop_var<=@num_of_rc
			do
			SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
            SET @alias_Name = @selected_rc;
            
			set @top_query=concat(@top_query,",SUM(CASE WHEN Category = '",@selected_rc,"' Then CategoryPercent else concat(0.00,'%') END) AS `",@alias_Name,"`");
				set @json_select=concat(@json_select,',"',@alias_Name,'",concat(round(`',@alias_Name,'`,2), "%")');
				set @loop_var=@loop_var+1;
			end while;
			
			SELECT @json_select,@top_query;
			
			set @view_stmt = concat("create or replace View V_Dashboard_Loyalty_Category_Table as
			(
			select Tier
			",@top_query,"
			from 
			(SELECT Tier, Category, concat(ifnull(CategoryPercent,0),'%') AS CategoryPercent
			FROM Dashboard_Loyalty_Category_Temp 
			WHERE Measure = 'BYMONTH'
			AND Bill_Year_Month = ",@LM,"
            AND Tier IS NOT NULL
			GROUP BY 1,2
			)A
			GROUP BY 1)
			;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Tier",`Tier`',@json_select,') )),"]")into @temp_result from V_Dashboard_Loyalty_Category_Table;') ; 
							
END IF;

        IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_1 = 'TMLMCHANGE' AND IN_AGGREGATION_2 = 'TIER'  
		THEN

        SELECT group_concat(distinct Category order by Category) into @distinct_rc from Dashboard_Loyalty_Category_Temp;
		    select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
			set @loop_var=1;
			set @json_select='';
            set @top_query='';
			select @loop_var,@num_of_rc,@distinct_rc;
			WHILE @loop_var<=@num_of_rc
			do
			SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
            SET @alias_Name = @selected_rc;
            
			set @top_query=concat(@top_query,",SUM(CASE WHEN Category = '",@selected_rc,"' Then CategoryPercent else concat(0.00,'%') END) AS `",@alias_Name,"`");
				set @json_select=concat(@json_select,',"',@alias_Name,'",concat(round(`',@alias_Name,'`,2), "%")');
				set @loop_var=@loop_var+1;
			end while;
			
			SELECT @json_select,@top_query;
            
            set @view_stmt1 = concat("create or replace View V_Dashboard_Loyalty_Category_Table_TM as
			SELECT Tier, Category, ifnull(CategoryPercent,0) AS CategoryPercent
			FROM Dashboard_Loyalty_Category_Temp 
			WHERE Bill_Year_Month = ",@TM,"
            AND Tier IS NOT NULL
			GROUP BY 1,2
			;");
            
            select @view_stmt1;
			PREPARE statement from @view_stmt1;
			Execute statement;
			Deallocate PREPARE statement;
			
			 set @view_stmt2 = concat("create or replace View V_Dashboard_Loyalty_Category_Table_LM as
			SELECT Tier, Category, ifnull(CategoryPercent,0) AS CategoryPercent
			FROM Dashboard_Loyalty_Category_Temp 
			WHERE Bill_Year_Month = ",@LM,"
            AND Tier IS NOT NULL
			GROUP BY 1,2
			;");
            
            select @view_stmt2;
			PREPARE statement from @view_stmt2;
			Execute statement;
			Deallocate PREPARE statement;
            
            set @view_stmt = concat("create or replace View V_Dashboard_Loyalty_Category_Table as
			(
			select Tier
			",@top_query,"
			from 
			(SELECT A.Tier, A.Category, concat((A.CategoryPercent-B.CategoryPercent),'%') AS CategoryPercent
			FROM V_Dashboard_Loyalty_Category_Table_TM A
            JOIN V_Dashboard_Loyalty_Category_Table_LM B
            ON A.Tier=B.Tier
            AND A.Category=B.Category
			GROUP BY 1,2
			)A
			GROUP BY 1)
			;");
            
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Tier",`Tier`',@json_select,') )),"]")into @temp_result from V_Dashboard_Loyalty_Category_Table;') ; 
        
END IF;


	IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_2 = 'TIER' AND IN_AGGREGATION_1 NOT IN ('TM','LM','TMLMCHANGE')
		THEN

        SELECT group_concat(distinct Category order by Category) into @distinct_rc from Dashboard_Loyalty_Category_Temp;
		    select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
			set @loop_var=1;
			set @json_select='';
            set @top_query='';
			select @loop_var,@num_of_rc,@distinct_rc;
			WHILE @loop_var<=@num_of_rc
			do
			SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
            SET @alias_Name = @selected_rc;
            
			set @top_query=concat(@top_query,",sum(CASE WHEN Category = '",@selected_rc,"' Then CategoryPercent else concat(0.00,'%') END) AS `",@alias_Name,"`");
				set @json_select=concat(@json_select,',"',@alias_Name,'",concat(round(`',@alias_Name,'`,2), "%")');
				set @loop_var=@loop_var+1;
			end while;
			
			SELECT @json_select,@top_query;
            
            SET @where_cond = CASE WHEN IN_AGGREGATION_1 = 'TY' THEN "Measure = 'TY'" WHEN IN_AGGREGATION_1 = 'LY' THEN "Measure = 'LY'" WHEN IN_AGGREGATION_1 = 'TYLYCHANGE' THEN "Measure = 'TYLYCHANGE'" WHEN IN_AGGREGATION_1 = 'L12M' THEN "Measure = 'L12M' "WHEN IN_AGGREGATION_1 = 'L1224M' THEN "Measure = 'L1224M'" WHEN IN_AGGREGATION_1 = 'L1224MCHANGE' THEN "Measure = 'L1224MCHANGE' "END;
			
			set @view_stmt = concat("create or replace View V_Dashboard_Loyalty_Category_Table as
			(
			select Tier
			",@top_query,"
			from 
			(SELECT Tier, Category, concat(ifnull(CategoryPercent,0),'%') AS CategoryPercent
			FROM Dashboard_Loyalty_Category_Temp 
			WHERE ",@where_cond,"
            AND Tier IS NOT NULL
			GROUP BY 1,2
			)A
			GROUP BY 1)
			;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Tier",`Tier`',@json_select,') )),"]")into @temp_result from V_Dashboard_Loyalty_Category_Table;') ; 
							
END IF;

IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_1 = 'TM' AND IN_AGGREGATION_2 = 'SEGMENT'  
		THEN

        SELECT group_concat(distinct Category order by Category) into @distinct_rc from Dashboard_Loyalty_Category_Temp;
		    select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
			set @loop_var=1;
			set @json_select='';
            set @top_query='';
			select @loop_var,@num_of_rc,@distinct_rc;
			WHILE @loop_var<=@num_of_rc
			do
			SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
            SET @alias_Name = @selected_rc;
            
			set @top_query=concat(@top_query,",SUM(CASE WHEN Category = '",@selected_rc,"' Then CategoryPercent else concat(0.00,'%') END) AS `",@alias_Name,"`");
				set @json_select=concat(@json_select,',"',@alias_Name,'",concat(round(`',@alias_Name,'`,2), "%")');
				set @loop_var=@loop_var+1;
			end while;
			
			SELECT @json_select,@top_query;
			
			set @view_stmt = concat("create or replace View V_Dashboard_Loyalty_Category_Table as
			(
			select Segment
			",@top_query,"
			from 
			(SELECT Segment, Category, concat(ifnull(CategoryPercent,0),'%') AS CategoryPercent
			FROM Dashboard_Loyalty_Category_Temp 
			WHERE Bill_Year_Month = ",@TM,"
            AND Segment IS NOT NULL
			GROUP BY 1,2
			)A
			GROUP BY 1)
			;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Segment",`Segment`',@json_select,') )),"]")into @temp_result from V_Dashboard_Loyalty_Category_Table;') ; 
							
END IF;

		IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_1 = 'LM' AND IN_AGGREGATION_2 = 'SEGMENT'  
		THEN

        SELECT group_concat(distinct Category order by Category) into @distinct_rc from Dashboard_Loyalty_Category_Temp;
		    select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
			set @loop_var=1;
			set @json_select='';
            set @top_query='';
			select @loop_var,@num_of_rc,@distinct_rc;
			WHILE @loop_var<=@num_of_rc
			do
			SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
            SET @alias_Name = @selected_rc;
            
			set @top_query=concat(@top_query,",SUM(CASE WHEN Category = '",@selected_rc,"' Then CategoryPercent else concat(0.00,'%') END) AS `",@alias_Name,"`");
				set @json_select=concat(@json_select,',"',@alias_Name,'",concat(round(`',@alias_Name,'`,2), "%")');
				set @loop_var=@loop_var+1;
			end while;
			
			SELECT @json_select,@top_query;
			
			set @view_stmt = concat("create or replace View V_Dashboard_Loyalty_Category_Table as
			(
			select Segment
			",@top_query,"
			from 
			(SELECT Segment, Category, concat(ifnull(CategoryPercent,0),'%') AS CategoryPercent
			FROM Dashboard_Loyalty_Category_Temp 
			WHERE Measure = 'BYMONTH'
			AND Bill_Year_Month = ",@LM,"
            AND Segment IS NOT NULL
			GROUP BY 1,2
			)A
			GROUP BY 1)
			;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Segment",`Segment`',@json_select,') )),"]")into @temp_result from V_Dashboard_Loyalty_Category_Table;') ; 
							
END IF;

        IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_1 = 'TMLMCHANGE' AND IN_AGGREGATION_2 = 'SEGMENT'  
		THEN

        SELECT group_concat(distinct Category order by Category) into @distinct_rc from Dashboard_Loyalty_Category_Temp;
		    select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
			set @loop_var=1;
			set @json_select='';
            set @top_query='';
			select @loop_var,@num_of_rc,@distinct_rc;
			WHILE @loop_var<=@num_of_rc
			do
			SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
            SET @alias_Name = @selected_rc;
            
			set @top_query=concat(@top_query,",SUM(CASE WHEN Category = '",@selected_rc,"' Then CategoryPercent else concat(0.00,'%') END) AS `",@alias_Name,"`");
				set @json_select=concat(@json_select,',"',@alias_Name,'",concat(round(`',@alias_Name,'`,2), "%")');
				set @loop_var=@loop_var+1;
			end while;
			
			SELECT @json_select,@top_query;
            
            set @view_stmt1 = concat("create or replace View V_Dashboard_Loyalty_Category_Table_TM as
			SELECT Segment, Category, ifnull(CategoryPercent,0) AS CategoryPercent
			FROM Dashboard_Loyalty_Category_Temp 
			WHERE Bill_Year_Month = ",@TM,"
            AND Segment IS NOT NULL
			GROUP BY 1,2
			;");
            
            select @view_stmt1;
			PREPARE statement from @view_stmt1;
			Execute statement;
			Deallocate PREPARE statement;
			
			 set @view_stmt2 = concat("create or replace View V_Dashboard_Loyalty_Category_Table_LM as
			SELECT Segment, Category, ifnull(CategoryPercent,0) AS CategoryPercent
			FROM Dashboard_Loyalty_Category_Temp 
			WHERE Bill_Year_Month = ",@LM,"
            AND Segment IS NOT NULL
			GROUP BY 1,2
			;");
            
            select @view_stmt2;
			PREPARE statement from @view_stmt2;
			Execute statement;
			Deallocate PREPARE statement;
            
            set @view_stmt = concat("create or replace View V_Dashboard_Loyalty_Category_Table as
			(
			select Segment
			",@top_query,"
			from 
			(SELECT A.Segment, A.Category, concat((A.CategoryPercent-B.CategoryPercent),'%') AS CategoryPercent
			FROM V_Dashboard_Loyalty_Category_Table_TM A
            JOIN V_Dashboard_Loyalty_Category_Table_LM B
            ON A.Segment=B.Segment
            AND A.Category=B.Category
			GROUP BY 1,2
			)A
			GROUP BY 1)
			;");
            
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Segment",`Segment`',@json_select,') )),"]")into @temp_result from V_Dashboard_Loyalty_Category_Table;') ; 
        
END IF;

IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_2 = 'SEGMENT'  AND IN_AGGREGATION_1 NOT IN ('TM','LM','TMLMCHANGE')
		THEN

        SELECT group_concat(distinct Category order by Category) into @distinct_rc from Dashboard_Loyalty_Category_Temp;
		    select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
			set @loop_var=1;
			set @json_select='';
            set @top_query='';
			select @loop_var,@num_of_rc,@distinct_rc;
			WHILE @loop_var<=@num_of_rc
			do
			SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
            SET @alias_Name = @selected_rc;
            
			set @top_query=concat(@top_query,",SUM(CASE WHEN Category = '",@selected_rc,"' Then CategoryPercent else concat(0.00,'%') END) AS `",@alias_Name,"`");
				set @json_select=concat(@json_select,',"',@alias_Name,'",concat(round(`',@alias_Name,'`,2), "%")');
				set @loop_var=@loop_var+1;
			end while;
			
			SELECT @json_select,@top_query;
            
            SET @where_cond = CASE WHEN IN_AGGREGATION_1 = 'TY' THEN "Measure = 'TY'" WHEN IN_AGGREGATION_1 = 'LY' THEN "Measure = 'LY'" WHEN IN_AGGREGATION_1 = 'TYLYCHANGE' THEN "Measure = 'TYLYCHANGE'" WHEN IN_AGGREGATION_1 = 'L12M' THEN "Measure = 'L12M' "WHEN IN_AGGREGATION_1 = 'L1224M' THEN "Measure = 'L1224M'" WHEN IN_AGGREGATION_1 = 'L1224MCHANGE' THEN "Measure = 'L1224MCHANGE' "END;
			
			set @view_stmt = concat("create or replace View V_Dashboard_Loyalty_Category_Table as
			(
			select Segment
			",@top_query,"
			from 
			(SELECT Segment, Category, concat(ifnull(CategoryPercent,0),'%') AS CategoryPercent
			FROM Dashboard_Loyalty_Category_Temp 
			WHERE ",@where_cond,"
            AND Segment IS NOT NULL
			GROUP BY 1,2
			)A
			GROUP BY 1)
			;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Segment",`Segment`',@json_select,') )),"]")into @temp_result from V_Dashboard_Loyalty_Category_Table;') ; 
							
END IF;

IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_1 = 'TM' AND IN_AGGREGATION_2 = 'MEMBERSHIP'  
		THEN

        SELECT group_concat(distinct Category order by Category) into @distinct_rc from Dashboard_Loyalty_Category_Temp;
		    select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
			set @loop_var=1;
			set @json_select='';
            set @top_query='';
			select @loop_var,@num_of_rc,@distinct_rc;
			WHILE @loop_var<=@num_of_rc
			do
			SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
            SET @alias_Name = @selected_rc;
            
			set @top_query=concat(@top_query,",SUM(CASE WHEN Category = '",@selected_rc,"' Then CategoryPercent else concat(0.00,'%') END) AS `",@alias_Name,"`");
				set @json_select=concat(@json_select,',"',@alias_Name,'",concat(round(`',@alias_Name,'`,2), "%")');
				set @loop_var=@loop_var+1;
			end while;
			
			SELECT @json_select,@top_query;
			
			set @view_stmt = concat("create or replace View V_Dashboard_Loyalty_Category_Table as
			(
			select Member_Desc
			",@top_query,"
			from 
			(SELECT Member_Desc, Category, concat(ifnull(CategoryPercent,0),'%') AS CategoryPercent
			FROM Dashboard_Loyalty_Category_Temp 
			WHERE Bill_Year_Month = ",@TM,"
            AND Member_Desc IS NOT NULL
			GROUP BY 1,2
			)A
			GROUP BY 1)
			;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Member_Desc",`Member_Desc`',@json_select,') )),"]")into @temp_result from V_Dashboard_Loyalty_Category_Table;') ; 
							
END IF;

		IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_1 = 'LM' AND IN_AGGREGATION_2 = 'MEMBERSHIP'  
		THEN

        SELECT group_concat(distinct Category order by Category) into @distinct_rc from Dashboard_Loyalty_Category_Temp;
		    select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
			set @loop_var=1;
			set @json_select='';
            set @top_query='';
			select @loop_var,@num_of_rc,@distinct_rc;
			WHILE @loop_var<=@num_of_rc
			do
			SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
            SET @alias_Name = @selected_rc;
            
			set @top_query=concat(@top_query,",SUM(CASE WHEN Category = '",@selected_rc,"' Then CategoryPercent else concat(0.00,'%') END) AS `",@alias_Name,"`");
				set @json_select=concat(@json_select,',"',@alias_Name,'",concat(round(`',@alias_Name,'`,2), "%")');
				set @loop_var=@loop_var+1;
			end while;
			
			SELECT @json_select,@top_query;
			
			set @view_stmt = concat("create or replace View V_Dashboard_Loyalty_Category_Table as
			(
			select Member_Desc
			",@top_query,"
			from 
			(SELECT Member_Desc, Category, concat(ifnull(CategoryPercent,0),'%') AS CategoryPercent
			FROM Dashboard_Loyalty_Category_Temp 
			WHERE Measure = 'BYMONTH'
			AND Bill_Year_Month = ",@LM,"
            AND Member_Desc IS NOT NULL
			GROUP BY 1,2
			)A
			GROUP BY 1)
			;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Member_Desc",`Member_Desc`',@json_select,') )),"]")into @temp_result from V_Dashboard_Loyalty_Category_Table;') ; 
							
END IF;

        IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_1 = 'TMLMCHANGE' AND IN_AGGREGATION_2 = 'MEMBERSHIP'  
		THEN

        SELECT group_concat(distinct Category order by Category) into @distinct_rc from Dashboard_Loyalty_Category_Temp;
		    select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
			set @loop_var=1;
			set @json_select='';
            set @top_query='';
			select @loop_var,@num_of_rc,@distinct_rc;
			WHILE @loop_var<=@num_of_rc
			do
			SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
            SET @alias_Name = @selected_rc;
            
			set @top_query=concat(@top_query,",SUM(CASE WHEN Category = '",@selected_rc,"' Then CategoryPercent else concat(0.00,'%') END) AS `",@alias_Name,"`");
				set @json_select=concat(@json_select,',"',@alias_Name,'",concat(round(`',@alias_Name,'`,2), "%")');
				set @loop_var=@loop_var+1;
			end while;
			
			SELECT @json_select,@top_query;
            
            set @view_stmt1 = concat("create or replace View V_Dashboard_Loyalty_Category_Table_TM as
			SELECT Member_Desc, Category, ifnull(CategoryPercent,0) AS CategoryPercent
			FROM Dashboard_Loyalty_Category_Temp 
			WHERE Bill_Year_Month = ",@TM,"
            AND Member_Desc IS NOT NULL
			GROUP BY 1,2
			;");
            
            select @view_stmt1;
			PREPARE statement from @view_stmt1;
			Execute statement;
			Deallocate PREPARE statement;
			
			 set @view_stmt2 = concat("create or replace View V_Dashboard_Loyalty_Category_Table_LM as
			SELECT Member_Desc, Category, ifnull(CategoryPercent,0) AS CategoryPercent
			FROM Dashboard_Loyalty_Category_Temp 
			WHERE Bill_Year_Month = ",@LM,"
            AND Member_Desc IS NOT NULL
			GROUP BY 1,2
			;");
            
            select @view_stmt2;
			PREPARE statement from @view_stmt2;
			Execute statement;
			Deallocate PREPARE statement;
            
            set @view_stmt = concat("create or replace View V_Dashboard_Loyalty_Category_Table as
			(
			select Member_Desc
			",@top_query,"
			from 
			(SELECT A.Member_Desc, A.Category, concat((A.CategoryPercent-B.CategoryPercent),'%') AS CategoryPercent
			FROM V_Dashboard_Loyalty_Category_Table_TM A
            JOIN V_Dashboard_Loyalty_Category_Table_LM B
            ON A.Member_Desc=B.Member_Desc
            AND A.Category=B.Category
			GROUP BY 1,2
			)A
			GROUP BY 1)
			;");
            
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Member_Desc",`Member_Desc`',@json_select,') )),"]")into @temp_result from V_Dashboard_Loyalty_Category_Table;') ; 
        
END IF;

IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_2 = 'MEMBERSHIP'  AND IN_AGGREGATION_1 NOT IN ('TM','LM','TMLMCHANGE')
		THEN

        SELECT group_concat(distinct Category order by Category) into @distinct_rc from Dashboard_Loyalty_Category_Temp;
		    select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
			set @loop_var=1;
			set @json_select='';
            set @top_query='';
			select @loop_var,@num_of_rc,@distinct_rc;
			WHILE @loop_var<=@num_of_rc
			do
			SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
            SET @alias_Name = @selected_rc;
            
			set @top_query=concat(@top_query,",SUM(CASE WHEN Category = '",@selected_rc,"' Then CategoryPercent else concat(0.00,'%') END) AS `",@alias_Name,"`");
				set @json_select=concat(@json_select,',"',@alias_Name,'",concat(round(`',@alias_Name,'`,2), "%")');
				set @loop_var=@loop_var+1;
			end while;
			
			SELECT @json_select,@top_query;
            
            SET @where_cond = CASE WHEN IN_AGGREGATION_1 = 'TY' THEN "Measure = 'TY'" WHEN IN_AGGREGATION_1 = 'LY' THEN "Measure = 'LY'" WHEN IN_AGGREGATION_1 = 'TYLYCHANGE' THEN "Measure = 'TYLYCHANGE'" WHEN IN_AGGREGATION_1 = 'L12M' THEN "Measure = 'L12M' "WHEN IN_AGGREGATION_1 = 'L1224M' THEN "Measure = 'L1224M'" WHEN IN_AGGREGATION_1 = 'L1224MCHANGE' THEN "Measure = 'L1224MCHANGE' "END;
			
			set @view_stmt = concat("create or replace View V_Dashboard_Loyalty_Category_Table as
			(
			select Member_Desc
			",@top_query,"
			from 
			(SELECT Member_Desc, Category, concat(ifnull(CategoryPercent,0),'%') AS CategoryPercent
			FROM Dashboard_Loyalty_Category_Temp 
			WHERE ",@where_cond,"
            AND Member_Desc IS NOT NULL
			GROUP BY 1,2
			)A
			GROUP BY 1)
			;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Member_Desc",`Member_Desc`',@json_select,') )),"]")into @temp_result from V_Dashboard_Loyalty_Category_Table;') ; 
							
END IF;

	IF IN_SCREEN <> "DOWNLOAD"
    THEN
		select @view_stmt;
		PREPARE statement from @view_stmt;
		Execute statement;
		Deallocate PREPARE statement;
		select @Query_String;
		PREPARE statement from @Query_String;
		Execute statement;
		Deallocate PREPARE statement;	
		SET out_result_json = @temp_result;
	ELSE
		call S_dashboard_performance_download('V_Dashboard_Loyalty_Category_Table',@out_result_json1);
		select @out_result_json1 into out_result_json;
    END IF;
	
END$$
DELIMITER ;
