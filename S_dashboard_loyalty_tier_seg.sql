DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_loyalty_tier_seg`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    
    
     SET @FY_SATRT_DATE = date_format(date_sub(concat(IN_FILTER_CURRENTMONTH,'01'), INTERVAL 11 MONTH),"%Y%m");
	 SET @FY_END_DATE = IN_FILTER_CURRENTMONTH;
	 SET @TM = IN_FILTER_CURRENTMONTH;
	 SET @LM = F_Month_Diff(IN_FILTER_CURRENTMONTH,1);
	 SET @SMLY = F_Month_Diff(IN_FILTER_CURRENTMONTH,12);
	 SET @TY=substr(@TM,1,4);
	 SET @LY=substr(@SMLY,1,4);
	 SET @LLY = @LY - 1;
     SET @SMLLY = F_Month_Diff(IN_FILTER_CURRENTMONTH,24);
	 
     
     SELECT @TM,@LM,@SMLY,@TY,@LY,@LLY,@SMLLY;
	
	IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_1 = 'OVERALL' AND IN_NUMBER_FORMAT = 'PERCENT'
	THEN 

        SELECT group_concat(distinct Segment order by Segment) into @distinct_rc from Dashboard_Loyalty_Segments_Tier_Temp;
		    select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
			set @loop_var=1;
			set @json_select='';
            set @top_query='';
			select @loop_var,@num_of_rc,@distinct_rc;
			WHILE @loop_var<=@num_of_rc
			do
			SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
            SET @alias_Name = @selected_rc;
            
			set @top_query=concat(@top_query,",SUM(CASE WHEN Segment = '",@selected_rc,"' Then SegmentPercent else concat(0.00,'%') END) AS `",@alias_Name,"`");
				set @json_select=concat(@json_select,',"',@alias_Name,'",concat(round(`',@alias_Name,'`,2), "%")');
				set @loop_var=@loop_var+1;
			end while;
			
			SELECT @json_select,@top_query;
			
			set @view_stmt = concat("create or replace View V_Dashboard_Loyalty_Seg_Tier_Table as
			(select Tier
			",@top_query,"
			from 
			(SELECT Tier, Segment, concat(ifnull(SegmentPercent,0),'%') AS SegmentPercent
			FROM Dashboard_Loyalty_Segments_Tier_Temp 
			WHERE Measure  = 'Overall'
			GROUP BY 1,2
			)A
			GROUP BY 1)
			;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Tier",`Tier`',@json_select,') )),"]")into @temp_result from V_Dashboard_Loyalty_Seg_Tier_Table;') ; 
							
END IF;

IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_1 = 'TY' AND IN_NUMBER_FORMAT = 'PERCENT'
	THEN 

        SELECT group_concat(distinct Segment order by Segment) into @distinct_rc from Dashboard_Loyalty_Segments_Tier_Temp;
		    select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
			set @loop_var=1;
			set @json_select='';
            set @top_query='';
			select @loop_var,@num_of_rc,@distinct_rc;
			WHILE @loop_var<=@num_of_rc
			do
			SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
            SET @alias_Name = @selected_rc;
            
			set @top_query=concat(@top_query,",SUM(CASE WHEN Segment = '",@selected_rc,"' Then SegmentPercent else concat(0.00,'%') END) AS `",@alias_Name,"`");
				set @json_select=concat(@json_select,',"',@alias_Name,'",concat(round(`',@alias_Name,'`,2), "%")');
				set @loop_var=@loop_var+1;
			end while;
			
			SELECT @json_select,@top_query;
			
			set @view_stmt = concat("create or replace View V_Dashboard_Loyalty_Seg_Tier_Table_TY as
			(select Tier
			",@top_query,"
			from 
			(SELECT Tier, Segment, concat(ifnull(SegmentPercent,0),'%') AS SegmentPercent
			FROM Dashboard_Loyalty_Segments_Tier_Temp 
			WHERE Measure  = 'TY'
			GROUP BY 1,2
			)A
			GROUP BY 1)
			;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Tier",`Tier`',@json_select,') )),"]")into @temp_result from V_Dashboard_Loyalty_Seg_Tier_Table_TY;') ; 
							
END IF;

IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_1 = 'LY' AND IN_NUMBER_FORMAT = 'PERCENT'
	THEN 

        SELECT group_concat(distinct Segment order by Segment) into @distinct_rc from Dashboard_Loyalty_Segments_Tier_Temp;
		    select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
			set @loop_var=1;
			set @json_select='';
            set @top_query='';
			select @loop_var,@num_of_rc,@distinct_rc;
			WHILE @loop_var<=@num_of_rc
			do
			SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
            SET @alias_Name = @selected_rc;
            
			set @top_query=concat(@top_query,",SUM(CASE WHEN Segment = '",@selected_rc,"' Then SegmentPercent else concat(0.00,'%') END) AS `",@alias_Name,"`");
				set @json_select=concat(@json_select,',"',@alias_Name,'",concat(round(`',@alias_Name,'`,2), "%")');
				set @loop_var=@loop_var+1;
			end while;
			
			SELECT @json_select,@top_query;
			
			set @view_stmt = concat("create or replace View V_Dashboard_Loyalty_Seg_Tier_Table_LY as
			(select Tier
			",@top_query,"
			from 
			(SELECT Tier, Segment, concat(ifnull(SegmentPercent,0),'%') AS SegmentPercent
			FROM Dashboard_Loyalty_Segments_Tier_Temp 
			WHERE Measure  = 'LY'
			GROUP BY 1,2
			)A
			GROUP BY 1)
			;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Tier",`Tier`',@json_select,') )),"]")into @temp_result from V_Dashboard_Loyalty_Seg_Tier_Table_LY;') ; 
							
END IF;

IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_1 = 'TYLYCHANGE' AND IN_NUMBER_FORMAT = 'PERCENT'
	THEN 

        SELECT group_concat(distinct Segment order by Segment) into @distinct_rc from Dashboard_Loyalty_Segments_Tier_Temp;
		    select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
			set @loop_var=1;
			set @json_select='';
            set @top_query='';
            set @top_query2='';
			select @loop_var,@num_of_rc,@distinct_rc;
			WHILE @loop_var<=@num_of_rc
			do
				SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
				SET @alias_Name = @selected_rc;
            
				set @top_query=concat(@top_query,",SUM(CASE WHEN Segment = '",@selected_rc,"' Then SegmentPercent else concat(0.00,'%') END) AS `",@alias_Name,"`");
				set @top_query2=concat(@top_query2,",A.",@selected_rc,"-B.",@selected_rc," as `",@alias_Name,"`");
				set @json_select=concat(@json_select,',"',@alias_Name,'",concat(round(`',@alias_Name,'`,2), "%")');
				set @loop_var=@loop_var+1;
			end while;
			
			SELECT @json_select,@top_query, @top_query2;
			
			set @view_stmt = concat("create or replace View V_Dashboard_Loyalty_Seg_Tier_Table as
			(select A.Tier
			",@top_query2,"
			from 
            V_Dashboard_Loyalty_Seg_Tier_Table_TY A, V_Dashboard_Loyalty_Seg_Tier_Table_LY B
			where A.Tier = B.Tier)
			");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Tier",`Tier`',@json_select,') )),"]")into @temp_result from V_Dashboard_Loyalty_Seg_Tier_Table;') ; 
							
END IF;

IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_1 = 'L12M' AND IN_NUMBER_FORMAT = 'PERCENT'
	THEN 

        SELECT group_concat(distinct Segment order by Segment) into @distinct_rc from Dashboard_Loyalty_Segments_Tier_Temp;
		    select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
			set @loop_var=1;
			set @json_select='';
            set @top_query='';
			select @loop_var,@num_of_rc,@distinct_rc;
			WHILE @loop_var<=@num_of_rc
			do
			SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
            SET @alias_Name = @selected_rc;
            
			set @top_query=concat(@top_query,",sum(CASE WHEN Segment = '",@selected_rc,"' Then SegmentPercent else concat(0.00,'%') END) AS `",@alias_Name,"`");
				set @json_select=concat(@json_select,',"',@alias_Name,'",concat(round(`',@alias_Name,'`,2), "%")');
				set @loop_var=@loop_var+1;
			end while;
			
			SELECT @json_select,@top_query;
			
			set @view_stmt = concat("create or replace View V_Dashboard_Loyalty_Seg_Tier_Table_L12M as
			(select Tier
			",@top_query,"
			from 
			(SELECT Tier, Segment, concat(ifnull(SegmentPercent,0),'%') AS SegmentPercent
			FROM Dashboard_Loyalty_Segments_Tier_Temp 
			WHERE Measure  = 'L12M'
			GROUP BY 1,2
			)A
			GROUP BY 1)
			;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Tier",`Tier`',@json_select,') )),"]")into @temp_result from V_Dashboard_Loyalty_Seg_Tier_Table_L12M;') ; 
							
END IF;

IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_1 = 'L1224M' AND IN_NUMBER_FORMAT = 'PERCENT'
	THEN 

        SELECT group_concat(distinct Segment order by Segment) into @distinct_rc from Dashboard_Loyalty_Segments_Tier_Temp;
		    select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
			set @loop_var=1;
			set @json_select='';
            set @top_query='';
			select @loop_var,@num_of_rc,@distinct_rc;
			WHILE @loop_var<=@num_of_rc
			do
			SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
            SET @alias_Name = @selected_rc;
            
			set @top_query=concat(@top_query,",sum(CASE WHEN Segment = '",@selected_rc,"' Then SegmentPercent else concat(0.00,'%') END) AS `",@alias_Name,"`");
				set @json_select=concat(@json_select,',"',@alias_Name,'",concat(round(`',@alias_Name,'`,2), "%")');
				set @loop_var=@loop_var+1;
			end while;
			
			SELECT @json_select,@top_query;
			
			set @view_stmt = concat("create or replace View V_Dashboard_Loyalty_Seg_Tier_Table_L1224M as
			(select Tier
			",@top_query,"
			from 
			(SELECT Tier, Segment, concat(ifnull(SegmentPercent,0),'%') AS SegmentPercent
			FROM Dashboard_Loyalty_Segments_Tier_Temp 
			WHERE Measure  = 'L1224M'
			GROUP BY 1,2
			)A
			GROUP BY 1)
			;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Tier",`Tier`',@json_select,') )),"]")into @temp_result from V_Dashboard_Loyalty_Seg_Tier_Table_L1224M;') ; 
							
END IF;

IF IN_MEASURE = 'TABLE' AND IN_AGGREGATION_1 = 'L1224MCHANGE' AND IN_NUMBER_FORMAT = 'PERCENT'
	THEN 

        SELECT group_concat(distinct Segment order by Segment) into @distinct_rc from Dashboard_Loyalty_Segments_Tier_Temp;
		    select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
			set @loop_var=1;
			set @json_select='';
            set @top_query='';
            set @top_query2='';
			select @loop_var,@num_of_rc,@distinct_rc;
			WHILE @loop_var<=@num_of_rc
			do
			SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
            SET @alias_Name = @selected_rc;
            
			set @top_query=concat(@top_query,",sum(CASE WHEN Segment = '",@selected_rc,"' Then SegmentPercent else concat(0.00,'%') END) AS `",@alias_Name,"`");
            set @top_query2=concat(@top_query2,",A.",@selected_rc,"-B.",@selected_rc," as `",@alias_Name,"`");
				
				set @json_select=concat(@json_select,',"',@alias_Name,'",concat(round(`',@alias_Name,'`,2), "%")');
				set @loop_var=@loop_var+1;
			end while;
			
			SELECT @json_select,@top_query,@top_query2;
			
			set @view_stmt = concat("create or replace View V_Dashboard_Loyalty_Seg_Tier_Table as
			(select A.Tier
			",@top_query2,"
			from 
            V_Dashboard_Loyalty_Seg_Tier_Table_L12M A, V_Dashboard_Loyalty_Seg_Tier_Table_L1224M B
			where A.Tier = B.Tier)
			");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Tier",`Tier`',@json_select,') )),"]")into @temp_result from V_Dashboard_Loyalty_Seg_Tier_Table;') ; 
							
END IF;

	IF IN_SCREEN <> "DOWNLOAD"
    THEN
		select @view_stmt;
		PREPARE statement from @view_stmt;
		Execute statement;
		Deallocate PREPARE statement;
		select @Query_String;
		PREPARE statement from @Query_String;
		Execute statement;
		Deallocate PREPARE statement;	
		SET out_result_json = @temp_result;
	ELSE
		call S_dashboard_performance_download('V_Dashboard_Loyalty_Seg_Tier_Table',@out_result_json1);
		select @out_result_json1 into out_result_json;
    END IF;
	
END$$
DELIMITER ;
