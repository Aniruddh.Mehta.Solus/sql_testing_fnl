DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_blueprint_creative`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
    declare IN_FILTER_CURRENTMONTH text;
    declare IN_MEASURE text;
    declare CreativeName varchar(512); 
    declare CreativeDesc varchar(512); 
    declare ExtCreativeKey varchar(512); 
    declare SenderKey varchar(512);
    declare Creative text;
    declare Header text;
    declare MicrositeCreative text;
    declare MicrositeHeader text;
	declare TemplatePersonalizationScore smallint(6);
	declare CommunicationCooloff int(11);
    declare ChannelName varchar(128);
    declare OfferName varchar(128);
    declare OfferDesc varchar(128);
    declare OfferStartDate varchar(128);
    declare OfferEndDate varchar(128);
    declare OfferType varchar(128);
    declare TimeslotName vaRCHAR(128);
	declare CustomAttr1 varchar(255);
    declare CustomAttr2 varchar(255);
    declare CustomAttr3 varchar(255);
    declare CustomAttr4 varchar(255);
    declare CustomAttr5 varchar(255);
    declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    SET IN_MEASURE=json_unquote(json_extract(in_request_json,"$.MEASURE"));
    SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
    SET CreativeName = json_unquote(json_extract(in_request_json,"$.CreativeName"));
    SET CreativeDesc = json_unquote(json_extract(in_request_json,"$.CreativeDesc"));
    SET ExtCreativeKey = json_unquote(json_extract(in_request_json,"$.ExtCreativeKey"));
    SET SenderKey = json_unquote(json_extract(in_request_json,"$.SenderKey"));
    SET Creative = json_unquote(json_extract(in_request_json,"$.Creative"));
    SET Header = json_unquote(json_extract(in_request_json,"$.Header"));
    SET MicrositeCreative = json_unquote(json_extract(in_request_json,"$.MicrositeCreative"));
    SET MicrositeHeader = json_unquote(json_extract(in_request_json,"$.MicrositeHeader"));
    SET TemplatePersonalizationScore = json_unquote(json_extract(in_request_json,"$.TemplatePersonalizationScore"));
    SET CommunicationCooloff = json_unquote(json_extract(in_request_json,"$.CommunicationCooloff"));
    SET CustomAttr1 = json_unquote(json_extract(in_request_json,"$.CustomAttr1"));
    SET CustomAttr2 = json_unquote(json_extract(in_request_json,"$.CustomAttr2"));
    SET CustomAttr3 = json_unquote(json_extract(in_request_json,"$.CustomAttr3"));
    SET CustomAttr4 = json_unquote(json_extract(in_request_json,"$.CustomAttr4"));
    SET CustomAttr5 = json_unquote(json_extract(in_request_json,"$.CustomAttr5"));
    SET ChannelName = json_unquote(json_extract(in_request_json,"$.ChannelName"));
	SET OfferName = json_unquote(json_extract(in_request_json,"$.OfferName"));
    SET OfferDesc = json_unquote(json_extract(in_request_json,"$.OfferDesc"));
    SET OfferStartDate = json_unquote(json_extract(in_request_json,"$.OfferStartDate"));
    SET OfferEndDate = json_unquote(json_extract(in_request_json,"$.OfferEndDate"));
    SET OfferType = json_unquote(json_extract(in_request_json,"$.OfferType"));
	SET TimeslotName = json_unquote(json_extract(in_request_json,"$.TimeslotName"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
    
    select IN_SCREEN,IN_MEASURE;
	CASE 
    WHEN IN_SCREEN="BLUEPRINT_CREATIVE_LIST" and IN_MEASURE="LIST" THEN
    BEGIN
           SET @Query_String1= 'create or replace view V_Dashboard_Blueprint_Creative_List as  
     select `Name` As `Creative Name`,`Communication_Channel` As `Channels`,`Recommendation_Type` As `Recommendation Algo`,`Offer_Code` As `Offer`,
`Communication_Cooloff` As `Cool Off`, date_format(`Inserted_On`,"%d-%m-%Y") AS `Date Created` from Communication_Template';
                select @Query_String1;
                PREPARE statement1 from @Query_String1;
                Execute statement1;
                Deallocate PREPARE statement1; 
				
			    SET @Query_String = 'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Creative Name",`Creative Name`,"Channels",`Channels`,"Recommendation",
                `Recommendation Algo`,"Offer",`Offer`,"Cool Off",`Cool Off`,"Date Created",`Date Created`) )),"]")into @temp_result from V_Dashboard_Blueprint_Creative_List;' ;
			
                select @Query_String;
                PREPARE statement from @Query_String;
                Execute statement;
                Deallocate PREPARE statement; 
                select @temp_result into out_result_json;
                              
    END;
       WHEN IN_SCREEN="BLUEPRINT_CREATIVE" and IN_MEASURE="INSERT" THEN
    BEGIN
    SET @Insert_Creative=concat('insert into CLM_Creative ( CreativeName, CreativeDesc, ExtCreativeKey, SenderKey, Creative, Header, Microsite_Creative,Microsite_Header, TemplatePersonalizationScore, Communication_Cooloff, Custom_Attr1, Custom_Attr2, Custom_Attr3, Custom_Attr4, Custom_Attr5, CreatedBy, ModifiedBy)
    select "',CreativeName,'", "',CreativeDesc,'","',ExtCreativeKey,'","',SenderKey,'","',Creative,'","',Header,'","',MicrositeCreative,'","',MicrositeHeader,'","',TemplatePersonalizationScore,'","',CommunicationCooloff,'","',CustomAttr1,'","',CustomAttr2,'","',CustomAttr3,'","',CustomAttr4,'","',CustomAttr5,'",7,7');
    
				select @Insert_Creative;
                PREPARE statement1 from @Insert_Creative;
                Execute statement1;
                Deallocate PREPARE statement1;
  
  set sql_safe_updates=0;
  set @update_channel=CONCAT('update CLM_Creative cr,CLM_Channel cl
  set cr.ChannelId=cl.ChannelId
  where cl.ChannelName="',ChannelName,'"
  and cr.CreativeName= "',CreativeName,'"');
  
				PREPARE statement1 from @update_channel;
                Execute statement1;
                Deallocate PREPARE statement1;
  
  set @update_timeslot=CONCAT('update CLM_Creative cr,CLM_Timeslot_Master ct
  set cr.Timeslot_Id=ct.Timeslot_Id
  where ct.Timeslot_Name="',TimeslotName,'"
  and cr.CreativeName= "',CreativeName,'"');
  
				PREPARE statement1 from @update_timeslot;
                Execute statement1;
                Deallocate PREPARE statement1;
               
  select OfferTypeId into @OfferTypeID from CLM_OfferType where OfferTypeName=OfferType;
  if OfferType='GLOBAL_OFFER' then
  set @update_offertype=CONCAT('insert ignore into CLM_Offer (OfferName,OfferDesc,OfferTypeId,OfferStartDate,OfferEndDate) select "',OfferName,'", "',OfferDesc,'","',@OfferTypeID,'","',OfferStartDate,'","',OfferEndDate,'"');
				select @update_offertype;
				PREPARE statement1 from @update_offertype;
                Execute statement1;
                Deallocate PREPARE statement1; 
  end if;
   
  set @update_offer=CONCAT('update CLM_Creative cr,CLM_Offer co
  set cr.OfferId=co.OfferId
  where co.OfferName="',OfferName,'"
  and cr.CreativeName= "',CreativeName,'"');
  
				PREPARE statement1 from @update_offer;
                Execute statement1;
                Deallocate PREPARE statement1; 
	
  set @reco_in_creative=CONCAT('insert into CLM_Reco_In_Creative (CreativeId)
  select CreativeId from CLM_Creative cr 
  where cr.CreativeName= "',CreativeName,'"');
  
				PREPARE statement1 from @reco_in_creative;
                Execute statement1;
                Deallocate PREPARE statement1;
  
    
       SET out_result_json=json_object("Status","Insert_Successful");
                              
    END;
    WHEN IN_SCREEN="BLUEPRINT_CREATIVE" and IN_MEASURE="EDIT" THEN
    BEGIN
    
    
    
           SET @Query_String1= concat('CREATE 
   OR REPLACE
VIEW V_Dashboard_Blueprint_Creative_Edit AS
    SELECT 
        CLM_Creative.CreativeName AS CreativeName,
        CLM_Creative.CreativeDesc AS CreativeDesc,
        (SELECT 
                CLM_Channel.ChannelName
            FROM
                CLM_Channel
            WHERE
                CLM_Channel.ChannelId = CLM_Creative.ChannelId) AS Channel,
        CLM_Creative.ExtCreativeKey AS ExtCreativeKey,
        CLM_Creative.SenderKey AS SenderKey,
        CLM_Creative.Creative AS Creative,
        CLM_Creative.Header AS Header,
        (SELECT 
                CLM_Offer.OfferName
            FROM
                CLM_Offer
            WHERE
                CLM_Offer.OfferId = CLM_Creative.OfferId) AS Offer,
		(SELECT 
                CLM_OfferType.OfferTypeName
            FROM
                CLM_OfferType,CLM_Offer
            WHERE
            CLM_OfferType.OfferTypeId=CLM_Offer.OfferTypeId and 
                CLM_Offer.OfferId = CLM_Creative.OfferId) AS OfferType,
		(SELECT 
                CLM_Offer.OfferStartDate
            FROM
                CLM_Offer
            WHERE
                CLM_Offer.OfferId = CLM_Creative.OfferId) AS OfferStartDate,
		(SELECT 
                CLM_Offer.OfferEndDate
            FROM
                CLM_Offer
            WHERE
                CLM_Offer.OfferId = CLM_Creative.OfferId) AS OfferEndDate
    FROM
        CLM_Creative
    WHERE
        CLM_Creative.CreativeName ="',IN_AGGREGATION_1,'";');
                select @Query_String1;
                PREPARE statement1 from @Query_String1;
                Execute statement1;
                Deallocate PREPARE statement1; 
                
                
				
			    SET @Query_String = 'SELECT CONCAT("[",(GROUP_CONCAT(json_object("CreativeName",`CreativeName`,
                "CreativeDesc",`CreativeDesc`,"ChannelName",`Channel`,
                "ExternalCreativeKey",`ExtCreativeKey`,"SenderKey",`SenderKey`,"Creative",`Creative`,
                "Header",`Header`,"OfferName",`Offer`,"OfferType",`OfferType`,"OfferStartDate",`OfferStartDate`,"OfferEndDate",`OfferEndDate`) )),"]")into @temp_result from V_Dashboard_Blueprint_Creative_Edit;' ;
			
                select @Query_String;
                PREPARE statement from @Query_String;
                Execute statement;
                Deallocate PREPARE statement; 
                select @temp_result into out_result_json;
                                      
    END;
        WHEN IN_SCREEN="BLUEPRINT_CREATIVE" and IN_MEASURE="UPDATE" THEN
    BEGIN
    SET @update_creative=concat('update CLM_Creative cr 
    set cr.CreativeDesc="',CreativeDesc,'",
	 cr.ExtCreativeKey="',ExtCreativeKey,'",
	 cr.SenderKey="',SenderKey,'",
     cr.Creative="',Creative,'",
     cr.Header="',Header,'",
     cr.Microsite_Creative="',MicrositeCreative,'",
     cr.Microsite_Header="',MicrositeHeader,'",
	 cr.TemplatePersonalizationScore="',TemplatePersonalizationScore,'",
     cr.Communication_Cooloff="',CommunicationCooloff,'",
	 cr.Custom_Attr1="',CustomAttr1,'",
     cr.Custom_Attr2="',CustomAttr2,'",
	 cr.Custom_Attr3="',CustomAttr3,'",
     cr.Custom_Attr4="',CustomAttr4,'",
	 cr.Custom_Attr5="',CustomAttr5,'"
    where cr.CreativeName="',CreativeName,'"');
    
    
    
				select @update_creative;
				PREPARE statement from @update_creative;
				Execute statement;
				Deallocate PREPARE statement; 
                
                
  set sql_safe_updates=0;
  set @update_channel=CONCAT('update CLM_Creative cr,CLM_Channel cl
  set cr.ChannelId=cl.ChannelId
  where cl.ChannelName="',ChannelName,'"
  and cr.CreativeName= "',CreativeName,'"');
  
				PREPARE statement1 from @update_channel;
                Execute statement1;
                Deallocate PREPARE statement1;
  
  set @update_timeslot=CONCAT('update CLM_Creative cr,CLM_Timeslot_Master ct
  set cr.Timeslot_Id=ct.Timeslot_Id
  where ct.Timeslot_Name="',TimeslotName,'"
  and cr.CreativeName= "',CreativeName,'"');
  
				PREPARE statement1 from @update_timeslot;
                Execute statement1;
                Deallocate PREPARE statement1;
                
  select OfferTypeId into @OfferTypeID from CLM_OfferType where OfferTypeName=OfferType;
  if OfferType='GLOBAL_OFFER' then
  set @update_offertype=CONCAT('insert ignore into CLM_Offer (OfferName,OfferDesc,OfferTypeId,OfferStartDate,OfferEndDate) select "',OfferName,'", "',OfferDesc,'","',@OfferTypeID,'","',OfferStartDate,'","',OfferEndDate,'"');
				select @update_offertype;
				PREPARE statement1 from @update_offertype;
                Execute statement1;
                Deallocate PREPARE statement1; 
  end if;
                
  set @update_offer=CONCAT('update CLM_Creative cr,CLM_Offer co
  set cr.OfferId=co.OfferId
  where co.OfferName="',OfferName,'"
  and cr.CreativeName= "',CreativeName,'"');
  
				PREPARE statement1 from @update_offer;
                Execute statement1;
                Deallocate PREPARE statement1; 
	          
               SET out_result_json=json_object("Status","Update_Successful");
	END;
    
    ELSE
			SET out_result_json=json_object("Status","Not Implemented Yet");
	    END CASE;	
END$$
DELIMITER ;