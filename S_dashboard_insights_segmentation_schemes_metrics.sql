DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_insights_segmentation_schemes_metrics`()
BEGIN

DECLARE vStart_Date BIGINT DEFAULT 0;
DECLARE vEnd_Date BIGINT DEFAULT 0;
DECLARE done1,done2 INT DEFAULT FALSE;
DECLARE Cursor_Check_Var Int Default 0;
DECLARE cur_Execution_Date_ID INT(11);
DECLARE cur_Segment TEXT;
DECLARE cur_Segmentscheme TEXT;
DECLARE vSuccess int (4);                      
DECLARE vErrMsg Text ; 

DECLARE curALL_Segments
    CURSOR For SELECT CONCAT('DB_',Segment_Name) AS Segments, Segment_Scheme
FROM
    Dashboard_Segment;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done1 = TRUE;
    DECLARE CONTINUE HANDLER FOR SQLWARNING
	BEGIN
		GET DIAGNOSTICS CONDITION 1 @errsqlstate=RETURNED_SQLSTATE, @errno=MYSQL_ERRNO, @errtext = MESSAGE_TEXT;
		SET @logmsg=CONCAT(ifnull(@errsqlstate,'WARNING') , ifnull(@errno,'1'), ifnull(@errtext,'WARNING MESSAGE'));

		SELECT 
    'S_dashboard_insights_segmentation_schemes_metrics : Warning Message :' AS '',
    @logmsg AS '';
	END;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		GET DIAGNOSTICS CONDITION 1 @errsqlstate=RETURNED_SQLSTATE, @errno=MYSQL_ERRNO, @errtext = MESSAGE_TEXT;
		SET @logmsg=CONCAT(ifnull(@errsqlstate,'ERROR') , ifnull(@errno,'1'), ifnull(@errtext,'ERROR MESSAGE'));
		SET vErrMsg=@logmsg;
		SET vSuccess=0;
		SELECT 
    'S_dashboard_insights_segmentation_schemes_metrics : Error Message :' AS '',
    @logmsg AS '';
	END;
    SET done1 = FALSE;
    
    INSERT INTO Dashboard_Metrics_Log
(
`Screen_Name`,
`Status`
)
VALUES
('INSIGHTS - SEGMENTATION',
'Started');

DELETE from log_solus where module = 'S_dashboard_insights_segmentation_schemes_metrics';
SET SQL_SAFE_UPDATES=0;

SET vStart_Date = date_format(date_sub(concat(date_format(current_date(),"%Y%m"),'01'), INTERVAL 12 MONTH),"%Y%m");
SET vEnd_Date = date_format(current_date(),"%Y%m");

INSERT INTO log_solus(module,msg) values ('S_dashboard_insights_segmentation_schemes_metrics','Loading data');

SET Cursor_Check_Var = FALSE;
OPEN curALL_Segments;
LOOP_ALL_DATES: LOOP

	FETCH curALL_Segments into cur_Segment,cur_Segmentscheme;
	IF done1 THEN
	   LEAVE LOOP_ALL_DATES;
	END IF;

		Select cur_Segment,cur_Segmentscheme,now();
        
        set @sqlCust = concat( '
        INSERT INTO Dashboard_Insights_Segmentation_Schemes
(
`AGGREGATION`,
`Measure`,
`Segmentation_Name`,
`Base`,
`Recency`,
`Frequency`,
`ABV`,
`Value`,
`Bills`,
`Amt`
)
select "EVER" AS AGGREGATION, "',cur_Segmentscheme,'" AS Measure, "',cur_Segment,'" AS Segmentation_Name, COUNT(DISTINCT Customer_Id1) AS Base, AVG(DATEDIFF(CURDATE(), Bill_Date)) AS Recency, COUNT(DISTINCT Bill_Header_Id)/COUNT(DISTINCT Customer_Id) AS Frequency, SUM(Sale_Net_Val)/COUNT(DISTINCT Bill_Header_Id) AS ABV, SUM(Sale_Net_Val)/COUNT(DISTINCT Customer_Id) AS Value, COUNT(DISTINCT Bill_Header_Id) AS Bills, SUM(Sale_Net_Val) AS Amt  from CDM_Bill_Details A, CLM_Customer_Segmentation B
WHERE A.Customer_Id = B.Customer_Id1
AND ',cur_Segment,' = "1";');
        prepare stmt1 from @sqlCust;
		execute stmt1;
		deallocate prepare stmt1;
        
set @sqlCust = concat( '
        INSERT INTO Dashboard_Insights_Segmentation_Schemes
(
`AGGREGATION`,
`Measure`,
`Segmentation_Name`,
`Base`,
`Recency`,
`Frequency`,
`ABV`,
`Value`,
`Bills`,
`Amt`
)
select "L12M" AS AGGREGATION, "',cur_Segmentscheme,'" AS Measure, "',cur_Segment,'" AS Segmentation_Name, COUNT(DISTINCT Customer_Id1) AS Base, AVG(DATEDIFF(CURDATE(), Bill_Date)) AS Recency, COUNT(DISTINCT Bill_Header_Id)/COUNT(DISTINCT Customer_Id) AS Frequency, SUM(Sale_Net_Val)/COUNT(DISTINCT Bill_Header_Id) AS ABV, SUM(Sale_Net_Val)/COUNT(DISTINCT Customer_Id) AS Value, COUNT(DISTINCT Bill_Header_Id) AS Bills, SUM(Sale_Net_Val) AS Amt  from CDM_Bill_Details A, CLM_Customer_Segmentation B
WHERE A.Customer_Id = B.Customer_Id1
AND Bill_Year_Month BETWEEN ',vStart_Date,' AND ',vEnd_Date,'
AND ',cur_Segment,' = "1";');
        prepare stmt1 from @sqlCust;
		execute stmt1;
		deallocate prepare stmt1;

set @sqlCust = concat( '
        UPDATE Dashboard_Insights_Segmentation_Schemes A,
(
WITH total as
(
select SUM(Base) as total from Dashboard_Insights_Segmentation_Schemes WHERE Measure = "',cur_Segmentscheme,'"
AND AGGREGATION = "EVER"
)
SELECT AGGREGATION, Segmentation_Name, Base*100/total.total AS Base_Percent
FROM Dashboard_Insights_Segmentation_Schemes, total
WHERE Measure = "',cur_Segmentscheme,'"
AND AGGREGATION = "EVER"
GROUP BY 1,2
) AS B
SET A.Base_Percent = B.Base_Percent
WHERE A.Segmentation_Name = B.Segmentation_Name
AND A.AGGREGATION = B.AGGREGATION;');
        prepare stmt1 from @sqlCust;
		execute stmt1;
		deallocate prepare stmt1;
        
set @sqlCust = concat( '
        UPDATE Dashboard_Insights_Segmentation_Schemes A,
(
WITH total as
(
select SUM(Bills) as total from Dashboard_Insights_Segmentation_Schemes WHERE Measure = "',cur_Segmentscheme,'"
AND AGGREGATION = "EVER"
)
SELECT AGGREGATION, Segmentation_Name, Bills*100/total.total AS Bills_Percent
FROM Dashboard_Insights_Segmentation_Schemes, total
WHERE Measure = "',cur_Segmentscheme,'"
AND AGGREGATION = "EVER"
GROUP BY 1,2
) AS B
SET A.Bills_Percent = B.Bills_Percent
WHERE A.Segmentation_Name = B.Segmentation_Name
AND A.AGGREGATION = B.AGGREGATION;');
        prepare stmt1 from @sqlCust;
		execute stmt1;
		deallocate prepare stmt1;
        
set @sqlCust = concat( '
        UPDATE Dashboard_Insights_Segmentation_Schemes A,
(
WITH total as
(
select SUM(Amt) as total from Dashboard_Insights_Segmentation_Schemes WHERE Measure = "',cur_Segmentscheme,'"
AND AGGREGATION = "EVER"
)
SELECT AGGREGATION, Segmentation_Name, Amt*100/total.total AS Amt_Percent
FROM Dashboard_Insights_Segmentation_Schemes, total
WHERE Measure = "',cur_Segmentscheme,'"
AND AGGREGATION = "EVER"
GROUP BY 1,2
) AS B
SET A.Amt_Percent = B.Amt_Percent
WHERE A.Segmentation_Name = B.Segmentation_Name
AND A.AGGREGATION = B.AGGREGATION;');
        prepare stmt1 from @sqlCust;
		execute stmt1;
		deallocate prepare stmt1;
        


set @sqlCust = concat( '
        UPDATE Dashboard_Insights_Segmentation_Schemes A,
(
WITH total as
(
select SUM(Base) as total from Dashboard_Insights_Segmentation_Schemes WHERE Measure = "',cur_Segmentscheme,'"
AND AGGREGATION = "L12M"
)
SELECT AGGREGATION, Segmentation_Name, Base*100/total.total AS Base_Percent
FROM Dashboard_Insights_Segmentation_Schemes, total
WHERE Measure = "',cur_Segmentscheme,'"
AND AGGREGATION = "L12M"
GROUP BY 1,2
) AS B
SET A.Base_Percent = B.Base_Percent
WHERE A.Segmentation_Name = B.Segmentation_Name
AND A.AGGREGATION = B.AGGREGATION;');
        prepare stmt1 from @sqlCust;
		execute stmt1;
		deallocate prepare stmt1;
        
set @sqlCust = concat( '
        UPDATE Dashboard_Insights_Segmentation_Schemes A,
(
WITH total as
(
select SUM(Bills) as total from Dashboard_Insights_Segmentation_Schemes WHERE Measure = "',cur_Segmentscheme,'"
AND AGGREGATION = "L12M"
)
SELECT AGGREGATION, Segmentation_Name, Bills*100/total.total AS Bills_Percent
FROM Dashboard_Insights_Segmentation_Schemes, total
WHERE Measure = "',cur_Segmentscheme,'"
AND AGGREGATION = "L12M"
GROUP BY 1,2
) AS B
SET A.Bills_Percent = B.Bills_Percent
WHERE A.Segmentation_Name = B.Segmentation_Name
AND A.AGGREGATION = B.AGGREGATION;');
        prepare stmt1 from @sqlCust;
		execute stmt1;
		deallocate prepare stmt1;
        
set @sqlCust = concat( '
        UPDATE Dashboard_Insights_Segmentation_Schemes A,
(
WITH total as
(
select SUM(Amt) as total from Dashboard_Insights_Segmentation_Schemes WHERE Measure = "',cur_Segmentscheme,'"
AND AGGREGATION = "L12M"
)
SELECT AGGREGATION, Segmentation_Name, Amt*100/total.total AS Amt_Percent
FROM Dashboard_Insights_Segmentation_Schemes, total
WHERE Measure = "',cur_Segmentscheme,'"
AND AGGREGATION = "L12M"
GROUP BY 1,2
) AS B
SET A.Amt_Percent = B.Amt_Percent
WHERE A.Segmentation_Name = B.Segmentation_Name
AND A.AGGREGATION = B.AGGREGATION;');
        prepare stmt1 from @sqlCust;
		execute stmt1;
		deallocate prepare stmt1;


END LOOP LOOP_ALL_DATES;

UPDATE Dashboard_Insights_Segmentation_Schemes
SET Segmentation_Name = REPLACE(Segmentation_Name,'DB_','');

UPDATE Dashboard_Metrics_Log
SET `Status` = 'Ended' WHERE Screen_Name = 'INSIGHTS - SEGMENTATION';

END$$
DELIMITER ;
