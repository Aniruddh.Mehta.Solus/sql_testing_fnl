DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_Customer_Reco`(IN in_request_json JSON, OUT out_result_json JSON)
BEGIN
    DECLARE IN_USER_NAME VARCHAR(240); 
    DECLARE IN_SCREEN VARCHAR(400); 
    DECLARE temp_result JSON;
    DECLARE IN_FILTER_CURRENTMONTH TEXT; 
    DECLARE IN_FILTER_MONTHDURATION TEXT; 
    DECLARE IN_MEASURE TEXT; 
    DECLARE IN_NUMBER_FORMAT TEXT;
    DECLARE IN_AGGREGATION_1 TEXT; 
    DECLARE IN_AGGREGATION_2 TEXT;
    DECLARE IN_AGGREGATION_3 TEXT;
    DECLARE Query_String TEXT;
    DECLARE IN_FILTER_REV_CENTER TEXT;
    DECLARE IN_SHOW_COL_PICKER TEXT;
    DECLARE IN_FILTER_SEGMENT TEXT;
    DECLARE IN_RECO_STORY TEXT;
    DECLARE STORY_ID INT;
    DECLARE CUSTOMER_ID BIGINT(20);   
    
    SET IN_FILTER_CURRENTMONTH = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.FILTER_CURRENTMONTH"));
    SET IN_FILTER_MONTHDURATION = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.FILTER_MONTHDURATION"));
    SET IN_MEASURE = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.MEASURE"));
    SET IN_NUMBER_FORMAT = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.NUMBER_FORMAT"));
    SET IN_AGGREGATION_1 = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.AGGREGATION_1"));
    SET IN_AGGREGATION_2 = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.AGGREGATION_2"));
    SET IN_AGGREGATION_3 = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.AGGREGATION_3"));
    SET IN_FILTER_REV_CENTER = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.FILTER_REV_CENTER"));
    SET IN_FILTER_SEGMENT = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.FILTER_SEGMENT"));
    SET IN_RECO_STORY = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.RECO_STORY"));
    SET IN_SHOW_COL_PICKER = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.SHOW_COL_PICKER"));
    SET IN_USER_NAME = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.USER"));
    SET IN_SCREEN = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.SCREEN"));
    SET CUSTOMER_ID = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.CUSTOMER_ID"));
    SET STORY_ID = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.STORY_ID"));
    set @temp_result='';
    IF IN_MEASURE = "Rcore_Story" THEN
    set @temp_result='';
        SET @Query_String = CONCAT('SELECT CONCAT("[",(GROUP_CONCAT(JSON_OBJECT(\'Story_Id\', a.Story_Id, \'Name\', a.Name))),"]") INTO @temp_result FROM RankedPickList_Stories_Master a JOIN RankedPickList_Stories b ON a.Story_Id = b.Story_Id WHERE b.Customer_Id = ', CUSTOMER_ID, ';');
    END IF;
    
    IF IN_MEASURE = 'RANDOM'
    THEN
		
        set @view_stmt=concat('CREATE or replace VIEW RandomCustomerView AS
SELECT CONCAT(\'[{"Customer_Id": "\', rp.Customer_Id, \'"}]\')
FROM RankedPickList_Stories rp
WHERE rp.Customer_Id IN (
    SELECT cbd.Customer_Id
    FROM Customer_One_View cbd
    WHERE cbd.Customer_Id = rp.Customer_Id AND cbd.LT_Num_Of_Visits > 0
)
LIMIT 1;');
	
SET @Query_String = CONCAT('select * into @temp_result from RandomCustomerView;');

    END IF;
    
    IF IN_MEASURE = 'NEXT' THEN
    SET @Query_String := CONCAT('SELECT CONCAT(
        \'[{"Customer_Id": "\', Customer_Id, \'"}]\'
    ) INTO @temp_result
    FROM RankedPickList_Stories
    WHERE Customer_Id > ', CUSTOMER_ID, '
    ORDER BY Customer_Id
    LIMIT 1;');
END IF;
    
     IF IN_MEASURE = "Rcore_Segment" THEN
    set @temp_result='';
      SET @Query_String = CONCAT('SELECT CONCAT("[",(GROUP_CONCAT(JSON_OBJECT(\'Customer_Id\', Customer_Id, \'Segment\', CLMSegmentName))),"]") into @temp_result
FROM CDM_Customer_Segment
WHERE Customer_Id = ',CUSTOMER_ID);
  END IF;
    
    IF IN_MEASURE = "ORDER" THEN
    set @temp_result='';
       SET @Query_String = CONCAT('SELECT CONCAT("[", GROUP_CONCAT(JSON_OBJECT(
    \'Bill Date\', Bill_Date,
    
    \'Store Name\', IFNULL(Store_Name, "     -    "),
    \'Product Name\', IF(Product_Name = \'\', \'-\', Product_Name),
    \'Attribute 1\', IFNULL(Product_Attributes_1,"-"),
    \'Attribute 2\', IFNULL(Product_Attributes_2,"-"),
    \'Attribute 3\', IFNULL(Product_Attributes_3,"-"),
    \'Attribute 4\', IFNULL(Product_Attributes_4,"-"),
    \'Store Id\', IFNULL(Store_Id,"-"),
    \'Sale Net Value\', Round(IFNULL(Sale_Net_Val, 0), 1),
    \'Sale Disc Value\', Round(IFNULL(Sale_Disc_Val, 0), 1)
)), "]") into @temp_result
FROM (
    SELECT
        Bill_Date,
        
        C.Store_Name,
        B.Product_Name,
        SUBSTRING_INDEX(SUBSTRING_INDEX(Product_Genome, \'___\', 1), \'___\', -1) AS Product_Attributes_1,
        SUBSTRING_INDEX(SUBSTRING_INDEX(Product_Genome, \'___\', 2), \'___\', -1) AS Product_Attributes_2,
        SUBSTRING_INDEX(SUBSTRING_INDEX(Product_Genome, \'___\', 3), \'___\', -1) AS Product_Attributes_3,
        SUBSTRING_INDEX(SUBSTRING_INDEX(Product_Genome, \'___\', 4), \'___\', -1) AS Product_Attributes_4,
        A.Store_Id,
        Sale_Net_Val,
        Sale_Disc_Val
    FROM CDM_Bill_Details A
    LEFT JOIN CDM_Product_Master B ON A.Product_Id = B.Product_Id
    LEFT JOIN CDM_Store_Master C ON A.Store_Id = C.Store_Id
    WHERE
        A.Customer_id = ', CUSTOMER_ID, '
    -- AND Story_Id = ', STORY_ID, '
    GROUP BY 1, 2, 8
    ORDER BY Bill_Date DESC
) AS subquery;');



    END IF;
    
IF IN_MEASURE = "RECOMENDATION" THEN
        SET @Query_String = CONCAT('SELECT CONCAT("[", GROUP_CONCAT(json_row SEPARATOR ","), "]") into @temp_result

        FROM (
            SELECT CONCAT(
                \'{"Rank":"\', Rank, \'","\',
                \'Product Name":"\', IF(Name = \'\', \'-\', Name), \'","\',
                \'Category":"\', Category, \'","\',
                \'Attribute 1":"\', Product_Attributes_1, \'","\',
                \'Attribute 2":"\', Product_Attributes_2, \'","\',
                \'Attribute 3":"\', Product_Attributes_3, \'","\',
                \'Attribute 4":"\', Product_Attributes_4, \'"}\'
            ) AS json_row
            FROM (
                SELECT "1" AS Rank,
                    Product_Name AS Name,
                    (SELECT LOV_Key FROM CDM_LOV_Master WHERE Cat_LOV_Id = LOV_id) AS Category,
                    SUBSTRING_INDEX(SUBSTRING_INDEX(Product_Genome, \'___\', 1), \'___\', -1) AS Product_Attributes_1,
                    SUBSTRING_INDEX(SUBSTRING_INDEX(Product_Genome, \'___\', 2), \'___\', -1) AS Product_Attributes_2,
                    SUBSTRING_INDEX(SUBSTRING_INDEX(Product_Genome, \'___\', 3), \'___\', -1) AS Product_Attributes_3,
                    SUBSTRING_INDEX(SUBSTRING_INDEX(Product_Genome, \'___\', 4), \'___\', -1) AS Product_Attributes_4
                FROM CDM_Product_Master
                WHERE product_id = (
                    SELECT Reco_Product_1
                    FROM RankedPickList_Stories
                    WHERE Customer_id = ',CUSTOMER_ID,'
                    AND Story_Id = ',STORY_ID,'
                )
                UNION
                SELECT "2" AS Rank,
                    Product_Name AS Name,
                    (SELECT LOV_Key FROM CDM_LOV_Master WHERE Cat_LOV_Id = LOV_id) AS Category,
                    SUBSTRING_INDEX(SUBSTRING_INDEX(Product_Genome, \'___\', 1), \'___\', -1) AS Product_Attributes_1,
                    SUBSTRING_INDEX(SUBSTRING_INDEX(Product_Genome, \'___\', 2), \'___\', -1) AS Product_Attributes_2,
                    SUBSTRING_INDEX(SUBSTRING_INDEX(Product_Genome, \'___\', 3), \'___\', -1) AS Product_Attributes_3,
                    SUBSTRING_INDEX(SUBSTRING_INDEX(Product_Genome, \'___\', 4), \'___\', -1) AS Product_Attributes_4
                FROM CDM_Product_Master
                WHERE product_id = (
                    SELECT Reco_Product_2
                    FROM RankedPickList_Stories
                    WHERE Customer_id = ',CUSTOMER_ID,'
                    AND Story_Id = ',STORY_ID,'
                )
                UNION
                SELECT "3" AS Rank,
                    Product_Name AS Name,
                    (SELECT LOV_Key FROM CDM_LOV_Master WHERE Cat_LOV_Id = LOV_id) AS Category,
                    SUBSTRING_INDEX(SUBSTRING_INDEX(Product_Genome, \'___\', 1), \'___\', -1) AS Product_Attributes_1,
                    SUBSTRING_INDEX(SUBSTRING_INDEX(Product_Genome, \'___\', 2), \'___\', -1) AS Product_Attributes_2,
                    SUBSTRING_INDEX(SUBSTRING_INDEX(Product_Genome, \'___\', 3), \'___\', -1) AS Product_Attributes_3,
                    SUBSTRING_INDEX(SUBSTRING_INDEX(Product_Genome, \'___\', 4), \'___\', -1) AS Product_Attributes_4
                FROM CDM_Product_Master
                WHERE product_id = (
                    SELECT Reco_Product_3
                    FROM RankedPickList_Stories
                    WHERE Customer_id = ',CUSTOMER_ID,'
                    AND Story_Id = ',STORY_ID,'
                )
                UNION
                SELECT "4" AS Rank,
                    Product_Name AS Name,
                    (SELECT LOV_Key FROM CDM_LOV_Master WHERE Cat_LOV_Id = LOV_id) AS Category,
                    SUBSTRING_INDEX(SUBSTRING_INDEX(Product_Genome, \'___\', 1), \'___\', -1) AS Product_Attributes_1,
                    SUBSTRING_INDEX(SUBSTRING_INDEX(Product_Genome, \'___\', 2), \'___\', -1) AS Product_Attributes_2,
                    SUBSTRING_INDEX(SUBSTRING_INDEX(Product_Genome, \'___\', 3), \'___\', -1) AS Product_Attributes_3,
                    SUBSTRING_INDEX(SUBSTRING_INDEX(Product_Genome, \'___\', 4), \'___\', -1) AS Product_Attributes_4
                FROM CDM_Product_Master
                WHERE product_id = (
                    SELECT Reco_Product_4
                    FROM RankedPickList_Stories
                    WHERE Customer_id = ',CUSTOMER_ID,'
                    AND Story_Id = ',STORY_ID,'
                )
                UNION
                SELECT "5" AS Rank,
                    Product_Name AS Name,
                    (SELECT LOV_Key FROM CDM_LOV_Master WHERE Cat_LOV_Id = LOV_id) AS Category,
                    SUBSTRING_INDEX(SUBSTRING_INDEX(Product_Genome, \'___\', 1), \'___\', -1) AS Product_Attributes_1,
                    SUBSTRING_INDEX(SUBSTRING_INDEX(Product_Genome, \'___\', 2), \'___\', -1) AS Product_Attributes_2,
                    SUBSTRING_INDEX(SUBSTRING_INDEX(Product_Genome, \'___\', 3), \'___\', -1) AS Product_Attributes_3,
                    SUBSTRING_INDEX(SUBSTRING_INDEX(Product_Genome, \'___\', 4), \'___\', -1) AS Product_Attributes_4
                FROM CDM_Product_Master
                WHERE product_id = (
                    SELECT Reco_Product_5
                    FROM RankedPickList_Stories
                    WHERE Customer_id = ',CUSTOMER_ID,'
                    AND Story_Id = ',STORY_ID,'
                )
                UNION
                SELECT "6" AS Rank,
                    Product_Name AS Name,
                    (SELECT LOV_Key FROM CDM_LOV_Master WHERE Cat_LOV_Id = LOV_id) AS Category,
                    SUBSTRING_INDEX(SUBSTRING_INDEX(Product_Genome, \'___\', 1), \'___\', -1) AS Product_Attributes_1,
                    SUBSTRING_INDEX(SUBSTRING_INDEX(Product_Genome, \'___\', 2), \'___\', -1) AS Product_Attributes_2,
                    SUBSTRING_INDEX(SUBSTRING_INDEX(Product_Genome, \'___\', 3), \'___\', -1) AS Product_Attributes_3,
                    SUBSTRING_INDEX(SUBSTRING_INDEX(Product_Genome, \'___\', 4), \'___\', -1) AS Product_Attributes_4
                FROM CDM_Product_Master
                WHERE product_id = (
                    SELECT Reco_Product_6
                    FROM RankedPickList_Stories
                    WHERE Customer_id = ',CUSTOMER_ID,'
                    AND Story_Id = ',STORY_ID,'
                )
                
            ) AS subquery
        ) AS result;');

    END IF;
    
     IF IN_MEASURE = "Rcore_Cards" THEN
    set @temp_result='';
SET @Query_String = CONCAT(
    'SELECT CONCAT("[", GROUP_CONCAT(JSON_OBJECT(\'Customer_Id\', Customer_Id, \'Recency\', Recency, \'LT_Num_Of_Visits\', LT_Num_Of_Visits,\'Prod_Range\', ifnull(Prod_Range,"-"),\'Cat_Range\',ifnull(Cat_Range,"-"), \'Fav_Prod\', ifnull(Fav_Prod,"-"), \'Fav_Cat\', Fav_Cat, \'Fav_Store\', ifnull(Fav_Store,"-") ,  \'Lt_Rev\', Lt_Rev)), "]") into @temp_result ',
    'FROM ( ',
    'SELECT ',
    'viewtable.Customer_Id, ',
    'viewtable.Recency, ',
    'viewtable.LT_Num_Of_Visits, ',
    '(SELECT LOV_Comm_Value FROM CDM_Customer_TP_Var favp LEFT JOIN CDM_LOV_Master favp_lov ON favp.LT_Fav_Prod_LOV_Id = favp_lov.LOV_Id WHERE favp.Customer_Id = ',CUSTOMER_ID,') AS Fav_Prod, ',
    '(SELECT LOV_Value FROM CDM_Customer_TP_Var favs LEFT JOIN CDM_LOV_Master favs_lov ON favs.LTD_Fav_Cat_Lov_Id = favs_lov.LOV_Id WHERE favs.Customer_Id = ',CUSTOMER_ID,') AS Fav_Cat, ',
    '(SELECT Store_Name FROM CDM_Customer_TP_Var favs LEFT JOIN CDM_Store_Master favs_lov ON favs.LT_Fav_Store_LOV_Id = favs_lov.LOV_Id WHERE favs.Customer_Id =',CUSTOMER_ID,') AS Fav_Store, ',
	'(SELECT LT_Distinct_Prod_Cnt FROM CDM_Customer_TP_Var favp  WHERE favp.Customer_Id = ',CUSTOMER_ID,') AS Prod_Range, ',
    '(SELECT LT_Distinct_Cat_Cnt FROM CDM_Customer_TP_Var favp  WHERE favp.Customer_Id = ',CUSTOMER_ID,') AS Cat_Range, ',
    '(SELECT Lt_Rev FROM CDM_Customer_TP_Var WHERE Customer_Id =',CUSTOMER_ID,') AS Lt_Rev ',
    'FROM Customer_One_View viewtable ',
    'WHERE viewtable.Customer_Id = ',CUSTOMER_ID,' ',
    ') AS subquery;'
);


    END IF;
    

    
 IF IN_SCREEN='CUSTOMER_RECO'
    THEN
		select @view_stmt;
		PREPARE statement from @view_stmt;
		Execute statement;
		Deallocate PREPARE statement; 
		
		
		PREPARE statement from @Query_String;
		Execute statement;
		Deallocate PREPARE statement; 
		SET out_result_json = @temp_result;
	END IF;
    SET out_result_json = @temp_result;
END$$
DELIMITER ;
