DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_UI_Campaign_Check_Coverage`(IN in_request_json json, OUT out_result_json json)
BEGIN
    
    -- User Information

    declare IN_USER_NAME varchar(240); 
    declare IN_SCREEN varchar(400); 
    declare vSuccess  varchar(400); 
    declare vErrMsg  varchar(400);
    declare vSQLCondition Text;
    declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text; 
	declare IN_AGGREGATION_3 text;
    -- Target Audience  
  
    declare Campaign_Segment text;
    declare Lifecycle_Segment text;
	declare Lifecycle_SegmentCondition varchar(400) Default '';
    declare Region Bigint;
    -- Advanced RFMP Criteria
    declare Valid_Mobile varchar(400);
	declare Valid_Email varchar(400);
	declare DND varchar(400);
	declare Favourite_Day Varchar(128);
    declare Throtling Bigint;
    
	declare Recency_Start VARCHAR(128);
	declare Recency_End VARCHAR(128);
	declare Vintage_Start VARCHAR(128);
	declare Vintage_End VARCHAR(128);
	declare Visit_Start VARCHAR(128);
	declare Visit_End VARCHAR(128);
	declare Monetary_Value_Start VARCHAR(128);
	declare Monetary_Value_End VARCHAR(128);
	declare AP_FAV_CAT VARCHAR(128);
	declare Last_Bought_Category VARCHAR(128);
	declare Favourite_Category VARCHAR(128);
	declare Purchase_Anniversary VARCHAR(128);
	declare Multiple_of_5 VARCHAR(128);
	declare Discount VARCHAR(128);

	declare SVOC_Condition text;
	declare SVOT_Condition text;
	DECLARE Number_of_filters text;
				DECLARE SVOT_Condition1 text;
				DECLARE Exclusion_Filter_Condition text;
    	

  -- --------------------- Decoding the JSON -----------------------------------------------------------------------------------------------------------------------------------------------
  
  
    Set @Err =0;
	
   
   -- User Information
   
    SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1")); 
    SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2")); 
    SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3")); 
  
    SET Campaign_Segment = json_unquote(json_extract(in_request_json,"$.Campagin_Segment"));
    SET Lifecycle_Segment = json_unquote(json_extract(in_request_json,"$.Lifecycle_Segment"));
	
	Set Lifecycle_Segment= replace (Lifecycle_Segment,"[",'');
	Set Lifecycle_Segment= replace (Lifecycle_Segment,"]",'');
	Set Lifecycle_Segment= replace (Lifecycle_Segment,'"','');
	Set Lifecycle_Segment= replace (Lifecycle_Segment,', ',',');
    
	Set Campaign_Segment= replace (Campaign_Segment,"[",'');
	Set Campaign_Segment= replace (Campaign_Segment,"]",'');
	Set Campaign_Segment= replace (Campaign_Segment,'"','');
	Set Campaign_Segment= replace (Campaign_Segment,', ',',');
	
	
    SET Region = json_unquote(json_extract(in_request_json,"$.Region"));
    
    -- Advanced RFMP Criteria
    SET Valid_Mobile = json_unquote(json_extract(in_request_json,"$.Valid_Mobile"));
	SET Valid_Email = json_unquote(json_extract(in_request_json,"$.Valid_Email"));
	SET DND = json_unquote(json_extract(in_request_json,"$.DND"));
	SET Favourite_Day = json_unquote(json_extract(in_request_json,"$.Favourite"));
	
    SET Recency_Start = json_unquote(json_extract(in_request_json,"$.Recency_Start"));
    SET Recency_End = json_unquote(json_extract(in_request_json,"$.Recency_End"));
    SET Visit_Start = json_unquote(json_extract(in_request_json,"$.Visit_Start"));
	SET Visit_End = json_unquote(json_extract(in_request_json,"$.Visit_End"));
    SET Monetary_Value_Start = json_unquote(json_extract(in_request_json,"$.Monetary_Value_Start"));
	SET Monetary_Value_End = json_unquote(json_extract(in_request_json,"$.Monetary_Value_End"));
    SET Discount = json_unquote(json_extract(in_request_json,"$.Discount"));
    SET Last_Bought_Category = json_unquote(json_extract(in_request_json,"$.Last_Bought_Category"));
    SET Favourite_Category = json_unquote(json_extract(in_request_json,"$.Favourite_Category"));
    SET AP_FAV_CAT = json_unquote(json_extract(in_request_json,"$.Active_Period_Category"));
	SET Purchase_Anniversary  = json_unquote(json_extract(in_request_json,"$.Purchase_Anniversary"));
	SET Multiple_of_5  = json_unquote(json_extract(in_request_json,"$.Multiple_Of_5_Transaction"));
	
    SET SVOC_Condition = json_unquote(json_extract(in_request_json,"$.Advance_Trigger_SVOC"));
	 SET SVOT_Condition = json_unquote(json_extract(in_request_json,"$.Advance_Trigger_SVOT"));
    SET @temp_result = Null;



    Select Favourite_Day;
    -- Creating the Replace Queary

	Set  @Replaced_Query='';
      Select  CONCAT_WS(' AND ',
                CASE
                    WHEN Recency_Start = '' THEN NULL
                    ELSE concat (' Recency Between ', Recency_Start ,' and ', Recency_End)
                END
                ,CASE
                    WHEN Visit_Start = '' THEN NULL
                    ELSE concat (' Lt_Num_of_Visits Between ', Visit_Start ,' and ', Visit_End)
                END
                ,CASE
                    WHEN Monetary_Value_Start = '' THEN NULL
                    ELSE concat (' LT_ABV Between ', Monetary_Value_Start ,' and ', Monetary_Value_End)
                END
                ,CASE
                    WHEN Last_Bought_Category = '' THEN NULL
                    ELSE Concat ('LTD_Fav_Cat_LOV_ID in (',Last_Bought_Category,')')
                END
                ,CASE
                    WHEN Favourite_Category = '' THEN NULL
                    ELSE Concat ('LT_Fav_Cat_LOV_ID in (',Favourite_Category,')')
                END
                ,CASE
                    WHEN Favourite_Day = "No" or Favourite_Day = " " THEN NULL
                    ELSE  Concat ('Lt_Fav_Day_Of_Week =weekDay(now())')
                END
				 ,CASE
                    WHEN Purchase_Anniversary = '' THEN NULL
                    ELSE  Concat ('((Vintage/365)  in (',Purchase_Anniversary,'))')
                END
				 ,CASE
                    WHEN Multiple_of_5 ='' THEN NULL
                    ELSE  Concat ('((Lt_Num_of_Visits/5) in (',Multiple_of_5,'))')
                END
				 ,CASE
                    WHEN AP_FAV_CAT ='' THEN NULL
                    ELSE Concat ('AP_Fav_Cat_LOV_ID in (',AP_FAV_CAT,')')
                END
				,CASE
                    WHEN Discount ='' THEN NULL
                    ELSE Concat ('Lt_Dis_Percent in (',Discount,')')
                END
				
                ) into @Replaced_Query;
                
		


	
	If SVOC_Condition is not null or SVOC_Condition !=''
	Then SET SVOC_Condition="1=1";
	End If;
	
	If @Replaced_Query is Null or @Replaced_Query =''
	THEN SET @Replaced_Query="1=1";	
    end If;	
	Select @Replaced_Query;
	
	If SVOC_Condition is not null or SVOC_Condition !=''
	Then 
	SET @Replaced_Query = CONCAT('( ',@Replaced_Query,' )');
	SET SVOC_Condition = CONCAT('( ',SVOC_Condition,' )');
	SET @Replaced_Query=concat(@Replaced_Query," AND ",SVOC_Condition);	
	End If;
	
	Select @Replaced_Query;
	If Valid_Mobile ="NO"
	THEN	Set Valid_Mobile ='-1,1';
	Else Set Valid_Mobile ='1';
	End If;

	If Valid_Email ="NO"
		THEN	Set Valid_Email ='-1,1';
	Else Set Valid_Email ='1';
	End If;
	
	If DND is null
		THEN	Set DND ="NO";
	
	End If;


	If DND ="NO"
		THEN	Set DND ='-1,1';
	Else Set DND ='1';
	End If;

	If Favourite_Day ="NO"
		THEN	Set Favourite_Day ='-1,1';
	Else Set Favourite_Day ='Favourite_Day = weekDay(now())';
	End If;
		
	
	If  @Err ='0'
	    THEN

	        If IN_AGGREGATION_1 ="Coverage" 
	          Then    
	              Select Lifecycle_Segment,Campaign_Segment,Region;
	             
	              If Lifecycle_Segment is not null 
	                Then
	                   
					   
						Set  @Campaign_SegmentReplacedQuery='';
						Set @Region_SegmentReplacedQuery='';
						Set @Lifecycle_SegmentReplacedQuery=' ';
	                    Select MSReplacedQuery into @Campaign_SegmentReplacedQuery  from CLM_MicroSegment where MicroSegmentId=Campaign_Segment;
                        
						Select RegionReplacedQuery into @Region_SegmentReplacedQuery  from CLM_RegionSegment where RegionSegmentId=Region;
						
                        
						If @Campaign_SegmentReplacedQuery is Null or @Campaign_SegmentReplacedQuery ='' 
							Then Set @Campaign_SegmentReplacedQuery="1=1";
						End If;	
						
						If @Region_SegmentReplacedQuery is Null or @Region_SegmentReplacedQuery=''
							Then Set @Region_SegmentReplacedQuery="1=1";
						End If;	
						
						If Lifecycle_Segment != "All_segment"
							Then 
							
							Set @SQL1=Concat('Select Concat("(",Replace(group_concat("(",CLM_Segment.CLMSegmentReplacedQuery,")"),","," or "),")") into @Lifecycle_SegmentReplacedQuery  from CLM_Segment where CLMSegmentId in (',Lifecycle_Segment,');
							');
							SELECT @SQL1;
							PREPARE statement from @SQL1;
							Execute statement;
							Deallocate PREPARE statement; 
							
						Else Set @Lifecycle_SegmentReplacedQuery='1=1';
						End If;	
						
	                    Select @Campaign_SegmentReplacedQuery, @Region_SegmentReplacedQuery ,@Lifecycle_SegmentReplacedQuery;

						SET @Campaign_SegmentReplacedQuery = CONCAT('( ',@Campaign_SegmentReplacedQuery,' )');
						SET @Region_SegmentReplacedQuery = CONCAT('( ',@Region_SegmentReplacedQuery,' )');
						SET @Lifecycle_SegmentReplacedQuery = CONCAT('( ',@Lifecycle_SegmentReplacedQuery,' )');


						
	                    set vSQLCondition= concat(@Replaced_Query,'  and ', @Campaign_SegmentReplacedQuery,' and ', @Region_SegmentReplacedQuery,' and ',@Lifecycle_SegmentReplacedQuery) ;
	                    
	                    Select vSQLCondition;
						
						Select MSReplacedQueryBill into @Campaign_SegmentBillQuery  from CLM_MicroSegment where MicroSegmentId=Campaign_Segment;
						Select RegionReplacedQueryBill into @Region_SegmentBillQuery  from CLM_RegionSegment where RegionSegmentId=Region;

						IF  @Campaign_SegmentBillQuery IS NULL OR @Campaign_SegmentBillQuery = ''
							THEN
								SET @SVOT_Condition_Campaign='SELECT 1 from DUAL';
						ELSE
							SET @SVOT_Condition_Campaign = CONCAT('SELECT 1 from V_CLM_Bill_Detail_One_View svot  WHERE ',@Campaign_SegmentBillQuery,'');
						END IF;

						IF  @Region_SegmentBillQuery IS NULL OR @Region_SegmentBillQuery = ''
							THEN
								SET @SVOT_Condition_Region='SELECT 1 from DUAL';
						ELSE
							SET @SVOT_Condition_Region = CONCAT('SELECT 1 from V_CLM_Bill_Detail_One_View svot  WHERE ',@Region_SegmentBillQuery,'');
						END IF;		
						
						IF  Valid_Mobile != '1'
							THEN
								SET @SVOT_Condition_Valid_Mobile='SELECT 1 from DUAL';
						ELSE
								SET @SVOT_Condition_Valid_Mobile = CONCAT('SELECT 1 from CDM_Customer_PII_Master Valid_Mobile  WHERE length(Mobile) >= 10 and Mobile REGEXP "^[0-9]+$"');
						END IF; 	
														
						IF  Valid_Email != '1'
							THEN
								SET @SVOT_Condition_Valid_Email='SELECT 1 from DUAL';
						ELSE
								SET @SVOT_Condition_Valid_Email = CONCAT('SELECT 1 from CDM_Customer_PII_Master Valid_Email  WHERE  Email REGEXP "(.*)@(.*)\\.(.*)"');
						END IF; 

						IF  DND != '1'
							THEN
								SET @SVOT_Condition_DND='SELECT 1 from DUAL';
						ELSE
								SET @SVOT_Condition_DND = CONCAT('SELECT 1 from CDM_Customer_Master DND  WHERE DND in(1,"y","Y")');
						END IF; 

						IF  SVOT_Condition IS  NULL OR SVOT_Condition =''
							THEN
								SET @Advance_SVOT_Condition='SELECT 1 from DUAL';
						ELSE		
								Set @TriggerSVOT_Condition=SVOT_Condition;
								SET @Advance_SVOT_Condition = CONCAT('SELECT 1 from V_CLM_Bill_Detail_One_View ADSVOT  WHERE ',@TriggerSVOT_Condition,')');
						END IF; 

						
								
									SET @sql_stmt = CONCAT('SELECT Convert(format(Count(1),","),CHAR)  INTO @temp_result 
																FROM V_CLM_Customer_One_View 
																WHERE  ',vSQLCondition,' 
																	AND EXISTS (',@SVOT_Condition_Campaign,')
																	AND EXISTS (',@SVOT_Condition_Region,')
																	AND EXISTS (',@SVOT_Condition_Valid_Mobile,')
																	AND EXISTS (',@SVOT_Condition_Valid_Email,')
																	AND EXISTS (',@SVOT_Condition_DND,')
																	AND EXISTS (',@Advance_SVOT_Condition,')
																
				
															
															');
												
						
	                    Select @sql_stmt;
	                    PREPARE stmt FROM @sql_stmt;
	                    EXECUTE stmt; 
	                    DEALLOCATE PREPARE stmt;


	                    Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message",@temp_result) )),"]") into @temp_result;') ;
	                                   
	                    SELECT @Query_String;
	                    PREPARE statement from @Query_String;
	                    Execute statement;
	                    Deallocate PREPARE statement; 

	              ELSE
	                          Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Invalid Parameters:-Please Select Lifecyle_Segment") )),"]") into @temp_result                       
	                                                                                  ;') ;
	                                   
	                           SELECT @Query_String;
	                           PREPARE statement from @Query_String;
	                           Execute statement;
	                           Deallocate PREPARE statement; 
	                                                


	             End If;

	        End If;
	        
	Else     Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Invalid Parameters:-Please Check the RFMP Condition ") )),"]") into @temp_result                       
	                                                                                  ;') ;
	         SELECT @Query_String;
	         PREPARE statement from @Query_String;
	         Execute statement;
	         Deallocate PREPARE statement; 

	End If;


	Select IN_AGGREGATION_1;
	If IN_AGGREGATION_1 ="Element_Coverage" 
	    Then 

			If IN_AGGREGATION_2="SVOC"
				Then
						Set @Message =0;
	                    SET @sql_stmt = CONCAT('SELECT Convert(format(Count(1),","),CHAR) INTO @Message FROM V_CLM_Customer_One_View WHERE ',IN_AGGREGATION_3);
	                    
	                    Select @sql_stmt;
	                    PREPARE stmt FROM @sql_stmt;
	                    EXECUTE stmt; 
	                    DEALLOCATE PREPARE stmt;
			End If;
				
			If IN_AGGREGATION_2="SVOT"
				THen
						Set @Message =0;
	                    SET @sql_stmt = CONCAT('SELECT Convert(format(Count(1),","),CHAR) INTO @Message FROM V_CLM_Bill_Detail_One_View WHERE ',IN_AGGREGATION_3);
	                    
	                    Select @sql_stmt;
	                    PREPARE stmt FROM @sql_stmt;
	                    EXECUTE stmt; 
	                    DEALLOCATE PREPARE stmt;
			End If;
			

                    Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message",@Message) )),"]") into @temp_result;') ;
	                                   
	                    SELECT @Query_String;
	                    PREPARE statement from @Query_String;
	                    Execute statement;
	                    Deallocate PREPARE statement; 			
	End If;

	
	
	Select IN_AGGREGATION_1;
	If IN_AGGREGATION_1 ="Update_Coverage" 
	    Then 

			If IN_AGGREGATION_2="CAMPAIGNSEGMENT"
				Then
						Select MSReplacedQuery into @Campaign_SegmentReplacedQuery  from CLM_MicroSegment where MicroSegmentId=IN_AGGREGATION_3;
						If @Campaign_SegmentReplacedQuery is Null or @Campaign_SegmentReplacedQuery ='' 
							Then Set @Campaign_SegmentReplacedQuery="1=1";
						End If;
						
						Select MSReplacedQueryBill into @Campaign_SegmentBillQuery  from CLM_MicroSegment where MicroSegmentId=IN_AGGREGATION_3;

						IF  @Campaign_SegmentBillQuery IS NULL OR @Campaign_SegmentBillQuery = ''
							THEN
								SET @SVOT_Condition_Campaign='SELECT 1 from DUAL';
						ELSE
							SET @SVOT_Condition_Campaign = CONCAT('SELECT 1 from V_CLM_Bill_Detail_One_View svot  WHERE ',@Campaign_SegmentBillQuery,'');
						END IF;

						SET @sql_stmt = CONCAT('Update CLM_MicroSegment
						Set Coverage =(Select Count(1) from V_CLM_Customer_One_View where ',Campaign_SegmentReplacedQuery,'  AND EXISTS (',@SVOT_Condition_Campaign,'))
						where MicroSegmentId=',IN_AGGREGATION_3);
						
						Select @sql_stmt;
	                    PREPARE stmt FROM @sql_stmt;
	                    EXECUTE stmt; 
	                    DEALLOCATE PREPARE stmt;
						
	                    Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Success") )),"]") into @temp_result;') ;
	                                   
	                    SELECT @Query_String;
	                    PREPARE statement from @Query_String;
	                    Execute statement;
	                    Deallocate PREPARE statement; 
			End If;
				
			If IN_AGGREGATION_2="LIFECYCLESEGMENT"
				THen
						Select CLMSegmentReplacedQuery into @Lifecycle_SegmentReplacedQuery  from CLM_Segment where CLMSegmentId=IN_AGGREGATION_3;
	
	                    SET @sql_stmt = CONCAT('Update CLM_Segment
						Set Coverage =(Select Count(1) from V_CLM_Customer_One_View where ',@Lifecycle_SegmentReplacedQuery,')
						where CLMSegmentId=',IN_AGGREGATION_3);
						
						Select @sql_stmt;
	                    PREPARE stmt FROM @sql_stmt;
	                    EXECUTE stmt; 
	                    DEALLOCATE PREPARE stmt;
						
	                    Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Success") )),"]") into @temp_result;') ;
	                                   
	                    SELECT @Query_String;
	                    PREPARE statement from @Query_String;
	                    Execute statement;
	                    Deallocate PREPARE statement;
			End If;
			
			If IN_AGGREGATION_2="REGION"
				THen
					Select RegionReplacedQuery into @Region_SegmentReplacedQuery  from CLM_RegionSegment where RegionSegmentId=IN_AGGREGATION_3;
					
					If @Region_SegmentReplacedQuery is Null or @Region_SegmentReplacedQuery=''
							Then Set @Region_SegmentReplacedQuery="1=1";
					End If;
					
					Select RegionReplacedQueryBill into @Region_SegmentBillQuery  from CLM_RegionSegment where RegionSegmentId=IN_AGGREGATION_3;
	
						IF  @Region_SegmentBillQuery IS NULL OR @Region_SegmentBillQuery = ''
							THEN
								SET @SVOT_Condition_Region='SELECT 1 from DUAL';
						ELSE
							SET @SVOT_Condition_Region = CONCAT('SELECT 1 from V_CLM_Bill_Detail_One_View svot  WHERE svot.Csutomer_id=svoc.Customer_id and ',@Region_SegmentBillQuery,'');
						END IF;	


	                SET @sql_stmt = CONCAT('Update CLM_RegionSegment
					Set Coverage =(Select Count(1) from V_CLM_Customer_One_View where ',Region_SegmentReplacedQuery,'  AND EXISTS (',@SVOT_Condition_Region,'))
					where RegionSegmentId=',IN_AGGREGATION_3);
						
					Select @sql_stmt;
	                PREPARE stmt FROM @sql_stmt;
	                EXECUTE stmt; 
	                DEALLOCATE PREPARE stmt;
						
	                Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Success") )),"]") into @temp_result;') ;
	                                   
	                SELECT @Query_String;
	                PREPARE statement from @Query_String;
	                Execute statement;
	                Deallocate PREPARE statement;
			End If;
			

                  			
	End If;
	
	If IN_AGGREGATION_1 ="Update_Event_Coverage" 
	    Then 
				
				Select Replaced_Query into SVOC_Condition from Event_Master where Id=IN_AGGREGATION_2;
                SELECT @SVOC_Condition;
				Select Replaced_Query_Bill into @SVOT_Condition from Event_Master where Id=IN_AGGREGATION_2;
				
				If SVOC_Condition is Null or SVOC_Condition='' then Set SVOC_Condition='1=1'; end if;
				
				If SVOT_Condition is Null or SVOT_Condition=''
				then 
				Set SVOT_Condition1='SELECT 1 from DUAL'; 
				else  SET SVOT_Condition1 = CONCAT('SELECT 1 from V_CLM_Bill_Detail_One_View rsvot  WHERE rsvot.Customer_id=V_CLM_Customer_One_View.Customer_id and  ',SVOT_Condition,'');
				end if; 
				
				
				
				
				SELECT Count(1)into Number_of_filters FROM Exclusion_Filter;
	
				IF Number_of_filters =0 Then 
					Select "NO Exclusion Filter Condition Is Selected"; 
					
					SET SQL_SAFE_UPDATES=0;
				Set @Query_String= concat('Update Event_Master_UI
				set Coverage= (Select Count(1) from V_CLM_Customer_One_View
								where ',SVOC_Condition,'   
								AND EXISTS(',SVOT_Condition1,')								
									)
					WHERE Event_id=',IN_AGGREGATION_2,'	;') ;
	                                   
	                SELECT @Query_String;
	                PREPARE statement from @Query_String;
	                Execute statement;
	                Deallocate PREPARE statement;
				
					
					
					
					
				END IF;
				
				IF Number_of_filters !=0 Then 
					Select GROUP_CONCAT(`Condition` SEPARATOR ' and  ') into Exclusion_Filter_Condition from Exclusion_Filter;
					
					SET SQL_SAFE_UPDATES=0;
				Set @Query_String= concat('Update Event_Master_UI
				set Coverage= (Select Count(1) from V_CLM_Customer_One_View
								where ',SVOC_Condition,'
								and Customer_Id not in (Select Customer_Id from V_CLM_Customer_One_View where ',Exclusion_Filter_Condition,')
									AND EXISTS(',SVOT_Condition1,')								
									)
					WHERE Event_id=',IN_AGGREGATION_2,'	;') ;
	                                   
	                SELECT @Query_String;
	                PREPARE statement from @Query_String;
	                Execute statement;
	                Deallocate PREPARE statement;
				
				END IF;
				
				
				
				
				
				
				
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Success") )),"]") into @temp_result;') ;
	                                   
	                SELECT @Query_String;
	                PREPARE statement from @Query_String;
	                Execute statement;
	                Deallocate PREPARE statement;
	
	END IF;	
	
	If IN_AGGREGATION_1 ="Check_Condition" 
	    Then 

		
			Set @Lifecycle_SegmentReplacedQuery='';
			Set @Campaign_SegmentReplacedQuery  ='' ;
      		Set  @Region_SegmentReplacedQuery  ='' ;
			Set @Region_SegmentBillQuery  ='' ;
      
			If Lifecycle_Segment != "All_segment"
					Then 
					 
						Set @SQL1=Concat('Select GROUP_CONCAT("(", `CLM_Segment`.`CLMSegmentReplacedQuery`, ")"SEPARATOR " OR ")
									into @Lifecycle_SegmentReplacedQuery  from CLM_Segment where CLMSegmentId in (',Lifecycle_Segment,');
									');
						SELECT @SQL1;
						PREPARE statement from @SQL1;
						Execute statement;
						Deallocate PREPARE statement; 
														
				Else  Set @Lifecycle_SegmentReplacedQuery='1=1';
			End If;	
	
	Select Campaign_Segment;
			Select GROUP_CONCAT('(', `Campaign_Segment`.`Segment_Condition_Solus`, ')'SEPARATOR ' OR ') into @Campaign_SegmentReplacedQuery  from Campaign_Segment where Segment_Id=Campaign_Segment;
    
	Select @Campaign_SegmentReplacedQuery;
			IF  @Campaign_SegmentReplacedQuery  is Null or  @Campaign_SegmentReplacedQuery  ='' 
				Then  Set @Campaign_SegmentReplacedQuery='1=1';
			 End If;
     
			
			Set @Region_SegmentReplacedQuery  ='' ;	
			Set @Region_SegmentBillQuery  ='' ;
			Set @Region_SegmentBillQuery_1  ='' ;
			
			Select RegionReplacedQuery into @Region_SegmentReplacedQuery  from CLM_RegionSegment where RegionSegmentId=Region;
			Select RegionReplacedQueryBill into @Region_SegmentBillQuery  from CLM_RegionSegment where RegionSegmentId=Region;
    
			IF  @Region_SegmentReplacedQuery  is Null or  @Region_SegmentReplacedQuery  ='' 
				Then  Set @Region_SegmentReplacedQuery='1=1';
			End If;
     
			IF  @Region_SegmentBillQuery  is Null or  @Region_SegmentBillQuery  ='' 
				Then  Set @Region_SegmentBillQuery_1='SELECT 1 from DUAL';
				
					ELSE SET @Region_SegmentBillQuery_1 = CONCAT('SELECT 1 from V_CLM_Bill_Detail_One_View svot WHERE svot.Customer_id=V_CLM_Customer_One_View.Customer_id and ',Region_SegmentBillQuery,'');
			End If;
     
			Set @SVOT_Condition_1='';
			
			IF  SVOT_Condition  is Null or  SVOT_Condition  ='' 
				Then  Set @SVOT_Condition_1='SELECT 1 from DUAL';
        
				ELSE SET @SVOT_Condition_1 = CONCAT('SELECT 1 from V_CLM_Bill_Detail_One_View svot WHERE svot.Customer_id=V_CLM_Customer_One_View.Customer_id and ',SVOT_Condition,'');
			
			End If;
     
			Set vSQLCondition='';

			SET @Campaign_SegmentReplacedQuery = CONCAT('( ',@Campaign_SegmentReplacedQuery,' )');
			SET @Region_SegmentReplacedQuery = CONCAT('( ',@Region_SegmentReplacedQuery,' )');
			SET @Lifecycle_SegmentReplacedQuery = CONCAT('( ',@Lifecycle_SegmentReplacedQuery,' )');
			
			SET vSQLCondition=concat(@Replaced_Query,'  and ', @Campaign_SegmentReplacedQuery,' and ', @Region_SegmentReplacedQuery,' and ',@Lifecycle_SegmentReplacedQuery) ;
			
			SET @sql_stmt = CONCAT('SELECT Distinct 1  INTO @temp_result 
								FROM V_CLM_Customer_One_View 
								WHERE  ',vSQLCondition,'
										AND EXISTS (',@Region_SegmentBillQuery_1,')
										AND EXISTS (',@SVOT_Condition_1,')	');
												
						
	        Select @sql_stmt;
	        PREPARE stmt FROM @sql_stmt;
	        EXECUTE stmt; 
	        DEALLOCATE PREPARE stmt;
			
			IF @temp_result =1
			Then
					
				Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Success") )),"]") into @temp_result;') ;
										   
				SELECT @Query_String;
				PREPARE statement from @Query_String;
				Execute statement;
				Deallocate PREPARE statement;
			
			End If;	
	End If;
	if @temp_result is null
        Then
                                
            Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Invalid Parameters") )),"]") into @temp_result;') ;
   
            SELECT @Query_String;
            PREPARE statement from @Query_String;
            Execute statement;
            Deallocate PREPARE statement; 
                        
    End if;
                            
    SET out_result_json = @temp_result;


END$$
DELIMITER ;
