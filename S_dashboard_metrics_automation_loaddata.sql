DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_metrics_automation_loaddata`(IN IN_SCREEN varchar(128), IN IN_MEASURE varchar(128), IN IN_COMPONENT varchar(128), IN IN_FILTER_CURRENTMONTH text)
BEGIN

DECLARE V_Screen_Name varchar(50);
DECLARE V_Measure_Name Varchar(50);
DECLARE V_Graph_Header_Name varchar(50);
DECLARE V_Component_Type varchar(50);
DECLARE V_Xaxis Varchar(500);
DECLARE V_Yaxis1 Varchar(500);
DECLARE V_Yaxis2 varchar(50);
DECLARE V_Yaxis3 Varchar(50);
DECLARE V_YAxis_stmt text;
DECLARE Loop_Check_Var Int Default 0;

DECLARE cur_Yaxis
    CURSOR For SELECT Screen_Name, Measure_Name, Graph_Header_Name, Component_Type, Xaxis, Yaxis1, Yaxis2
FROM
    Dashboard_Metrics_Defination
    WHERE Screen_Name = IN_SCREEN
    AND Measure_Name = IN_MEASURE
    AND Component_Type = IN_COMPONENT;
  
DECLARE CONTINUE HANDLER FOR NOT FOUND SET Loop_Check_Var=1;

    SELECT IN_FILTER_CURRENTMONTH;
    
    SET @IN_FILTER_CURRENTMONTH = IN_FILTER_CURRENTMONTH;

	SET Loop_Check_Var=0;
		OPEN cur_Yaxis;
		LOOP_ALL_METRICS : Loop
			FETCH cur_Yaxis into V_Screen_Name,V_Measure_Name,V_Graph_Header_Name,V_Component_Type,V_Xaxis,V_Yaxis1,V_Yaxis2;
			IF Loop_Check_Var THEN    
				LEAVE LOOP_ALL_METRICS;
			END IF;
        
        SELECT V_Screen_Name, V_Measure_Name, V_Graph_Header_Name, V_Component_Type, V_Xaxis, V_Yaxis1, V_Yaxis2;
        
        SET @V_YAxis_stmt = concat("concat(",concat("'",V_Yaxis1,"'"),")");
        
        set @YAxis_exec_stmt = concat('select ',@V_YAxis_stmt,' into @YAxis_exec_stmt1');
        
		prepare stmt1 from @YAxis_exec_stmt;
		execute stmt1;
        
        set @view_stmt= CONCAT('
							INSERT into Dashboard_Response_Temp
							SELECT 
								`Screen_Name`,
								`Measure_Name`,
								`Graph_Header_Name`,
								`Component_Type`,
								`Xaxis`,
								(',@YAxis_exec_stmt1,') AS Yaxis1,
								`Yaxis2`,
								`Yaxis3`
							FROM
								Dashboard_Metrics_Defination
                            WHERE Screen_Name = "',V_Screen_Name,'"
							AND Measure_Name = "',V_Measure_Name,'"
                            AND Component_Type = "',V_Component_Type,'"
                            AND Xaxis = "',V_Xaxis,'"');
		
		prepare stmt1 from @view_stmt;
		execute stmt1;
		deallocate prepare stmt1;
        
        END LOOP LOOP_ALL_METRICS;
		CLOSE cur_Yaxis;

    
END$$
DELIMITER ;
