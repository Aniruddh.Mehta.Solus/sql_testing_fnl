
CREATE or REPLACE PROCEDURE `S_CLM_Update_Segment`()
begin

	DECLARE vStart_Cnt BIGINT DEFAULT 0;
    DECLARE vEnd_Cnt BIGINT DEFAULT 0;
	DECLARE vBatchSize BIGINT DEFAULT 100000;
    DECLARE done1,done2 INT DEFAULT FALSE;
    DECLARE CLMSegmentId_cur1 int;
    DECLARE CLMSegmentReplacedQuery_cur1 TEXT;
    Declare cur1 cursor for
    select distinct CLMSegmentId,CLMSegmentReplacedQuery from CLM_Segment
    where CLMSegmentReplacedQuery <> '';
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done1 = TRUE;
    SET done1 = FALSE;
    SET SQL_SAFE_UPDATES=0;
    set foreign_key_checks=0;
	SELECT Customer_Id INTO @Rec_Cnt FROM CDM_Customer_Master ORDER BY Customer_Id DESC LIMIT 1;
    open cur1;
	new_loop_1: LOOP
					FETCH cur1 into CLMSegmentId_cur1,CLMSegmentReplacedQuery_cur1;
                    IF done1 THEN
			 LEAVE new_loop_1;
		END IF;
        SET vStart_Cnt = 0;
PROCESS_LOOP: LOOP
			SET vEnd_Cnt = vStart_Cnt + vBatchSize;
        insert into log_solus(module,rec_start,rec_end)values('Update_Segment_Id',vStart_Cnt,vEnd_Cnt);
        select CLMSegmentId_cur1,CLMSegmentReplacedQuery_cur1;

        insert into log_solus(module, rec_start) values ('Update_Segment',CLMSegmentId_cur1);
        
        set @sqlCust = concat( 'update Event_Execution_History
        set Segment_Id = ',CLMSegmentId_cur1,' where Customer_Id between ',vStart_Cnt,' and ',vEnd_Cnt,' and Customer_Id in (select Customer_Id from Customer_One_View where ',CLMSegmentReplacedQuery_cur1,' and Customer_Id between ',vStart_Cnt,' and ',vEnd_Cnt,' ) and Segment_Id = -1');
	SELECT @sqlCust;
				prepare stmt1 from @sqlCust;
				execute stmt1;
				deallocate prepare stmt1;
				SET vStart_Cnt=vEnd_Cnt+1;
        IF vStart_Cnt  >= @Rec_Cnt THEN
            LEAVE PROCESS_LOOP;
        END IF;         
 END LOOP PROCESS_LOOP;
        
	end LOOP;
	close cur1;
    
END

