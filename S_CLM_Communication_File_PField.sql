
CREATE OR REPLACE PROCEDURE `S_CLM_Communication_File_PField`(IN OnDemandEventId bigint(20), IN RUN_MODE Varchar(20), OUT Out_FileName text, OUT Out_FileHeader text, OUT Out_FileLocation text)
BEGIN
DECLARE done,done1 INT DEFAULT FALSE;
DECLARE vTempelate TEXT DEFAULT NULL;
DECLARE vTemplate_Original TEXT DEFAULT NULL;
DECLARE V_ID BIGINT(20);
DECLARE V_Communication_Channel varchar(255);
DECLARE V_File_Header varchar(512);
DECLARE vMicrosite_Template TEXT DEFAULT '';
DECLARE vMicrosite_Header VARCHAR(512) DEFAULT '';
DECLARE Exe_ID BIGINT(20);
DECLARE vMicro_Start_Cnt BIGINT(20) DEFAULT -1;
DECLARE output1 TEXT;
Declare cur1 CURSOR for 
select distinct ct.Template,ct.ID,ct.Communication_Channel,ct.Header,ct.Microsite_Template,ct.Microsite_Header from Event_Execution ee,Communication_Template ct where ct.ID=ee.Communication_Template_ID
	AND ee.Is_Target = 'Y'   
    AND ee.In_Control_Group is null 
    AND ee.In_Event_Control_Group is null ;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done1 = TRUE;
SET done1 = FALSE;
SET SQL_SAFE_UPDATES=0;
set foreign_key_checks=0;
INSERT into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
		values ('S_CLM_Communication_File', RUN_MODE ,now(),null,'Started',090921);
SET Exe_ID = F_Get_Execution_Id();   
select * from ETL_Execution_Details Order by Execution_Id DESC LIMIT 1;

SELECT Customer_id INTO @Rec_Cnt FROM Event_Execution ORDER BY Customer_id DESC LIMIT 1;
SELECT Customer_id INTO @vStart_CntCust FROM Event_Execution ORDER BY Customer_id ASC LIMIT 1;
SELECT ifnull(value,500000) into @vBatchSizeCust from M_Config where name='Communication_File_Batch_Size';
set @vBatchSizeCust=ifnull(@vBatchSizeCust,500000);

SET done1 = FALSE;

OPEN cur1;
LOOP_COMMTEMPLATES: LOOP
	FETCH cur1 into vTemplate_Original,V_ID,V_Communication_Channel,V_File_Header,vMicrosite_Template,vMicrosite_Header;
	IF done1 THEN
		LEAVE LOOP_COMMTEMPLATES;
	END IF;

    
    SELECT ifnull(Value,'FIRST') into @conf from M_Config where Name = 'Event_Execution_Id_Position';
	SET @conf=ifnull(@conf,'FIRST');
	IF @conf <> 'LAST' 
    THEN
		SET vTempelate=CONCAT('<SOLUS_PFIELD>Event_Execution_Id</SOLUS_PFIELD>|',vTemplate_Original);
		SET @Out_File_Header = concat('"Event_Execution_ID,',replace(V_File_Header,'|',','),'"');
    ELSE
        SET vTempelate=CONCAT(vTemplate_Original,'|<SOLUS_PFIELD>Event_Execution_Id</SOLUS_PFIELD>');
        SET @Out_File_Header = concat('"',replace(V_File_Header,'|',','),',Event_Executon_ID"');
    END IF;
    
    select '55',done1;

    
    if vTempelate = '' or vTempelate = '""' or vTempelate is NULL
    THEN
		SET @vPFields='';
	ELSE 
		select concat('concat("',replace(replace(replace(vTempelate,'|','"),concat("'),'<SOLUS_PFIELD>','",'),'</SOLUS_PFIELD>',',"'),'")') into @vPFields;
	END IF;
    

    
    if @vPFields <> '""' and @vPFields <> '' and @vPFields <> 'concat("")'
	THEN
		
		CALL PROC_CLM_Extract_All_Col_Name_From_Comm_Template(replace(replace(vTempelate,'</SOLUS_PFIELD>',") is NULL or </SOLUS_PFIELD>"),'<SOLUS_PFIELD>','<SOLUS_PFIELD>trim('),'<SOLUS_PFIELD>','</SOLUS_PFIELD>', @vPField_For_Check1);
        CALL PROC_CLM_Extract_All_Col_Name_From_Comm_Template(replace(replace(vTempelate,'</SOLUS_PFIELD>',") = '' or </SOLUS_PFIELD>"),'<SOLUS_PFIELD>','<SOLUS_PFIELD>trim('),'<SOLUS_PFIELD>','</SOLUS_PFIELD>', @vPField_For_Check2);
        
		SET @PField_Check = CONCAT(REPLACE(@vPField_For_Check1,' ,',' '),' 1<>1 or ', REPLACE(@vPField_For_Check2,' ,',' '),' 1<>1');
        
		SET @sql0=concat('insert ignore into Event_Execution_Rejected (select * from Event_Execution ee where exists (select 1 from Customer_Details_View cdv where cdv.Customer_Id = ee.Customer_Id and ( ',@PField_Check,' ) ) and ee.In_Control_Group is null and ee.In_Event_Control_Group is null and ee.Is_Target = "Y" and ee.Communication_Template_ID= ',V_ID,' )');
        SELECT V_ID as 'Template_ID',@sql0 as 'PField_Check';
		PREPARE stmt from @sql0;        
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;
        
        Update Event_Execution_Rejected eer,Event_Execution ee
        SET eer.Message='Personalised Field Not Available'
        where eer.Event_Execution_ID=ee.Event_Execution_ID;
        
		DELETE FROM Event_Execution where Event_Execution_ID in (Select Event_Execution_ID from Event_Execution_Rejected);
	END IF;
	SELECT `Value` INTO @Out_FileLocation FROM M_Config WHERE `Name` = 'File_Path';
    

    SELECT Customer_id INTO @vStart_CntCust FROM Event_Execution ORDER BY Customer_id ASC LIMIT 1;
	PROCESS_ALL_CUSTOMERS: LOOP
		SET @vEnd_CntCust = @vStart_CntCust + @vBatchSizeCust;
        
        select `Value` into @Channel_Check from M_Config where `Name`='Keep_Channel_Config_Dynamic';
        if @Channel_Check ='Y' then
		CALL S_CLM_Custom_GenFileName_Custom(RUN_MODE,V_ID ,V_Communication_Channel, @vEnd_CntCust, @Out_FileName, @Out_FileDelimiter,@OUT_MarkerFile);
        else
        CALL S_CLM_Custom_GenFileName(RUN_MODE,V_ID ,V_Communication_Channel, @vEnd_CntCust, @Out_FileName, @Out_FileDelimiter,@OUT_MarkerFile);
        end if;
        SET @Out_File_Header = REPLACE(@Out_File_Header,',',@Out_FileDelimiter);
        IF RUN_MODE = 'TEST' OR RUN_MODE = 'TEST_ALL'
        THEN
			SET @Out_FileName = CONCAT(@Out_FileName,current_time()+0,'.test');
		END IF;
        
        set @PField_msg = replace(@vPFields,",concat",(concat(",'",@Out_FileDelimiter,"',concat")));  
        
		SET @vStart_CntCust_Update=@vStart_CntCust;
        SET @Rec_Cnt_Update=@vEnd_CntCust;
        SET @vBatchSize=10000;
		
		PROCESS_ALL_CUSTOMERS_UPDATE: LOOP
			SET @vEnd_CntCust_Update = @vStart_CntCust_Update +  @vBatchSize;
            
            
            
             delete from Event_Execution_Bitly_AutoIncrId where 1=1;
            
            insert ignore into Event_Execution_Bitly_AutoIncrId(Event_Execution_Id) select Event_Execution_Id from Event_Execution
            where Customer_Id between @vStart_CntCust_Update and @vEnd_CntCust_Update
            and Communication_Template_Id=V_ID;
            
            update Event_Execution x, Event_Execution_Bitly_AutoIncrId y
            set x.Microsite_URL=concat(GETSHORTURL(x.Event_ID),ConvertIntegerToBase36(y.id))
            where x.Event_Execution_Id=y.Event_Execution_Id;

            
            
            
			set @sql_msg = concat(" 
				update Event_Execution tgt, 
				(select concat(",@PField_msg,") as val, Customer_Id, ID
							from Customer_Details_View  cdv
							where cdv.Customer_Id between ",@vStart_CntCust_Update," and ",@vEnd_CntCust_Update,"  
							and cdv.ID =",V_ID," ) src
			set tgt.Message = src.val
			where tgt.Customer_Id = src.Customer_Id
			and tgt.Customer_Id between ",@vStart_CntCust_Update," and ",@vEnd_CntCust_Update,"  
			and src.ID =",V_ID) ;
			PREPARE stmt1 from @sql_msg;        
			EXECUTE stmt1;
			DEALLOCATE PREPARE stmt1;
			IF @vEnd_CntCust_Update >= @Rec_Cnt_Update
			THEN
			  LEAVE PROCESS_ALL_CUSTOMERS_UPDATE;
			END IF;
			SET @vStart_CntCust_Update = @vEnd_CntCust_Update+1;
		END LOOP PROCESS_ALL_CUSTOMERS_UPDATE;
		
       
		
		IF  RUN_MODE <> 'TEST' AND RUN_MODE <>'TEST_ALL'
		THEN
			insert into Event_Execution_History select * from Event_Execution where Customer_Id between @vStart_CntCust and @vEnd_CntCust and Is_Target = 'Y' and Communication_Template_ID=V_ID; 
			SET @check_campaign_history_condition=concat('Event_Execution_Id in ( select Event_Execution_Id from Event_Execution_History where Customer_Id between ',@vStart_CntCust,' and ',@vEnd_CntCust,')');
		ELSE
			SET @check_campaign_history_condition=' 1 = 1 ';
		END IF;
		SELECT `Value` into @Out_FileCharset from M_Config where `Name`='Default_Character_Set';
		
		 SELECT `Value` into @fields_config_check from M_Config where `Name`='Enclose_Output_in_Double_Quotes';
        	IF @fields_config_check='Y' then set @fields_config='FIELDS enclosed by \'\"\' escaped by \"\"';
		
		
        	ELSE set @fields_config='FIELDS escaped by \"\"' ;
        	END IF;

		SET @sqlGenerateFile=concat(' SELECT * INTO OUTFILE "',@Out_FileName,'" charset ',@Out_FileCharset,' ',@fields_config,' LINES TERMINATED BY "\n" FROM
        (SELECT ', @Out_File_Header,' UNION ALL  SELECT MESSAGE 
        FROM Event_Execution ee
		WHERE ee.Customer_Id between "',@vStart_CntCust,'" and "',@vEnd_CntCust,'"  
		AND ee.Communication_Template_ID =',V_ID,' 
		AND ee.In_Control_Group is null and ee.In_Event_Control_Group is null and ee.Is_Target = "Y"
		AND ee.Communication_Channel= "',V_Communication_Channel,'" 
		AND ',@check_campaign_history_condition,' )A');
		SET @sqlGenerateFile=ifnull(@sqlGenerateFile,'select "No Customers Selected"');
		IF @vEnd_CntCust >= @Rec_Cnt
		THEN
		  SELECT @sqlGenerateFile;
		END IF;
		PREPARE stmt from @sqlGenerateFile;        
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;
            select '162',done1;

		
		IF @OUT_MarkerFile <> '' THEN
			SET @sqlMarkerFile=concat('SELECT "" INTO OUTFILE "',@OUT_MarkerFile,'" FIELDS TERMINATED BY "|" LINES TERMINATED BY "\n"');
			PREPARE stmtsqlMarkerFile from @sqlMarkerFile;
			EXECUTE stmtsqlMarkerFile;
			DEALLOCATE PREPARE stmtsqlMarkerFile;
		END IF;
			
		INSERT into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
			values ('S_CLM_Communication_File',Concat("Communication File got generated at ",@Out_FileName),now(),now(),'Started',090921);
			
			
		   
        if vMicrosite_Template = '' or vMicrosite_Template = '""' or vMicrosite_Template is NULL
		THEN
			SET @vMicrosite_PFields='';
		ELSE
			select concat('concat("',replace(replace(replace(vMicrosite_Template,'|','"),concat("'),'<SOLUS_PFIELD>','",'),'</SOLUS_PFIELD>',',"'),'")') into @vMicrosite_PFields;
            
			SELECT CONCAT(@Out_FileName,'.microsite') into @Microsite_Out_FileName;
			IF RUN_MODE = 'TEST' OR RUN_MODE = 'TEST_ALL'
			THEN
				SET @Microsite_Out_FileName = CONCAT(@Microsite_Out_FileName,current_time()+0,'.test');
			END IF;
		END IF;
        
		
        IF @vMicrosite_PFields <> '' and @vMicrosite_PFields <>'""' and @vMicrosite_PFields <> 'concat("")'
        THEN
			SET @vMicro_Start = @vStart_CntCust;
			SET @vMicro_End = @vEnd_CntCust;
            SET @vMicrosite_Out_FileHeader = concat('"',replace(vMicrosite_Header,'|','","'),'"');
			PROCESS_ALL_CUSTOMERS_MICROSITE: LOOP
				SET vMicro_Start_Cnt= @vMicro_Start;
				SELECT COUNT(1) INTO @Check_Cust from Event_Execution ee where ee.Customer_Id = vMicro_Start_Cnt and ee.Communication_Template_ID=V_ID and ee.In_Control_Group is null and ee.In_Event_Control_Group is null;
				SET @Check_Cust = ifnull(@Check_Cust,0);
				IF @Check_Cust > 0
				THEN
					
                    select ConvertIntegerToBase36(replace(Event_Execution_Id,'-','')) into @shorturl from Event_Execution where Customer_Id = @vMicro_Start and Communication_Template_ID = V_ID;
                    select Event_Execution_Id into @eeid from Event_Execution where Customer_Id = @vMicro_Start and Communication_Template_ID = V_ID;
                    
					SET @Microsite_Out_FileName = CONCAT(@Out_FileLocation,'/',@eeid,'.html');
                    Select @Microsite_Out_FileName;
					SET @sql3=concat('Select ',@vMicrosite_PFields,'
								FROM Customer_Details_View cdv
						where cdv.Customer_Id in (select Customer_Id from Event_Execution where Customer_Id = ',@vMicro_Start,' ) AND
								cdv.Customer_Id = ',@vMicro_Start,'  
								and cdv.ID=',V_ID,' 
								and cdv.In_Control_Group is null and cdv.In_Event_Control_Group is null and cdv.Is_Target = "Y"
								and cdv.Communication_Channel= "',V_Communication_Channel,'"');							
										
					
                    
                    SET @sql4=concat(' ',@sql3,' INTO OUTFILE "',@Microsite_Out_FileName,'" FIELDS TERMINATED BY "',@Out_FileDelimiter,'" escaped by "\" LINES TERMINATED BY "\n"');
                    SET @sql4=ifnull(@sql4,'select "No Customers Selected"');
                    
					PREPARE stmt from @sql4;        
					execute stmt;
					deallocate prepare stmt;
				END IF;
				SET @vMicro_Start = @vMicro_Start+1;
				IF @vMicro_Start > @vMicro_End
				THEN
					LEAVE PROCESS_ALL_CUSTOMERS_MICROSITE;
				END IF;
			END LOOP PROCESS_ALL_CUSTOMERS_MICROSITE;
            SET @Client_Code=(SELECT `Value` from M_Config where `Name` ='Client_Code');
            SET @Client_Code=IFNULL(@Client_Code,'UNKNOWN');
            SET @Microsite_Out_MappingFileName = CONCAT(@Out_FileLocation,'/',V_ID,'_Mapping_File_',CAST(CURDATE() AS UNSIGNED),'.csv');
            SET @sql5=concat(' SELECT "Long_URL","Short_URL" UNION ALL select concat("https://s00.ai/","',@Client_Code,'/",Event_Execution_Id,".html"),concat("',@Client_Code,'/",ConvertIntegerToBase36(replace(Event_Execution_Id,"-","")),"") from Event_Execution where Communication_Template_Id = ',V_ID,' INTO OUTFILE "',@Microsite_Out_MappingFileName,'" FIELDS TERMINATED BY "',@Out_FileDelimiter,'" escaped by "\" LINES TERMINATED BY "\n"');
            SET @sql5=ifnull(@sql5,'select "No Customers Selected"');
            
					PREPARE stmt from @sql5;        
					execute stmt;
					deallocate prepare stmt;
		END IF;	
		IF @vEnd_CntCust >= @Rec_Cnt
		THEN
		  LEAVE PROCESS_ALL_CUSTOMERS;
		END IF;
		SET @vStart_CntCust = @vEnd_CntCust+1;
	END LOOP PROCESS_ALL_CUSTOMERS;
END LOOP LOOP_COMMTEMPLATES;
CLOSE cur1;

UPDATE ETL_Execution_Details SET Status = 'Succeeded', End_Time = NOW() WHERE Procedure_Name = 'S_CLM_Communication_File' AND Execution_ID = Exe_ID;
SELECT 
    `Value`
INTO @Out_FileLocation FROM
    M_Config
WHERE
    `Name` = 'File_Path'; 

	
SET Out_FileName=@Out_FileName;
SET Out_FileHeader=@Out_File_Header;
SET Out_FileLocation=@Out_FileLocation;
END

