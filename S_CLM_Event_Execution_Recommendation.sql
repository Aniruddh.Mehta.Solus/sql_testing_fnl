
CREATE or replace PROCEDURE `S_CLM_Event_Execution_Recommendation`(
IN Event_ID_cur1 BIGINT(20),
IN Communication_Template_ID_cur1 BIGINT(20),
IN vStart_CntCust BIGINT(20),
IN vEnd_CntCust BIGINT(20)
)
BEGIN
set @Event_ID_cur1=Event_ID_cur1;
set @Communication_Template_ID_cur1=Communication_Template_ID_cur1;
set @vStart_CntCust=vStart_CntCust;
set @vEnd_CntCust=vEnd_CntCust;

SELECT 
	Number_Of_Recommendation,
	Recommendation_Type,
	Exclude_Stock_out,
	Exclude_No_Image,
	Exclude_Recommended_X_Days,
	Exclude_Transaction_X_Days,
	Exclude_Never_Brought,
	Exclude_Recommended_SameLTD_FavCat,
    Recommendation_Filter_Logic
INTO @no_of_reco , @algo , @Exclude_Stock_out , @Exclude_No_Image , @Exclude_Recommended_X_Days , @Exclude_Transaction_X_Days , @Exclude_Never_Brought , @Exclude_Recommended_SameLTD_FavCat , @vRecommendation_Filter_Logic
FROM
	Communication_Template
WHERE
	ID = @Communication_Template_ID_cur1;
    
SET @sqlRecoCriteria=concat('Event: ',@Event_ID_cur1,'Template: ',@Communication_Template_ID_cur1,'NoOfReco: ',@no_of_reco ,'Algo: ', @algo ,'RecoFilterLogic: ', @vRecommendation_Filter_Logic ,'ExcludeStockOut: ', @Exclude_Stock_out ,'ExcludeNoImage: ', @Exclude_No_Image , 'ExcludeRecosLastXDays: ',@Exclude_Recommended_X_Days , 'ExcludeBoughtLastXDays: ',@Exclude_Transaction_X_Days , 'ExcludeNeverBought: ',@Exclude_Never_Brought , 'ExcludeLTDFavCat: ',@Exclude_Recommended_SameLTD_FavCat );


/** Set Algo Condition **/
IF @algo <> '"HYBRIDIZER"' 
THEN 
	IF @algo='"IBCF"' 					THEN SET @algo_condition= 'rcore.IBCF=1';
		ELSEIF  @algo='"GENOME"' 		THEN SET @algo_condition ='rcore.GENOME=1';
		ELSEIF  @algo='"NN"' 			THEN SET @algo_condition ='rcore.NN=1';
		ELSEIF  @algo='"PREFERENCE"' 	THEN SET @algo_condition ='rcore.PREFERENCE=1';
		ELSEIF  @algo='"TRENDING"' 		THEN SET @algo_condition ='rcore.TRENDING=1';
		ELSEIF  @algo='"TRENDINGSTLY"' 	THEN SET @algo_condition ='rcore.TRENDINGSTLY=1';
		ELSE 								 SET @algo_condition = ' 1 = 1 ';
	END IF;
ELSE 
	SET @algo_condition = ' 1 = 1 ';
END IF;     

/* Apply Exclusion Criteria - Exclude Image **/
IF @Exclude_No_Image=1 
THEN 
	set @prod_image_criteria='pm.Product_URL <> "" AND pm.Product_URL <> -1 AND pm.Product_URL is not null'; 
ELSE 
	set @prod_image_criteria='1=1'; 
END IF;

/* Apply Exclusion Criteria - Stock Out **/
if @Exclude_Stock_out=1 
then 
	set @inventory_cond = 'pm.Product_Id  in (select Product_Id from CDM_Inventory_Master where Stock_Qty>0)';
elseif @Exclude_Stock_out in (2,3,4) 
then
    if @Exclude_Stock_out=2 	then set @store_var='Ltd_Fav_Store_LOV_Id';
	elseif @Exclude_Stock_out=3 then set @store_var='AP_Fav_Store_LOV_Id';
	elseif @Exclude_Stock_out=4 then set @store_var='Lt_Fav_Store_LOV_Id';
    end if;
    set @inventory_cond=concat('exists(select 1 from V_CLM_Customer_One_View svoc ,CDM_Inventory_Master inv where
            svoc.Customer_Id=ee.Customer_Id
            and svoc.Customer_Id between ',@vStart_CntCust,' and ',@vEnd_CntCust,'
            and svoc.',@store_var,'=inv.Store_LOV_Id
            and inv.Stock_Qty>0
            and inv.Product_Id=pm.Product_Id)');
else set @inventory_cond='1=1';
end if;

  
/* Apply Exclusion Criteria - Recommendatin Filter **/
IF @vRecommendation_Filter_Logic <> '' 
THEN
	set @svoc_criteria='1=1';
	set @bill_criteria='1=1';
	set @campaign_hist_criteria='1=1';       
	set @Str=@vRecommendation_Filter_Logic;
	set @n_loop=(select length(@Str)-length(replace(@Str,'|','')))+1;
	set @loop_var=1;
    
	while @loop_var<=@n_loop
	do
		set @str1=substring_index(substring_index(@Str,'|',@loop_var),'|',-1);
		set @cond1= substring_index(substring_index(@str1,':::',2),':::',-1);
		set @cond2= substring_index(substring_index(@str1,':::',3),':::',-1);
		set @cond3= substring_index(substring_index(@str1,':::',4),':::',-1);
		set @cond4= substring_index(substring_index(@str1,':::',5),':::',-1);
		set @exists_operator=
			  case 
			  when @cond1='INCLUDE' then 'exists'
			  when @cond1='EXCLUDE' then  'not exists'
			  end;
		set @period=@cond2;
		set @behaviour=@cond3;
		set @attribute=@cond4;
		set @svoc_attrib=concat(@period,'_',@attribute);
		set @svocattributename=concat(@period,'_','Fav','_',@attribute,'_','LOV_Id');
		set @productattributename=concat(@attribute,'_','LOV_Id');
        
		if @behaviour='FAVOURITE' 
        then
			set @svoc_criteria=concat('',@exists_operator,'(
							select 1 from  V_CLM_Customer_One_View svoc, CDM_Product_Master pm1
			where svoc.',@svocattributename,'=pm1.',@productattributename,' AND
			svoc.Customer_Id between ',@vStart_CntCust,' and ',@vEnd_CntCust,' AND
			pm1.Product_Id=pm.Product_Id AND
			ee.Customer_Id=svoc.Customer_Id) AND ',@svoc_criteria,'');
            
		elseif @behaviour='TRANSACTION' 
        then
			set @bill_criteria= concat('',@exists_operator,'( 
							select 1 from CDM_Bill_Details bills,CDM_Product_Master pm2
							where (bills.Customer_Id between ',@vStart_CntCust,' and ',@vEnd_CntCust,')
							and bills.Bill_Date > date_sub(current_date(),interval ',@period,' day)
							and bills.Product_id = pm2.Product_id
							and bills.Customer_Id = rcore.Customer_Id
							and pm2.',@productattributename,'=pm.',@productattributename,'
							 ) AND ',@bill_criteria,'');
                             
		elseif @behaviour = 'COMMUNICATED' 
        then
			set @campaign_hist_criteria=concat('',@exists_operator,'(
						select 1
						from
						(select Customer_Id,
						Recommendation_Product_Id_1,
						Recommendation_Product_Id_2,
						Recommendation_Product_Id_3,
						Recommendation_Product_Id_4,
						Recommendation_Product_Id_5,
						Recommendation_Product_Id_6
						from Event_Execution_History where Customer_Id between ',@vStart_CntCust,' and ',@vEnd_CntCust,'
						and Event_Execution_Date_ID > date_format(date_sub(current_date(),interval ',@period,' day),"%Y%m%d")) as eeh_temp,
						CDM_Product_Master pm_e1, CDM_Product_Master pm_e2, CDM_Product_Master pm_e3 
						,CDM_Product_Master pm_e4, CDM_Product_Master pm_e5, CDM_Product_Master pm_e6
						where 
						(eeh_temp.Recommendation_Product_Id_1=pm_e1.Product_Id AND
						eeh_temp.Recommendation_Product_Id_2=pm_e2.Product_Id AND
						eeh_temp.Recommendation_Product_Id_3=pm_e3.Product_Id AND
						eeh_temp.Recommendation_Product_Id_4=pm_e4.Product_Id AND
						eeh_temp.Recommendation_Product_Id_5=pm_e5.Product_Id AND
						eeh_temp.Recommendation_Product_Id_6=pm_e6.Product_Id AND
						eeh_temp.Customer_Id = rcore.Customer_Id) AND
						(pm.',@productattributename,' in (ifnull(pm_e1.',@productattributename,',-1),ifnull(pm_e2.',@productattributename,',-1),ifnull(pm_e3.',@productattributename,',-1),ifnull(pm_e4.',@productattributename,',-1),ifnull(pm_e5.',@productattributename,',-1),ifnull(pm_e6.',@productattributename,',-1)))) AND  ',@campaign_hist_criteria,'');
			else 
				set @svoc_criteria='1=1';
				set @bill_criteria='1=1';
				set @campaign_hist_criteria='1=1';
                SELECT CONCAT('WARNING :Invalid Recommendation Filter Condition. Exclusion Filter is Skipped !! ',@vRecommendation_Filter_Logic);
		end if;
		set @loop_var=@loop_var+1;
	end while;	
    
	else 
		set @svoc_criteria='1=1';
		set @bill_criteria='1=1';
		set @campaign_hist_criteria='1=1';
END IF; 

SET @SQL2=concat('update Event_Execution ee1, 
	(select Customer_Id, 
	group_concat(recogenome) recogenome,
	group_concat(Product_Id) Product_Id,
	group_concat(Algo) Algo,
	rank1,
	Number_Of_Recommendation from (
	select
	rcore.Customer_Id as Customer_Id, 
	(pm.Product_Genome_LOV_Id) recogenome,
	pm.Product_Id Product_Id,
	dense_rank() over( partition by rcore.Customer_Id order by rcore.Hybridizer_Rank asc) rank1,
	ct.Number_Of_Recommendation as Number_Of_Recommendation,
	rcore.Algo
	from RankedPickList rcore, Event_Execution ee, CDM_Product_Master pm, Communication_Template ct
	where ee.Event_id = ',@Event_ID_cur1,'
	and ee.Customer_Id between ',@vStart_CntCust,' and ',@vEnd_CntCust,'
	and ee.Customer_Id=rcore.Customer_Id
	and pm.Product_Id=rcore.Product_Id 
	and ee.Communication_Template_Id = ct.ID
	-- recommedendation from a specific algo
	and ',@algo_condition,'
   -- SVoC_Condition
	and ',@svoc_criteria,'
   -- Bill_Condition
	and ',@bill_criteria,'
   -- Campaign_History_Condition
	and ',@campaign_hist_criteria,'
	-- Product_Image_Criteria
	and ',@prod_image_criteria,'
	-- Inventory_Condition
	and ',@inventory_cond,'
	) drvd_Tbl where rank1 <= Number_Of_Recommendation
	group by Customer_Id) temprecos
	SET ee1.Recommendation_Product_Id_6=substring_Index(substring_index(CONCAT(temprecos.Product_Id,",-1"),",",6),",",-1),
	ee1.Recommendation_Product_Id_5=substring_Index(substring_index(CONCAT(temprecos.Product_Id,",-1"),",",5),",",-1),
	ee1.Recommendation_Product_Id_4=substring_Index(substring_index(CONCAT(temprecos.Product_Id,",-1"),",",4),",",-1),
	ee1.Recommendation_Product_Id_3=substring_Index(substring_index(CONCAT(temprecos.Product_Id,",-1"),",",3),",",-1),
	ee1.Recommendation_Product_Id_2=substring_Index(substring_index(CONCAT(temprecos.Product_Id,",-1"),",",2),",",-1),
	ee1.Recommendation_Product_Id_1=substring_Index(substring_index(CONCAT(temprecos.Product_Id,",-1"),",",1),",",-1),
	ee1.Recommendation_Genome_Lov_Id_6=substring_Index(substring_index(CONCAT(temprecos.recogenome,",-1"),",",6),",",-1),
	ee1.Recommendation_Genome_Lov_Id_5=substring_Index(substring_index(CONCAT(temprecos.recogenome,",-1"),",",5),",",-1),
	ee1.Recommendation_Genome_Lov_Id_4=substring_Index(substring_index(CONCAT(temprecos.recogenome,",-1"),",",4),",",-1),
	ee1.Recommendation_Genome_Lov_Id_3=substring_Index(substring_index(CONCAT(temprecos.recogenome,",-1"),",",3),",",-1),
	ee1.Recommendation_Genome_Lov_Id_2=substring_Index(substring_index(CONCAT(temprecos.recogenome,",-1"),",",2),",",-1),
	ee1.Recommendation_Genome_Lov_Id_1=substring_Index(substring_index(CONCAT(temprecos.recogenome,",-1"),",",1),",",-1),
	ee1.Recommendation_Algo_6=substring_Index(substring_index(CONCAT(temprecos.Algo,",-1"),",",6),",",-1),
	ee1.Recommendation_Algo_5=substring_Index(substring_index(CONCAT(temprecos.Algo,",-1"),",",5),",",-1),
	ee1.Recommendation_Algo_4=substring_Index(substring_index(CONCAT(temprecos.Algo,",-1"),",",4),",",-1),
	ee1.Recommendation_Algo_3=substring_Index(substring_index(CONCAT(temprecos.Algo,",-1"),",",3),",",-1),
	ee1.Recommendation_Algo_2=substring_Index(substring_index(CONCAT(temprecos.Algo,",-1"),",",2),",",-1),
	ee1.Recommendation_Algo_1=substring_Index(substring_index(CONCAT(temprecos.Algo,",-1"),",",1),",",-1)
	where ee1.Event_id = ',@Event_ID_cur1,'
	and ee1.Customer_Id between ',@vStart_CntCust,' and ',@vEnd_CntCust,'
	and ee1.Customer_Id=temprecos.Customer_Id');
			
	IF @vStart_CntCust <= 0
	THEN
		select 'S_CLM_Event_Execution_Recommendation : ' as '', NOW() as '', concat (' RCORE FETCHING RECOMMENDATION !! ID:',@Event_ID_cur1) as '', ' Criteria: ',@sqlRecoCriteria as '', '  <RCORE><SQL> ' as '', @SQL2 as '',' </SQL></RCORE> ' as '';
	END IF;
	
	PREPARE stmt1 from @SQL2;
	EXECUTE stmt1;
	DEALLOCATE PREPARE stmt1;
    
SET sql_safe_updates=0;

-- NO of recommendations not matching with expected no of recommendations

UPDATE Event_Execution ee,
    Communication_Template ct 
SET 
	Message=@sqlRecoCriteria,
    Is_Target = CASE
        WHEN
            ct.Number_Of_Recommendation = 1
                AND Recommendation_Product_Id_1 IN (- 1 , '')
        THEN
            'A'
        WHEN
            ct.Number_Of_Recommendation = 2
                AND (Recommendation_Product_Id_1 IN (- 1 , '')
                OR Recommendation_Product_Id_2 IN (- 1 , ''))
        THEN
            'A'
        WHEN
            ct.Number_Of_Recommendation = 3
                AND (Recommendation_Product_Id_1 IN (- 1 , '')
                OR Recommendation_Product_Id_2 IN (- 1 , '')
                OR Recommendation_Product_Id_3 IN (- 1 , ''))
        THEN
            'A'
        WHEN
            ct.Number_Of_Recommendation = 4
                AND (Recommendation_Product_Id_1 IN (- 1 , '')
                OR Recommendation_Product_Id_2 IN (- 1 , '')
                OR Recommendation_Product_Id_3 IN (- 1 , '')
                OR Recommendation_Product_Id_4 IN (- 1 , ''))
        THEN
            'A'
        WHEN
            ct.Number_Of_Recommendation = 5
                AND (Recommendation_Product_Id_1 IN (- 1 , '')
                OR Recommendation_Product_Id_2 IN (- 1 , '')
                OR Recommendation_Product_Id_3 IN (- 1 , '')
                OR Recommendation_Product_Id_4 IN (- 1 , '')
                OR Recommendation_Product_Id_5 IN (- 1 , ''))
        THEN
            'A'
        WHEN
            ct.Number_Of_Recommendation = 6
                AND (Recommendation_Product_Id_1 IN (- 1 , '')
                OR Recommendation_Product_Id_2 IN (- 1 , '')
                OR Recommendation_Product_Id_3 IN (- 1 , '')
                OR Recommendation_Product_Id_4 IN (- 1 , '')
                OR Recommendation_Product_Id_5 IN (- 1 , '')
                OR Recommendation_Product_Id_6 IN (- 1 , ''))
        THEN
            'A'
        ELSE 'Y'
    END
WHERE
    ee.Communication_Template_ID = ct.ID
AND Communication_Template_ID = @Communication_Template_ID_cur1;


END


