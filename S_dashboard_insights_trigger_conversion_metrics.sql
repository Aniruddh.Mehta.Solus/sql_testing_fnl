DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_insights_trigger_conversion_metrics`(IN vBatchSize BIGINT)
BEGIN
DECLARE vStart_Cnt BIGINT DEFAULT 0;
DECLARE vEnd_Cnt BIGINT DEFAULT 0;
DECLARE vRec_Cnt BIGINT DEFAULT 0;
DECLARE Cursor_Check_Var Int Default 0;
DECLARE cur_Execution_Date_ID INT(11);
DECLARE cur_Event_Id INT(11);


DECLARE curALL_EVENTS 
    CURSOR For SELECT DISTINCT
    Event_Id
FROM
    Event_Execution_History A
    WHERE Event_Id <> 0
	ORDER BY Event_Id ASC;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET Cursor_Check_Var=1;

DELETE from log_solus where module = 'S_dashboard_insights_trigger_conversion_metrics';
SET SQL_SAFE_UPDATES=0;

    SELECT `Value` into @Online_Store from M_Config where `Name` = 'Online_Store_Id';
        
        SELECT @Online_Store;

SET @Start_Date = date_format(date_sub(concat(date_format(current_date(),"%Y%m"),'01'), INTERVAL 12 MONTH),"%Y%m");
SET @End_Date = date_format(current_date(),"%Y%m");

DROP TABLE IF EXISTS Dashboard_Insights_Trigger_Conversion_Customers_Temp;
  CREATE TABLE IF NOT EXISTS Dashboard_Insights_Trigger_Conversion_Customers_Temp ( 
  Event_ID int(11) NOT NULL,
  Event_Execution_Month int(6) NOT NULL,
  Transaction_Mode CHAR(10) NOT NULL,
  Bill_Header_Id bigint(20) NOT NULL
  ) ENGINE = INNODB DEFAULT CHARACTER SET=LATIN1; 
 
  ALTER TABLE Dashboard_Insights_Trigger_Conversion_Customers_Temp 
    ADD INDEX (Event_ID,Event_Execution_Month,Transaction_Mode);

SELECT 
    Customer_Id
INTO vRec_Cnt FROM
    Event_Execution_History
ORDER BY Customer_Id DESC
LIMIT 1;
	SET vStart_Cnt=0;
	SET vEnd_Cnt=0; 
LOOP_ALL_CUSTOMERS: LOOP
	SET vEnd_Cnt = vStart_Cnt + vBatchSize;
	SELECT vStart_Cnt, vEnd_Cnt, CURRENT_TIMESTAMP();
    IF vStart_Cnt  >= vRec_Cnt THEN
		LEAVE LOOP_ALL_CUSTOMERS;
	END IF;  
INSERT INTO log_solus(module, rec_start,rec_end,msg) values ('S_dashboard_insights_trigger_conversion_metrics',vStart_Cnt,vEnd_Cnt,'Loading Events for Customers');

INSERT INTO Dashboard_Insights_Trigger_Conversion_Customers_Temp
(
Event_ID,
Event_Execution_Month,
Transaction_Mode,
Bill_Header_Id
)
SELECT 
Event_ID, 
date_format(Event_Execution_Date_ID,"%Y%m") AS Event_Execution_Month,
'Online' AS Transaction_Mode,
Bill_Header_Id
FROM Event_Execution_History A
INNER JOIN CDM_Bill_Details B
ON A.Customer_Id = B.Customer_Id
AND date_format(A.Event_Execution_Date_ID,"%Y%m") = B.Bill_Year_Month
WHERE A.Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt
AND In_Control_Group IS NULL
AND Store_ID = @Online_Store;

INSERT INTO Dashboard_Insights_Trigger_Conversion_Customers_Temp
(
Event_ID,
Event_Execution_Month,
Transaction_Mode,
Bill_Header_Id
)
SELECT 
Event_ID, 
date_format(Event_Execution_Date_ID,"%Y%m") AS Event_Execution_Month,
'Offline' AS Transaction_Mode,
Bill_Header_Id
FROM Event_Execution_History A
INNER JOIN CDM_Bill_Details B
ON A.Customer_Id = B.Customer_Id
AND date_format(A.Event_Execution_Date_ID,"%Y%m") = B.Bill_Year_Month
WHERE A.Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt
AND In_Control_Group IS NULL
AND Store_ID <> @Online_Store;

SET vStart_Cnt = vEnd_Cnt;
END LOOP LOOP_ALL_CUSTOMERS;

INSERT INTO Dashboard_Insights_Trigger_Conversion
(
Event_ID,
Event_Execution_Month,
Transaction_Mode,
Bills
)
SELECT 
Event_ID, 
Event_Execution_Month,
Transaction_Mode,  
COUNT(DISTINCT Bill_Header_Id) AS Bills
FROM Dashboard_Insights_Trigger_Conversion_Customers_Temp
WHERE Event_Execution_Month BETWEEN @Start_Date AND @End_Date
GROUP BY 1,2,3;

SET Cursor_Check_Var = FALSE;
OPEN curALL_EVENTS;
LOOP_ALL_DATES: LOOP

	FETCH curALL_EVENTS into cur_Event_Id;
	IF Cursor_Check_Var THEN
	   LEAVE LOOP_ALL_DATES;
	END IF;

		Select cur_Event_Id,now();

INSERT INTO Dashboard_Insights_Trigger_Conversion
(
Event_ID,
Transaction_Mode,
Event_Execution_Month,
Bills_Col_Pct
)
SELECT
cur_Event_Id AS Event_ID,
Transaction_Mode,
Event_Execution_Month,
Bills AS Bills_Col_Pct
FROM
(
SELECT C.Event_ID, C.Transaction_Mode, C.Event_Execution_Month , Bills FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills', A.Event_ID, A.Transaction_Mode, A.Event_Execution_Month FROM
(
SELECT Event_ID, Transaction_Mode, Event_Execution_Month, Bills AS c1
FROM Dashboard_Insights_Trigger_Conversion
WHERE Event_Execution_Month BETWEEN @Start_Date AND @End_Date
AND Event_Id = cur_Event_Id
AND Transaction_Mode = 'Offline'
AND Bills IS NOT NULL
GROUP BY 1,2,3
)A
,
(
SELECT Event_Execution_Month,SUM(Bills) AS COUNT2 
FROM Dashboard_Insights_Trigger_Conversion
WHERE Event_Execution_Month BETWEEN @Start_Date AND @End_Date
AND Bills IS NOT NULL
AND Transaction_Mode = 'Offline'
GROUP BY 1
)B
WHERE A.Event_Execution_Month = B.Event_Execution_Month
) C
GROUP BY C.Event_Execution_Month
) AS T;

INSERT INTO Dashboard_Insights_Trigger_Conversion
(
Event_ID,
Transaction_Mode,
Event_Execution_Month,
Bills_Col_Pct
)
SELECT
cur_Event_Id AS Event_ID,
Transaction_Mode,
Event_Execution_Month,
Bills AS Bills_Col_Pct
FROM
(
SELECT C.Event_ID, C.Transaction_Mode, C.Event_Execution_Month , Bills FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills', A.Event_ID, A.Transaction_Mode, A.Event_Execution_Month FROM
(
SELECT Event_ID, Transaction_Mode, Event_Execution_Month, Bills AS c1
FROM Dashboard_Insights_Trigger_Conversion
WHERE Event_Execution_Month BETWEEN @Start_Date AND @End_Date
AND Event_Id = cur_Event_Id
AND Transaction_Mode = 'Online'
AND Bills IS NOT NULL
GROUP BY 1,2,3
)A
,
(
SELECT Event_Execution_Month,SUM(Bills) AS COUNT2 
FROM Dashboard_Insights_Trigger_Conversion
WHERE Event_Execution_Month BETWEEN @Start_Date AND @End_Date
AND Bills IS NOT NULL
AND Transaction_Mode = 'Online'
GROUP BY 1
)B
WHERE A.Event_Execution_Month = B.Event_Execution_Month
) C
GROUP BY C.Event_Execution_Month
) AS T;

INSERT INTO Dashboard_Insights_Trigger_Conversion
(
Event_ID,
Transaction_Mode,
Event_Execution_Month,
Bills_Row_Pct
)
SELECT
cur_Event_Id AS Event_ID, 
Transaction_Mode,
Event_Execution_Month,
Bills AS Bills_Row_Pct
FROM
(
SELECT C.Event_ID, C.Transaction_Mode, C.Event_Execution_Month , Bills FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills', A.Event_ID, A.Transaction_Mode, B.Event_Execution_Month FROM
(
SELECT Event_ID, Transaction_Mode, Event_Execution_Month, Bills AS c1
FROM Dashboard_Insights_Trigger_Conversion
WHERE Event_Execution_Month BETWEEN @Start_Date AND @End_Date
AND Event_Id = cur_Event_Id
AND Transaction_Mode = 'Offline'
AND Bills IS NOT NULL
GROUP BY 1,2,3
)A
,
(
SELECT Event_Execution_Month,SUM(Bills) AS COUNT2 
FROM Dashboard_Insights_Trigger_Conversion
WHERE Event_Id = cur_Event_Id
AND Event_Execution_Month BETWEEN @Start_Date AND @End_Date
AND Bills IS NOT NULL
GROUP BY 1
)B
WHERE A.Event_Execution_Month = B.Event_Execution_Month
) C
GROUP BY C.Event_Execution_Month
) AS T;

INSERT INTO Dashboard_Insights_Trigger_Conversion
(
Event_ID,
Transaction_Mode,
Event_Execution_Month,
Bills_Row_Pct
)
SELECT
cur_Event_Id AS Event_ID, 
Transaction_Mode,
Event_Execution_Month,
Bills AS Bills_Row_Pct
FROM
(
SELECT C.Event_ID, C.Transaction_Mode, C.Event_Execution_Month , Bills FROM 
(
SELECT ifnull(round((c1*100/COUNT2),1),0) AS 'Bills', A.Event_ID, A.Transaction_Mode, B.Event_Execution_Month FROM
(
SELECT Event_ID, Transaction_Mode, Event_Execution_Month, Bills AS c1
FROM Dashboard_Insights_Trigger_Conversion
WHERE Event_Execution_Month BETWEEN @Start_Date AND @End_Date
AND Event_Id = cur_Event_Id
AND Transaction_Mode = 'Online'
AND Bills IS NOT NULL
GROUP BY 1,2,3
)A
,
(
SELECT Event_Execution_Month,SUM(Bills) AS COUNT2 
FROM Dashboard_Insights_Trigger_Conversion
WHERE Event_Id = cur_Event_Id
AND Event_Execution_Month BETWEEN @Start_Date AND @End_Date
AND Bills IS NOT NULL
GROUP BY 1
)B
WHERE A.Event_Execution_Month = B.Event_Execution_Month
) C
GROUP BY C.Event_Execution_Month
) AS T;

END LOOP LOOP_ALL_DATES;

UPDATE Dashboard_Insights_Trigger_Conversion A, 
(
SELECT EventId, `CampTriggerName` FROM CLM_Campaign_Trigger
JOIN CLM_Campaign_Events
ON `CLM_Campaign_Events`.`CampTriggerId` = `CLM_Campaign_Trigger`.`CampTriggerId`
) B
SET A.`Trigger` = B.CampTriggerName
WHERE A.Event_ID = B.EventId;

END$$
DELIMITER ;
