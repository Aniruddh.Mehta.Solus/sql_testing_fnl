
CREATE OR REPLACE PROCEDURE `S_UI_Create_Coverage_View`()
BEGIN


/* Selecting all the columns which need to be displayed in the view*/
SELECT group_concat(case when Column_Name="Enrolled_Store_Id" then '(select
                `CDM_Store_Master`.`Store_Name`
            FROM
                `CDM_Store_Master`
            WHERE
                `CDM_Store_Master`.`Store_Id` =`CDM_CM`.`Enrolled_Store_Id`) as Enrolled_Store'
                else CONCAT('`',Table_Alias,'`','.','`',Column_Name,'`',' as ','`',Column_Name_in_One_View,'`') end separator ',') into @var_list2 FROM Customer_View_Config WHERE In_Use_Coverage_View in ('Y','Mandatory') and Is_LOV_Id='N' and Column_Name<>'Customer_Id';
                
set @comm1=',';

/*Selecting all the LOV Id columns */
select concat('',@comm1,'',group_concat(concat('(SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `',Table_Alias,'`.`',Column_Name,'`)', ' AS ', '`',Column_Name_in_One_View,'`') separator ',')) into @lov_var_list  FROM Customer_View_Config WHERE In_Use_Coverage_View in ('Y','Mandatory') and Is_LOV_Id='Y';


/*Selecting the brackets */
select group_concat(br SEPARATOR '') into @brackets2 from( 
select '(' as br from Customer_View_Config where In_Use_Coverage_View in ('Y','Mandatory') and Table_Name not in ('CDM_Customer_Time_Dependent_Var')
group by Table_Name)L;

/*Selecting all the tables to join*/
select group_concat(Join_Tables SEPARATOR ' ') into @table_joins2 from (
select concat('LEFT JOIN ','`',Table_Name,'`',' ','`',Table_Alias,'`',' ON(`',Table_Alias,'`.`',case when Table_Name='Client_SVOC' then 'Customer_Id_1' when Table_Name='Customer_Recos_View' then 'Customer_Id_2' else 'Customer_Id' end,'`=`CDM_CTDV`.`Customer_Id`))') as Join_Tables from Customer_View_Config where In_Use_Coverage_View in ('Y','Mandatory') and Table_Name not in ('CDM_Customer_Time_Dependent_Var')
group by Table_Name)L;




set @var_list2=ifnull(@var_list2,'');
set @brackets2=ifnull(@brackets2,'');
set @lov_var_list=ifnull(@lov_var_list,'');
set @table_joins2=ifnull(@table_joins2,'');


SET @s2 = Concat('CREATE OR REPLACE VIEW Customer_Coverage_View AS
                         SELECT 
                         `CDM_CTDV`.`Customer_Id` AS `Customer_Id`,
                         `CDM_CTDV`.`LifeCycleSegment_Id` AS `LifeCycleSegment_Id`,
			
                         ',@var_list2,'
                         ',@lov_var_list,'
						 FROM ',@brackets2,'
						 `CDM_Customer_Time_Dependent_Var` `CDM_CTDV`
						 ',@table_joins2,'
                         ');
       SELECT @s2;                 
		PREPARE stmt from @s2;        
		execute stmt;
		deallocate prepare stmt;
END

