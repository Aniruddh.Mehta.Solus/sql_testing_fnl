
CREATE or replace PROCEDURE `S_UI_Instant_Campaign_Adapter`(IN in_request_json json, OUT out_result_json json)
BEGIN
    
    

    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
    declare EDIT Bigint; 
    declare AGGREGATION_1 text;
    declare AGGREGATION_2 text;
	SET SQL_SAFE_UPDATES=0;
	SET foreign_key_checks=0;

	Set in_request_json = Replace(in_request_json,'\n','\\n');
  
   
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));

	
    
SET AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
SET AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));

	Set @temp_result =Null;
    Set @vSuccess ='';
	Set @vErrMsg='';
	Set @vSuccess='';
	Set @Err =0;
	set sql_safe_updates=0;

    if   AGGREGATION_1 = 'Adapter_Details'
    then
    
    
    Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Provider Name",Provider_Name,"Channel",Channel,"Status",status,"Type",type,"In Use",In_Use))),"]")
				    into @temp_result from Downstream_Adapter_Details  order by Modified_Date desc;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
    
     elseif AGGREGATION_1 = 'Get_Sent_Count'
    then
    
    select Total_Success into @cnt from Downstream_Adapter_Details where In_Use = 'Enable' and Channel = AGGREGATION_2;
    Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Success_Count",@cnt) )),"]") 
														into @temp_result                       
												      ;') ;

							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
						
    
    elseif AGGREGATION_1 = 'Email_Templates_List'
    then
    
       Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Id",Id,"template_id",template_id,"html_template_name", replace(html_template_name,".html",""),"Header",Header))),"]")
				    into @temp_result from Email_HTML_Template ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
                            
             else
             
             Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Invalid Paramters") )),"]") 
														into @temp_result                       
												      ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
	end if;
    

    
    if @temp_result is null
		Then
								
							Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Invalid Parameters") )),"]") 
														into @temp_result                       
												      ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
						
	End if;
							
    SET out_result_json = @temp_result;


END


