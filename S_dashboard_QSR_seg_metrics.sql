DELIMITER $$
CREATE DEFINER=`DOMINOSM_SOLUS_DEP_USER`@`%` PROCEDURE `S_dashboard_QSR_seg_metrics`()
BEGIN

DELETE from log_solus where module = 'S_dashboard_QSR_seg_metrics';
SET SQL_SAFE_UPDATES=0;

SELECT `Value1` into @TYSM from UI_Configuration_Global where `Config_Name` = 'FISCAL';
SELECT @TYSM;

SELECT `Value2` into @TYEM from UI_Configuration_Global where `Config_Name` = 'FISCAL';
SELECT @TYEM;

SET @LYSM = F_Month_Diff(@TYSM,12);
SET @LYEM = F_Month_Diff(@TYEM,12);

SELECT @LYSM,@LYEM;

SET @LLYSM = F_Month_Diff(@LYSM,12);
SET @LLYEM = F_Month_Diff(@LYEM,12);

SELECT @LLYSM,@LLYEM;

SET @Start_Date = date_format(date_sub(concat(date_format(current_date(),"%Y%m"),'01'), INTERVAL 14 MONTH),"%Y%m");
SET @End_Date = date_format(current_date(),"%Y%m");

SET @TM = (SELECT date_format(current_date(),"%Y%m"));
SET @LM = F_Month_Diff(@TM,1);
SET @SMLY = F_Month_Diff(@TM,12);
SET @TY=substr(@TM,1,4);
SET @LY=substr(@SMLY,1,4);
SET @LLY = @LY - 1;
SET @SMLLY = F_Month_Diff(@TM,24);
	
DROP TABLE IF EXISTS Dashboard_QSR_Seg_Temp;
  CREATE TABLE IF NOT EXISTS Dashboard_QSR_Seg_Temp ( 
	`Measure` varchar(50),
	`Segment` varchar(50),
    `Bill_Year_Month` int(20),
	`TM` int(20),
	`LM` int(20),
	`SMLY` int(20),
	`CH_LM_PER` decimal(10,2),
	`CH_SMLY_PER` decimal(10,2),
	`TY` int(20),
    `LY` int(20),
	`LLY` int(20),
	`CH_LY_PER` decimal(10,2),
	`CH_LLY_PER` decimal(10,2),
	`L12M` int(20),
	`L24M` int(20),
	`CH_L12M_PER` decimal(10,2),
	`OVERALL` int(20)
  ) ENGINE = INNODB DEFAULT CHARACTER SET=LATIN1; 
 
  ALTER TABLE Dashboard_QSR_Seg_Temp    
    ADD INDEX (Bill_Year_Month), 
    ADD INDEX (Measure), 
    ADD INDEX (Segment);
set @temp_var = 1;

LOOP_6_MONTHS : LOOP

		set @temp_var = @temp_var + 1;
		IF @temp_var = 6 THEN
				LEAVE LOOP_6_MONTHS;
			END IF;
            
		SELECT @TM, @LM, @SMLY, @TY, @LY, @LLY, @SMLLY, CURRENT_TIMESTAMP();
INSERT INTO Dashboard_QSR_Seg_Temp
(
`Measure`,
`Bill_Year_Month`,
`Segment`,
`TM`,
`LM`,
`SMLY`,
`TY`,
`LY`,
`LLY`,
`L12M`,
`L24M`,
`OVERALL`
)		
WITH GROUP1 AS
                            (
							SELECT Segment as 'Segment',count(distinct(Customer_Id)) as TM
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @TM
							 and Region <> ''
							GROUP BY 1
                            )
                             ,
                             GROUP2 AS
							(
                             SELECT Segment as 'Segment',count(distinct(Customer_Id)) as LM
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @LM
							 and Region <> ''
							GROUP BY 1
                            )
                             ,
                             GROUP3 AS
							(
                             SELECT Segment as 'Segment',count(distinct(Customer_Id)) as SMLY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @SMLY
							 and Region <> ''
							GROUP BY 1
                            )
                             ,
                             GROUP4 AS
							(
                             SELECT Segment as 'Segment',count(distinct(Customer_Id)) as TY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @TYSM AND @TYEM
							 and Region <> ''
							GROUP BY 1
                            )
                             ,
                             GROUP5 AS
							(
                             SELECT Segment as 'Segment',count(distinct(Customer_Id)) as LY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @LYSM AND @LYEM
							 and Region <> ''
							GROUP BY 1
                            )
                            ,
                             GROUP6 AS
							(
                             SELECT Segment as 'Segment',count(distinct(Customer_Id)) as LLY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @LLYSM AND @LLYEM
							 and Region <> ''
							GROUP BY 1
                            )
                            ,
                             GROUP7 AS
							(
                             SELECT Segment as 'Segment',count(distinct(Customer_Id)) as L12M
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @SMLY AND @TM
							 and Region <> ''
							GROUP BY 1
                            )
                            ,
                             GROUP8 AS
							(
                             SELECT Segment as 'Segment',count(distinct(Customer_Id)) as L24M
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @SMLLY AND @SMLY
							 and Region <> ''
							GROUP BY 1
                            )
                             ,
                             GROUP9 AS
							(
                             SELECT Segment as 'Segment',count(distinct(Customer_Id)) as OVERALL
							from Dashboard_Insights_QSR_Customers_Temp
							WHERE  Region <> ''
							GROUP BY 1
                            )
							,
                             GROUP10 AS
							(
                            SELECT distinct(Segment) AS Segment
							from Dashboard_Insights_QSR_Customers_Temp
							where  Region <> ''
                            group by 1
                            )
                            SELECT 'Base' AS Measure, @TM AS Bill_Year_Month, J.Segment,TM, LM, SMLY, TY, LY, LLY, L12M, L24M, OVERALL
                            FROM GROUP10 J
                            LEFT OUTER JOIN GROUP2 B ON B.Segment=J.Segment
                            LEFT OUTER JOIN GROUP3 C ON C.Segment=J.Segment 
                            LEFT OUTER JOIN GROUP4 D ON D.Segment=J.Segment
                            LEFT OUTER JOIN GROUP5 E ON E.Segment=J.Segment
                            LEFT OUTER JOIN GROUP6 F ON F.Segment=J.Segment
                            LEFT OUTER JOIN GROUP7 G ON G.Segment=J.Segment
                            LEFT OUTER JOIN GROUP8 H ON H.Segment=J.Segment
                            LEFT OUTER JOIN GROUP9 I ON I.Segment=J.Segment
							LEFT OUTER JOIN GROUP1 A ON A.Segment=J.Segment
                            
                            UNION
                            
                            SELECT 'Base' AS Measure, @TM AS Bill_Year_Month, J.Segment,TM, LM, SMLY, TY, LY, LLY, L12M, L24M, OVERALL
                            FROM GROUP10 J
                            RIGHT OUTER JOIN GROUP2 B ON B.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP3 C ON C.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP4 D ON D.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP5 E ON E.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP6 F ON F.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP7 G ON G.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP8 H ON H.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP9 I ON I.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP1 A ON A.Segment=J.Segment
                            WHERE J.Segment is not NULL;

UPDATE Dashboard_QSR_Seg_Temp
SET CH_LM_PER = (TM-LM)*100/LM
WHERE Bill_Year_Month = @TM
AND Measure = 'Base';

UPDATE Dashboard_QSR_Seg_Temp
SET CH_SMLY_PER = (TM-SMLY)*100/SMLY
WHERE Bill_Year_Month = @TM
AND Measure = 'Base';

UPDATE Dashboard_QSR_Seg_Temp
SET CH_LY_PER = (TY-LY)*100/LY
WHERE Bill_Year_Month = @TM
AND Measure = 'Base';

UPDATE Dashboard_QSR_Seg_Temp
SET CH_LLY_PER = (TY-LLY)*100/LLY
WHERE Bill_Year_Month = @TM
AND Measure = 'Base';

UPDATE Dashboard_QSR_Seg_Temp
SET CH_L12M_PER = (L12M-L24M)*100/L24M
WHERE Bill_Year_Month = @TM
AND Measure = 'Base';



INSERT INTO Dashboard_QSR_Seg_Temp
(
`Measure`,
`Bill_Year_Month`,
`Segment`,
`TM`,
`LM`,
`SMLY`,
`TY`,
`LY`,
`LLY`,
`L12M`,
`L24M`
)		
WITH GROUP1 AS
                            (
							SELECT Segment as 'Segment',COUNT(DISTINCT Bill_Header_Id) as TM
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @TM
							 and Region <> ''
							GROUP BY 1
                            )
                             ,
                             GROUP2 AS
							(
                             SELECT Segment as 'Segment',COUNT(DISTINCT Bill_Header_Id) as LM
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @LM
							 and Region <> ''
							GROUP BY 1
                            )
                             ,
                             GROUP3 AS
							(
                             SELECT Segment as 'Segment',COUNT(DISTINCT Bill_Header_Id) as SMLY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @SMLY
							 and Region <> ''
							GROUP BY 1
                            )
                             ,
                             GROUP4 AS
							(
                             SELECT Segment as 'Segment',COUNT(DISTINCT Bill_Header_Id) as TY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @TYSM AND @TYEM
							 and Region <> ''
							GROUP BY 1
                            )
                             ,
                             GROUP5 AS
							(
                             SELECT Segment as 'Segment',COUNT(DISTINCT Bill_Header_Id) as LY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @LYSM AND @LYEM
							 and Region <> ''
							GROUP BY 1
                            )
                            ,
                             GROUP6 AS
							(
                             SELECT Segment as 'Segment',COUNT(DISTINCT Bill_Header_Id) as LLY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @LLYSM AND @LLYEM
							 and Region <> ''
							GROUP BY 1
                            )
                            ,
                             GROUP7 AS
							(
                             SELECT Segment as 'Segment',COUNT(DISTINCT Bill_Header_Id) as L12M
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @SMLY AND @TM
							 and Region <> ''
							GROUP BY 1
                            )
                            ,
                             GROUP8 AS
							(
                             SELECT Segment as 'Segment',COUNT(DISTINCT Bill_Header_Id) as L24M
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @SMLLY AND @SMLY
							 and Region <> ''
							GROUP BY 1
                            )
                             ,
                             GROUP9 AS
							(
                             SELECT Segment as 'Segment',COUNT(DISTINCT Bill_Header_Id) as OVERALL
							from Dashboard_Insights_QSR_Customers_Temp
							WHERE  Region <> ''
							GROUP BY 1
                            )
							,
                             GROUP10 AS
							(
                            SELECT distinct(Segment) AS Segment
							from Dashboard_Insights_QSR_Customers_Temp
							where  Region <> ''
                            group by 1
                            )
                            SELECT 'Bills' AS Measure, @TM AS Bill_Year_Month, J.Segment,TM, LM, SMLY, TY, LY, LLY, L12M, L24M
                            FROM GROUP10 J
                            LEFT OUTER JOIN GROUP2 B ON B.Segment=J.Segment
                            LEFT OUTER JOIN GROUP3 C ON C.Segment=J.Segment 
                            LEFT OUTER JOIN GROUP4 D ON D.Segment=J.Segment
                            LEFT OUTER JOIN GROUP5 E ON E.Segment=J.Segment
                            LEFT OUTER JOIN GROUP6 F ON F.Segment=J.Segment
                            LEFT OUTER JOIN GROUP7 G ON G.Segment=J.Segment
                            LEFT OUTER JOIN GROUP8 H ON H.Segment=J.Segment
                            LEFT OUTER JOIN GROUP9 I ON I.Segment=J.Segment
							LEFT OUTER JOIN GROUP1 A ON A.Segment=J.Segment
                            
                            UNION
                            
                            SELECT 'Bills' AS Measure, @TM AS Bill_Year_Month, J.Segment,TM, LM, SMLY, TY, LY, LLY, L12M, L24M
                            FROM GROUP10 J
                            RIGHT OUTER JOIN GROUP2 B ON B.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP3 C ON C.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP4 D ON D.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP5 E ON E.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP6 F ON F.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP7 G ON G.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP8 H ON H.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP9 I ON I.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP1 A ON A.Segment=J.Segment
                            WHERE J.Segment is not NULL;

UPDATE Dashboard_QSR_Seg_Temp
SET CH_LM_PER = (TM-LM)*100/LM
WHERE Bill_Year_Month = @TM
AND Measure = 'Bills';

UPDATE Dashboard_QSR_Seg_Temp
SET CH_SMLY_PER = (TM-SMLY)*100/SMLY
WHERE Bill_Year_Month = @TM
AND Measure = 'Bills';

UPDATE Dashboard_QSR_Seg_Temp
SET CH_LY_PER = (TY-LY)*100/LY
WHERE Bill_Year_Month = @TM
AND Measure = 'Bills';

UPDATE Dashboard_QSR_Seg_Temp
SET CH_LLY_PER = (TY-LLY)*100/LLY
WHERE Bill_Year_Month = @TM
AND Measure = 'Bills';

UPDATE Dashboard_QSR_Seg_Temp
SET CH_L12M_PER = (L12M-L24M)*100/L24M
WHERE Bill_Year_Month = @TM
AND Measure = 'Bills';



INSERT INTO Dashboard_QSR_Seg_Temp
(
`Measure`,
`Bill_Year_Month`,
`Segment`,
`TM`,
`LM`,
`SMLY`,
`TY`,
`LY`,
`LLY`,
`L12M`,
`L24M`
)		
WITH GROUP1 AS
                            (
							SELECT Segment as 'Segment',sum(Sale_Net_Val) as TM
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @TM
							 and Region <> ''
							GROUP BY 1
                            )
                             ,
                             GROUP2 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Net_Val) as LM
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @LM
							 and Region <> ''
							GROUP BY 1
                            )
                             ,
                             GROUP3 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Net_Val) as SMLY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @SMLY
							 and Region <> ''
							GROUP BY 1
                            )
                             ,
                             GROUP4 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Net_Val) as TY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @TYSM AND @TYEM
							 and Region <> ''
							GROUP BY 1
                            )
                             ,
                             GROUP5 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Net_Val) as LY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @LYSM AND @LYEM
							 and Region <> ''
							GROUP BY 1
                            )
                            ,
                             GROUP6 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Net_Val) as LLY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @LLYSM AND @LLYEM
							 and Region <> ''
							GROUP BY 1
                            )
                            ,
                             GROUP7 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Net_Val) as L12M
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @SMLY AND @TM
							 and Region <> ''
							GROUP BY 1
                            )
                            ,
                             GROUP8 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Net_Val) as L24M
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @SMLLY AND @SMLY
							 and Region <> ''
							GROUP BY 1
                            )
                             ,
                             GROUP9 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Net_Val) as OVERALL
							from Dashboard_Insights_QSR_Customers_Temp
							WHERE  Region <> ''
							GROUP BY 1
                            )
							,
                             GROUP10 AS
							(
                            SELECT distinct(Segment) AS Segment
							from Dashboard_Insights_QSR_Customers_Temp
							where  Region <> ''
                            group by 1
                            )
                            SELECT 'Sale_Net_Val' AS Measure, @TM AS Bill_Year_Month, J.Segment,TM, LM, SMLY, TY, LY, LLY, L12M, L24M
                            FROM GROUP10 J
                            LEFT OUTER JOIN GROUP2 B ON B.Segment=J.Segment
                            LEFT OUTER JOIN GROUP3 C ON C.Segment=J.Segment 
                            LEFT OUTER JOIN GROUP4 D ON D.Segment=J.Segment
                            LEFT OUTER JOIN GROUP5 E ON E.Segment=J.Segment
                            LEFT OUTER JOIN GROUP6 F ON F.Segment=J.Segment
                            LEFT OUTER JOIN GROUP7 G ON G.Segment=J.Segment
                            LEFT OUTER JOIN GROUP8 H ON H.Segment=J.Segment
                            LEFT OUTER JOIN GROUP9 I ON I.Segment=J.Segment
							LEFT OUTER JOIN GROUP1 A ON A.Segment=J.Segment
                            
                            UNION
                            
                            SELECT 'Sale_Net_Val' AS Measure, @TM AS Bill_Year_Month, J.Segment,TM, LM, SMLY, TY, LY, LLY, L12M, L24M
                            FROM GROUP10 J
                            RIGHT OUTER JOIN GROUP2 B ON B.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP3 C ON C.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP4 D ON D.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP5 E ON E.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP6 F ON F.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP7 G ON G.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP8 H ON H.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP9 I ON I.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP1 A ON A.Segment=J.Segment
                            WHERE J.Segment is not NULL;

UPDATE Dashboard_QSR_Seg_Temp
SET CH_LM_PER = (TM-LM)*100/LM
WHERE Bill_Year_Month = @TM
AND Measure = 'Sale_Net_Val';

UPDATE Dashboard_QSR_Seg_Temp
SET CH_SMLY_PER = (TM-SMLY)*100/SMLY
WHERE Bill_Year_Month = @TM
AND Measure = 'Sale_Net_Val';

UPDATE Dashboard_QSR_Seg_Temp
SET CH_LY_PER = (TY-LY)*100/LY
WHERE Bill_Year_Month = @TM
AND Measure = 'Sale_Net_Val';

UPDATE Dashboard_QSR_Seg_Temp
SET CH_LLY_PER = (TY-LLY)*100/LLY
WHERE Bill_Year_Month = @TM
AND Measure = 'Sale_Net_Val';

UPDATE Dashboard_QSR_Seg_Temp
SET CH_L12M_PER = (L12M-L24M)*100/L24M
WHERE Bill_Year_Month = @TM
AND Measure = 'Sale_Net_Val';



INSERT INTO Dashboard_QSR_Seg_Temp
(
`Measure`,
`Bill_Year_Month`,
`Segment`,
`TM`,
`LM`,
`SMLY`,
`TY`,
`LY`,
`LLY`,
`L12M`,
`L24M`
)		
WITH GROUP1 AS
                            (
							SELECT Segment as 'Segment',sum(Sale_Net_Val) as TM
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @TM
							 and Region <> ''
							 and Is_Existing_Customer = 1
							GROUP BY 1
                            )
                             ,
                             GROUP2 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Net_Val) as LM
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @LM
							 and Region <> ''
							  and Is_Existing_Customer = 1
							GROUP BY 1
                            )
                             ,
                             GROUP3 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Net_Val) as SMLY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @SMLY
							 and Region <> ''
							  and Is_Existing_Customer = 1
							GROUP BY 1
                            )
                             ,
                             GROUP4 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Net_Val) as TY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @TYSM AND @TYEM
							 and Region <> ''
							  and Is_Existing_Customer = 1
							GROUP BY 1
                            )
                             ,
                             GROUP5 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Net_Val) as LY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @LYSM AND @LYEM
							 and Region <> ''
							  and Is_Existing_Customer = 1
							GROUP BY 1
                            )
                            ,
                             GROUP6 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Net_Val) as LLY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @LLYSM AND @LLYEM
							 and Region <> ''
							  and Is_Existing_Customer = 1
							GROUP BY 1
                            )
                            ,
                             GROUP7 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Net_Val) as L12M
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @SMLY AND @TM
							 and Region <> ''
							  and Is_Existing_Customer = 1
							GROUP BY 1
                            )
                            ,
                             GROUP8 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Net_Val) as L24M
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @SMLLY AND @TM
							 and Region <> ''
							  and Is_Existing_Customer = 1
							GROUP BY 1
                            )
                             ,
                             GROUP9 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Net_Val) as OVERALL
							from Dashboard_Insights_QSR_Customers_Temp
							WHERE  Region <> ''
							 and Is_Existing_Customer = 1
							GROUP BY 1
                            )
							,
                             GROUP10 AS
							(
                            SELECT distinct(Segment) AS Segment
							from Dashboard_Insights_QSR_Customers_Temp
							where  Region <> ''
                            group by 1
                            )
                            SELECT 'Repeat_Revenue' AS Measure, @TM AS Bill_Year_Month, J.Segment,TM, LM, SMLY, TY, LY, LLY, L12M, L24M
                            FROM GROUP10 J
                            LEFT OUTER JOIN GROUP2 B ON B.Segment=J.Segment
                            LEFT OUTER JOIN GROUP3 C ON C.Segment=J.Segment 
                            LEFT OUTER JOIN GROUP4 D ON D.Segment=J.Segment
                            LEFT OUTER JOIN GROUP5 E ON E.Segment=J.Segment
                            LEFT OUTER JOIN GROUP6 F ON F.Segment=J.Segment
                            LEFT OUTER JOIN GROUP7 G ON G.Segment=J.Segment
                            LEFT OUTER JOIN GROUP8 H ON H.Segment=J.Segment
                            LEFT OUTER JOIN GROUP9 I ON I.Segment=J.Segment
							LEFT OUTER JOIN GROUP1 A ON A.Segment=J.Segment
                            
                            UNION
                            
                            SELECT 'Repeat_Revenue' AS Measure, @TM AS Bill_Year_Month, J.Segment,TM, LM, SMLY, TY, LY, LLY, L12M, L24M
                            FROM GROUP10 J
                            RIGHT OUTER JOIN GROUP2 B ON B.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP3 C ON C.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP4 D ON D.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP5 E ON E.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP6 F ON F.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP7 G ON G.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP8 H ON H.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP9 I ON I.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP1 A ON A.Segment=J.Segment
                            WHERE J.Segment is not NULL;

UPDATE Dashboard_QSR_Seg_Temp
SET CH_LM_PER = (TM-LM)*100/LM
WHERE Bill_Year_Month = @TM
AND Measure = 'Repeat_Revenue';

UPDATE Dashboard_QSR_Seg_Temp
SET CH_SMLY_PER = (TM-SMLY)*100/SMLY
WHERE Bill_Year_Month = @TM
AND Measure = 'Repeat_Revenue';

UPDATE Dashboard_QSR_Seg_Temp
SET CH_LY_PER = (TY-LY)*100/LY
WHERE Bill_Year_Month = @TM
AND Measure = 'Repeat_Revenue';

UPDATE Dashboard_QSR_Seg_Temp
SET CH_LLY_PER = (TY-LLY)*100/LLY
WHERE Bill_Year_Month = @TM
AND Measure = 'Repeat_Revenue';

UPDATE Dashboard_QSR_Seg_Temp
SET CH_L12M_PER = (L12M-L24M)*100/L24M
WHERE Bill_Year_Month = @TM
AND Measure = 'Repeat_Revenue';



INSERT INTO Dashboard_QSR_Seg_Temp
(
`Measure`,
`Bill_Year_Month`,
`Segment`,
`TM`,
`LM`,
`SMLY`,
`TY`,
`LY`,
`LLY`,
`L12M`,
`L24M`
)		
WITH GROUP1 AS
                            (
							select 
							A.Segment,
                            Bill_Year_Month,
							'OVERALL' AS Measure,
							(A.c1*100)/B.c2 as TM 
							from 
							(
							(select Segment,Bill_Year_Month,
							SUM(Sale_Net_Val) as c1
							from Dashboard_Insights_QSR_Customers_Temp 
							where Is_Existing_Customer = "1" 
							AND Bill_Year_Month = @TM
							group by Segment)A
							,
							(select Segment, 
							SUM(Sale_Net_Val) as c2
							from Dashboard_Insights_QSR_Customers_Temp 
							WHERE Bill_Year_Month = @TM
							group by Segment)B
							)
							where A.Segment = B.Segment
							and A.Segment <> ""
							group by A.Segment
                            )
                             ,
                             GROUP2 AS
							(
                            select 
							A.Segment,
							'OVERALL' AS Measure,
							(A.c1*100)/B.c2 as LM 
							from 
							(
							(select Segment,
							SUM(Sale_Net_Val) as c1
							from Dashboard_Insights_QSR_Customers_Temp 
							where Is_Existing_Customer = "1" 
							AND Bill_Year_Month = @LM
							group by Segment)A
							,
							(select Segment, 
							SUM(Sale_Net_Val) as c2
							from Dashboard_Insights_QSR_Customers_Temp 
							WHERE Bill_Year_Month = @LM
							group by Segment)B
							)
							where A.Segment = B.Segment
							and A.Segment <> ""
							group by A.Segment
                            )
                             ,
                             GROUP3 AS
							(
                            select 
							A.Segment,
							'OVERALL' AS Measure,
							(A.c1*100)/B.c2 as SMLY 
							from 
							(
							(select Segment,
							SUM(Sale_Net_Val) as c1
							from Dashboard_Insights_QSR_Customers_Temp 
							where Is_Existing_Customer = "1" 
							AND Bill_Year_Month = @SMLY
							group by Segment)A
							,
							(select Segment, 
							SUM(Sale_Net_Val) as c2
							from Dashboard_Insights_QSR_Customers_Temp 
							WHERE Bill_Year_Month = @SMLY
							group by Segment)B
							)
							where A.Segment = B.Segment
							and A.Segment <> ""
							group by A.Segment
                            )
                             ,
                             GROUP4 AS
							(
                            select 
							A.Segment,
							'OVERALL' AS Measure,
							(A.c1*100)/B.c2 as TY 
							from 
							(
							(select Segment,
							SUM(Sale_Net_Val) as c1
							from Dashboard_Insights_QSR_Customers_Temp 
							where Is_Existing_Customer = "1" 
							AND Bill_Year_Month BETWEEN @TYSM AND @TYEM
							group by Segment)A
							,
							(select Segment, 
							SUM(Sale_Net_Val) as c2
							from Dashboard_Insights_QSR_Customers_Temp 
							WHERE Bill_Year_Month BETWEEN @TYSM AND @TYEM
							group by Segment)B
							)
							where A.Segment = B.Segment
							and A.Segment <> ""
							group by A.Segment
                            )
                             ,
                             GROUP5 AS
							(
                            select 
							A.Segment,
							'OVERALL' AS Measure,
							(A.c1*100)/B.c2 as LY 
							from 
							(
							(select Segment,
							SUM(Sale_Net_Val) as c1
							from Dashboard_Insights_QSR_Customers_Temp 
							where Is_Existing_Customer = "1" 
							AND Bill_Year_Month BETWEEN @LYSM AND @LYEM
							group by Segment)A
							,
							(select Segment, 
							SUM(Sale_Net_Val) as c2
							from Dashboard_Insights_QSR_Customers_Temp 
							WHERE Bill_Year_Month BETWEEN @LYSM AND @LYEM
							group by Segment)B
							)
							where A.Segment = B.Segment
							and A.Segment <> ""
							group by A.Segment
                            )
                            ,
                             GROUP6 AS
							(
                            select 
							A.Segment,
							'OVERALL' AS Measure,
							(A.c1*100)/B.c2 as LLY 
							from 
							(
							(select Segment,
							SUM(Sale_Net_Val) as c1
							from Dashboard_Insights_QSR_Customers_Temp 
							where Is_Existing_Customer = "1" 
							AND Bill_Year_Month BETWEEN @LLYSM AND @LLYEM
							group by Segment)A
							,
							(select Segment, 
							SUM(Sale_Net_Val) as c2
							from Dashboard_Insights_QSR_Customers_Temp 
							WHERE Bill_Year_Month BETWEEN @LLYSM AND @LLYEM
							group by Segment)B
							)
							where A.Segment = B.Segment
							and A.Segment <> ""
							group by A.Segment
                            )
                            ,
                             GROUP7 AS
							(
                            select 
							A.Segment,
							'OVERALL' AS Measure,
							(A.c1*100)/B.c2 as L12M
							from 
							(
							(select Segment,
							SUM(Sale_Net_Val) as c1
							from Dashboard_Insights_QSR_Customers_Temp 
							where Is_Existing_Customer = "1" 
							AND Bill_Year_Month BETWEEN @SMLY AND @TM
							group by Segment)A
							,
							(select Segment, 
							SUM(Sale_Net_Val) as c2
							from Dashboard_Insights_QSR_Customers_Temp 
							WHERE Bill_Year_Month BETWEEN @SMLY AND @TM
							group by Segment)B
							)
							where A.Segment = B.Segment
							and A.Segment <> ""
							group by A.Segment
                            )
                            ,
                             GROUP8 AS
							(
                            select 
							A.Segment,
							'OVERALL' AS Measure,
							(A.c1*100)/B.c2 as L24M
							from 
							(
							(select Segment,
							SUM(Sale_Net_Val) as c1
							from Dashboard_Insights_QSR_Customers_Temp 
							where Is_Existing_Customer = "1" 
							AND Bill_Year_Month BETWEEN @SMLLY AND @SMLY
							group by Segment)A
							,
							(select Segment, 
							SUM(Sale_Net_Val) as c2
							from Dashboard_Insights_QSR_Customers_Temp 
							WHERE Bill_Year_Month BETWEEN @SMLLY AND @SMLY
							group by Segment)B
							)
							where A.Segment = B.Segment
							and A.Segment <> ""
							group by A.Segment
                            )
							,
                             GROUP9 AS
							(
                            SELECT distinct(Segment) AS Segment
							from Dashboard_Insights_QSR_Customers_Temp
							where  Region <> ''
                            group by 1
                            )
                            SELECT 'REPEAT_REV_CONTR' AS Measure, @TM AS Bill_Year_Month, J.Segment,TM, LM, SMLY, TY, LY, LLY, L12M, L24M
                            FROM GROUP9 J
                            LEFT OUTER JOIN GROUP2 B ON B.Segment=J.Segment
                            LEFT OUTER JOIN GROUP3 C ON C.Segment=J.Segment 
                            LEFT OUTER JOIN GROUP4 D ON D.Segment=J.Segment
                            LEFT OUTER JOIN GROUP5 E ON E.Segment=J.Segment
                            LEFT OUTER JOIN GROUP6 F ON F.Segment=J.Segment
                            LEFT OUTER JOIN GROUP7 G ON G.Segment=J.Segment
                            LEFT OUTER JOIN GROUP8 H ON H.Segment=J.Segment
							LEFT OUTER JOIN GROUP1 A ON A.Segment=J.Segment
                            
                            UNION
                            
                            SELECT 'REPEAT_REV_CONTR' AS Measure, @TM AS Bill_Year_Month, J.Segment,TM, LM, SMLY, TY, LY, LLY, L12M, L24M
                            FROM GROUP9 J
                            RIGHT OUTER JOIN GROUP2 B ON B.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP3 C ON C.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP4 D ON D.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP5 E ON E.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP6 F ON F.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP7 G ON G.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP8 H ON H.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP1 A ON A.Segment=J.Segment
                            WHERE J.Segment is not NULL;

UPDATE Dashboard_QSR_Seg_Temp
SET CH_LM_PER = (TM-LM)*100/LM
WHERE Bill_Year_Month = @TM
AND Measure = 'REPEAT_REV_CONTR';

UPDATE Dashboard_QSR_Seg_Temp
SET CH_SMLY_PER = (TM-SMLY)*100/SMLY
WHERE Bill_Year_Month = @TM
AND Measure = 'REPEAT_REV_CONTR';

UPDATE Dashboard_QSR_Seg_Temp
SET CH_LY_PER = (TY-LY)*100/LY
WHERE Bill_Year_Month = @TM
AND Measure = 'REPEAT_REV_CONTR';

UPDATE Dashboard_QSR_Seg_Temp
SET CH_LLY_PER = (TY-LLY)*100/LLY
WHERE Bill_Year_Month = @TM
AND Measure = 'REPEAT_REV_CONTR';

UPDATE Dashboard_QSR_Seg_Temp
SET CH_L12M_PER = (L12M-L24M)*100/L24M
WHERE Bill_Year_Month = @TM
AND Measure = 'REPEAT_REV_CONTR';



INSERT INTO Dashboard_QSR_Seg_Temp
(
`Measure`,
`Bill_Year_Month`,
`Segment`,
`TM`,
`LM`,
`SMLY`,
`TY`,
`LY`,
`LLY`,
`L12M`,
`L24M`
)		
WITH GROUP1 AS
                            (
							SELECT Segment as 'Segment',sum(Sale_Net_Val)/COUNT(DISTINCT Bill_Header_Id) as TM
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @TM
							 and Region <> ''
							GROUP BY 1
                            )
                             ,
                             GROUP2 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Net_Val)/COUNT(DISTINCT Bill_Header_Id) as LM
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @LM
							 and Region <> ''
							GROUP BY 1
                            )
                             ,
                             GROUP3 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Net_Val)/COUNT(DISTINCT Bill_Header_Id) as SMLY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @SMLY
							 and Region <> ''
							GROUP BY 1
                            )
                             ,
                             GROUP4 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Net_Val)/COUNT(DISTINCT Bill_Header_Id) as TY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @TYSM AND @TYEM
							 and Region <> ''
							GROUP BY 1
                            )
                             ,
                             GROUP5 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Net_Val)/COUNT(DISTINCT Bill_Header_Id) as LY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @LYSM AND @LYEM
							 and Region <> ''
							GROUP BY 1
                            )
                            ,
                             GROUP6 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Net_Val)/COUNT(DISTINCT Bill_Header_Id) as LLY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @LLYSM AND @LLYEM
							 and Region <> ''
							GROUP BY 1
                            )
                            ,
                             GROUP7 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Net_Val)/COUNT(DISTINCT Bill_Header_Id) as L12M
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @SMLY AND @TM
							 and Region <> ''
							GROUP BY 1
                            )
                            ,
                             GROUP8 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Net_Val)/COUNT(DISTINCT Bill_Header_Id) as L24M
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @SMLLY AND @SMLY
							 and Region <> ''
							GROUP BY 1
                            )
                             ,
                             GROUP9 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Net_Val)/COUNT(DISTINCT Bill_Header_Id) as OVERALL
							from Dashboard_Insights_QSR_Customers_Temp
							WHERE  Region <> ''
							GROUP BY 1
                            )
							,
                             GROUP10 AS
							(
                            SELECT distinct(Segment) AS Segment
							from Dashboard_Insights_QSR_Customers_Temp
							where  Region <> ''
                            group by 1
                            )
                            SELECT 'APC' AS Measure, @TM AS Bill_Year_Month, J.Segment,TM, LM, SMLY, TY, LY, LLY, L12M, L24M
                            FROM GROUP10 J
                            LEFT OUTER JOIN GROUP2 B ON B.Segment=J.Segment
                            LEFT OUTER JOIN GROUP3 C ON C.Segment=J.Segment 
                            LEFT OUTER JOIN GROUP4 D ON D.Segment=J.Segment
                            LEFT OUTER JOIN GROUP5 E ON E.Segment=J.Segment
                            LEFT OUTER JOIN GROUP6 F ON F.Segment=J.Segment
                            LEFT OUTER JOIN GROUP7 G ON G.Segment=J.Segment
                            LEFT OUTER JOIN GROUP8 H ON H.Segment=J.Segment
                            LEFT OUTER JOIN GROUP9 I ON I.Segment=J.Segment
							LEFT OUTER JOIN GROUP1 A ON A.Segment=J.Segment
                            
                            UNION
                            
                            SELECT 'APC' AS Measure, @TM AS Bill_Year_Month, J.Segment,TM, LM, SMLY, TY, LY, LLY, L12M, L24M
                            FROM GROUP10 J
                            RIGHT OUTER JOIN GROUP2 B ON B.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP3 C ON C.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP4 D ON D.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP5 E ON E.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP6 F ON F.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP7 G ON G.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP8 H ON H.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP9 I ON I.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP1 A ON A.Segment=J.Segment
                            WHERE J.Segment is not NULL;

UPDATE Dashboard_QSR_Seg_Temp
SET CH_LM_PER = (TM-LM)*100/LM
WHERE Bill_Year_Month = @TM
AND Measure = 'APC';

UPDATE Dashboard_QSR_Seg_Temp
SET CH_SMLY_PER = (TM-SMLY)*100/SMLY
WHERE Bill_Year_Month = @TM
AND Measure = 'APC';

UPDATE Dashboard_QSR_Seg_Temp
SET CH_LY_PER = (TY-LY)*100/LY
WHERE Bill_Year_Month = @TM
AND Measure = 'APC';

UPDATE Dashboard_QSR_Seg_Temp
SET CH_LLY_PER = (TY-LLY)*100/LLY
WHERE Bill_Year_Month = @TM
AND Measure = 'APC';

UPDATE Dashboard_QSR_Seg_Temp
SET CH_L12M_PER = (L12M-L24M)*100/L24M
WHERE Bill_Year_Month = @TM
AND Measure = 'APC';



INSERT INTO Dashboard_QSR_Seg_Temp
(
`Measure`,
`Bill_Year_Month`,
`Segment`,
`TM`,
`LM`,
`SMLY`,
`TY`,
`LY`,
`LLY`,
`L12M`,
`L24M`
)		
WITH GROUP1 AS
                            (
							SELECT Segment as 'Segment',sum(Sale_Qty) as TM
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @TM
							 and Region <> ''
							GROUP BY 1
                            )
                             ,
                             GROUP2 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Qty) as LM
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @LM
							 and Region <> ''
							GROUP BY 1
                            )
                             ,
                             GROUP3 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Qty) as SMLY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @SMLY
							 and Region <> ''
							GROUP BY 1
                            )
                             ,
                             GROUP4 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Qty) as TY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @TYSM AND @TYEM
							 and Region <> ''
							GROUP BY 1
                            )
                             ,
                             GROUP5 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Qty) as LY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @LYSM AND @LYEM
							 and Region <> ''
							GROUP BY 1
                            )
                            ,
                             GROUP6 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Qty) as LLY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @LLYSM AND @LLYEM
							 and Region <> ''
							GROUP BY 1
                            )
                            ,
                             GROUP7 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Qty) as L12M
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @SMLY AND @TM
							 and Region <> ''
							GROUP BY 1
                            )
                            ,
                             GROUP8 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Qty) as L24M
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @SMLLY AND @SMLY
							 and Region <> ''
							GROUP BY 1
                            )
                             ,
                             GROUP9 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Qty) as OVERALL
							from Dashboard_Insights_QSR_Customers_Temp
							WHERE  Region <> ''
							GROUP BY 1
                            )
							,
                             GROUP10 AS
							(
                            SELECT distinct(Segment) AS Segment
							from Dashboard_Insights_QSR_Customers_Temp
							where  Region <> ''
                            group by 1
                            )
                            SELECT 'Sale_Qty' AS Measure, @TM AS Bill_Year_Month, J.Segment,TM, LM, SMLY, TY, LY, LLY, L12M, L24M
                            FROM GROUP10 J
                            LEFT OUTER JOIN GROUP2 B ON B.Segment=J.Segment
                            LEFT OUTER JOIN GROUP3 C ON C.Segment=J.Segment 
                            LEFT OUTER JOIN GROUP4 D ON D.Segment=J.Segment
                            LEFT OUTER JOIN GROUP5 E ON E.Segment=J.Segment
                            LEFT OUTER JOIN GROUP6 F ON F.Segment=J.Segment
                            LEFT OUTER JOIN GROUP7 G ON G.Segment=J.Segment
                            LEFT OUTER JOIN GROUP8 H ON H.Segment=J.Segment
                            LEFT OUTER JOIN GROUP9 I ON I.Segment=J.Segment
							LEFT OUTER JOIN GROUP1 A ON A.Segment=J.Segment
                            
                            UNION
                            
                            SELECT 'Sale_Qty' AS Measure, @TM AS Bill_Year_Month, J.Segment,TM, LM, SMLY, TY, LY, LLY, L12M, L24M
                            FROM GROUP10 J
                            RIGHT OUTER JOIN GROUP2 B ON B.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP3 C ON C.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP4 D ON D.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP5 E ON E.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP6 F ON F.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP7 G ON G.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP8 H ON H.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP9 I ON I.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP1 A ON A.Segment=J.Segment
                            WHERE J.Segment is not NULL;

UPDATE Dashboard_QSR_Seg_Temp
SET CH_LM_PER = (TM-LM)*100/LM
WHERE Bill_Year_Month = @TM
AND Measure = 'Sale_Qty';

UPDATE Dashboard_QSR_Seg_Temp
SET CH_SMLY_PER = (TM-SMLY)*100/SMLY
WHERE Bill_Year_Month = @TM
AND Measure = 'Sale_Qty';

UPDATE Dashboard_QSR_Seg_Temp
SET CH_LY_PER = (TY-LY)*100/LY
WHERE Bill_Year_Month = @TM
AND Measure = 'Sale_Qty';

UPDATE Dashboard_QSR_Seg_Temp
SET CH_LLY_PER = (TY-LLY)*100/LLY
WHERE Bill_Year_Month = @TM
AND Measure = 'Sale_Qty';

UPDATE Dashboard_QSR_Seg_Temp
SET CH_L12M_PER = (L12M-L24M)*100/L24M
WHERE Bill_Year_Month = @TM
AND Measure = 'Sale_Qty';



INSERT INTO Dashboard_QSR_Seg_Temp
(
`Measure`,
`Bill_Year_Month`,
`Segment`,
`TM`,
`LM`,
`SMLY`,
`TY`,
`LY`,
`LLY`,
`L12M`,
`L24M`
)		
WITH GROUP1 AS
                            (
							SELECT Segment as 'Segment',sum(Sale_Qty)/COUNT(DISTINCT Bill_Header_Id) as TM
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @TM
							 and Region <> ''
							GROUP BY 1
                            )
                             ,
                             GROUP2 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Qty)/COUNT(DISTINCT Bill_Header_Id) as LM
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @LM
							 and Region <> ''
							GROUP BY 1
                            )
                             ,
                             GROUP3 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Qty)/COUNT(DISTINCT Bill_Header_Id) as SMLY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month = @SMLY
							 and Region <> ''
							GROUP BY 1
                            )
                             ,
                             GROUP4 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Qty)/COUNT(DISTINCT Bill_Header_Id) as TY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @TYSM AND @TYEM
							 and Region <> ''
							GROUP BY 1
                            )
                             ,
                             GROUP5 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Qty)/COUNT(DISTINCT Bill_Header_Id) as LY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @LYSM AND @LYEM
							 and Region <> ''
							GROUP BY 1
                            )
                            ,
                             GROUP6 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Qty)/COUNT(DISTINCT Bill_Header_Id) as LLY
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @LLYSM AND @LLYEM
							 and Region <> ''
							GROUP BY 1
                            )
                            ,
                             GROUP7 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Qty)/COUNT(DISTINCT Bill_Header_Id) as L12M
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @SMLY AND @TM
							 and Region <> ''
							GROUP BY 1
                            )
                            ,
                             GROUP8 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Qty)/COUNT(DISTINCT Bill_Header_Id) as L24M
							from Dashboard_Insights_QSR_Customers_Temp
                            where Bill_Year_Month BETWEEN @SMLLY AND @SMLY
							 and Region <> ''
							GROUP BY 1
                            )
                             ,
                             GROUP9 AS
							(
                             SELECT Segment as 'Segment',sum(Sale_Qty)/COUNT(DISTINCT Bill_Header_Id) as OVERALL
							from Dashboard_Insights_QSR_Customers_Temp
							WHERE  Region <> ''
							GROUP BY 1
                            )
							,
                             GROUP10 AS
							(
                            SELECT distinct(Segment) AS Segment
							from Dashboard_Insights_QSR_Customers_Temp
							where  Region <> ''
                            group by 1
                            )
                            SELECT 'BS' AS Measure, @TM AS Bill_Year_Month, J.Segment,TM, LM, SMLY, TY, LY, LLY, L12M, L24M
                            FROM GROUP10 J
                            LEFT OUTER JOIN GROUP2 B ON B.Segment=J.Segment
                            LEFT OUTER JOIN GROUP3 C ON C.Segment=J.Segment 
                            LEFT OUTER JOIN GROUP4 D ON D.Segment=J.Segment
                            LEFT OUTER JOIN GROUP5 E ON E.Segment=J.Segment
                            LEFT OUTER JOIN GROUP6 F ON F.Segment=J.Segment
                            LEFT OUTER JOIN GROUP7 G ON G.Segment=J.Segment
                            LEFT OUTER JOIN GROUP8 H ON H.Segment=J.Segment
                            LEFT OUTER JOIN GROUP9 I ON I.Segment=J.Segment
							LEFT OUTER JOIN GROUP1 A ON A.Segment=J.Segment
                            
                            UNION
                            
                            SELECT 'BS' AS Measure, @TM AS Bill_Year_Month, J.Segment,TM, LM, SMLY, TY, LY, LLY, L12M, L24M
                            FROM GROUP10 J
                            RIGHT OUTER JOIN GROUP2 B ON B.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP3 C ON C.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP4 D ON D.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP5 E ON E.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP6 F ON F.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP7 G ON G.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP8 H ON H.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP9 I ON I.Segment=J.Segment
                            RIGHT OUTER JOIN GROUP1 A ON A.Segment=J.Segment
                            WHERE J.Segment is not NULL;

UPDATE Dashboard_QSR_Seg_Temp
SET CH_LM_PER = (TM-LM)*100/LM
WHERE Bill_Year_Month = @TM
AND Measure = 'BS';

UPDATE Dashboard_QSR_Seg_Temp
SET CH_SMLY_PER = (TM-SMLY)*100/SMLY
WHERE Bill_Year_Month = @TM
AND Measure = 'BS';

UPDATE Dashboard_QSR_Seg_Temp
SET CH_LY_PER = (TY-LY)*100/LY
WHERE Bill_Year_Month = @TM
AND Measure = 'BS';

UPDATE Dashboard_QSR_Seg_Temp
SET CH_LLY_PER = (TY-LLY)*100/LLY
WHERE Bill_Year_Month = @TM
AND Measure = 'BS';

UPDATE Dashboard_QSR_Seg_Temp
SET CH_L12M_PER = (L12M-L24M)*100/L24M
WHERE Bill_Year_Month = @TM
AND Measure = 'BS';
		SET @TM = F_Month_Diff(@TM,1);
		SET @LM = F_Month_Diff(@TM,1);
		SET @SMLY = F_Month_Diff(@TM,12);
		SET @TY=substr(@TM,1,4);
		SET @LY=substr(@SMLY,1,4);
		SET @LLY = @LY - 1;
		SET @SMLLY = F_Month_Diff(@TM,24);
END LOOP LOOP_6_MONTHS;

END$$
DELIMITER ;
