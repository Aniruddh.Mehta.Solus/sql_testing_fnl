DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_explore_Customer`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_FILTER_MONTHDURATION text; 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
    declare IN_FUNDS text;
    declare IN_ASSET_CLASS text;
    declare IN_SUB_ASSET_CLASS text;
    declare IN_FAV_CLASS text;
    declare IN_FAV_SUB_CLASS text;
    declare IN_SIP text;
    declare V_Event_Execution_Id varchar(100); 
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    
    SET IN_FUNDS = json_unquote(json_extract(in_request_json,"$.FUNDS"));
	SET IN_ASSET_CLASS = json_unquote(json_extract(in_request_json,"$.ASSET_CLASS"));
	SET IN_SUB_ASSET_CLASS = json_unquote(json_extract(in_request_json,"$.SUB_ASSET_CLASS"));
	SET IN_FAV_CLASS = json_unquote(json_extract(in_request_json,"$.FAV_CLASS"));
    SET IN_FAV_SUB_CLASS = json_unquote(json_extract(in_request_json,"$.FAV_SUB_CLASS"));
    SET IN_SIP = json_unquote(json_extract(in_request_json,"$.SIP"));
    
    
    
    IF IN_MEASURE = "NAME" OR IN_MEASURE = "CARDS" OR IN_MEASURE='PERSONAL'
	THEN
		SET @Query_Measure=' First_Name ';
		SET @name_measure=@Query_Measure;
        SET @name_table = ' CDM_Customer_PII_Master ';
        SET @table_used=@name_table;
	END IF;
     IF IN_MEASURE = "PAN" OR IN_MEASURE = "CARDS" OR IN_MEASURE='PERSONAL'
	THEN
		SET @Query_Measure=' Customer_Key ';
		SET @pan_measure=@Query_Measure;
        SET @pan_table = ' CDM_Customer_Key_Lookup ';
        SET @table_used=@pan_table;
	END IF;
    IF IN_MEASURE = "GENDER" OR IN_MEASURE = "CARDS" OR IN_MEASURE='PERSONAL'
	THEN
		SET @Query_Measure=' LOV_Key ';
		SET @gender_measure=@Query_Measure;
        SET @gender_table = ' CDM_Customer_Master ';
        SET @table_used=@gender_table;
	END IF;
    IF IN_MEASURE = "MOBILE" OR IN_MEASURE = "CARDS" OR IN_MEASURE='PERSONAL'
	THEN
		SET @Query_Measure=' Mobile ';
		SET @mobile_measure=@Query_Measure;
        SET @mobile_table = ' CDM_Customer_PII_Master ';
        SET @table_used=@mobile_table;
	END IF;
    IF IN_MEASURE = "EMAIL" OR IN_MEASURE = "CARDS" OR IN_MEASURE='PERSONAL'
	THEN
		SET @Query_Measure=' Email ';
		SET @email_measure=@Query_Measure;
        SET @email_table = ' CDM_Customer_PII_Master ';
        SET @table_used=@email_table;
	END IF;
    IF IN_MEASURE = "DOB" OR IN_MEASURE = "CARDS" OR IN_MEASURE='PERSONAL'
	THEN
		SET @Query_Measure=' DOB ';
		SET @dob_measure=@Query_Measure;
        SET @dob_table = ' CDM_Customer_Master ';
        SET @table_used=@dob_table;
	END IF;
     IF IN_MEASURE = "DOJ" OR IN_MEASURE = "CARDS" OR IN_MEASURE='PERSONAL'
	THEN
		SET @Query_Measure=' Doj ';
		SET @doj_measure=@Query_Measure;
        SET @doj_table = ' CDM_Customer_Master ';
        SET @table_used=@doj_table;
	END IF;
    IF IN_MEASURE = "NATION" OR IN_MEASURE = "CARDS"
	THEN
		SET @Query_Measure=' Custom_Tag_1 ';
		SET @nation_measure=@Query_Measure;
        SET @nation_table = ' CDM_Customer_Master ';
        SET @table_used=@nation_table;
	END IF;
    IF IN_MEASURE = "RECENCY" OR IN_MEASURE = "CARDS"
	THEN
		SET @Query_Measure=' Recency ';
		SET @recency_measure=@Query_Measure;
        SET @recency_table = ' Customer_One_View_Original ';
        SET @table_used=@recency_table;
	END IF;
	IF IN_MEASURE = "FREQ_12MTH" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' AP_Num_of_Visits ';
		SET @freq12mth_measure=@Query_Measure;
        SET @freq12mth_table= ' Customer_One_View_Original ';
        SET @table_used=@freq12mth_table;
	END IF;
    IF IN_MEASURE = "FREQ_EVER" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' Lt_Num_of_Visits ';
		SET @freqever_measure=@Query_Measure;
        SET @freqever_table=' Customer_One_View_Original ';
        SET @table_used=@freqever_table;
	END IF;
     IF IN_MEASURE = "ADGBT" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' Lt_ADGBT ';
		SET @adgbt_measure=@Query_Measure;
        SET @adgbt_table=' Customer_One_View_Original ';
        SET @table_used=@adgbt_table;
	END IF;
    IF IN_MEASURE = "TENURE" OR IN_MEASURE = "CARDS" 
	THEN
        SET @Query_Measure=' Tenure ';
		SET @tenure_measure=@Query_Measure;
        SET @tenure_table=' Customer_One_View_Original ';
        SET @table_used=@tenure_table;
	END IF;
    IF IN_MEASURE = "VALUE_12MTH" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' AP_Rev ';
		SET @value12mth_measure=@Query_Measure;
        SET @value12mth_table=' Customer_One_View_Original ';
        SET @table_used=@value12mth_table;
	END IF;
    IF IN_MEASURE = "VALUE_EVER" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' Lt_Rev ';
		SET @valueever_measure=@Query_Measure;
        SET @valueever_table=' Customer_One_View_Original ';
        SET @table_used=@valueever_table;
	END IF;
    IF IN_MEASURE = "ABV" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' Lt_ABV ';
		SET @abv_measure=@Query_Measure;
        SET @abv_table=' Customer_One_View_Original ';
        SET @table_used=@abv_table;
	END IF;
    IF IN_MEASURE = "FTD" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' FTD ';
		SET @ftd_measure=@Query_Measure;
        SET @ftd_table=' Customer_One_View_Original ';
        SET @table_used=@ftd_table;
	END IF;
    IF IN_MEASURE = "LTD" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' LTD ';
		SET @ltd_measure=@Query_Measure;
        SET @ltd_table=' Customer_One_View_Original ';
        SET @table_used=@ltd_table;
	END IF;
	IF IN_MEASURE = "PRODUCT_RANGE" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' LT_Distinct_Prod_Cnt ';
		SET @productrange_measure=@Query_Measure;
        SET @productrange_table=' Customer_One_View_Original ';
        SET @table_used=@productrange_table;
	END IF;
    IF IN_MEASURE = "ASSET_CLASS_RANGE" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' LT_Distinct_Cat_Cnt ';
		SET @assetclassrange_measure=@Query_Measure;
        SET @assetclassrange_table=' Customer_One_View_Original ';
        SET @table_used=@assetclassrange_table;
	END IF;
    IF IN_MEASURE = "SUB_ASSET_CLASS_RANGE" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' LT_Distinct_Dep_Cnt ';
		SET @subassetclassrange_measure=@Query_Measure;
        SET @subassetclassrange_table=' Customer_One_View_Original ';
        SET @table_used=@subassetclassrange_table;
	END IF;
    IF IN_MEASURE = "FAV_ASSET_CLASS" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' Cat_Key ';
		SET @favassetclass_measure=@Query_Measure;
        SET @favassetclass_table=' Customer_One_View_Original ';
        SET @table_used=@favassetclass_table;
	END IF;
    IF IN_MEASURE = "FAV_SUB_ASSET_CLASS" OR IN_MEASURE = "CARDS" 
	THEN
		SET @Query_Measure=' Dep_Key ';
		SET @favsubassetclass_measure=@Query_Measure;
        SET @favsubassetclass_table=' Customer_One_View_Original ';
        SET @table_used=@favsubassetclass_table;
	END IF;
     IF IN_MEASURE = "FAV_PRODUCT" OR IN_MEASURE = "CARDS" 
	THEN 
		SET @Query_Measure=' Product_Name ';
		SET @favproduct_measure=@Query_Measure;
        SET @favsubassetclass_table=' Customer_One_View_Original ';
        SET @table_used=@favsubassetclass_table;
	END IF;
    
     IF IN_MEASURE = "FAV_TYPE" OR IN_MEASURE = "CARDS" 
	THEN 
		SET @Query_Measure=' Fav_Type ';
		SET @favtype_measure=@Query_Measure;
        SET @favtype_table=' SVOC_MF ';
        SET @table_used=@favsubassetclass_table;
	END IF;
    
     IF IN_MEASURE = "Total_SIPs" OR IN_MEASURE = "CARDS" 
	THEN 
		SET @Query_Measure=' Total_Active_SIPs ';
		SET @total_sip_measure=@Query_Measure;
        SET @total_sip_table=' SVOC_MF ';
	END IF;
    
      IF IN_MEASURE = "Product_Pitched" OR IN_MEASURE = "CARDS" THEN 
    SET @Query_Measure = 'IFNULL(Product_name, "-")';
    SET @total_product_pitched = @Query_Measure;
    SET @total_intelligence_table = 'RankedPickList_Stories';
END IF;
    
    SELECT @name_measure, @pan_measure, @gender_measure, @mobile_measure, @email_measure, @dob_measure, @nation_measure, @recency_measure, @freq12mth_measure, @freqever_measure, @adgbt_measure, @tenure_measure, @value12mth_measure, @valueever_measure, @abv_measure, @ftd_measure, @ltd_measure, @productrange_measure, @assetclassrange_measure, @subassetclassrange_measure, @favassetclass_measure, @favsubassetclass_measure,@total_product_pitched;
    
    CREATE OR REPLACE VIEW Customer_One_View_Original_1 AS SELECT Customer_Id, REPLACE(Mobile," ","") AS Mobile FROM CDM_Customer_PII_Master;

    IF IN_MEASURE = 'RANDOM' AND IN_AGGREGATION_1 = '' AND IN_AGGREGATION_2 = ''
    THEN
    
    set @view_stmt=concat('CREATE OR REPLACE VIEW V_Dashboard_explore_Random as
       select Customer_Key from CDM_Customer_Key_Lookup order by Customer_Id limit 1;');
 
	SET @Query_String =  'SELECT CONCAT("[", JSON_OBJECT("Customer_Key", Customer_Key), "]") into @temp_result
from V_Dashboard_explore_Random;' ;
    END IF;
    
    IF IN_MEASURE='HEADER' AND IN_AGGREGATION_1 = '' AND IN_AGGREGATION_2 <> ''
    THEN
    set @cust_id="";
    SELECT Customer_Id INTO @cust_id from Customer_One_View_Original_1 WHERE Mobile = IN_AGGREGATION_2;
    SELECT @cust_id;
    if @cust_id<>"" then
    begin
    
    set @view_stmt=concat('create or replace view V_Dashboard_explore_Header as
        (select Customer_Id, CLMSegmentName from CDM_Customer_Segment where Customer_Id = "',@cust_id,'")');
        
	SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Customer_Id",`Customer_Id`,"Segment",`CLMSegmentName`) )),"]")into @temp_result from V_Dashboard_explore_Header;' ;
    end;
    else
    SET @temp_result = JSON_OBJECT('message', "No data found");
    end if;
    END IF;
    
    IF IN_MEASURE='HEADER' AND IN_AGGREGATION_1 <> '' AND IN_AGGREGATION_2 = ''
    THEN
    set @cust_id='';
    SELECT Customer_Id INTO @cust_id from CDM_Customer_Key_Lookup WHERE Customer_Key = IN_AGGREGATION_1;
    SELECT @cust_id;
    if @cust_id<>"" then
    begin
    set @view_stmt=concat('create or replace view V_Dashboard_explore_Header as
        (select Customer_Id, CLMSegmentName from CDM_Customer_Segment where Customer_Id = "',@cust_id,'")');
        
	SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Customer_Id",`Customer_Id`,"Segment",`CLMSegmentName`) )),"]")into @temp_result from V_Dashboard_explore_Header;' ;
    end;
    else
    SET @temp_result = JSON_OBJECT('message', "No data found");
    end if;
    END IF;
    
    IF IN_MEASURE='PERSONAL' AND IN_AGGREGATION_1 = '' AND IN_AGGREGATION_2 <> ''
    THEN
    set @cust_id="";
    SELECT Customer_Id INTO @cust_id from Customer_One_View_Original_1 WHERE Mobile = IN_AGGREGATION_2;
    SELECT @cust_id;
    if @cust_id<>"" then
    begin
		set @view_stmt=concat('create or replace view V_Dashboard_explore_Card as
        (select "Name" as Card_Name,',@name_measure,' as `Value1`,"" as `Value2`, "Demographics" as `Value3`,"NAME" as `Measure`  from ',@name_table,' WHERE Customer_Id = ',@cust_id,')
        UNION
		(select "PAN" as Card_Name,',@pan_measure,' as `Value1`,"" as `Value2`, "Demographics" as `Value3`,"PAN" as `Measure`  from ',@pan_table,' WHERE Customer_Id = ',@cust_id,')
        UNION
        (select "Gender" as Card_Name,',@gender_measure,' as `Value1`,"" as `Value2`, "Demographics" as `Value3`,"GENDER" as `Measure`  from CDM_Customer_Master a inner join CDM_LOV_Master b on a.Gender_LOV_Id = b.LOV_Id WHERE Customer_Id = ',@cust_id,')
        UNION
        (select "Mobile" as Card_Name,',@mobile_measure,' as `Value1`,"" as `Value2`, "Demographics" as `Value3`,"MOBILE" as `Measure`  from ',@mobile_table,' WHERE Customer_Id = ',@cust_id,')
        UNION
        (select "Email" as Card_Name,',@email_measure,' as `Value1`,"" as `Value2`, "Demographics" as `Value3`,"EMAIL" as `Measure`  from ',@email_table,' WHERE Customer_Id = ',@cust_id,')
        UNION
        (select "DOB" as Card_Name,',@dob_measure,' as `Value1`,"" as `Value2`, "Demographics" as `Value3`,"DOB" as `Measure`  from ',@dob_table,' WHERE Customer_Id = ',@cust_id,')
        union
          (select "Doj" as Card_Name,',@doj_measure,' as `Value1`,"" as `Value2`, "Demographics" as `Value3`,"Doj" as `Measure`  from ',@doj_table,' WHERE Customer_Id = ',@cust_id,')
		');
       
	SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Card_Name",`Card_Name`,"Value1",IFNULL(`Value1`,""),"Value2",`Value2`,"Value3",`Value3`,"Measure",`Measure`) )),"]")into @temp_result from V_Dashboard_explore_Card;' ;
    end;
    else
    SET @temp_result = JSON_OBJECT('message', "No data found");
       end if;
    END IF;
    
     IF IN_MEASURE='CARDS' AND IN_AGGREGATION_1 = '' AND IN_AGGREGATION_2 <> ''
    THEN
    set @cust_id="";
    SELECT Customer_Id INTO @cust_id from Customer_One_View_Original_1 WHERE Mobile = IN_AGGREGATION_2;
    SELECT @cust_id;
    if @cust_id<>"" then
		select Value1 into @showcards FROM Customer_One_View_Config WHERE Config_Name = 'Show SIP Card';
        IF @showcards = 'Yes'
        THEN
		set @view_stmt=concat('create or replace view V_Dashboard_explore_Card as
	(select "Recency (Days)" as Card_Name,',@recency_measure,' as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"RECENCY" as `Measure`  from ',@recency_table,' WHERE Customer_Id = ',@cust_id,')
		Union
		(select "Freq. (Active Period)" as Card_Name,',@freq12mth_measure,' as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"FREQ_12MTH" as `Measure`  from ',@freq12mth_table,' WHERE Customer_Id = ',@cust_id,')
		Union
		(select "Freq. (Ever)" as Card_Name,',@freqever_measure,' as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"FREQ_EVER" as `Measure`  from ',@freqever_table,' WHERE Customer_Id = ',@cust_id,')
		Union
		(select "ADGBT" as Card_Name,',@adgbt_measure,' as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"ADGBT" as `Measure`  from ',@adgbt_table,' WHERE Customer_Id = ',@cust_id,')
		Union
		(select "Tenure (Days)" as Card_Name,',@tenure_measure,' as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"TENURE" as `Measure`  from ',@tenure_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "Amt (Active Period)" as Card_Name,round((',@value12mth_measure,'),0) as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"VALUE_12MTH" as `Measure`  from ',@value12mth_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "Amt (ever)" as Card_Name,round((',@valueever_measure,'),0) as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"VALUE_EVER" as `Measure`  from ',@valueever_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "Avg. Ticket" as Card_Name,round((',@abv_measure,'),0) as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"ABV" as `Measure`  from ',@abv_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "First Transaction" as Card_Name,',@ftd_measure,' as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"FTD" as `Measure`  from ',@ftd_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "Recent Transaction" as Card_Name,',@ltd_measure,' as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"LTD" as `Measure`  from ',@ltd_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "Product Range" as Card_Name,',@productrange_measure,' as `Value1`,"" as `Value2`, (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias For Funds") as `Value3`,"PRODUCT_RANGE" as `Measure`  from ',@productrange_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias for Asset Class Range") as Card_Name,',@assetclassrange_measure,' as `Value1`,"" as `Value2`, (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias For Funds") as `Value3`,"ASSET_CLASS_RANGE" as `Measure`  from ',@assetclassrange_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias for Sub Asset Class Range") as Card_Name,',@subassetclassrange_measure,' as `Value1`,"" as `Value2`, (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias For Funds") as `Value3`,"SUB_ASSET_CLASS_RANGE" as `Measure`  from ',@subassetclassrange_table,' WHERE Customer_Id = ',@cust_id,')
        Union
        (select (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias for Fav Asset Class") as Card_Name,',@favassetclass_measure,' as `Value1`,"" as `Value2`, (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias For Funds") as `Value3`,"FAV_ASSET_CLASS" as `Measure`  from  Customer_One_View_Original a inner join CDM_Cat_Key_Lookup b on a.LT_Fav_Cat_LOV_Id = b.LOV_Id WHERE Customer_Id = ',@cust_id,')
        Union
        (select (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias for Fav Sub Asset Class") as Card_Name,',@favsubassetclass_measure,' as `Value1`,"" as `Value2`, (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias For Funds") as `Value3`,"FAV_SUB_ASSET_CLASS" as `Measure`  from  Customer_One_View_Original a inner join CDM_Dep_Key_Lookup b on a.LT_Fav_Dep_LOV_Id = b.LOV_Id WHERE Customer_Id = ',@cust_id,')
        Union
        (select "Fav Product" as Card_Name,',@favproduct_measure,' as `Value1`,"" as `Value2`, (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias For Funds") as `Value3`,"FAV_PRODUCT" as `Measure`  from  Customer_One_View_Original a inner join CDM_Product_Master b on a.LT_Fav_Prod_LOV_Id = b.Product_LOV_Id WHERE Customer_Id = ',@cust_id,')
	UNION
    	(select "Fav Type" as Card_Name,',@favtype_measure,' as `Value1`,"" as `Value2`, (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias For Funds") as `Value3`,"Fav_Type" as `Measure`  from ',@favtype_table,' WHERE Customer_Id = ',@cust_id,')
             UNION
     ( SELECT "Reco basis Fav. Asset Class" as Card_Name,', @total_product_pitched,' as Value1, "" as Value2, "Intelligence" as Value3, "Product_Pitched" as Measure
FROM ',@total_intelligence_table,'
INNER JOIN CDM_Product_Master ON Reco_Product_1 = Product_Id
WHERE Story_Id = 51 AND Customer_Id = ',@cust_id,')
   UNION
     ( SELECT "Reco basis Preference" as Card_Name,', @total_product_pitched,' as Value1, "" as Value2, "Intelligence" as Value3, "Product_Preference" as Measure
FROM ',@total_intelligence_table,'
INNER JOIN CDM_Product_Master ON Reco_Product_1 = Product_Id
WHERE Story_Id = 32 AND Customer_Id = ',@cust_id,')
 UNION
     ( SELECT "Reco basis Genome" as Card_Name,IFNULL(', @total_product_pitched,', "-")  as Value1, "" as Value2, "Intelligence" as Value3, "Product_Genome" as Measure
FROM ',@total_intelligence_table,'
INNER JOIN CDM_Product_Master ON Reco_Product_1 = Product_Id
WHERE Story_Id = 36 AND Customer_Id = ',@cust_id,')
UNION
     ( SELECT "Reco basis Trending" as Card_Name,', @total_product_pitched,' as Value1, "" as Value2, "Intelligence" as Value3, "Product_Trending" as Measure
FROM ',@total_intelligence_table,'
INNER JOIN CDM_Product_Master ON Reco_Product_1 = Product_Id
WHERE Story_Id = 33 AND Customer_Id = ',@cust_id,')
UNION
     ( SELECT "Reco basis Hybridizer" as Card_Name,', @total_product_pitched,' as Value1, "" as Value2, "Intelligence" as Value3, "Product_Hybridizer" as Measure
FROM ',@total_intelligence_table,'
INNER JOIN CDM_Product_Master ON Reco_Product_1 = Product_Id
WHERE Story_Id = 1 AND Customer_Id = ',@cust_id,')

        UNION
    	(select "# Ongoing SIPs" as Card_Name,',@total_sip_measure,' as `Value1`,"" as `Value2`, "Funds" as `Value3`,"Total_SIP" as `Measure`  from ',@total_sip_table,' WHERE Customer_Id = ',@cust_id,')
        UNION
        (select "Propensity (7 Days)" as Card,round(NextTrxn7_CLMSegment,2) as `Value1`,"" as `Value2`, "Intelligence" as `Value3`,"Propensity_7" as `Measure`  from  CLM_MCore_Customer_Percentile a  WHERE Customer_Id =  ',@cust_id,')
           UNION
        (select "Propensity (15 Days)" as Card,round(NextTrxn15_CLMSegment,2) as `Value1`,"" as `Value2`, "Intelligence" as `Value3`,"Propensity_15" as `Measure`  from  CLM_MCore_Customer_Percentile a  WHERE Customer_Id =  ',@cust_id,')
           UNION
        (select "Propensity (30 Days)" as Card,round(NextTrxn30_CLMSegment,2) as `Value1`,"" as `Value2`, "Intelligence" as `Value3`,"Propensity_30" as `Measure`  from  CLM_MCore_Customer_Percentile a  WHERE Customer_Id =  ',@cust_id,')
		');
       END IF; 
       IF @showcards = 'No'
        THEN
		set @view_stmt=concat('create or replace view V_Dashboard_explore_Card as
	(select "Recency (Days)" as Card_Name,',@recency_measure,' as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"RECENCY" as `Measure`  from ',@recency_table,' WHERE Customer_Id = ',@cust_id,')
		Union
		(select "Freq. (Active Period)" as Card_Name,',@freq12mth_measure,' as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"FREQ_12MTH" as `Measure`  from ',@freq12mth_table,' WHERE Customer_Id = ',@cust_id,')
		Union
		(select "Freq. (Ever)" as Card_Name,',@freqever_measure,' as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"FREQ_EVER" as `Measure`  from ',@freqever_table,' WHERE Customer_Id = ',@cust_id,')
		Union
		(select "ADGBT" as Card_Name,',@adgbt_measure,' as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"ADGBT" as `Measure`  from ',@adgbt_table,' WHERE Customer_Id = ',@cust_id,')
		Union
		(select "Tenure (Days)" as Card_Name,',@tenure_measure,' as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"TENURE" as `Measure`  from ',@tenure_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "Amt (Active Period)" as Card_Name,round((',@value12mth_measure,'),0) as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"VALUE_12MTH" as `Measure`  from ',@value12mth_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "Amt (ever)" as Card_Name,round((',@valueever_measure,'),0) as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"VALUE_EVER" as `Measure`  from ',@valueever_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "Avg. Ticket" as Card_Name,round((',@abv_measure,'),0) as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"ABV" as `Measure`  from ',@abv_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "First Transaction" as Card_Name,',@ftd_measure,' as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"FTD" as `Measure`  from ',@ftd_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "Recent Transaction" as Card_Name,',@ltd_measure,' as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"LTD" as `Measure`  from ',@ltd_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "Product Range" as Card_Name,',@productrange_measure,' as `Value1`,"" as `Value2`, (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Funds") as `Value3`,"PRODUCT_RANGE" as `Measure`  from ',@productrange_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias for Asset Class Range") as Card_Name,',@assetclassrange_measure,' as `Value1`,"" as `Value2`, (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Funds") as `Value3`,"ASSET_CLASS_RANGE" as `Measure`  from ',@assetclassrange_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias for Sub Asset Class Range") as Card_Name,',@subassetclassrange_measure,' as `Value1`,"" as `Value2`, (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Funds") as `Value3`,"SUB_ASSET_CLASS_RANGE" as `Measure`  from ',@subassetclassrange_table,' WHERE Customer_Id = ',@cust_id,')
        Union
        (select (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias for Fav Asset Class") as Card_Name,',@favassetclass_measure,' as `Value1`,"" as `Value2`, (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Funds") as `Value3`,"FAV_ASSET_CLASS" as `Measure`  from  Customer_One_View_Original a inner join CDM_Cat_Key_Lookup b on a.LT_Fav_Cat_LOV_Id = b.LOV_Id WHERE Customer_Id = ',@cust_id,')
        Union
        (select (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias for Fav Sub Asset Class") as Card_Name,',@favsubassetclass_measure,' as `Value1`,"" as `Value2`, (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Funds") as `Value3`,"FAV_SUB_ASSET_CLASS" as `Measure`  from  Customer_One_View_Original a inner join CDM_Dep_Key_Lookup b on a.LT_Fav_Dep_LOV_Id = b.LOV_Id WHERE Customer_Id = ',@cust_id,')
        Union
        (select "Fav Product" as Card_Name,',@favproduct_measure,' as `Value1`,"" as `Value2`, (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Funds") as `Value3`,"FAV_PRODUCT" as `Measure`  from  Customer_One_View_Original a inner join CDM_Product_Master b on a.LT_Fav_Prod_LOV_Id = b.Product_LOV_Id WHERE Customer_Id = ',@cust_id,')
             UNION
     ( SELECT "Reco basis Fav. Asset Class" as Card_Name,', @total_product_pitched,' as Value1, "" as Value2, "Intelligence" as Value3, "Product_Pitched" as Measure
FROM ',@total_intelligence_table,'
INNER JOIN CDM_Product_Master ON Reco_Product_1 = Product_Id
WHERE Story_Id = 51 AND Customer_Id = ',@cust_id,')
   UNION
     ( SELECT "Reco basis Preference" as Card_Name,', @total_product_pitched,' as Value1, "" as Value2, "Intelligence" as Value3, "Product_Preference" as Measure
FROM ',@total_intelligence_table,'
INNER JOIN CDM_Product_Master ON Reco_Product_1 = Product_Id
WHERE Story_Id = 32 AND Customer_Id = ',@cust_id,')
 UNION
     ( SELECT "Reco basis Genome" as Card_Name,IFNULL(', @total_product_pitched,', "-")  as Value1, "" as Value2, "Intelligence" as Value3, "Product_Genome" as Measure
FROM ',@total_intelligence_table,'
INNER JOIN CDM_Product_Master ON Reco_Product_1 = Product_Id
WHERE Story_Id = 36 AND Customer_Id = ',@cust_id,')
UNION
     ( SELECT "Reco basis Trending" as Card_Name,', @total_product_pitched,' as Value1, "" as Value2, "Intelligence" as Value3, "Product_Trending" as Measure
FROM ',@total_intelligence_table,'
INNER JOIN CDM_Product_Master ON Reco_Product_1 = Product_Id
WHERE Story_Id = 33 AND Customer_Id = ',@cust_id,')
UNION
     ( SELECT "Reco basis Hybridizer" as Card_Name,', @total_product_pitched,' as Value1, "" as Value2, "Intelligence" as Value3, "Product_Hybridizer" as Measure
FROM ',@total_intelligence_table,'
INNER JOIN CDM_Product_Master ON Reco_Product_1 = Product_Id
WHERE Story_Id = 1 AND Customer_Id = ',@cust_id,')
        UNION
        (select "Propensity (7 Days)" as Card,round(NextTrxn7_CLMSegment,2) as `Value1`,"" as `Value2`, "Intelligence" as `Value3`,"Propensity_7" as `Measure`  from  CLM_MCore_Customer_Percentile a  WHERE Customer_Id =  ',@cust_id,')
           UNION
        (select "Propensity (15 Days)" as Card,round(NextTrxn15_CLMSegment,2) as `Value1`,"" as `Value2`, "Intelligence" as `Value3`,"Propensity_15" as `Measure`  from  CLM_MCore_Customer_Percentile a  WHERE Customer_Id =  ',@cust_id,')
           UNION
        (select "Propensity (30 Days)" as Card,round(NextTrxn30_CLMSegment,2) as `Value1`,"" as `Value2`, "Intelligence" as `Value3`,"Propensity_30" as `Measure`  from  CLM_MCore_Customer_Percentile a  WHERE Customer_Id =  ',@cust_id,')
		');
       END IF; 
        
	SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Card_Name",`Card_Name`,"Value1",IFNULL(`Value1`,""),"Value2",`Value2`,"Value3",`Value3`,"Measure",`Measure`) )),"]")into @temp_result from V_Dashboard_explore_Card;' ;
     else
    SET @temp_result = JSON_OBJECT('message', "No data found");
        end if;
    END IF;
    
    IF IN_MEASURE='PERSONAL' AND IN_AGGREGATION_1 <> '' AND IN_AGGREGATION_2 = ''
    THEN
    set @cust_id="";
    SELECT Customer_Id INTO @cust_id from CDM_Customer_Key_Lookup WHERE Customer_Key = IN_AGGREGATION_1;
    SELECT @cust_id;
    if @cust_id<>"" then
    begin
		set @view_stmt=concat('create or replace view V_Dashboard_explore_Card as
        (select "Name" as Card_Name,',@name_measure,' as `Value1`,"" as `Value2`, "Demographics" as `Value3`,"NAME" as `Measure`  from ',@name_table,' WHERE Customer_Id = ',@cust_id,')
        UNION
		(select "PAN" as Card_Name,',@pan_measure,' as `Value1`,"" as `Value2`, "Demographics" as `Value3`,"PAN" as `Measure`  from ',@pan_table,' WHERE Customer_Id = ',@cust_id,')
        UNION
        (select "Gender" as Card_Name,',@gender_measure,' as `Value1`,"" as `Value2`, "Demographics" as `Value3`,"GENDER" as `Measure`  from CDM_Customer_Master a inner join CDM_LOV_Master b on a.Gender_LOV_Id = b.LOV_Id WHERE Customer_Id = ',@cust_id,')
        UNION
        (select "Mobile" as Card_Name,',@mobile_measure,' as `Value1`,"" as `Value2`, "Demographics" as `Value3`,"MOBILE" as `Measure`  from ',@mobile_table,' WHERE Customer_Id = ',@cust_id,')
        UNION
        (select "Email" as Card_Name,',@email_measure,' as `Value1`,"" as `Value2`, "Demographics" as `Value3`,"EMAIL" as `Measure`  from ',@email_table,' WHERE Customer_Id = ',@cust_id,')
        UNION
        (select "DOB" as Card_Name,',@dob_measure,' as `Value1`,"" as `Value2`, "Demographics" as `Value3`,"DOB" as `Measure`  from ',@dob_table,' WHERE Customer_Id = ',@cust_id,')
         union
          (select "Doj" as Card_Name,',@doj_measure,' as `Value1`,"" as `Value2`, "Demographics" as `Value3`,"Doj" as `Measure`  from ',@doj_table,' WHERE Customer_Id = ',@cust_id,')
		');
        
	SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Card_Name",`Card_Name`,"Value1",IFNULL(`Value1`,""),"Value2",`Value2`,"Value3",`Value3`,"Measure",`Measure`) )),"]")into @temp_result from V_Dashboard_explore_Card;' ;
    end;
    else
    SET @temp_result = JSON_OBJECT('message', "No data found");
        end if;
    END IF;
    
     IF IN_MEASURE='CARDS' AND IN_AGGREGATION_1 <> '' AND IN_AGGREGATION_2 = ''
    THEN
    set @cust_id="";
    SELECT Customer_Id INTO @cust_id from CDM_Customer_Key_Lookup WHERE Customer_Key = IN_AGGREGATION_1;
    SELECT @cust_id;
    if @cust_id<>"" then
		select Value1 into @showcards FROM Customer_One_View_Config WHERE Config_Name = 'Show SIP Card';
        select @showcards;
        IF @showcards = 'Yes'
        THEN
		set @view_stmt=concat('create or replace view V_Dashboard_explore_Card as
		(select "Recency (Days)" as Card_Name,',@recency_measure,' as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"RECENCY" as `Measure`  from ',@recency_table,' WHERE Customer_Id = ',@cust_id,')
		Union
		(select "Freq. (Active Period)" as Card_Name,',@freq12mth_measure,' as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"FREQ_12MTH" as `Measure`  from ',@freq12mth_table,' WHERE Customer_Id = ',@cust_id,')
		Union
		(select "Freq. (Ever)" as Card_Name,',@freqever_measure,' as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"FREQ_EVER" as `Measure`  from ',@freqever_table,' WHERE Customer_Id = ',@cust_id,')
		Union
		(select "ADGBT" as Card_Name,',@adgbt_measure,' as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"ADGBT" as `Measure`  from ',@adgbt_table,' WHERE Customer_Id = ',@cust_id,')
		Union
		(select "Tenure (Days)" as Card_Name,',@tenure_measure,' as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"TENURE" as `Measure`  from ',@tenure_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "Amt (Active Period)" as Card_Name,round((',@value12mth_measure,'),0) as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"VALUE_12MTH" as `Measure`  from ',@value12mth_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "Amt (ever)" as Card_Name,round((',@valueever_measure,'),0) as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"VALUE_EVER" as `Measure`  from ',@valueever_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "Avg. Ticket" as Card_Name,round((',@abv_measure,'),0) as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"ABV" as `Measure`  from ',@abv_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "First Transaction" as Card_Name,',@ftd_measure,' as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"FTD" as `Measure`  from ',@ftd_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "Recent Transaction" as Card_Name,',@ltd_measure,' as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"LTD" as `Measure`  from ',@ltd_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "Product Range" as Card_Name,',@productrange_measure,' as `Value1`,"" as `Value2`, (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias For Funds") as `Value3`,"PRODUCT_RANGE" as `Measure`  from ',@productrange_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias for Asset Class Range") as Card_Name,',@assetclassrange_measure,' as `Value1`,"" as `Value2`, (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias For Funds") as `Value3`,"ASSET_CLASS_RANGE" as `Measure`  from ',@assetclassrange_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias for Sub Asset Class Range") as Card_Name,',@subassetclassrange_measure,' as `Value1`,"" as `Value2`, (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias For Funds") as `Value3`,"SUB_ASSET_CLASS_RANGE" as `Measure`  from ',@subassetclassrange_table,' WHERE Customer_Id = ',@cust_id,')
        Union
        (select (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias for Fav Asset Class") as Card_Name,',@favassetclass_measure,' as `Value1`,"" as `Value2`, (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias For Funds") as `Value3`,"FAV_ASSET_CLASS" as `Measure`  from  Customer_One_View_Original a inner join CDM_Cat_Key_Lookup b on a.LT_Fav_Cat_LOV_Id = b.LOV_Id WHERE Customer_Id = ',@cust_id,')
        Union
        (select (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias for Fav Sub Asset Class") as Card_Name,',@favsubassetclass_measure,' as `Value1`,"" as `Value2`, (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias For Funds") as `Value3`,"FAV_SUB_ASSET_CLASS" as `Measure`  from  Customer_One_View_Original a inner join CDM_Dep_Key_Lookup b on a.LT_Fav_Dep_LOV_Id = b.LOV_Id WHERE Customer_Id = ',@cust_id,')
        Union
        (select "Fav Product" as Card_Name,',@favproduct_measure,' as `Value1`,"" as `Value2`, (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias For Funds") as `Value3`,"FAV_PRODUCT" as `Measure`  from  Customer_One_View_Original a inner join CDM_Product_Master b on a.LT_Fav_Prod_LOV_Id = b.Product_LOV_Id WHERE Customer_Id = ',@cust_id,')
	UNION
    	(select "Fav Type" as Card_Name,',@favtype_measure,' as `Value1`,"" as `Value2`, (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias For Funds") as `Value3`,"Fav_Type" as `Measure`  from ',@favtype_table,' WHERE Customer_Id = ',@cust_id,')
             UNION
     ( SELECT "Reco basis Fav. Asset Class" as Card_Name,', @total_product_pitched,' as Value1, "" as Value2, "Intelligence" as Value3, "Product_Pitched" as Measure
FROM ',@total_intelligence_table,'
INNER JOIN CDM_Product_Master ON Reco_Product_1 = Product_Id
WHERE Story_Id = 51 AND Customer_Id = ',@cust_id,')
   UNION
     ( SELECT "Reco basis Preference" as Card_Name,', @total_product_pitched,' as Value1, "" as Value2, "Intelligence" as Value3, "Product_ Preference" as Measure
FROM ',@total_intelligence_table,'
INNER JOIN CDM_Product_Master ON Reco_Product_1 = Product_Id
WHERE Story_Id = 32 AND Customer_Id = ',@cust_id,')
 UNION
     ( SELECT "Reco basis Genome" as Card_Name,', @total_product_pitched,' as Value1, "" as Value2, "Intelligence" as Value3, "Product_Reco basis Genome" as Measure
FROM ',@total_intelligence_table,'
left JOIN CDM_Product_Master ON Reco_Product_1 = Product_Id
WHERE Story_Id = 36 AND Customer_Id = ',@cust_id,')
UNION
     ( SELECT "Reco basis Trending" as Card_Name,', @total_product_pitched,' as Value1, "" as Value2, "Intelligence" as Value3, "Product_Reco basis Trending" as Measure
FROM ',@total_intelligence_table,'
INNER JOIN CDM_Product_Master ON Reco_Product_1 = Product_Id
WHERE Story_Id = 33 AND Customer_Id = ',@cust_id,')
UNION
     ( SELECT "Reco basis Hybridizer" as Card_Name,', @total_product_pitched,' as Value1, "" as Value2, "Intelligence" as Value3, "Product_Hybridizer" as Measure
FROM ',@total_intelligence_table,'
INNER JOIN CDM_Product_Master ON Reco_Product_1 = Product_Id
WHERE Story_Id = 1 AND Customer_Id = ',@cust_id,')

        UNION
    	(select "# Ongoing SIPs" as Card_Name,',@total_sip_measure,' as `Value1`,"" as `Value2`, "Funds" as `Value3`,"Total_SIP" as `Measure`  from ',@total_sip_table,' WHERE Customer_Id = ',@cust_id,')
        UNION
        (select "Propensity (7 Days)" as Card,round(NextTrxn7_CLMSegment,2) as `Value1`,"" as `Value2`, "Intelligence" as `Value3`,"Propensity_7" as `Measure`  from  CLM_MCore_Customer_Percentile a  WHERE Customer_Id =  ',@cust_id,')
           UNION
        (select "Propensity (15 Days)" as Card,round(NextTrxn15_CLMSegment,2) as `Value1`,"" as `Value2`, "Intelligence" as `Value3`,"Propensity_15" as `Measure`  from  CLM_MCore_Customer_Percentile a  WHERE Customer_Id =  ',@cust_id,')
           UNION
        (select "Propensity (30 Days)" as Card,round(NextTrxn30_CLMSegment,2) as `Value1`,"" as `Value2`, "Intelligence" as `Value3`,"Propensity_30" as `Measure`  from  CLM_MCore_Customer_Percentile a  WHERE Customer_Id =  ',@cust_id,')
   
		');
        end if;
        IF @showcards = 'No'
        THEN
		set @view_stmt=concat('create or replace view V_Dashboard_explore_Card as
		(select "Recency (Days)" as Card_Name,',@recency_measure,' as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"RECENCY" as `Measure`  from ',@recency_table,' WHERE Customer_Id = ',@cust_id,')
		Union
		(select "Freq. (Active Period)" as Card_Name,',@freq12mth_measure,' as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"FREQ_12MTH" as `Measure`  from ',@freq12mth_table,' WHERE Customer_Id = ',@cust_id,')
		Union
		(select "Freq. (Ever)" as Card_Name,',@freqever_measure,' as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"FREQ_EVER" as `Measure`  from ',@freqever_table,' WHERE Customer_Id = ',@cust_id,')
		Union
		(select "ADGBT" as Card_Name,',@adgbt_measure,' as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"ADGBT" as `Measure`  from ',@adgbt_table,' WHERE Customer_Id = ',@cust_id,')
		Union
		(select "Tenure (Days)" as Card_Name,',@tenure_measure,' as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"TENURE" as `Measure`  from ',@tenure_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "Amt (Active Period)" as Card_Name,round((',@value12mth_measure,'),0) as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"VALUE_12MTH" as `Measure`  from ',@value12mth_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "Amt (ever)" as Card_Name,round((',@valueever_measure,'),0) as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"VALUE_EVER" as `Measure`  from ',@valueever_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "Avg. Ticket" as Card_Name,round((',@abv_measure,'),0) as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"ABV" as `Measure`  from ',@abv_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "First Transaction" as Card_Name,',@ftd_measure,' as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"FTD" as `Measure`  from ',@ftd_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "Recent Transaction" as Card_Name,',@ltd_measure,' as `Value1`,"" as `Value2`, "Transactions" as `Value3`,"LTD" as `Measure`  from ',@ltd_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select "Product Range" as Card_Name,',@productrange_measure,' as `Value1`,"" as `Value2`, (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias For Funds") as `Value3`,"PRODUCT_RANGE" as `Measure`  from ',@productrange_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias for Asset Class Range") as Card_Name,',@assetclassrange_measure,' as `Value1`,"" as `Value2`, (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias For Funds") as `Value3`,"ASSET_CLASS_RANGE" as `Measure`  from ',@assetclassrange_table,' WHERE Customer_Id = ',@cust_id,')
        Union
		(select (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias for Sub Asset Class Range") as Card_Name,',@subassetclassrange_measure,' as `Value1`,"" as `Value2`, (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias For Funds") as `Value3`,"SUB_ASSET_CLASS_RANGE" as `Measure`  from ',@subassetclassrange_table,' WHERE Customer_Id = ',@cust_id,')
        Union
        (select (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias for Fav Asset Class") as Card_Name,',@favassetclass_measure,' as `Value1`,"" as `Value2`, (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias For Funds") as `Value3`,"FAV_ASSET_CLASS" as `Measure`  from  Customer_One_View_Original a inner join CDM_Cat_Key_Lookup b on a.LT_Fav_Cat_LOV_Id = b.LOV_Id WHERE Customer_Id = ',@cust_id,')
        Union
        (select (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias for Fav Sub Asset Class") as Card_Name,',@favsubassetclass_measure,' as `Value1`,"" as `Value2`, (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias For Funds") as `Value3`,"FAV_SUB_ASSET_CLASS" as `Measure`  from  Customer_One_View_Original a inner join CDM_Dep_Key_Lookup b on a.LT_Fav_Dep_LOV_Id = b.LOV_Id WHERE Customer_Id = ',@cust_id,')
        Union
        (select "Fav Product" as Card_Name,',@favproduct_measure,' as `Value1`,"" as `Value2`, (SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias For Funds") as `Value3`,"FAV_PRODUCT" as `Measure`  from  Customer_One_View_Original a inner join CDM_Product_Master b on a.LT_Fav_Prod_LOV_Id = b.Product_LOV_Id WHERE Customer_Id = ',@cust_id,')
	UNION
     ( SELECT "Reco basis Fav. Asset Class" as Card_Name,', @total_product_pitched,' as Value1, "" as Value2, "Intelligence" as Value3, "Product_Pitched" as Measure
FROM ',@total_intelligence_table,'
INNER JOIN CDM_Product_Master ON Reco_Product_1 = Product_Id
WHERE Story_Id = 51 AND Customer_Id = ',@cust_id,')
   UNION
     ( SELECT "Reco basis Preference" as Card_Name,', @total_product_pitched,' as Value1, "" as Value2, "Intelligence" as Value3, "Product_ Preference" as Measure
FROM ',@total_intelligence_table,'
INNER JOIN CDM_Product_Master ON Reco_Product_1 = Product_Id
WHERE Story_Id = 32 AND Customer_Id = ',@cust_id,')
 UNION
     ( SELECT "Reco basis Genome" as Card_Name,', @total_product_pitched,' as Value1, "" as Value2, "Intelligence" as Value3, "Product_Reco basis Genome" as Measure
FROM ',@total_intelligence_table,'
left JOIN CDM_Product_Master ON Reco_Product_1 = Product_Id
WHERE Story_Id = 36 AND Customer_Id = ',@cust_id,')
UNION
     ( SELECT "Reco basis Trending" as Card_Name,', @total_product_pitched,' as Value1, "" as Value2, "Intelligence" as Value3, "Product_Reco basis Trending" as Measure
FROM ',@total_intelligence_table,'
INNER JOIN CDM_Product_Master ON Reco_Product_1 = Product_Id
WHERE Story_Id = 33 AND Customer_Id = ',@cust_id,')
UNION
     ( SELECT "Reco basis Hybridizer" as Card_Name,', @total_product_pitched,' as Value1, "" as Value2, "Intelligence" as Value3, "Product_Hybridizer" as Measure
FROM ',@total_intelligence_table,'
INNER JOIN CDM_Product_Master ON Reco_Product_1 = Product_Id
WHERE Story_Id = 1 AND Customer_Id = ',@cust_id,')
        UNION
        (select "Propensity (7 Days)" as Card,round(NextTrxn7_CLMSegment,2) as `Value1`,"" as `Value2`, "Intelligence" as `Value3`,"Propensity_7" as `Measure`  from  CLM_MCore_Customer_Percentile a  WHERE Customer_Id =  ',@cust_id,')
           UNION
        (select "Propensity (15 Days)" as Card,round(NextTrxn15_CLMSegment,2) as `Value1`,"" as `Value2`, "Intelligence" as `Value3`,"Propensity_15" as `Measure`  from  CLM_MCore_Customer_Percentile a  WHERE Customer_Id =  ',@cust_id,')
           UNION
        (select "Propensity (30 Days)" as Card,round(NextTrxn30_CLMSegment,2) as `Value1`,"" as `Value2`, "Intelligence" as `Value3`,"Propensity_30" as `Measure`  from  CLM_MCore_Customer_Percentile a  WHERE Customer_Id =  ',@cust_id,')
   
		');
        end if;
	SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Card_Name",`Card_Name`,"Value1",IFNULL(`Value1`,""),"Value2",`Value2`,"Value3",`Value3`,"Measure",`Measure`) )),"]")into @temp_result from V_Dashboard_explore_Card;' ;
    else
    SET @temp_result = JSON_OBJECT('message', "No data found");
        end if;
    END IF;
    
     IF IN_MEASURE='CARD1' AND IN_AGGREGATION_1 <> '' AND IN_AGGREGATION_2 = ''
    THEN
    set @cust_id="";
    SELECT Customer_Id INTO @cust_id from CDM_Customer_Key_Lookup WHERE Customer_Key = IN_AGGREGATION_1;
    SELECT @cust_id;
    if @cust_id<>"" then
    begin
		set @view_stmt=concat('create or replace view V_Dashboard_explore_Card1 as
						SELECT "Recent Purchase" as Heading,
						(SELECT Product_Comm_Name AS Value1 FROM Customer_One_View_Original A, CDM_Product_Master B WHERE A.LTD_Fav_Prod_LOV_Id = B.Product_LOV_Id AND Customer_Id = ',@cust_Id,') AS Fund,
						(SELECT Cat_Key AS Value1 FROM Customer_One_View_Original A, CDM_Cat_Key_Lookup B WHERE A.LTD_Fav_Cat_LOV_Id = B.LOV_Id AND Customer_Id = ',@cust_Id,') AS "Asset Class",
						(SELECT Dep_Key AS Value1 FROM Customer_One_View_Original A, CDM_Dep_Key_Lookup B WHERE A.LTD_Fav_Dep_LOV_Id = B.LOV_Id AND Customer_Id = ',@cust_Id,') AS "Sub Asset Class",
						date_format(LTD,"%d %b %Y") as "Date",
						(SELECT Store_Name AS Value1 FROM Customer_One_View_Original A, CDM_Store_Master B WHERE A.Ltd_Store_LOV_Id = B.LOV_Id AND Customer_Id = ',@cust_Id,') AS "Channel"
						from Customer_One_View_Original
                        WHERE Customer_Id = ',@cust_Id,'
		');
        
	SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Heading",`Heading`,(SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias for Funds"),`Fund`,(SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias for Asset Class Range"),`Asset Class`,(SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias for Sub Asset Class Range"),`Sub Asset Class`,"Date",`Date`,"Channel",`Channel`) )),"]")into @temp_result from V_Dashboard_explore_Card1;' ;
    end;
    else
    SET @temp_result = JSON_OBJECT('message', "No data found");
        end if;
    END IF;
    
         IF IN_MEASURE='CARD2' AND IN_AGGREGATION_1 <> '' AND IN_AGGREGATION_2 = ''
    THEN
    set @cust_id="";
    SELECT Customer_Id INTO @cust_id from CDM_Customer_Key_Lookup WHERE Customer_Key = IN_AGGREGATION_1;
    SELECT @cust_id;
    if @cust_id<>"" then
    begin
    select Event_Execution_Id into V_Event_Execution_Id from Event_Execution_History where Customer_Id = @cust_id order by Event_Execution_Date_ID DESC LIMIT 1;
        select V_Event_Execution_Id;
        
        IF V_Event_Execution_Id <> '' and V_Event_Execution_Id is not null
        then
   
            select ifnull(`Name`,'') into @campaign from Event_Master where ID = (select Event_Id from Event_Execution_History where Event_Execution_ID = V_Event_Execution_Id);
            select Communication_Channel into @channel from Event_Execution_History where Event_Execution_ID = V_Event_Execution_Id;
            select date_format(Event_Execution_Date_ID,"%d/%m/%Y") into @eedate from Event_Execution_History where Event_Execution_ID = V_Event_Execution_Id;
            select "Delivered" into @delivery_stat;
            select case when @eeid in (select distinct Event_Execution_ID from Event_Attribution_History) THEN "Responded" else "No Response" END into @response;
            SET @campaign = ifnull(@campaign,' ');
		ELSE
			SET @campaign="No Campaigns Done Yet";
            SET @channel="";
            SET @delivery_stat="";
            SET @response="";
            SET @eedate="";
            
	END IF;
    
		set @view_stmt=concat('create or replace view V_Dashboard_explore_Card2 as
						select "Recent Campaign" as Heading,
                        "',@campaign,'" as value1,
                        "',@eedate,'" as value2,
                        "',@channel,'" as value3,
                        "',@delivery_stat,'" as value4,
                        "',@response,'" as value5;
		');
        
	SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Heading",`Heading`,"Campaign",`value1`,"Date",`value2`,"Channel",`value3`,"Delivery Status",`value4`,"Response Status",`value5`) )),"]")into @temp_result from V_Dashboard_explore_Card2;' ;
    end;
    else
    SET @temp_result = JSON_OBJECT('message', "No data found");
    
    end if;
    END IF;
    
    
IF IN_MEASURE = 'CARD3' AND IN_AGGREGATION_1 <> '' AND IN_AGGREGATION_2 = '' THEN

set @cust_id="";
    SELECT Customer_Id INTO @cust_id FROM CDM_Customer_Key_Lookup WHERE Customer_Key = IN_AGGREGATION_1;
    select Value1 into @showcards FROM Customer_One_View_Config WHERE Config_Name = 'Show SIP Card';
    select @showcards;
	if @cust_id<>"" AND @showcards="Yes" then
    SET @view_stmt = CONCAT('CREATE OR REPLACE VIEW V_Dashboard_explore_Card3 AS
                        SELECT "Recent SIP" AS Heading,Total_Active_SIPs AS `SIP HELD`,SIP_1_Name AS SIP_1,SIP_1_Renewal_Date AS SIP1_Renewal_Date,SIP_2_Name AS SIP_2,SIP_2_Renewal_Date AS SIP2_Renewal_Date,SIP_3_Name AS SIP_3,SIP_3_Renewal_Date  AS SIP3_Renewal_Date
                        FROM SVOC_MF
                        WHERE Customer_Id = ', @cust_Id, ';');
    
    SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(JSON_OBJECT("Heading", `Heading`, "SIP HELD", `SIP HELD`, "SIP 1", `SIP_1`, "SIP1_Renewal_Date", `SIP1_Renewal_Date`, "SIP 2", `SIP_2`, "SIP2_Renewal_Date", `SIP2_Renewal_Date`, "SIP 3", `SIP_3`,"SIP3_Renewal_Date", `SIP3_Renewal_Date`))),"]") INTO @temp_result FROM V_Dashboard_explore_Card3;';
    end if;
    
 if @cust_id<>"" AND @showcards="No" then
	SET @view_stmt = CONCAT('CREATE OR REPLACE VIEW V_Dashboard_explore_Card3 AS
                        SELECT "No data found" AS Message;');
    SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(JSON_OBJECT("Message", `Message`))),"]") INTO @temp_result FROM V_Dashboard_explore_Card3;';
    end if;
    
END IF;


IF IN_MEASURE = 'CARD3' AND IN_AGGREGATION_1 = '' AND IN_AGGREGATION_2 <>'' THEN

set @cust_id="";
    SELECT Customer_Id INTO @cust_id FROM Customer_One_View_Original_1 WHERE Mobile = IN_AGGREGATION_2;
    select Value1 into @showcards FROM Customer_One_View_Config WHERE Config_Name = 'Show SIP Card';
    select @showcards;
	if @cust_id<>"" AND @showcards="Yes" then
    SET @view_stmt = CONCAT('CREATE OR REPLACE VIEW V_Dashboard_explore_Card3 AS
                        SELECT "Recent SIP" AS Heading,Total_Active_SIPs AS `SIP HELD`,SIP_1_Name AS SIP_1,SIP_1_Renewal_Date AS SIP1_Renewal_Date,SIP_2_Name AS SIP_2,SIP_2_Renewal_Date AS SIP2_Renewal_Date,SIP_3_Name AS SIP_3,SIP_3_Renewal_Date  AS SIP3_Renewal_Date
                        FROM SVOC_MF
                        WHERE Customer_Id = ', @cust_Id, ';');
    
    SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(JSON_OBJECT("Heading", `Heading`, "SIP HELD", `SIP HELD`, "SIP 1", `SIP_1`, "SIP1_Renewal_Date", `SIP1_Renewal_Date`, "SIP 2", `SIP_2`, "SIP2_Renewal_Date", `SIP2_Renewal_Date`, "SIP 3", `SIP_3`,"SIP3_Renewal_Date", `SIP3_Renewal_Date`))),"]") INTO @temp_result FROM V_Dashboard_explore_Card3;';
    end if;
    
 if @cust_id<>"" AND @showcards="No" then
	SET @view_stmt = CONCAT('CREATE OR REPLACE VIEW V_Dashboard_explore_Card3 AS
                        SELECT "No data found" AS Message;');
    SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(JSON_OBJECT("Message", `Message`))),"]") INTO @temp_result FROM V_Dashboard_explore_Card3;';
    end if;
    
END IF;


	 IF IN_MEASURE='CARD1' AND IN_AGGREGATION_1 = '' AND IN_AGGREGATION_2 <> ''
    THEN
    set @cust_id="";
    SELECT Customer_Id INTO @cust_id from Customer_One_View_Original_1 WHERE Mobile = IN_AGGREGATION_2;
    SELECT @cust_id;
    if @cust_id<>"" then
    begin
		set @view_stmt=concat('create or replace view V_Dashboard_explore_Card1 as
						SELECT "Recent Purchase" as Heading,
						(SELECT Product_Comm_Name AS Value1 FROM Customer_One_View_Original A, CDM_Product_Master B WHERE A.LTD_Fav_Prod_LOV_Id = B.Product_LOV_Id AND Customer_Id = ',@cust_Id,') AS Fund,
						(SELECT Cat_Key AS Value1 FROM Customer_One_View_Original A, CDM_Cat_Key_Lookup B WHERE A.LTD_Fav_Cat_LOV_Id = B.LOV_Id AND Customer_Id = ',@cust_Id,') AS "Asset Class",
						(SELECT Dep_Key AS Value1 FROM Customer_One_View_Original A, CDM_Dep_Key_Lookup B WHERE A.LTD_Fav_Dep_LOV_Id = B.LOV_Id AND Customer_Id = ',@cust_Id,') AS "Sub Asset Class",
						date_format(LTD,"%d %b %Y") as "Date",
						(SELECT Store_Name AS Value1 FROM Customer_One_View_Original A, CDM_Store_Master B WHERE A.Ltd_Store_LOV_Id = B.LOV_Id AND Customer_Id = ',@cust_Id,') AS "Channel"
						from Customer_One_View_Original
                        WHERE Customer_Id = ',@cust_Id,'
		');
        
	SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Heading",`Heading`,"Fund",`Fund`,(SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias for Asset Class Range"),`Asset Class`,(SELECT Value1 FROM Customer_One_View_Config WHERE Config_Name = "Alias for Sub Asset Class Range"),`Sub Asset Class`,"Date",`Date`,"Channel",`Channel`) )),"]")into @temp_result from V_Dashboard_explore_Card1;' ;
    end;
    else
    SET @temp_result = JSON_OBJECT('message', "No data found");
    end if;
    END IF;
    
         IF IN_MEASURE='CARD2' AND IN_AGGREGATION_1 = '' AND IN_AGGREGATION_2 <> ''
    THEN
	set @cust_id="";
    SELECT Customer_Id INTO @cust_id from Customer_One_View_Original_1 WHERE Mobile = IN_AGGREGATION_2;
    SELECT @cust_id;
    if @cust_id<>"" then
    begin
    
    select Event_Execution_Id into V_Event_Execution_Id from Event_Execution_History where Customer_Id = @cust_id order by Event_Execution_Date_ID DESC LIMIT 1;
        select V_Event_Execution_Id;
        
        IF V_Event_Execution_Id <> '' and V_Event_Execution_Id is not null
        then
   
            select ifnull(`Name`,'') into @campaign from Event_Master where ID = (select Event_Id from Event_Execution_History where Event_Execution_ID = V_Event_Execution_Id);
            select Communication_Channel into @channel from Event_Execution_History where Event_Execution_ID = V_Event_Execution_Id;
            select date_format(Event_Execution_Date_ID,"%d/%m/%Y") into @eedate from Event_Execution_History where Event_Execution_ID = V_Event_Execution_Id;
            select "Delivered" into @delivery_stat;
            select case when @eeid in (select distinct Event_Execution_ID from Event_Attribution_History) THEN "Responded" else "No Response" END into @response;
            SET @campaign = ifnull(@campaign,' ');
		ELSE
			SET @campaign="No Campaigns Done Yet";
            SET @channel="";
            SET @delivery_stat="";
            SET @response="";
            SET @eedate="";
            
	END IF;
    
		set @view_stmt=concat('create or replace view V_Dashboard_explore_Card2 as
						select "Recent Campaign" as Heading,
                        "',@campaign,'" as value1,
                        "',@eedate,'" as value2,
                        "',@channel,'" as value3,
                        "',@delivery_stat,'" as value4,
                        "',@response,'" as value5;
		');
        
	SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Heading",`Heading`,"Campaign",`value1`,"Date",`value2`,"Channel",`value3`,"Delivery Status",`value4`,"Response Status",`value5`) )),"]")into @temp_result from V_Dashboard_explore_Card2;' ;
    end;
    else
    SET @temp_result = JSON_OBJECT('message', "No data found");
    
    end if;
    END IF;
    
     if IN_AGGREGATION_1="DISPLAY" Then
    SET @view_stmt = concat('CREATE OR REPLACE VIEW V_UI_Dashboard_Customer_Config
							AS  	select Config_Name,Value1 from Customer_One_View_Config;');
			SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Config_Name",`Config_Name`,"Value1",`Value1`) )),"]")
			into @temp_result from V_UI_Dashboard_Customer_Config;') ;
	end if;
    
    IF IN_AGGREGATION_1 = "EDIT"
    THEN
        
			SET SQL_SAFE_UPDATES=0;
            
			UPDATE  Customer_One_View_Config
            SET Value1 = IN_FUNDS
            WHERE  Config_Name = "Alias For Funds";
            
            UPDATE   Customer_One_View_Config
            SET Value1 = IN_ASSET_CLASS
            WHERE  Config_Name = "Alias for Asset Class Range";
            
            UPDATE   Customer_One_View_Config
            SET Value1 = IN_SUB_ASSET_CLASS
            WHERE  Config_Name = "Alias for Sub Asset Class Range";
            
            UPDATE   Customer_One_View_Config
            SET Value1 = IN_FAV_CLASS
            WHERE  Config_Name = "Alias for Fav Asset Class";
            
            UPDATE   Customer_One_View_Config
            SET Value1 = IN_FAV_SUB_CLASS
            WHERE  Config_Name = "Alias for Fav Sub Asset Class";
            
            UPDATE   Customer_One_View_Config
            SET Value1 = IN_SIP
            WHERE  Config_Name = "Show SIP Card";
		
        END IF;

	IF IN_MEASURE <> "DOWNLOAD"
    THEN
	select @view_stmt;
	PREPARE statement from @view_stmt;
	Execute statement;
	Deallocate PREPARE statement;

	select @Query_String;
	PREPARE statement from @Query_String;
	Execute statement;
	Deallocate PREPARE statement; 
    SET out_result_json = @temp_result;
    END IF;
    
END$$
DELIMITER ;
