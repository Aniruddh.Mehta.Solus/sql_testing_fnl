Drop table if EXISTS UI_Variables_Sample_Data;
CREATE TABLE `UI_Variables_Sample_Data` (
`Variable_View_Name` varchar(128) DEFAULT NULL,
  `Variable_Name` varchar(128) DEFAULT NULL,
  `Vairable_Sample_Data` varchar(128) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



Insert Ignore into UI_Variables_Sample_Data(Variable_View_Name,Variable_Name)
SELECT 'Sample_Data',COLUMN_NAME
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = 'Customer_One_View';


Insert Ignore into UI_Variables_Sample_Data(Variable_View_Name,Variable_Name)
SELECT 'Sample_Data',COLUMN_NAME
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = 'Customer_Details_View';

Insert Ignore into UI_Variables_Sample_Data(Variable_View_Name,Variable_Name)
SELECT 'Sample_Data',COLUMN_NAME
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = 'V_CLM_Bill_Detail_One_View';