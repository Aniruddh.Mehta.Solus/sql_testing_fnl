DELIMITER $$
CREATE or REPLACE PROCEDURE `Update_Segment_Id_Historic`()
begin

	DECLARE vStart_Cnt BIGINT DEFAULT 0;
    DECLARE vEnd_Cnt BIGINT DEFAULT 0;
	DECLARE vBatchSize BIGINT DEFAULT 100000;
    SET SQL_SAFE_UPDATES=0;
    set foreign_key_checks=0;
	SELECT Customer_Id INTO @Rec_Cnt FROM CDM_Customer_Master ORDER BY Customer_Id DESC LIMIT 1;
	SET vStart_Cnt = 0;
PROCESS_LOOP: LOOP
			SET vEnd_Cnt = vStart_Cnt + vBatchSize;
        insert into log_solus(module,rec_start,rec_end)values('Ashutosh_Loop_Updates',vStart_Cnt,vEnd_Cnt);
        select vStart_Cnt,vEnd_Cnt;
        select current_timestamp();
        
        update Event_Execution_History a,Event_Master b
		set a.Segment_Id=b.Arms
		where a.Event_Id=b.ID
		and a.Customer_Id between vStart_Cnt and vEnd_Cnt;

				SET vStart_Cnt=vEnd_Cnt+1;
        IF vStart_Cnt  > @Rec_Cnt THEN
            LEAVE PROCESS_LOOP;
        END IF;         
 END LOOP PROCESS_LOOP;
end$$
DELIMITER ;
