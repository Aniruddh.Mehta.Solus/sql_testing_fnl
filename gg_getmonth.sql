DROP FUNCTION IF EXISTS gg_getmonth;

CREATE  FUNCTION gg_getmonth() RETURNS char(2) CHARSET latin1
BEGIN
set @month=month(current_date());
set @day=day(current_date());
if ( @day > 21 )
then
set @month=@month+1;
end if;

if @month < 10
then
set @month= concat('0',@month);
end if;

return @month;
END


