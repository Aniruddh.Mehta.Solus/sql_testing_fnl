DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_UI_Dashboard_Response_Attr_Configs`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 INT(20);
    declare IN_AGGREGATION_4 text;
    declare IN_AGGREGATION_5 text;
    declare IN_AGGREGATION_6 text;
	declare IN_FTD TEXT;
    declare IN_LTD TEXT;
    declare IN_BILLHEADER TEXT;
    declare IN_DELIVERY TEXT;
    declare IN_HOURS TEXT;
    declare IN_HARDRESPONSE TEXT;
    
    
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_AGGREGATION_4 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_4"));
    SET IN_AGGREGATION_5 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_5"));
    SET IN_AGGREGATION_6 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_6"));
    SET IN_FTD = json_unquote(json_extract(in_request_json,"$.R_FTD"));
    SET IN_LTD = json_unquote(json_extract(in_request_json,"$.R_LTD"));
    SET IN_BILLHEADER = json_unquote(json_extract(in_request_json,"$.R_BILLHEADER"));
    SET IN_DELIVERY = json_unquote(json_extract(in_request_json,"$.R_DELIVERY"));
    SET IN_HOURS = json_unquote(json_extract(in_request_json,"$.R_HOURS"));
    SET IN_HARDRESPONSE = json_unquote(json_extract(in_request_json,"$.R_HARDRESPONSE"));

	IF IN_AGGREGATION_1 = "DISPLAY"
    THEN
		IF IN_AGGREGATION_2 = "RESPONSE"
        THEN
				
			SET @view_stmt = concat('CREATE OR REPLACE VIEW V_UI_Dashboard_Global_Config
							AS  	select Name,`Value` from CLM_Response_Config
									where Name in ("Response_LTD","Response_FTD");');
			SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Config_Name",`Name`,"Value1",`Value`) )),"]")
			into @temp_result from V_UI_Dashboard_Global_Config;') ;	
		
        END IF;
		IF IN_AGGREGATION_2 = "BILLHEADER"
        THEN
				
			SET @view_stmt = concat('CREATE OR REPLACE VIEW V_UI_Dashboard_Global_Config
							AS  	select Name,`Value` from CLM_Response_Config
									where Name in ("Override_Revenue_From_BillHeader");');
			SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Config_Name",`Name`,"Value1",`Value`) )),"]")
			into @temp_result from V_UI_Dashboard_Global_Config;') ;	
		
        END IF;
		IF IN_AGGREGATION_2 = "DELIVERY"
        THEN
				
			SET @view_stmt = concat('CREATE OR REPLACE VIEW V_UI_Dashboard_Global_Config
							AS  	select Name,`Value` from CLM_Response_Config
									where Name in ("Consider_No_Delivery_Report_As_Undelivered");');
			SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Config_Name",`Name`,"Value1",`Value`) )),"]")
			into @temp_result from V_UI_Dashboard_Global_Config;') ;	
		
        END IF;
		IF IN_AGGREGATION_2 = "HOURS"
        THEN
				
			SET @view_stmt = concat('CREATE OR REPLACE VIEW V_UI_Dashboard_Global_Config
							AS  	select Name,`Value` from CLM_Response_Config
									where Name in ("Use_Hours_As_Response_Window");');
			SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Config_Name",`Name`,"Value1",`Value`) )),"]")
			into @temp_result from V_UI_Dashboard_Global_Config;') ;	
		
        END IF;
		IF IN_AGGREGATION_2 = "HARDRESPONSE"
        THEN
				
			SET @view_stmt = concat('CREATE OR REPLACE VIEW V_UI_Dashboard_Global_Config
							AS  	select Name,TRIM(`Value`) AS Value from CLM_Response_Config
									where Name in ("Calculate_Hard_Response");');
			SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Config_Name",`Name`,"Value1",`Value`) )),"]")
			into @temp_result from V_UI_Dashboard_Global_Config;') ;	
		
        END IF;
		IF IN_AGGREGATION_2 = "DATE"
        THEN
				
			SET @view_stmt = concat('CREATE OR REPLACE VIEW V_UI_Dashboard_Global_Config
							AS  	select Name,`Value` from CLM_Response_Config
									where Name in ("Last_Response_Execution_Date");');
			SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Config_Name",`Name`,"Value1",`Value`) )),"]")
			into @temp_result from V_UI_Dashboard_Global_Config;') ;	
		
        END IF;
		
	END IF;
	
    
    IF IN_AGGREGATION_1 = "EDIT"
    THEN
		IF IN_AGGREGATION_2 = "RESPONSE"
        THEN
        
			SET SQL_SAFE_UPDATES=0;
				
			UPDATE  CLM_Response_Config
            SET `Value` = IN_FTD
            WHERE  `Name` = "Response_FTD";
            
            UPDATE  CLM_Response_Config
            SET `Value` = IN_LTD
            WHERE  `Name` = "Response_LTD";	
		
        END IF;
		IF IN_AGGREGATION_2 = "BILLHEADER"
        THEN
        
			SET SQL_SAFE_UPDATES=0;
				
			UPDATE CLM_Response_Config
            SET `Value` = IN_BILLHEADER
            WHERE `Name` = "Override_Revenue_From_BillHeader";
		
        END IF;
		IF IN_AGGREGATION_2 = "DELIVERY"
        THEN
        
			SET SQL_SAFE_UPDATES=0;
				
			UPDATE CLM_Response_Config
            SET `Value` = IN_DELIVERY
            WHERE `Name` = "Consider_No_Delivery_Report_As_Undelivered";
		
        END IF;
		IF IN_AGGREGATION_2 = "HOURS"
        THEN
        
			SET SQL_SAFE_UPDATES=0;
				
			UPDATE CLM_Response_Config
            SET `Value` = IN_HOURS
            WHERE `Name` = "Use_Hours_As_Response_Window";
		
        END IF;
		IF IN_AGGREGATION_2 = "HARDRESPONSE"
        THEN
        
			SET SQL_SAFE_UPDATES=0;
				
			UPDATE CLM_Response_Config
            SET `Value` = IN_HARDRESPONSE
            WHERE `Name` = "Calculate_Hard_Response";
		
        END IF;
        
	END IF;
    
    IF IN_SCREEN <> "DOWNLOAD"
    THEN
    select @view_stmt;
	PREPARE statement from @view_stmt;
	Execute statement;
	Deallocate PREPARE statement;
	SELECT @Query_String;
	PREPARE statement from @Query_String;
	Execute statement;
	Deallocate PREPARE statement; 
    SET out_result_json = @temp_result;
    ELSE
		call S_dashboard_performance_download('V_Dashboard_Camp_seg_report_col',@out_result_json1);
		SELECT @out_result_json1 INTO out_result_json;
    END IF;
    
END$$
DELIMITER ;
