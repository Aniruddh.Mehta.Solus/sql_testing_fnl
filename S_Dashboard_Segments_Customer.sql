DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_Dashboard_Segments_Customer`(IN vBatchSize bigint)
begin
	DECLARE vStart_Cnt BIGINT DEFAULT 0;
    DECLARE vEnd_Cnt BIGINT DEFAULT 0;
    DECLARE done1,done2 INT DEFAULT FALSE;
    DECLARE SegmentId_cur1,Exe_ID int;
    DECLARE SegmentReplacedQuery_cur1 TEXT;
    DECLARE vSuccess int (4);                      
	DECLARE vErrMsg Text ; 
    -- declare cur2 cursor for
    -- select distinct SegmentType from Dashboard_Segments_Master where SegmentReplacedQuery <> '' and IsValidCondition=1 and IsActive=1;
    Declare cur1 cursor for
    select distinct SegmentId,TRIM(SegmentReplacedQuery) from Dashboard_Segments_Master
    where SegmentReplacedQuery <> '' and IsValidCondition=1 and IsActive=1
    ORDER BY SegmentSequence ASC;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done1 = TRUE;
    DECLARE CONTINUE HANDLER FOR SQLWARNING
	BEGIN
		GET DIAGNOSTICS CONDITION 1 @errsqlstate=RETURNED_SQLSTATE, @errno=MYSQL_ERRNO, @errtext = MESSAGE_TEXT;
		SET @logmsg=CONCAT(ifnull(@errsqlstate,'WARNING') , ifnull(@errno,'1'), ifnull(@errtext,'WARNING MESSAGE'));
		
		SELECT 
    'S_Dashboard_Segments_Customer : Warning Message :' AS '',
    @logmsg AS '';
	END;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		GET DIAGNOSTICS CONDITION 1 @errsqlstate=RETURNED_SQLSTATE, @errno=MYSQL_ERRNO, @errtext = MESSAGE_TEXT;
		SET @logmsg=CONCAT(ifnull(@errsqlstate,'ERROR') , ifnull(@errno,'1'), ifnull(@errtext,'ERROR MESSAGE'));
		SET vErrMsg=@logmsg;
		SET vSuccess=0;
		SELECT 
    'S_Dashboard_Segments_Customer : Error Message :' AS '',
    @logmsg AS '';
	END;
    SET done1 = FALSE;
    SET SQL_SAFE_UPDATES=0;
    set foreign_key_checks=0;
    TRUNCATE Dashboard_Segments_Customer;
	SELECT 
    Customer_Id
INTO @Rec_Cnt FROM
    CDM_Customer_Master
ORDER BY Customer_Id DESC
LIMIT 1;
    open cur1;
	new_loop_1: LOOP
	FETCH cur1 into SegmentId_cur1,SegmentReplacedQuery_cur1;
	IF done1 THEN
			 LEAVE new_loop_1;
	END IF;
	SET vStart_Cnt = 0;
SELECT SegmentId_cur1, SegmentReplacedQuery_cur1;
    /*select count(1) into @Customers_selected from Customer_One_View where SegmentReplacedQuery_cur1;
    select @Customers_selected;
    If @Customers_selected<=0
    THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR : Segment Condition selects 0 customers disable this and run again';
    END IF;
*/
	PROCESS_LOOP: LOOP
		SET vEnd_Cnt = vStart_Cnt + vBatchSize;
        INSERT IGNORE INTO Dashboard_Segments_Customer(Customer_Id,SegmentId)
        SELECT Customer_Id,-1 from CDM_Customer_Master where Customer_Id between vStart_Cnt and vEnd_Cnt
        and Customer_Id not in (Select Customer_Id from Dashboard_Segments_Customer);
        
        set @sqlCust = concat( 'UPDATE Dashboard_Segments_Customer A, (select Customer_Id from V_CLM_Customer_One_View where Customer_Id between ',vStart_Cnt,' AND ',vEnd_Cnt,' AND ',SegmentReplacedQuery_cur1,') B
SET A.SegmentId=',SegmentId_cur1,'
where A.SegmentId=-1 AND
A.Customer_Id = B.Customer_Id;');

        prepare stmt1 from @sqlCust;
		execute stmt1;
		deallocate prepare stmt1;
        
		SET vStart_Cnt=vEnd_Cnt+1;
        IF vStart_Cnt  >= @Rec_Cnt THEN
            LEAVE PROCESS_LOOP;
        END IF;         
 END LOOP PROCESS_LOOP;
 end LOOP;
	close cur1;
	SELECT 
    Customer_Id
INTO vStart_Cnt FROM
    Dashboard_Segments_Customer
ORDER BY Customer_Id ASC
LIMIT 1;
	SELECT 
    Customer_Id
INTO @Rec_Cnt FROM
    Dashboard_Segments_Customer
ORDER BY Customer_Id DESC
LIMIT 1;
TRUNCATE CDM_Customer_Segment;
INSERT IGNORE INTO `CDM_Customer_Segment`
	(`Customer_Id`,
	`CLMSegmentName`,
	`sequence`)
	Values
	(-1, 'NA', 0);
	PROCESS_LOOP: LOOP
	SET vEnd_Cnt = vStart_Cnt + vBatchSize;

	INSERT IGNORE INTO `CDM_Customer_Segment`
	(`Customer_Id`,
	`CLMSegmentName`,
	`sequence`)
	SELECT 
		Customer_Id,b.SegmentName,b.SegmentSequence
        from Dashboard_Segments_Customer a join Dashboard_Segments_Master b
        on a.SegmentId=b.SegmentId
	WHERE a.Customer_id between vStart_Cnt and vEnd_Cnt-1 
	AND `a`.`Customer_Id` > 0
    and b.SegmentType='CLM';

	SET vStart_Cnt = vEnd_Cnt;
	IF vStart_Cnt  >= @Rec_Cnt THEN
		LEAVE PROCESS_LOOP;
	END IF;        
	END LOOP PROCESS_LOOP;  

end$$
DELIMITER ;