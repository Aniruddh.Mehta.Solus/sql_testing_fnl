DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_UI_Dashboard_Global_Configs`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_MEASURE text; 
	declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 INT(20);
    declare IN_AGGREGATION_4 text;
    declare IN_AGGREGATION_5 text;
    declare IN_AGGREGATION_6 text;
    declare IN_COST_PER_PN DECIMAL(20,5);
    declare IN_COST_PER_SMS DECIMAL(20,5);
    declare IN_COST_PER_WHATSAPP DECIMAL(20,5);
    declare IN_COST_PER_EMAIL DECIMAL(20,5);
    declare IN_MONTHLY_CREATIVE BIGINT(20);
    declare IN_MONTHLY_PLATFORM BIGINT(20);
    declare IN_CURRENCY TEXT;
    declare IN_INCR_METHOD TEXT;
    declare IN_PRIMARY_CHANNEL TEXT;
    declare IN_FY_CY TEXT;
    declare IN_DOW TEXT;
	declare IN_FTD TEXT;
    declare IN_LTD TEXT;
    declare IN_FY_START_DATE TEXT;
    declare IN_FY_END_DATE TEXT;
    declare IN_CY_START_DATE TEXT;
    declare IN_CY_END_DATE TEXT;
    declare IN_CAMP_THEME TEXT;
    
    
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_AGGREGATION_4 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_4"));
    SET IN_AGGREGATION_5 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_5"));
    SET IN_AGGREGATION_6 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_6"));
    SET IN_COST_PER_PN = json_unquote(json_extract(in_request_json,"$.COST_PER_PN"));
    SET IN_COST_PER_SMS = json_unquote(json_extract(in_request_json,"$.COST_PER_SMS"));
    SET IN_COST_PER_WHATSAPP = json_unquote(json_extract(in_request_json,"$.COST_PER_WHATSAPP"));
    SET IN_COST_PER_EMAIL = json_unquote(json_extract(in_request_json,"$.COST_PER_EMAIL"));
    SET IN_MONTHLY_CREATIVE = json_unquote(json_extract(in_request_json,"$.MONTHLY_CREATIVE"));
    SET IN_MONTHLY_PLATFORM = json_unquote(json_extract(in_request_json,"$.MONTHLY_PLATFORM"));
    SET IN_CURRENCY = json_unquote(json_extract(in_request_json,"$.CURRENCY"));
    SET IN_INCR_METHOD = json_unquote(json_extract(in_request_json,"$.INCREMENTAL_METHOD"));
    SET IN_PRIMARY_CHANNEL = json_unquote(json_extract(in_request_json,"$.PRIMARY_CHANNEL"));
    SET IN_FY_CY = json_unquote(json_extract(in_request_json,"$.TIME_PERIOD"));
    SET IN_DOW = json_unquote(json_extract(in_request_json,"$.DOW"));
    SET IN_FTD = json_unquote(json_extract(in_request_json,"$.R_FTD"));
    SET IN_LTD = json_unquote(json_extract(in_request_json,"$.R_LTD"));
    SET IN_FY_START_DATE = json_unquote(json_extract(in_request_json,"$.FY_START_DATE"));
    SET IN_FY_END_DATE = json_unquote(json_extract(in_request_json,"$.FY_END_DATE"));
    SET IN_CY_START_DATE = json_unquote(json_extract(in_request_json,"$.CY_START_DATE"));
    SET IN_CY_END_DATE = json_unquote(json_extract(in_request_json,"$.CY_END_DATE"));
    SET IN_CAMP_THEME = json_unquote(json_extract(in_request_json,"$.CAMP_THEME"));

	IF IN_AGGREGATION_1 = "DISPLAY"
    THEN
		IF IN_AGGREGATION_2 = "COST_OF_COMMUNICATION"
        THEN
				
			SET @view_stmt = concat('CREATE OR REPLACE VIEW V_UI_Dashboard_Global_Config
							AS  	select Config_Name,Value1 from UI_Configuration_Global
									where Config_Name in ("Cost_per_SMS","Cost_per_PN","Cost_per_EMAIL","Cost_per_Whatsapp","Monthly_Creative_Cost","Monthly_Platform_Cost");');
			SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Config_Name",`Config_Name`,"Value1",`Value1`) )),"]")
			into @temp_result from V_UI_Dashboard_Global_Config;') ;	
		
        END IF;
		IF IN_AGGREGATION_2 = "CURRENCY"
        THEN
				
			SET @view_stmt = concat('CREATE OR REPLACE VIEW V_UI_Dashboard_Global_Config
							AS  	select Config_Name,Value1 from UI_Configuration_Global
									where Config_Name in ("CURRENCY");');
			SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Config_Name",`Config_Name`,"Value1",`Value1`) )),"]")
			into @temp_result from V_UI_Dashboard_Global_Config;') ;
		
        
        END IF;
		IF IN_AGGREGATION_2 = "INCR_METHOD"
        THEN
				
			SET @view_stmt = concat('CREATE OR REPLACE VIEW V_UI_Dashboard_Global_Config
							AS  	select Config_Name,Value1 from UI_Configuration_Global
									where Config_Name in ("Incremental_Revenue_Method");');
			SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Config_Name",`Config_Name`,"Value1",`Value1`) )),"]")
			into @temp_result from V_UI_Dashboard_Global_Config;') ;
		
        
        END IF;
		IF IN_AGGREGATION_2 = "PRIMARY_CHANNEL"
        THEN
				
			SET @view_stmt = concat('CREATE OR REPLACE VIEW V_UI_Dashboard_Global_Config
							AS  	select Config_Name,Value1 from UI_Configuration_Global
									where Config_Name in ("PRIMARY_CHANNEL");');
			SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Config_Name",`Config_Name`,"Value1",`Value1`) )),"]")
			into @temp_result from V_UI_Dashboard_Global_Config;') ;
		
        
        END IF;
		IF IN_AGGREGATION_2 = "FY/CY_YEAR"
        THEN
				
			SET @view_stmt = concat('CREATE OR REPLACE VIEW V_UI_Dashboard_Global_Config
							AS  	select Config_Name,
                            date_format(STR_TO_DATE(Value1,"%Y%c"),"%Y-%m") as Value1, 
                            date_format(STR_TO_DATE(Value2,"%Y%c"),"%Y-%m") as Value2 
                            from UI_Configuration_Global
									where Config_Name IN ("',IN_FY_CY,'");');
			SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Config_Name",`Config_Name`,"Value1",`Value1`,"Value2",`Value2`) )),"]")
			into @temp_result from V_UI_Dashboard_Global_Config;') ;
		
        
        END IF;
		IF IN_AGGREGATION_2 = "DOW"
        THEN
				
			SET @view_stmt = concat('CREATE OR REPLACE VIEW V_UI_Dashboard_Global_Config
							AS  	select Config_Name,Value1 from UI_Configuration_Global
									where Config_Name in ("FIRSTDAYOFWEEK");');
			SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Config_Name",`Config_Name`,"Value1",trim(`Value1`)) )),"]")
			into @temp_result from V_UI_Dashboard_Global_Config;') ;	
		
        
        END IF;
        IF IN_AGGREGATION_2 = "CAMP_THEME" AND IN_MEASURE = "NOT_DROP_DOWN"
        THEN
        
        IF IN_AGGREGATION_3 ="Search"
		THEN
		SET @Condition = CONCAT('`Name` LIKE  "%',IN_AGGREGATION_4,'%"');
		ELSE
		SET @Condition = ' 1=1 ';
		Select @Condition;
		END IF;
				
			SET @view_stmt = concat('CREATE OR REPLACE VIEW V_UI_Dashboard_Global_Config
							AS  	select DISTINCT ID AS "Event_Id",`Name` AS `Trigger_Name`,CampaignThemeName from Event_Master WHERE ',@Condition,';');
			SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Event_Id",`Event_Id`,"Trigger_Name",`Trigger_Name`,"CampaignThemeName",`CampaignThemeName`) )),"]")
			into @temp_result from V_UI_Dashboard_Global_Config;') ;	
		
        
        END IF;
        IF IN_AGGREGATION_2 = "CAMP_THEME" AND IN_MEASURE = "DROP_DOWN"
        THEN
				
			SET @view_stmt = concat('CREATE OR REPLACE VIEW V_UI_Dashboard_Global_Config
							AS  	select distinct CampaignThemeName from Event_Master;');
			SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("CampaignThemeName",`CampaignThemeName`) )),"]")
			into @temp_result from V_UI_Dashboard_Global_Config;') ;	
		
        
        END IF;
	END IF;
	
    
    IF IN_AGGREGATION_1 = "EDIT"
    THEN
		IF IN_AGGREGATION_2 = "COST_OF_COMMUNICATION"
        THEN
        
			SET SQL_SAFE_UPDATES=0;
            
			UPDATE  UI_Configuration_Global
            SET Value1 = IN_COST_PER_PN
            WHERE  Config_Name = "Cost_per_PN";
            
            UPDATE   UI_Configuration_Global
            SET Value1 = IN_COST_PER_SMS
            WHERE  Config_Name = "Cost_per_SMS";
            
            UPDATE   UI_Configuration_Global
            SET Value1 = IN_COST_PER_EMAIL
            WHERE  Config_Name = "Cost_per_EMAIL";
            
            UPDATE   UI_Configuration_Global
            SET Value1 = IN_COST_PER_WHATSAPP
            WHERE  Config_Name = "Cost_per_Whatsapp";
            
            UPDATE   UI_Configuration_Global
            SET Value1 = IN_MONTHLY_CREATIVE
            WHERE  Config_Name = "Monthly_Creative_Cost";
            
            UPDATE   UI_Configuration_Global
            SET Value1 = IN_MONTHLY_PLATFORM
            WHERE  Config_Name = "Monthly_Platform_Cost";
		
        END IF;
		IF IN_AGGREGATION_2 = "CURRENCY"
        THEN
			SET IN_CURRENCY = TRIM(IN_CURRENCY);
            UPDATE UI_Configuration_Global
            SET Value1 = IN_CURRENCY
            WHERE  Config_Name = "CURRENCY";
		
        
        END IF;
		IF IN_AGGREGATION_2 = "INCR_METHOD"
        THEN
		
			UPDATE   UI_Configuration_Global
            SET Value1 = IN_INCR_METHOD
            WHERE  Config_Name = "Incremental_Revenue_Method";
        
        END IF;
		IF IN_AGGREGATION_2 = "PRIMARY_CHANNEL"
        THEN
		
			UPDATE   UI_Configuration_Global
            SET Value1 = IN_PRIMARY_CHANNEL
            WHERE  Config_Name = "PRIMARY_CHANNEL";
        
        END IF;
		IF IN_AGGREGATION_2 = "FY/CY_YEAR" AND IN_FY_CY = "FISCAL"
        THEN
			
            SET IN_FY_START_DATE = replace(IN_FY_START_DATE,"-","");
            SET IN_FY_END_DATE = replace(IN_FY_END_DATE,"-","");
            
            SELECT IN_FY_START_DATE,IN_FY_END_DATE;
            UPDATE UI_Configuration_Global
            SET Value1 = IN_FY_START_DATE,
				Value2 = IN_FY_END_DATE
            WHERE  Config_Name = IN_FY_CY;
            
            SET @view_stmt = concat('CREATE OR REPLACE VIEW V_UI_Dashboard_Global_Config
							AS  	select Config_Name,
                            date_format(STR_TO_DATE(Value1,"%Y%c"),"%Y-%m") as Value1, 
                            date_format(STR_TO_DATE(Value2,"%Y%c"),"%Y-%m") as Value2 
                            from UI_Configuration_Global
									where Config_Name IN ("',IN_FY_CY,'");');
			SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Config_Name",`Config_Name`,"Value1",`Value1`,"Value2",`Value2`) )),"]")
			into @temp_result from V_UI_Dashboard_Global_Config;') ;
        
        END IF;
        
        IF IN_AGGREGATION_2 = "FY/CY_YEAR" AND IN_FY_CY = "CALENDAR"
        THEN
			
            SET IN_CY_START_DATE = replace(IN_CY_START_DATE,"-","");
            SET IN_CY_END_DATE = replace(IN_CY_END_DATE,"-","");
            SELECT IN_CY_START_DATE,IN_CY_END_DATE;
            UPDATE UI_Configuration_Global
            SET Value1 = IN_CY_START_DATE,
				Value2 = IN_CY_END_DATE
            WHERE  Config_Name = IN_FY_CY;
            
            SET @view_stmt = concat('CREATE OR REPLACE VIEW V_UI_Dashboard_Global_Config
							AS  	select Config_Name,
                            date_format(STR_TO_DATE(Value1,"%Y%c"),"%Y-%m") as Value1, 
                            date_format(STR_TO_DATE(Value2,"%Y%c"),"%Y-%m") as Value2 
                            from UI_Configuration_Global
									where Config_Name IN ("',IN_FY_CY,'");');
			SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Config_Name",`Config_Name`,"Value1",`Value1`,"Value2",`Value2`) )),"]")
			into @temp_result from V_UI_Dashboard_Global_Config;') ;
        
        END IF;
        
		IF IN_AGGREGATION_2 = "DOW"
        THEN
        
			SET SQL_SAFE_UPDATES=0;
			
			UPDATE UI_Configuration_Global
            SET Value1 = IN_DOW
            WHERE  Config_Name = "FIRSTDAYOFWEEK";	
        
        END IF;
        IF IN_AGGREGATION_2 = "CAMP_THEME"
        THEN
			
			UPDATE CLM_Campaign_Events A, 
            (
            SELECT CampaignThemeId, CampaignThemeName
            FROM CLM_CampaignTheme
            WHERE CampaignThemeName = IN_CAMP_THEME
            ) AS B
            SET A.CampaignThemeId = B.CampaignThemeId
            WHERE EventId = IN_AGGREGATION_3;	
        
        END IF;
	END IF;
    
    IF IN_SCREEN <> "DOWNLOAD"
    THEN
    select @view_stmt;
	PREPARE statement from @view_stmt;
	Execute statement;
	Deallocate PREPARE statement;
	SELECT @Query_String;
	PREPARE statement from @Query_String;
	Execute statement;
	Deallocate PREPARE statement; 
    SET out_result_json = @temp_result;
    ELSE
		call S_dashboard_performance_download('V_Dashboard_Camp_seg_report_col',@out_result_json1);
		SELECT @out_result_json1 INTO out_result_json;
    END IF;
    
END$$
DELIMITER ;
