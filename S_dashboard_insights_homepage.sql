DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_insights_homepage`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare IN_FILTER_CURRENTMONTH text; 
	declare IN_MEASURE text; 
    declare IN_NUMBER_FORMAT text;
	declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
    declare IN_AGGREGATION_4 text;
    declare IN_AGGREGATION_5 text;
    declare IN_AGGREGATION_6 text;
	
	declare Query_String text;
    declare Query_Measure_Name varchar(100);
    
    
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_AGGREGATION_4 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_4"));
    SET IN_AGGREGATION_5 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_5"));
    SET IN_AGGREGATION_6 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_6"));

	IF IN_AGGREGATION_1 = 'CUSTOMERSBYYEAR'
	THEN 
    
    SELECT DATE_FORMAT(STR_TO_DATE(`Value1`, '%Y%m'), '%Y-%m-01') into @sdate FROM UI_Configuration_Global WHERE Config_Name = 'FISCAL';

SELECT DATE_FORMAT(STR_TO_DATE(`Value2`, '%Y%m'), '%Y-%m-31') into @edate FROM UI_Configuration_Global WHERE Config_Name = 'FISCAL';

select @sdate, @edate;
    
        set @view_stmt= CONCAT(' create or replace view V_Dashboard_homepage_insights as 
        SELECT concat("Fy.",(SUBSTRING(`YYYYMM`,1,4))) as `Year`, Base
FROM
(
select YYYYMM, SUM(T12MthExistingBase) + SUM(T12MthNewBase) AS Base  from Dashboard_Insights_Home_Page WHERE YYYYMM = EXTRACT(YEAR_MONTH FROM DATE_SUB("',@edate,'", INTERVAL 4 YEAR))
UNION
select YYYYMM, SUM(T12MthExistingBase) + SUM(T12MthNewBase) AS Base from Dashboard_Insights_Home_Page WHERE YYYYMM = EXTRACT(YEAR_MONTH FROM DATE_SUB("',@edate,'", INTERVAL 3 YEAR))
UNION
select YYYYMM, SUM(T12MthExistingBase) + SUM(T12MthNewBase) AS Base  from Dashboard_Insights_Home_Page WHERE YYYYMM = EXTRACT(YEAR_MONTH FROM DATE_SUB("',@edate,'", INTERVAL 2 YEAR))
UNION
select YYYYMM, SUM(T12MthExistingBase) + SUM(T12MthNewBase) AS Base from Dashboard_Insights_Home_Page WHERE YYYYMM = EXTRACT(YEAR_MONTH FROM DATE_SUB("',@edate,'", INTERVAL 1 YEAR))
UNION
select YYYYMM, SUM(T12MthExistingBase) + SUM(T12MthNewBase) AS Base from Dashboard_Insights_Home_Page WHERE YYYYMM = EXTRACT(YEAR_MONTH FROM "',@edate,'")
) AS A
WHERE `YYYYMM` IS NOT NULL;
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`Year`,"Value1",`Base`) )),"]")into @temp_result from V_Dashboard_homepage_insights;' ;

END IF;

    IF IN_AGGREGATION_1 = 'SingleMulti'
    THEN
        set @filter_cond = IN_FILTER_CURRENTMONTH;
          
		set @view_stmt = concat('create or replace view V_Dashboard_insights_homepage as
                                 (Select "Single" as `X_Data`, ROUND((SUM(T12MthSingle)/(SUM(T12MthSingle) + SUM(T12MthMulti)) *100),0) as `Value1`,
                                  ROUND((SUM(T12MthSingleRevenue)/(SUM(T12MthSingleRevenue) + SUM(T12MthMultiRevenue)) *100),0) as `Value2`
                                  FROM Dashboard_Insights_Base WHERE YYYYMM = ',@filter_cond,'  AND AGGREGATION = "BY_MONTH")
                                  UNION
                                 (Select "Multi" as `X_Data`,ROUND((SUM(T12MthMulti)/(SUM(T12MthSingle) + SUM(T12MthMulti)) *100),0) as `Value1`,
                                  ROUND((SUM(T12MthMultiRevenue)/(SUM(T12MthSingleRevenue) + SUM(T12MthMultiRevenue)) *100),0) as `Value2`
                                  FROM Dashboard_Insights_Base WHERE YYYYMM = ',@filter_cond,' AND AGGREGATION = "BY_MONTH")');
                                  
	    set @Query_String = 'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`X_Data`,"Value1",`Value1`,"Value2",`Value2`) )),"]")into @temp_result from V_Dashboard_insights_homepage;' ;
    
END IF;

       IF IN_AGGREGATION_1 = 'NewRepeat'
	   THEN
          set @filter_cond = IN_FILTER_CURRENTMONTH;
     
		  set @view_stmt = concat('create or replace view V_Dashboard_insights_homepage as
                                     (select "New" as `X_Data`,
                                     round(((sum(MonthlyNewBase)/(sum(MonthlyNewBase)+sum(MonthlyExistingBase)))*100),0) as `Value1`,
                                     round(((sum(MonthlyNewRevenue)/(sum(MonthlyNewRevenue)+sum(MonthlyExistingRevenue)))*100),0) as `Value2` 
                                     from Dashboard_Insights_Home_Page WHERE YYYYMM = ',@filter_cond,' AND AGGREGATION = "BY_MONTH")
                                     UNION
                                     (select "Repeat" as `X_Data`,
                                     round(((sum(MonthlyExistingBase)/(sum(MonthlyNewBase)+sum(MonthlyExistingBase)))*100),0) as `Value1`,
                                     round(((sum(MonthlyExistingRevenue)/(sum(MonthlyNewRevenue)+sum(MonthlyExistingRevenue)))*100),0) as `Value2`
                                     from Dashboard_Insights_Home_Page WHERE YYYYMM = ',@filter_cond,' AND AGGREGATION = "BY_MONTH")');
                                     
		  set @Query_String = 'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`X_Data`,"Value1",`Value1`,"Value2",`Value2`) )),"]")into @temp_result from V_Dashboard_insights_homepage' ;
     
END IF;
    
	IF IN_AGGREGATION_1 = 'CUSTOMERSBYMONTH'
	THEN 
    
    SELECT DATE_FORMAT(DATE_SUB(REPLACE(STR_TO_DATE(IN_FILTER_CURRENTMONTH, '%Y%m'), '00', '01'), INTERVAL 3 MONTH), '%Y%m') INTO @FY_SATRT_DATE;
		SET @FY_END_DATE=IN_FILTER_CURRENTMONTH;
        
        SET @filter_cond=concat(" YYYYMM between '",@FY_SATRT_DATE,"' and '",@FY_END_DATE,"'");
    
        set @view_stmt= CONCAT(' create or replace view V_Dashboard_homepage_insights as 
							SELECT date_format(STR_TO_DATE(`YYYYMM`, "%Y%m"),"%b-%Y") AS "Month", format(SUM(MonthlyActive),",") AS Customers, concat(ROUND(SUM(MonthlyExistingBase)*100/SUM(MonthlyActive),0),"%") AS `Repeat`, concat(ROUND(SUM(MonthlyExistingRevenue)*100/SUM(MonthlyActiveRevenue),0),"%") AS `Repeat_Rev`, format(ROUND(SUM(MonthlyActiveRevenue)/SUM(MonthlyActiveBills),0),",") AS ATV FROM Dashboard_Insights_Home_Page WHERE ',@filter_cond,' AND AGGREGATION = "BY_MONTH" GROUP BY 1 ORDER BY YYYYMM ASC;
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("",`Month`,"Customers",`Customers`,"Repeat",`Repeat`,"Repeat Rev",`Repeat_Rev`,"ATV",`ATV`) )),"]")into @temp_result from V_Dashboard_homepage_insights;' ;

END IF;

IF IN_AGGREGATION_1 = 'BILLSBYMONTH' AND IN_MEASURE = 'LINEGRAPH'
	THEN 
    
		SELECT DATE_FORMAT(DATE_SUB(REPLACE(STR_TO_DATE(IN_FILTER_CURRENTMONTH, '%Y%m'), '00', '01'), INTERVAL 7 MONTH), '%Y%m') INTO @FY_SATRT_DATE;
		SET @FY_END_DATE=IN_FILTER_CURRENTMONTH;
        
        SET @filter_cond=concat(" YYYYMM between '",@FY_SATRT_DATE,"' and '",@FY_END_DATE,"'");

        SET @view_stmt=concat('CREATE OR REPLACE VIEW V_Dashboard_homepage_insights AS
							SELECT date_format(STR_TO_DATE(`YYYYMM`, "%Y%m"),"%b-%Y") AS "Year", (SUM(MonthlyActiveBills)) AS Bills FROM Dashboard_Insights_Home_Page WHERE ',@filter_cond,' AND AGGREGATION = "BY_MONTH" GROUP BY 1 ORDER BY YYYYMM ASC;
                        ');
                        
	SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`Year`,"Value1",`Bills`) )),"]")into @temp_result from V_Dashboard_homepage_insights;' ;
    
	END IF;
    
    IF IN_AGGREGATION_1 = 'GLUEL12M'
	THEN 
    
        set @view_stmt= CONCAT(' create or replace view V_Dashboard_homepage_insights as 
							select concat(NoOfDays," to ",NoOfDays+1) as X_Data,round(NoOfCustomerPercent,0) as v from Dashboard_Insights_BounceCurve where AGGREGATION = "GLUE" and Measure = "L12M" AND NoOfDays between 1 and 4 order by NoOfDays;
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`X_Data`,"Value",`v`) )),"]")into @temp_result from V_Dashboard_homepage_insights;' ;

END IF;

IF IN_AGGREGATION_1 = 'GLUEDL12M'
	THEN 
    
        set @view_stmt= CONCAT(' create or replace view V_Dashboard_homepage_insights as 
							select NoOfDays as X_Data,round(NoOfCustomerPercent,1) as v from Dashboard_Insights_BounceCurve where AGGREGATION = "GLUEDISTRIBUTION" and Measure = "L12M" and NoOfDays between 1 and 5 order by NoOfDays;
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`X_Data`,"Value",`v`) )),"]")into @temp_result from V_Dashboard_homepage_insights;' ;

END IF;

IF IN_AGGREGATION_1 = 'L12MFREQ'
	THEN 
    
        set @view_stmt= CONCAT(' create or replace view V_Dashboard_homepage_insights as 
							select "New", ROUND(SUM(T12MthNewBills)/SUM(T12MthNewBase),1) AS NEW_Base, "Retained", ROUND(SUM(T12MthExistingBills)/SUM(T12MthExistingBase),1) AS Existing_Base FROM Dashboard_Insights_Base WHERE YYYYMM = "',IN_FILTER_CURRENTMONTH,'";
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("New",`New`,"NEW_Base",`NEW_Base`,"Existing",`Existing`,"Existing_Base",`Existing_Base`) )),"]")into @temp_result from V_Dashboard_homepage_insights;' ;

END IF;

IF IN_AGGREGATION_1 = 'BOUNCECURVE'
	THEN 
    
        set @view_stmt= CONCAT(' create or replace view V_Dashboard_homepage_insights as 
							WITH GROUP1 AS
(
SELECT "10%" AS pct, NoOfDays AS days, AGGREGATION FROM Dashboard_Insights_BounceCurve WHERE AGGREGATION = "NTN1" AND NoOfCustomerPercent >= 10 AND Measure = "ATM" order by NoOfCustomerPercent asc limit 1
),
GROUP2 AS
(
SELECT "20%" AS pct, NoOfDays AS days, AGGREGATION FROM Dashboard_Insights_BounceCurve WHERE AGGREGATION = "NTN1" AND NoOfCustomerPercent >= 20 AND Measure = "ATM" order by NoOfCustomerPercent asc limit 1
),
GROUP3 AS
(
SELECT "30%" AS pct, NoOfDays AS days, AGGREGATION FROM Dashboard_Insights_BounceCurve WHERE AGGREGATION = "NTN1" AND NoOfCustomerPercent >= 30 AND Measure = "ATM" order by NoOfCustomerPercent asc limit 1
),
GROUP4 AS
(
SELECT "40%" AS pct, NoOfDays AS days, AGGREGATION FROM Dashboard_Insights_BounceCurve WHERE AGGREGATION = "NTN1" AND NoOfCustomerPercent >= 40 AND Measure = "ATM" order by NoOfCustomerPercent asc limit 1
),
GROUP5 AS
(
SELECT "50%" AS pct, NoOfDays AS days, AGGREGATION FROM Dashboard_Insights_BounceCurve WHERE AGGREGATION = "NTN1" AND NoOfCustomerPercent >= 50 AND Measure = "ATM" order by NoOfCustomerPercent asc limit 1
),
GROUP6 AS
(
SELECT "60%" AS pct, NoOfDays AS days, AGGREGATION FROM Dashboard_Insights_BounceCurve WHERE AGGREGATION = "NTN1" AND NoOfCustomerPercent >= 60 AND Measure = "ATM" order by NoOfCustomerPercent asc limit 1
),
GROUP7 AS
(
SELECT "70%" AS pct, NoOfDays AS days, AGGREGATION FROM Dashboard_Insights_BounceCurve WHERE AGGREGATION = "NTN1" AND NoOfCustomerPercent >= 70 AND Measure = "ATM" order by NoOfCustomerPercent asc limit 1
),
GROUP8 AS
(
SELECT "80%" AS pct, NoOfDays AS days, AGGREGATION FROM Dashboard_Insights_BounceCurve WHERE AGGREGATION = "NTN1" AND NoOfCustomerPercent >= 80 AND Measure = "ATM" order by NoOfCustomerPercent asc limit 1
),
GROUP9 AS
(
SELECT "90%" AS pct, NoOfDays AS days, AGGREGATION FROM Dashboard_Insights_BounceCurve WHERE AGGREGATION = "NTN1" AND NoOfCustomerPercent >= 90 AND Measure = "ATM" order by NoOfCustomerPercent asc limit 1
)
SELECT A.pct, `Days` FROM GROUP1 A
UNION
SELECT B.pct, `Days` FROM GROUP2 B
UNION
SELECT C.pct, `Days` FROM GROUP3 C
UNION
SELECT D.pct, `Days` FROM GROUP4 D
UNION
SELECT E.pct, `Days` FROM GROUP5 E
UNION
SELECT F.pct, `Days` FROM GROUP6 F
UNION
SELECT G.pct, `Days` FROM GROUP7 G
UNION
SELECT H.pct, `Days` FROM GROUP8 H
UNION
SELECT I.pct, `Days` FROM GROUP9 I;
							');
                            
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("X_Data",`pct`,"Value1",`Days`) )),"]")into @temp_result from V_Dashboard_homepage_insights;' ;

END IF;

IF IN_AGGREGATION_1 = 'BOUNCETABLE1'
	THEN 
    
        set @view_stmt= CONCAT(' create or replace view V_Dashboard_BounceCurve1 as 
							select concat(round(pct,0),"%") as pct, days as "days" from Dashboard_BounceCurve_Summary 
                            where AGGREGATION = "NTN1"');
     
			SET @Query_String =  
		'SELECT CONCAT("[",(GROUP_CONCAT(json_object("%Base",`pct`,"Days",`days`) )),"]")into @temp_result from V_Dashboard_BounceCurve1 ;';

END IF;

IF IN_AGGREGATION_1 = 'BOUNCETABLE2'
	THEN 
    
        set @view_stmt= CONCAT(' create or replace view V_Dashboard_homepage_insights as 
							WITH GROUP1 AS
(
SELECT "30 Days" AS days, concat(ROUND(NoOfCustomerPercent,1),"%") AS "% Repeat", AGGREGATION FROM Dashboard_Insights_BounceCurve WHERE AGGREGATION = "NTR" AND NoOfDays >= 30 AND Measure = "ATM" order by 1 asc limit 1
),
GROUP2 AS
(
SELECT "90 Days" AS days, concat(ROUND(NoOfCustomerPercent,1),"%") AS "% Repeat", AGGREGATION FROM Dashboard_Insights_BounceCurve WHERE AGGREGATION = "NTR" AND NoOfDays >= 90 AND Measure = "ATM" order by 1 asc limit 1
),
GROUP3 AS
(
SELECT "180 Days" AS days, concat(ROUND(NoOfCustomerPercent,1),"%") AS "% Repeat", AGGREGATION FROM Dashboard_Insights_BounceCurve WHERE AGGREGATION = "NTR" AND NoOfDays >= 180 AND Measure = "ATM" order by 1 asc limit 1
)
SELECT A.Days, `% Repeat` FROM GROUP1 A
UNION
SELECT B.DAYS, `% Repeat` FROM GROUP2 B
UNION
SELECT C.DAYS, `% Repeat` FROM GROUP3 C;
							');
     
			SET @Query_String =  'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Days",`Days`,"% Repeat",`% Repeat`) )),"]")into @temp_result from V_Dashboard_homepage_insights;' ;

END IF;

		IF IN_AGGREGATION_1 = 'AHFTABLE'
        
		THEN
			SELECT group_concat(distinct Segmentation_Name) into @distinct_rc from Dashboard_Insights_Segmentation_Schemes WHERE Segmentation_Name <> 'NA' AND Measure = 'CLM Segment';
		    select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
			set @loop_var=1;
			set @json_select='';
            set @top_query='';
			select @loop_var,@num_of_rc,@distinct_rc;
			WHILE @loop_var<=@num_of_rc
			do
			SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
            SET @alias_Name = @selected_rc;
            
			set @top_query=concat(@top_query,",MAX(CASE WHEN CLMSegmentName = '",@selected_rc,"' Then Bills else 0 END) AS `",@alias_Name,"`");
				set @json_select=concat(@json_select,',"',@alias_Name,'",`',@alias_Name,'`');
				set @loop_var=@loop_var+1;
			end while;
			
			
			SELECT @json_select,@top_query;
			set @view_stmt = concat("create or replace  View V_Dashboard_Segment_Table as
			(select '% Base' as 'Overview' 
			",@top_query,"
			from 
			(select A.Segmentation_Name,concat(Base_Percent,'%') as 'Bills' from Dashboard_Insights_Segmentation_Schemes A WHERE Measure = 'CLM Segment'
			)A)
			UNION
			(select '% Rev (L12M)' as 'Overview'
            ",@top_query,"
			from 
			(select A.Segmentation_Name,concat(Amt_Percent,'%') as 'Bills' from Dashboard_Insights_Segmentation_Schemes A WHERE Measure = 'CLM Segment'
			)A)
			UNION
			(select 'Freq (L12M)' as 'Overview'
            ",@top_query,"
			from 
			(select A.Segmentation_Name,Frequency as 'Bills' from Dashboard_Insights_Segmentation_Schemes A WHERE Measure = 'CLM Segment'
			)A)
			UNION
			(select 'ABV (L12M)' as 'Overview'
            ",@top_query,"
			from 
			(select A.Segmentation_Name,format(ABV,',') as 'Bills' from Dashboard_Insights_Segmentation_Schemes A WHERE Measure = 'CLM Segment'
			)A)
			;");
		SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Overview",`Overview`',@json_select,') )),"]")into @temp_result from V_Dashboard_Segment_Table;') ;
        
        END IF;
        
        IF IN_MEASURE <> "DOWNLOAD"
    THEN
	select @view_stmt;
	PREPARE statement from @view_stmt;
	Execute statement;
	Deallocate PREPARE statement;

	select @Query_String;
	PREPARE statement from @Query_String;
	Execute statement;
	Deallocate PREPARE statement; 
    SET out_result_json = @temp_result;
    END IF;
        
END$$
DELIMITER ;
