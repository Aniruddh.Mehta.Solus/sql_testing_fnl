DELIMITER $$
CREATE or replace PROCEDURE `S_CLM_Create_View`()
begin


SELECT group_concat(CONCAT('`',Table_Alias,'`','.','`',`Column_Name`,'`',' as ','`',Column_Name_in_One_View,'`') separator ',') into @var_list FROM Customer_View_Config WHERE In_Use_Customer_One_View in ('Y','Mandatory') and Column_Name_in_One_View not in ('Customer_Id');


select group_concat(br SEPARATOR '') into @brackets from( 
select '(' as br from Customer_View_Config where In_Use_Customer_One_View in ('Y','Mandatory') and Table_Name not in ('CDM_Customer_Master')
group by Table_Name)L;
select group_concat(Join_Tables SEPARATOR ' ') into @table_joins from (
select concat('LEFT JOIN ','`',Table_Name,'`',' ','`',Table_Alias,'`',' ON(`',Table_Alias,'`.`',case when Table_Name='Client_SVOC' then 'Customer_Id_1' when Table_Name='Customer_Recos_View' then 'Customer_Id_2' else 'Customer_Id' end,'`=`CDM_CM`.`Customer_Id`))') as Join_Tables from Customer_View_Config where In_Use_Customer_One_View in ('Y','Mandatory') and Table_Name not in ('CDM_Customer_Master')
group by Table_Name)L; 
select count(1) into @col_check from Customer_View_Config  where In_Use_Customer_One_View in ('Y','Mandatory');
if @col_check>0 then set @var2=',';
else set @var2='';
end if;
set @var_list=ifnull(@var_list,'');
set @brackets=ifnull(@brackets,'');
set @table_joins=ifnull(@table_joins,'');

SET @s1 = Concat('create or replace view Customer_One_View as
                         select 
                         `CDM_CM`.`Customer_Id` AS `Customer_Id`,
                         CURDATE() AS `Today`,
                                 CASE
                         WHEN `CDM_CM`.`Is_New_Cust_Flag` = 1 THEN "Y"
                         ELSE "N"
                         END AS `Is_New_Cust_Flag`
                         ',@var2,'
                         ',@var_list,'
						 FROM ',@brackets,'
						 `CDM_Customer_Master` `CDM_CM`
                         ',@table_joins,'');
       SELECT @s1;                 
		PREPARE stmt from @s1;        
		execute stmt;
		deallocate prepare stmt;
create or replace view V_CLM_Customer_One_View as select * from Customer_One_View;     


SELECT group_concat(case when Column_Name="Enrolled_Store_Id" then '(select
                `CDM_Store_Master`.`Store_Name`
            FROM
                `CDM_Store_Master`
            WHERE
                `CDM_Store_Master`.`Store_Id` =`CDM_CM`.`Enrolled_Store_Id`) as Enrolled_Store'
                else CONCAT('`',Table_Alias,'`','.','`',Column_Name,'`',' as ','`',Column_Name_in_Details_View,'`') end separator ',') into @var_list2 FROM Customer_View_Config WHERE In_Use_Customer_Details_View in ('Y','Mandatory') and Is_LOV_Id='N' and Column_Name<>'Customer_Id';
set @comm1=',';
select concat('',@comm1,'',group_concat(concat('(SELECT 
                `CDM_LOV_Master`.`LOV_Comm_Value`
            FROM
                `CDM_LOV_Master`
            WHERE
                `CDM_LOV_Master`.`LOV_Id` = `',Table_Alias,'`.`',Column_Name,'`)', ' AS ', '`',Column_Name_in_Details_View,'`') separator ',')) into @lov_var_list  FROM Customer_View_Config WHERE In_Use_Customer_Details_View in ('Y','Mandatory') and Is_LOV_Id='Y';

select group_concat(br SEPARATOR '') into @brackets2 from( 
select '(' as br from Customer_View_Config where In_Use_Customer_Details_View in ('Y','Mandatory') and Table_Name not in ('Event_Execution','Communication_Template','CDM_Customer_PII_Master')
group by Table_Name)L;
select group_concat(Join_Tables SEPARATOR ' ') into @table_joins2 from (
select concat('LEFT JOIN ','`',Table_Name,'`',' ','`',Table_Alias,'`',' ON(`',Table_Alias,'`.`',case when Table_Name='Client_SVOC' then 'Customer_Id_1' when Table_Name='Customer_Recos_View' then 'Customer_Id_2' else 'Customer_Id' end,'`=`CDM_CPIIM`.`Customer_Id`))') as Join_Tables from Customer_View_Config where In_Use_Customer_Details_View in ('Y','Mandatory') and Table_Name not in ('Event_Execution','Communication_Template','CDM_Customer_PII_Master')
group by Table_Name)L;




set @var_list2=ifnull(@var_list2,'');
set @brackets2=ifnull(@brackets2,'');
set @lov_var_list=ifnull(@lov_var_list,'');
set @table_joins2=ifnull(@table_joins2,'');


SET @s2 = Concat('create or replace view Customer_Details_View as
                         select 
                         `CDM_CPIIM`.`Customer_Id` AS `Customer_Id`,CAST(F_VALID_CUSTOMER_NAME(`CDM_CPIIM`.`Full_Name`,
                    (SELECT 
                            `M_Config`.`Value`
                        FROM
                            `M_Config`
                        WHERE
                            `M_Config`.`Name` = "Customer_Valid_Name_Size"),
                    (SELECT 
                            `M_Config`.`Value`
                        FROM
                            `M_Config`
                        WHERE
                            `M_Config`.`Name` = "Customer_Valid_Name_Default"),
                    (SELECT 
                            `M_Config`.`Value`
                        FROM
                            `M_Config`
                        WHERE
                            `M_Config`.`Name` = "Customer_Valid_Name_Regexp"))
            AS CHAR CHARSET UTF8) AS `Customer_Name`,
						LTSO1.so2_code as LastBoughtProduct_PDP_so2,
						LTSO2.so2_code as FavProduct_PDP_so2,
			
                         ',@var_list2,'
                         ',@lov_var_list,'
						 FROM ((((',@brackets2,'
						 `CDM_Customer_PII_Master` `CDM_CPIIM`
                         LEFT JOIN `Event_Execution` `EE` ON (`CDM_CPIIM`.`Customer_Id` = `EE`.`Customer_Id`))
						 LEFT JOIN `Communication_Template` `CT` ON (`EE`.`Communication_Template_ID` = `CT`.`ID`))
						 ',@table_joins2,'
						 LEFT JOIN `CLM_so2_Product_Master` `LTSO1` ON(`LTSO1`.`Product_LOV_Id`=`CDM_CTPV`.`LT_Fav_Prod_LOV_Id`))
						 LEFT JOIN `CLM_so2_Product_Master` `LTSO2` ON(`LTSO2`.`Product_LOV_Id`=`CDM_CCPV`.`CP_Fav_Prod_LOV_Id`)) 
                         ');
       SELECT @s2;                 
		PREPARE stmt from @s2;        
		execute stmt;
		deallocate prepare stmt;
end$$
DELIMITER ;
