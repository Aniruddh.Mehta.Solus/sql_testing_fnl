DELIMITER $$
CREATE or replace PROCEDURE `S_Dashboard_Hard_Response`(IN vBatchSize bigint)
BEGIN
DECLARE Load_Exe_ID, Exe_ID,Load_Execution_ID int;
DECLARE vStart_Cnt BIGINT DEFAULT 0;
DECLARE vEnd_Cnt BIGINT;
DECLARE done1,done2 INT DEFAULT FALSE;
DECLARE vSuccess int (4);                      
DECLARE vErrMsg Text ; 
DECLARE Event_ID_cur1 INT;
DECLARE cur1 CURSOR For SELECT DISTINCT
    Event_ID
FROM
    CLM_Event_Attribution_CURRUN;

-- DECLARE CONTINUE HANDLER FOR NOT FOUND SET Loop_Check_Var=1;
DECLARE CONTINUE HANDLER FOR SQLWARNING
BEGIN
	GET DIAGNOSTICS CONDITION 1 @errsqlstate=RETURNED_SQLSTATE, @errno=MYSQL_ERRNO, @errtext = MESSAGE_TEXT;
	SET @logmsg=CONCAT(ifnull(@errsqlstate,'WARNING') , ifnull(@errno,'1'), ifnull(@errtext,'WARNING MESSAGE'));
    INSERT into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
		values ('S_CLM_Event_Attribution_HARD_RESPONSE', @logmsg ,now(),null,@errno, ifnull(Load_Exe_ID,1));
    SELECT 'S_CLM_Event_Attribution_HARD_RESPONSE : Warning Message :' as '', @logmsg as '';
END;
DECLARE EXIT HANDLER FOR SQLEXCEPTION
BEGIN
	GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
	SET @vErrMsg = CONCAT(@errno, " (", @sqlstate, "): ", @text);
	INSERT into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
			values ('S_CLM_Event_Attribution_HARD_RESPONSE ', @vErrMsg ,now(),null,'ERROR', Load_Exe_ID);
	select now() as '', @vErrMsg as '' ;
END;
SET SQL_SAFE_UPDATES=0;
SET FOREIGN_KEY_CHECKS=0;

SET Load_Exe_ID = F_Get_Load_Execution_Id();
SET Exe_ID = F_Get_Execution_Id();
SET Load_Execution_ID = 1;

CREATE OR REPLACE TABLE CLM_Event_Attribution_CURRUN like Event_Attribution_History;

INSERT INTO CLM_Event_Attribution_CURRUN
SELECT * from Event_Attribution_History where Created_Date > (select date_sub(MAX(Created_Date), INTERVAL 2 DAY) from Event_Attribution_History) and Event_Id in (select em.ID from Event_Master em join Communication_Template ct on em.Communication_Template_Id = ct.ID where ct.Number_Of_Recommendation>0 OR ct.Hard_Recommendation <> 'NA');
CREATE or replace TABLE CDM_Product_Master_Hard_Response
(Product_Id BIGINT(20),
Product_Genome VARCHAR(512),
ProductGenome_Attr1_LOV_Id bigint(20) NOT NULL DEFAULT -1,
ProductGenome_Attr2_LOV_Id bigint(20) NOT NULL DEFAULT -1,
ProductGenome_Attr3_LOV_Id bigint(20) NOT NULL DEFAULT -1,
ProductGenome_Attr4_LOV_Id bigint(20) NOT NULL DEFAULT -1,
ProductGenome_Attr5_LOV_Id bigint(20) NOT NULL DEFAULT -1,
ProductGenome_Attr6_LOV_Id bigint(20) NOT NULL DEFAULT -1,
ProductGenome_Attr7_LOV_Id bigint(20) NOT NULL DEFAULT -1,
Primary key(`Product_Id`),
KEY `ProductGenome_Attr1_LOV_Id` (`ProductGenome_Attr1_LOV_Id`),
KEY `ProductGenome_Attr2_LOV_Id` (`ProductGenome_Attr2_LOV_Id`),
KEY `ProductGenome_Attr3_LOV_Id` (`ProductGenome_Attr3_LOV_Id`),
KEY `ProductGenome_Attr4_LOV_Id` (`ProductGenome_Attr4_LOV_Id`),
KEY `ProductGenome_Attr5_LOV_Id` (`ProductGenome_Attr5_LOV_Id`),
KEY `ProductGenome_Attr6_LOV_Id` (`ProductGenome_Attr6_LOV_Id`),
KEY `ProductGenome_Attr7_LOV_Id` (`ProductGenome_Attr7_LOV_Id`)
);

INSERT INTO CDM_Product_Master_Hard_Response (Product_Id)
SELECT DISTINCT Product_Id from CDM_Product_Master; -- CLM_Event_Attribution_CURRUN;

select `Value` into @attr1 from CDM_Product_Genome_Config where `Name` = "ProductGenome_Attr1";
select `Value` into @attr2 from CDM_Product_Genome_Config where `Name` = "ProductGenome_Attr2";
select `Value` into @attr3 from CDM_Product_Genome_Config where `Name` = "ProductGenome_Attr3";
select `Value` into @attr4 from CDM_Product_Genome_Config where `Name` = "ProductGenome_Attr4";
-- select `Value` into @attr5 from CDM_Product_Genome_Config where `Name` = "ProductGenome_Attr5";



SET @sql1= concat('update CDM_Product_Master_Hard_Response a, CDM_Product_Master b
					SET a.Product_Genome=b.Product_Genome,
                    a.ProductGenome_Attr1_LOV_Id=b.',@attr1,',
                    a.ProductGenome_Attr2_LOV_Id=b.',@attr2,',
                    a.ProductGenome_Attr3_LOV_Id=b.',@attr3,',
                    a.ProductGenome_Attr4_LOV_Id=b.',@attr4,'
                    where a.Product_Id=b.Product_Id
					');
select @sql1;
prepare stmt from @sql1;
execute stmt;
deallocate prepare stmt;

CREATE OR REPLACE TABLE `Event_Execution_History_Hard_Response` (
  `Event_Execution_ID` char(50) NOT NULL,
  `Event_ID` int(11) NOT NULL,
  `Event_Execution_Date_ID` int(11) NOT NULL,
  `Customer_Id` bigint(20) NOT NULL,
  `Recommendation_Product_Id_1` bigint(20) DEFAULT -1,
  `Recommendation_Product_Id_2` bigint(20) DEFAULT -1,
  `Recommendation_Product_Id_3` bigint(20) DEFAULT -1,
  `Recommendation_Product_Id_4` bigint(20) DEFAULT -1,
  `Recommendation_Product_Id_5` bigint(20) DEFAULT -1,
  `Recommendation_Product_Id_6` bigint(20) DEFAULT -1,
  PRIMARY KEY (`Customer_Id`,`Event_Execution_ID`),
  KEY `E_E_H_Event_Execution_Date_ID_idx` (`Event_Execution_Date_ID`),
  KEY `Recommendation_Product_Id_1` (`Recommendation_Product_Id_1`),
  KEY `Recommendation_Product_Id_2` (`Recommendation_Product_Id_2`),
  KEY `Recommendation_Product_Id_3` (`Recommendation_Product_Id_3`),
  KEY `Recommendation_Product_Id_4` (`Recommendation_Product_Id_4`),
  KEY `Recommendation_Product_Id_5` (`Recommendation_Product_Id_5`),
  KEY `Recommendation_Product_Id_6` (`Recommendation_Product_Id_6`)
  );
UPDATE CLM_Event_Attribution_CURRUN
SET Matched_ProductId=0,
Matched_ProductGenome_Percent=0,
Matched_ProductGenome=0;

SELECT Customer_Id INTO vStart_Cnt FROM CLM_Event_Attribution_CURRUN ORDER BY Customer_Id ASC LIMIT 1;
SELECT Customer_Id INTO @Rec_Cnt FROM CLM_Event_Attribution_CURRUN ORDER BY Customer_Id DESC LIMIT 1;
SET @Num_of_Attr=(select (char_length(Product_Genome)-char_length(replace(Product_Genome,'___','__')))+1 from CDM_Product_Master where Product_Genome <> '' and Product_Genome is not null LIMIT 1);
SELECT @Num_of_Attr;

open cur1;
new_loop_1: LOOP
	FETCH cur1 into Event_ID_cur1;
	IF done1 THEN
		 LEAVE new_loop_1;
	END IF;
    SELECT "Processing of Event - ",Event_ID_cur1;
    SET vStart_Cnt = 0;    

	PROCESS_LOOP: LOOP
		SET vEnd_Cnt = vStart_Cnt + vBatchSize;

		SELECT vStart_Cnt,vEnd_Cnt,CURRENT_TIMESTAMP;

		TRUNCATE Event_Execution_History_Hard_Response;
		INSERT INTO Event_Execution_History_Hard_Response
		SELECT 
		`Event_Execution_ID`,
		`Event_ID`,
		`Event_Execution_Date_ID`,
		`Customer_Id`,
		`Recommendation_Product_Id_1`,
		`Recommendation_Product_Id_2`,
		`Recommendation_Product_Id_3`,
		`Recommendation_Product_Id_4`,
		`Recommendation_Product_Id_5`,
		`Recommendation_Product_Id_6` from Event_Execution_History
		where Customer_Id between vStart_Cnt and vEnd_Cnt
        and Event_Id=Event_ID_cur1
		and Event_Execution_Id in (select distinct Event_Execution_Id from CLM_Event_Attribution_CURRUN where Event_Id = Event_ID_cur1);
        
        select Hard_Recommendation into @hard_reco from Communication_Template a join Event_Master b on a.ID=b.Communication_Template_Id where b.ID = Event_ID_cur1;
        
        select Number_Of_Recommendation into @num_of_reco from Communication_Template a join Event_Master b on a.ID=b.Communication_Template_Id where b.ID = Event_ID_cur1;
        
        SET @hard_reco=ifnull(@hard_reco,'NA');
        SELECT @hard_reco;
        
        IF @num_of_reco > 0
        THEN       
			update CLM_Event_Attribution_CURRUN eah
							SET eah.Matched_ProductId = 1
							WHERE eah.Customer_Id between vStart_Cnt and vEnd_Cnt
							AND Event_Id = Event_ID_cur1
							AND EXISTS -- bills.Product_Id in 
							(select 1 from Event_Execution_History_Hard_Response eeh where eeh.Event_Execution_Id=eah.Event_Execution_Id AND (eeh.Recommendation_Product_Id_1=eah.Product_Id OR eeh.Recommendation_Product_Id_2=eah.Product_Id OR eeh.Recommendation_Product_Id_3=eah.Product_Id OR eeh.Recommendation_Product_Id_4=eah.Product_Id OR eeh.Recommendation_Product_Id_5=eah.Product_Id OR eeh.Recommendation_Product_Id_6=eah.Product_Id)
							);
							
			IF @Num_of_Attr>=1
			THEN
				UPDATE CLM_Event_Attribution_CURRUN eah, CDM_Product_Master_Hard_Response pm, Event_Execution_History_Hard_Response eeh
				SET eah.Matched_ProductGenome_Percent = Matched_ProductGenome_Percent+(1/@Num_of_Attr),
				eah.Matched_ProductGenome=1
				where eah.Customer_Id between vStart_Cnt and vEnd_Cnt
				AND eah.Event_Id = Event_ID_cur1
				and eah.Product_Id = pm.Product_Id
				and eeh.Event_Execution_Id=eah.Event_Execution_Id
				and (pm.ProductGenome_Attr1_LOV_Id=(select ifnull(pm1.ProductGenome_Attr1_LOV_Id,-1) from CDM_Product_Master_Hard_Response pm1 where eeh.Recommendation_Product_Id_1=pm1.Product_Id)
				OR pm.ProductGenome_Attr1_LOV_Id=(select ifnull(pm1.ProductGenome_Attr1_LOV_Id,-1) from CDM_Product_Master_Hard_Response pm1 where eeh.Recommendation_Product_Id_2=pm1.Product_Id)
				OR pm.ProductGenome_Attr1_LOV_Id=(select ifnull(pm1.ProductGenome_Attr1_LOV_Id,-1) from CDM_Product_Master_Hard_Response pm1 where eeh.Recommendation_Product_Id_3=pm1.Product_Id)
				OR pm.ProductGenome_Attr1_LOV_Id=(select ifnull(pm1.ProductGenome_Attr1_LOV_Id,-1) from CDM_Product_Master_Hard_Response pm1 where eeh.Recommendation_Product_Id_4=pm1.Product_Id)
				OR pm.ProductGenome_Attr1_LOV_Id=(select ifnull(pm1.ProductGenome_Attr1_LOV_Id,-1) from CDM_Product_Master_Hard_Response pm1 where eeh.Recommendation_Product_Id_5=pm1.Product_Id)
				OR pm.ProductGenome_Attr1_LOV_Id=(select ifnull(pm1.ProductGenome_Attr1_LOV_Id,-1) from CDM_Product_Master_Hard_Response pm1 where eeh.Recommendation_Product_Id_6=pm1.Product_Id));
			END IF;

			IF @Num_of_Attr>=2
			THEN
				UPDATE CLM_Event_Attribution_CURRUN eah, CDM_Product_Master_Hard_Response pm, Event_Execution_History_Hard_Response eeh
				SET eah.Matched_ProductGenome_Percent = Matched_ProductGenome_Percent+(1/@Num_of_Attr),
				eah.Matched_ProductGenome=concat(eah.Matched_ProductGenome,1)
				where eah.Customer_Id between vStart_Cnt and vEnd_Cnt
				AND eah.Event_Id = Event_ID_cur1
				and eah.Product_Id = pm.Product_Id
				and eeh.Event_Execution_Id=eah.Event_Execution_Id
				and (pm.ProductGenome_Attr2_LOV_Id=(select ifnull(pm1.ProductGenome_Attr2_LOV_Id,-1) from CDM_Product_Master_Hard_Response pm1 where eeh.Recommendation_Product_Id_1=pm1.Product_Id)
				OR pm.ProductGenome_Attr2_LOV_Id=(select ifnull(pm1.ProductGenome_Attr2_LOV_Id,-1) from CDM_Product_Master_Hard_Response pm1 where eeh.Recommendation_Product_Id_2=pm1.Product_Id)
				OR pm.ProductGenome_Attr2_LOV_Id=(select ifnull(pm1.ProductGenome_Attr2_LOV_Id,-1) from CDM_Product_Master_Hard_Response pm1 where eeh.Recommendation_Product_Id_3=pm1.Product_Id)
				OR pm.ProductGenome_Attr2_LOV_Id=(select ifnull(pm1.ProductGenome_Attr2_LOV_Id,-1) from CDM_Product_Master_Hard_Response pm1 where eeh.Recommendation_Product_Id_4=pm1.Product_Id)
				OR pm.ProductGenome_Attr2_LOV_Id=(select ifnull(pm1.ProductGenome_Attr2_LOV_Id,-1) from CDM_Product_Master_Hard_Response pm1 where eeh.Recommendation_Product_Id_5=pm1.Product_Id)
				OR pm.ProductGenome_Attr2_LOV_Id=(select ifnull(pm1.ProductGenome_Attr2_LOV_Id,-1) from CDM_Product_Master_Hard_Response pm1 where eeh.Recommendation_Product_Id_6=pm1.Product_Id));

				UPDATE CLM_Event_Attribution_CURRUN eah
				SET eah.Matched_ProductGenome=CONCAT(eah.Matched_ProductGenome,0)
				WHERE eah.Customer_Id between vStart_Cnt and vEnd_Cnt
				AND eah.Event_Id = Event_ID_cur1
				AND LENGTH(eah.Matched_ProductGenome)=1;

			END IF;


			IF @Num_of_Attr>=3
			THEN
				UPDATE CLM_Event_Attribution_CURRUN eah, CDM_Product_Master_Hard_Response pm, Event_Execution_History_Hard_Response eeh
				SET eah.Matched_ProductGenome_Percent = Matched_ProductGenome_Percent+(1/@Num_of_Attr),
				eah.Matched_ProductGenome=concat(eah.Matched_ProductGenome,1)
				where eah.Customer_Id between vStart_Cnt and vEnd_Cnt
				AND eah.Event_Id = Event_ID_cur1
				and eah.Product_Id = pm.Product_Id
				and eeh.Event_Execution_Id=eah.Event_Execution_Id
				and (pm.ProductGenome_Attr3_LOV_Id=(select ifnull(pm1.ProductGenome_Attr3_LOV_Id,-1) from CDM_Product_Master_Hard_Response pm1 where eeh.Recommendation_Product_Id_1=pm1.Product_Id)
				OR pm.ProductGenome_Attr3_LOV_Id=(select ifnull(pm1.ProductGenome_Attr3_LOV_Id,-1) from CDM_Product_Master_Hard_Response pm1 where eeh.Recommendation_Product_Id_2=pm1.Product_Id)
				OR pm.ProductGenome_Attr3_LOV_Id=(select ifnull(pm1.ProductGenome_Attr3_LOV_Id,-1) from CDM_Product_Master_Hard_Response pm1 where eeh.Recommendation_Product_Id_3=pm1.Product_Id)
				OR pm.ProductGenome_Attr3_LOV_Id=(select ifnull(pm1.ProductGenome_Attr3_LOV_Id,-1) from CDM_Product_Master_Hard_Response pm1 where eeh.Recommendation_Product_Id_4=pm1.Product_Id)
				OR pm.ProductGenome_Attr3_LOV_Id=(select ifnull(pm1.ProductGenome_Attr3_LOV_Id,-1) from CDM_Product_Master_Hard_Response pm1 where eeh.Recommendation_Product_Id_5=pm1.Product_Id)
				OR pm.ProductGenome_Attr3_LOV_Id=(select ifnull(pm1.ProductGenome_Attr3_LOV_Id,-1) from CDM_Product_Master_Hard_Response pm1 where eeh.Recommendation_Product_Id_6=pm1.Product_Id));
				
				UPDATE CLM_Event_Attribution_CURRUN eah
				SET eah.Matched_ProductGenome=CONCAT(eah.Matched_ProductGenome,0)
				WHERE eah.Customer_Id between vStart_Cnt and vEnd_Cnt
				AND eah.Event_Id = Event_ID_cur1
				AND LENGTH(eah.Matched_ProductGenome)=2;

			END IF;

			IF @Num_of_Attr>=4
			THEN
				UPDATE CLM_Event_Attribution_CURRUN eah, CDM_Product_Master_Hard_Response pm, Event_Execution_History_Hard_Response eeh
				SET eah.Matched_ProductGenome_Percent = Matched_ProductGenome_Percent+(1/@Num_of_Attr),
				eah.Matched_ProductGenome=concat(eah.Matched_ProductGenome,1)
				where eah.Customer_Id between vStart_Cnt and vEnd_Cnt
				AND eah.Event_Id = Event_ID_cur1
				and eah.Product_Id = pm.Product_Id
				and eeh.Event_Execution_Id=eah.Event_Execution_Id
				and (pm.ProductGenome_Attr4_LOV_Id=(select ifnull(pm1.ProductGenome_Attr4_LOV_Id,-1) from CDM_Product_Master_Hard_Response pm1 where eeh.Recommendation_Product_Id_1=pm1.Product_Id)
				OR pm.ProductGenome_Attr4_LOV_Id=(select ifnull(pm1.ProductGenome_Attr4_LOV_Id,-1) from CDM_Product_Master_Hard_Response pm1 where eeh.Recommendation_Product_Id_2=pm1.Product_Id)
				OR pm.ProductGenome_Attr4_LOV_Id=(select ifnull(pm1.ProductGenome_Attr4_LOV_Id,-1) from CDM_Product_Master_Hard_Response pm1 where eeh.Recommendation_Product_Id_3=pm1.Product_Id)
				OR pm.ProductGenome_Attr4_LOV_Id=(select ifnull(pm1.ProductGenome_Attr4_LOV_Id,-1) from CDM_Product_Master_Hard_Response pm1 where eeh.Recommendation_Product_Id_4=pm1.Product_Id)
				OR pm.ProductGenome_Attr4_LOV_Id=(select ifnull(pm1.ProductGenome_Attr4_LOV_Id,-1) from CDM_Product_Master_Hard_Response pm1 where eeh.Recommendation_Product_Id_5=pm1.Product_Id)
				OR pm.ProductGenome_Attr4_LOV_Id=(select ifnull(pm1.ProductGenome_Attr4_LOV_Id,-1) from CDM_Product_Master_Hard_Response pm1 where eeh.Recommendation_Product_Id_6=pm1.Product_Id));
				
				UPDATE CLM_Event_Attribution_CURRUN eah
				SET eah.Matched_ProductGenome=CONCAT(eah.Matched_ProductGenome,0)
				WHERE eah.Customer_Id between vStart_Cnt and vEnd_Cnt
				AND eah.Event_Id = Event_ID_cur1
				AND LENGTH(eah.Matched_ProductGenome)=3;
			END IF;

			IF @Num_of_Attr>=5
			THEN
				UPDATE CLM_Event_Attribution_CURRUN eah, CDM_Product_Master_Hard_Response pm, Event_Execution_History_Hard_Response eeh
				SET eah.Matched_ProductGenome_Percent = Matched_ProductGenome_Percent+(1/@Num_of_Attr),
				eah.Matched_ProductGenome=concat(eah.Matched_ProductGenome,1)
				where eah.Customer_Id between vStart_Cnt and vEnd_Cnt
				AND eah.Event_Id = Event_ID_cur1
				and eah.Product_Id = pm.Product_Id
				and eeh.Event_Execution_Id=eah.Event_Execution_Id
				and (pm.ProductGenome_Attr5_LOV_Id=(select ifnull(pm1.ProductGenome_Attr5_LOV_Id,-1) from CDM_Product_Master_Hard_Response pm1 where eeh.Recommendation_Product_Id_1=pm1.Product_Id)
				OR pm.ProductGenome_Attr5_LOV_Id=(select ifnull(pm1.ProductGenome_Attr5_LOV_Id,-1) from CDM_Product_Master_Hard_Response pm1 where eeh.Recommendation_Product_Id_2=pm1.Product_Id)
				OR pm.ProductGenome_Attr5_LOV_Id=(select ifnull(pm1.ProductGenome_Attr5_LOV_Id,-1) from CDM_Product_Master_Hard_Response pm1 where eeh.Recommendation_Product_Id_3=pm1.Product_Id)
				OR pm.ProductGenome_Attr5_LOV_Id=(select ifnull(pm1.ProductGenome_Attr5_LOV_Id,-1) from CDM_Product_Master_Hard_Response pm1 where eeh.Recommendation_Product_Id_4=pm1.Product_Id)
				OR pm.ProductGenome_Attr5_LOV_Id=(select ifnull(pm1.ProductGenome_Attr5_LOV_Id,-1) from CDM_Product_Master_Hard_Response pm1 where eeh.Recommendation_Product_Id_5=pm1.Product_Id)
				OR pm.ProductGenome_Attr5_LOV_Id=(select ifnull(pm1.ProductGenome_Attr5_LOV_Id,-1) from CDM_Product_Master_Hard_Response pm1 where eeh.Recommendation_Product_Id_6=pm1.Product_Id));
				
				UPDATE CLM_Event_Attribution_CURRUN eah
				SET eah.Matched_ProductGenome=CONCAT(eah.Matched_ProductGenome,0)
				WHERE eah.Customer_Id between vStart_Cnt and vEnd_Cnt
				AND eah.Event_Id = Event_ID_cur1
				AND LENGTH(eah.Matched_ProductGenome)=4;
			END IF;
		END IF;
        IF @hard_reco <> 'NA'
        THEN
			set @n_loop=(select length(@hard_reco)-length(replace(@hard_reco,'|','')))+1;
			set @loop_var=1;
            set @query_cond='';
            while @loop_var<=@n_loop
			do
				SET @iter = substring_Index(substring_index(@hard_reco,"|",@loop_var),"|",-1);
				SET @Hard_resp_column = substring_Index(substring_index(@iter,"::",1),"::",-1);
				SET @Hard_resp_Id = substring_Index(substring_index(@iter,"::",2),"::",-1);
                set @query_cond=concat(@query_cond,' AND pm.',@Hard_resp_column,'_LOV_ID = ',@Hard_resp_Id,'');
			set @loop_var=@loop_var+1;
			END WHILE;	
			IF @Hard_resp_Id='EEH'
            THEN
				UPDATE CLM_Event_Attribution_CURRUN
				SET Hard_Responded = 'Y'
				where Event_Id = Event_ID_cur1
				AND Matched_ProductId=1;
			ELSE
				SET @sql1=concat("UPDATE CLM_Event_Attribution_CURRUN eah, CDM_Product_Master pm
				SET Hard_Responded = 'Y'
				where eah.Event_Id = ",Event_ID_cur1,"
				and eah.Product_Id=pm.Product_Id
				",@query_cond," ");
				SELECT @sql1;
				PREPARE statement from @sql1;
				Execute statement;
				Deallocate PREPARE statement;
			END IF;
            
            UPDATE CLM_Event_Attribution_CURRUN
            SET Hard_Responded = 'N'
            where Hard_Responded = 'NA'
            and Event_Id=Event_ID_cur1;
        END IF;
		SET vStart_Cnt = vEnd_Cnt+1;

		IF vStart_Cnt  >= @Rec_Cnt THEN
			LEAVE PROCESS_LOOP;
		END IF;        
	END LOOP PROCESS_LOOP;  
	end LOOP new_loop_1;
close cur1;

UPDATE CLM_Event_Dashboard a,(select SUM(Sale_Net_Val) as cnt,Event_Id,Event_Execution_Date_ID from CLM_Event_Attribution_CURRUN where In_Control_Group IS NULL and In_Event_Control_Group is null and Hard_Responded = 'Y' group by Event_Id, Event_Execution_Date_ID) b
SET a.Hard_Revenue_Target = b.cnt
where a.Event_Id=b.Event_Id
and a.Event_Execution_Date_ID=b.Event_Execution_Date_ID;

UPDATE CLM_Event_Dashboard a,(select count(DISTINCT Event_Execution_Id) as cnt,Event_Id,Event_Execution_Date_ID from CLM_Event_Attribution_CURRUN where In_Control_Group IS NULL and In_Event_Control_Group is null and Hard_Responded = 'Y' group by Event_Id, Event_Execution_Date_ID) b
SET a.Hard_Responder_Target = b.cnt
where a.Event_Id=b.Event_Id
and a.Event_Execution_Date_ID=b.Event_Execution_Date_ID;

UPDATE Event_Attribution_History a, CLM_Event_Attribution_CURRUN b
SET a.Matched_ProductId=b.Matched_ProductId,
a.Matched_ProductGenome_Percent=b.Matched_ProductGenome_Percent,
a.Matched_ProductGenome=b.Matched_ProductGenome,
a.Hard_Responded=b.Hard_Responded
where a.Event_Execution_ID=b.Event_Execution_ID
and a.Bill_Details_ID=b.Bill_Details_ID;
END$$
DELIMITER ;
