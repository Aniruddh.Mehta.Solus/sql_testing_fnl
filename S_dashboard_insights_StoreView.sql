DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_insights_StoreView`(IN in_request_json json, OUT out_result_json json)
BEGIN

   declare IN_USER_NAME varchar(240); 
   declare IN_SCREEN varchar(400);
   declare IN_MEASURE text; 
   declare IN_FILTER_CURRENTMONTH text; 
   declare IN_FILTER_CURRENTYEAR text;
   declare IN_FILTER_MONTHDURATION text; 
   declare IN_NUMBER_FORMAT text;
   declare IN_AGGREGATION_1 text; 
   declare IN_AGGREGATION_2 text;
   declare IN_AGGREGATION_3 text;
   declare Query_String text;
   declare Query_Measure_Name varchar(100);
  
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
    SET IN_MEASURE = json_unquote(json_extract(in_request_json,"$.MEASURE"));
	SET IN_FILTER_CURRENTMONTH = json_unquote(json_extract(in_request_json,"$.FILTER_CURRENTMONTH"));
	SET IN_FILTER_MONTHDURATION = json_unquote(json_extract(in_request_json,"$.FILTER_MONTHDURATION"));
	SET IN_NUMBER_FORMAT = json_unquote(json_extract(in_request_json,"$.NUMBER_FORMAT"));
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
  
   IF IN_MEASURE = 'Transactions' AND IN_SCREEN = 'Store View'
   THEN
      IF IN_AGGREGATION_1 = 'L12M'
      THEN
		 set @IN_FILTER_CURRENTMONTH = IN_FILTER_CURRENTMONTH;
         IF IN_AGGREGATION_2 = 'All'
         THEN
            set @filter_cond = concat('Store_Name');
		 ELSE
         set @temp_cond = IN_AGGREGATION_2;
         set @filter_cond = concat(' Store_Name order by Amount desc limit ',@temp_cond,'');
         END IF;
         
         set @view_stmt = concat('create or replace view V_Dashboard_StoreView as
								  select Store_Name, sum(T12MthActive) as `Customers`,
                                  sum(T12MthActiveRevenue) as `Amount`,
                                  sum(T12MthActiveBills) as `Bills`,
                                  (sum(T12MthActiveVisits)/sum(T12MthActive)) as `Freq/Cust`,
                                  (sum(T12MthActiveRevenue)/sum(T12MthActive)) as `Amt/Cust`,
                                  sum(T12MthActiveRevenue) / sum(T12MthActiveBills)  as `ABV`
                                  from Dashboard_Insights_Base_By_Store where YYYYMM = ',@IN_FILTER_CURRENTMONTH,'
                                  and AGGREGATION = "BY_MONTH" AND Store_Name <> "" group by ',@filter_cond,' ');
	   ELSE 
		 set @IN_FILTER_CURRENTMONTH = IN_FILTER_CURRENTMONTH;
         IF IN_AGGREGATION_2 = 'All'
         THEN
            set @filter_cond = concat('Store_Name');
		 ELSE
         set @temp_cond = IN_AGGREGATION_2;
         set @filter_cond = concat(' Store_Name order by Amount desc limit ',@temp_cond,'');
         END IF;
         
         set @view_stmt = concat('create or replace view V_Dashboard_StoreView as
                                  select Store_Name, sum(FYBase) as `Customers`,
                                  sum(FYBaseRevenue) as `Amount`,
                                  sum(FYBaseBills) as `Bills`,
								  sum(FYBaseVisits)/sum(FYBase) as `Freq/Cust`,
                                  sum(FYBaseRevenue)/sum(FYBase) as `Amt/Cust`,
								  (sum(FYBaseRevenue)/sum(FYBaseVisits)) as `ABV`
                                  from Dashboard_Insights_Base_By_Store where YYYYMM = ',@IN_FILTER_CURRENTMONTH,'
                                  and AGGREGATION = "BY_MONTH" AND Store_Name <> "" group by ',@filter_cond,' ');
      END IF;
      
      set @Query_String = 'SELECT CONCAT("[",
                                         (GROUP_CONCAT(json_object(
                                         "Store_Name",`Store_Name`,
                                         "Customers",CAST(format(`Customers`,",") AS CHAR),
                                         "Amount",CAST(format(`Amount`,",") AS CHAR) ,
                                         "Bills",CAST(format(`Bills`,",") AS CHAR),
                                         "Freq/Cust",ROUND(`Freq/Cust`,1),
                                         "Amt/Cust",CAST(format(`Amt/Cust`,",") AS CHAR),
                                         "ABV",CAST(format(`ABV`,",") AS CHAR)
                                         ) )),
                                         "]")into @temp_result from V_Dashboard_StoreView' ;
END IF;
   
      IF IN_MEASURE = 'New/Existing' AND IN_SCREEN = 'Store View'
   THEN
      IF IN_AGGREGATION_1 = 'L12M'
      THEN
		 set @IN_FILTER_CURRENTMONTH = IN_FILTER_CURRENTMONTH;
         IF IN_AGGREGATION_2 = 'All'
         THEN
            set @filter_cond = concat('Store_Name');
		 ELSE
         set @temp_cond = IN_AGGREGATION_2;
         set @filter_cond = concat(' Store_Name order by Customers desc limit ',@temp_cond,'');
         END IF;
         
         set @view_stmt = concat('create or replace view V_Dashboard_StoreView as
                                  select Store_Name, sum(T12MthActive) as `Customers`,
                                  sum(T12MthNewBase) as `New Customers`,
                                  sum(T12MthExistingBase) as `Existing Customers`,
                                  concat(round(((sum(T12MthNewBase)/sum(T12MthActive))*100),1),"%") as `New%`,
                                  concat(round(((sum(T12MthExistingBase)/sum(T12MthActive))*100),1),"%") as `Exisitng%`,
                                  concat(round(((sum(T12MthNewRevenue)/sum(T12MthActiveRevenue))*100),1),"%") as `New Rev%`,
                                  concat(round(((sum(T12MthExistingRevenue)/sum(T12MthActiveRevenue))*100),1),"%") as `Exisitng Rev%`
                                  from Dashboard_Insights_Base_By_Store where YYYYMM = ',@IN_FILTER_CURRENTMONTH,'
                                  and AGGREGATION = "BY_MONTH" AND Store_Name <> "" group by ',@filter_cond,' ');
	   ELSE 
		 set @IN_FILTER_CURRENTMONTH = IN_FILTER_CURRENTMONTH;
         IF IN_AGGREGATION_2 = 'All'
         THEN
            set @filter_cond = concat('Store_Name');
		 ELSE
         set @temp_cond = IN_AGGREGATION_2;
         set @filter_cond = concat(' Store_Name order by Customers desc limit ',@temp_cond,'');
         END IF;
         
         set @view_stmt = concat('create or replace view V_Dashboard_StoreView as
                                  select Store_Name , sum(FYBase) as `Customers`,
                                  sum(FYNewBase) as `New Customers`,
                                  sum(FYExistingBase) as `Existing Customers`,
                                  concat(round(((sum(FYNewBase)/sum(FYBase))*100),1),"%") as `New%`,
								  concat(round(((sum(FYExistingBase)/sum(FYBase))*100),1),"%") as `Exisitng%`,
                                  concat(round(((sum(FYNewRevenue)/sum(FYBaseRevenue))*100),1),"%") as `New Rev%`,
                                  concat(round(((sum(FYExistingRevenue)/sum(FYBaseRevenue))*100),1),"%") as `Exisitng Rev%`
                                  from Dashboard_Insights_Base_By_Store where YYYYMM = ',@IN_FILTER_CURRENTMONTH,'
                                  and AGGREGATION = "BY_MONTH" AND Store_Name <> "" group by ',@filter_cond,' ');
      END IF;
      
      set @Query_String = 'SELECT CONCAT("[",
                                         (GROUP_CONCAT(json_object(
                                         "Store_Name",`Store_Name`,
                                         "Customers",CAST(format(`Customers`,",") AS CHAR),
                                         "New Customers",CAST(format(`New Customers`,",") AS CHAR),
                                         "Existing Customers",CAST(format(`Existing Customers`,",") AS CHAR) ,
                                         "New%",`New%`,
                                         "Exisitng%",`Exisitng%`,
                                         "New Rev%",`New Rev%`,
                                         "Exisitng Rev%",`Exisitng Rev%`
                                         ) )),
                                         "]")into @temp_result from V_Dashboard_StoreView' ;
   END IF;
   
   IF IN_MEASURE = 'Campaigns' AND IN_SCREEN = 'Store View'
   THEN
      IF IN_AGGREGATION_1 = 'L12M'
      THEN
        SELECT DATE_FORMAT(DATE_SUB(REPLACE(STR_TO_DATE(IN_FILTER_CURRENTMONTH, '%Y%m'), '00', '01'), INTERVAL 11 MONTH), '%Y%m') INTO @FY_SATRT_DATE;
		SET @FY_END_DATE=IN_FILTER_CURRENTMONTH;
        SET @filter_cond_time=concat(" YM between '",@FY_SATRT_DATE,"' and '",@FY_END_DATE,"'");
        
         IF IN_AGGREGATION_2 = 'All'
         THEN
            set @filter_cond = concat('Store');
		 ELSE
         set @temp_cond = IN_AGGREGATION_2;
         set @filter_cond = concat(' Store order by Customers desc limit ',@temp_cond,'');
         END IF;
      
      set @view_stmt = concat('create or replace view V_Dashboard_StoreView as
                               select Store, MAX(Total_Customer_Base_By_Store) as `Customers`,
                               sum(Target_Base) as `Outreaches`,
                               MAX(Total_Distinct_Customers_Targeted_This_Month) as `Customers Targeted`,
							   concat(round((MAX(Total_Distinct_Customers_Targeted_This_Month)/MAX(Total_Customer_Base_By_Store)*100),1),"%") as `Coverage%`,
                               sum(Target_Responder) as `Responders#`,
							   concat(round((sum(Target_Responder)/sum(Target_Delivered)*100),1),"%") as `Responders%`,
                               sum(Target_Revenue) as `TG Revenue`
                               from CLM_Event_Dashboard_By_Store 
                               where ',@filter_cond_time,'
                               AND Store <> ""
                               group by ',@filter_cond,'  ');
        
        ELSE 
        SELECT Value1 INTO @FY_SATRT_DATE FROM UI_Configuration_Global WHERE Config_Name = 'FISCAL';
        SELECT Value2 INTO @FY_END_DATE FROM UI_Configuration_Global WHERE Config_Name = 'FISCAL';
		-- SET @FY_END_DATE=IN_FILTER_CURRENTMONTH;
        SET @filter_cond_time=concat(" YM between '",@FY_SATRT_DATE,"' and '",@FY_END_DATE,"'");
        
         IF IN_AGGREGATION_2 = 'All'
         THEN
            set @filter_cond = concat('Store');
		 ELSE
         set @temp_cond = IN_AGGREGATION_2;
         set @filter_cond = concat(' Store order by Customers desc limit ',@temp_cond,'');
         END IF;
         
      set @view_stmt = concat('create or replace view V_Dashboard_StoreView as
                               select Store, MAX(Total_Customer_Base_By_Store) as `Customers`,
                               sum(Target_Base) as `Outreaches`,
                               MAX(Total_Distinct_Customers_Targeted_This_Month) as `Customers Targeted`,
							   concat(round((MAX(Total_Distinct_Customers_Targeted_This_Month)/MAX(Total_Customer_Base_By_Store)*100),1),"%") as `Coverage%`,
                               sum(Target_Responder) as `Responders#`,
							   concat(round((sum(Target_Responder)/sum(Target_Delivered)*100),1),"%") as `Responders%`,
                               sum(Target_Revenue) as `TG Revenue`
                               from CLM_Event_Dashboard_By_Store where ',@filter_cond_time,' AND Store <> ""
                               group by  ',@filter_cond,'  ');
         
        END IF;
      
      set @Query_String = 'SELECT CONCAT("[",
                                         (GROUP_CONCAT(json_object(
                                         "Store_Name",`Store`,
                                         "Customers",CAST(format(`Customers`,",") AS CHAR),
                                         "Outreaches",CAST(format(`Outreaches`,",") AS CHAR),
                                         "Customers Targeted",CAST(format(`Customers Targeted`,",") AS CHAR),
                                         "Coverage%",ROUND(`Coverage%`,1),
                                         "Responders#",CAST(format(`Responders#`,",") AS CHAR),
                                         "Responders%",ROUND(`Responders%`,1),
                                         "TG Revenue",CAST(format(`TG Revenue`,",") AS CHAR)
                                         ) )),
                                         "]")into @temp_result from V_Dashboard_StoreView' ;
      
   END IF;
   
      IF IN_MEASURE = 'Segments' AND IN_SCREEN = 'Store View'
   THEN
   			SELECT group_concat(distinct CLMSegmentName order by CLMSegmentName) into @distinct_rc from Dashboard_Insights_Base_By_Store WHERE CLMSegmentName <> "";
		    select (length(@distinct_rc)-length(replace(@distinct_rc,',','')))+1 into @num_of_rc;
			set @loop_var=1;
			set @json_select='';
            set @top_query='';
			select @loop_var,@num_of_rc,@distinct_rc;
			WHILE @loop_var<=@num_of_rc
			do
			SET @selected_rc=substring_index(substring_index(concat(@distinct_rc,',-1'),',',@loop_var),',',-1);
            SET @alias_Name = @selected_rc;
            
			set @top_query=concat(@top_query,",MAX(CASE WHEN CLMSegmentName = '",@selected_rc,"' Then segment else 0 END) AS `",@alias_Name,"`");
				set @json_select=concat(@json_select,',"',@alias_Name,'",`',@alias_Name,'`');
				set @loop_var=@loop_var+1;
			end while;		
            
		 SELECT @json_select,@top_query;
      IF IN_AGGREGATION_1 = 'L12M'
      THEN
		 set @IN_FILTER_CURRENTMONTH = IN_FILTER_CURRENTMONTH;
         IF IN_AGGREGATION_2 = 'All'
         THEN
            set @filter_cond = concat('Store_Name');
		 ELSE
         set @temp_cond = IN_AGGREGATION_2;
         set @filter_cond = concat('Store_Name order by Customer desc limit ',@temp_cond,'');
         END IF;
         
         set @view_stmt = concat('create or replace view V_Dashboard_StoreView_Segment as
								  select Store_Name as `Store_Name`,
								  sum(CustomerSegment) as `Customer`
			                      ',@top_query,'
								  from 
								  (select YYYYMM, Store_Name,CLMSegmentName,CustomerSegment,Customer,
                                   concat(round( ((CustomerSegment)/(Customer)*100),1),"%") as `segment` from
                                   (select YYYYMM, 
                                   Store_Name,CLMSegmentName, 
                                   sum(T12MthActive) over(partition by Store,CLMSegmentName) as `CustomerSegment`,
                                    sum(T12MthActive) over (partition by Store) as `Customer`
									from Dashboard_Insights_Base_By_Store 
                                    where YYYYMM = ',@IN_FILTER_CURRENTMONTH,' and AGGREGATION = "BY_MONTH" AND Store_Name <> "" ) temp_table) A
									group by ',@filter_cond,'');
	   ELSE     
		 set @IN_FILTER_CURRENTMONTH = IN_FILTER_CURRENTMONTH;
         IF IN_AGGREGATION_2 = 'All'
         THEN
            set @filter_cond = concat('Store_Name');
		 ELSE
         set @temp_cond = IN_AGGREGATION_2;
         set @filter_cond = concat('Store_Name order by Customer desc limit ',@temp_cond,'');
         END IF;
         
         set @view_stmt = concat('create or replace view V_Dashboard_StoreView_Segment as
								  select Store_Name as `Store_Name`,
								  Customer as `Customer`
			                      ',@top_query,'
								  from 
                                  (select YYYYMM, Store_Name,CLMSegmentName,CustomerSegment,Customer,
								  concat(round( ((CustomerSegment)/(Customer)*100),1),"%") as `segment` from
								  (select YYYYMM, 
								   Store_Name,CLMSegmentName, 
                                   sum(FYBase) over(partition by Store,CLMSegmentName) as `CustomerSegment`,
								   sum(FYBase) over (partition by Store) as `Customer`
								   from Dashboard_Insights_Base_By_Store 
								   where YYYYMM = ',@IN_FILTER_CURRENTMONTH,' and AGGREGATION = "BY_MONTH" AND Store_Name <> "") temp_table) A
                                   group by ',@filter_cond,' ');
      END IF;
      
      set @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object(
                                                   "Store_Name",`Store_Name`,
                                                   "Customers",CAST(format(`Customer`,",") AS CHAR)
                                                   ',@json_select,'
                                                   ) )),"]")into @temp_result from V_Dashboard_StoreView_Segment;') ;
   END IF;
   
   IF IN_MEASURE = 'DEFAULT'
    THEN
          
	SET @Query_String =  'SELECT CONCAT("[", JSON_OBJECT("Store_Name", `Store_Name`), "]") into @temp_result
from Dashboard_Insights_Base_By_Store order by RAND() limit 1;
;' ;
    END IF;
  
   IF IN_SCREEN = 'Location View' AND IN_MEASURE <> 'CARDS'
  THEN
      
      set @view_stmt = concat('create or replace view V_Dashboard_Store_Details_Filter as
                               select distinct Store_Name from Dashboard_Insights_Base_By_Store;');
                               
	  set @Query_String = 'SELECT CONCAT("[",(GROUP_CONCAT(json_object("store",`Store_Name`) )),"]")into @temp_result from V_Dashboard_Store_Details_Filter';
      
  END IF;
  
  IF IN_SCREEN = 'Location View' AND IN_MEASURE = 'CARDS'
  THEN
     SET @storeName = IN_AGGREGATION_1;
     SET @IN_FILTER_CURRENTMONTH = IN_FILTER_CURRENTMONTH;
     IF IN_FILTER_MONTHDURATION = 'L12M'
     THEN
        select format(round(avg(T12MthActive),1),",") into @AvgCustomer from Dashboard_Insights_Base_By_Store where YYYYMM = @IN_FILTER_CURRENTMONTH and AGGREGATION = "BY_MONTH";
        select format(round(avg(T12MthActiveRevenue),1),",") into @AvgAmount from Dashboard_Insights_Base_By_Store where YYYYMM = @IN_FILTER_CURRENTMONTH and AGGREGATION = "BY_MONTH";
        select format(round(avg(T12MthActiveBills),1),",") into @AvgBills from Dashboard_Insights_Base_By_Store where YYYYMM = @IN_FILTER_CURRENTMONTH and AGGREGATION = "BY_MONTH";
        select round((avg(T12MthActiveVisits)/avg(T12MthActive)),1) into @AvgFreqByCust from Dashboard_Insights_Base_By_Store where YYYYMM = @IN_FILTER_CURRENTMONTH and AGGREGATION = "BY_MONTH";
        select round((avg(T12MthActiveRevenue)/avg(T12MthActive)),1) into @AvgAmtByCust from Dashboard_Insights_Base_By_Store where YYYYMM = @IN_FILTER_CURRENTMONTH and AGGREGATION = "BY_MONTH";
        select round((avg(T12MthActiveRevenue)/avg(T12MthActiveBills)),1) into @AvgABV from Dashboard_Insights_Base_By_Store where YYYYMM = @IN_FILTER_CURRENTMONTH and AGGREGATION = "BY_MONTH";
        select format(round(avg(T12MthNewBase),1),",") into @AvgNewCustomer from Dashboard_Insights_Base_By_Store where YYYYMM = @IN_FILTER_CURRENTMONTH and AGGREGATION = "BY_MONTH";
        select format(round(avg(T12MthExistingBase),1),",") into @AvgExistingCustomer from Dashboard_Insights_Base_By_Store where YYYYMM = @IN_FILTER_CURRENTMONTH and AGGREGATION = "BY_MONTH";
        select concat(round(((avg(T12MthNewBase)/avg(T12MthActive))*100),1),"%") into @AvgNewCustomerPercent from Dashboard_Insights_Base_By_Store where YYYYMM = @IN_FILTER_CURRENTMONTH and AGGREGATION = "BY_MONTH";
        select concat(round(((avg(T12MthExistingBase)/avg(T12MthActive))*100),1),"%") into @AvgExistingCustomerPercent from Dashboard_Insights_Base_By_Store where YYYYMM = @IN_FILTER_CURRENTMONTH and AGGREGATION = "BY_MONTH";
        select concat(round(((avg(T12MthNewRevenue)/avg(T12MthActiveRevenue))*100),1),"%") into @AvgNewRevPercent from Dashboard_Insights_Base_By_Store where YYYYMM = @IN_FILTER_CURRENTMONTH and AGGREGATION = "BY_MONTH";
        select concat(round(((avg(T12MthExistingRevenue)/avg(T12MthActiveRevenue))*100),1),"%") into @AvgExistingRevPercent from Dashboard_Insights_Base_By_Store where YYYYMM = @IN_FILTER_CURRENTMONTH and AGGREGATION = "BY_MONTH";
        select format(round(avg(T12MthExistingBase),1),",") into @AvgExistingCustomer from Dashboard_Insights_Base_By_Store where YYYYMM = @IN_FILTER_CURRENTMONTH and AGGREGATION = "BY_MONTH";
        select format(round(ifnull(avg(Target_Base),0),1),",") into @AvgOutreaches from CLM_Event_Dashboard_By_Store where YM = @IN_FILTER_CURRENTMONTH;
        select round(ifnull(avg(Total_Distinct_Customers_Targeted_This_Month),0),1) into @AvgCustTarget from CLM_Event_Dashboard_By_Store where YM = @IN_FILTER_CURRENTMONTH;
        select concat(round((ifnull(avg(Total_Distinct_Customers_Targeted_This_Month)/avg(Total_Customer_Base_By_Store),0)*100),1),"%") into @AvgCoverage from CLM_Event_Dashboard_By_Store where YM = @IN_FILTER_CURRENTMONTH;
        select round(ifnull(avg(Target_Bills),0),1) into @AvgConversion from CLM_Event_Dashboard_By_Store where YM = @IN_FILTER_CURRENTMONTH;
        select concat(round(ifnull((avg(Target_Bills)/avg(Target_Base)*100),0),1),"%") into @AvgConversionPercent from CLM_Event_Dashboard_By_Store where YM = @IN_FILTER_CURRENTMONTH;
        select round(ifnull(avg(Target_Revenue),0),1) into @AvgTGRevenue from CLM_Event_Dashboard_By_Store where YM = @IN_FILTER_CURRENTMONTH;
        select @AvgOutreaches,@AvgCustTarget,@AvgCoverage,@AvgConversion,@AvgConversionPercent,@AvgTGRevenue;
        
        
        set @view_stmt = concat('create or replace view V_Dashboard_Store_Details as
                                 (select "Customers" as `Card_Name`, format(sum(T12MthActive),",") as `Value1`, "Average:',@AvgCustomer,'" as `Value2` from Dashboard_Insights_Base_By_Store where YYYYMM = "',@IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and Store_Name = "',@storeName,'" )
								UNION
								(select "Amount" as `Card_Name`, format(sum(T12MthActiveRevenue),",") as `Value1`, "Average:',@AvgAmount,'" as `Value2` from Dashboard_Insights_Base_By_Store where YYYYMM = "',@IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and Store_Name = "',@storeName,'" )
								UNION
								(select "Bills" as `Card_Name`, format(sum(T12MthActiveBills),",") as `Value1`, "Average:',@AvgBills,'" as `Value2` from Dashboard_Insights_Base_By_Store where YYYYMM = "',@IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and Store_Name = "',@storeName,'" )
								UNION
								(select "Freq/Cust" as `Card_Name`, round((sum(T12MthActiveVisits)/sum(T12MthActive)),1) as `Value1`, "Average:',@AvgFreqByCust,'" as `Value2` from Dashboard_Insights_Base_By_Store where YYYYMM = "',@IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and Store_Name = "',@storeName,'" )
								UNION
								(select "Amt/Cust" as `Card_Name`, round((sum(T12MthActiveRevenue)/sum(T12MthActive)),0) as `Value1`, "Average:',@AvgAmtByCust,'" as `Value2` from Dashboard_Insights_Base_By_Store where YYYYMM = "',@IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and Store_Name = "',@storeName,'" )
								UNION
								(select "ABV" as `Card_Name`, round((sum(T12MthActiveRevenue) / sum(T12MthActiveBills)),0)  as `Value1`, "Average:',@AvgABV,'"  as `Value2` from Dashboard_Insights_Base_By_Store where YYYYMM = "',@IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and Store_Name = "',@storeName,'")
                                UNION
                                (select "New Cust" as `Card_Name`, format(sum(T12MthNewBase),",") as `Value1`, "Average:',@AvgNewCustomer,'"  as `Value2` from Dashboard_Insights_Base_By_Store where YYYYMM = "',@IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and Store_Name = "',@storeName,'")
                                UNION
                                (select "Existing Cust" as `Card_Name`, format(sum(T12MthExistingBase),",") as `Value1`, "Average:',@AvgExistingCustomer,'"  as `Value2` from Dashboard_Insights_Base_By_Store where YYYYMM = "',@IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and Store_Name = "',@storeName,'")
                                UNION
                                (select "% New" as `Card_Name`, concat(round(((sum(T12MthNewBase)/sum(T12MthActive))*100),1),"%") as `Value1`, "Average:',@AvgNewCustomerPercent,'"  as `Value2` from Dashboard_Insights_Base_By_Store where YYYYMM = "',@IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and Store_Name = "',@storeName,'")
                                UNION
                                (select "% Existing" as `Card_Name`, concat(round(((sum(T12MthExistingBase)/sum(T12MthActive))*100),1),"%") as `Value1`, "Average:',@AvgExistingCustomerPercent,'"  as `Value2` from Dashboard_Insights_Base_By_Store where YYYYMM = "',@IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and Store_Name = "',@storeName,'")
                                UNION
                                (select "% New Rev" as `Card_Name`, concat(round(((sum(T12MthNewRevenue)/sum(T12MthActiveRevenue))*100),1),"%") as `Value1`, "Average:',@AvgNewRevPercent,'"  as `Value2` from Dashboard_Insights_Base_By_Store where YYYYMM = "',@IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and Store_Name = "',@storeName,'")
                                UNION
                                (select "% Existing Rev" as `Card_Name`, concat(round(((sum(T12MthExistingRevenue)/sum(T12MthActiveRevenue))*100),1),"%") as `Value1`, "Average:',@AvgExistingRevPercent,'"  as `Value2` from Dashboard_Insights_Base_By_Store where YYYYMM = "',@IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and Store_Name = "',@storeName,'")
                                UNION
                                (select "Outreaches" as `Card_Name`, format(ifnull(sum(Target_Base),0),",") as `Value1`, "Average:',@AvgOutreaches,'" as `Value2` from CLM_Event_Dashboard_By_Store where YM = "',@IN_FILTER_CURRENTMONTH,'" and Store = "',@storeName,'")
                                UNION
                                (select "Cust. Targerted" as `Card_Name`, format(ifnull(max(Total_Distinct_Customers_Targeted_This_Month),0),",") as `Value1`, "Average:',@AvgCustTarget,'" as `Value2` from CLM_Event_Dashboard_By_Store where YM = "',@IN_FILTER_CURRENTMONTH,'" and Store = "',@storeName,'")
                                UNION
                                (select "Coverage" as `Card_Name`, concat(round(ifnull((max(Total_Distinct_Customers_Targeted_This_Month)/max(Total_Customer_Base_By_Store)*100),0),1),"%") as `Value1`, "Average:',@AvgCoverage,'" as `Value2` from CLM_Event_Dashboard_By_Store where YM = "',@IN_FILTER_CURRENTMONTH,'" and Store = "',@storeName,'")
                                UNION
                                (select "Conversion" as `Card_Name`, format(ifnull(sum(Target_Bills),0),",") as `Value1`, "Average:',@AvgConversion,'" as `Value2` from CLM_Event_Dashboard_By_Store where YM = "',@IN_FILTER_CURRENTMONTH,'" and Store = "',@storeName,'")
                                UNION
                                (select "Conversion %" as `Card_Name`, concat(round(ifnull((sum(Target_Bills)/sum(Target_Base)*100),0),1),"%") as `Value1`, "Average:',@AvgConversionPercent,'" as `Value2` from CLM_Event_Dashboard_By_Store where YM = "',@IN_FILTER_CURRENTMONTH,'" and Store = "',@storeName,'")
                                UNION
                                (select "TG Revenue" as `Card_Name`, format(ifnull(sum(Target_Revenue),0),",") as `Value1`, "Average:',@AvgTGRevenue,'" as `Value2` from CLM_Event_Dashboard_By_Store where YM = "',@IN_FILTER_CURRENTMONTH,'" and Store = "',@storeName,'")'
                                );
                                
		set @Query_String = 'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Card_Name",`Card_Name`,"Value1",`Value1`,"Value2",`Value2`) )),"]")into @temp_result from V_Dashboard_Store_Details';
     
     ELSE
        select format(round(avg(FYBase),1),",") into @AvgCustomer from Dashboard_Insights_Base_By_Store where YYYYMM = @IN_FILTER_CURRENTMONTH and AGGREGATION = "BY_MONTH";
        select format(round(avg(FYBaseRevenue),1),",") into @AvgAmount from Dashboard_Insights_Base_By_Store where YYYYMM = @IN_FILTER_CURRENTMONTH and AGGREGATION = "BY_MONTH";
        select format(round(avg(FYBaseBills),1),",") into @AvgBills from Dashboard_Insights_Base_By_Store where YYYYMM = @IN_FILTER_CURRENTMONTH and AGGREGATION = "BY_MONTH";
        select round((avg(FYBaseVisits)/avg(FYBase)),1) into @AvgFreqByCust from Dashboard_Insights_Base_By_Store where YYYYMM = @IN_FILTER_CURRENTMONTH and AGGREGATION = "BY_MONTH";
        select round((avg(FYBaseRevenue)/avg(FYBase)),1) into @AvgAmtByCust from Dashboard_Insights_Base_By_Store where YYYYMM = @IN_FILTER_CURRENTMONTH and AGGREGATION = "BY_MONTH";
        select round((avg(FYBaseRevenue)/avg(FYBaseVisits)),1) into @AvgABV from Dashboard_Insights_Base_By_Store where YYYYMM = @IN_FILTER_CURRENTMONTH and AGGREGATION = "BY_MONTH";
        select format(round(avg(FYNewBase),1),",") into @AvgNewCustomer from Dashboard_Insights_Base_By_Store where YYYYMM = @IN_FILTER_CURRENTMONTH and AGGREGATION = "BY_MONTH";
        select format(round(avg(FYExistingBase),1),",") into @AvgExistingCustomer from Dashboard_Insights_Base_By_Store where YYYYMM = @IN_FILTER_CURRENTMONTH and AGGREGATION = "BY_MONTH";
        select concat(round(((avg(FYNewBase)/avg(FYBase))*100),1),"%") into @AvgNewCustomerPercent from Dashboard_Insights_Base_By_Store where YYYYMM = @IN_FILTER_CURRENTMONTH and AGGREGATION = "BY_MONTH";
        select concat(round(((avg(FYExistingBase)/avg(FYBase))*100),1),"%") into @AvgExistingCustomerPercent from Dashboard_Insights_Base_By_Store where YYYYMM = @IN_FILTER_CURRENTMONTH and AGGREGATION = "BY_MONTH";
        select concat(round(((avg(FYNewRevenue)/avg(FYBaseRevenue))*100),1),"%") into @AvgNewRevPercent from Dashboard_Insights_Base_By_Store where YYYYMM = @IN_FILTER_CURRENTMONTH and AGGREGATION = "BY_MONTH";
        select concat(round(((avg(FYExistingRevenue)/avg(FYBaseRevenue))*100),1),"%") into @AvgExistingRevPercent from Dashboard_Insights_Base_By_Store where YYYYMM = @IN_FILTER_CURRENTMONTH and AGGREGATION = "BY_MONTH";
        select format(round(ifnull(avg(Target_Base),0),1),",") into @AvgOutreaches from CLM_Event_Dashboard_By_Store where YM = @IN_FILTER_CURRENTMONTH;
        select round(ifnull(avg(Total_Distinct_Customers_Targeted_This_Month),0),1) into @AvgCustTarget from CLM_Event_Dashboard_By_Store where YM = @IN_FILTER_CURRENTMONTH;
        select concat(round((ifnull(avg(Total_Distinct_Customers_Targeted_This_Month)/avg(Total_Customer_Base_By_Store),0)*100),1),"%") into @AvgCoverage from CLM_Event_Dashboard_By_Store where YM = @IN_FILTER_CURRENTMONTH;
        select round(ifnull(avg(Target_Bills),0),1) into @AvgConversion from CLM_Event_Dashboard_By_Store where YM = @IN_FILTER_CURRENTMONTH;
        select concat(round(ifnull((avg(Target_Bills)/avg(Target_Base)*100),0),1),"%") into @AvgConversionPercent from CLM_Event_Dashboard_By_Store where YM = @IN_FILTER_CURRENTMONTH;
        select round(ifnull(avg(Target_Revenue),0),1) into @AvgTGRevenue from CLM_Event_Dashboard_By_Store where YM = @IN_FILTER_CURRENTMONTH;
     
        set @view_stmt = concat('create or replace view V_Dashboard_Store_Details as
                                 (select "Customers" as `Card_Name`, format(sum(FYBase),",") as `Value1`, "Average:',@AvgCustomer,'" as `Value2` from Dashboard_Insights_Base_By_Store where YYYYMM = "',@IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and Store_Name = "',@storeName,'" )
								UNION
								(select "Amount" as `Card_Name`, format(sum(FYBaseRevenue),",") as `Value1`, "Average:',@AvgAmount,'" as `Value2` from Dashboard_Insights_Base_By_Store where YYYYMM = "',@IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and Store_Name = "',@storeName,'" )
								UNION
								(select "Bills" as `Card_Name`, format(sum(FYBaseBills),",") as `Value1`, "Average:',@AvgBills,'" as `Value2` from Dashboard_Insights_Base_By_Store where YYYYMM = "',@IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and Store_Name = "',@storeName,'" )
								UNION
								(select "Freq/Cust" as `Card_Name`, round((sum(FYBaseVisits)/sum(FYBase)),1) as `Value1`, "Average:',@AvgFreqByCust,'" as `Value2` from Dashboard_Insights_Base_By_Store where YYYYMM = "',@IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and Store_Name = "',@storeName,'" )
								UNION
								(select "Amt/Cust" as `Card_Name`, round((sum(FYBaseRevenue)/sum(FYBase)),1) as `Value1`, "Average:',@AvgAmtByCust,'" as `Value2` from Dashboard_Insights_Base_By_Store where YYYYMM = "',@IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and Store_Name = "',@storeName,'" )
								UNION
								(select "ABV" as `Card_Name`, round((sum(FYBaseRevenue) / sum(FYBaseVisits)),1)  as `Value1`, "Average:',@AvgABV,'"  as `Value2` from Dashboard_Insights_Base_By_Store where YYYYMM = "',@IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and Store_Name = "',@storeName,'")
                                UNION
                                (select "New Cust" as `Card_Name`, format(sum(FYNewBase),",") as `Value1`, "Average:',@AvgNewCustomer,'"  as `Value2` from Dashboard_Insights_Base_By_Store where YYYYMM = "',@IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and Store_Name = "',@storeName,'")
                                UNION
                                (select "Existing Cust" as `Card_Name`, format(sum(FYExistingBase),",") as `Value1`, "Average:',@AvgExistingCustomer,'"  as `Value2` from Dashboard_Insights_Base_By_Store where YYYYMM = "',@IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and Store_Name = "',@storeName,'")
                                UNION
                                (select "% New" as `Card_Name`, concat(round(((sum(FYNewBase)/sum(FYBase))*100),1),"%") as `Value1`, "Average:',@AvgNewCustomerPercent,'"  as `Value2` from Dashboard_Insights_Base_By_Store where YYYYMM = "',@IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and Store_Name = "',@storeName,'")
                                UNION
                                (select "% Existing" as `Card_Name`, concat(round(((sum(FYExistingBase)/sum(FYBase))*100),1),"%") as `Value1`, "Average:',@AvgExistingCustomerPercent,'"  as `Value2` from Dashboard_Insights_Base_By_Store where YYYYMM = "',@IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and Store_Name = "',@storeName,'")
                                UNION
                                (select "% New Rev" as `Card_Name`, concat(round(((sum(FYNewRevenue)/sum(FYBaseRevenue))*100),1),"%") as `Value1`, "Average:',@AvgNewRevPercent,'"  as `Value2` from Dashboard_Insights_Base_By_Store where YYYYMM = "',@IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and Store_Name = "',@storeName,'")
                                UNION
                                (select "% Existing Rev" as `Card_Name`, concat(round(((sum(FYExistingRevenue)/sum(FYBaseRevenue))*100),1),"%") as `Value1`, "Average:',@AvgExistingRevPercent,'"  as `Value2` from Dashboard_Insights_Base_By_Store where YYYYMM = "',@IN_FILTER_CURRENTMONTH,'" and AGGREGATION = "BY_MONTH" and Store_Name = "',@storeName,'")
                                UNION
                                (select "Outreaches" as `Card_Name`, format(ifnull(sum(Target_Base),0),",") as `Value1`, "Average:',@AvgOutreaches,'" as `Value2` from CLM_Event_Dashboard_By_Store where YM = "',@IN_FILTER_CURRENTMONTH,'" and Store = "',@storeName,'")
                                UNION
                                (select "Cust. Targerted" as `Card_Name`, ifnull(max(Total_Distinct_Customers_Targeted_This_Month),0) as `Value1`, "Average:',@AvgCustTarget,'" as `Value2` from CLM_Event_Dashboard_By_Store where YM = "',@IN_FILTER_CURRENTMONTH,'" and Store = "',@storeName,' ")
                                UNION
                                (select "Coverage" as `Card_Name`, concat(round(ifnull((max(Total_Distinct_Customers_Targeted_This_Month)/max(Total_Customer_Base)*100),0),1),"%") as `Value1`, "Average:',@AvgCoverage,'" as `Value2` from CLM_Event_Dashboard_By_Store where YM = "',@IN_FILTER_CURRENTMONTH,'" and Store = "',@storeName,'")
                                UNION
                                (select "Conversion" as `Card_Name`, ifnull(sum(Target_Bills),0) as `Value1`, "Average:',@AvgConversion,'" as `Value2` from CLM_Event_Dashboard_By_Store where YM = "',@IN_FILTER_CURRENTMONTH,'" and Store = "',@storeName,'")
                                UNION
                                (select "Conversion %" as `Card_Name`, concat(round(ifnull((sum(Target_Bills)/sum(Target_Base)*100),0),1),"%") as `Value1`, "Average:',@AvgConversionPercent,'" as `Value2` from CLM_Event_Dashboard_By_Store where YM = "',@IN_FILTER_CURRENTMONTH,'" and Store = "',@storeName,'")
                                UNION
                                (select "TG Revenue" as `Card_Name`, ifnull(sum(Target_Revenue),0) as `Value1`, "Average:',@AvgTGRevenue,'" as `Value2` from CLM_Event_Dashboard_By_Store where YM = "',@IN_FILTER_CURRENTMONTH,'" and Store = "',@storeName,'")'
                                );
                                
         set @Query_String = 'SELECT CONCAT("[",(GROUP_CONCAT(json_object("Card_Name",`Card_Name`,"Value1",`Value1`,"Value2",`Value2`) )),"]")into @temp_result from V_Dashboard_Store_Details';                       
     END IF;
  END IF;
  
  IF IN_SCREEN = 'Location View' AND IN_AGGREGATION_3 = 'Search'
  THEN
  	SET @Condition = CONCAT('Store_Name LIKE  "%',IN_AGGREGATION_1,'%"');
	
	Select @Condition;
    
    set @view_stmt = concat('create or replace view V_Dashboard_Store_Details_Search as
                               select distinct Store_Name from Dashboard_Insights_Base_By_Store
                               where ',@Condition,' ;');
                               
	set @Query_String = 'SELECT CONCAT("[",(GROUP_CONCAT(json_object("store",`Store_Name`) )),"]")into @temp_result from V_Dashboard_Store_Details_Search';
  
  END IF;
  
   IF IN_SCREEN <> "DOWNLOAD"
    THEN
        select @view_stmt;
	    PREPARE statement from @view_stmt;
	    Execute statement;
	    Deallocate PREPARE statement;
    
		select @Query_String;
		PREPARE statement from @Query_String;
		Execute statement;
		Deallocate PREPARE statement; 
		SET out_result_json = @temp_result;
    END IF;
END$$
DELIMITER ;
