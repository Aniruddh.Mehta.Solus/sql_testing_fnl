DELIMITER $$
CREATE or replace PROCEDURE s_dashboard_filldate(IN datestart date, IN  dateend date)
begin
			DROP TABLE if Exists  tablenew;
            
			CREATE TABLE  tablenew
            (ID INT AUTO_INCREMENT ,
            EE_date DATE, 
            PRIMARY KEY(ID));
			
			while datestart <= dateend do
			
			insert into tablenew (EE_date) values (datestart);
			set datestart = date_ADD(datestart, interval 1 day );
			end while;
			end$$
DELIMITER ;
