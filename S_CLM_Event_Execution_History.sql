
CREATE OR REPLACE PROCEDURE `S_CLM_Event_Execution_History`(IN OnDemandEventId BIGINT(20),IN Run_Mode Varchar(10),OUT no_of_customers BIGINT(20))
BEGIN
	DECLARE Load_Exe_ID, Exe_ID int;
    
DECLARE vEnd_Cnt BIGINT;
DECLARE vStart_Cnt BIGINT;
DECLARE vBatchSize BIGINT;
DECLARE vRec_Cnt BIGINT;


    
    set foreign_key_checks=0;
    
    set Load_Exe_ID = F_Get_Load_Execution_Id();
    
	insert into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
    values ('S_CLM_Event_Execution_History', '',now(),null,'Started', Load_Exe_ID);
    
    set Exe_ID = F_Get_Execution_Id();

	    SET vStart_Cnt=0;
SET vBatchSize=50000;
SET vRec_Cnt= 0;
SET vEnd_Cnt=0;

SELECT 
    Customer_Id
INTO vRec_Cnt FROM
    CDM_Customer_Master
ORDER BY Customer_Id DESC
LIMIT 1;
        
        PROCESS_LOOP: LOOP
SET vEnd_Cnt = vStart_Cnt + vBatchSize;
insert into log_solus(module, rec_start, rec_end) values ('S_CLM_Event_Execution_History',vStart_Cnt,vEnd_Cnt);

If  (Run_Mode='TEST' or Run_Mode='RUN')
THEN


Delete From Event_Execution_History where Customer_ID  between vStart_Cnt and vEnd_Cnt and 
              Event_Execution_ID in (Select Event_Execution_ID from Event_Execution)
             AND Event_ID =OnDemandEventId;

			 


	
             
             INSERT ignore INTO Event_Execution_History (Event_ID, Customer_Id, Communication_Template_ID, Event_Execution_Date_ID, In_Control_Group, Event_Execution_ID,Communication_Channel,Offer_Code,In_Event_Control_Group,Is_MAB_Event,Campaign_Key,Recommendation1,Recommendation2,Recommendation3,Recommendation4,Recommendation5,Recommendation_Genome_Lov_Id_1,Recommendation_Genome_Lov_Id_2,Recommendation_Genome_Lov_Id_3,Recommendation_Genome_Lov_Id_4,Recommendation_Genome_Lov_Id_5,Recommendation_URL_1,Recommendation_URL_2,Recommendation_URL_3,Recommendation_URL_4,Recommendation_URL_5) 
		select Event_ID, Customer_Id, Communication_Template_ID, Event_Execution_Date_ID, In_Control_Group, Event_Execution_ID,ct.Communication_Channel,ee.Offer_Code,In_Event_Control_Group,Is_MAB_Event,Campaign_Key,Recommendation1,Recommendation2,Recommendation3,Recommendation4,Recommendation5,Recommendation_Genome_Lov_Id_1,Recommendation_Genome_Lov_Id_2,Recommendation_Genome_Lov_Id_3,Recommendation_Genome_Lov_Id_4,Recommendation_Genome_Lov_Id_5,Recommendation_URL_1,Recommendation_URL_2,Recommendation_URL_3,Recommendation_URL_4,Recommendation_URL_5 from Event_Execution ee join Communication_Template ct on ee.Communication_Template_ID=ct.ID
        where Customer_Id between vStart_Cnt and vEnd_Cnt and  
        ee.Is_Target = 'Y' AND Event_ID =OnDemandEventId ;
		
	
        
        

ELSE
		INSERT ignore INTO Event_Execution_History (Event_ID, Customer_Id, Communication_Template_ID, Event_Execution_Date_ID, In_Control_Group, Event_Execution_ID,Communication_Channel,Offer_Code,In_Event_Control_Group,Is_MAB_Event,Campaign_Key,Recommendation1,Recommendation2,Recommendation3,Recommendation4,Recommendation5,Recommendation_Genome_Lov_Id_1,Recommendation_Genome_Lov_Id_2,Recommendation_Genome_Lov_Id_3,Recommendation_Genome_Lov_Id_4,Recommendation_Genome_Lov_Id_5,Recommendation_URL_1,Recommendation_URL_2,Recommendation_URL_3,Recommendation_URL_4,Recommendation_URL_5) 
		select Event_ID, Customer_Id, Communication_Template_ID, Event_Execution_Date_ID, In_Control_Group, Event_Execution_ID,ct.Communication_Channel,ee.Offer_Code,In_Event_Control_Group,Is_MAB_Event,Campaign_Key,Recommendation1,Recommendation2,Recommendation3,Recommendation4,Recommendation5,Recommendation_Genome_Lov_Id_1,Recommendation_Genome_Lov_Id_2,Recommendation_Genome_Lov_Id_3,Recommendation_Genome_Lov_Id_4,Recommendation_Genome_Lov_Id_5,Recommendation_URL_1,Recommendation_URL_2,Recommendation_URL_3,Recommendation_URL_4,Recommendation_URL_5 from Event_Execution ee join Communication_Template ct on ee.Communication_Template_ID=ct.ID
        where Customer_Id between vStart_Cnt and vEnd_Cnt and 
        ee.Is_Target = 'Y' AND Event_ID <>OnDemandEventId ;
		
		
END IF;	
IF vEnd_Cnt  >= vRec_Cnt THEN
		LEAVE PROCESS_LOOP;
END IF;
SET vStart_Cnt = vStart_Cnt + vBatchSize;
END LOOP PROCESS_LOOP;


If  (Run_Mode='TEST' or Run_Mode='RUN')
THEN

SELECT 
    COUNT(DISTINCT Customer_Id)
INTO no_of_customers FROM
    Event_Execution_History
WHERE
   Event_Execution_Date_ID >= DATE_FORMAT(CURDATE() - INTERVAL 0 DAY, '%Y%m%d')
        AND In_Control_Group IS NULL
        AND In_Event_Control_Group IS NULL
		 AND Event_ID =OnDemandEventId;
else 
		SELECT 
    COUNT(DISTINCT Customer_Id)
INTO no_of_customers FROM
    Event_Execution_History
WHERE
   Event_Execution_Date_ID >= DATE_FORMAT(CURDATE() - INTERVAL 0 DAY, '%Y%m%d')
        AND In_Control_Group IS NULL
        AND In_Event_Control_Group IS NULL
		 AND Event_ID <>OnDemandEventId;
END IF;	

UPDATE ETL_Execution_Details 
SET 
    Status = 'Succeeded',
    End_Time = NOW()
WHERE
    Procedure_Name = 'S_CLM_Event_Execution_History'
        AND Load_Execution_ID = Load_Exe_ID
        AND Execution_ID = Exe_ID;
	



END

