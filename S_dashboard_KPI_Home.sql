DELIMITER $$
CREATE OR REPLACE PROCEDURE `S_dashboard_KPI_Home`(IN in_request_json JSON, OUT out_result_json JSON)
BEGIN
 
    DECLARE IN_SCREEN VARCHAR(400);  
    DECLARE IN_MEASURE TEXT; 
    DECLARE IN_AGGREGATION_1 TEXT;  
    DECLARE vCurMM		int(4);
	DECLARE vPrevYYYY		int(4);
    DECLARE vCurYYYY		int(4);
    DECLARE IYR_Repeat_Value		decimal(10,1);
    DECLARE NTR_Repeat_Value		decimal(10,1);
    DECLARE RETENTION_Retained_Value		decimal(10,1);
    DECLARE WINBACK_Wonback_Value		decimal(10,1);
    DECLARE YTD_Visits_Value		decimal(10,1);

    
    
    

    DECLARE NTR_YTD DECIMAL(10,1);
      DECLARE YTD_Goal DECIMAL(10,1);
    DECLARE Year_Goal DECIMAL(10,1);
    DECLARE YTD_Bar DECIMAL(10,0);
    DECLARE NTR_YTD_Goal DECIMAL(10,1);
    DECLARE LY_YTD_Goal DECIMAL(10,1);
    DECLARE YTD_Goal_Bar DECIMAL(10,0);
    DECLARE LY_Ach DECIMAL(10,1);
    DECLARE LY_Ach_Bar DECIMAL(10,0);
    DECLARE Year_Goal_Bar DECIMAL(10,0);
    DECLARE LY_YTD_Goal_Bar DECIMAL(10,0);
    DECLARE Repeat_New DECIMAL(10,0);
     DECLARE Not_Repeat_New DECIMAL(10,0);
        DECLARE Total_New DECIMAL(10,0);
        DECLARE Color varchar(10);
set Color='Green';
    -- SET IN_MEASURE = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.MEASURE"));
    
    SET IN_AGGREGATION_1 = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.AGGREGATION_1"));
  
    SET IN_SCREEN = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.SCREEN"));
    
    select date_format(now(),'%m') 		into vCurMM;
    select date_format(now(),'%Y') 		into vCurYYYY;
	select date_format(now(),'%Y')-1 	into vPrevYYYY;
    
    if IN_AGGREGATION_1="Year_Goal" Then
    begin
     SET IYR_Repeat_Value = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.IYR_Repeat"));
      SET NTR_Repeat_Value = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.NTR_Repeat"));
       SET RETENTION_Retained_Value = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.RETENTION_Retained"));
        SET WINBACK_Wonback_Value = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.WINBACK_Wonback"));
         SET YTD_Visits_Value = JSON_UNQUOTE(JSON_EXTRACT(in_request_json, "$.YTD_Visits"));
  
  UPDATE `PLUM_SOLUS_REPORTING`.Dashboard_Insights_KPI
SET IYR_Repeat = IYR_Repeat_Value, NTR_Repeat = NTR_Repeat_Value, RETENTION_Retained = RETENTION_Retained_Value
,WINBACK_Wonback = WINBACK_Wonback_Value,YTD_Visits = YTD_Visits_Value
WHERE YYYY = 1;
create or replace view Dashboard_Insights_KPI as select * from PLUM_SOLUS_REPORTING.Dashboard_Insights_KPI;

    end;
    end if;
   
    
    
     if IN_AGGREGATION_1="Fetch_Year_Goal" Then
     begin
      SELECT CONCAT('[', GROUP_CONCAT(
    JSON_OBJECT('IYR_Repeat', IYR_Repeat, 'NTR_Repeat', NTR_Repeat, 'RETENTION_Retained', RETENTION_Retained,
    'WINBACK_Wonback', WINBACK_Wonback, 'YTD_Visits', YTD_Visits)
), ']') INTO @temp_result
FROM Dashboard_Insights_KPI
WHERE YYYY = 1;
     end;
     end if;
   
    
    if IN_AGGREGATION_1="NTR" THEN
    begin
    select NTR_Repeat into Repeat_New from Dashboard_Insights_KPI where YYYY=vCurYYYY;
	select NTR_NonRepeat into Not_Repeat_New from Dashboard_Insights_KPI where YYYY=vCurYYYY;
	SELECT SUM(Repeat_New + Not_Repeat_New) into Total_New
	FROM Dashboard_Insights_KPI
	WHERE YYYY = vCurYYYY;
      select round((Repeat_New/Total_New)*100,1) into NTR_YTD FROM Dashboard_Insights_KPI WHERE YYYY = vCurYYYY;
    select NTR_Repeat into Year_Goal from Dashboard_Insights_KPI where YYYY=1;
    select round((Repeat_New/Total_New)*100,1) into YTD_Goal FROM Dashboard_Insights_KPI WHERE YYYY = vCurYYYY;
   
    set NTR_YTD_Goal=round((vCurMM/12)*Year_Goal,1);
 
    select NTR_Repeat into @LY_Repeat_New from Dashboard_Insights_KPI where YYYY=vPrevYYYY;
    select NTR_NonRepeat into @LY_Not_Repeat_New from Dashboard_Insights_KPI where YYYY=vPrevYYYY;
    SELECT SUM(@LY_Repeat_New + @LY_Not_Repeat_New) into @Total_Ach FROM Dashboard_Insights_KPI WHERE YYYY = vPrevYYYY;
    select round((@LY_Repeat_New/@Total_Ach)*100,1) into LY_Ach FROM Dashboard_Insights_KPI WHERE YYYY = vPrevYYYY;
    set LY_YTD_Goal=round((vCurMM/12)*LY_Ach,1);
    set YTD_Bar=round((NTR_YTD/Year_Goal)*100,0);
    set YTD_Goal_Bar=round((NTR_YTD_Goal/Year_Goal)*100,0);
    set LY_YTD_Goal_Bar=round((LY_YTD_Goal/LY_Ach)*100,0);
    set LY_Ach_Bar=100;
    set Year_Goal_Bar=100;
    
     

IF NTR_YTD >= NTR_YTD_Goal THEN
    SET Color = 'Green';
ELSEIF NTR_YTD >= (NTR_YTD_Goal * 0.9) AND NTR_YTD < NTR_YTD_Goal THEN
    SET Color = 'Blue';
ELSEIF NTR_YTD < (NTR_YTD_Goal * 0.9) THEN
    SET Color = 'Red';
END IF;

    end;
    end if;

    
    
    
     if IN_AGGREGATION_1="IYR" THEN
    begin
    select IYR_Repeat into Repeat_New from Dashboard_Insights_KPI where YYYY=vCurYYYY;
	select IYR_NonRepeat into Not_Repeat_New from Dashboard_Insights_KPI where YYYY=vCurYYYY;
	SELECT SUM(Repeat_New + Not_Repeat_New) into Total_New
	FROM Dashboard_Insights_KPI
	WHERE YYYY = vCurYYYY;
      select round((Repeat_New/Total_New)*100,1) into NTR_YTD FROM Dashboard_Insights_KPI WHERE YYYY = vCurYYYY;
    select IYR_Repeat into Year_Goal from Dashboard_Insights_KPI where YYYY=1;
    select round((Repeat_New/Total_New)*100,1) into YTD_Goal FROM Dashboard_Insights_KPI WHERE YYYY = vCurYYYY;
   
      set NTR_YTD_Goal=round((vCurMM/12)*Year_Goal,1);
 
    select IYR_Repeat into @LY_Repeat_New from Dashboard_Insights_KPI where YYYY=vPrevYYYY;
    select IYR_NonRepeat into @LY_Not_Repeat_New from Dashboard_Insights_KPI where YYYY=vPrevYYYY;
    SELECT SUM(@LY_Repeat_New + @LY_Not_Repeat_New) into @Total_Ach FROM Dashboard_Insights_KPI WHERE YYYY = vPrevYYYY;
    select round((@LY_Repeat_New/@Total_Ach)*100,1) into LY_Ach FROM Dashboard_Insights_KPI WHERE YYYY = vPrevYYYY;
    set LY_YTD_Goal=round((vCurMM/12)*LY_Ach,1);
    set YTD_Bar=round((NTR_YTD/Year_Goal)*100,0);
    set YTD_Goal_Bar=round((NTR_YTD_Goal/Year_Goal)*100,0);
    set LY_YTD_Goal_Bar=round((LY_YTD_Goal/LY_Ach)*100,0);
    set LY_Ach_Bar=100;
    set Year_Goal_Bar=100;
    
    IF NTR_YTD >= NTR_YTD_Goal THEN
    SET Color = 'Green';
ELSEIF NTR_YTD >= (NTR_YTD_Goal * 0.9) AND NTR_YTD < NTR_YTD_Goal THEN
    SET Color = 'Blue';
ELSEIF NTR_YTD < (NTR_YTD_Goal * 0.9) THEN
    SET Color = 'Red';
END IF;

    end;
    end if;
    
      if IN_AGGREGATION_1="RETENTION" THEN
    begin
    select RETENTION_Retained into Repeat_New from Dashboard_Insights_KPI where YYYY=vCurYYYY;
	select RETENTION_NonRetained into Not_Repeat_New from Dashboard_Insights_KPI where YYYY=vCurYYYY;
	SELECT SUM(Repeat_New + Not_Repeat_New) into Total_New
	FROM Dashboard_Insights_KPI
	WHERE YYYY = vCurYYYY;
      select round((Repeat_New/Total_New)*100,1) into NTR_YTD FROM Dashboard_Insights_KPI WHERE YYYY = vCurYYYY;
    select RETENTION_Retained into Year_Goal from Dashboard_Insights_KPI where YYYY=1;
    select round((Repeat_New/Total_New)*100,1) into YTD_Goal FROM Dashboard_Insights_KPI WHERE YYYY = vCurYYYY;
   
     set NTR_YTD_Goal=round((vCurMM/12)*Year_Goal,1);
 
    select RETENTION_Retained into @LY_Repeat_New from Dashboard_Insights_KPI where YYYY=vPrevYYYY;
    select RETENTION_NonRetained into @LY_Not_Repeat_New from Dashboard_Insights_KPI where YYYY=vPrevYYYY;
    SELECT SUM(@LY_Repeat_New + @LY_Not_Repeat_New) into @Total_Ach FROM Dashboard_Insights_KPI WHERE YYYY = vPrevYYYY;
    select round((@LY_Repeat_New/@Total_Ach)*100,1) into LY_Ach FROM Dashboard_Insights_KPI WHERE YYYY = vPrevYYYY;
    set LY_YTD_Goal=round((vCurMM/12)*LY_Ach,1);
    set YTD_Bar=round((NTR_YTD/Year_Goal)*100,0);
    set YTD_Goal_Bar=round((NTR_YTD_Goal/Year_Goal)*100,0);
    set LY_YTD_Goal_Bar=round((LY_YTD_Goal/LY_Ach)*100,0);
    set LY_Ach_Bar=100;
    set Year_Goal_Bar=100;
    
    IF NTR_YTD >= NTR_YTD_Goal THEN
    SET Color = 'Green';
ELSEIF NTR_YTD >= (NTR_YTD_Goal * 0.9) AND NTR_YTD < NTR_YTD_Goal THEN
    SET Color = 'Blue';
ELSEIF NTR_YTD < (NTR_YTD_Goal * 0.9) THEN
    SET Color = 'Red';
END IF;

    end;
    end if;
    
    
     if IN_AGGREGATION_1="WINBACK" THEN
    begin
    select WINBACK_Wonback into Repeat_New from Dashboard_Insights_KPI where YYYY=vCurYYYY;
	select WINBACK_NotWonback into Not_Repeat_New from Dashboard_Insights_KPI where YYYY=vCurYYYY;
	SELECT SUM(Repeat_New + Not_Repeat_New) into Total_New
	FROM Dashboard_Insights_KPI
	WHERE YYYY = vCurYYYY;
      select round((Repeat_New/Total_New)*100,1) into NTR_YTD FROM Dashboard_Insights_KPI WHERE YYYY = vCurYYYY;
    select WINBACK_Wonback into Year_Goal from Dashboard_Insights_KPI where YYYY=1;
    select round((Repeat_New/Total_New)*100,1) into YTD_Goal FROM Dashboard_Insights_KPI WHERE YYYY = vCurYYYY;
   
       set NTR_YTD_Goal=round((vCurMM/12)*Year_Goal,1);
 
    select WINBACK_Wonback into @LY_Repeat_New from Dashboard_Insights_KPI where YYYY=vPrevYYYY;
    select WINBACK_NotWonback into @LY_Not_Repeat_New from Dashboard_Insights_KPI where YYYY=vPrevYYYY;
    SELECT SUM(@LY_Repeat_New + @LY_Not_Repeat_New) into @Total_Ach FROM Dashboard_Insights_KPI WHERE YYYY = vPrevYYYY;
    select round((@LY_Repeat_New/@Total_Ach)*100,1) into LY_Ach FROM Dashboard_Insights_KPI WHERE YYYY = vPrevYYYY;
    set LY_YTD_Goal=round((vCurMM/12)*LY_Ach,1);
    set YTD_Bar=round((NTR_YTD/Year_Goal)*100,0);
    set YTD_Goal_Bar=round((NTR_YTD_Goal/Year_Goal)*100,0);
    set LY_YTD_Goal_Bar=round((LY_YTD_Goal/LY_Ach)*100,0);
    set LY_Ach_Bar=100;
    set Year_Goal_Bar=100;
    
    IF NTR_YTD >= NTR_YTD_Goal THEN
    SET Color = 'Green';
ELSEIF NTR_YTD >= (NTR_YTD_Goal * 0.9) AND NTR_YTD < NTR_YTD_Goal THEN
    SET Color = 'Blue';
ELSEIF NTR_YTD < (NTR_YTD_Goal * 0.9) THEN
    SET Color = 'Red';
END IF;

    end;
    end if;
    
     if IN_AGGREGATION_1="VISITS" THEN
    begin
    select YTD_Visits into Repeat_New from Dashboard_Insights_KPI where YYYY=vCurYYYY;
	select YTD_Bills into Not_Repeat_New from Dashboard_Insights_KPI where YYYY=vCurYYYY;
	SELECT YTD_Customers into Total_New
	FROM Dashboard_Insights_KPI
	WHERE YYYY = vCurYYYY;
      select round((Repeat_New/Total_New),1) into NTR_YTD FROM Dashboard_Insights_KPI WHERE YYYY = vCurYYYY;
    select YTD_Visits into Year_Goal from Dashboard_Insights_KPI where YYYY=1;
    select round((Repeat_New/Total_New),1) into YTD_Goal FROM Dashboard_Insights_KPI WHERE YYYY = vCurYYYY;
   
      set NTR_YTD_Goal=round((vCurMM/12)*Year_Goal,1);
 
    select YTD_Visits into @LY_Repeat_New from Dashboard_Insights_KPI where YYYY=vPrevYYYY;
    select YTD_Bills into @LY_Not_Repeat_New from Dashboard_Insights_KPI where YYYY=vPrevYYYY;
    SELECT YTD_Customers into @Total_Ach FROM Dashboard_Insights_KPI WHERE YYYY = vPrevYYYY;
    select round((@LY_Repeat_New/@Total_Ach),1) into LY_Ach FROM Dashboard_Insights_KPI WHERE YYYY = vPrevYYYY;
    set LY_YTD_Goal=round((vCurMM/12)*LY_Ach,1);
    set YTD_Bar=round((NTR_YTD/Year_Goal)*100,1);
    set YTD_Goal_Bar=round((NTR_YTD_Goal/Year_Goal)*100,1);
    set LY_YTD_Goal_Bar=round((LY_YTD_Goal/LY_Ach)*100,1);
    set LY_Ach_Bar=100;
    set Year_Goal_Bar=100;
    
    IF NTR_YTD >= NTR_YTD_Goal THEN
    SET Color = 'Green';
ELSEIF NTR_YTD >= (NTR_YTD_Goal * 0.9) AND NTR_YTD < NTR_YTD_Goal THEN
    SET Color = 'Blue';
ELSEIF NTR_YTD < (NTR_YTD_Goal * 0.9) THEN
    SET Color = 'Red';
END IF;

    end;
    end if;
    
    if IN_AGGREGATION_1 ="Year_Goal" then
    begin
    set out_result_json=JSON_ARRAY(
    JSON_OBJECT('Message',"Data Updated"));
    end;
    elseif IN_AGGREGATION_1 ="Fetch_Year_Goal" then
    begin
    set out_result_json=@temp_result;
    end;
    else
    SET out_result_json = JSON_ARRAY(
    JSON_OBJECT(
		'Color',Color,
        'Repeat_New', format(Repeat_New,0),
        'Not_Repeat_New', format(Not_Repeat_New,0),
        'Total', format(Total_New,0),
        'YTD', NTR_YTD,
        'Year_Goal', Year_Goal,
        'YTD_Bar', YTD_Bar,
        'YTD_Goal', NTR_YTD_Goal,
        'LY_YTD_Goal', LY_YTD_Goal,
        'YTD_Goal_Bar', YTD_Goal_Bar,
        'LY_Ach', LY_Ach,
        'LY_Ach_Bar', LY_Ach_Bar,
        'Year_Goal_Bar', Year_Goal_Bar,
        'LY_YTD_Goal_Bar', LY_YTD_Goal_Bar
    )
);

    
    end if;
     
    

   

    

   --  SET out_result_json = @temp_result;
END$$
DELIMITER ;
