
CREATE OR REPLACE PROCEDURE `S_CLM_Update_Segment_Event_Execution`()
begin
	DECLARE vStart_Cnt BIGINT DEFAULT 0;
    DECLARE vEnd_Cnt BIGINT DEFAULT 0;
	DECLARE vBatchSize BIGINT DEFAULT 100000;
    DECLARE done1,done2 INT DEFAULT FALSE;
    DECLARE CLMSegmentId_cur1,Exe_ID int;
    DECLARE CLMSegmentReplacedQuery_cur1 TEXT;
    Declare cur1 cursor for
    select distinct CLMSegmentId,CLMSegmentReplacedQuery from CLM_Segment
    where CLMSegmentReplacedQuery <> '';
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done1 = TRUE;
    SET done1 = FALSE;
    SET SQL_SAFE_UPDATES=0;
    set foreign_key_checks=0;
    INSERT into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
	values ('S_CLM_Update_Segment_Event_Execution', '' ,now(),null,'Started', 090921);
    SET Exe_ID = F_Get_Execution_Id();
	select * from ETL_Execution_Details Order by Execution_Id DESC LIMIT 1;
	SELECT Customer_Id INTO @Rec_Cnt FROM CDM_Customer_Master ORDER BY Customer_Id DESC LIMIT 1;
    open cur1;
	new_loop_1: LOOP
	FETCH cur1 into CLMSegmentId_cur1,CLMSegmentReplacedQuery_cur1;
	IF done1 THEN
			 LEAVE new_loop_1;
	END IF;
	SET vStart_Cnt = 0;

	PROCESS_LOOP: LOOP
		SET vEnd_Cnt = vStart_Cnt + vBatchSize;
        set @sqlCust = concat( 'update Event_Execution_Current_Run
        set Segment_Id = ',CLMSegmentId_cur1,' where Customer_Id between ',vStart_Cnt,' and ',vEnd_Cnt,' and Customer_Id in (select Customer_Id from Customer_One_View where ',CLMSegmentReplacedQuery_cur1,' and Customer_Id between ',vStart_Cnt,' and ',vEnd_Cnt,' ) and Segment_Id = -1');
		prepare stmt1 from @sqlCust;
		execute stmt1;
		deallocate prepare stmt1;
        
		Update Event_Execution_Current_Run a,CDM_Revenue_Center b
        set a.Revenue_Center=b.Revenue_Center
        where a.Customer_Id = b.Customer_Id
        and a.Customer_Id between vStart_Cnt and vEnd_Cnt
        and a.Revenue_Center = -1
        and b.Customer_Id between vStart_Cnt and vEnd_Cnt;
		
        update Event_Execution_History eeh, Event_Execution_Current_Run ee
        set eeh.Revenue_Center=ee.Revenue_Center,
        eeh.Segment_Id=ee.Segment_Id
		where ee.Event_Execution_Id=eeh.Event_Execution_Id
        and ee.Customer_Id between vStart_Cnt and vEnd_Cnt
        and eeh.Customer_Id between vStart_Cnt and vEnd_Cnt;
                
		SET vStart_Cnt=vEnd_Cnt+1;
        IF vStart_Cnt  >= @Rec_Cnt THEN
            LEAVE PROCESS_LOOP;
        END IF;         
 END LOOP PROCESS_LOOP;
 end LOOP;
close cur1;
 UPDATE ETL_Execution_Details SET Status = 'Succeeded', End_Time = NOW() WHERE Procedure_Name = 'S_CLM_Update_Segment_Event_Execution' AND Execution_ID = Exe_ID;   
END

